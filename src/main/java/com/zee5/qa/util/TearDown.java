package com.zee5.qa.util;

import org.testng.annotations.AfterSuite;
import com.zee5.qa.pages.LoginPage;


public class TearDown {

	SendMail sendmail;
	LoginPage loginPage;

	public TearDown() {
		super();
	}

	@AfterSuite
	public void tearDown() throws Exception {
		Thread.sleep(3000);		
		sendmail=new SendMail();
		sendmail.sendMail();

	}

}
