package com.zee5.qa.util;

import java.io.IOException;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import com.zee5.qa.base.TestBase;

import com.aventstack.extentreports.Status;

public class Listener implements ITestListener {

	public void onFinish(ITestContext arg0) {
		// TODO Auto-generated method stub

	}

	public void onStart(ITestContext arg0) {
		// TODO Auto-generated method stub
	}

	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		// TODO Auto-generated method stub
	}

	public void onTestFailure(ITestResult result) {

		System.setProperty("org.uncommons.reportng.escape-output", "false");
		try {
			TestUtil.captureScreenshot(result.getName(),"Fail");
		} catch (IOException e) {
			e.printStackTrace();
		}
		//Reporter.log("<a href=" + TestUtil.fileName + " target=\"_blank\">Screenshot Captured</a>");
		//Reporter.log("<br>");
		//Reporter.log("<a href=" + TestUtil.fileName + " target=\"_blank\"><img src="+"/Screenshots/"+TestUtil.fileName+" height=200 width=200></a>");
		TestBase.logger.log(Status.FAIL, result.getName()+" - Failed. Failure reason is below.");		
		TestBase.logger.info(result.getThrowable());
	}

	public void onTestSkipped(ITestResult result) {
		// TODO Auto-generated method stub

	}

	public void onTestStart(ITestResult result) {
		// TODO Auto-generated method stub

	}

	public void onTestSuccess(ITestResult result) {
		// TODO Auto-generated method stub
		TestBase.logger.log(Status.PASS, result.getName()+" - Verified");		

	}

}
