package com.zee5.qa.util;


import java.io.File;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.commons.compress.archivers.dump.InvalidFormatException;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.zee5.qa.base.TestBase;



public class TestUtil extends TestBase{
	
	public static long PAGE_LOAD_TIMEOUT = 40;
	public static long IMPLICIT_WAIT = 20;
	public static Logger log = Logger.getLogger(TestUtil.class);
	public static String  fileName;
	
	public static String TESTDATA_SHEET_PATH = "C:/Users/ravindra.pal/Desktop/Zee5Automation"
			+ "/src/main/java/com/zee5/qa/ExcelData/TestData.xlsx";

	static Workbook book;
	static Sheet sheet;

	/* Below method will return data from provided sheetName */

	public static Object[][] getTestData(String sheetName) {
		FileInputStream file = null;
		try {
			file = new FileInputStream(TESTDATA_SHEET_PATH);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		try {
			book = WorkbookFactory.create(file);
		} catch (InvalidFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		sheet = book.getSheet(sheetName);
		Object[][] data = new Object[sheet.getLastRowNum()][sheet.getRow(0).getLastCellNum()];
		// System.out.println(sheet.getLastRowNum() + "--------" +
		// sheet.getRow(0).getLastCellNum());
		for (int i = 0; i < sheet.getLastRowNum(); i++) {
			for (int k = 0; k < sheet.getRow(0).getLastCellNum(); k++) {
				data[i][k] = sheet.getRow(i + 1).getCell(k).toString();
				System.out.println(data[i][k]);
			}
		}
		return data;
	}

	public String timestamp() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public static void captureScreenshot(String page,String type) throws IOException {
		
		LocalDateTime myDateObj = LocalDateTime.now();
		DateTimeFormatter date = DateTimeFormatter.ofPattern("E, dd-MM-yyyy hh:mm a");  
		String formattedDate = myDateObj.format(date).replace(":", "-");
		DateTimeFormatter day = DateTimeFormatter.ofPattern("E, dd-MM-yy");
		String formattedDay = myDateObj.format(day);
		fileName = page +" " + formattedDate + ".jpg";
		File scr = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		switch(type)
		{
		case "Movie":
			FileUtils.copyFile(scr, new File(System.getProperty("user.dir") + "/Screenshots/Movies/"+formattedDay+"/" +fileName));
			FileUtils.copyFile(scr, new File(System.getProperty("user.dir") + "/test-output/Screenshots/Movies/"+formattedDay+"/" +fileName));
			break;
			
		case "Video":			
			FileUtils.copyFile(scr, new File(System.getProperty("user.dir") + "/Screenshots/Videos/"+formattedDay+"/" +fileName));
			FileUtils.copyFile(scr, new File(System.getProperty("user.dir") + "/test-output/Screenshots/Videos/"+formattedDay+"/" +fileName));
			break;
			
		case "TV Show":			
			FileUtils.copyFile(scr, new File(System.getProperty("user.dir") + "/Screenshots/TV Shows/"+formattedDay+"/" +fileName));
			FileUtils.copyFile(scr, new File(System.getProperty("user.dir") + "/test-output/Screenshots/TV Shows/"+formattedDay+"/" +fileName));
			break;
			
		case "Season":		
			FileUtils.copyFile(scr, new File(System.getProperty("user.dir") + "/Screenshots/Seasons/"+formattedDay+"/" +fileName));
			FileUtils.copyFile(scr, new File(System.getProperty("user.dir") + "/test-output/Screenshots/Seasons/"+formattedDay+"/" +fileName));
			break;
			
		case "Episode":			
			FileUtils.copyFile(scr, new File(System.getProperty("user.dir") + "/Screenshots/Episodes/"+formattedDay+"/" +fileName));
			FileUtils.copyFile(scr, new File(System.getProperty("user.dir") + "/test-output/Screenshots/Episodes/"+formattedDay+"/" +fileName));
			break;
			
		case "Fail":
			FileUtils.copyFile(scr, new File(System.getProperty("user.dir") + "/Screenshots/Failed Tests/"+formattedDay+"/" +fileName));
			FileUtils.copyFile(scr, new File(System.getProperty("user.dir") + "/test-output/Screenshots/Failed Tests/"+formattedDay+"/" +fileName));
		}
	}
}


