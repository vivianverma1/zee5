
package com.zee5.qa.base;

import java.io.FileInputStream;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;
import com.zee5.qa.util.TestUtil;

import io.github.bonigarcia.wdm.WebDriverManager;

public class TestBase {
	
	public static WebDriver driver;
	public static Properties prop;
	public static Logger log = Logger.getLogger(TestBase.class);
	public static ExtentHtmlReporter htmlReporter;
	public static ExtentReports report;
	public static ExtentTest logger;

	
	public TestBase() {
		
		try {
			prop = new Properties();
			
			FileInputStream fis = new FileInputStream(System.getProperty("user.dir") + "/src/main/java/com/zee5/qa/config/config.properties");
			prop.load(fis);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();

		}
		
	}
	
	public void initializeExtentReport(String testName) throws Exception{
		LocalDateTime myDateObj = LocalDateTime.now();
		DateTimeFormatter date = DateTimeFormatter.ofPattern("dd-MM-yyyy hh:mm a");  
		String formattedDate = myDateObj.format(date).replace(":", "-");
		DateTimeFormatter day = DateTimeFormatter.ofPattern("E, dd-MM-yy");
		String formattedDay = myDateObj.format(day);
		String reportName = "Test Report "+testName+" "+formattedDate;
		htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") + "/test-output/Extent-Reports/"+formattedDay+"/"+reportName+".html");
    	report = new ExtentReports();  
		report.attachReporter(htmlReporter);
		report.setSystemInfo("Host Name",System.getProperty("user.name") );
		report.setSystemInfo("Environment", "QA");
		report.setSystemInfo("User Name", System.getenv("USERNAME"));
    	htmlReporter.config().setDocumentTitle("Automation Test Results"); 
    	htmlReporter.config().setReportName("Automation Test Results :  "); 
    	htmlReporter.config().setTheme(Theme.DARK);
	}
	
	public void initializeExtentReportForSuiteTest(String testName) throws Exception{

		String reportName = "Test Report "+testName;
		//htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") + "/test-output/Extent-Reports/"+reportName+".html");
		htmlReporter = new ExtentHtmlReporter("/opt/atlassian/pipelines/agent/build/test-output/Extent-Reports/"+reportName+".html");
    	report = new ExtentReports();  
		report.attachReporter(htmlReporter);
		report.setSystemInfo("Host Name",System.getProperty("user.name") );
		report.setSystemInfo("Environment", "QA MS2");
		report.setSystemInfo("User Name", System.getenv("USERNAME"));
    	htmlReporter.config().setDocumentTitle("MS2 Automation Test Results"); 
    	htmlReporter.config().setReportName("MS2 Automation Test Results :  "); 
    	htmlReporter.config().setTheme(Theme.DARK);
	}
	
	@SuppressWarnings("deprecation")
	public static void initialization(String browserName) throws InterruptedException, MalformedURLException{
		String os = System.getProperty("os.name").toLowerCase();
		if(os.contains("windows"))
		{
			System.out.println("Operation System is :  "+os);
			//String browserName = prop.getProperty("browser");

			if (browserName.equals("chrome")) {
				//WebDriverManager.chromedriver().setup();
				//driver = new ChromeDriver();
			
		
				//------------------- For Selenium Docker ------------------------
				ChromeOptions options = new ChromeOptions();
				options.addArguments("--disable-dev-shm-usage");
				options.addArguments("--no-sandbox");
				driver = new RemoteWebDriver(new URL("http://localhost:5910/wd/hub"),options);		
				log.info("Opening Chrome Browser");
				
				
			} else if (browserName.equals("firefox")) {
				WebDriverManager.firefoxdriver().setup();
				DesiredCapabilities capabilities = new DesiredCapabilities();
				capabilities.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.ACCEPT);
				driver = new FirefoxDriver(capabilities);
				log.info("Opening Firefox Browser");
				

			} else if (browserName.equals("ie")) {
				WebDriverManager.iedriver().setup();
				driver = new InternetExplorerDriver();
				log.info("Opening IE Browser");

			}else if (browserName.equals("chromeoptions")) {
				WebDriverManager.chromedriver().setup();
				driver = new ChromeDriver();
				ChromeOptions options = new ChromeOptions();
				options.addArguments("--incognito");  				//to open Chrome browser in incognito mode
				options.addArguments("--headless");					//to open headless browser
				DesiredCapabilities capabilities = DesiredCapabilities.chrome();
				capabilities.setCapability(ChromeOptions.CAPABILITY, options);
				log.info("Opening Chrome options Browser");
			}
				
			//System.setProperty("webdriver.chrome.driver", "./jars/chromedriver.exe");
			
			driver.manage().window().maximize();        //code to maximize the screen.
			driver.manage().deleteAllCookies();			// Code to delete all the cookies of the browser
			driver.manage().timeouts().pageLoadTimeout(TestUtil.PAGE_LOAD_TIMEOUT, TimeUnit.SECONDS);       
			driver.manage().timeouts().implicitlyWait(TestUtil.IMPLICIT_WAIT, TimeUnit.SECONDS);
			driver.manage().timeouts().setScriptTimeout(TestUtil.PAGE_LOAD_TIMEOUT, TimeUnit.SECONDS);
			driver.get(prop.getProperty("url"));
		}

		else
		{
			System.out.println("Operation System is :  "+os);
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--disable-dev-shm-usage");
			options.addArguments("--no-sandbox");
			driver = new RemoteWebDriver(new URL("http://localhost:5910/wd/hub"),options);		
			driver.get(prop.getProperty("url"));
		
		
	}
}
}
