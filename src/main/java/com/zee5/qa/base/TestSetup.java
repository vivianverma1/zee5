package com.zee5.qa.base;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import com.zee5.qa.pages.LoginPage;
import com.zee5.qa.util.SendMail;

public class TestSetup extends TestBase {

	SendMail sendmail;
	LoginPage loginPage;

	public TestSetup() {
		super();
	}

	@Parameters({"browserType"})
	@BeforeTest
	public void setup(@Optional String browser) throws Exception{

		try {
		if(browser.equalsIgnoreCase("chrome"))
		{
		System.out.println("Initialize Chrome");
		initializeExtentReportForSuiteTest("Chrome");
		initialization("chrome");
		//loginPage = new LoginPage();
		//loginPage.gmailLogin(prop.getProperty("Gusername"),prop.getProperty("adminPass"));
		}
		else if(browser.equalsIgnoreCase("firefox"))
		{	
		System.out.println("Initialize Firefox");
		initializeExtentReportForSuiteTest("Firefox");
		initialization("firefox");
		loginPage = new LoginPage();
		loginPage.gmailLogin(prop.getProperty("Gusername"),prop.getProperty("adminPass"));
		}
		}
		catch(Exception e)
		{
		initializeExtentReportForSuiteTest("Chrome");
		initialization("chrome");
		//loginPage = new LoginPage();
		//loginPage.gmailLogin(prop.getProperty("Gusername"),prop.getProperty("adminPass"));
		}

	}
	

	@AfterSuite
	public void tearDown() throws Exception {
		Thread.sleep(3000);
		report.flush();
		Thread.sleep(3000);
		//sendmail=new SendMail();
		//sendmail.sendMail();
		driver.quit();
	}

}
