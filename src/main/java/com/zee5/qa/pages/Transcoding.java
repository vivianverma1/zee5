package com.zee5.qa.pages;

import java.io.IOException;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.Reporter;
import com.qa.utilities.WebUtility;
import com.zee5.qa.util.TestUtil;
import com.zee5.qa.base.TestBase;

public class Transcoding extends TestBase {
    JavascriptExecutor js = (JavascriptExecutor) driver;
    String videoTitle = "Star Trek";

    @FindBy(xpath = "//button[contains(@class,'auto-tab-Dashboard')]")
    WebElement dashboard;
    
    @FindBy(xpath = "//div[contains(@class,'auto-Video')]")
    WebElement videoButton;
    
    @FindBy(name = "searchString")
    WebElement searchString;
    
    @FindBy(xpath = "//div[contains(@class,'mov-icon mov-view tooltip-sec auto-view')]")
    List < WebElement > editButton;
    
    @FindBy(xpath = "//input[@name='mediathekFileUid']")
    WebElement mediaThekUID;
    
    @FindBy(linkText = "Import Details")
    WebElement importDetails;
    
    @FindBy(xpath = "//button[contains(@class,'auto-leftTab-Videos')]")
    WebElement videos;
    
    @FindBy(xpath = "//div[contains(@class,'mark-fill-active')]")
    WebElement doneButtonActive;
    
    
    //TODO -- Work in Progress --
    public Transcoding() {
    	PageFactory.initElements(driver, this); 
	} 

    public void TranscodingTest() throws InterruptedException, Exception {
	    WebUtility.Click(dashboard);
	    WebUtility.Click(videoButton);
		WebUtility.Click(searchString);
		searchString.sendKeys(videoTitle);
		WebUtility.Wait();
		WebUtility.Wait();
		WebUtility.Click(editButton.get(0));
	
		WebUtility.Wait();
		WebUtility.Click(videos);
		
        mediaThekUID.sendKeys(Keys.CONTROL + "a");
        mediaThekUID.sendKeys(Keys.BACK_SPACE);
        mediaThekUID.sendKeys("Soudaminir_Sansar_Ep364_Preview_12012021_bn");
        
        importDetails.click();
        WebUtility.Wait();
    
        //TODO -------- Update this when the mark as done button is clicked by default -----------
        
        String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
        Assert.assertEquals(doneButtonActiveColor, "rgba(255, 255, 255, 1)", "Video Section Import Details Mark As Done Assertion Fails");
        WebUtility.Click(doneButtonActive);
    }
}
