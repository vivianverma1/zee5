package com.zee5.qa.pages;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Reporter;
import org.testng.asserts.SoftAssert;

import com.qa.utilities.WebUtility;
import com.zee5.qa.base.TestBase;
import com.zee5.qa.util.TestUtil;


public class EpisodeQuickfillingPage extends TestBase {
	
		String tvShowQuickFilingTitle = "The Expanse Quick Filing";
		JavascriptExecutor js = (JavascriptExecutor) driver; 
		Actions actionProvider = new Actions(driver);
		SoftAssert sa = new SoftAssert();
		
	    @FindBy(xpath = "//div[contains(@class,'auto-TVShow')]")
	    WebElement tvShowButton;

	    @FindBy(xpath = "//div[contains(text(),'Movie')]")
	    WebElement movieButton;

	    @FindBy(xpath ="//div[contains(@class,'mov-icon mov-view tooltip-sec auto-view')]")
	    List < WebElement > editButton;

	    @FindBy(xpath = "//span[contains(text(),'Edit TV Show')]")
	    WebElement editTvShow;

	    @FindBy(name = "searchString")
	    WebElement searchString;

	    @FindBy(xpath = "//span[contains(text(),'Yes') or contains(text(),'Ok')]")
	    List < WebElement > yes;

	    @FindBy(xpath = "//span[contains(text(),'Ok') or contains(text(),'ok')]")
	    WebElement ok;
	    
	    @FindBy(xpath = "//span[contains(text(),'No')]")
	    WebElement no;
	    
	    @FindBy(xpath ="//span[contains(text(),'Save')]")
	    WebElement save;

	    @FindBy(css = "span.MuiIconButton-label > svg.MuiSvgIcon-root.MuiSvgIcon-fontSizeSmall")
	    WebElement clearInput;
	    @FindBy(className = "next-step-btn")
	    WebElement nextButton;
	    
	    @FindBy(className = "prev-step-btn")
	    WebElement previousButton;
	   

	    @FindBy(xpath = "//div[contains(@class,'mark-fill-active')]")
	    WebElement doneButtonActive; // rgba(255, 255, 255, 1)

	    @FindBy(xpath = "//li[@role='option' or @role='menuitem']")
	    List < WebElement > dropDownList;

	    @FindBy(xpath = "//div[contains(@class,'mark-active')]")
	    WebElement doneButtonClicked; // rgba(52, 194, 143, 1)

	    @FindBy(xpath = "//button[contains(@class,'auto-tab-Dashboard')]")
	    WebElement dashboard;
	    
	    @FindBy(xpath = "//div[@class='MuiPaper-root MuiPopover-paper MuiPaper-elevation8 MuiPaper-rounded']")
	    WebElement datePicker;
	 // --------------------------------------------- Episode Properties---------------------------------------------------------------
	    @FindBy(className =  "auto-quickLinks-Seasons")
	    WebElement seasons;
	    
	    @FindBy(className =  "auto-quickLinks-Episodes")
	    WebElement episodes;    
	    
	    @FindBy(xpath =  "//button[contains(.,'Create Episode')]")
	    WebElement createEpisode;     
	    
	    @FindBy(xpath =  "(//input[@name='selectCreateType'])[1]")
	    WebElement createManualEpisode;  
	   
	    @FindBy(xpath =  "(//input[@name='selectJourney'])[3]")
	    WebElement createMainEpisode;  
	    
	    @FindBy(xpath =  "(//input[@name='selectJourney'])[2]")
	    WebElement createQuickFilingEpisode; 
	    
	    @FindBy(xpath =  "(//input[@name='selectJourney'])[1]")
	    WebElement createSingleLandingEpisode;   
	    
	    @FindBy(xpath = "//button[contains(.,'Episode Properties')]")
	    WebElement episodeProperties;
	    
		@FindBy(xpath = "//div[contains(@class,'s-badge create-movie-stage ')]")
		WebElement contentStatus; 
		
	    ////////////////Episode Quick filling Properties
	    @FindBy(name = "index")
	    WebElement index;
	    
	    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-0')]")
	    WebElement editQuickFillingTitle;
	  
	    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-1')]")
	    WebElement editQuickFillingShortDesc;
	    
	    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-2')]")
	    WebElement editQuickFillingIndex;
	    
	    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-3')]")
	    WebElement editQuickFillingSubType;
	    
	    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-4')]")
	    WebElement editQuickFillinZee5ReleaseDate;
	    
	    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-5')]")
	    WebElement editQuickFillingVideoDuration;
	    
	    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-6')]")
	    WebElement editQuickFillingAudioLanguage;
	  
	  
	    @FindBy(xpath = "//button[contains(@class,'auto-leftTab-TitleSummary')]")  
	    WebElement titleSummary;
	    
	    @FindBy(xpath = "//input[@name=\"title\"]")
	    WebElement title;
	    
	    @FindBy(css = "button.MuiButtonBase-root.MuiIconButton-root.MuiAutocomplete-popupIndicator.MuiAutocomplete-popupIndicatorOpen > span.MuiIconButton-label > svg.MuiSvgIcon-root")
	    WebElement closeDropdown;

	    @FindBy(xpath = "//div[contains(@id,'auto-shortDescription')]//child::div[contains(@class,'ql-editor')]")
	    WebElement shortDesc;
	    
	    @FindBy(xpath = "//input[@id='subtype']")
	    WebElement subType;
	    
	    @FindBy(name = "dateZee5Published")
	    WebElement zee5ReleaseDate;
	    
	    @FindBy(name = "videoDuration")
	    WebElement videoDuration;
	    
	    @FindBy(xpath = "//input[@id='audioLanguages']")
	    WebElement audioLanguage;
	    
	    @FindBy(id = "certification")
	    WebElement certification;
	    
	    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-0')]")
	    WebElement editCertification;
	    ////////////Videos
	    @FindBy(xpath = "//button[contains(.,'Video')]")
	    WebElement videos;
	    
	    @FindBy(xpath = "//input[@name='dashRootFolderName']")
	    WebElement dashRootVideo;

	    @FindBy(xpath = "//input[@name='dashManifestName']")
	    WebElement dashManifestVideo;
	    
	    @FindBy(xpath = "//input[@name='hlsRootFolderName']")
	    WebElement hlsRootVideo;

	    @FindBy(xpath = "//input[@name='hlsManifestName']")
	    WebElement hlsManifestVideo;
	    
	    @FindBy(xpath = "//input[@name='mediathekFileUid']")
	    WebElement mediaThekUID;
	    
	    @FindBy(linkText = "Import Details")
	    WebElement importDetails;
	    
	    ////////////////License
	    @FindBy(xpath = "//button[contains(@class,'auto-leftTab-LicenseModule')]")
	    WebElement license;

	    @FindBy(xpath = "//div[contains(text(),'Create License')]")
	    WebElement createLicense;

	    @FindBy(xpath = "//input[@name='fromDate']")
	    WebElement licenseFromDate;

		@FindBy(xpath = "//input[@name='toDate']")
		WebElement licenseToDate;

		@FindBy(xpath = "//input[contains(@id,'country')]")
	    WebElement licenseCountry;
		
		@FindBy(xpath = "//input[contains(@id,'businessType')]")   
	    WebElement licenseBusinessType;
		
		@FindBy(xpath = "//input[contains(@id,'platform')]")   
	    WebElement licensePlatform;		
		
		@FindBy(xpath = "//input[contains(@id,'tvodTier')]")
		WebElement licenseTVOD;

		@FindBy(xpath = "//span[contains(text(),\"SAVE\")]")
		WebElement saveLicense;

	    @FindBy(xpath = "//div[@role='tooltip']")
	    WebElement inactivateLicenseDropdown;

	    @FindBy(xpath = "//div[@class='ml-10 status-dropdown val']")
	    WebElement licenseStatusButton;

	    @FindBy(id = "auto-reason-0-0")
	    WebElement inactivateLicenseReason;
	    
	    @FindBy(xpath = "//input[@name='setName']")
	    WebElement licenseSetName;
	    
	    @FindBy(xpath = "//button[contains(@class,'auto-button-short-btn')]")
	    WebElement expiredLicense;

	    @FindBy(className = "item")
	    WebElement expiredLicenseBack;

	    @FindBy(xpath = "//button[contains(@class,'auto-button-filter-btn')]")
	    WebElement licenseFilters;

	    @FindBy(id = "auto-businessType-0-2")
	    WebElement licenseFilterBusinessType;

	    @FindBy(id = "auto-billingType-0-3")
	    WebElement licenseFilterBillingType;

	    @FindBy(id = "auto-platform-0-4")
	    WebElement licenseFilterPlatform;

	    @FindBy(xpath = "//input[@name='status' and @value = '1']")
	    WebElement licenseFilterActiveStatus;
	    
	    @FindBy(xpath = "//input[@name='status' and @value = '0']")
	    WebElement licenseFilterInActiveStatus;

	    @FindBy(xpath = "//span[contains(text(),'Apply Filter')]")
	    WebElement applyFilter;
	    
	    @FindBy(xpath = "//span[contains(text(),'Clear')]")
	    WebElement clearFilter;
	    
	    @FindBy(css = "button.MuiButtonBase-root.MuiIconButton-root.MuiAutocomplete-popupIndicator.MuiAutocomplete-popupIndicatorOpen > span.MuiIconButton-label > svg.MuiSvgIcon-root")
	    WebElement filterCloseDropdown;
	    
	    ///////////////////quick images//////
	    //--------------------------------------------Images -----------------------------------------------------------------
	    @FindBy(xpath = "//button[contains(@class,'auto-leftTab-Images')]")
	    WebElement images;

	    @FindBy(name = "cover")
	    WebElement cover;

	    @FindBy(name = "appcover")
	    WebElement appcover;

	    @FindBy(name = "list")
	    WebElement list;

	    @FindBy(name = "square")
	    WebElement square;

	    @FindBy(name = "tvcover")
	    WebElement tvCover;

	    @FindBy(name = "portrait")
	    WebElement portrait;

	    @FindBy(name = "listclean")
	    WebElement listClean;

	    @FindBy(name = "portraitclean")
	    WebElement portraitClean;

	    @FindBy(name = "telcosquare")
	    WebElement telcoSquare;

	    @FindBy(name = "passport")
	    WebElement passport;

	    @FindBy(xpath = "//button[contains(@class,'upload-btn')]")
	    WebElement imagesCreateNewSet;
	    
	    @FindBy(name = "setName")
	    WebElement newSetName;

	    @FindBy(id="auto-GroupCountry-0-1")
	    WebElement newSetCountry;
	    
	    @FindBy(id = "auto-Platform-0-2")
	    WebElement newSetPlatform;
	    
	    @FindBy(id = "auto-gender-0-0")
	    WebElement newSetGender;
	    
	    @FindBy(id="auto-genre-0-1")
	    WebElement newSetGenre;
	    
	    @FindBy(id = "auto-ageGroup-0-2")
	    WebElement newSetAgeGroup;
	    
	    @FindBy(id = "auto-language-0-3")
	    WebElement newSetLanguage;
	    
	    @FindBy(name="others")
	    WebElement newSetOthers;
	    
	    @FindBy(xpath = "//div[contains(@class,'question-list flex')]")
	    List<WebElement> imageSets;
	    
	    @FindBy(xpath = "(//button[contains(@class,'Active')])[1]")
	    WebElement imageSetActiveButton;
	    
	    @FindBy(xpath = "//span[@class='remove']")
	    List<WebElement> removeSet;
	///////////////Quick Seo///////////////
	    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-1']")
	    WebElement editSeoTitleTag;  
	    
	    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-2']")
	    WebElement editSeoMetaDescription;
	    
	    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-3']")
	    WebElement editSeoMetaSynopsis;
	    
	    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-4']")
	    WebElement quickeditSeoH1Heading;
	    
	    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-5']")
	    WebElement quickeditSeoH2Heading;
	    
	    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-6']")
	    WebElement quickeditSeoVideoSchema;
	    
	    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-7']")
	    WebElement quickeditSeoWebSchema;
	    
	    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-8']")
	    WebElement quickeditSeoBreadcrunSchema;
	    
	    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-9']")
	    WebElement quickeditSeoimageSchema;
	    
	    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-10']")
	    WebElement quickeditSeoOrganizationSchema;
	    
	    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-11']")
	    WebElement editSeoH5Heading;
	    
	    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-12']")
	    WebElement editSeoH6Heading;
	    @FindBy(xpath = "//button[contains(@class,'auto-leftTab-SEODetails')]")
	    WebElement seoDetails;

	    @FindBy(name = "titleTag")
	    WebElement seoTitleTag;

	    @FindBy(name = "metaDescription")
	    WebElement seoMetaDescription;

	    @FindBy(name = "metaSynopsis")
	    WebElement seoMetaSynopsis;

	    @FindBy(id = "redirectionType")
	    WebElement seoRedirectionType;

	    @FindBy(name = "redirectionLink")
	    WebElement seoRedirectionLink;

	    @FindBy(name = "noIndexNoFollow")
	    WebElement seoNoIndexNoFollow;

	    @FindBy(name = "h1Heading")
	    WebElement seoH1Heading;

	    @FindBy(name = "h2Heading")
	    WebElement seoH2Heading;

	    @FindBy(name = "h3Heading")
	    WebElement seoH3Heading;

	    @FindBy(name = "h4Heading")
	    WebElement seoH4Heading;

	    @FindBy(name = "h5Heading")
	    WebElement seoH5Heading;

	    @FindBy(name = "h6Heading")
	    WebElement seoH6Heading;

	    @FindBy(name = "robotsMetaNoIndex")
	    List < WebElement > seoRobotsMetaIndex;

	    @FindBy(name = "robotsMetaImageIndex")
	    WebElement seoRobotsMetaImageIndex;

	    @FindBy(name = "robotsMetaImageNoIndex")
	    WebElement seoRobotsMetaImageNoIndex;

	    @FindBy(name = "breadcrumbTitle")
	    WebElement seoBreadcrumbTitle;

	    @FindBy(name = "internalLinkBuilding")
	    WebElement seoInternalLinkBuilding;

	    @FindBy(xpath = "//input[@type=\"checkbox\"]")
	    List < WebElement > seoCheckboxes;
	    //----------------------------------- CheckList---------------------------------------------------

	    @FindBy(xpath = "//button[contains(@class,'auto-leftTab-Checklist')]")
	    WebElement checklist;

	    @FindBy(xpath = "//div[contains(@class,'auto-countryGroup')]//descendant::input")
	    WebElement checklistCountry;

	    @FindBy(xpath = "//span[contains(text(),'Publish Content')]")
	    WebElement publishContent;
	    
	    @FindBy(xpath="//span[contains(text(),'Edit Season')]")
	    WebElement editseasonback;

	    @FindBy(xpath = "//span[contains(text(),'Republish Content')]")
	    WebElement republishContent;

	    @FindBy(xpath = "//span[contains(text(),'Unpublish Content')]")
	    WebElement unpublishContent;

	    @FindBy(xpath = "//span[@class='edit tooltip-sec hand-cursor']")
	    WebElement restoreButton;

	    @FindBy(id = "archive-icon_svg__Rectangle_273")
	    List < WebElement > archiveButton;

	    @FindBy(xpath = "//div[contains(@class,'Schedule-box')]//div[contains(@class,'add-btn create-btn')]")
	    WebElement scheduleContent;

	    @FindBy(name = "scheduledPublicationTime")
	    WebElement scheduledPublicationTime;

	    @FindBy(xpath = "//div[contains(@class,'auto-countryGroup')]//descendant::input")
	    List < WebElement > scheduledCountry;

	    @FindBy(xpath = "//div[contains(@class,'Schedule-box m-b-20 ')]//div[contains(@class,'auto-countryGroup')]//input")
	    WebElement scheduledContentCountry;
	    
	    @FindBy(xpath = "//span[contains(text(),'Schedule Content')]")
	    WebElement scheduleContentButton;

	    @FindBy(xpath = "//label[contains(text(),'Country')]//following::input")
	    List<WebElement> unpublishReason;

	    @FindBy(xpath = "//li[contains(text(),'Published History')]")
	    WebElement publishedHistory;

	    @FindBy(xpath = "//a[contains(@class,'auto-breadCrum-Episodes')]")
	    WebElement episodesBreadcrumb;

		@FindBy(xpath = "(//div[contains(@class,'val')])[3]")
		WebElement scheduledTime;
		
		@FindBy(xpath = "(//div[contains(@class,'icon-w-text icon-w-14 m-b-10')])[1]")
		WebElement currentTime;	
		
		@FindBy(xpath = "(//button[contains(@class,'MuiPickersToolbarButton-toolbarBtn')])[4]")
		WebElement datePickerMinutes;		
		
		@FindBy(xpath = "//span[contains(@class,'MuiTypography-root MuiPickersClockNumber-clockNumber') and contains(text(),'05')]")
		WebElement datePickerMinutesClock;	
		
		//-----------------------------------------------------------------------------------------------------------------------------------
	    
	
	public EpisodeQuickfillingPage() {
		PageFactory.initElements(driver, this);
	}

	public void QuickFillingEpisodeProperties() throws InterruptedException, Exception {
		JavascriptExecutor js = (JavascriptExecutor) driver;  
		WebUtility.javascript(dashboard);
		WebUtility.Click(tvShowButton);
		WebUtility.Click(searchString);
		searchString.sendKeys(tvShowQuickFilingTitle);
		searchString.sendKeys(Keys.ENTER);
		WebUtility.Wait();
		WebUtility.Wait();
		WebUtility.Click(editButton.get(0));
		
		WebUtility.Wait();
		WebUtility.Click(seasons);
		WebUtility.Click(editButton.get(0));
		
		WebUtility.Wait();
		WebUtility.Click(episodes);
		WebUtility.Wait();
		WebUtility.Click(createEpisode);
		createManualEpisode.click();
	
		WebUtility.Click(yes.get(0));
		
		createQuickFilingEpisode.click();
		WebUtility.Click(yes.get(0));
		
		//////////Title summary
		WebUtility.Click(editQuickFillingTitle);
        WebUtility.Click(title);
        title.sendKeys(Keys.CONTROL + "a");
        title.sendKeys(Keys.BACK_SPACE);
        title.sendKeys("The Expanse - Episode Quick Filing");
        WebUtility.Click(editQuickFillingTitle);
    	Thread.sleep(1000);   

        WebUtility.Click(editQuickFillingShortDesc);
        WebUtility.Click(shortDesc);
        shortDesc.sendKeys(Keys.CONTROL + "a");
        shortDesc.sendKeys(Keys.BACK_SPACE);
        shortDesc.sendKeys("The Expanse is an American science fiction television series developed by Mark Fergus and Hawk Ostby - Episode Quick Filing");
        WebUtility.Click(editQuickFillingShortDesc);
    	Thread.sleep(1000);  
        
        WebUtility.Click(editQuickFillingSubType);
        WebUtility.Click(subType);
        clearInput.click();
        WebUtility.Click(dropDownList.get(0));
        WebUtility.Click(editQuickFillingSubType);
        Thread.sleep(1000);  

       

        WebUtility.Click(editQuickFillinZee5ReleaseDate);
        WebUtility.Click(zee5ReleaseDate);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ESCAPE);
        WebUtility.Click(editQuickFillinZee5ReleaseDate);
    	Thread.sleep(1000);  

        WebUtility.Click(editQuickFillingVideoDuration);
        videoDuration.click();
        List < WebElement > videoduration = driver.findElements(By.xpath("//li[@role='button']"));
        for (WebElement e: videoduration) {
            String videohrs = e.getText();
            if (videohrs.contains("06")) {
                e.click();
                break;
            }
        }
		WebUtility.Click(editQuickFillingVideoDuration);
		Thread.sleep(1000);  
		
		WebUtility.Click(editQuickFillingAudioLanguage);
		js.executeScript("arguments[0].focus()", audioLanguage);
		audioLanguage.click();
		WebUtility.Wait();
		WebUtility.Click(clearInput);
		WebUtility.Click(dropDownList.get(0));
		WebUtility.Click(editQuickFillingAudioLanguage);
		Thread.sleep(1000);  
		nextButton.click();
		Thread.sleep(1000);
		
		//////////////Classification
		editCertification.click();
		Thread.sleep(1000);
		certification.click();
		Thread.sleep(1000);  
		certification.sendKeys(Keys.BACK_SPACE);
		certification.sendKeys(Keys.BACK_SPACE);
		certification.sendKeys(Keys.ARROW_DOWN);
		certification.sendKeys(Keys.ENTER);
		WebUtility.element_to_be_clickable(editCertification);
		Thread.sleep(1000);
		editCertification.click();
		Thread.sleep(1000);

		nextButton.click();
		Thread.sleep(2000);
		String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
		sa.assertTrue(doneButtonActiveColor.contains("255, 255, 255") || doneButtonActiveColor.contains("255,255,255"), "Episode Quick Filing -  Properties Mark As Done Assertion Fails");
		WebUtility.element_to_be_clickable(doneButtonActive);
		doneButtonActive.click();

		
		 try {
	            TestUtil.captureScreenshot("Episode Quick Filing Properties Page","Episode");
	        } catch (IOException e) {
	            System.out.println("Screenshot Capture Failed ->" + e.getMessage());
	        }



		

	}

	public void QuickFillingEpisodeVideoSection() throws InterruptedException {

        WebUtility.Click(videos);
        WebUtility.Wait();       
         /*   
        dashRootVideo.sendKeys(Keys.CONTROL + "a");
        dashRootVideo.sendKeys(Keys.BACK_SPACE);
        dashRootVideo.sendKeys("Video Dash root Episode Quick Filing");

       
        dashManifestVideo.sendKeys(Keys.CONTROL + "a");
        dashManifestVideo.sendKeys(Keys.BACK_SPACE);
        dashManifestVideo.sendKeys("Video Dash Manifest Episode Quick Filing");
      
        hlsRootVideo.sendKeys(Keys.CONTROL + "a");
        hlsRootVideo.sendKeys(Keys.BACK_SPACE);
        hlsRootVideo.sendKeys("Test Data Episode Quick Filing");
        
        hlsManifestVideo.sendKeys(Keys.CONTROL + "a");
        hlsManifestVideo.sendKeys(Keys.BACK_SPACE);
        hlsManifestVideo.sendKeys("Test Data Episode Quick Filing");
       */ 
        mediaThekUID.sendKeys(Keys.CONTROL + "a");
        mediaThekUID.sendKeys(Keys.BACK_SPACE);
        mediaThekUID.sendKeys("Kumkum_Bhagya_2_Episode_04_May_2021_bn");
        
        importDetails.click();
        WebUtility.Click(yes.get(0));
        WebUtility.Click(ok);
        WebUtility.Wait();
        
        String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
        sa.assertTrue(doneButtonActiveColor.contains("255, 255, 255") || doneButtonActiveColor.contains("255,255,255"), "Episode Quick Filing -  Videos Section Mark As Done Assertion Fails");
        WebUtility.Click(doneButtonActive);


	}
	public void quickfillingEpisodeLicense() throws InterruptedException, Exception{
		WebUtility.Click(license);
		WebUtility.Click(createLicense);

		WebUtility.Click(licenseSetName);
        licenseSetName.sendKeys("Automation Test Data");
        
        WebUtility.Click(licenseFromDate);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ESCAPE);
        
        WebUtility.Click(licenseToDate);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ESCAPE);
	
        WebUtility.Click(licenseCountry);		
        for (int x = 0; x < dropDownList.size(); x++) {
        	if(x<4) {
            WebUtility.Click(dropDownList.get(x));
        	}
        	else {
        		break;
        	}
        }
		
		
		WebUtility.Click(licenseBusinessType);
		WebUtility.Click(dropDownList.get(0));

		WebUtility.Wait();
		driver.findElement(By.xpath("//span[contains(text(),'SAVE')]")).click();
		
		WebUtility.Click(license);
		WebUtility.Click(createLicense);
		
		WebUtility.Click(licenseSetName);
        licenseSetName.sendKeys("Automation Test Data II");

    	WebUtility.Click(licenseFromDate);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ESCAPE);

    	WebUtility.Click(licenseToDate);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ESCAPE);
		
        WebUtility.Click(licenseCountry);
        for (int x = 0; x < dropDownList.size(); x++) {
        	if(x<4) {
            WebUtility.Click(dropDownList.get(x));
        	}
        	else {
        		break;
        	}
        }
		
		
		WebUtility.Click(licenseBusinessType);
		WebUtility.Click(dropDownList.get(0));
		WebUtility.Wait();
		driver.findElement(By.xpath("//span[contains(text(),'SAVE')]")).click();
		WebUtility.Wait();
  
		String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
		sa.assertTrue(doneButtonActiveColor.contains("255, 255, 255") || doneButtonActiveColor.contains("255,255,255"), "Episode Quick Filing -  License Mark As Done Assertion Fails");
		WebUtility.Click(doneButtonActive);
		
        WebUtility.Click(expiredLicense);
        Thread.sleep(2000);
        WebUtility.Click(expiredLicenseBack);
        Thread.sleep(2000);

        WebUtility.Click(licenseFilters);
        WebUtility.Click(licenseFromDate);
        datePicker.sendKeys(Keys.ARROW_LEFT);
        datePicker.sendKeys(Keys.ARROW_LEFT);
        datePicker.sendKeys(Keys.ESCAPE);

        WebUtility.Click(licenseToDate);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ESCAPE);

        WebUtility.Click(applyFilter);

        Thread.sleep(2000);
        WebUtility.Click(licenseFilters);
        WebUtility.Click(clearFilter);
        
        WebUtility.Click(licenseFilters);
        WebUtility.Click(licenseFilterBusinessType);
        WebUtility.Click(dropDownList.get(0));
        WebUtility.Wait();
        WebUtility.Click(applyFilter);

        Thread.sleep(2000);
        WebUtility.Click(licenseFilters);
        WebUtility.Click(clearFilter);
	    
        WebUtility.Click(licenseFilters);
        WebUtility.Wait();
        licenseFilterActiveStatus.click();
        WebUtility.Wait();
        WebUtility.Click(applyFilter);

        Thread.sleep(2000);
        WebUtility.Click(licenseFilters);
        WebUtility.Click(clearFilter);

        WebUtility.Click(licenseFilters);
        WebUtility.Wait();
        licenseFilterInActiveStatus.click();
        WebUtility.Wait();
        WebUtility.Click(applyFilter);

        Thread.sleep(2000);
        WebUtility.Click(licenseFilters);
        WebUtility.Click(clearFilter);

	}
	
	public void quickfillingImages() throws InterruptedException {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		WebUtility.Click(images);

		WebUtility.Wait();
		cover.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\CoverPNG.png");

		WebUtility.Wait();
		appcover.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\AppCoverPNG.png");
		try {
			WebUtility.Wait();
			list.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\List.jpg");
		} catch (Exception e) {

		}
		WebUtility.Wait();
		square.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\Square.jpg");
        
        js.executeScript("window.scrollBy(0,600)");

        Thread.sleep(2000);
        tvCover.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\TVCover.jpg");

        WebUtility.Wait();
        portrait.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\portraitPNG.png");

        WebUtility.Wait();
        listClean.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\ListClean.jpg");

		js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
		
		WebUtility.Wait();
        portraitClean.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\PortraitCleanPNG.png");

        WebUtility.Wait();
        telcoSquare.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\TelcoSquare.jpg");

        WebUtility.Wait();
        passport.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\Passport.jpg");
        WebUtility.Wait();

		
		try {
		WebUtility.Click(imagesCreateNewSet);

		WebUtility.Click(newSetName);
		newSetName.sendKeys("Automation Test Data");

		newSetCountry.click();
		WebUtility.Click(dropDownList.get(0));
		closeDropdown.click();

		newSetPlatform.click();
		WebUtility.Click(dropDownList.get(0));
		closeDropdown.click();

		newSetGender.click();
		WebUtility.Click(dropDownList.get(0));
		closeDropdown.click();

		newSetGenre.click();
		WebUtility.Click(dropDownList.get(0));
		closeDropdown.click();

		newSetAgeGroup.click();
		WebUtility.Click(dropDownList.get(0));
		closeDropdown.click();

		newSetLanguage.click();
		WebUtility.Click(dropDownList.get(0));
		closeDropdown.click();

		newSetOthers.click();
		newSetOthers.sendKeys("Automation Test Data");

		WebUtility.Click(save);

		WebUtility.Click(imageSets.get(1));

		js.executeScript("window.scrollTo(0,0)");
		Thread.sleep(2000);
		WebUtility.Wait();
        cover.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\CoverPNG.png");

        WebUtility.Wait();
        appcover.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\AppCoverPNG.png");

        WebUtility.Wait();
        list.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\List.jpg");

        WebUtility.Wait();
        square.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\Square.jpg");
        
        js.executeScript("window.scrollBy(0,600)");

        Thread.sleep(2000);
        tvCover.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\TVCover.jpg");

        WebUtility.Wait();
        portrait.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\portraitPNG.png");

        WebUtility.Wait();
        listClean.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\ListClean.jpg");

		js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
		
		WebUtility.Wait();
        portraitClean.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\PortraitCleanPNG.png");

        WebUtility.Wait();
        telcoSquare.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\TelcoSquare.jpg");

        WebUtility.Wait();
        passport.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\Passport.jpg");
        WebUtility.Wait();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		try {
			// -------------------------- Image Set 3
			// -----------------------------------------------------
			WebUtility.element_to_be_fluentwait(imagesCreateNewSet);
			WebUtility.Click(imagesCreateNewSet);

			WebUtility.Click(newSetName);
			newSetName.sendKeys("Automation Test Data II");

			newSetCountry.click();
			WebUtility.Click(dropDownList.get(1));
			closeDropdown.click();

			newSetPlatform.click();
			WebUtility.Click(dropDownList.get(1));
			closeDropdown.click();

			newSetGender.click();
			WebUtility.Click(dropDownList.get(1));
			closeDropdown.click();

			newSetGenre.click();
			WebUtility.Click(dropDownList.get(1));
			closeDropdown.click();

			newSetAgeGroup.click();
			WebUtility.Click(dropDownList.get(1));
			closeDropdown.click();

			newSetLanguage.click();
			WebUtility.Click(dropDownList.get(1));
			closeDropdown.click();

			newSetOthers.click();
			newSetOthers.sendKeys("Automation Test Data II");

			WebUtility.Click(save);

			WebUtility.Click(imageSets.get(2));

			js.executeScript("window.scrollTo(0,0)");

			WebUtility.Wait();
	        cover.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\CoverPNG.png");

	        WebUtility.Wait();
	        appcover.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\AppCoverPNG.png");

	        WebUtility.Wait();
	        list.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\List.jpg");

	        WebUtility.Wait();
	        square.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\Square.jpg");
	        
	        js.executeScript("window.scrollBy(0,600)");

	        Thread.sleep(2000);
	        tvCover.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\TVCover.jpg");

	        WebUtility.Wait();
	        portrait.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\portraitPNG.png");

	        WebUtility.Wait();
	        listClean.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\ListClean.jpg");

			js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
			
			WebUtility.Wait();
	        portraitClean.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\PortraitCleanPNG.png");

	        WebUtility.Wait();
	        telcoSquare.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\TelcoSquare.jpg");

	        WebUtility.Wait();
	        passport.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\Passport.jpg");
	        WebUtility.Wait();
		} catch (Exception e) {
			e.printStackTrace();
		}

		// ---------------------- Inactivate Image Set ------------------------
		try {
			js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
			WebUtility.Wait();
			WebUtility.Click(imageSetActiveButton);
			WebUtility.Wait();
			WebUtility.Click(dropDownList.get(0));
			WebUtility.javascript(yes.get(0));
			WebUtility.Wait();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		//------------------------ Delete Image Set -----------------------------
		try {
			removeSet.get(1).click();
			WebUtility.Wait();
			yes.get(0).click();
			WebUtility.Wait();
		} catch (Exception e) {
			e.printStackTrace();
		}
		String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
		sa.assertTrue(doneButtonActiveColor.contains("255, 255, 255") || doneButtonActiveColor.contains("255,255,255"), "Episode Quick Filing -  Images Mark As Done Assertion Fails");
		WebUtility.Click(doneButtonActive);

		js.executeScript("window.scrollTo(0,0);");
		WebUtility.Wait();
		WebUtility.Click(imageSets.get(0));
		try {
			TestUtil.captureScreenshot("Episode Quick Filing Images Section Default Set","Episode");
		} catch (IOException e) {
			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}

		js.executeScript("window.scrollTo(0,document.body.scrollHeight);");

		try {
			TestUtil.captureScreenshot("Episode Quick Filing Images Section Default Set 2 ","Episode");
		} catch (IOException e) {
			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}

		WebUtility.Click(imageSets.get(1));
		js.executeScript("window.scrollTo(0,0);");
		try {
			TestUtil.captureScreenshot("Episode Quick Filing Images Section Created Set","Episode");
		} catch (IOException e) {
			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}
		js.executeScript("window.scrollTo(0,document.body.scrollHeight);");

		try {
			TestUtil.captureScreenshot("Episode Quick Filing Images Section Created Set 2","Episode");
		} catch (IOException e) {
			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}


	}
	public void quickfillingSeo() throws InterruptedException {
		seoDetails.click();
		WebUtility.Wait();
	
		editSeoTitleTag.click();
		Thread.sleep(1000);
		seoTitleTag.click();
		seoTitleTag.sendKeys(Keys.CONTROL + "a");
		seoTitleTag.sendKeys(Keys.BACK_SPACE);
		Thread.sleep(2000);
		seoTitleTag.sendKeys("Automation Test Data Episode Quick Filing");
		Thread.sleep(1000);
		editSeoTitleTag.click();
		Thread.sleep(1000);

		
		editSeoMetaDescription.click();
		Thread.sleep(1000);
		seoMetaDescription.click();
		seoMetaDescription.sendKeys(Keys.CONTROL + "a");
		seoMetaDescription.sendKeys(Keys.BACK_SPACE);
		Thread.sleep(2000);
		seoMetaDescription.sendKeys("Automation Test Data Episode Quick Filing");
		Thread.sleep(1000);
		editSeoMetaDescription.click();
		Thread.sleep(1000);

		editSeoMetaSynopsis.click();
		Thread.sleep(1000);
		seoMetaSynopsis.click();
		seoMetaSynopsis.sendKeys(Keys.CONTROL + "a");
		seoMetaSynopsis.sendKeys(Keys.BACK_SPACE);
		Thread.sleep(2000);
		seoMetaSynopsis.sendKeys("Automation Test Data Episode Quick Filing");
		Thread.sleep(1000);
		editSeoMetaSynopsis.click();
		Thread.sleep(1000);

		quickeditSeoH1Heading.click();
		Thread.sleep(1000);
		seoH1Heading.click();
		seoH1Heading.sendKeys(Keys.CONTROL + "a");
		seoH1Heading.sendKeys(Keys.BACK_SPACE);
		Thread.sleep(2000);
		seoH1Heading.sendKeys("Automation Test Data Episode Quick Filing");
		Thread.sleep(1000);
		quickeditSeoH1Heading.click();
		Thread.sleep(1000);
		
		quickeditSeoH2Heading.click();
		Thread.sleep(1000);
		seoH2Heading.click();
		seoH2Heading.sendKeys(Keys.CONTROL + "a");
		seoH2Heading.sendKeys(Keys.BACK_SPACE);
		Thread.sleep(2000);
		seoH2Heading.sendKeys("Automation Test Data Episode Quick Filing");
		Thread.sleep(1000);
		quickeditSeoH2Heading.click();
		Thread.sleep(1000);
				
		quickeditSeoVideoSchema.click();
		Thread.sleep(2000);
		quickeditSeoVideoSchema.click();
		Thread.sleep(1000);
		
		quickeditSeoWebSchema.click();
		Thread.sleep(2000);
		quickeditSeoWebSchema.click();
		Thread.sleep(1000);
		
		quickeditSeoBreadcrunSchema.click();
		Thread.sleep(2000);
		quickeditSeoBreadcrunSchema.click();
		Thread.sleep(1000);		
		
		quickeditSeoimageSchema.click();
		Thread.sleep(2000);
		quickeditSeoimageSchema.click();
		Thread.sleep(1000);
		
		quickeditSeoOrganizationSchema.click();
		Thread.sleep(2000);
		quickeditSeoOrganizationSchema.click();
		Thread.sleep(1000);
		doneButtonActive.click();
	}
	
	public void EpisodePublishFlow() throws InterruptedException, Exception{
		WebUtility.Wait();
		//--Verifying Episode QuickFiling Draft Status
		try
		{
			episodeProperties.click();
			WebUtility.element_to_be_visible(contentStatus);
			TestUtil.captureScreenshot("Episode QuickFiling Draft Status","Episode");
			String expectedStatus = "Draft";
			String actualStatus = contentStatus.getText();
			sa.assertEquals(actualStatus,expectedStatus,"Episode QuickFiling Draft Status is Failed");
			Reporter.log("Episode QuickFiling Draft Status is Passed",true);	
		}
		catch (Exception e)
		{
			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}
		
		WebUtility.Click(checklist);
		WebUtility.Wait();
		js.executeScript("window.scrollBy(0,600)");
		scheduledCountry.get(0).click();
		WebUtility.Click(dropDownList.get(0));
		WebUtility.Click(publishContent);
		WebUtility.Click(yes.get(0));
		WebUtility.Click(ok);
     
        js.executeScript("window.scrollTo(0,0);");

        
        //--Verifying Episode QuickFiling Published Status
  		try
  		{
  			episodeProperties.click();
  			WebUtility.element_to_be_visible(contentStatus);
  			TestUtil.captureScreenshot("Episode QuickFiling Published Status","Episode");
  			String expectedStatus = "Published";
  			String actualStatus = contentStatus.getText();
  			sa.assertEquals(actualStatus,expectedStatus,"Episode  QuickFiling Published Status is Failed");
  			Reporter.log("Episode QuickFiling Published Status is Passed",true);	
  		}
  		catch (Exception e){
  			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
  		}
        
        //----------------Republish -----------------------------------------------------------------------------
	       
        try {
	        license.click();
	        WebUtility.Wait();
	       	
	        WebUtility.Click(licenseStatusButton);
	        WebUtility.Click(inactivateLicenseDropdown);
	        WebUtility.Wait();
	        actionProvider.moveToElement(inactivateLicenseReason).click().build().perform();
	        WebUtility.Click(dropDownList.get(0));
	        WebUtility.Click(yes.get(0));
	        WebUtility.Click(doneButtonActive);
       }
       catch(Exception e)
       {
    	   try
    	   {
    	   WebUtility.Click(no);
    	   }
    	   catch(Exception e1) {
    		 e1.printStackTrace();
    	   }
			WebUtility.Click(seoDetails);
			editSeoTitleTag.click();
			Thread.sleep(1000);
			seoTitleTag.click();
			seoTitleTag.sendKeys(Keys.CONTROL + "a");
			seoTitleTag.sendKeys(Keys.BACK_SPACE);
			Thread.sleep(2000);
			seoTitleTag.sendKeys("Automation Test Data Episode Quick Filing Edit");
			Thread.sleep(1000);
			editSeoTitleTag.click();
			Thread.sleep(1000);
			WebUtility.Click(doneButtonActive);
			e.printStackTrace();           
       }
        
        WebUtility.Click(checklist);
        js.executeScript("window.scrollTo(0,document.body.scrollHeight)");
        WebUtility.Click(republishContent);
        WebUtility.Click(yes.get(0));
        //WebUtility.Click(yes.get(1));
        WebUtility.Click(ok);
        js.executeScript("window.scrollTo(0,0)");
		WebUtility.Wait();
		
		//--Verifying TvShow Republished status
		try
		{
				episodeProperties.click();
				WebUtility.element_to_be_visible(contentStatus);
				TestUtil.captureScreenshot("Episode Main Content Republished Status","Episode");
				String expectedStatus = "Published";
				String actualStatus = contentStatus.getText();
				sa.assertEquals(actualStatus,expectedStatus,"Episode Quick Filing Republished Status is Failed");
		}
		catch (Exception e)
		{
				e.printStackTrace();
		}
		
		//----------------------------Unpublish ----------------------------------------------

        
		WebUtility.Click(checklist);
        js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
		WebUtility.Wait();
        WebUtility.Click(unpublishContent);
        WebUtility.Click(unpublishReason.get(2));
        WebUtility.Click(dropDownList.get(0));
    	WebUtility.Click(yes.get(0));
		WebUtility.Click(ok);         
        
        js.executeScript("window.scrollTo(0,0)");
        
        //--Verifying Episode QuickFiling Unpublished Status
        try {
        	episodeProperties.click();
			WebUtility.element_to_be_visible(contentStatus);
			TestUtil.captureScreenshot("Episode QuickFiling Unpublished Status","Episode");
			String expectedStatus = "Unpublished";
			String actualStatus = contentStatus.getText();
			sa.assertEquals(actualStatus,expectedStatus,"Episode QuickFiling Unpublished Status is Failed");
			Reporter.log("Episode QuickFiling Unpublished Status is Passed",true);	
        } catch (IOException e) {
            System.out.println("Screenshot Capture Failed ->" + e.getMessage());
        }
        
        // ------------- Schedule Content -------------------------------------
		WebUtility.Click(checklist);
		js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
		WebUtility.Click(scheduleContent);

		//System.out.println("--------> Current Time = " + currentTime.getAttribute("innerText").substring(20, 28));
		//String currentTimeString = currentTime.getAttribute("innerText").substring(20, 27);
		//js.executeScript("window.scrollTo(0,document.body.scrollHeight);");

		WebUtility.Click(scheduledPublicationTime);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        WebUtility.Click(datePickerMinutes);
        actionProvider.moveToElement(datePickerMinutesClock).click().build().perform();      
        datePicker.sendKeys(Keys.ESCAPE);

		WebUtility.Click(scheduledContentCountry);
		WebUtility.Click(dropDownList.get(0));

		WebUtility.Click(scheduleContentButton);
		WebUtility.Click(yes.get(0));
		WebUtility.Wait();
		WebUtility.Click(ok);

		js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
		WebUtility.Wait();
		js.executeScript("window.scrollTo(0,0);");
		WebUtility.Wait();

		//System.out.println("--------> Scheduled Time = " + scheduledTime.getAttribute("innerText").substring(12, 20));
		//String scheduledTimeString = scheduledTime.getAttribute("innerText").substring(12, 20);
		//js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
		//sa.assertTrue(scheduledTimeString.contains(currentTimeString), "Episode Quick Filing - Current Time and Scheduled Time are not Equal");

       //-------------------Verifying Episode Scheduled Status---------------------------------
         try {
         	episodeProperties.click();
 			WebUtility.Wait();
 			WebUtility.element_to_be_visible(contentStatus);
             TestUtil.captureScreenshot("Episode Quick Filing Scheduled Status","Episode");
 			String expectedStatus = "Scheduled";
 			String actualStatus = contentStatus.getText();
 			sa.assertEquals(actualStatus,expectedStatus,"Episode Quick Filing Scheduled Status is Failed");
         } catch (Exception e) {
             e.printStackTrace();
         }
         
         publishedHistory.click();
         WebUtility.Wait();
         episodesBreadcrumb.click();
	}
	
	public void EpisodeAssertionResults() throws Exception{
		sa.assertAll();
	}
}
