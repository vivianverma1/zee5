package com.zee5.qa.pages;

import java.io.IOException;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;												
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.asserts.SoftAssert;

import com.qa.utilities.WebUtility;
import com.zee5.qa.util.TestUtil;
import com.zee5.qa.base.TestBase;

public class SeasonsPage extends TestBase {
	
	//------------------------------TV Show Titles ---------------------------------------------------------
	
	String tvShowTitle ="The Expanse";
	String tvShowQuickFilingTitle = "The Expanse Quick Filing";
	String tvShowSingleLandingTitle = "The Expanse Single Landing";
	JavascriptExecutor js = (JavascriptExecutor) driver; 
	Actions actionProvider = new Actions(driver);
	String externalID;
	SoftAssert sa = new SoftAssert();
	//---------------------------- TV Show Properties ----------------------------------------------------------------
    @FindBy(xpath = "//div[contains(@class,'auto-tvShow')]")
    WebElement createTvShow;

    @FindBy(xpath = "//button[contains(@class,'auto-leftTab-TVShowProperties')]")
    WebElement tvShowProperties;

   // @FindBy(xpath = "//span[contains(text(),'Title Summary')]")
    @FindBy(xpath = "//button[contains(@class,'auto-leftTab-TitleSummary')]")  
    WebElement titleSummary;

    @FindBy(xpath = "//input[@name='note']")
    WebElement note;

    @FindBy(xpath = "//input[@name=\"title\"]")
    WebElement title;

    @FindBy(xpath = "//div[contains(@id,'auto-shortDescription')]//child::div[contains(@class,'ql-editor')]")
    WebElement shortDesc;
    
    @FindBy(xpath = "//div[contains(@class,'auto-WebDescription')]//child::div[contains(@class,'ql-editor')]")
    WebElement webDesc;
    
    @FindBy(xpath = "//div[contains(@class,'auto-AppDescription')]//child::div[contains(@class,'ql-editor')]")
    WebElement appDesc;
  
    @FindBy(xpath = "//div[contains(@class,'auto-TVDescription')]//child::div[contains(@class,'ql-editor')]")
    WebElement tvDesc;  

    @FindBy(xpath = "//input[@id='subtype']")
    WebElement subType;
    
    @FindBy(xpath = "//input[@id='category']")
    WebElement category;

    @FindBy(xpath = "//input[@id='primaryGenre']")
    WebElement primaryGenre;    

    @FindBy(xpath = "//input[@id='secondaryGenre']")
    WebElement secondaryGenre;
    
    @FindBy(xpath = "//input[@id='thematicGenre']")
    WebElement thematicGenre;

    @FindBy(xpath = "//input[@id='settingGenre']")
    WebElement settingGenre;

    @FindBy(xpath = "//input[@id='rcsCategory']")
    WebElement rcsCategory;

    @FindBy(xpath = "//input[@name='creativeTitle']")
    WebElement creativeTitle;

    @FindBy(xpath = "//input[@name='alternatetitle']")
    WebElement alternateTitle;
    
    @FindBy(xpath = "//input[@name='pageTitle']")
    WebElement pageTitle;

    @FindBy(xpath = "//textarea[@name='pageDescription']")
    WebElement pageDescription;

    @FindBy(name = "titleForSocialShare")
    WebElement titleForSocialShare;

    @FindBy(name = "descriptionForSocialShare")
    WebElement descriptionForSocialShare;
     
    @FindBy(name = "dateZee5Published")
    WebElement zee5ReleaseDate;

    @FindBy(name = "telecastDate")
    WebElement telecastDate;

    @FindBy(xpath = "//div[@class='MuiPaper-root MuiPopover-paper MuiPaper-elevation8 MuiPaper-rounded']")
    WebElement datePicker; 
    
    @FindBy(xpath = "//input[@name='isMultiAudio']")
    WebElement multiAudio;

    @FindBy(xpath = "//input[@id='audioLanguages']")
    WebElement audioLanguage;
       
    @FindBy(xpath = "//input[@id='contentLanguage']")
    WebElement contentLanguage;
    
    @FindBy(xpath = "//input[@id='primaryLanguage']")
    WebElement primaryLanguage;

    @FindBy(xpath = "//input[@id='dubbedLanguageTitle']")
    WebElement dubbedLanguageTitle;

    @FindBy(xpath = "//input[@id='originalLanguage']")
    WebElement originalLanguage;
    
    @FindBy(xpath = "//input[@id='subtitleLanguages']")
    WebElement subtitleLanguages;

    @FindBy(xpath = "//input[@id='contentVersion']")
    WebElement contentVersion;

    @FindBy(xpath = "//input[@id='theme']")
    WebElement theme;

    @FindBy(xpath = "//input[@name='contractId']")
    WebElement contractId;

    @FindBy(xpath = "//input[@id='specialCategory']")
    WebElement specialCategory;

    @FindBy(xpath = "//input[@id='specialCategoryCountry']")
    WebElement specialCategoryCountry;

    @FindBy(xpath = "//input[@name='specialCategoryFrom']")
    WebElement specialCategoryFrom;

    @FindBy(xpath = "//input[@name='specialCategoryTo']")
    WebElement specialCategoryTo;
    
	@FindBy(xpath = "//div[contains(@class,'s-badge create-movie-stage ')]")
	WebElement contentStatus; 

    //-------------------------------------------------TV Show Properties ->Classification -----------------------------------------------------------------------------------------------
    @FindBy(id = "broadcastState")
    WebElement broadcastState;    
    
    @FindBy(id = "rating")
    WebElement rating;

    @FindBy(id = "contentOwner")
    WebElement contentOwner;

    @FindBy(id = "originCountry")
    WebElement originCountry;
   
    @FindBy(id = "contentProduction")
    WebElement contentProduction;    
    
    @FindBy(name = "upcomingPage")
    WebElement upcomingPage;
  
    @FindBy(id = "ageRating")
    WebElement ageRating;
    
    @FindBy(id = "certification")
    WebElement certification;
    
    @FindBy(id = "emotions")
    WebElement emotions;
    
    @FindBy(id = "contentGrade")
    WebElement contentGrade;
    
    @FindBy(id = "era")
    WebElement era;

    @FindBy(id = "targetAge")
    WebElement targetAge;
    
    @FindBy(id = "targetAudiences")
    WebElement targetAudiences;
    
    @FindBy(id = "tags")
    WebElement tags;
    
    @FindBy(name = "digitalKeywords")
    WebElement digitalKeywords;
    
    @FindBy(name = "adaptation")
    WebElement adaptation;
    
    @FindBy(name = "events")
    WebElement events;
    
    @FindBy(id = "productionCompany")
    WebElement productionCompany;
    
    @FindBy(name = "popularityScore")
    WebElement popularityScore;
    
    @FindBy(name = "trivia")
    WebElement trivia;
    
    @FindBy(name = "epgProgramId")
    WebElement epgProgramId;
    
    @FindBy(id = "channel")
    WebElement channel; 
    
    @FindBy(name = "showAirTime")
    WebElement showAirTime;
    
    @FindBy(id = "showAirTimeDays")
    WebElement showAirTimeDays; 


    @FindBy(id = "awardRecipient")
    WebElement awardRecipient;

    @FindBy(id = "awardsCategory")
    WebElement awardsCategory;

    @FindBy(name = "awardsandrecognition")
    WebElement awardHonor;
    
    //---------------------------------------------------------TV Show Properties -> Global Fields ---------------------------------------------------------------

    @FindBy(id="auto-country-0-0")
    WebElement globalCountry;
    
    @FindBy(id="country")
    WebElement globalCountryQuickFilling;
    
    @FindBy(name="title")
    WebElement globalTitle;
    
    @FindBy(id="auto-title-0-0")
    WebElement globalTitleQuickFilling;
        
    @FindBy(id="auto-creativeTitle-0-2")
    WebElement globalCreativeTitle;
    
    @FindBy(id="auto-alternativeTitle-0-3")
    WebElement globalAlternativeTitle;    
    
    @FindBy(id="auto-pageTitle-0-4")
    WebElement globalPageTitle;
    
    @FindBy(id="auto-pageDescription-0-5")
    WebElement globalPageDescription;
    
    @FindBy(id="auto-socialShareTitle-0-6")
    WebElement globalSocialShareTitle;
        
    @FindBy(name="telecastDate")
    WebElement globalTelecastDate;
    
    @FindBy(id="subtitleLanguages")
    WebElement globalSubtitleLanguages;
    
    
    @FindBy(id="auto-socialShareDescription-0-10")
    WebElement globalDescriptionForSocialShare;
    
    @FindBy(xpath = "//div[contains(@id,'auto-shortDescription')]//child::div[contains(@class,'ql-editor')]")
    WebElement globalShortDesc;
    
    @FindBy(xpath = "//div[contains(@class,'auto-WebDescription')]//child::div[contains(@class,'ql-editor')]")    
    WebElement globalWebDesc;
    
    @FindBy(xpath = "//div[contains(@class,'auto-AppDescription')]//child::div[contains(@class,'ql-editor')]")
    WebElement globalAppDesc;
  
    @FindBy(xpath = "//div[contains(@class,'auto-TVDescription')]//child::div[contains(@class,'ql-editor')]")
    WebElement globalTVDesc;
   
    @FindBy(name="upcomingPageText")
    WebElement globalUpcomingPage;
    
  
  
    //-------------------------------------------- Cast and Crew -----------------------------------------------------------
    @FindBy(xpath = "//button[contains(@class,'auto-leftTab-Cast&Crew')]")
    WebElement castCrew;

    @FindBy(xpath = "//input[contains(@id,'auto-castName')]")
    WebElement actor;

    @FindBy(xpath = "//input[contains(@id,'auto-character')]")
    WebElement character;
    
    @FindBy(xpath = "//div[contains(@class,'auto-ActorChange')]//descendant::input")
    WebElement actorChange;      

    @FindBy(xpath = "//div[contains(@class,'auto-Performer')]//descendant::input")
    WebElement performer;

    @FindBy(xpath = "//div[contains(@class,'auto-Host/Anchorman')]//descendant::input")
    WebElement host;

    @FindBy(xpath = "//div[contains(@class,'auto-Singer')]//descendant::input")
    WebElement singer;

    @FindBy(xpath = "//div[contains(@class,'auto-Lyricist')]//descendant::input")
    WebElement lyricst;

    @FindBy(xpath = "//div[contains(@class,'auto-Director')]//descendant::input")
    WebElement director;

    @FindBy(xpath = "//div[contains(@class,'auto-Cinematography/DOP')]//descendant::input")
    WebElement cinematography;

    @FindBy(xpath = "//div[contains(@class,'auto-Producer')]//descendant::input")
    WebElement producer;

    @FindBy(xpath = "//div[contains(@class,'auto-ExecutiveProducer')]//descendant::input")
    WebElement executiveProducer;

    @FindBy(xpath = "//div[contains(@class,'auto-MusicDirector')]//descendant::input")
    WebElement musicDirector;

    @FindBy(xpath = "//div[contains(@class,'auto-Choreographer')]//descendant::input")
    WebElement choreographer;

    @FindBy(xpath = "//div[contains(@class,'auto-TitleThemeMusic')]//descendant::input")
    WebElement titleThemeMusic;

    @FindBy(xpath = "//div[contains(@class,'auto-BackgroundScore')]//descendant::input")
    WebElement backgroundScore;

    @FindBy(xpath = "//div[contains(@class,'auto-StoryWriter')]//descendant::input")
    WebElement storyWriter;

    @FindBy(xpath = "//div[contains(@class,'auto-ScreenPlay')]//descendant::input")
    WebElement screenPlay;

    @FindBy(xpath = "//div[contains(@class,'auto-DialogueWriter')]//descendant::input")
    WebElement dialogueWriter;

    @FindBy(xpath = "//div[contains(@class,'auto-FilmEditing')]//descendant::input")
    WebElement filmEditing;

    @FindBy(xpath = "//div[contains(@class,'auto-Casting')]//descendant::input")
    WebElement casting;

    @FindBy(xpath = "//div[contains(@class,'auto-ProductionDesign')]//descendant::input")
    WebElement productionDesign;

    @FindBy(xpath = "//div[contains(@class,'auto-ArtDirection')]//descendant::input")
    WebElement artDirection;

    @FindBy(xpath = "//div[contains(@class,'auto-SetDecoration')]//descendant::input")
    WebElement setDecoration;

    @FindBy(xpath = "//div[contains(@class,'auto-CostumeDesign')]//descendant::input")
    WebElement costumeDesign;

    @FindBy(xpath = "//div[contains(@class,'auto-ProductionCo')]//descendant::input")
    WebElement productionCo;

    @FindBy(xpath = "//div[contains(@class,'auto-Presenter')]//descendant::input")
    WebElement presenter;

    @FindBy(xpath = "//div[contains(@class,'auto-Guest')]//descendant::input")
    WebElement guest;

    @FindBy(xpath = "//div[contains(@class,'auto-Participant')]//descendant::input")
    WebElement participant;

    @FindBy(xpath = "//div[contains(@class,'auto-Judges')]//descendant::input")
    WebElement judges;

    @FindBy(xpath = "//div[contains(@class,'auto-Narrator')]//descendant::input")
    WebElement narrator;

    @FindBy(xpath = "//div[contains(@class,'auto-Sponsor')]//descendant::input")
    WebElement sponsor;

    @FindBy(xpath = "//div[contains(@class,'auto-Graphics')]//descendant::input")
    WebElement graphics;

    @FindBy(xpath = "//div[contains(@class,'auto-Vocalist')]//descendant::input")
    WebElement vocalist;
    
    @FindBy(id = "sponsors")
    WebElement globalSponsors;
    
    @FindBy(id = "country")
    WebElement globalGroupCountry;


    //--------------------------------------License ------------------------------------------------------	
    @FindBy(xpath = "//button[contains(@class,'auto-leftTab-LicenseModule')]")
    WebElement license;

    @FindBy(xpath = "//div[contains(text(),'Create License')]")
    WebElement createLicense;

    @FindBy(xpath = "//input[@name='fromDate']")
    WebElement licenseFromDate;

	@FindBy(xpath = "//input[@name='toDate']")
	WebElement licenseToDate;

	@FindBy(xpath = "//input[contains(@id,'auto-country')]")
    WebElement licenseCountry;

	@FindBy(xpath = "//input[contains(@id,'auto-business')]")   
    WebElement licenseBusinessType;
	
	@FindBy(xpath = "//input[contains(@id,'auto-platform')]")   
    WebElement licensePlatform;
	
	@FindBy(xpath = "//input[contains(@id,'auto-tvodTier')]")
	WebElement licenseTVOD;

	@FindBy(xpath = "//span[contains(text(),\"SAVE\")]")
	WebElement saveLicense;

    @FindBy(xpath = "//div[@role='tooltip']")
    WebElement inactivateLicenseDropdown;

    @FindBy(xpath = "//div[@class='ml-10 status-dropdown val']")
    WebElement licenseStatusButton;

    @FindBy(id = "auto-reason-0-0")
    WebElement inactivateLicenseReason;

    @FindBy(xpath = "//input[@name='manual' and @value='2']")
    WebElement useTemplateRadioButton;

    @FindBy(xpath = "//input[@type='text']")
    WebElement selectTemplate;

    @FindBy(xpath = "//div[contains(@class,'default-edit-btn')]")
    WebElement editLicense;

    @FindBy(id = "auto-country-0-0")
    WebElement editLicenseCountry;

    @FindBy(xpath = "//button[contains(@class,'auto-button-short-btn')]")
    WebElement expiredLicense;
    
    @FindBy(xpath = "//input[@name='setName']")
    WebElement licenseSetName;

    @FindBy(className = "item")
    WebElement expiredLicenseBack;

    @FindBy(xpath = "//button[contains(@class,'auto-button-filter-btn')]")
    WebElement licenseFilters;

    @FindBy(id = "auto-businessType-0-2")
    WebElement licenseFilterBusinessType;

    @FindBy(id = "auto-billingType-0-3")
    WebElement licenseFilterBillingType;

    @FindBy(id = "auto-platform-0-4")
    WebElement licenseFilterPlatform;

    @FindBy(xpath = "//input[@name='status' and @value = '1']")
    WebElement licenseFilterActiveStatus;
    
    @FindBy(xpath = "//input[@name='status' and @value = '0']")
    WebElement licenseFilterInActiveStatus;
    
	@FindBy(xpath = "//div[contains(@class,'delete')]")
	List<WebElement> licenseTemplateDeleteSets;

    //--------------------------------------------Images -----------------------------------------------------------------
    @FindBy(xpath = "//button[contains(@class,'auto-leftTab-Images')]")
    WebElement images;

    @FindBy(name = "cover")
    WebElement cover;

    @FindBy(name = "appcover")
    WebElement appcover;

    @FindBy(name = "list")
    WebElement list;

    @FindBy(name = "square")
    WebElement square;

    @FindBy(name = "tvcover")
    WebElement tvCover;

    @FindBy(name = "portrait")
    WebElement portrait;

    @FindBy(name = "listclean")
    WebElement listClean;

    @FindBy(name = "portraitclean")
    WebElement portraitClean;

    @FindBy(name = "telcosquare")
    WebElement telcoSquare;

    @FindBy(name = "passport")
    WebElement passport;

    @FindBy(xpath = "//button[contains(@class,'auto-upload-btn')]")
    WebElement imagesCreateNewSet;
    
    @FindBy(name = "setName")
    WebElement newSetName;

    @FindBy(id="auto-GroupCountry-0-1")
    WebElement newSetCountry;
    
    @FindBy(id = "auto-Platform-0-2")
    WebElement newSetPlatform;
    
    @FindBy(id = "auto-gender-0-0")
    WebElement newSetGender;
    
    @FindBy(id="auto-genre-0-1")
    WebElement newSetGenre;
    
    @FindBy(id = "auto-ageGroup-0-2")
    WebElement newSetAgeGroup;
    
    @FindBy(id = "auto-language-0-3")
    WebElement newSetLanguage;
    
    @FindBy(name="others")
    WebElement newSetOthers;
    
    @FindBy(xpath = "//div[contains(@class,'question-list flex')]")
    List<WebElement> imageSets;
    
    @FindBy(xpath = "//span[@class='remove']")
    List<WebElement> removeSet;
    
    @FindBy(xpath = "(//button[contains(@class,'Active')])[1]")
    WebElement imageSetActiveButton;
    //----------------------------------------- SEO Details ---------------------------------------------------------------

    @FindBy(xpath = "//button[contains(@class,'auto-leftTab-SEODetails')]")
    WebElement seoDetails;

    @FindBy(name = "titleTag")
    WebElement seoTitleTag;

    @FindBy(name = "metaDescription")
    WebElement seoMetaDescription;

    @FindBy(name = "metaSynopsis")
    WebElement seoMetaSynopsis;

    @FindBy(id = "redirectionType")
    WebElement seoRedirectionType;

    @FindBy(name = "redirectionLink")
    WebElement seoRedirectionLink;

    @FindBy(name = "noIndexNoFollow")
    WebElement seoNoIndexNoFollow;

    @FindBy(name = "h1Heading")
    WebElement seoH1Heading;

    @FindBy(name = "h2Heading")
    WebElement seoH2Heading;

    @FindBy(name = "h3Heading")
    WebElement seoH3Heading;

    @FindBy(name = "h4Heading")
    WebElement seoH4Heading;

    @FindBy(name = "h5Heading")
    WebElement seoH5Heading;

    @FindBy(name = "h6Heading")
    WebElement seoH6Heading;

    @FindBy(name = "robotsMetaNoIndex")
    List < WebElement > seoRobotsMetaIndex;

    @FindBy(name = "robotsMetaImageIndex")
    WebElement seoRobotsMetaImageIndex;

    @FindBy(name = "robotsMetaImageNoIndex")
    WebElement seoRobotsMetaImageNoIndex;

    @FindBy(name = "breadcrumbTitle")
    WebElement seoBreadcrumbTitle;

    @FindBy(name = "internalLinkBuilding")
    WebElement seoInternalLinkBuilding;

    @FindBy(xpath = "//input[@type=\"checkbox\"]")
    List < WebElement > seoCheckboxes;

    @FindBy(xpath = "//input[@name='videoObjectSchema']")
    WebElement seoVideoObjectSchema;
    
    @FindBy(xpath = "//input[@name='websiteSchema']")
    WebElement seoWebsiteSchema;
    
    @FindBy(xpath = "//input[@name='breadcrumListSchema']")
    WebElement seoBreadcrumbListSchema;
    
    @FindBy(xpath = "//input[@name='imageObjectSchema']")
    WebElement seoImageObjectSchema;
    
    @FindBy(xpath = "//input[@name='organizationSchema']")
    WebElement seoOrganizationSchema;
    
    // ---------------------------- Seo Details Global Fields -----------------------------------------------------------------------------------
    
    @FindBy(xpath="//div[contains(@class,'auto-Country/Group')]//input")
    WebElement globalSeoCountry;
    
    //-------------------------- MAP Content -----------------------------------------------------------------------------------------------------

    @FindBy(xpath = "//button[contains(@class,'auto-leftTab-MapContent')]")
    WebElement mapContent;

    @FindBy(xpath = "//div[contains(text(),\"Add Content\")]")
    WebElement addContent;

    @FindBy(xpath = "//input[@type='checkbox']")
    WebElement checkbox;

    //@FindBy(className = "btn-create-user  ")
 

    @FindBy(xpath = "//div[@class='mark-done mark-fill-active']")
    WebElement mapMarkAsDone;

    @FindBy(name = "searchVal")
    List < WebElement > searchMapContent;

    //-----------------------------------------Quick Links ----------------------------------------------------

    @FindBy(xpath = "//li[contains(text(),'Related Content')]")
    WebElement relatedContent;

    @FindBy(name = "searchVal")
    WebElement relatedContentSearch;

    @FindBy(xpath = "//span[contains(text(),'Assign Movies')]")
    WebElement relatedContentAssignMovies;
    
    @FindBy(xpath = "//div[contains(text(),'Add Movies')]")
    WebElement relatedContentAddMovies;
    
    @FindBy(xpath = "//span[contains(text(),'Assign TV Shows')]")
    WebElement relatedContentAssignTvShows;
    
    @FindBy(xpath = "//div[contains(text(),'Add TV Shows')]")
    WebElement relatedContentAddTvShows;

    @FindBy(xpath = "//span[contains(text(),\"Assign Videos\")]")
    WebElement relatedContentAssignVideos;

    @FindBy(xpath = "//div[contains(text(),'Add Videos')]")
    WebElement relatedContentAddVideos;
    
    @FindBy(xpath = "//button[contains(.,'Assign Seasons')]")
    WebElement relatedContentAssignSeasons;

    @FindBy(xpath = "//div[contains(text(),'Add Seasons')]")
    WebElement relatedContentAddSeasons;

    @FindBy(xpath = "//button[contains(.,'Assign Episodes')]")
    WebElement relatedContentAssignEpisodes;

    @FindBy(xpath = "//div[contains(text(),'Add Episodes')]")
    WebElement relatedContentAddEpisodes;
      
    @FindBy(xpath = "//a[contains(text(),'Update Season')]")
    WebElement updateSeasonBreadcrumb;

    @FindBy(id = "assignedContent")
    WebElement assignRelatedContent;
      
    @FindBy(xpath = "(//span[contains(@class,'auto-delete')])[1]")
    WebElement deleteRelatedContent;

    @FindBy(xpath = "//li[contains(text(),'Published History')]")
    WebElement publishedHistory;
    
    //----------------------------------- CheckList---------------------------------------------------

    @FindBy(xpath = "//button[contains(@class,'auto-leftTab-Checklist')]")
    WebElement checklist;

    @FindBy(xpath = "//div[contains(@class,'auto-countryGroup')]//descendant::input")
    WebElement checklistCountry;

    @FindBy(xpath = "//span[contains(text(),'Publish Content')]")
    WebElement publishContent;

    @FindBy(xpath = "//span[contains(text(),'Republish Content')]")
    WebElement republishContent;

    @FindBy(xpath = "//span[contains(text(),'Unpublish Content')]//ancestor::button")
    WebElement unpublishContent;

    @FindBy(xpath = "//span[@class='edit tooltip-sec hand-cursor']")
    WebElement restoreButton;

    @FindBy(id = "archive-icon_svg__Rectangle_273")
    List < WebElement > archiveButton;

    @FindBy(xpath = "//div[contains(@class,'Schedule-box')]//div[contains(@class,'add-btn create-btn')]")
    WebElement scheduleContent;

    @FindBy(name = "scheduledPublicationTime")
    WebElement scheduledPublicationTime;

    @FindBy(xpath = "//div[contains(@class,'auto-countryGroup')]//descendant::input")
    List < WebElement > scheduledCountry;

    @FindBy(xpath = "//div[contains(@class,'Schedule-box m-b-20 ')]//div[contains(@class,'auto-countryGroup')]//input")
    WebElement scheduledContentCountry;
    
    @FindBy(xpath = "//span[contains(text(),'Schedule Content')]")
    WebElement scheduleContentButton;
   
    @FindBy(xpath = "(//label[contains(text(),'Country')]//following::input)[3]")
    WebElement unpublishReason;
    
	@FindBy(xpath = "//button[contains(@class,'delete')]")
	WebElement deleteSchedule;
	
	@FindBy(xpath = "(//div[contains(@class,'val')])[3]")
	WebElement scheduledTime;
	
	@FindBy(xpath = "(//div[contains(@class,'icon-w-text icon-w-14 m-b-10')])[1]")
	WebElement currentTime;	
	
	@FindBy(xpath = "(//button[contains(@class,'MuiPickersToolbarButton-toolbarBtn')])[4]")
	WebElement datePickerMinutes;		
	
	@FindBy(xpath = "//span[contains(@class,'MuiTypography-root MuiPickersClockNumber-clockNumber') and contains(text(),'05')]")
	WebElement datePickerMinutesClock;	
    // ------------------------------Sort And Filter -------------------------------------------------------

    @FindBy(xpath = "//span[contains(text(),'Sort')]")
    WebElement sortVideo;

    @FindBy(xpath = "//input[@value='asc']")
    WebElement ascOrderVideo;

    @FindBy(xpath = "//input[@value='desc']")
    WebElement descOrderVideo;

    @FindBy(xpath = "//span[contains(text(),'Apply Sort')]")
    WebElement applySortVideo;

    @FindBy(xpath = "//span[contains(text(),'Clear')]")
    WebElement clearFilter;

    @FindBy(xpath = "//span[contains(text(),'Filters')]")
    WebElement filters;

    @FindBy(xpath = "//div[contains(text(),'Draft')]")
    WebElement filtersDraftStatus;

    @FindBy(xpath = "//div[contains(text(),'Changed')]")
    WebElement filtersChangedStatus;

    @FindBy(xpath = "//div[contains(text(),'Published')]")
    WebElement filtersPublishedStatus;

    @FindBy(xpath = "//div[contains(text(),'Unpublished')]")
    WebElement filtersUnpublishedStatus;

    @FindBy(xpath = "//div[contains(text(),'Need Work')]")
    WebElement filtersNeedWorkStatus;

    @FindBy(xpath = "//div[contains(text(),'Scheduled')]")
    WebElement filtersScheduledStatus;

    @FindBy(xpath = "//div[contains(text(),'Submitted to Review')]")
    WebElement filtersSubmittedToReviewStatus;

    @FindBy(xpath = "//div[contains(text(),'Archived')]")
    WebElement filtersArchivedStatus;

    @FindBy(xpath = "//input[@type='date']")
    List < WebElement > filtersStartDate;

    @FindBy(xpath = "//input[@type='date']")
    List < WebElement > filtersEndDate;

    @FindBy(xpath = "//input[@id='primaryGenre']")
    WebElement filtersPrimaryGenre;

    @FindBy(xpath = "//input[@id='secondaryGenre']")
    WebElement filtersSecondaryGenre;

    @FindBy(xpath = "//input[@id='thematicGenre']")
    WebElement filtersThematicGenre;

    @FindBy(xpath = "//input[@id='settingGenre']")
    WebElement filtersSettingGenre;

    @FindBy(xpath = "//input[@id='VideoActivity']")
    WebElement filtersActivity;

    @FindBy(xpath = "//input[@id='VideoStyle']")
    WebElement filtersStyle;

    @FindBy(xpath = "//input[@id='actor']")
    WebElement filtersActor;

    @FindBy(xpath = "//input[@id='subType']")
    WebElement filtersSubType;

    @FindBy(xpath = "//input[@id='contentCategory']")
    WebElement filtersContentCategory;

    @FindBy(xpath = "//input[@id='rcsCategory']")
    WebElement filtersRcsCategory;

    @FindBy(xpath = "//input[@id='theme']")
    WebElement filtersTheme;

    @FindBy(xpath = "//input[@id='targetAudience']")
    WebElement filtersTargetAudience;

    @FindBy(xpath = "//input[@id='licenseGroupCountries']")
    WebElement filtersLicenseGroup;

    @FindBy(xpath = "//input[@id='ageRating']")
    WebElement filtersAgeRating;

    @FindBy(xpath = "//input[@id='businessType']")
    WebElement filtersBusinessType;

    @FindBy(xpath = "//input[@name='externalId']")
    WebElement filtersExternalID;

    @FindBy(xpath = "//input[@id='contentRating']")
    WebElement filtersContentRating;

    @FindBy(xpath = "//input[@id='contentOwner']")
    WebElement filtersContentOwner;

    @FindBy(xpath = "//input[@id='audioLanguage']")
    WebElement filtersAudioLanguage;

    @FindBy(xpath = "//input[@id='originalLanguage']")
    WebElement filtersOriginalLanguage;

    @FindBy(xpath = "//input[@id='tags']")
    WebElement filtersTags;

    @FindBy(xpath = "//input[@id='translationLanguage']")
    WebElement filtersTranslationLanguage;

    @FindBy(xpath = "//input[@id='translationStatus']")
    WebElement filtersTranslationLanguageStatus;

    @FindBy(xpath = "//input[@id='moodEmotion']")
    WebElement filtersMood;

    @FindBy(xpath = "//span[contains(text(),'Apply Filter')]")
    WebElement applyFilter;

    
    //------------------------------------------Tranlations ---------------------------------------------------------------
  
    @FindBy(className =  "auto-quickLinks-Translations")
    WebElement translations;
    
    @FindBy(name = "upcomingPageText")
    WebElement translationsUpcomingPage;
    
    @FindBy(xpath = "//input[@name='alternativeTitle']")
    WebElement translationsAlternativeTitle;
    
    @FindBy(name = "socialShareTitle")
    WebElement translationsTitleForSocialShare;    
    
    @FindBy(name = "socialShareDescription")
    WebElement translationsDescriptionForSocialShare;
    
    
    //------------------------------------------ Quick Filling --------------------------------------------------------
    @FindBy(xpath = "//span[contains(text(),'Quick Filing')]")
    WebElement quickFilling;
    
    @FindBy(xpath = "//li[contains(text(),'Create Quick Filing')]")
    WebElement createQuickFiling;
    
    @FindBy(xpath = "//li[contains(text(),'Single Landing Page')]")
    WebElement createSingleLanding;
    
    
    // --------------------------------------------- Seasons Properties---------------------------------------------------------------
    @FindBy(className =  "auto-quickLinks-Seasons")
    WebElement seasons;
    
    @FindBy(xpath = "//button[contains(text(),'Create Season')]")
    WebElement createSeason;
    
    @FindBy(xpath = "//button[contains(.,'Season Properties')]")
    WebElement seasonProperties;
    
    @FindBy(xpath = "//input[@name='selectJourney' and @value = 1]")
    WebElement createSeasonMainSeason;
     
    @FindBy(xpath = "//input[@name='selectJourney' and @value = 2]")
    WebElement createSeasonQuickFiling;
    
    @FindBy(xpath = "//input[@name='selectJourney' and @value = 3]")
    WebElement createSeasonSingleLanding;
 
    @FindBy(name = "index")
    WebElement index;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-0')]")
    WebElement editNote;
  
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-1')]")
    WebElement editTitle;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-2')]")
    WebElement editShortDesc;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-3')]")
    WebElement editWebDesc;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-4')]")
    WebElement editAppDesc;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-5')]")
    WebElement editTvDesc;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-6')]")
    WebElement editIndex;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-7')]")
    WebElement editCategory;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-8')]")
    WebElement editPrimaryGenre;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-9')]")
    WebElement editSecondaryGenre;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-10')]")
    WebElement editThematicGenre;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-11')]")
    WebElement editSettingGenre;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-12')]")
    WebElement editRcsCategory;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-13')]")
    WebElement editPageTitle;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-14')]")
    WebElement editPageDescription;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-15')]")
    WebElement editTitleForSocialShare;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-16')]")
    WebElement editDescriptionForSocialShare;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-17')]")
    WebElement editZeeReleaseDate;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-18')]")
    WebElement editOriginalTelecastDate;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-19')]")
    WebElement editMultiAudio;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-20')]")
    WebElement editAudioLanguage;
    
    @FindBy(xpath = "(//span[@class='edit-save-b auto-edit-save-b-0'])[2]")
    WebElement editSpecialCategory;
  
    @FindBy(xpath = "(//span[@class='edit-save-b auto-edit-save-b-1'])[2]")
    WebElement editSpecialCategoryCountry;
    
    @FindBy(xpath = "(//span[@class='edit-save-b auto-edit-save-b-2'])[2]")
    WebElement editSpecialFromTimeline;
    
    @FindBy(xpath = "(//span[@class='edit-save-b auto-edit-save-b-3'])[2]")
    WebElement editSpecialToTimeline;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-0')]")
    WebElement editBroadcastState;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-1')]")
    WebElement editUpcomingPage;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-2')]")
    WebElement editAgeRating;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-3')]")
    WebElement editCertification;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-0']")
    List<WebElement> editAwardRecepient;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-1']")
    List<WebElement> editAwardCategory;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-2']")
    List<WebElement> editAwardHonor;
    
    
    //------------------------------------------Season Cast/Crew ---------------------------------------------------------
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-0']")
    WebElement editActor;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-1']")
    WebElement editCharacter;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-0']")
    List<WebElement> editActorChange;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-1']")
    List<WebElement> editPerformer;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-2']")
    WebElement editHost;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-3']")
    WebElement editSinger;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-4']")
    WebElement editLyricst;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-5']")
    WebElement editDirector;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-6']")
    WebElement editCinematography;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-7']")
    WebElement editProducer;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-8']")
    WebElement editExecutiveProducer;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-9']")
    WebElement editMusicDirector;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-10']")
    WebElement editChoreographer;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-11']")
    WebElement editTitleThemeMusic;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-12']")
    WebElement editBackgroundScore;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-13']")
    WebElement editStoryWriter;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-14']")
    WebElement editScreenPlay;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-15']")
    WebElement editDialogueWriter;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-16']")
    WebElement editFilmEditing;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-17']")
    WebElement editCasting;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-18']")
    WebElement editProductionDesign;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-19']")
    WebElement editArtDirection;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-20']")
    WebElement editSetDecoration;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-21']")
    WebElement editCostumeDesign;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-22']")
    WebElement editProductionCo;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-23']")
    WebElement editPresenter;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-24']")
    WebElement editGuest;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-25']")
    WebElement editParticipant;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-26']")
    WebElement editJudges;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-27']")
    WebElement editNarrator;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-28']")
    WebElement editSponsors;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-29']")
    WebElement editGraphics;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-30']")
    WebElement editVocalist;

    @FindBy(xpath = "//div[contains(@class,'remove-btn')]")
    WebElement removeActor;
    
    //------------------------------------------Season SEO Details -------------------------------------------------------

    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-1']")
    WebElement editSeoTitleTag;  
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-2']")
    WebElement editSeoMetaDescription;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-3']")
    WebElement editSeoMetaSynopsis;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-4']")
    WebElement editSeoRedirectionType;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-5']")
    WebElement editSeoRedirectionLink;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-6']")
    WebElement editSeoNoIndex;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-7']")
    WebElement editSeoH1Heading;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-8']")
    WebElement editSeoH2Heading;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-9']")
    WebElement editSeoH3Heading;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-10']")
    WebElement editSeoH4Heading;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-11']")
    WebElement editSeoH5Heading;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-12']")
    WebElement editSeoH6Heading;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-13']")
    WebElement editSeoRobotMetaIndex;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-14']")
    WebElement editSeoRobotMetaNoIndex;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-15']")
    WebElement editSeoRobotMetaImageIndex;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-16']")
    WebElement editSeoRobotMetaImageNoIndex;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-17']")
    WebElement editSeoCanonicalURL;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-18']")
    WebElement editSeoBreadcrumbTitle;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-19']")
    WebElement editSeoInternalLinkBuilding;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-20']")
    WebElement editSeoVideoObjectSchema;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-21']")
    WebElement editSeoWebsiteSchema;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-22']")
    WebElement editSeoBreadcrumbListSchema;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-23']")
    WebElement editSeoImageObjectSchema;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-24']")
    WebElement editSeoOrganizationSchema;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-25']")
    WebElement editSeoImageGallerySchema;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-26']")
    WebElement editSeoLinkToStories;
    
    
    //------------------------------------------Tranlations ---------------------------------------------------------------
    
    @FindBy(xpath = "//div[contains(@id,'auto-Short Description')]//child::div[contains(@class,'ql-editor')]")
    WebElement translationsShortDesc;

    @FindBy(xpath = "//div[contains(@id,'auto-Web Description')]//child::div[contains(@class,'ql-editor')]")
    WebElement translationsWebDesc;

    @FindBy(xpath = "//div[contains(@id,'auto-App Description')]//child::div[contains(@class,'ql-editor')]")
    WebElement translationsAppDesc;

    @FindBy(xpath = "//div[contains(@id,'auto-TV Description')]//child::div[contains(@class,'ql-editor')]")
    WebElement translationsTVDesc;

    @FindBy(xpath = "//input[contains(@id,'auto-Actor')]")
    WebElement translationsActor;

    @FindBy(xpath = "//input[contains(@id,'auto-Character')]")
    WebElement translationsCharacter;
    
    @FindBy(xpath = "//span[contains(@class,'editSaveButton')]")
    List < WebElement > editTranslation;
    
    @FindBy(xpath = "//span[contains(text(),'Save')]")
    WebElement saveTranslation;
    
    @FindBy(xpath = "(//span[contains(@class,'editSaveButton')])[1]")
     WebElement  editTranslationActor;
    
    @FindBy(xpath = "(//span[contains(@class,'editSaveButton')])[2]")
    WebElement  editTranslationCharacter;
    
    //----------------------------------------------Season Quick Filing -----------------------------------------------------------
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-0')]")
    WebElement quickeditTitle;
  
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-1')]")
    WebElement quickeditShortDesc;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-2')]")
    WebElement quickeditIndex;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-3')]")
    WebElement quickeditZee5ReleaseDate;
        
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-4')]")
    WebElement editQuickFillingOriginalReleaseDate;

    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-5')]")
    WebElement quickFilingEditMultiAudio;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-6')]")
    WebElement quickeditAudio;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-0')]")
    WebElement editQuickFillingCertification;
    
    @FindBy(xpath="//button[contains(@class,'auto-dropdown-btn-link-dropdown')]")
    WebElement threedot;
    
    @FindBy(xpath="//ul[@id=\"menu-list-grow\"]/li[2]")
    WebElement editImage;
    
    @FindBy(xpath="//input[@name='List']")
    WebElement editQuickFilingImage;
    
    @FindBy(xpath="//span[contains(text(),'Save')]")
    WebElement SaveImage;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-4']")
    WebElement quickeditSeoH1Heading;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-5']")
    WebElement quickeditSeoH2Heading;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-6']")
    WebElement quickeditSeoVideoSchema;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-7']")
    WebElement quickeditSeoWebSchema;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-8']")
    WebElement quickeditSeoBreadcrunSchema;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-9']")
    WebElement quickeditSeoimageSchema;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-10']")
    WebElement quickeditSeoOrganizationSchema;
    
    //---------------------------------------------- Season Single Landing ------------------------------------------------------
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-0']")
    WebElement editSingleLandingTitle;  
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-1']")
    WebElement editSingleLandingShortDesc;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-2']")
    WebElement editSingleLandingIndex;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-3']")
    WebElement editSingleLandingZee5ReleaseDate;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-4']")
    WebElement editSingleLandingTelecastDate;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-5']")
    WebElement editSingleLandingMultiAudio;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-6']")
    WebElement editSingleLandingAudioLanguage;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-0']")
    WebElement editSingleLandingBroadcastState;  
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-1']")
    WebElement editSingleLandingUpcomingPageText;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-0']")
    WebElement editSingleLandingActor;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-1']")
    WebElement editSingleLandingCharacter;
    
    @FindBy(xpath = "(//span[@class='edit-save-b auto-edit-save-b-0'])[2]")
    WebElement editSingleLandingDirector;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-1']")
    WebElement editSingleLandingSeoTitleTag;  
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-2']")
    WebElement editSingleLandingSeoMetaDescription;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-3']")
    WebElement editSingleLandingSeoMetaSynopsis;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-4']")
    WebElement editSingleLandingH1Heading;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-5']")
    WebElement editSingleLandingH2Heading;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-6']")
    WebElement editSingleLandingVideoObjectSchema;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-7']")
    WebElement editSingleLandingWebsiteSchema;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-8']")
    WebElement editSingleLandingBreadcrumbListSchema;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-9']")
    WebElement editSingleLandingImageObjectSchema;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-10']")
    WebElement editSingleLandingOrganizationSchema;
    
    
    //-------------------------------------------------- Ordering -------------------------------------------------------
    @FindBy(xpath = "//div[@class='drop-icon flex align-items-center justify-content-center auto-drop-icon']")  
    List<WebElement> seasonListingOrder;
 
    @FindBy(xpath = "//span[contains(text(),'Set Ordering')]")  
    WebElement setOrdering;
  
    @FindBy(xpath = "//button[contains(text(),'Create New Set')]")  
    WebElement createNewSet;
    
    @FindBy(xpath = "//input[@name='setName']")  
    WebElement setName;
  
    @FindBy(xpath = "//input[@id='country']")  
    WebElement setCountry;
   
     @FindBy(xpath = "//div[contains(@data-rbd-draggable-id,'draggable')]")
     List<WebElement> seasonCard;
     
     @FindBy(xpath = " //div[contains(@class,'auto-question-block')]")
     WebElement expandSetOrder;  						   
  
     //------------------------------------------Listing Page ---------------------------------------
     
     @FindBy(xpath = "//a[contains(@class,'auto-breadCrum-TVShow')]")
     WebElement tvShowBreadcrumb;
     
     @FindBy(xpath = "//button[contains(@class,'auto-dropdown-btn-TVShow')]")
     WebElement tvShowSearchType;
     
     @FindBy(xpath = "//button[contains(@class,'auto-dropdown-btn-Seasons')]")
     WebElement seasonsSearchType;
     
     @FindBy(xpath = "//button[contains(@class,'auto-dropdown-btn-Episodes')]")
     WebElement episodesSearchType;
   
     @FindBy(xpath = "//ul[@id='menu-list-grow']/li[1]")
     WebElement searchTVShow;
     
     @FindBy(xpath = "//ul[@id='menu-list-grow']/li[2]")
     WebElement searchSeasons;
     
     @FindBy(xpath = "//ul[@id='menu-list-grow']/li[3]")
     WebElement searchEpisodes;
   
     @FindBy(xpath = "(//div[@class = 'license-badge'])[1]")
     WebElement listingExpiringLicense;
  
     @FindBy(xpath = "(//div[contains(@class,'auto-mov-link-btn')])[1]")
     WebElement listingLinkTVShows;
     
     @FindBy(xpath = "(//a)[3]")
     WebElement listingLicenseCountries;
     
     @FindBy(xpath = "//span[contains(text(),'Close')]")
     WebElement listingCloseLicenseCountries;  
     
     @FindBy(xpath = "(//span[@class='pub-history'])[1]")
     WebElement listingPublishedHistory;    
     
     @FindBy(xpath = "//div[@class='side-close-btn']")
     WebElement listingClosePublishedHistory;
     
     @FindBy(xpath = "(//span[@class = 'val'])[1]")
     WebElement listingTvShowLink;
     
     @FindBy(xpath = "(//span[@class = 'val'])[2]")
     WebElement listingEpisodeLink;
     
     @FindBy(xpath = " (//div[@data-test='tvshow-child-arrow'])[1]")
     WebElement listingSeasonsArrow;
     
     @FindBy(xpath = "//li[@class='auto-leftTab-All']")
     WebElement listingAllStatus;
     	
     @FindBy(xpath = "//li[@class='auto-leftTab-Draft']")
     WebElement listingDraftStatus;
     
     @FindBy(xpath = "//li[@class='auto-leftTab-Changed']")
     WebElement listingChangedStatus;
     
     @FindBy(xpath = "//li[@class='auto-leftTab-Published']")
     WebElement listingPublishedStatus;
     
     @FindBy(xpath = "//li[@class='auto-leftTab-Unpublished']")
     WebElement listingUnpublishedStatus;
     
     @FindBy(xpath = "//li[@class='auto-leftTab-NeedWork']")
     WebElement listingNeedWorkStatus;
     
     @FindBy(xpath = "//li[@class='auto-leftTab-Scheduled']")
     WebElement listingScheduledStatus;
     
     @FindBy(xpath = "//li[@class='auto-leftTab-SubmittedToReview']")
     WebElement listingSubmittedToReviewStatus;
     
     @FindBy(xpath = "//li[@class='auto-leftTab-Archived']")
     WebElement listingArchivedStatus;
     
     @FindBy(xpath = "//li[@class='auto-leftTab-PublishingQueue']")
     WebElement listingPublishingQueueStatus;
    //----------------------------------------- Common Elements -----------------------------------------------------------
    @FindBy(className = "next-step-btn")
    WebElement nextButton;
    
    @FindBy(className = "prev-step-btn")
    WebElement previousButton;
    

    @FindBy(css = "button.MuiButtonBase-root.MuiIconButton-root.MuiAutocomplete-popupIndicator.MuiAutocomplete-popupIndicatorOpen > span.MuiIconButton-label > svg.MuiSvgIcon-root")
    WebElement closeDropdown;

    @FindBy(xpath = "//div[contains(@class,'auto-TVShow')]")
    WebElement tvShowButton;

    @FindBy(xpath = "//div[contains(text(),'Movie')]")
    WebElement movieButton;

    @FindBy(xpath ="//div[contains(@class,'mov-icon mov-view tooltip-sec auto-view')]")
    List < WebElement > editButton;

    @FindBy(xpath = "//span[contains(text(),'Edit TV Show')]")
    WebElement editTvShow;

    @FindBy(name = "searchString")
    WebElement searchString;

    @FindBy(xpath = "//span[contains(text(),'Yes') or contains(text(),'Ok')]")
    List < WebElement > yes;

    @FindBy(xpath = "//span[contains(text(),'No')]")
    WebElement no;
    
    @FindBy(xpath = "//span[contains(text(),'Ok')]")
    WebElement ok;

    @FindBy(xpath = "//div[contains(@class,'mark-fill-active')]")
    WebElement doneButtonActive; // rgba(255, 255, 255, 1)

    @FindBy(xpath = "//li[@role='option' or @role='menuitem']")
    List < WebElement > dropDownList;

    @FindBy(xpath = "//div[contains(@class,'mark-active')]")
    WebElement doneButtonClicked; // rgba(52, 194, 143, 1)

    @FindBy(xpath = "//button[contains(@class,'auto-tab-Dashboard')]")
    WebElement dashboard;

    @FindBy(css = "button.MuiButtonBase-root.MuiIconButton-root.MuiAutocomplete-popupIndicator.MuiAutocomplete-popupIndicatorOpen > span.MuiIconButton-label > svg.MuiSvgIcon-root")
    WebElement filterCloseDropdown;

    @FindBy(xpath = ("//input[@type='checkbox']"))
    List < WebElement > checkBox;
    
    @FindBy(xpath ="//span[contains(text(),'Save')]")
    WebElement save;

    @FindBy(css = "span.MuiIconButton-label > svg.MuiSvgIcon-root.MuiSvgIcon-fontSizeSmall")
    WebElement clearInput;
    
    @FindBy(xpath = "//a[contains(text(),'TV Shows')]")
    WebElement tvShowsLink;
    
    @FindBy(id = "assignedContent")
    WebElement assignContent;
    
    @FindBy(xpath = "//a[contains(@class,'auto-breadCrum-Seasons')]")
    WebElement seasonsBreadcrumb;
          
    
    //----------------------------- Content Properties Assertions -----------------------------------------------
    @FindBy(xpath="(//div[contains(text(),'Note')]//following::div[@class='data'])[1]")
    WebElement noteAssertion;
    
    @FindBy(xpath="(//div[contains(text(),'Title')]//following::div[@class='data'])[1]")
    WebElement titleAssertion;
    
    @FindBy(xpath="(//div[contains(text(),'Short Description')]//following::div[@class='data'])[1]")
    WebElement shortDescriptionAssertion;
    
    @FindBy(xpath="(//div[contains(text(),'Primary Genre')]//following::div[@class='data'])[1]")
    WebElement primaryGenreAssertion;
    
    @FindBy(xpath="(//div[contains(text(),'ZEE5 Release Date')]//following::div[@class='data'])[1]")
    WebElement zee5ReleaseDateAssertion;
    
    @FindBy(xpath="(//div[contains(text(),'Audio Language')]//following::div[@class='data'])[1]")
    WebElement audioLanguageAssertion;
    
    @FindBy(xpath="(//div[contains(text(),'Broadcast State')]//following::div[@class='data'])[1]")
    WebElement broadcastStateAssertion;
    
    @FindBy(xpath="(//div[contains(text(),'Certification')]//following::div[@class='data'])[1]")
    WebElement certificationAssertion;
    
    //------------------------- Cast/Crew Assertion ------------------------------------------------------
    
    @FindBy(xpath="(//div[contains(text(),'Actor')]//following::div[@class='data'])[1]")
    WebElement actorAssertion;
    
    @FindBy(xpath="(//div[contains(text(),'Character')]//following::div[@class='data'])[2]")
    WebElement characterAssertion;
    
    
    //---------------------------- Seasons Listing Page Search --------------------------------------------------------------------------------
    
    
    @FindBy(xpath="//button[contains(@class,'TVShow')]")
    WebElement tvShowSearch;
    
    @FindBy(xpath="//button[contains(@class,'Seasons')]")
    WebElement seasonSearch;
    
    @FindBy(xpath="//button[contains(@class,'Episodes')]")
    WebElement episodesSearch;

    @FindBy(className = "head-external-id")
    WebElement seasonExternalID;   
  
    //-----------------------------Initializing the page objects------------------------------------------
    
	public SeasonsPage() {
		PageFactory.initElements(driver, this);
	}

	public void SeasonProperties() throws InterruptedException, Exception {	
			
		WebUtility.Click(dashboard);
        WebUtility.Click(tvShowButton);
	
		searchString.click();
		searchString.sendKeys(tvShowTitle);
		WebUtility.Wait();
		WebUtility.Wait();
					
		WebUtility.element_to_be_clickable(editButton.get(0));
		editButton.get(0).click();
	
  
		WebUtility.Wait();
		WebUtility.Click(seasons);
		WebUtility.Click(createSeason);
		createSeasonMainSeason.click();
		yes.get(0).click();
		
		WebUtility.Wait();
		externalID = js.executeScript("return arguments[0].childNodes[1].innerText", seasonExternalID).toString();
		WebUtility.Click(editNote);
		WebUtility.element_to_be_clickable(note);
        note.sendKeys(Keys.CONTROL + "a");
        note.sendKeys(Keys.DELETE);
        note.sendKeys("Automation Test - Season");
		WebUtility.Click(editNote);
	     Thread.sleep(1000);
        
		WebUtility.Click(editTitle);
        WebUtility.Click(title);
        title.sendKeys(Keys.CONTROL + "a");
        title.sendKeys(Keys.BACK_SPACE);
        title.sendKeys(tvShowTitle+" - Season");
        WebUtility.Click(editTitle);
        Thread.sleep(1000);     

        WebUtility.Click(editShortDesc);
        WebUtility.Click(shortDesc);
        shortDesc.sendKeys(Keys.CONTROL + "a");
        shortDesc.sendKeys(Keys.BACK_SPACE);
        shortDesc.sendKeys("The Expanse is an American science fiction television series developed by Mark Fergus and Hawk Ostby, based on the series of novels of the same name by James S. A. Corey.- Season");
        WebUtility.Click(editShortDesc);
        Thread.sleep(1000);
                 
        WebUtility.Click(editWebDesc);
        WebUtility.Click(webDesc);
        webDesc.sendKeys(Keys.CONTROL + "a");
        webDesc.sendKeys(Keys.BACK_SPACE);
        webDesc.sendKeys("Automation Test - Season");
        WebUtility.Click(editWebDesc);
        Thread.sleep(1000);
        

        WebUtility.Click(editAppDesc);
        WebUtility.Click(appDesc);
        appDesc.sendKeys(Keys.CONTROL + "a");
        appDesc.sendKeys(Keys.BACK_SPACE);
        appDesc.sendKeys("Automation Test - Season");
        WebUtility.Click(editAppDesc);
        Thread.sleep(1000);
 
        WebUtility.Click(editTvDesc);
        WebUtility.javascript(tvDesc);
        js.executeScript("arguments[0].focus()", tvDesc);
        tvDesc.sendKeys(Keys.CONTROL + "a");
        tvDesc.sendKeys(Keys.BACK_SPACE);
        tvDesc.sendKeys("Automation Test - Season");
        WebUtility.Click(editTvDesc);
        Thread.sleep(1000);
  


        WebUtility.javascript(editCategory);
        WebUtility.Click(category);
        category.sendKeys(Keys.BACK_SPACE);
        WebUtility.Click(dropDownList.get(0));
        editCategory.click(); 
        Thread.sleep(1000);
        
        WebUtility.Click(editPrimaryGenre);        
        primaryGenre.click();
        primaryGenre.sendKeys(Keys.BACK_SPACE);
		WebUtility.Click(dropDownList.get(0));
        editPrimaryGenre.click();
        Thread.sleep(1000);
 
		WebUtility.Click(editSecondaryGenre);
		secondaryGenre.click();
		secondaryGenre.sendKeys(Keys.BACK_SPACE);
		WebUtility.Click(dropDownList.get(0));
		editSecondaryGenre.click();
		Thread.sleep(1000);

		WebUtility.Click(editThematicGenre);
		thematicGenre.click();
		thematicGenre.sendKeys(Keys.BACK_SPACE);
		WebUtility.Click(dropDownList.get(0));
		editThematicGenre.click();
		Thread.sleep(1000);
	   
		WebUtility.Click(editSettingGenre);
        settingGenre.click();
        settingGenre.sendKeys(Keys.BACK_SPACE);
        WebUtility.Click(dropDownList.get(0));
        editSettingGenre.click();
        Thread.sleep(1000);   
        
        WebUtility.Click(editRcsCategory);
        rcsCategory.click();
        rcsCategory.sendKeys(Keys.BACK_SPACE);
        WebUtility.Click(dropDownList.get(0));
        WebUtility.Click(editRcsCategory);
        Thread.sleep(1000);

        WebUtility.Click(editPageTitle);
        pageTitle.click();
        pageTitle.sendKeys(Keys.CONTROL + "a");
        pageTitle.sendKeys(Keys.BACK_SPACE);
        pageTitle.sendKeys("Automation Page Title - Season");
        WebUtility.Click(editPageTitle);
        Thread.sleep(1000);
    
        WebUtility.Click(editPageDescription);
        pageDescription.click();
        pageDescription.sendKeys(Keys.CONTROL + "a");
        pageDescription.sendKeys(Keys.BACK_SPACE);
        pageDescription.sendKeys("Automation Page Description - Season");
        WebUtility.Click(editPageDescription);
    	Thread.sleep(1000);	
  
        WebUtility.Click(editTitleForSocialShare);
        titleForSocialShare.click();
        titleForSocialShare.sendKeys(Keys.CONTROL + "a");
        titleForSocialShare.sendKeys(Keys.BACK_SPACE);
        titleForSocialShare.sendKeys("Automation Title for Social Share - Season");
        WebUtility.Click(editTitleForSocialShare);
    	Thread.sleep(1000);	
   
        WebUtility.Click(editDescriptionForSocialShare);
        descriptionForSocialShare.click();
        descriptionForSocialShare.sendKeys(Keys.CONTROL + "a");
        descriptionForSocialShare.sendKeys(Keys.BACK_SPACE);
        descriptionForSocialShare.sendKeys("Automation Description for Social Share - Season");
        WebUtility.Click(editDescriptionForSocialShare);
        Thread.sleep(1000);

        WebUtility.Click(editZeeReleaseDate);
        zee5ReleaseDate.click();
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ESCAPE);
        WebUtility.Click(editZeeReleaseDate);
        Thread.sleep(1000);
	
        WebUtility.Click(editOriginalTelecastDate);
        telecastDate.click();
        datePicker.sendKeys(Keys.ARROW_LEFT);
        datePicker.sendKeys(Keys.ESCAPE);
        WebUtility.Click(editOriginalTelecastDate);
        Thread.sleep(1000);
	
		WebUtility.Click(editMultiAudio);
		if(!multiAudio.isSelected())
		{
		    multiAudio.click();
		    editMultiAudio.click();
		}
		else
		    editMultiAudio.click();		
	    WebUtility.Wait();

		WebUtility.Click(editAudioLanguage);
		audioLanguage.click();
		audioLanguage.sendKeys(Keys.BACK_SPACE);
		WebUtility.Click(dropDownList.get(0));
		editAudioLanguage.click();
		Thread.sleep(1000);

		WebUtility.Click(editSpecialCategory);
		specialCategory.click();
		specialCategory.sendKeys(Keys.BACK_SPACE);
		WebUtility.Click(dropDownList.get(0));
		WebUtility.Click(editSpecialCategory);
		Thread.sleep(1000);

		WebUtility.Click(editSpecialCategoryCountry);
		specialCategoryCountry.click();
		specialCategoryCountry.sendKeys(Keys.BACK_SPACE);
		WebUtility.Click(dropDownList.get(0));
		WebUtility.Click(editSpecialCategoryCountry);
		Thread.sleep(1000);

		WebUtility.Click(editSpecialToTimeline);
		specialCategoryTo.click();		
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ESCAPE);	
        WebUtility.Click(editSpecialToTimeline);
		Thread.sleep(1000);
		
		WebUtility.Click(editSpecialFromTimeline);
		specialCategoryFrom.click();
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ESCAPE);
        WebUtility.Click(editSpecialFromTimeline);
		Thread.sleep(1000);
  
		WebUtility.Click(nextButton);
		
        WebUtility.Click(editBroadcastState);
        broadcastState.click();
        broadcastState.sendKeys(Keys.BACK_SPACE);
		WebUtility.Click(dropDownList.get(0));
		WebUtility.Click(editBroadcastState);
		Thread.sleep(1000);		
		
		WebUtility.Click(editUpcomingPage);
		upcomingPage.click();
		upcomingPage.sendKeys(Keys.CONTROL + "a");
		upcomingPage.sendKeys(Keys.BACK_SPACE);
		upcomingPage.sendKeys("Automation Upcoming Page Text - Season");
        WebUtility.Click(editUpcomingPage);
    	Thread.sleep(1000);		
		
        WebUtility.Click(editAgeRating);
        ageRating.click();
        ageRating.sendKeys(Keys.BACK_SPACE);
		WebUtility.Click(dropDownList.get(0));
		WebUtility.Click(editAgeRating);
		Thread.sleep(1000);			
		
		WebUtility.Click(editCertification);
		certification.click();
		certification.sendKeys(Keys.BACK_SPACE);
		WebUtility.Click(dropDownList.get(0));
		WebUtility.Click(editCertification);
		Thread.sleep(1000);

		WebUtility.Click(nextButton);
		WebUtility.Wait();
		String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
		sa.assertTrue(doneButtonActiveColor.contains("255, 255, 255") || doneButtonActiveColor.contains("255,255,255"), "Seasons Main Content Properties Mark As Done Assertion Fails");
		WebUtility.Click(doneButtonActive);
		Thread.sleep(1000);

		try {
			TestUtil.captureScreenshot("Season Main Content Properties Page","Season");
		} catch (IOException e) {
			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}
	
	}

	public void SeasonPropertiesAssertions() throws InterruptedException {
	/*
		WebUtility.Click(dashboard);
        WebUtility.Click(tvShowButton);
	
		searchString.click();
		searchString.sendKeys(tvShowTitle);
		WebUtility.Wait();
		WebUtility.Wait();
		WebUtility.element_to_be_clickable(editButton.get(0));
		editButton.get(0).click();

		
		WebUtility.Wait();
		WebUtility.Click(seasons);
		WebUtility.Click(editButton.get(0));
		WebUtility.Wait();
*/
		WebUtility.Click(titleSummary);
		WebUtility.Wait();
		
		String expectedNote = "Automation Test - Season";
		Assert.assertEquals(noteAssertion.getAttribute("innerText"), expectedNote, "Season Main Content - Note Assertion Fails");
		String expectedTitle = tvShowTitle+" - Season";
		Assert.assertEquals(titleAssertion.getAttribute("innerText"), expectedTitle, "Season Main Content - Title Assertion Fails");
		
		js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
		Assert.assertNotEquals(primaryGenreAssertion.getAttribute("innerText"),"NA","Season Main Content - Primary Genre Assertion Fails");
		Assert.assertNotEquals(audioLanguageAssertion.getAttribute("innerText"),"NA","Season Main Content - Audio Language Assertion Fails");
		
		WebUtility.Click(nextButton);
		WebUtility.Wait();
		Assert.assertNotEquals(broadcastStateAssertion.getAttribute("innerText"),"NA","Season Main Content - Broadcast State Assertion Fails");
		Assert.assertNotEquals(certificationAssertion.getAttribute("innerText"),"NA","Season Main Content - Certification Assertion Fails");
		
		String doneButtonClickedColor = doneButtonClicked.getCssValue("background-color");
        Assert.assertTrue(doneButtonClickedColor.contains("52, 194, 143"), "Season Main Content - Properties Mark As Done Assertion Fails");
	}
	
	// ------------------------------------- Season Cast/Crew -------------------------------------------------------------------------------------
	public void SeasonCastCrew() throws InterruptedException, Exception{
/*	    
		WebUtility.Click(dashboard);
		WebUtility.Click(tvShowButton);
		WebUtility.Wait();

		searchString.click();
		searchString.sendKeys(tvShowTitle);
		WebUtility.Wait();
		WebUtility.Wait();
		WebUtility.element_to_be_clickable(editButton.get(0));
		editButton.get(0).click();
					 

		
		WebUtility.Wait();
		WebUtility.Click(seasons);
		WebUtility.Click(createSeason);
		createSeasonMainSeason.click();
		yes.get(0).click();
				
*/		
		WebUtility.Click(castCrew);
		WebUtility.Wait();	
		WebUtility.Wait();			  
		try {
		  removeActor.click();
		} catch (Exception e) {
			System.out.println("No Additional Actor/Characters");
			e.printStackTrace();		   
		}
		try {
			WebUtility.Wait();		 
			WebUtility.Click(editActor);
			actor.sendKeys(Keys.CONTROL + "a");
			actor.sendKeys(Keys.BACK_SPACE);
			actor.sendKeys("Salman Khan Season");
			WebUtility.element_to_be_clickable(dropDownList.get(0));
			dropDownList.get(0).click();
			WebUtility.Wait();
			WebUtility.Click(editActor);
			Thread.sleep(1000);

			WebUtility.Click(editCharacter);
			character.sendKeys(Keys.CONTROL + "a");
			character.sendKeys(Keys.BACK_SPACE);
			character.sendKeys("Test Character Season");
			WebUtility.Click(editCharacter);
			WebUtility.Wait();

			WebUtility.Click(editActorChange.get(1));
			actorChange.sendKeys(Keys.CONTROL + "a");
			actorChange.sendKeys(Keys.BACK_SPACE);
			actorChange.sendKeys("Test Actor Season");
			WebUtility.element_to_be_clickable(dropDownList.get(0));
			dropDownList.get(0).click();
			WebUtility.element_to_be_clickable(editActorChange.get(1));
			editActorChange.get(1).click();
			Thread.sleep(1000);

			editPerformer.get(1).click();
			performer.sendKeys(Keys.CONTROL + "a");
			performer.sendKeys(Keys.BACK_SPACE);
			performer.sendKeys("Test Performer Season");
			WebUtility.element_to_be_clickable(dropDownList.get(0));
			dropDownList.get(0).click();
			WebUtility.element_to_be_clickable(editPerformer.get(1));
			editPerformer.get(1).click();
			Thread.sleep(1000);

			WebUtility.Click(editHost);
			host.sendKeys(Keys.CONTROL + "a");
			host.sendKeys(Keys.BACK_SPACE);
			host.sendKeys("Test Host Season");
			WebUtility.element_to_be_clickable(dropDownList.get(0));
			dropDownList.get(0).click();
			WebUtility.Click(editHost);
			Thread.sleep(1000);

			WebUtility.Click(editSinger);
			singer.sendKeys(Keys.CONTROL + "a");
			singer.sendKeys(Keys.BACK_SPACE);
			singer.sendKeys("Automation Test Data Season");
			WebUtility.element_to_be_clickable(dropDownList.get(0));
			dropDownList.get(0).click();
			WebUtility.Click(editSinger);
			Thread.sleep(1000);

			WebUtility.Click(editLyricst);
			lyricst.sendKeys(Keys.CONTROL + "a");
			lyricst.sendKeys(Keys.BACK_SPACE);
			lyricst.sendKeys("Automation Test Data Season");
			WebUtility.element_to_be_clickable(dropDownList.get(0));
			dropDownList.get(0).click();
			WebUtility.Click(editLyricst);
			Thread.sleep(1000);

			WebUtility.Click(editDirector);
			director.sendKeys(Keys.CONTROL + "a");
			director.sendKeys(Keys.BACK_SPACE);
			director.sendKeys("Automation Test Data Season");
			WebUtility.element_to_be_clickable(dropDownList.get(0));
			dropDownList.get(0).click();
			WebUtility.Click(editDirector);
			Thread.sleep(1000);

			WebUtility.Click(editCinematography);
			cinematography.sendKeys(Keys.CONTROL + "a");
			cinematography.sendKeys(Keys.BACK_SPACE);
			cinematography.sendKeys("Automation Test Data Season");
			WebUtility.element_to_be_clickable(dropDownList.get(0));
			dropDownList.get(0).click();
			WebUtility.Click(editCinematography);
			Thread.sleep(1000);

			WebUtility.Click(editProducer);
			producer.sendKeys(Keys.CONTROL + "a");
			producer.sendKeys(Keys.BACK_SPACE);
			producer.sendKeys("Automation Test Data Season");
			WebUtility.element_to_be_clickable(dropDownList.get(0));
			dropDownList.get(0).click();
			WebUtility.Click(editProducer);
			Thread.sleep(1000);

			WebUtility.Click(editExecutiveProducer);
			executiveProducer.sendKeys(Keys.CONTROL + "a");
			executiveProducer.sendKeys(Keys.BACK_SPACE);
			executiveProducer.sendKeys("Automation Test Data Season");
			WebUtility.element_to_be_clickable(dropDownList.get(0));
			dropDownList.get(0).click();
			WebUtility.Click(editExecutiveProducer);
			Thread.sleep(1000);

			WebUtility.Click(editMusicDirector);
			musicDirector.sendKeys(Keys.CONTROL + "a");
			musicDirector.sendKeys(Keys.BACK_SPACE);
			musicDirector.sendKeys("Automation Test Data Season");
			WebUtility.element_to_be_clickable(dropDownList.get(0));
			dropDownList.get(0).click();
			WebUtility.Click(editMusicDirector);
			Thread.sleep(1000);

			WebUtility.Click(editChoreographer);
			choreographer.sendKeys(Keys.CONTROL + "a");
			choreographer.sendKeys(Keys.BACK_SPACE);
			choreographer.sendKeys("Automation Test Data Season");
			WebUtility.element_to_be_clickable(dropDownList.get(0));
			dropDownList.get(0).click();
			WebUtility.Click(editChoreographer);
			Thread.sleep(1000);

			WebUtility.Click(editTitleThemeMusic);
			titleThemeMusic.sendKeys(Keys.CONTROL + "a");
			titleThemeMusic.sendKeys(Keys.BACK_SPACE);
			titleThemeMusic.sendKeys("Automation Test Data Season");
			WebUtility.element_to_be_clickable(dropDownList.get(0));
			dropDownList.get(0).click();
			WebUtility.Click(editTitleThemeMusic);
			Thread.sleep(1000);

			WebUtility.Click(editBackgroundScore);
			backgroundScore.sendKeys(Keys.CONTROL + "a");
			backgroundScore.sendKeys(Keys.BACK_SPACE);
			backgroundScore.sendKeys("Automation Test Data Season");
			WebUtility.element_to_be_clickable(dropDownList.get(0));
			dropDownList.get(0).click();
			WebUtility.Click(editBackgroundScore);
			Thread.sleep(1000);

			WebUtility.Click(editStoryWriter);
			storyWriter.sendKeys(Keys.CONTROL + "a");
			storyWriter.sendKeys(Keys.BACK_SPACE);
			storyWriter.sendKeys("Automation Test Data Season");
			WebUtility.element_to_be_clickable(dropDownList.get(0));
			dropDownList.get(0).click();
			WebUtility.Click(editStoryWriter);
			Thread.sleep(1000);

			WebUtility.Click(editScreenPlay);
			screenPlay.sendKeys(Keys.CONTROL + "a");
			screenPlay.sendKeys(Keys.BACK_SPACE);
			screenPlay.sendKeys("Automation Test Data Season");
			WebUtility.element_to_be_clickable(dropDownList.get(0));
			dropDownList.get(0).click();
			WebUtility.Click(editScreenPlay);
			Thread.sleep(1000);

			WebUtility.Click(editDialogueWriter);
			dialogueWriter.sendKeys(Keys.CONTROL + "a");
			dialogueWriter.sendKeys(Keys.BACK_SPACE);
			dialogueWriter.sendKeys("Automation Test Data Season");
			WebUtility.element_to_be_clickable(dropDownList.get(0));
			dropDownList.get(0).click();
			WebUtility.Click(editDialogueWriter);
			Thread.sleep(1000);

			WebUtility.Click(editFilmEditing);
			filmEditing.sendKeys(Keys.CONTROL + "a");
			filmEditing.sendKeys(Keys.BACK_SPACE);
			filmEditing.sendKeys("Automation Test Data Season");
			WebUtility.element_to_be_clickable(dropDownList.get(0));
			dropDownList.get(0).click();
			WebUtility.Click(editFilmEditing);
			Thread.sleep(1000);

			WebUtility.Click(editCasting);
			casting.sendKeys(Keys.CONTROL + "a");
			casting.sendKeys(Keys.BACK_SPACE);
			casting.sendKeys("Automation Test Data Season");
			WebUtility.element_to_be_clickable(dropDownList.get(0));
			dropDownList.get(0).click();
			WebUtility.Click(editCasting);
			Thread.sleep(1000);

			WebUtility.Click(editProductionDesign);
			productionDesign.sendKeys(Keys.CONTROL + "a");
			productionDesign.sendKeys(Keys.BACK_SPACE);
			productionDesign.sendKeys("Automation Test Data Season");
			WebUtility.element_to_be_clickable(dropDownList.get(0));
			dropDownList.get(0).click();
			WebUtility.Click(editProductionDesign);
			Thread.sleep(1000);

			WebUtility.Click(editArtDirection);
			artDirection.sendKeys(Keys.CONTROL + "a");
			artDirection.sendKeys(Keys.BACK_SPACE);
			artDirection.sendKeys("Automation Test Data Season");
			WebUtility.element_to_be_clickable(dropDownList.get(0));
			dropDownList.get(0).click();
			WebUtility.Click(editArtDirection);
			Thread.sleep(1000);

			WebUtility.Click(editSetDecoration);
			setDecoration.sendKeys(Keys.CONTROL + "a");
			setDecoration.sendKeys(Keys.BACK_SPACE);
			setDecoration.sendKeys("Automation Test Data Season");
			WebUtility.element_to_be_clickable(dropDownList.get(0));
			dropDownList.get(0).click();
			WebUtility.Wait();
			WebUtility.Click(editSetDecoration);
			Thread.sleep(1000);

			WebUtility.Click(editCostumeDesign);
			costumeDesign.sendKeys(Keys.CONTROL + "a");
			costumeDesign.sendKeys(Keys.BACK_SPACE);
			costumeDesign.sendKeys("Automation Test Data Season");
			WebUtility.element_to_be_clickable(dropDownList.get(0));
			dropDownList.get(0).click();
			WebUtility.Click(editCostumeDesign);
			Thread.sleep(1000);

			WebUtility.Click(editProductionCo);
			productionCo.sendKeys(Keys.CONTROL + "a");
			productionCo.sendKeys(Keys.BACK_SPACE);
			productionCo.sendKeys("Automation Test Data Season");
			WebUtility.element_to_be_clickable(dropDownList.get(0));
			dropDownList.get(0).click();
			WebUtility.Click(editProductionCo);
			Thread.sleep(1000);

			WebUtility.Click(editPresenter);
			presenter.sendKeys(Keys.CONTROL + "a");
			presenter.sendKeys(Keys.BACK_SPACE);
			presenter.sendKeys("Automation Test Data Season");
			WebUtility.element_to_be_clickable(dropDownList.get(0));
			dropDownList.get(0).click();
			WebUtility.Click(editPresenter);
			Thread.sleep(1000);

			WebUtility.Click(editGuest);
			guest.sendKeys(Keys.CONTROL + "a");
			guest.sendKeys(Keys.BACK_SPACE);
			guest.sendKeys("Automation Test Data Season");
			WebUtility.element_to_be_clickable(dropDownList.get(0));
			dropDownList.get(0).click();
			WebUtility.Wait();
			WebUtility.Click(editGuest);
			Thread.sleep(1000);

			WebUtility.Click(editParticipant);
			participant.sendKeys(Keys.CONTROL + "a");
			participant.sendKeys(Keys.BACK_SPACE);
			participant.sendKeys("Automation Test Data Season");
			WebUtility.element_to_be_clickable(dropDownList.get(0));
			dropDownList.get(0).click();
			WebUtility.Click(editParticipant);
			Thread.sleep(1000);

			WebUtility.Click(editJudges);
			judges.sendKeys(Keys.CONTROL + "a");
			judges.sendKeys(Keys.BACK_SPACE);
			judges.sendKeys("Automation Test Data Season");
			WebUtility.element_to_be_clickable(dropDownList.get(0));
			dropDownList.get(0).click();
			WebUtility.Click(editJudges);
			Thread.sleep(1000);

			WebUtility.Click(editNarrator);
			narrator.sendKeys(Keys.CONTROL + "a");
			narrator.sendKeys(Keys.BACK_SPACE);
			narrator.sendKeys("Automation Test Data Season");
			WebUtility.element_to_be_clickable(dropDownList.get(0));
			dropDownList.get(0).click();
			WebUtility.Click(editNarrator);
			Thread.sleep(1000);

			WebUtility.Click(editSponsors);
			sponsor.sendKeys(Keys.CONTROL + "a");
			sponsor.sendKeys(Keys.BACK_SPACE);
			sponsor.sendKeys("Automation Test Data Season");
			WebUtility.element_to_be_clickable(dropDownList.get(0));
			dropDownList.get(0).click();
			WebUtility.Click(editSponsors);
			Thread.sleep(1000);

			WebUtility.Click(editGraphics);
			graphics.sendKeys(Keys.CONTROL + "a");
			graphics.sendKeys(Keys.BACK_SPACE);
			graphics.sendKeys("Automation Test Data Season");
			WebUtility.element_to_be_clickable(dropDownList.get(0));
			dropDownList.get(0).click();
			WebUtility.Click(editGraphics);
			Thread.sleep(1000);

			WebUtility.Click(editVocalist);
			vocalist.sendKeys(Keys.CONTROL + "a");
			vocalist.sendKeys(Keys.BACK_SPACE);
			vocalist.sendKeys("Automation Test Data Season");
			WebUtility.element_to_be_clickable(dropDownList.get(0));
			dropDownList.get(0).click();
			WebUtility.Click(editVocalist);
			WebUtility.Wait();

			String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
			sa.assertTrue(doneButtonActiveColor.contains("255, 255, 255") || doneButtonActiveColor.contains("255,255,255"), "Seasons Cast & Crew Mark As Done Assertion Fails");
			WebUtility.Click(doneButtonActive);

			try {
				TestUtil.captureScreenshot("Season Main Content Cast and Crew Section","Season");
			} catch (IOException e) {
				System.out.println("Screenshot Capture Failed ->" + e.getMessage());
			}

			js.executeScript("window.scrollTo(0,document.body.scrollHeight);");

			try {
				TestUtil.captureScreenshot("Season Main Content Cast and Crew Section 2","Season");
			} catch (IOException e) {
				System.out.println("Screenshot Capture Failed ->" + e.getMessage());
			}
		} catch (Exception e) {
			WebUtility.Click(doneButtonActive);
			e.printStackTrace();		   
		}
	    }
	
	 public void SeasonCastCrewAssertions() throws InterruptedException {
	    	WebUtility.Click(castCrew);
	    	WebUtility.Wait();
			String expectedActor = "Salman Khan Season";
			Assert.assertEquals(actorAssertion.getAttribute("innerText"), expectedActor, "Season Main Content - Cast Crew Actor Assertion Fails");
			String expectedCharacter = "Test Character Season";
			Assert.assertEquals(characterAssertion.getAttribute("innerText"), expectedCharacter, "Season Main Content - Cast Crew Actor Assertion Fails");
			String doneButtonClickedColor = doneButtonClicked.getCssValue("background-color");
	        Assert.assertTrue(doneButtonClickedColor.contains("52, 194, 143"), "Season Main Content - Cast & Crew Mark As Done Assertion Fails");
	 }
	
	public void SeasonLicense() throws InterruptedException, Exception{
		 
        WebUtility.Click(license);
     
        try
        {
	        WebUtility.Click(createLicense);
	
	        WebUtility.Wait();
	        useTemplateRadioButton.click();
	
	        WebUtility.Click(selectTemplate);
	        WebUtility.Click(dropDownList.get(0));
	        js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
	        try {
				int licenseTemplateSize = licenseTemplateDeleteSets.size();
				for(int i = 1;i< licenseTemplateSize ;i++)
				{
					System.out.println("License Sets Size--->"+licenseTemplateDeleteSets.size());
					licenseTemplateDeleteSets.get(1).click();
					Thread.sleep(1000);
					System.out.println("License Sets Size After Delete--->"+licenseTemplateDeleteSets.size());
				}
			}
			catch (Exception e){
				System.out.println("No Sets in License Template");
			}
	        WebUtility.Click(editLicense);
	        WebUtility.Click(editLicenseCountry);
	        WebUtility.Click(clearInput);
	        for (int x = 0; x < dropDownList.size(); x++) {
	        	if(x<4) {
	            WebUtility.Click(dropDownList.get(x));
	        	}
	        	else {
	        		break;
	        	}
	        }
	        WebUtility.Wait();
	        WebUtility.Click(editLicenseCountry);
	        WebUtility.Wait();
	        WebUtility.javascript(saveLicense);
	
	        WebUtility.Click(licenseSetName);
	        licenseSetName.sendKeys("Automation Test Data");
	        
	        WebUtility.Click(licenseFromDate);
	        datePicker.sendKeys(Keys.ARROW_RIGHT);
	        datePicker.sendKeys(Keys.ESCAPE);
	
	        WebUtility.Click(licenseToDate);
	        datePicker.sendKeys(Keys.ARROW_RIGHT);
	        datePicker.sendKeys(Keys.ARROW_RIGHT);
	        datePicker.sendKeys(Keys.ESCAPE);
	
	        WebUtility.Wait();
	        WebUtility.Click(saveLicense);
        }
        catch(Exception e)
        {
        	System.out.println("Creating a license through template Failed!!");
        	e.printStackTrace();
        }
      
        try
        {
	        WebUtility.Click(createLicense);
	        
	        WebUtility.Click(licenseSetName);
	        licenseSetName.sendKeys("Automation Test Data II");
	        
	        WebUtility.Click(licenseFromDate);
	        datePicker.sendKeys(Keys.ARROW_RIGHT);
	        datePicker.sendKeys(Keys.ARROW_RIGHT);
	        datePicker.sendKeys(Keys.ARROW_RIGHT);
	        datePicker.sendKeys(Keys.ESCAPE);
	
	        WebUtility.Click(licenseToDate);
	        datePicker.sendKeys(Keys.ARROW_RIGHT);
	        datePicker.sendKeys(Keys.ARROW_RIGHT);
	        datePicker.sendKeys(Keys.ARROW_RIGHT);
	        datePicker.sendKeys(Keys.ARROW_RIGHT);
	        datePicker.sendKeys(Keys.ESCAPE);
	
	        WebUtility.Click(licenseCountry);
	        for (int x = 0; x < dropDownList.size(); x++) {
	        	if(x<4) {
	            WebUtility.Click(dropDownList.get(x));
	        	}
	        	else {
	        		break;
	        	}
	        }
	
	        licenseBusinessType.click();
	        WebUtility.Click(dropDownList.get(0));
	
	        licensePlatform.click();
	        WebUtility.Click(dropDownList.get(0));
	
	        licenseTVOD.click();
	        WebUtility.Click(dropDownList.get(0));
	
	        WebUtility.Wait();
	        WebUtility.javascript(saveLicense);
        }
        catch(Exception e)
        {
        	System.out.println("Creating a Manual License Failed");
        	e.printStackTrace();
        }

		WebUtility.element_to_be_clickable(doneButtonActive);
		String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
		sa.assertTrue(doneButtonActiveColor.contains("255, 255, 255") || doneButtonActiveColor.contains("255,255,255"), "Seasons Main Content License Mark As Done Assertion Fails");
		WebUtility.Click(doneButtonActive);

        try {
            TestUtil.captureScreenshot("Season Main Content - License Section","Season");
        } catch (IOException e) {
            System.out.println("Screenshot Capture Failed ->" + e.getMessage());
        }


        WebUtility.Click(expiredLicense);
        Thread.sleep(2000);
        WebUtility.Click(expiredLicenseBack);
        Thread.sleep(2000);

        WebUtility.Click(licenseFilters);
        WebUtility.Click(licenseFromDate);
        datePicker.sendKeys(Keys.ARROW_LEFT);
        datePicker.sendKeys(Keys.ARROW_LEFT);
        datePicker.sendKeys(Keys.ESCAPE);


        WebUtility.Click(licenseToDate);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ESCAPE);

        WebUtility.Click(applyFilter);

        Thread.sleep(2000);
        WebUtility.Click(licenseFilters);
        WebUtility.Click(clearFilter);
        
        WebUtility.Click(licenseFilters);
        WebUtility.Click(licenseFilterBusinessType);
        WebUtility.Click(dropDownList.get(0));
        WebUtility.Wait();
        
        WebUtility.Click(applyFilter);

        Thread.sleep(2000);
        WebUtility.Click(licenseFilters);
        WebUtility.Click(clearFilter);

        WebUtility.Click(licenseFilters);
        WebUtility.Click(licenseFilterBillingType);
        WebUtility.Click(dropDownList.get(0));
        WebUtility.Wait();
        WebUtility.Click(applyFilter);

        Thread.sleep(2000);
        WebUtility.Click(licenseFilters);
        WebUtility.Click(clearFilter);

        WebUtility.Click(licenseFilters);
        WebUtility.Click(licenseFilterPlatform);
        WebUtility.Click(dropDownList.get(0));
        WebUtility.Wait();
        filterCloseDropdown.click();
        WebUtility.Click(applyFilter);

        Thread.sleep(2000);
        WebUtility.Click(licenseFilters);
        WebUtility.Click(clearFilter);

        WebUtility.Click(licenseFilters);
        WebUtility.Wait();
        licenseFilterActiveStatus.click();
        WebUtility.Wait();
        WebUtility.Click(applyFilter);

        Thread.sleep(2000);
        WebUtility.Click(licenseFilters);
        WebUtility.Click(clearFilter);

        WebUtility.Click(licenseFilters);
        WebUtility.Wait();
        licenseFilterInActiveStatus.click();
        WebUtility.Wait();
        WebUtility.Click(applyFilter);

        Thread.sleep(2000);
        WebUtility.Click(licenseFilters);
        WebUtility.Click(clearFilter);
 
	}
	
	public void SeasonImages() throws InterruptedException, Exception{
	/*
		dashboard.click();
		tvShowButton.click();
		Thread.sleep(2000);

		searchString.click();
		searchString.sendKeys("Zee5 The Expanse Suite Single Landing");
		Thread.sleep(2000);
		WebUtility.element_to_be_clickable(editButton.get(0));
		editButton.get(1).click();
		Thread.sleep(2000);

		WebUtility.Click(seasons);
		WebUtility.element_to_be_clickable(createSeason);
		createSeason.click();
		Thread.sleep(2000);
		createSeasonMainSeason.click();
		yes.get(0).click();
*/
		WebUtility.Click(images);
		WebUtility.Wait();
		try {
			if(removeSet.get(0).isDisplayed()==true);
			{
			for(int x = 0 ; x < removeSet.size(); x++)
				{
				removeSet.get(x).click();
				WebUtility.Wait();
				yes.get(0).click();
				WebUtility.Wait();
				}
			}
		}
		catch(Exception e){
			System.out.println("No Image Sets Exist");
		}

		WebUtility.Click(imagesCreateNewSet);

		WebUtility.Click(newSetName);
		newSetName.sendKeys("Automation Test Data - Season");

		newSetCountry.click();
		WebUtility.Click(dropDownList.get(0));
		closeDropdown.click();

		newSetPlatform.click();
		WebUtility.Click(dropDownList.get(0));
		closeDropdown.click();

		newSetGender.click();
		WebUtility.Click(dropDownList.get(0));
		closeDropdown.click();

		newSetGenre.click();
		WebUtility.Click(dropDownList.get(0));
		closeDropdown.click();

		newSetAgeGroup.click();
		WebUtility.Click(dropDownList.get(0));
		closeDropdown.click();

		newSetLanguage.click();
		WebUtility.Click(dropDownList.get(0));
		closeDropdown.click();

		newSetOthers.click();
		newSetOthers.sendKeys("Automation Test Data");

		WebUtility.Click(save);
		WebUtility.Wait();

		WebUtility.Click(imageSets.get(1));

		js.executeScript("window.scrollTo(0,0)");
		WebUtility.Wait();
		cover.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\Cover.jpg");

		WebUtility.Wait();
		appcover.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\AppCover.jpg");

		WebUtility.Wait();
		list.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\List.jpg");

		WebUtility.Wait();
		square.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\SquarePNG.png");

		js.executeScript("window.scrollBy(0,600)");

		WebUtility.Wait();
		tvCover.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\TVCoverPNG.png");

		WebUtility.Wait();
		portrait.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\portrait.jpg");

		WebUtility.Wait();
		listClean.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\ListCleanPNG.png");

		js.executeScript("window.scrollTo(0,document.body.scrollHeight);");

		WebUtility.Wait();
		portraitClean.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\PortraitCleanPNG.png");

		WebUtility.Wait();
		telcoSquare.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\TelcoSquare.jpg");

		WebUtility.Wait();
		passport.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\PassportPNG.png");

		WebUtility.Wait();
		
		try {
			// -------------------------- Image Set 3
			// -----------------------------------------------------
			WebUtility.element_to_be_fluentwait(imagesCreateNewSet);
			WebUtility.Click(imagesCreateNewSet);

			WebUtility.Click(newSetName);
			newSetName.sendKeys("Automation Test Data II");

			newSetCountry.click();
			WebUtility.Click(dropDownList.get(1));
			closeDropdown.click();

			newSetPlatform.click();
			WebUtility.Click(dropDownList.get(1));
			closeDropdown.click();

			newSetGender.click();
			WebUtility.Click(dropDownList.get(1));
			closeDropdown.click();

			newSetGenre.click();
			WebUtility.Click(dropDownList.get(1));
			closeDropdown.click();

			newSetAgeGroup.click();
			WebUtility.Click(dropDownList.get(1));
			closeDropdown.click();

			newSetLanguage.click();
			WebUtility.Click(dropDownList.get(1));
			closeDropdown.click();

			newSetOthers.click();
			newSetOthers.sendKeys("Automation Test Data II");

			WebUtility.Click(save);

			WebUtility.Click(imageSets.get(2));

			js.executeScript("window.scrollTo(0,0)");
			Thread.sleep(2000);
			cover.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\Cover.jpg");

			WebUtility.Wait();
			appcover.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\AppCover.jpg");

			WebUtility.Wait();
			list.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\List.jpg");

			WebUtility.Wait();
			square.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\SquarePNG.png");

			js.executeScript("window.scrollBy(0,600)");

			WebUtility.Wait();
			tvCover.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\TVCoverPNG.png");

			WebUtility.Wait();
			portrait.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\portrait.jpg");

			WebUtility.Wait();
			listClean.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\ListCleanPNG.png");

			js.executeScript("window.scrollTo(0,document.body.scrollHeight);");

			WebUtility.Wait();
			portraitClean.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\PortraitCleanPNG.png");

			WebUtility.Wait();
			telcoSquare.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\TelcoSquare.jpg");

			WebUtility.Wait();
			passport.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\PassportPNG.png");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
        
		try {
			js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
			WebUtility.Wait();
			WebUtility.Click(imageSetActiveButton);
			WebUtility.Wait();
			WebUtility.Click(dropDownList.get(0));
			WebUtility.javascript(yes.get(0));
			WebUtility.Wait();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		//------------------------ Delete Image Set -----------------------------
		try {
			removeSet.get(1).click();
			WebUtility.Wait();
			yes.get(0).click();
			WebUtility.Wait();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
		sa.assertTrue(doneButtonActiveColor.contains("255, 255, 255") || doneButtonActiveColor.contains("255,255,255"), "Seasons Main Content Images Mark As Done Assertion Fails");
		WebUtility.Click(doneButtonActive);

		js.executeScript("window.scrollTo(0,0);");
		WebUtility.Wait();
		WebUtility.Click(imageSets.get(0));
		try {
			TestUtil.captureScreenshot("Season Main Content Images Section Default Set","Season");
		} catch (IOException e) {
			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}

		js.executeScript("window.scrollTo(0,document.body.scrollHeight);");

		try {
			TestUtil.captureScreenshot("Season Main Content Images Section Default Set 2","Season");
		} catch (IOException e) {
			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}

		WebUtility.Click(imageSets.get(1));
		js.executeScript("window.scrollTo(0,0);");
		try {
			TestUtil.captureScreenshot("Season Main Content Images Section Created Set","Season");
		} catch (IOException e) {
			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}
		js.executeScript("window.scrollTo(0,document.body.scrollHeight);");

		try {
			TestUtil.captureScreenshot("Season Main Content Images Section Created Set 2","Season");
		} catch (IOException e) {
			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}


	}
	
    public void SeasonImagesAssertions() throws InterruptedException {
		WebUtility.Click(images);
		WebUtility.Wait();
		WebUtility.Click(imageSets.get(0));
		String coverName = "CoverPNG", appCoverName = "AppCoverPNG", listName = "List", squareName = "Square",
				tvCoverName = "TVCover", portraitName = "PortraitPNG", listCleanName = "ListClean",
				portraitCleanName = "PortraitCleanPNG", telcoSquareName = "TelcoSquare", passportName = "Passport";
		boolean coverFound = false, appCoverFound = false, listFound = false, squareFound = false, tvCoverFound = false,
				portraitFound = false, listCleanFound = false, portraitCleanFound = false, telcoSquareFound = false,
				passportFound = false;
		List<WebElement> imageCheck = driver.findElements(By.tagName("strong"));

		for (int i = 0; i < imageCheck.size(); i++) {
			if (imageCheck.get(i).getText().equals(coverName.toLowerCase()))
				coverFound = true;

			if (imageCheck.get(i).getText().equals(appCoverName.toLowerCase()))
				appCoverFound = true;

			if (imageCheck.get(i).getText().equals(listName.toLowerCase()))
				listFound = true;

			if (imageCheck.get(i).getText().equals(squareName.toLowerCase()))
				squareFound = true;

			if (imageCheck.get(i).getText().equals(tvCoverName.toLowerCase()))
				tvCoverFound = true;

			if (imageCheck.get(i).getText().equals(portraitName.toLowerCase()))
				portraitFound = true;

			if (imageCheck.get(i).getText().equals(listCleanName.toLowerCase()))
				listCleanFound = true;

			if (imageCheck.get(i).getText().equals(portraitCleanName.toLowerCase()))
				portraitCleanFound = true;

			if (imageCheck.get(i).getText().equals(telcoSquareName.toLowerCase()))
				telcoSquareFound = true;

			if (imageCheck.get(i).getText().equals(passportName.toLowerCase()))
				passportFound = true;

    }

    Assert.assertEquals(coverFound, true, "Seasons Main Content - Image Cover Assertion Failed");
    Assert.assertEquals(appCoverFound, true, "Seasons Main Content - Image App Cover Assertion Failed");
    Assert.assertEquals(listFound, true, "Seasons Main Content - Image List Assertion Failed");
    Assert.assertEquals(squareFound, true, "Seasons Main Content - Image Square Assertion Failed");
    Assert.assertEquals(tvCoverFound, true, "Seasons Main Content - Image TV Cover Assertion Failed");
    Assert.assertEquals(portraitFound, true, "Seasons Main Content - Image Portrait Assertion Failed");
    Assert.assertEquals(listCleanFound, true, "Seasons Main Content - Image List CLean Assertion Failed");
    Assert.assertEquals(portraitCleanFound, true, "Seasons Main Content - Image Portrait Clean Assertion Failed");
    Assert.assertEquals(telcoSquareFound, true, "Seasons Main Content - Image Telco Square Assertion Failed");
    Assert.assertEquals(passportFound, true, "Seasons Main Content - Image Passport Assertion Failed");
    
    String doneButtonClickedColor = doneButtonClicked.getCssValue("background-color");
    Assert.assertTrue(doneButtonClickedColor.contains("52, 194, 143"), "Season Main Content - Images Mark As Done Assertion Fails");
    }
    
	public void SeasonSEODetails() throws InterruptedException, Exception{
	/*
		dashboard.click();
		tvShowButton.click();
		Thread.sleep(2000);

		searchString.click();
		searchString.sendKeys("The Expanse");
		Thread.sleep(2000);
		WebUtility.element_to_be_clickable(editButton.get(0));
		editButton.get(0).click();
		Thread.sleep(2000);

		seasons.click();
		WebUtility.element_to_be_clickable(createSeason);
		createSeason.click();
		Thread.sleep(2000);
		createSeasonMainSeason.click();
		yes.get(0).click();
	*/	
		WebUtility.Click(seoDetails);
    	WebUtility.Wait();

	
    	editSeoTitleTag.click();
		seoTitleTag.click();
		seoTitleTag.sendKeys(Keys.CONTROL + "a");
		seoTitleTag.sendKeys(Keys.BACK_SPACE);
		seoTitleTag.sendKeys("Automation Test Data Season");
		editSeoTitleTag.click();
		Thread.sleep(1000);

		
		editSeoMetaDescription.click();
		seoMetaDescription.click();
		seoMetaDescription.sendKeys(Keys.CONTROL + "a");
		seoMetaDescription.sendKeys(Keys.BACK_SPACE);
		seoMetaDescription.sendKeys("Automation Test Data Season");
		editSeoMetaDescription.click();
		Thread.sleep(1000);

		editSeoMetaSynopsis.click();
		seoMetaSynopsis.click();
		seoMetaSynopsis.sendKeys(Keys.CONTROL + "a");
		seoMetaSynopsis.sendKeys(Keys.BACK_SPACE);
		seoMetaSynopsis.sendKeys("Automation Test Data Season");
		editSeoMetaSynopsis.click();
		Thread.sleep(1000);

		editSeoRedirectionType.click();
		seoRedirectionType.click();
		dropDownList.get(0).click();
		WebUtility.Wait();
		editSeoRedirectionType.click();
		Thread.sleep(1000);

		editSeoRedirectionLink.click();
		seoRedirectionLink.click();
		seoRedirectionLink.sendKeys(Keys.CONTROL + "a");
		seoRedirectionLink.sendKeys(Keys.BACK_SPACE);
		seoRedirectionLink.sendKeys("https://www.kelltontech.com/Season");
		editSeoRedirectionLink.click();
		Thread.sleep(1000);

		
		editSeoNoIndex.click();
		WebUtility.Wait();
		editSeoNoIndex.click();
		Thread.sleep(1000);

		editSeoH1Heading.click();
		seoH1Heading.click();
		seoH1Heading.sendKeys(Keys.CONTROL + "a");
		seoH1Heading.sendKeys(Keys.BACK_SPACE);
		seoH1Heading.sendKeys("Automation Test Data Season");
		editSeoH1Heading.click();
		Thread.sleep(1000);

		editSeoH2Heading.click();
		seoH2Heading.click();
		seoH2Heading.sendKeys(Keys.CONTROL + "a");
		seoH2Heading.sendKeys(Keys.BACK_SPACE);
		seoH2Heading.sendKeys("Automation Test Data Season");
		editSeoH2Heading.click();
		Thread.sleep(1000);

		editSeoH3Heading.click();
		seoH3Heading.click();
		seoH3Heading.sendKeys(Keys.CONTROL + "a");
		seoH3Heading.sendKeys(Keys.BACK_SPACE);
		seoH3Heading.sendKeys("Automation Test Data Season");
		editSeoH3Heading.click();
		Thread.sleep(1000);

		editSeoH4Heading.click();
		seoH4Heading.click();
		seoH4Heading.sendKeys(Keys.CONTROL + "a");
		seoH4Heading.sendKeys(Keys.BACK_SPACE);
		seoH4Heading.sendKeys("Automation Test Data Season");
		editSeoH4Heading.click();
		Thread.sleep(1000);

		editSeoH5Heading.click();
		seoH5Heading.click();
		seoH5Heading.sendKeys(Keys.CONTROL + "a");
		seoH5Heading.sendKeys(Keys.BACK_SPACE);
		seoH5Heading.sendKeys("Automation Test Data Season");
		editSeoH5Heading.click();
		Thread.sleep(1000);

		editSeoH6Heading.click();
		seoH6Heading.click();
		seoH6Heading.sendKeys(Keys.CONTROL + "a");
		seoH6Heading.sendKeys(Keys.BACK_SPACE);
		seoH6Heading.sendKeys("Automation Test Data Season");
		editSeoH6Heading.click();
		Thread.sleep(1000);

		editSeoRobotMetaIndex.click();
		seoRobotsMetaIndex.get(0).click();
		seoRobotsMetaIndex.get(0).sendKeys(Keys.CONTROL + "a");
		seoRobotsMetaIndex.get(0).sendKeys(Keys.BACK_SPACE);
		seoRobotsMetaIndex.get(0).sendKeys("Automation Test Data Season");
		editSeoRobotMetaIndex.click();
		Thread.sleep(1000);

		editSeoRobotMetaNoIndex.click();
		seoRobotsMetaIndex.get(0).click();
		seoRobotsMetaIndex.get(0).sendKeys(Keys.CONTROL + "a");
		seoRobotsMetaIndex.get(0).sendKeys(Keys.BACK_SPACE);
		seoRobotsMetaIndex.get(0).sendKeys("Automation Test Data Season");
		editSeoRobotMetaNoIndex.click();
		Thread.sleep(1000);
		
		js.executeScript("window.scrollTo(0,document.body.scrollHeight);");

		editSeoRobotMetaImageIndex.click();
		seoRobotsMetaImageIndex.click();
		seoRobotsMetaImageIndex.sendKeys(Keys.CONTROL + "a");
		seoRobotsMetaImageIndex.sendKeys(Keys.BACK_SPACE);
		seoRobotsMetaImageIndex.sendKeys("Automation Test Data Season");
		editSeoRobotMetaImageIndex.click();
		Thread.sleep(1000);

		editSeoRobotMetaImageNoIndex.click();
		seoRobotsMetaImageNoIndex.click();
		seoRobotsMetaImageNoIndex.sendKeys(Keys.CONTROL + "a");
		seoRobotsMetaImageNoIndex.sendKeys(Keys.BACK_SPACE);
		seoRobotsMetaImageNoIndex.sendKeys("Automation Test Data Season");
		editSeoRobotMetaImageNoIndex.click();
		Thread.sleep(1000);

		editSeoBreadcrumbTitle.click();
		seoBreadcrumbTitle.click();
		seoBreadcrumbTitle.sendKeys(Keys.CONTROL + "a");
		seoBreadcrumbTitle.sendKeys(Keys.BACK_SPACE);
		seoBreadcrumbTitle.sendKeys("Automation Test Data Season");
		editSeoBreadcrumbTitle.click();
		Thread.sleep(1000);

		editSeoInternalLinkBuilding.click();
		seoInternalLinkBuilding.click();
		seoInternalLinkBuilding.sendKeys(Keys.CONTROL + "a");
		seoInternalLinkBuilding.sendKeys(Keys.BACK_SPACE);
		seoInternalLinkBuilding.sendKeys("Automation Test Data Season");
		editSeoInternalLinkBuilding.click();
		Thread.sleep(1000);
		
		editSeoVideoObjectSchema.click();
		editSeoVideoObjectSchema.click();
		Thread.sleep(1000);

		editSeoWebsiteSchema.click();
		editSeoWebsiteSchema.click();
		Thread.sleep(1000);

		editSeoBreadcrumbListSchema.click();
		editSeoBreadcrumbListSchema.click();
		Thread.sleep(1000);

		editSeoImageObjectSchema.click();
		editSeoImageObjectSchema.click();
		Thread.sleep(1000);

		editSeoOrganizationSchema.click();
		editSeoOrganizationSchema.click();
		Thread.sleep(1000);

		editSeoImageGallerySchema.click();
		editSeoImageGallerySchema.click();
		WebUtility.Wait();

		editSeoLinkToStories.click();
		editSeoLinkToStories.click();
		WebUtility.Wait();
		
		String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
		sa.assertTrue(doneButtonActiveColor.contains("255, 255, 255") || doneButtonActiveColor.contains("255,255,255"), "Seasons Main Content SEO Details Mark As Done Assertion Fails");
		WebUtility.Click(doneButtonActive);
	
	}
	
	
	public void SeasonsPublishFlow() throws InterruptedException, Exception{
		
		WebUtility.Wait();
		//--Verifying Season Draft Status
		try
		{
			seasonProperties.click();
			WebUtility.element_to_be_visible(contentStatus);
			TestUtil.captureScreenshot("Season Content Draft Status","Season");
			String expectedStatus = "Draft";
			String actualStatus = contentStatus.getText();
			sa.assertEquals(actualStatus,expectedStatus,"Season Draft Status is Failed");
			Reporter.log("Season Draft Status is Passed",true);	
		}
		catch (Exception e)
		{
			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}
		
		WebUtility.Click(checklist);
		WebUtility.Wait();
		js.executeScript("window.scrollBy(0,600)");
		scheduledCountry.get(0).click();
		WebUtility.Click(dropDownList.get(0));
		WebUtility.Click(publishContent);
		WebUtility.Click(yes.get(0));
		WebUtility.Click(ok);   
        js.executeScript("window.scrollTo(0,0)");  
     
	 //--Verifying Season Published Status
		try
		{
			seasonProperties.click();
			WebUtility.element_to_be_visible(contentStatus);
			TestUtil.captureScreenshot("Season Content Published Status","Season");
			String expectedStatus = "Published";
			String actualStatus = contentStatus.getText();
			sa.assertEquals(actualStatus,expectedStatus,"Season Content Published Status is Failed");
			Reporter.log("Season Content Published Status is Passed",true);	
		}
		catch (Exception e){
			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}
        		
        //----------------Republish -----------------------------------------------------------------------------
	       
        try {
	        license.click();
	        WebUtility.Wait();
	       	
	        WebUtility.Click(licenseStatusButton);
	        WebUtility.Click(inactivateLicenseDropdown);
	        WebUtility.Wait();
	        actionProvider.moveToElement(inactivateLicenseReason).click().build().perform();
	        WebUtility.Click(dropDownList.get(0));
	        WebUtility.Click(yes.get(0));
	        WebUtility.Click(doneButtonActive);
       }
       catch(Exception e)
       {
    	   try
    	   {
    	   WebUtility.Click(no);
    	   }
    	   catch(Exception e1) {
    		 e1.printStackTrace();
    	   }
    	   
  	        WebUtility.Click(seasonProperties);
			WebUtility.Click(seoDetails);
			editSeoTitleTag.click();
			Thread.sleep(1000);
			seoTitleTag.click();
			seoTitleTag.sendKeys(Keys.CONTROL + "a");
			seoTitleTag.sendKeys(Keys.BACK_SPACE);
			Thread.sleep(2000);
			seoTitleTag.sendKeys("Automation Test Data Season Main Content Edit");
			Thread.sleep(1000);
			editSeoTitleTag.click();
			Thread.sleep(1000);
			WebUtility.Click(doneButtonActive);
			e.printStackTrace();
           
       }
        
        WebUtility.Click(checklist);
        js.executeScript("window.scrollTo(0,document.body.scrollHeight)");
        WebUtility.Click(republishContent);
        WebUtility.Click(yes.get(0));
        WebUtility.Click(ok);
        js.executeScript("window.scrollTo(0,0)");
		WebUtility.Wait();
		
		//---------------------Verifying Season Republished status-------------------------------
		try
		{
				WebUtility.Click(seasonProperties);
				WebUtility.element_to_be_visible(contentStatus);
				TestUtil.captureScreenshot("Season Main Content Republished Status","TV Show");
				String expectedStatus = "Published";
				String actualStatus = contentStatus.getText();
				sa.assertEquals(actualStatus,expectedStatus,"Season Main Content Republished Status is Failed");
		}
		catch (Exception e)
		{
				e.printStackTrace();
		}
		
		//-----------------------Unpublish -----------------------------------
		WebUtility.Click(checklist);
		WebUtility.Wait();
        js.executeScript("window.scrollTo(0,document.body.scrollHeight);");      
        WebUtility.Click(unpublishContent);
        WebUtility.Click(unpublishReason);
        WebUtility.Click(dropDownList.get(0));
    	WebUtility.Click(yes.get(0));
		WebUtility.Click(ok);
        
        js.executeScript("window.scrollTo(0,0)");
        
      //-----------------Verifying Season Unpublished Status----------------------
        try {
        	seasonProperties.click();
			WebUtility.element_to_be_visible(contentStatus);
			TestUtil.captureScreenshot("Season Content Unpublished Status","Season");
			String expectedStatus = "Unpublished";
			String actualStatus = contentStatus.getText();
			sa.assertEquals(actualStatus,expectedStatus,"Season Content Unpublished Status is Failed");
			Reporter.log("Season Content Unpublished Status is Passed",true);	
        } catch (IOException e) {
            System.out.println("Screenshot Capture Failed ->" + e.getMessage());
        }    
        
        
        // ------------- Schedule Content -------------------------------------
    	WebUtility.Click(checklist);
    	js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
        WebUtility.Click(scheduleContent);
        
       // System.out.println("--------> Current Time = "+currentTime.getAttribute("innerText").substring(20, 28));
       // String currentTimeString  = currentTime.getAttribute("innerText").substring(20, 27);
      // js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
 
        WebUtility.Click(scheduledPublicationTime);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        WebUtility.Click(datePickerMinutes);
        actionProvider.moveToElement(datePickerMinutesClock).click().build().perform();      
        datePicker.sendKeys(Keys.ESCAPE);		  

        WebUtility.Click(scheduledContentCountry);
        WebUtility.Click(dropDownList.get(0));

        WebUtility.Click(scheduleContentButton);
        WebUtility.Click(yes.get(0));
        WebUtility.Wait();
        WebUtility.Click(ok);

    	js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
        WebUtility.Wait();
        js.executeScript("window.scrollTo(0,0);");
        WebUtility.Wait();
        
     
        //System.out.println("--------> Scheduled Time = "+scheduledTime.getAttribute("innerText").substring(12, 20));
        //String scheduledTimeString = scheduledTime.getAttribute("innerText").substring(12, 20);
    	//js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
    	//sa.assertTrue(scheduledTimeString.contains(currentTimeString),"Season Main Content - Current Time and Scheduled Time are not Equal");

      //-------------------Verifying Season Scheduled Status---------------------------------
        try {
        	seasonProperties.click();
			WebUtility.Wait();
			WebUtility.element_to_be_visible(contentStatus);
            TestUtil.captureScreenshot("Season Main Content Scheduled Status","TV Show");
			String expectedStatus = "Scheduled";
			String actualStatus = contentStatus.getText();
			sa.assertEquals(actualStatus,expectedStatus,"Season Main Content Scheduled Status is Failed");
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        WebUtility.Wait();
        WebUtility.Click(checklist);
        WebUtility.Click(deleteSchedule);
        WebUtility.Click(yes.get(0));
        WebUtility.Click(ok);
        
        WebUtility.Wait();
        js.executeScript("window.scrollTo(0,document.body.scrollHeight)");
        WebUtility.Click(scheduledCountry.get(0));
        WebUtility.Click(dropDownList.get(0));

        WebUtility.Click(publishContent);
        WebUtility.Click(yes.get(0));
        WebUtility.Click(ok); 	
        js.executeScript("window.scrollTo(0,0)");
        
        publishedHistory.click();
        WebUtility.Wait();
        try {
            TestUtil.captureScreenshot("Season Main Content - Published History","Season");
        } catch (IOException e) {
            System.out.println("Screenshot Capture Failed ->" + e.getMessage());
        }
        
		seasonsBreadcrumb.click();
        Thread.sleep(2000);
	}
	
	public void SeasonsRelatedContent() throws InterruptedException, Exception{
	
		WebUtility.Click(relatedContent);
		WebUtility.Wait();
		WebUtility.Click(relatedContentAssignTvShows);

		WebUtility.element_to_be_visible(relatedContentAddTvShows);
		WebUtility.Click(relatedContentAddTvShows);

		WebUtility.Wait();
		checkBox.get(0).click();
		checkBox.get(1).click();
		WebUtility.Click(assignRelatedContent);
		WebUtility.Click(deleteRelatedContent);
		WebUtility.Wait();
		WebUtility.Click(yes.get(0));
		WebUtility.Wait();

		WebUtility.Click(relatedContentAssignVideos);
		WebUtility.element_to_be_visible(relatedContentAddVideos);
		WebUtility.Click(relatedContentAddVideos);

		WebUtility.Wait();
		checkBox.get(0).click();
		checkBox.get(1).click();
		WebUtility.Click(assignRelatedContent);
		WebUtility.Click(deleteRelatedContent);
		WebUtility.Wait();
		WebUtility.Click(yes.get(0));
		WebUtility.Wait();

		WebUtility.Click(relatedContentAssignMovies);
		WebUtility.element_to_be_visible(relatedContentAddMovies);
		WebUtility.Click(relatedContentAddMovies);

		WebUtility.Wait();
		checkBox.get(0).click();
		checkBox.get(1).click();
		WebUtility.Click(assignRelatedContent);
		WebUtility.Click(deleteRelatedContent);
		WebUtility.Wait();
		WebUtility.Click(yes.get(0));
		WebUtility.Wait();

		WebUtility.Click(relatedContentAssignSeasons);
		WebUtility.element_to_be_visible(relatedContentAddSeasons);
		WebUtility.Click(relatedContentAddSeasons);

		WebUtility.Wait();
		checkBox.get(0).click();
		checkBox.get(1).click();
		WebUtility.Click(assignRelatedContent);
		WebUtility.Click(deleteRelatedContent);
		WebUtility.Wait();
		WebUtility.Click(yes.get(0));
		WebUtility.Wait();

		WebUtility.Click(relatedContentAssignEpisodes);
		WebUtility.element_to_be_visible(relatedContentAddEpisodes);
		WebUtility.Click(relatedContentAddEpisodes);

		WebUtility.Wait();
		checkBox.get(0).click();
		checkBox.get(1).click();
		WebUtility.Click(assignRelatedContent);
		WebUtility.Click(deleteRelatedContent);
		WebUtility.Wait();
		WebUtility.Click(yes.get(0));
		WebUtility.Wait();

		String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
		sa.assertTrue(doneButtonActiveColor.contains("255, 255, 255") || doneButtonActiveColor.contains("255,255,255"), "Seasons Main Content Related Content Mark As Done Assertion Fails");
		WebUtility.Click(doneButtonActive);

		WebUtility.Click(updateSeasonBreadcrumb);

	}
	
	
    public void SeasonsTranslations() throws InterruptedException {	   
	 	
    	JavascriptExecutor js = (JavascriptExecutor) driver;
    	WebUtility.Click(translations);
        js.executeScript("window.scrollTo(0,0);");
    	WebUtility.Wait();
    	
    	WebUtility.Click(editTranslation.get(0));
    	WebUtility.Click(title);
        title.sendKeys(Keys.CONTROL + "a");
        title.sendKeys(Keys.BACK_SPACE);
        title.sendKeys("फैलाव");
        saveTranslation.click();
        Thread.sleep(1000);

        WebUtility.Click(editTranslation.get(1));
        WebUtility.Click(translationsShortDesc);
        js.executeScript("arguments[0].focus()", translationsShortDesc);
        translationsShortDesc.sendKeys(Keys.CONTROL + "a");
        translationsShortDesc.sendKeys(Keys.BACK_SPACE);
        translationsShortDesc.sendKeys("जेम्स एस ए कोरी द्वारा इसी नाम के उपन्यासों की श्रृंखला के आधार पर मार्क फर्गस और हॉक ओस्बी द्वारा विकसित एक अमेरिकी विज्ञान कथा टेलीविजन श्रृंखला है।");
        saveTranslation.click();
        Thread.sleep(1000);
        
        WebUtility.Click(editTranslation.get(2));
        WebUtility.Click(translationsWebDesc);
        translationsWebDesc.sendKeys(Keys.CONTROL + "a");
        translationsWebDesc.sendKeys(Keys.BACK_SPACE);
        translationsWebDesc.sendKeys("स्वचालन परीक्षण");
        saveTranslation.click();
        Thread.sleep(1000);

        WebUtility.Click(editTranslation.get(3));
        WebUtility.Click(translationsAppDesc);
        translationsAppDesc.sendKeys(Keys.CONTROL + "a");
        translationsAppDesc.sendKeys(Keys.BACK_SPACE);
        translationsAppDesc.sendKeys("स्वचालन परीक्षण");
        saveTranslation.click();
        Thread.sleep(1000);

        WebUtility.Click(editTranslation.get(4));
        WebUtility.Click(translationsTVDesc);
        translationsTVDesc.sendKeys(Keys.CONTROL + "a");
        translationsTVDesc.sendKeys(Keys.BACK_SPACE);
        translationsTVDesc.sendKeys("स्वचालन परीक्षण");
        saveTranslation.click();
        Thread.sleep(1000);

        WebUtility.Click(editTranslation.get(5));
        WebUtility.Click(pageTitle);
        pageTitle.sendKeys(Keys.CONTROL + "a");
        pageTitle.sendKeys(Keys.BACK_SPACE);
        pageTitle.sendKeys("स्वचालन रचनात्मक शीर्षक");
        saveTranslation.click();
        Thread.sleep(1000);

        WebUtility.Click(editTranslation.get(6));
        WebUtility.Click(pageDescription);
        pageDescription.sendKeys(Keys.CONTROL + "a");
        pageDescription.sendKeys(Keys.BACK_SPACE);
        pageDescription.sendKeys("स्वचालन पृष्ठ विवरण");
        saveTranslation.click();
        Thread.sleep(1000);
        
        WebUtility.Click(editTranslation.get(7));
        WebUtility.Click(translationsTitleForSocialShare);
        translationsTitleForSocialShare.sendKeys(Keys.CONTROL + "a");
        translationsTitleForSocialShare.sendKeys(Keys.BACK_SPACE);
        translationsTitleForSocialShare.sendKeys("सामाजिक हिस्सेदारी के लिए स्वचालन शीर्षक");
        saveTranslation.click();
        Thread.sleep(1000);

        WebUtility.Click(editTranslation.get(8));
        WebUtility.Click(translationsDescriptionForSocialShare);
        translationsDescriptionForSocialShare.sendKeys(Keys.CONTROL + "a");
        translationsDescriptionForSocialShare.sendKeys(Keys.BACK_SPACE);
        translationsDescriptionForSocialShare.sendKeys("सामाजिक शेयर के लिए स्वचालन विवरण");
        saveTranslation.click();
        Thread.sleep(1000);

        WebUtility.Click(editTranslation.get(9));
        WebUtility.Click(translationsUpcomingPage);
        translationsUpcomingPage.sendKeys(Keys.CONTROL + "a");
        translationsUpcomingPage.sendKeys(Keys.BACK_SPACE);
        translationsUpcomingPage.sendKeys("ऑटोमेशन आगामी पृष्ठ पाठ");
        saveTranslation.click();
        Thread.sleep(1000);
        
        js.executeScript("window.scrollTo(0,0);");
        WebUtility.Click(doneButtonActive);
        WebUtility.Wait();

        castCrew.click();        
        
        WebUtility.Click(editTranslationActor);
        WebUtility.Click(translationsActor);
        translationsActor.sendKeys(Keys.CONTROL + "a");
        translationsActor.sendKeys(Keys.BACK_SPACE);
        translationsActor.sendKeys("ऑटोमेशन आगामी पृष्ठ पाठ");
        saveTranslation.click();
        Thread.sleep(1000);

        WebUtility.Click(editTranslationCharacter);
        WebUtility.Click(translationsCharacter);
        translationsCharacter.sendKeys(Keys.CONTROL + "a");
        translationsCharacter.sendKeys(Keys.BACK_SPACE);
        translationsCharacter.sendKeys("ऑटोमेशन आगामी पृष्ठ पाठ");
        saveTranslation.click();
        Thread.sleep(1000);

		String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
		sa.assertTrue(doneButtonActiveColor.contains("255, 255, 255") || doneButtonActiveColor.contains("255,255,255"), "Seasons Main Content Translations Mark As Done Assertion Fails");
        WebUtility.Click(doneButtonActive);
        WebUtility.Wait();

        WebUtility.Click(updateSeasonBreadcrumb);
        WebUtility.Wait();

    }		

	//------------------------------------------- Seasons Quick Filing ---------------------------------------------------------
	public void SeasonQuickfilingProperties() throws InterruptedException {
		WebUtility.Click(dashboard);
		WebUtility.Click(tvShowButton);

		searchString.click();
		searchString.sendKeys(tvShowQuickFilingTitle);
		WebUtility.Wait();
		WebUtility.Wait();
		WebUtility.element_to_be_clickable(editButton.get(0));
		editButton.get(0).click(); 
  		
  		WebUtility.Wait();
  		WebUtility.Click(seasons);
  		WebUtility.Click(createSeason);
  		createSeasonQuickFiling.click();
		yes.get(0).click();

		WebUtility.Wait();
  		WebUtility.Click(quickeditTitle);
  		title.sendKeys(Keys.CONTROL + "a");
        title.sendKeys(Keys.BACK_SPACE);
        title.sendKeys(tvShowQuickFilingTitle);
        WebUtility.Click(quickeditTitle);
        WebUtility.Wait();   

        WebUtility.Click(quickeditShortDesc);
        WebUtility.Click(shortDesc);
        shortDesc.sendKeys(Keys.CONTROL + "a");
        shortDesc.sendKeys(Keys.BACK_SPACE);
        shortDesc.sendKeys("The Expanse is an American science fiction television series developed by Mark Fergus and Hawk Ostby - Season Quick Filing");
        WebUtility.Click(quickeditShortDesc);
        WebUtility.Wait(); 
    
	/*	
		quickeditIndex.click();
		WebUtility.element_to_be_clickable(index);
		index.click();
		index.sendKeys(Keys.CONTROL + "a");
		index.sendKeys(Keys.BACK_SPACE);
		index.sendKeys("44");
		quickeditIndex.click();
		WebUtility.Wait(); 
	*/	
        
        WebUtility.Click(quickeditZee5ReleaseDate);
        WebUtility.Click(zee5ReleaseDate);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ESCAPE);
		WebUtility.Click(quickeditZee5ReleaseDate);
		WebUtility.Wait();
		
        WebUtility.Click(editQuickFillingOriginalReleaseDate);
		WebUtility.Click(telecastDate);
        datePicker.sendKeys(Keys.ARROW_LEFT);
        datePicker.sendKeys(Keys.ARROW_LEFT);
        datePicker.sendKeys(Keys.ESCAPE);
		WebUtility.Click(editQuickFillingOriginalReleaseDate);
		WebUtility.Wait(); 
	
		quickFilingEditMultiAudio.click();
		if(!multiAudio.isSelected()) {
			multiAudio.click();
			quickFilingEditMultiAudio.click();
		}
  	    else
  	    	quickFilingEditMultiAudio.click();
  	    WebUtility.Wait();
		
		
		WebUtility.Click(quickeditAudio);
		audioLanguage.click();
		audioLanguage.sendKeys(Keys.BACK_SPACE);
		WebUtility.Click(dropDownList.get(0));
		WebUtility.Click(quickeditAudio);
		WebUtility.Wait();
		
		nextButton.click();
		
		WebUtility.Click(editQuickFillingCertification);
		certification.click();
		certification.sendKeys(Keys.BACK_SPACE);
		WebUtility.Click(dropDownList.get(0));
		WebUtility.Click(editQuickFillingCertification);
		WebUtility.Wait();

		nextButton.click();
		WebUtility.Wait();
		String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
		sa.assertTrue(doneButtonActiveColor.contains("255, 255, 255") || doneButtonActiveColor.contains("255,255,255"), "Seasons Quick Filing - Properties Mark As Done Assertion Fails");
		WebUtility.Click(doneButtonActive);
		WebUtility.Wait();
		try {
			TestUtil.captureScreenshot("Season Quick Filing - Properties Page","Season");
		} catch (IOException e) {
			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}

	}
	
	public void SeasonQuickfilingLicense() throws InterruptedException, Exception{
		WebUtility.Click(license);
		WebUtility.Click(createLicense);

		WebUtility.Click(licenseSetName);
        licenseSetName.sendKeys("Automation Test Data");
        
        WebUtility.Click(licenseFromDate);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ESCAPE);

        WebUtility.Click(licenseToDate);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ESCAPE);	
		
        WebUtility.Click(licenseCountry);
        for (int x = 0; x < dropDownList.size(); x++) {
        	if(x<4) {
            WebUtility.Click(dropDownList.get(x));
        	}
        	else {
        		break;
        	}
        }
		
		
		WebUtility.Click(licenseBusinessType);
		WebUtility.Click(dropDownList.get(0));

		WebUtility.Wait();
		driver.findElement(By.xpath("//span[contains(text(),'SAVE')]")).click();
		
		WebUtility.Click(license);
		WebUtility.Click(createLicense);
		
		WebUtility.Click(licenseSetName);
        licenseSetName.sendKeys("Automation Test Data II");

        WebUtility.Click(licenseFromDate);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ESCAPE);

        WebUtility.Click(licenseToDate);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ESCAPE);
			
        WebUtility.Click(licenseCountry);
        for (int x = 0; x < dropDownList.size(); x++) {
        	if(x<4) {
            WebUtility.Click(dropDownList.get(x));
        	}
        	else {
        		break;
        	}
        }
		
		
		WebUtility.Click(licenseBusinessType);
		WebUtility.Click(dropDownList.get(0));
		WebUtility.Wait();
		driver.findElement(By.xpath("//span[contains(text(),'SAVE')]")).click();
		WebUtility.Wait();
  
		String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
		sa.assertTrue(doneButtonActiveColor.contains("255, 255, 255") || doneButtonActiveColor.contains("255,255,255"), "Seasons Quick Filing - License Mark As Done Assertion Fails");
		WebUtility.Click(doneButtonActive);		

        WebUtility.Click(expiredLicense);
        Thread.sleep(2000);
        WebUtility.Click(expiredLicenseBack);
        Thread.sleep(2000);

        WebUtility.Click(licenseFilters);
        WebUtility.Click(licenseFromDate);
        datePicker.sendKeys(Keys.ARROW_LEFT);
        datePicker.sendKeys(Keys.ARROW_LEFT);
        datePicker.sendKeys(Keys.ESCAPE);

        WebUtility.Click(licenseToDate);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ESCAPE);
        WebUtility.Click(applyFilter);

        Thread.sleep(2000);
        WebUtility.Click(licenseFilters);
        WebUtility.Click(clearFilter);
        
        WebUtility.Click(licenseFilters);
        WebUtility.Click(licenseFilterBusinessType);
        WebUtility.Click(dropDownList.get(0));
        WebUtility.Wait();
        WebUtility.Click(applyFilter);

        Thread.sleep(2000);
        WebUtility.Click(licenseFilters);
        WebUtility.Click(clearFilter);
	    
        WebUtility.Click(licenseFilters);
        WebUtility.Wait();
        licenseFilterActiveStatus.click();
        WebUtility.Wait();
        WebUtility.Click(applyFilter);

        Thread.sleep(2000);
        WebUtility.Click(licenseFilters);
        WebUtility.Click(clearFilter);

        WebUtility.Click(licenseFilters);
        WebUtility.Wait();
        licenseFilterInActiveStatus.click();
        WebUtility.Wait();
        WebUtility.Click(applyFilter);

        Thread.sleep(2000);
        WebUtility.Click(licenseFilters);
        WebUtility.Click(clearFilter);

	}
	
	public void SeasonQuickfilingImages() throws InterruptedException {
		//================Edit Flow for Testing ====================
		/*
		WebUtility.Click(dashboard);
		WebUtility.Click(tvShowButton);

		searchString.click();
		searchString.sendKeys(tvShowQuickFilingTitle);
		WebUtility.Wait();
		WebUtility.Wait();
		WebUtility.element_to_be_clickable(editButton.get(0));
		editButton.get(0).click(); 
  		
  		WebUtility.Wait();
  		WebUtility.Click(seasons);
  		WebUtility.Wait();
  		WebUtility.Click(editButton.get(0));
  		WebUtility.Wait();
  		WebUtility.Click(driver.findElement(By.xpath("//span[contains(text(),'Edit Season')]")));
		createSeasonQuickFiling.click();
		WebUtility.Click(yes.get(0));
  		*/
  		
		WebUtility.Click(images);
		WebUtility.Wait();
		try {
			if (removeSet.get(0).isDisplayed() == true)
				;
			{
				for (int x = 0; x < removeSet.size(); x++) {
					removeSet.get(x).click();
					WebUtility.Wait();
					yes.get(0).click();
					WebUtility.Wait();
				}
			}
		} catch (Exception e) {
			System.out.println("No Image Sets Exist");
		}
		WebUtility.Click(imageSets.get(0));
		WebUtility.Click(threedot);
		WebUtility.Wait();
		editImage.click();
		WebUtility.Wait();
		editQuickFilingImage.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\CoverPNG.png");
		WebUtility.Wait();
		WebUtility.javascript(SaveImage);
		WebUtility.Wait();

		// -----------------------------------------------------Image Set 2
		// ---------------------------------------------------------------------
		try {
			WebUtility.Click(imagesCreateNewSet);

			WebUtility.Click(newSetName);
			newSetName.sendKeys("Automation Test Data");

			newSetCountry.click();
			WebUtility.Click(dropDownList.get(0));
			closeDropdown.click();

			newSetPlatform.click();
			WebUtility.Click(dropDownList.get(0));
			closeDropdown.click();

			newSetGender.click();
			WebUtility.Click(dropDownList.get(0));
			closeDropdown.click();

			newSetGenre.click();
			WebUtility.Click(dropDownList.get(0));
			closeDropdown.click();

			newSetAgeGroup.click();
			WebUtility.Click(dropDownList.get(0));
			closeDropdown.click();

			newSetLanguage.click();
			WebUtility.Click(dropDownList.get(0));
			closeDropdown.click();

			newSetOthers.click();
			newSetOthers.sendKeys("Automation Test Data");

			WebUtility.Click(save);

			WebUtility.Click(imageSets.get(1));

			js.executeScript("window.scrollTo(0,0)");
			Thread.sleep(2000);
			WebUtility.Wait();
			list.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\List.jpg");
			WebUtility.Wait();
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			// -------------------------- Image Set 3
			// -----------------------------------------------------
			WebUtility.element_to_be_fluentwait(imagesCreateNewSet);
			WebUtility.Click(imagesCreateNewSet);

			WebUtility.Click(newSetName);
			newSetName.sendKeys("Automation Test Data II");

			newSetCountry.click();
			WebUtility.Click(dropDownList.get(1));
			closeDropdown.click();

			newSetPlatform.click();
			WebUtility.Click(dropDownList.get(1));
			closeDropdown.click();

			newSetGender.click();
			WebUtility.Click(dropDownList.get(1));
			closeDropdown.click();

			newSetGenre.click();
			WebUtility.Click(dropDownList.get(1));
			closeDropdown.click();

			newSetAgeGroup.click();
			WebUtility.Click(dropDownList.get(1));
			closeDropdown.click();

			newSetLanguage.click();
			WebUtility.Click(dropDownList.get(1));
			closeDropdown.click();

			newSetOthers.click();
			newSetOthers.sendKeys("Automation Test Data II");

			WebUtility.Click(save);

			WebUtility.Click(imageSets.get(2));

			js.executeScript("window.scrollTo(0,0)");
			WebUtility.Wait();

			list.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\List.jpg");
			WebUtility.Wait();

		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
			WebUtility.Wait();
			WebUtility.Click(imageSetActiveButton);
			WebUtility.Wait();
			WebUtility.Click(dropDownList.get(0));
			WebUtility.javascript(yes.get(0));
			WebUtility.Wait();
		} catch (Exception e) {
			e.printStackTrace();
		}

		// ------------------------ Delete Image Set -----------------------------
		try {
			removeSet.get(1).click();
			WebUtility.Wait();
			yes.get(0).click();
			WebUtility.Wait();
		} catch (Exception e) {
			e.printStackTrace();
		}

		String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
		sa.assertTrue(doneButtonActiveColor.contains("255, 255, 255") || doneButtonActiveColor.contains("255,255,255"), "Seasons Quick Filing - Images Mark As Done Assertion Fails");
		WebUtility.Click(doneButtonActive);

	}
	public void SeasonQuickfillingSeo() throws InterruptedException {
		WebUtility.Click(seoDetails);
		WebUtility.Wait();
	
		WebUtility.Click(editSingleLandingSeoTitleTag);
  		seoTitleTag.click();
  		seoTitleTag.sendKeys(Keys.CONTROL + "a");
  		seoTitleTag.sendKeys(Keys.BACK_SPACE);
  		seoTitleTag.sendKeys("Automation Test Data Season Quick Filing");
  		editSingleLandingSeoTitleTag.click();
  		WebUtility.Wait();


  		WebUtility.Click(editSingleLandingSeoMetaDescription);
  		seoMetaDescription.click();
  		seoMetaDescription.sendKeys(Keys.CONTROL + "a");
  		seoMetaDescription.sendKeys(Keys.BACK_SPACE);
  		seoMetaDescription.sendKeys("Automation Test Data Season Quick Filing");
  		WebUtility.Click(editSingleLandingSeoMetaDescription);
  		WebUtility.Wait();

  		WebUtility.Click(editSingleLandingSeoMetaSynopsis);
  		seoMetaSynopsis.click();
  		seoMetaSynopsis.sendKeys(Keys.CONTROL + "a");
  		seoMetaSynopsis.sendKeys(Keys.BACK_SPACE);
  		seoMetaSynopsis.sendKeys("Automation Test Data Season Quick Filing");
  		WebUtility.Click(editSingleLandingSeoMetaSynopsis);
  		WebUtility.Wait();

  		WebUtility.Click(editSingleLandingH1Heading);
  		seoH1Heading.click();
  		seoH1Heading.sendKeys(Keys.CONTROL + "a");
  		seoH1Heading.sendKeys(Keys.BACK_SPACE);
  		seoH1Heading.sendKeys("Automation Test Data Season Quick Filing");
  		WebUtility.Click(editSingleLandingH1Heading);
  		WebUtility.Wait();

  		WebUtility.Click(editSingleLandingH2Heading);
  		seoH2Heading.click();
  		seoH2Heading.sendKeys(Keys.CONTROL + "a");
  		seoH2Heading.sendKeys(Keys.BACK_SPACE);
  		seoH2Heading.sendKeys("Automation Test Data Season Quick Filing");
  		WebUtility.Click(editSingleLandingH2Heading);
  		WebUtility.Wait();

  		editSingleLandingVideoObjectSchema.click();
  	    if(!seoVideoObjectSchema.isSelected())
  	    	seoVideoObjectSchema.click();
  	    else
  	    	editSingleLandingVideoObjectSchema.click();
  	    WebUtility.Wait();

  		editSingleLandingWebsiteSchema.click();
  	    if(!seoWebsiteSchema.isSelected())
  	    	seoWebsiteSchema.click();
  	    else
  	    	editSingleLandingWebsiteSchema.click();
  	    WebUtility.Wait();

  		editSingleLandingBreadcrumbListSchema.click();
  	    if(!seoBreadcrumbListSchema.isSelected())
  	    	seoBreadcrumbListSchema.click();
  	    else
  	    	editSingleLandingBreadcrumbListSchema.click();
  	    WebUtility.Wait();

  		editSingleLandingImageObjectSchema.click();
  	    if(!seoImageObjectSchema.isSelected())
  	    	seoImageObjectSchema.click();
  	    else
  	    	editSingleLandingImageObjectSchema.click();
  	    WebUtility.Wait();

  		editSingleLandingOrganizationSchema.click();
  	    if(!seoOrganizationSchema.isSelected())
  	    	seoOrganizationSchema.click();
  	    else
  	    	editSingleLandingOrganizationSchema.click();
  	    WebUtility.Wait();
  	    
		String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
		sa.assertTrue(doneButtonActiveColor.contains("255, 255, 255") || doneButtonActiveColor.contains("255,255,255"), "Seasons Quick Filing - SEO Details Mark As Done Assertion Fails");
		WebUtility.Click(doneButtonActive);
	}

	public void SeasonQuickfilingPublishFlow() throws InterruptedException, Exception{
		
		WebUtility.Wait();
		//--Verifying Season QuickFiling Draft Status
		try
		{
			seasonProperties.click();
			WebUtility.element_to_be_visible(contentStatus);
			TestUtil.captureScreenshot("Season QuickFiling Draft Status","Season");
			String expectedStatus = "Draft";
			String actualStatus = contentStatus.getText();
			sa.assertEquals(actualStatus,expectedStatus,"Season QuickFiling Draft Status is Failed");
			Reporter.log("Season QuickFiling Draft Status is Passed",true);	
		}
		catch (Exception e)
		{
			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}
		
		WebUtility.Click(checklist);
		WebUtility.Wait();
		js.executeScript("window.scrollBy(0,600)");
		scheduledCountry.get(0).click();
		WebUtility.Click(dropDownList.get(0));
		WebUtility.Click(publishContent);
		WebUtility.Click(yes.get(0));
		WebUtility.Click(ok);
        
        js.executeScript("window.scrollTo(0,0)");   
        
        //--Verifying Season QuickFiling Published Status
  		try
  		{
  			seasonProperties.click();
  			WebUtility.element_to_be_visible(contentStatus);
  			TestUtil.captureScreenshot("Season QuickFiling Published Status","Season");
  			String expectedStatus = "Published";
  			String actualStatus = contentStatus.getText();
  			sa.assertEquals(actualStatus,expectedStatus,"Season QuickFiling Published Status is Failed");
  			Reporter.log("Season QuickFiling Published Status is Passed",true);	
  		}
  		catch (Exception e){
  			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
  		}
        WebUtility.Wait();
        
        js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
        //----------------Republish -----------------------------------------------------------------------------
	       
        try {
	        license.click();
	        WebUtility.Wait();
	       	
	        WebUtility.Click(licenseStatusButton);
	        WebUtility.Click(inactivateLicenseDropdown);
	        WebUtility.Wait();
	        actionProvider.moveToElement(inactivateLicenseReason).click().build().perform();
	        WebUtility.Click(dropDownList.get(0));
	        WebUtility.Click(yes.get(0));
	        WebUtility.Click(doneButtonActive);
       }
       catch(Exception e)
       {
    	   try
    	   {
    	   WebUtility.Click(no);
    	   }
    	   catch(Exception e1) {
    		 e1.printStackTrace();
    	   }
    	   
   	        WebUtility.Click(seasonProperties);
			WebUtility.Click(seoDetails);
			editSeoTitleTag.click();
			Thread.sleep(1000);
			seoTitleTag.click();
			seoTitleTag.sendKeys(Keys.CONTROL + "a");
			seoTitleTag.sendKeys(Keys.BACK_SPACE);
			Thread.sleep(2000);
			seoTitleTag.sendKeys("Automation Test Data Season Quick Filing Edit");
			Thread.sleep(1000);
			editSeoTitleTag.click();
			Thread.sleep(1000);
			WebUtility.Click(doneButtonActive);
			e.printStackTrace();  
           
       }
        
        WebUtility.Click(checklist);
        js.executeScript("window.scrollTo(0,document.body.scrollHeight)");
        WebUtility.Click(republishContent);
        WebUtility.Click(yes.get(0));
        //WebUtility.Click(yes.get(1));
        WebUtility.Click(ok);
        js.executeScript("window.scrollTo(0,0)");
		WebUtility.Wait();
		
		//---------------------Verifying Season Republished status-------------------------------
		try
		{
				WebUtility.Click(seasonProperties);
				WebUtility.element_to_be_visible(contentStatus);
				TestUtil.captureScreenshot("Season Quick Filing Republished Status","TV Show");
				String expectedStatus = "Published";
				String actualStatus = contentStatus.getText();
				sa.assertEquals(actualStatus,expectedStatus,"Season Main Content Republished Status is Failed");
		}
		catch (Exception e)
		{
				e.printStackTrace();
		}
		
		//-----------------------Unpublish -----------------------------------
		WebUtility.Click(checklist);
		WebUtility.Wait();
        WebUtility.Click(unpublishContent);
        WebUtility.Click(unpublishReason);
        WebUtility.Click(dropDownList.get(0));
    	WebUtility.Click(yes.get(0));
		WebUtility.Click(ok);
        
        js.executeScript("window.scrollTo(0,0)");
        
        //--Verifying Season QuickFiling Unpublished Status
        try {
        	seasonProperties.click();
			WebUtility.element_to_be_visible(contentStatus);
			TestUtil.captureScreenshot("Season Quick Filing Unpublished Status","Season");
			String expectedStatus = "Unpublished";
			String actualStatus = contentStatus.getText();
			sa.assertEquals(actualStatus,expectedStatus,"Season QuickFiling Unpublished Status is Failed");
			Reporter.log("Season QuickFiling Unpublished Status is Passed",true);	
        } catch (IOException e) {
            System.out.println("Screenshot Capture Failed ->" + e.getMessage());
        }
        
        // ------------- Schedule Content -------------------------------------
		WebUtility.Click(checklist);
		js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
		WebUtility.Click(scheduleContent);

		//System.out.println("--------> Current Time = " + currentTime.getAttribute("innerText").substring(20, 28));
		//String currentTimeString = currentTime.getAttribute("innerText").substring(20, 27);
		//js.executeScript("window.scrollTo(0,document.body.scrollHeight);");

		WebUtility.Click(scheduledPublicationTime);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        WebUtility.Click(datePickerMinutes);
        actionProvider.moveToElement(datePickerMinutesClock).click().build().perform();      
        datePicker.sendKeys(Keys.ESCAPE);	

		WebUtility.Click(scheduledContentCountry);
		WebUtility.Click(dropDownList.get(0));

		WebUtility.Click(scheduleContentButton);
		WebUtility.Click(yes.get(0));
		WebUtility.Wait();
		WebUtility.Click(ok);

		js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
        WebUtility.Wait();
        js.executeScript("window.scrollTo(0,0);");
        WebUtility.Wait();
        
     
        //System.out.println("--------> Scheduled Time = "+scheduledTime.getAttribute("innerText").substring(12, 20));
        //String scheduledTimeString = scheduledTime.getAttribute("innerText").substring(12, 20);
    	//js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
    	//sa.assertTrue(scheduledTimeString.contains(currentTimeString),"Season Quick Filing - Current Time and Scheduled Time are not Equal");

		// -------------------Verifying Season Scheduled
		// Status---------------------------------
		try {
			seasonProperties.click();
			WebUtility.Wait();
			WebUtility.element_to_be_visible(contentStatus);
			TestUtil.captureScreenshot("Season Quick Filing Scheduled Status", "TV Show");
			String expectedStatus = "Scheduled";
			String actualStatus = contentStatus.getText();
			sa.assertEquals(actualStatus, expectedStatus, "Season Quick Filing Scheduled Status is Failed");
		} catch (Exception e) {
			e.printStackTrace();
		}

		WebUtility.Wait();
		WebUtility.Click(checklist);
		WebUtility.Click(deleteSchedule);
		WebUtility.Click(yes.get(0));
		WebUtility.Click(ok);

		WebUtility.Wait();
		js.executeScript("window.scrollTo(0,document.body.scrollHeight)");
		WebUtility.Click(scheduledCountry.get(0));
		WebUtility.Click(dropDownList.get(0));

		WebUtility.Click(publishContent);
		WebUtility.Click(yes.get(0));
		WebUtility.Click(ok);
		js.executeScript("window.scrollTo(0,0)");

		publishedHistory.click();
		WebUtility.Wait();
		try {
			TestUtil.captureScreenshot("Season Quick Filing - Published History", "Season");
		} catch (IOException e) {
			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}

		seasonsBreadcrumb.click();
		Thread.sleep(2000);
	}
	
 
	// ------------------------------------ Season Single Landing -------------------------------------------------------------------
	public void SeasonSingleLandingProperties() throws InterruptedException, Exception{
		WebUtility.Click(dashboard);
        WebUtility.Click(tvShowButton);
	
		searchString.click();
		searchString.sendKeys(tvShowSingleLandingTitle);
		WebUtility.Wait();
		WebUtility.Wait();
		WebUtility.element_to_be_clickable(editButton.get(0));
		editButton.get(0).click();

		WebUtility.Wait();
		WebUtility.Click(seasons);
		WebUtility.Click(createSeason);
		createSeasonSingleLanding.click();
		yes.get(0).click();
	
		WebUtility.Wait();
		WebUtility.Click(editSingleLandingTitle);
        WebUtility.Click(title);
        title.sendKeys(Keys.CONTROL + "a");
        title.sendKeys(Keys.BACK_SPACE);
        title.sendKeys(tvShowSingleLandingTitle+" - Season Single Landing");
        WebUtility.Click(editSingleLandingTitle);
        WebUtility.Wait();         
        
        WebUtility.Click(editSingleLandingShortDesc);
        WebUtility.Click(shortDesc);
        shortDesc.sendKeys(Keys.CONTROL + "a");
        shortDesc.sendKeys(Keys.BACK_SPACE);
		shortDesc.sendKeys("The Expanse is an American science fiction television series developed by Mark Fergus and Hawk Ostby - Season Single Landing");
        WebUtility.Click(editSingleLandingShortDesc);
        WebUtility.Wait();         
       
        WebUtility.Click(editSingleLandingZee5ReleaseDate);
        WebUtility.Click(zee5ReleaseDate);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ESCAPE);
        WebUtility.Click(editSingleLandingZee5ReleaseDate);
        WebUtility.Wait();
	
        WebUtility.Click(editSingleLandingTelecastDate);
        WebUtility.Click(telecastDate);
        datePicker.sendKeys(Keys.ARROW_LEFT);
        datePicker.sendKeys(Keys.ARROW_LEFT);
        datePicker.sendKeys(Keys.ESCAPE);
        WebUtility.Click(editSingleLandingTelecastDate);
        WebUtility.Wait();	

		 WebUtility.Click(editSingleLandingMultiAudio);
		 if(!multiAudio.isSelected())
		 {
		       multiAudio.click();
		       WebUtility.Click(editSingleLandingMultiAudio);
		 }
		 else
		 WebUtility.Click(editSingleLandingMultiAudio);	   
		 WebUtility.Wait();
        
	
		WebUtility.Click(editSingleLandingAudioLanguage);
		WebUtility.Click(audioLanguage);
		audioLanguage.sendKeys(Keys.BACK_SPACE);
		WebUtility.Wait();
		WebUtility.Click(dropDownList.get(0));
		WebUtility.Click(editSingleLandingAudioLanguage);
		WebUtility.Wait();
	
		nextButton.click();
		WebUtility.Wait();
		
		
		WebUtility.Click(editSingleLandingBroadcastState);
		WebUtility.Click(broadcastState);
		broadcastState.sendKeys(Keys.BACK_SPACE);
		WebUtility.Click(dropDownList.get(0));
		WebUtility.Click(editSingleLandingBroadcastState);
		WebUtility.Wait();
		
		WebUtility.Click(editSingleLandingUpcomingPageText);
		upcomingPage.click();
		upcomingPage.sendKeys(Keys.CONTROL + "a");
		upcomingPage.sendKeys(Keys.BACK_SPACE);
		upcomingPage.sendKeys("Automation Upcoming Page Text - Season Single Landing");
		WebUtility.Click(editSingleLandingUpcomingPageText);
		WebUtility.Wait();		
	
		nextButton.click();
		WebUtility.Wait();
		
		String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
		sa.assertTrue(doneButtonActiveColor.contains("255, 255, 255") || doneButtonActiveColor.contains("255,255,255"), "Seasons Single Landing - Properties Mark As Done Assertion Fails");
		WebUtility.element_to_be_clickable(doneButtonActive);
		doneButtonActive.click();
		Thread.sleep(2000);
	}

	public void SeasonSingleLandingCastCrew() throws InterruptedException,Exception{
	/*
		dashboard.click();
		tvShowButton.click();
		Thread.sleep(2000);

		searchString.click();
		searchString.sendKeys("The Expanse Suite");
		Thread.sleep(2000);
		WebUtility.element_to_be_clickable(editButton.get(0));
		editButton.get(0).click();
		Thread.sleep(2000);
		
		seasons.click();
		WebUtility.element_to_be_clickable(createSeason);
		createSeason.click();
		Thread.sleep(2000);
		createSeasonSingleLanding.click();
		yes.get(0).click();
		Thread.sleep(3000);
	*/	
		castCrew.click();
		
		WebUtility.Wait();
		try {
		removeActor.click();
		Thread.sleep(2000);
		}
		catch(Exception e)
		{
			//------No Action Required -----
		}
		
		WebUtility.Click(editSingleLandingActor);
		actor.sendKeys(Keys.CONTROL + "a");
		actor.sendKeys(Keys.BACK_SPACE);
		actor.sendKeys("Salman Khan Season Single Landing");
		WebUtility.element_to_be_clickable(dropDownList.get(0));
		dropDownList.get(0).click();
		WebUtility.Wait();
		WebUtility.Click(editSingleLandingActor);
		WebUtility.Wait();

		WebUtility.Click(editSingleLandingCharacter);
		character.sendKeys(Keys.CONTROL + "a");
		character.sendKeys(Keys.BACK_SPACE);
		character.sendKeys("Test Character Season Single Landing");
		WebUtility.Click(editSingleLandingCharacter);
		WebUtility.Wait();
	     
	
		WebUtility.Click(editSingleLandingDirector);
		director.sendKeys(Keys.CONTROL + "a");
		director.sendKeys(Keys.BACK_SPACE);
		director.sendKeys("Automation Test Data Season Single Landing");
		WebUtility.element_to_be_clickable(dropDownList.get(0));
		dropDownList.get(0).click();
		WebUtility.Wait();
		WebUtility.Click(editSingleLandingDirector);
		WebUtility.Wait();	
		
		String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
		sa.assertTrue(doneButtonActiveColor.contains("255, 255, 255") || doneButtonActiveColor.contains("255,255,255"), "Seasons Single Landing - Cast & Crew Mark As Done Assertion Fails");
		WebUtility.element_to_be_clickable(doneButtonActive);
		doneButtonActive.click();
	
	}
	
	public void SeasonSingleLandingLicense() throws InterruptedException, Exception{
		WebUtility.Click(license);
		WebUtility.Click(createLicense);

		WebUtility.Click(licenseSetName);
        licenseSetName.sendKeys("Automation Test Data");
        
        WebUtility.Click(licenseFromDate);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ESCAPE);

        WebUtility.Click(licenseToDate);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ESCAPE);
		
        WebUtility.Click(licenseCountry);
        for (int x = 0; x < dropDownList.size(); x++) {
        	if(x<4) {
            WebUtility.Click(dropDownList.get(x));
        	}
        	else {
        		break;
        	}
        }
				
		WebUtility.Click(licenseBusinessType);
		WebUtility.Click(dropDownList.get(0));

		WebUtility.Wait();
		driver.findElement(By.xpath("//span[contains(text(),'SAVE')]")).click();
		
		WebUtility.Click(license);
		WebUtility.Click(createLicense);
		
		WebUtility.Click(licenseSetName);
        licenseSetName.sendKeys("Automation Test Data II");

        WebUtility.Click(licenseFromDate);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ESCAPE);

        WebUtility.Click(licenseToDate);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ESCAPE);
	
        WebUtility.Click(licenseCountry);
        for (int x = 0; x < dropDownList.size(); x++) {
        	if(x<4) {
            WebUtility.Click(dropDownList.get(x));
        	}
        	else {
        		break;
        	}
        }
		
		
		WebUtility.Click(licenseBusinessType);
		WebUtility.Click(dropDownList.get(0));
		WebUtility.Wait();
		driver.findElement(By.xpath("//span[contains(text(),'SAVE')]")).click();
		WebUtility.Wait();
  
		WebUtility.element_to_be_clickable(doneButtonActive);
		String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
		sa.assertTrue(doneButtonActiveColor.contains("255, 255, 255") || doneButtonActiveColor.contains("255,255,255"), "Seasons Single Landing - License Mark As Done Assertion Fails");
		WebUtility.Click(doneButtonActive);
		
        WebUtility.Click(expiredLicense);
        Thread.sleep(2000);
        WebUtility.Click(expiredLicenseBack);
        Thread.sleep(2000);

        WebUtility.Click(licenseFilters);
        WebUtility.Click(licenseFromDate);
        datePicker.sendKeys(Keys.ARROW_LEFT);
        datePicker.sendKeys(Keys.ARROW_LEFT);
        datePicker.sendKeys(Keys.ESCAPE);

        WebUtility.Click(licenseToDate);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ESCAPE);

        WebUtility.Click(applyFilter);

        Thread.sleep(2000);
        WebUtility.Click(licenseFilters);
        WebUtility.Click(clearFilter);
        
        WebUtility.Click(licenseFilters);
        WebUtility.Click(licenseFilterBusinessType);
        WebUtility.Click(dropDownList.get(0));
        WebUtility.Click(applyFilter);

        Thread.sleep(2000);
        WebUtility.Click(licenseFilters);
        WebUtility.Click(clearFilter);
	    
        WebUtility.Click(licenseFilters);
        WebUtility.Wait();
        licenseFilterActiveStatus.click();
        WebUtility.Wait();
        WebUtility.Click(applyFilter);

        Thread.sleep(2000);
        WebUtility.Click(licenseFilters);
        WebUtility.Click(clearFilter);

        WebUtility.Click(licenseFilters);
        WebUtility.Wait();
        licenseFilterInActiveStatus.click();
        WebUtility.Wait();
        WebUtility.Click(applyFilter);

        Thread.sleep(2000);
        WebUtility.Click(licenseFilters);
        WebUtility.Click(clearFilter);


	}
	
	public void SeasonSingleLandingImages() throws InterruptedException, Exception{

		WebUtility.Click(images);
		WebUtility.Wait();
		try {
			if(removeSet.get(0).isDisplayed()==true);
			{
			for(int x = 0 ; x < removeSet.size(); x++)
				{
				removeSet.get(x).click();
				WebUtility.Wait();
				yes.get(0).click();
				WebUtility.Wait();
				}
			}
		}
		catch(Exception e){
			System.out.println("No Image Sets Exist");
		}

		WebUtility.Click(imagesCreateNewSet);

		WebUtility.Click(newSetName);
		newSetName.sendKeys("Automation Test Data ");

		newSetCountry.click();
		WebUtility.Click(dropDownList.get(0));
		closeDropdown.click();

		newSetPlatform.click();
		WebUtility.Click(dropDownList.get(0));
		closeDropdown.click();

		newSetGender.click();
		WebUtility.Click(dropDownList.get(0));
		closeDropdown.click();

		newSetGenre.click();
		WebUtility.Click(dropDownList.get(0));
		closeDropdown.click();

		newSetAgeGroup.click();
		WebUtility.Click(dropDownList.get(0));
		closeDropdown.click();

		newSetLanguage.click();
		WebUtility.Click(dropDownList.get(0));
		closeDropdown.click();

		newSetOthers.click();
		newSetOthers.sendKeys("Automation Test Data");

		WebUtility.Click(save);

		WebUtility.Click(imageSets.get(1));

		js.executeScript("window.scrollTo(0,0)");
		Thread.sleep(2000);
		cover.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\Cover.jpg");

		WebUtility.Wait();
		appcover.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\AppCover.jpg");

		WebUtility.Wait();
		list.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\List.jpg");

		WebUtility.Wait();
		square.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\SquarePNG.png");

		js.executeScript("window.scrollBy(0,600)");

		WebUtility.Wait();
		tvCover.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\TVCoverPNG.png");

		WebUtility.Wait();
		portrait.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\portrait.jpg");

		WebUtility.Wait();
		listClean.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\ListCleanPNG.png");

		js.executeScript("window.scrollTo(0,document.body.scrollHeight);");

		WebUtility.Wait();
		portraitClean.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\PortraitCleanPNG.png");

		WebUtility.Wait();
		telcoSquare.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\TelcoSquare.jpg");

		WebUtility.Wait();
		passport.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\PassportPNG.png");

		WebUtility.Wait();
        
		try {
			// -------------------------- Image Set 3
			// -----------------------------------------------------
			WebUtility.element_to_be_fluentwait(imagesCreateNewSet);
			WebUtility.Click(imagesCreateNewSet);

			WebUtility.Click(newSetName);
			newSetName.sendKeys("Automation Test Data II");

			newSetCountry.click();
			WebUtility.Click(dropDownList.get(1));
			closeDropdown.click();

			newSetPlatform.click();
			WebUtility.Click(dropDownList.get(1));
			closeDropdown.click();

			newSetGender.click();
			WebUtility.Click(dropDownList.get(1));
			closeDropdown.click();

			newSetGenre.click();
			WebUtility.Click(dropDownList.get(1));
			closeDropdown.click();

			newSetAgeGroup.click();
			WebUtility.Click(dropDownList.get(1));
			closeDropdown.click();

			newSetLanguage.click();
			WebUtility.Click(dropDownList.get(1));
			closeDropdown.click();

			newSetOthers.click();
			newSetOthers.sendKeys("Automation Test Data II");

			WebUtility.Click(save);

			WebUtility.Click(imageSets.get(2));

			js.executeScript("window.scrollTo(0,0)");

			WebUtility.Wait();
			cover.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\CoverPNG.png");

			WebUtility.Wait();
			appcover.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\AppCoverPNG.png");

			WebUtility.Wait();
			list.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\List.jpg");

			WebUtility.Wait();
			square.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\Square.jpg");

			js.executeScript("window.scrollBy(0,600)");

			WebUtility.Wait();
			tvCover.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\TVCover.jpg");

			WebUtility.Wait();
			portrait.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\portraitPNG.png");

			WebUtility.Wait();
			listClean.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\ListClean.jpg");

			js.executeScript("window.scrollTo(0,document.body.scrollHeight);");

			WebUtility.Wait();
			portraitClean.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\PortraitCleanPNG.png");

			WebUtility.Wait();
			telcoSquare.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\TelcoSquare.jpg");

			WebUtility.Wait();
			passport.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\Passport.jpg");
		} catch (Exception e) {
			e.printStackTrace();
		}
        
		try {
			js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
			WebUtility.Wait();
			WebUtility.Click(imageSetActiveButton);
			WebUtility.Wait();
			WebUtility.Click(dropDownList.get(0));
			WebUtility.javascript(yes.get(0));
			WebUtility.Wait();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		//------------------------ Delete Image Set -----------------------------
		try {
			removeSet.get(1).click();
			WebUtility.Wait();
			yes.get(0).click();
			WebUtility.Wait();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
		sa.assertTrue(doneButtonActiveColor.contains("255, 255, 255") || doneButtonActiveColor.contains("255,255,255"), "Seasons Single Landing - Images Mark As Done Assertion Fails");
		WebUtility.Click(doneButtonActive);

		js.executeScript("window.scrollTo(0,0);");
		WebUtility.Wait();
		WebUtility.Click(imageSets.get(0));
		try {
			TestUtil.captureScreenshot("Season Single Landing Images Section Default Set","Season");
		} catch (IOException e) {
			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}

		js.executeScript("window.scrollTo(0,document.body.scrollHeight);");

		try {
			TestUtil.captureScreenshot("Season Single Landing Images Section Default Set 2","Season");
		} catch (IOException e) {
			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}

		WebUtility.Click(imageSets.get(1));
		js.executeScript("window.scrollTo(0,0);");
		try {
			TestUtil.captureScreenshot("Season Single Landing Images Section Created Set","Season");
		} catch (IOException e) {
			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}
		js.executeScript("window.scrollTo(0,document.body.scrollHeight);");

		try {
			TestUtil.captureScreenshot("Season Single Landing Images Section Created Set 2","Season");
		} catch (IOException e) {
			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}

	}

	public void SeasonSingleLandingSeoDetails() throws InterruptedException, Exception {
		WebUtility.Click(seoDetails);
		WebUtility.Wait();	   

		WebUtility.Click(editSingleLandingSeoTitleTag);		
		seoTitleTag.click();
		seoTitleTag.sendKeys(Keys.CONTROL + "a");
		seoTitleTag.sendKeys(Keys.BACK_SPACE);
		seoTitleTag.sendKeys("Automation Test Data Season Single Landing");
		editSingleLandingSeoTitleTag.click();
		Thread.sleep(1000);

		WebUtility.Click(editSingleLandingSeoMetaDescription);		
		seoMetaDescription.click();
		seoMetaDescription.sendKeys(Keys.CONTROL + "a");
		seoMetaDescription.sendKeys(Keys.BACK_SPACE);
		seoMetaDescription.sendKeys("Automation Test Data Season Single Landing");
		WebUtility.Click(editSingleLandingSeoMetaDescription);	
		Thread.sleep(1000);
		
		WebUtility.Click(editSingleLandingSeoMetaSynopsis);		
		seoMetaSynopsis.click();
		seoMetaSynopsis.sendKeys(Keys.CONTROL + "a");
		seoMetaSynopsis.sendKeys(Keys.BACK_SPACE);
		seoMetaSynopsis.sendKeys("Automation Test Data Season Single Landing");
		WebUtility.Click(editSingleLandingSeoMetaSynopsis);	
		Thread.sleep(1000);

		WebUtility.Click(editSingleLandingH1Heading);		
		seoH1Heading.click();
		seoH1Heading.sendKeys(Keys.CONTROL + "a");
		seoH1Heading.sendKeys(Keys.BACK_SPACE);
		seoH1Heading.sendKeys("Automation Test Data Season Single Landing");
		WebUtility.Click(editSingleLandingH1Heading);	
		Thread.sleep(1000);
	
		WebUtility.Click(editSingleLandingH2Heading);		
		seoH2Heading.click();
		seoH2Heading.sendKeys(Keys.CONTROL + "a");
		seoH2Heading.sendKeys(Keys.BACK_SPACE);
		seoH2Heading.sendKeys("Automation Test Data Season Single Landing");
		WebUtility.Click(editSingleLandingH2Heading);	
		Thread.sleep(1000);		
				
		editSingleLandingVideoObjectSchema.click();
	    if(!seoVideoObjectSchema.isSelected())
	    	seoVideoObjectSchema.click();
	    else
	    	editSingleLandingVideoObjectSchema.click();
	    Thread.sleep(1000);
	    		
		editSingleLandingWebsiteSchema.click();
	    if(!seoWebsiteSchema.isSelected())
	    	seoWebsiteSchema.click();
	    else
	    	editSingleLandingWebsiteSchema.click();
	    Thread.sleep(1000);
	  		
		editSingleLandingBreadcrumbListSchema.click();
	    if(!seoBreadcrumbListSchema.isSelected())
	    	seoBreadcrumbListSchema.click();
	    else
	    	editSingleLandingBreadcrumbListSchema.click();
	    Thread.sleep(1000);
		
		editSingleLandingImageObjectSchema.click();
	    if(!seoImageObjectSchema.isSelected())
	    	seoImageObjectSchema.click();
	    else
	    	editSingleLandingImageObjectSchema.click();
	    Thread.sleep(1000);
			
		editSingleLandingOrganizationSchema.click();
	    if(!seoOrganizationSchema.isSelected())
	    	seoOrganizationSchema.click();
	    else
	    	editSingleLandingOrganizationSchema.click();
	    Thread.sleep(1000);
	    
	   
		String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
		sa.assertTrue(doneButtonActiveColor.contains("255, 255, 255") || doneButtonActiveColor.contains("255,255,255"), "Seasons Single Landing - SEO Details Mark As Done Assertion Fails");
		WebUtility.element_to_be_clickable(doneButtonActive);
		doneButtonActive.click();

	}
	
public void SeasonsSingleLandingRelatedContent() throws InterruptedException {
    	
		WebUtility.Click(relatedContent);
		WebUtility.Click(relatedContentAssignTvShows);

		WebUtility.element_to_be_visible(relatedContentAddTvShows);
		WebUtility.Click(relatedContentAddTvShows);

		WebUtility.Wait();
		WebUtility.Wait();
		checkBox.get(0).click();
		checkBox.get(1).click();
		WebUtility.Click(assignRelatedContent);
		WebUtility.Click(deleteRelatedContent);
		WebUtility.Wait();
		WebUtility.Click(yes.get(0));
		WebUtility.Wait();

		WebUtility.Click(relatedContentAssignVideos);
		WebUtility.element_to_be_visible(relatedContentAddVideos);
		WebUtility.Click(relatedContentAddVideos);

		WebUtility.Wait();
		WebUtility.Wait();
		checkBox.get(0).click();
		checkBox.get(1).click();
		WebUtility.Click(assignRelatedContent);
		WebUtility.Click(deleteRelatedContent);
		WebUtility.Wait();
		WebUtility.Click(yes.get(0));
		WebUtility.Wait();

		WebUtility.Click(relatedContentAssignMovies);
		WebUtility.element_to_be_visible(relatedContentAddMovies);
		WebUtility.Click(relatedContentAddMovies);

		WebUtility.Wait();
		WebUtility.Wait();
		checkBox.get(0).click();
		checkBox.get(1).click();
		WebUtility.Click(assignRelatedContent);
		WebUtility.Click(deleteRelatedContent);
		WebUtility.Wait();
		WebUtility.Click(yes.get(0));
		WebUtility.Wait();

		WebUtility.Click(relatedContentAssignSeasons);
		WebUtility.element_to_be_visible(relatedContentAddSeasons);
		WebUtility.Click(relatedContentAddSeasons);

		WebUtility.Wait();
		WebUtility.Wait();
		checkBox.get(0).click();
		checkBox.get(1).click();
		WebUtility.Click(assignRelatedContent);
		WebUtility.Click(deleteRelatedContent);
		WebUtility.Wait();
		WebUtility.Click(yes.get(0));
		WebUtility.Wait();

		WebUtility.Click(relatedContentAssignEpisodes);
		WebUtility.element_to_be_visible(relatedContentAddEpisodes);
		WebUtility.Click(relatedContentAddEpisodes);

		WebUtility.Wait();
		WebUtility.Wait();
		checkBox.get(0).click();
		checkBox.get(1).click();
		WebUtility.Click(assignRelatedContent);
		WebUtility.Click(deleteRelatedContent);
		WebUtility.Wait();
		WebUtility.Click(yes.get(0));
		WebUtility.Wait();

		String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
		sa.assertTrue(doneButtonActiveColor.contains("255, 255, 255") || doneButtonActiveColor.contains("255,255,255"), "Seasons Single Landing - Related Content Mark As Done Assertion Fails");
		WebUtility.Click(doneButtonActive);

		WebUtility.Wait();
		updateSeasonBreadcrumb.click();
		WebUtility.Wait();
}

	public void SeasonsSingleLandingPublishFlow() throws InterruptedException, Exception{
		
		WebUtility.Wait();
		//--Verifying Season Single Landing Draft Status
		try
		{
			seasonProperties.click();
			WebUtility.element_to_be_visible(contentStatus);
			TestUtil.captureScreenshot("Season SingleLanding Draft Status","Season");
			String expectedStatus = "Draft";
			String actualStatus = contentStatus.getText();
			sa.assertEquals(actualStatus,expectedStatus,"Season SingleLanding Draft Status is Failed");
			Reporter.log("Season SingleLanding Draft Status is Passed",true);	
		}
		catch (Exception e)
		{
			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}
		
		WebUtility.Click(checklist);
		WebUtility.Wait();
		js.executeScript("window.scrollBy(0,600)");
		scheduledCountry.get(0).click();
		WebUtility.Click(dropDownList.get(0));
		WebUtility.Click(publishContent);
		WebUtility.Click(yes.get(0));
		WebUtility.Click(ok);  
      
        
        js.executeScript("window.scrollTo(0,0)");   
        try {
            TestUtil.captureScreenshot("Seasons Single Landing Publish","Season");
        } catch (IOException e) {
            System.out.println("Screenshot Capture Failed ->" + e.getMessage());
        }
        WebUtility.Wait();
        
        js.executeScript("window.scrollTo(0,document.body.scrollHeight);");																   
        
        js.executeScript("window.scrollTo(0,0)");        
        Thread.sleep(3000);
        //--Verifying Season Single Landing Published Status
  		try
  		{
  			seasonProperties.click();
  			WebUtility.element_to_be_visible(contentStatus);
  			TestUtil.captureScreenshot("Season SingleLanding Published Status","Season");
  			String expectedStatus = "Published";
  			String actualStatus = contentStatus.getText();
  			sa.assertEquals(actualStatus,expectedStatus,"Season SingleLanding Published Status is Failed");
  			Reporter.log("Season SingleLanding Published Status is Passed",true);	
  		}
  		catch (Exception e){
  			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
  		}
          
  		 //----------------Republish -----------------------------------------------------------------------------
	       
        try {
	        license.click();
	        WebUtility.Wait();
	       	
	        WebUtility.Click(licenseStatusButton);
	        WebUtility.Click(inactivateLicenseDropdown);
	        WebUtility.Wait();
	        actionProvider.moveToElement(inactivateLicenseReason).click().build().perform();
	        WebUtility.Click(dropDownList.get(0));
	        WebUtility.Click(yes.get(0));
	        WebUtility.Click(doneButtonActive);
       }
       catch(Exception e)
       {
    	   try
    	   {
    	   WebUtility.Click(no);
    	   }
    	   catch(Exception e1) {
    		 e1.printStackTrace();
    	   }
    	   
    	   WebUtility.Click(seasonProperties);
			WebUtility.Click(seoDetails);
			editSeoTitleTag.click();
			Thread.sleep(1000);
			seoTitleTag.click();
			seoTitleTag.sendKeys(Keys.CONTROL + "a");
			seoTitleTag.sendKeys(Keys.BACK_SPACE);
			Thread.sleep(2000);
			seoTitleTag.sendKeys("Automation Test Data Season Single Landing Edit");
			Thread.sleep(1000);
			editSeoTitleTag.click();
			Thread.sleep(1000);
			WebUtility.Click(doneButtonActive);
			e.printStackTrace();          
       }
        
        WebUtility.Click(checklist);
        js.executeScript("window.scrollTo(0,document.body.scrollHeight)");
        WebUtility.Click(republishContent);
        WebUtility.Click(yes.get(0));
        //WebUtility.Click(yes.get(1));
        WebUtility.Click(ok);
        js.executeScript("window.scrollTo(0,0)");
		WebUtility.Wait();
		
		//---------------------Verifying Season Republished status-------------------------------
		try
		{
				WebUtility.Click(seasonProperties);
				WebUtility.element_to_be_visible(contentStatus);
				TestUtil.captureScreenshot("Season Quick Filing Republished Status","TV Show");
				String expectedStatus = "Published";
				String actualStatus = contentStatus.getText();
				sa.assertEquals(actualStatus,expectedStatus,"Season Single Landing Republished Status is Failed");
		}
		catch (Exception e)
		{
				e.printStackTrace();
		}
		
		//-----------------------Unpublish -----------------------------------
  		WebUtility.Click(checklist);
  		WebUtility.Wait();      
        js.executeScript("window.scrollTo(0,document.body.scrollHeight);");        
        WebUtility.Click(unpublishContent);
        WebUtility.Click(unpublishReason);
        WebUtility.Click(dropDownList.get(0));
    	WebUtility.Click(yes.get(0));
		WebUtility.Click(ok);
        
        js.executeScript("window.scrollTo(0,0)");
        //--Verifying Season Single Landing Unpublished Status
        try {
        	seasonProperties.click();
			WebUtility.element_to_be_visible(contentStatus);
			TestUtil.captureScreenshot("Season SingleLanding Unpublished Status","Season");
			String expectedStatus = "Unpublished";
			String actualStatus = contentStatus.getText();
			sa.assertEquals(actualStatus,expectedStatus,"Season SingleLanding Unpublished Status is Failed");
			Reporter.log("Season SingleLanding Unpublished Status is Passed",true);	
        } catch (IOException e) {
            System.out.println("Screenshot Capture Failed ->" + e.getMessage());
        }
        
        // ------------- Schedule Content -------------------------------------
		WebUtility.Click(checklist);
		js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
		WebUtility.Click(scheduleContent);

		//System.out.println("--------> Current Time = " + currentTime.getAttribute("innerText").substring(20, 28));
		//String currentTimeString = currentTime.getAttribute("innerText").substring(20, 27);
		//js.executeScript("window.scrollTo(0,document.body.scrollHeight);");

		WebUtility.Click(scheduledPublicationTime);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        WebUtility.Click(datePickerMinutes);
        actionProvider.moveToElement(datePickerMinutesClock).click().build().perform();      
        datePicker.sendKeys(Keys.ESCAPE);	

		WebUtility.Click(scheduledContentCountry);
		WebUtility.Click(dropDownList.get(0));

		WebUtility.Click(scheduleContentButton);
		WebUtility.Click(yes.get(0));
		WebUtility.Wait();
		WebUtility.Click(ok);

		js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
        WebUtility.Wait();
        js.executeScript("window.scrollTo(0,0);");
        WebUtility.Wait();
             
        //System.out.println("--------> Scheduled Time = "+scheduledTime.getAttribute("innerText").substring(12, 20));
        //String scheduledTimeString = scheduledTime.getAttribute("innerText").substring(12, 20);
    	//js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
    	//sa.assertTrue(scheduledTimeString.contains(currentTimeString),"Season Single Landing - Current Time and Scheduled Time are not Equal");
    	
		// -------------------Verifying Season Scheduled Status---------------------------------
		try {
			seasonProperties.click();
			WebUtility.Wait();
			WebUtility.element_to_be_visible(contentStatus);
			TestUtil.captureScreenshot("Season Single Landing Scheduled Status", "TV Show");
			String expectedStatus = "Scheduled";
			String actualStatus = contentStatus.getText();
			sa.assertEquals(actualStatus, expectedStatus, "Season Single Landing Scheduled Status is Failed");
		} catch (Exception e) {
			e.printStackTrace();
		}

		WebUtility.Wait();
		WebUtility.Click(checklist);
		WebUtility.Click(deleteSchedule);
		WebUtility.Click(yes.get(0));
		WebUtility.Click(ok);

		WebUtility.Wait();
		js.executeScript("window.scrollTo(0,document.body.scrollHeight)");
		WebUtility.Click(scheduledCountry.get(0));
		WebUtility.Click(dropDownList.get(0));

		WebUtility.Click(publishContent);
		WebUtility.Click(yes.get(0));
		WebUtility.Click(ok);
		js.executeScript("window.scrollTo(0,0)");

		publishedHistory.click();
		WebUtility.Wait();
		try {
			TestUtil.captureScreenshot("Season Single Landing - Published History", "Season");
		} catch (IOException e) {
			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}

		seasonsBreadcrumb.click();
		Thread.sleep(2000);
	}

	
	
 
//-------------------------------------------------END of Single Landing --------------------------------------------------------
	
//-------------------------------------------------Season Quick Filling ---------------------------------------------------------
	
	
	
	//-----------------------------------------Season Ordering ----------------------------------------------------------------------------
	public void SeasonOrdering() throws InterruptedException,Exception{

			
		WebUtility.Click(dashboard);
		WebUtility.Click(tvShowButton);

		searchString.click();
		searchString.sendKeys(tvShowTitle);
		WebUtility.Wait();
		WebUtility.Wait();
		WebUtility.element_to_be_clickable(editButton.get(0));
		editButton.get(0).click();

		WebUtility.Wait();
		WebUtility.Click(seasons);
		WebUtility.Click(createSeason);
		createSeasonMainSeason.click();
		yes.get(0).click();
		WebUtility.Click(seasonsBreadcrumb);
		WebUtility.Wait();

														  
		Actions action = new Actions(driver);
		action.moveToElement(seasonListingOrder.get(0)).clickAndHold().build().perform();
		action.moveToElement(seasonCard.get(0)).moveToElement(seasonListingOrder.get(1)).release().build().perform();
		WebUtility.Wait();

		WebUtility.Click(setOrdering);
		WebUtility.Click(createNewSet);
					   
		WebUtility.Click(setName);
				  
		setName.sendKeys("Ordering Test");
					 
		setCountry.click();
					 
		for (int x = 0; x < dropDownList.size(); x++)
			dropDownList.get(x).click();
					 
		closeDropdown.click();
		yes.get(0).click();
		WebUtility.Click(expandSetOrder);

		WebUtility.Wait();
		action.moveToElement(seasonListingOrder.get(0)).clickAndHold().build().perform();
		action.moveToElement(seasonCard.get(0)).moveToElement(seasonListingOrder.get(1)).release().build().perform();
		WebUtility.Wait();
			
		}
  
	
	//--------------------------------Seasons Listing Page --------------------------------------------
	public void SeasonsListingPage() throws InterruptedException,Exception{
        dashboard.click();
        WebUtility.Wait();
        tvShowButton.click();
        WebUtility.Wait();	
	
		WebUtility.Click(tvShowSearch);
		WebUtility.Click(dropDownList.get(1));
		WebUtility.Wait();
		WebUtility.Click(searchString);
		searchString.sendKeys(tvShowTitle+" - Season");
		WebUtility.Wait();
		WebUtility.Wait();
		
		WebUtility.Click(listingExpiringLicense);
		WebUtility.Wait();
		Thread.sleep(3000);
		WebUtility.Click(tvShowBreadcrumb);
		WebUtility.Wait();
		
		WebUtility.Click(tvShowSearch);
		WebUtility.Click(dropDownList.get(1));
		WebUtility.Wait();
		WebUtility.Click(searchString);
		searchString.sendKeys(tvShowTitle+" - Season");
		WebUtility.Wait();
		WebUtility.Wait();
		
		WebUtility.Click(listingPublishedHistory);
		WebUtility.Wait();
		Thread.sleep(3000);
		WebUtility.Click(listingClosePublishedHistory);
		WebUtility.Wait();		
		
		try {
		WebUtility.Click(listingLicenseCountries);
		WebUtility.Wait();
		WebUtility.Click(listingCloseLicenseCountries);
		WebUtility.Wait();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
		WebUtility.Click(listingTvShowLink);
		WebUtility.Wait();
		Thread.sleep(3000);
		WebUtility.Click(tvShowBreadcrumb);
		WebUtility.Wait();
	/*	
		WebUtility.Click(tvShowSearch);
		WebUtility.Click(dropDownList.get(1));
		WebUtility.Wait();
		WebUtility.Click(searchString);
		searchString.sendKeys(tvShowTitle+" - Season");
		WebUtility.Wait();
		WebUtility.Wait();
		
		WebUtility.Click(listingEpisodeLink);
		WebUtility.Wait();
		Thread.sleep(3000);
		WebUtility.Click(tvShowBreadcrumb);
		WebUtility.Wait();
*/		
		WebUtility.Click(tvShowSearch);
		WebUtility.Click(dropDownList.get(1));
		WebUtility.Wait();
		
		listingDraftStatus.click();
		WebUtility.Wait();
		Thread.sleep(3000);
		
		listingAllStatus.click();
		WebUtility.Wait();
		Thread.sleep(3000);
		
		listingChangedStatus.click();
		WebUtility.Wait();
		Thread.sleep(3000);
		
		listingPublishedStatus.click();
		WebUtility.Wait();
		Thread.sleep(3000);
		
		listingUnpublishedStatus.click();
		WebUtility.Wait();
		Thread.sleep(3000);
		
		listingNeedWorkStatus.click();
		WebUtility.Wait();
		Thread.sleep(3000);
		
		listingScheduledStatus.click();
		WebUtility.Wait();
		Thread.sleep(3000);
		
		listingSubmittedToReviewStatus.click();
		WebUtility.Wait();
		Thread.sleep(3000);
		
		listingArchivedStatus.click();
		WebUtility.Wait();
		Thread.sleep(3000);
		
		listingPublishingQueueStatus.click();
		WebUtility.Wait();
		Thread.sleep(3000);
		

	}

	//---------------------------- Search by External ID ----------------------------------------------
	public void SeasonsListingSearch() throws InterruptedException,Exception {	

		// -------- Edit Flow For Testing Purposes ----------------------------
		/*
		WebUtility.Click(dashboard);
		WebUtility.Click(tvShowButton);

		searchString.click();
		searchString.sendKeys(tvShowTitle);
		WebUtility.Wait();
		WebUtility.Wait();
		WebUtility.element_to_be_clickable(editButton.get(0));
		editButton.get(0).click();
		WebUtility.Wait();
		WebUtility.Click(seasons);
		WebUtility.Click(editButton.get(0));
		WebUtility.Wait();

		externalID = js.executeScript("return arguments[0].childNodes[1].innerText", seasonExternalID).toString();
*/		
		WebUtility.Click(dashboard);
		WebUtility.Click(tvShowButton);
		
		WebUtility.Click(tvShowSearch);
		WebUtility.Click(dropDownList.get(1));
		WebUtility.Wait();
		
		WebUtility.Click(searchString);
		searchString.sendKeys((tvShowTitle+" - Season").toLowerCase());
		WebUtility.Wait();
		WebUtility.Wait();
		
		
		WebUtility.Click(searchString);
		searchString.sendKeys(Keys.CONTROL+"a");
		searchString.sendKeys(Keys.BACK_SPACE);
		searchString.sendKeys(externalID);
		WebUtility.Wait();
		WebUtility.Wait();
		
		try {
			TestUtil.captureScreenshot("Season Search", "Season");
		} catch (IOException e) {
			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}


	}
	
	public void SeasonAssertionResults() throws Exception{
		sa.assertAll();
	}
	
	
}

