package com.zee5.qa.pages;



import java.io.IOException;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.asserts.SoftAssert;

import com.qa.utilities.WebUtility;
import com.zee5.qa.base.TestBase;
import com.zee5.qa.util.TestUtil;


public class TvShowsPage extends TestBase{
	
	//------------------------------TV Titles ----------------------------------------
	String tvShowTitle ="The Expanse";
	String tvShowQuickFilingTitle = "The Expanse Quick Filing";
	String tvShowSingleLandingTitle = "The Expanse Single Landing";
	String externalID;
	SoftAssert sa = new SoftAssert();
	JavascriptExecutor js = (JavascriptExecutor) driver; 		
	Actions actionProvider = new Actions(driver);
	//---------------------------- TV Show Properties ----------------------------------------------------------------
    @FindBy(xpath = "//div[contains(@class,'auto-tvShow')]")
    WebElement createTvShow;

    @FindBy(xpath = "//button[contains(@class,'auto-leftTab-TVShowProperties')]")
    WebElement tvShowProperties;
    
    @FindBy(xpath = "//button[contains(@class,'auto-leftTab-ContentProperties')]")
    WebElement contentProperties;

   // @FindBy(xpath = "//span[contains(text(),'Title Summary')]")
    @FindBy(xpath = "//button[contains(@class,'auto-leftTab-TitleSummary')]")  
    WebElement titleSummary;

    @FindBy(xpath = "//input[@name='note']")
    WebElement note;

    @FindBy(xpath = "//input[@name=\"title\"]")
    WebElement title;

    @FindBy(xpath = "//div[contains(@id,'auto-shortDescription')]//child::div[contains(@class,'ql-editor')]")
    WebElement shortDesc;
    
    @FindBy(xpath = "//div[contains(@class,'auto-WebDescription')]//child::div[contains(@class,'ql-editor')]")
    WebElement webDesc;
    
    @FindBy(xpath = "//div[contains(@class,'auto-AppDescription')]//child::div[contains(@class,'ql-editor')]")
    WebElement appDesc;
  
    @FindBy(xpath = "//div[contains(@class,'auto-TVDescription')]//child::div[contains(@class,'ql-editor')]")
    WebElement tvDesc;  

    @FindBy(xpath = "//input[@id='subtype']")
    WebElement subType;
    
    @FindBy(xpath = "//input[@id='category']")
    WebElement category;

    @FindBy(xpath = "//input[@id='primaryGenre']")
    WebElement primaryGenre;
    
    @FindBy(xpath="//div[contains(@class,'auto-PrimaryGenre')]//descendant::span[@class='MuiChip-label']")
    WebElement primaryGenreAssertion;

    @FindBy(xpath = "//input[@id='secondaryGenre']")
    WebElement secondaryGenre;

    @FindBy(xpath="//div[contains(@class,'auto-SecondaryGenre')]//descendant::span[@class='MuiChip-label']")
    WebElement secondaryGenreAssertion;
    
    @FindBy(xpath = "//input[@id='thematicGenre']")
    WebElement thematicGenre;

    @FindBy(xpath = "//input[@id='settingGenre']")
    WebElement settingGenre;

    @FindBy(xpath = "//input[@id='rcsCategory']")
    WebElement rcsCategory;

    @FindBy(xpath = "//input[@name='creativeTitle']")
    WebElement creativeTitle;

    @FindBy(xpath = "//input[@name='alternatetitle']")
    WebElement alternateTitle;
    
    @FindBy(xpath = "//input[@name='pageTitle']")
    WebElement pageTitle;

    @FindBy(xpath = "//textarea[@name='pageDescription']")
    WebElement pageDescription;

    @FindBy(name = "titleForSocialShare")
    WebElement titleForSocialShare;

    @FindBy(name = "descriptionForSocialShare")
    WebElement descriptionForSocialShare;
     
//    @FindBy(name = "dateZee5Published")
    @FindBy(xpath = "//input[contains(@name,'dateZee5Published')]")
    WebElement zee5ReleaseDate;
    
    @FindBy(xpath = "//input[contains(@name,'telecastDate')]")
    WebElement telecastDate;
    
    @FindBy(xpath = "//div[@class='MuiPaper-root MuiPopover-paper MuiPaper-elevation8 MuiPaper-rounded']")
    WebElement datePicker;    

    @FindBy(xpath = "//input[@name='isMultiAudio']")
    WebElement multiAudio;

    @FindBy(xpath = "//input[@id='audioLanguages']")
    WebElement audioLanguage;
    
    @FindBy(xpath="//div[contains(@class,'auto-AudioLanguage')]//descendant::span[@class='MuiChip-label']")
    WebElement audioLanguagesAssertion;
    
    @FindBy(xpath = "//input[@id='contentLanguage']")
    WebElement contentLanguage;
    
    @FindBy(xpath = "//input[@id='primaryLanguage']")
    WebElement primaryLanguage;

    @FindBy(xpath = "//input[@id='dubbedLanguageTitle']")
    WebElement dubbedLanguageTitle;
    
    @FindBy(xpath="//div[contains(@class,'auto-DubbedLanguage')]//descendant::span[@class='MuiChip-label']")
    WebElement dubbedLanguageAssertion;

    @FindBy(xpath = "//input[@id='originalLanguage']")
    WebElement originalLanguage;
    
    @FindBy(xpath = "//input[@id='subtitleLanguages']")
    WebElement subtitleLanguages;

    @FindBy(xpath = "//input[@id='contentVersion']")
    WebElement contentVersion;

    @FindBy(xpath = "//input[@id='theme']")
    WebElement theme;

    @FindBy(xpath = "//input[@name='contractId']")
    WebElement contractId;

    @FindBy(xpath = "//input[@id='specialCategory']")
    WebElement specialCategory;

    @FindBy(xpath = "//input[@id='specialCategoryCountry']")
    WebElement specialCategoryCountry;

    @FindBy(xpath = "//input[@name='specialCategoryFrom']")
    WebElement specialCategoryFrom;

    @FindBy(xpath = "//input[@name='specialCategoryTo']")
    WebElement specialCategoryTo;
    
	@FindBy(xpath = "//div[@class='status-head flex align-items-center']//div[contains(@class,'s-badge create-movie-stage ')]")
	WebElement contentStatus; 

    //-------------------------------------------------TV Show Properties ->Classification -----------------------------------------------------------------------------------------------
    @FindBy(id = "broadcastState")
    WebElement broadcastState;    
    
    @FindBy(id = "rating")
    WebElement rating;

    @FindBy(id = "contentOwner")
    WebElement contentOwner;

    @FindBy(id = "originCountry")
    WebElement originCountry;
   
    @FindBy(id = "contentProduction")
    WebElement contentProduction;    
    
    @FindBy(name = "upcomingPage")
    WebElement upcomingPage;
  
    @FindBy(id = "ageRating")
    WebElement contentDescriptor;
    
    @FindBy(id = "certification")
    WebElement certification;
    
    @FindBy(xpath = "//div[contains(@class,'auto-Certification')]//descendant::span[@class='MuiChip-label']")
    WebElement certificationAssertion;
  
    @FindBy(id = "emotions")
    WebElement emotions;
    
    @FindBy(id = "contentGrade")
    WebElement contentGrade;
    
    @FindBy(id = "era")
    WebElement era;

    @FindBy(id = "targetAge")
    WebElement targetAge;
    
    @FindBy(id = "targetAudiences")
    WebElement targetAudiences;
    
    @FindBy(xpath = "//input[contains(@id,'auto-tags')]")
    WebElement tags;
    
    @FindBy(name = "digitalKeywords")
    WebElement digitalKeywords;
    
    @FindBy(name = "adaptation")
    WebElement adaptation;
    
    @FindBy(name = "events")
    WebElement events;
    
    @FindBy(id = "productionCompany")
    WebElement productionCompany;
    
    @FindBy(name = "popularityScore")
    WebElement popularityScore;
    
    @FindBy(name = "trivia")
    WebElement trivia;
    
    @FindBy(name = "epgProgramId")
    WebElement epgProgramId;
    
    @FindBy(id = "channel")
    WebElement channel; 
    
    @FindBy(name = "showAirTime")
    WebElement showAirTime;
    
    @FindBy(id = "showAirTimeDays")
    WebElement showAirTimeDays; 

    @FindBy(name = "officialSite")
    WebElement officialSite;
    
    @FindBy(xpath = "//input[contains(@id,'auto-awardRecipient')]")
    WebElement awardRecipient;

    @FindBy(id = "awardsCategory")
    WebElement awardsCategory;

    @FindBy(xpath = "//input[contains(@id,'auto-awardsandrecognition')]")
    WebElement awardHonor;
    
    //---------------------------------------------------------TV Show Properties -> Global Fields ---------------------------------------------------------------

    @FindBy(id="auto-country-0-0")
    WebElement globalCountry;
    
    @FindBy(id="country")
    WebElement globalCountryQuickFilling;
    
    @FindBy(name="title")
    WebElement globalTitle;
    
    @FindBy(id="auto-title-0-0")
    WebElement globalTitleQuickFilling;
        
    @FindBy(id="auto-creativeTitle-0-2")
    WebElement globalCreativeTitle;
    
    @FindBy(id="auto-alternativeTitle-0-3")
    WebElement globalAlternativeTitle;    
    
    @FindBy(id="auto-pageTitle-0-4")
    WebElement globalPageTitle;
    
    @FindBy(id="auto-pageDescription-0-5")
    WebElement globalPageDescription;
    
    @FindBy(id="auto-socialShareTitle-0-6")
    WebElement globalSocialShareTitle;
        
    @FindBy(name="telecastDate")
    WebElement globalTelecastDate;
    
    @FindBy(id="subtitleLanguages")
    WebElement globalSubtitleLanguages;    
	
    
    @FindBy(id="auto-socialShareDescription-0-10")
    WebElement globalDescriptionForSocialShare;
    
    @FindBy(xpath = "//div[contains(@id,'auto-shortDescription')]//child::div[contains(@class,'ql-editor')]")
    WebElement globalShortDesc;
    
    @FindBy(xpath = "//div[contains(@class,'auto-WebDescription')]//child::div[contains(@class,'ql-editor')]")    
    WebElement globalWebDesc;
    
    @FindBy(xpath = "//div[contains(@class,'auto-AppDescription')]//child::div[contains(@class,'ql-editor')]")
    WebElement globalAppDesc;
  
    @FindBy(xpath = "//div[contains(@class,'auto-TVDescription')]//child::div[contains(@class,'ql-editor')]")
    WebElement globalTVDesc;
   
    @FindBy(name="upcomingPageText")
    WebElement globalUpcomingPage;
    
  
  
    //-------------------------------------------- Cast and Crew -----------------------------------------------------------
    @FindBy(xpath = "//button[contains(@class,'auto-leftTab-Cast&Crew')]")
    WebElement castCrew;

    @FindBy(xpath = "//input[contains(@id,'auto-castName')]")
    WebElement actor;
    
    @FindBy(xpath = "//input[contains(@id,'auto-castName-1-0')]")
    WebElement actor2;
    
    @FindBy(xpath = "//input[contains(@id,'auto-castName-2-0')]")
    WebElement actor3;

    @FindBy(xpath = "//input[contains(@id,'auto-character')]")
    WebElement character;
    
    @FindBy(xpath = "//input[contains(@id,'auto-character-1-1')]")
    WebElement character2;
    
    @FindBy(xpath = "//input[contains(@id,'auto-character-2-1')]")
    WebElement character3;
    
    @FindBy(xpath = "//div[contains(@class,'auto-ActorChange')]//descendant::input")
    WebElement actorChange;      

    @FindBy(xpath = "//div[contains(@class,'auto-Performer')]//descendant::input")
    WebElement performer;

    @FindBy(xpath = "//div[contains(@class,'auto-Host/Anchorman')]//descendant::input")
    WebElement host;

    @FindBy(xpath = "//div[contains(@class,'auto-Singer')]//descendant::input")
    WebElement singer;

    @FindBy(xpath = "//div[contains(@class,'auto-Lyricist')]//descendant::input")
    WebElement lyricst;

    @FindBy(xpath = "//div[contains(@class,'auto-Director')]//descendant::input")
    WebElement director;

    @FindBy(xpath = "//div[contains(@class,'auto-Cinematography/DOP')]//descendant::input")
    WebElement cinematography;

    @FindBy(xpath = "//div[contains(@class,'auto-Producer')]//descendant::input")
    WebElement producer;

    @FindBy(xpath = "//div[contains(@class,'auto-ExecutiveProducer')]//descendant::input")
    WebElement executiveProducer;

    @FindBy(xpath = "//div[contains(@class,'auto-MusicDirector')]//descendant::input")
    WebElement musicDirector;

    @FindBy(xpath = "//div[contains(@class,'auto-Choreographer')]//descendant::input")
    WebElement choreographer;

    @FindBy(xpath = "//div[contains(@class,'auto-TitleThemeMusic')]//descendant::input")
    WebElement titleThemeMusic;

    @FindBy(xpath = "//div[contains(@class,'auto-BackgroundScore')]//descendant::input")
    WebElement backgroundScore;

    @FindBy(xpath = "//div[contains(@class,'auto-StoryWriter')]//descendant::input")
    WebElement storyWriter;

    @FindBy(xpath = "//div[contains(@class,'auto-ScreenPlay')]//descendant::input")
    WebElement screenPlay;

    @FindBy(xpath = "//div[contains(@class,'auto-DialogueWriter')]//descendant::input")
    WebElement dialogueWriter;

    @FindBy(xpath = "//div[contains(@class,'auto-FilmEditing')]//descendant::input")
    WebElement filmEditing;

    @FindBy(xpath = "//div[contains(@class,'auto-Casting')]//descendant::input")
    WebElement casting;

    @FindBy(xpath = "//div[contains(@class,'auto-ProductionDesign')]//descendant::input")
    WebElement productionDesign;

    @FindBy(xpath = "//div[contains(@class,'auto-ArtDirection')]//descendant::input")
    WebElement artDirection;

    @FindBy(xpath = "//div[contains(@class,'auto-SetDecoration')]//descendant::input")
    WebElement setDecoration;

    @FindBy(xpath = "//div[contains(@class,'auto-CostumeDesign')]//descendant::input")
    WebElement costumeDesign;

    @FindBy(xpath = "//div[contains(@class,'auto-ProductionCo')]//descendant::input")
    WebElement productionCo;

    @FindBy(xpath = "//div[contains(@class,'auto-Presenter')]//descendant::input")
    WebElement presenter;

    @FindBy(xpath = "//div[contains(@class,'auto-Guest')]//descendant::input")
    WebElement guest;

    @FindBy(xpath = "//div[contains(@class,'auto-Participant')]//descendant::input")
    WebElement participant;

    @FindBy(xpath = "//div[contains(@class,'auto-Judges')]//descendant::input")
    WebElement judges;

    @FindBy(xpath = "//div[contains(@class,'auto-Narrator')]//descendant::input")
    WebElement narrator;

    @FindBy(xpath = "//div[contains(@class,'auto-Sponsor')]//descendant::input")
    WebElement sponsor;

    @FindBy(xpath = "//div[contains(@class,'auto-Graphics')]//descendant::input")
    WebElement graphics;

    @FindBy(xpath = "//div[contains(@class,'auto-Vocalist')]//descendant::input")
    WebElement vocalist;
    
    @FindBy(id = "sponsors")
    WebElement globalSponsors;
    
    @FindBy(id = "country")
    WebElement globalGroupCountry;

    @FindBy(xpath = "//div[contains(@class,'add-btn')]//button")
    WebElement addActor;
    
    @FindBy(xpath = "//div[contains(@class,'remove-btn')]//button")
    List<WebElement> removeActor;

    //--------------------------------------License ------------------------------------------------------	
    @FindBy(xpath = "//button[contains(@class,'auto-leftTab-LicenseModule')]")
    WebElement license;

    @FindBy(xpath = "//div[contains(text(),'Create License')]")
    WebElement createLicense;

    @FindBy(xpath = "//input[@name='fromDate']")
    WebElement licenseFromDate;

	@FindBy(xpath = "//input[@name='toDate']")
	WebElement licenseToDate;

	@FindBy(xpath = "//input[contains(@id,'auto-country')]")
    WebElement licenseCountry;

	@FindBy(xpath = "//input[contains(@id,'auto-business')]")   
    WebElement licenseBusinessType;
	
	@FindBy(xpath = "//input[contains(@id,'auto-platform')]")   
    WebElement licensePlatform;
	
	@FindBy(xpath = "//input[contains(@id,'auto-tvodTier')]")
	WebElement licenseTVOD;

	@FindBy(xpath = "//span[contains(text(),'SAVE')]//ancestor::button")
	WebElement saveLicense;

    @FindBy(xpath = "//div[@role='tooltip']")
    WebElement inactivateLicenseDropdown;

    @FindBy(xpath = "//div[@class='ml-10 status-dropdown val']")
    WebElement licenseStatusButton;

    @FindBy(xpath = "//input[@id='auto-reason-0-0']")
    WebElement inactivateLicenseReason;

    @FindBy(xpath = "//input[@name='manual' and @value='2']")
    WebElement useTemplateRadioButton;

    @FindBy(xpath = "//input[@type='text']")
    WebElement selectTemplate;

    @FindBy(xpath = "//div[@class='default-edit-btn']")
    WebElement editLicense;
    
    @FindBy(id = "auto-country-0-0")
    WebElement editLicenseCountry;

    @FindBy(xpath = "//button[contains(@class,'auto-button-short-btn')]")
    WebElement expiredLicense;

    @FindBy(className = "item")
    WebElement expiredLicenseBack;

    @FindBy(xpath = "//button[contains(@class,'auto-button-filter-btn')]")
    WebElement licenseFilters;

    @FindBy(id = "auto-businessType-0-2")
    WebElement licenseFilterBusinessType;

    @FindBy(id = "auto-billingType-0-3")
    WebElement licenseFilterBillingType;

    @FindBy(id = "auto-platform-0-4")
    WebElement licenseFilterPlatform;

    @FindBy(xpath = "//input[@name='status' and @value = '1']")
    WebElement licenseFilterActiveStatus;
    
    @FindBy(xpath = "//input[@name='status' and @value = '0']")
    WebElement licenseFilterInActiveStatus;
    
    @FindBy(xpath = "//input[@name='setName']")
    WebElement licenseSetName;											   						  

	@FindBy(xpath = "//div[contains(@class,'delete')]")
	List<WebElement> licenseTemplateDeleteSets;

    //--------------------------------------------Images -----------------------------------------------------------------
    @FindBy(xpath = "//button[contains(@class,'auto-leftTab-Images')]")
    WebElement images;

    @FindBy(xpath = "//input[@name='cover']")
    WebElement cover;

    @FindBy(xpath = "//input[@name='appcover']")
    WebElement appcover;

    @FindBy(xpath = "//input[@name='list']")
    WebElement list;

    @FindBy(xpath = "//input[@name='square']")
    WebElement square;

    @FindBy(xpath = "//input[@name='tvcover']")
    WebElement tvCover;

    @FindBy(xpath = "//input[@name='portrait']")
    WebElement portrait;

    @FindBy(xpath = "//input[@name='listclean']")
    WebElement listClean;

    @FindBy(xpath = "//input[@name='portraitclean']")
    WebElement portraitClean;

    @FindBy(xpath = "//input[@name='telcosquare']")
    WebElement telcoSquare;

    @FindBy(xpath = "//input[@name='passport']")
    WebElement passport;

    @FindBy(xpath = "//button[contains(@class,'auto-create-newSet')]")
    WebElement imagesCreateNewSet;
    
    @FindBy(name = "setName")
    WebElement newSetName;

    @FindBy(id="auto-GroupCountry-0-1")
    WebElement newSetCountry;
    
    @FindBy(id = "auto-Platform-0-2")
    WebElement newSetPlatform;
    
    @FindBy(id = "auto-gender-0-0")
    WebElement newSetGender;
    
    @FindBy(id="auto-genre-0-1")
    WebElement newSetGenre;
    
    @FindBy(id = "auto-ageGroup-0-2")
    WebElement newSetAgeGroup;
    
    @FindBy(id = "auto-language-0-3")
    WebElement newSetLanguage;
    
    @FindBy(name="others")
    WebElement newSetOthers;
    
    @FindBy(xpath = "//div[contains(@class,'imageset-acc')]")
    List<WebElement> imageSets;
    
    @FindBy(xpath = "(//button[contains(@class,'Active')])[1]")
    WebElement imageSetActiveButton;
    
    @FindBy(xpath = "//span[@class='remove']")
    List<WebElement> removeSet;
    
    @FindBy(xpath = "//body/div[4]/div[3]/div[1]/div[3]/button[1]")
    WebElement deactivateSetYes;
    //----------------------------------------- SEO Details ---------------------------------------------------------------

    @FindBy(xpath = "//button[contains(@class,'auto-leftTab-SEODetails')]")
    WebElement seoDetails;

    @FindBy(name = "titleTag")
    WebElement seoTitleTag;

    @FindBy(name = "metaDescription")
    WebElement seoMetaDescription;

    @FindBy(name = "metaSynopsis")
    WebElement seoMetaSynopsis;

    @FindBy(id = "redirectionType")
    WebElement seoRedirectionType;

    @FindBy(name = "redirectionLink")
    WebElement seoRedirectionLink;

    @FindBy(name = "noIndexNoFollow")
    WebElement seoNoIndexNoFollow;

    @FindBy(name = "h1Heading")
    WebElement seoH1Heading;

    @FindBy(name = "h2Heading")
    WebElement seoH2Heading;

    @FindBy(name = "h3Heading")
    WebElement seoH3Heading;

    @FindBy(name = "h4Heading")
    WebElement seoH4Heading;

    @FindBy(name = "h5Heading")
    WebElement seoH5Heading;

    @FindBy(name = "h6Heading")
    WebElement seoH6Heading;

    @FindBy(name = "robotsMetaNoIndex")
    List < WebElement > seoRobotsMetaIndex;

    @FindBy(name = "robotsMetaImageIndex")
    WebElement seoRobotsMetaImageIndex;

    @FindBy(name = "robotsMetaImageNoIndex")
    WebElement seoRobotsMetaImageNoIndex;

    @FindBy(name = "breadcrumbTitle")
    WebElement seoBreadcrumbTitle;

    @FindBy(name = "internalLinkBuilding")
    WebElement seoInternalLinkBuilding;

    @FindBy(xpath = "//input[@type=\"checkbox\"]")
    List < WebElement > seoCheckboxes;

    
    // ---------------------------- Seo Details Global Fields -----------------------------------------------------------------------------------
    
    @FindBy(xpath="//div[contains(@class,'auto-Country/Group')]//input")
    WebElement globalSeoCountry;
    
    //-------------------------- MAP Content -----------------------------------------------------------------------------------------------------

    @FindBy(xpath = "//button[contains(@class,'auto-leftTab-MapContent')]")
    WebElement mapContent;

    @FindBy(xpath = "//div[contains(text(),\"Add Content\")]")
    WebElement addContent;

    @FindBy(xpath = "//input[@type='checkbox']")
    WebElement checkbox;

    @FindBy(xpath = "//div[contains(text(),'Assign Content')]")
    List < WebElement > mapContentAssign;
    //@FindBy(className = "btn-create-user  ")
 

    @FindBy(xpath = "//div[@class='mark-done mark-fill-active']")
    WebElement mapMarkAsDone;

    @FindBy(name = "searchVal")
    List < WebElement > searchMapContent;

    //-----------------------------------------Quick Links ----------------------------------------------------

    @FindBy(xpath = "//li[contains(text(),'Related Content')]")
    WebElement relatedContent;

    @FindBy(name = "searchVal")
    WebElement relatedContentSearch;

    @FindBy(xpath = "//span[contains(text(),'Assign Movies')]")
    WebElement relatedContentAssignMovies;
    
    @FindBy(xpath = "//div[contains(text(),'Add Movies')]")
    WebElement relatedContentAddMovies;
    
    @FindBy(xpath = "//span[contains(text(),'Assign TV Shows')]")
    WebElement relatedContentAssignTvShows;
    
    @FindBy(xpath = "//div[contains(text(),'Add TV Shows')]")
    WebElement relatedContentAddTvShows;

    @FindBy(xpath = "//span[contains(text(),\"Assign Videos\")]")
    WebElement relatedContentAssignVideos;

    @FindBy(xpath = "//div[contains(text(),'Add Videos')]")
    WebElement relatedContentAddVideos;
    
    @FindBy(xpath = "//button[contains(.,'Assign Seasons')]")
    WebElement relatedContentAssignSeasons;

    @FindBy(xpath = "//div[contains(text(),'Add Seasons')]")
    WebElement relatedContentAddSeasons;

    @FindBy(xpath = "//button[contains(.,'Assign Episodes')]")
    WebElement relatedContentAssignEpisodes;

    @FindBy(xpath = "//div[contains(text(),'Add Episodes')]")
    WebElement relatedContentAddEpisodes;
      
    @FindBy(id = "assignedContent")
    WebElement assignRelatedContent;
    
    @FindBy(xpath = "//li[contains(text(),'Published History')]")
    WebElement publishedHistory;

    @FindBy(xpath = "(//span[contains(@class,'auto-delete')])[1]")
    WebElement deleteRelatedContent;

    //----------------------------------- CheckList---------------------------------------------------

    @FindBy(xpath = "//button[contains(@class,'auto-leftTab-Checklist')]")
    WebElement checklist;

    @FindBy(xpath = "//div[contains(@class,'auto-countryGroup')]//descendant::input")
    WebElement checklistCountry;

    @FindBy(xpath = "//span[contains(text(),'Publish Content')]")
    WebElement publishContent;

    @FindBy(xpath = "//span[contains(text(),'Republish Content')]")
    WebElement republishContent;

    @FindBy(xpath = "//span[contains(text(),'Unpublish Content')]")
    WebElement unpublishContent;

    @FindBy(xpath = "//span[@class='edit tooltip-sec hand-cursor']")
    WebElement restoreButton;

    @FindBy(id = "archive-icon_svg__Rectangle_273")
    List < WebElement > archiveButton;

    @FindBy(xpath = "//div[contains(@class,'Schedule-box')]//div[contains(@class,'add-btn create-btn')]")
    WebElement scheduleContent;

//    @FindBy(name = "scheduledPublicationTime")
    @FindBy(xpath = "//button[@class='MuiButtonBase-root MuiIconButton-root']")
    WebElement scheduledPublicationTime;

    @FindBy(xpath = "//div[contains(@class,'auto-countryGroup')]//descendant::input")
    List < WebElement > scheduledCountry;

    @FindBy(xpath = "//div[contains(@class,'Schedule-box m-b-20 ')]//div[contains(@class,'auto-countryGroup')]//input")
    WebElement scheduledContentCountry;
    
    @FindBy(xpath = "//span[contains(text(),'Schedule Content')]")
    WebElement scheduleContentButton;

    @FindBy(xpath = "(//label[contains(text(),'Country')]//following::input)[3]")
    WebElement unpublishReason;
    
	@FindBy(id = "archive-icon_svg__Rectangle_273")
    List < WebElement > Archive;

	@FindBy(xpath="//span[contains(text(),'Yes')]")
	WebElement ArchiveYes;
	
	@FindBy(xpath="//span[@class='edit tooltip-sec hand-cursor']")
	WebElement Restore;

	@FindBy(xpath="//span[contains(text(),'Yes')]")
	WebElement RestoreYes;
    
	@FindBy(xpath = "//button[contains(@class,'delete')]")
	WebElement deleteSchedule;
	
	@FindBy(xpath = "(//div[contains(@class,'val')])[3]")
	WebElement scheduledTime;
	
	@FindBy(xpath = "(//div[contains(@class,'icon-w-text icon-w-14 m-b-10')])[1]")
	WebElement currentTime;	
	
	@FindBy(xpath = "(//button[contains(@class,'MuiPickersToolbarButton-toolbarBtn')])[4]")
	WebElement datePickerMinutes;		
	
	@FindBy(xpath = "//span[contains(@class,'MuiTypography-root MuiPickersClockNumber-clockNumber') and contains(text(),'05')]")
	WebElement datePickerMinutesClock;	
	
	    
    // ------------------------------Sort And Filter -------------------------------------------------------

    @FindBy(xpath = "//div[contains(@class,'dropdown-btn auto-dropdown-wrapper')]")
    WebElement searchContentType;
      
    @FindBy(xpath = "//span[contains(text(),'Clear')]")
    WebElement clearFilter;
    
 	@FindBy(xpath="//button[contains(.,'Sort')]")
 	WebElement Sort;

 	@FindBy(xpath="//span[contains(text(),'Ascending to Descending')]")
 	WebElement sortAscToDesc;

 	@FindBy(xpath="//button[contains(.,'Apply Sort')]")
 	WebElement applySort;

 	@FindBy(xpath="//span[contains(text(),'Clear')]")
 	WebElement clearSort;

 	@FindBy(xpath="//span[contains(text(),'Descending to Ascending')]")
 	WebElement sortDescToAsc;

 	@FindBy(xpath="//span[contains(text(),'Oldest to Newest')]")
 	WebElement sortOldToNew;
 	
 	@FindBy(xpath="//span[contains(text(),'Newest to Oldest')]")
 	WebElement sortNewToOld;

 	@FindBy(xpath="//button[contains(.,'Filters')]")
 	WebElement Filter;

 	@FindBy(xpath="//div[contains(text(),'Draft')]")
 	WebElement filterDraft;
 	
 	@FindBy(xpath="//div[contains(text(),'All')]")
 	WebElement filterAllStatus;

 	@FindBy(xpath="//div[contains(text(),'Changed')]")
 	WebElement filterChanged;

 	@FindBy(xpath="//div[contains(text(),'Published')]")
 	WebElement filterPublishedStatus;

 	@FindBy(xpath="//div[contains(text(),'Unpublished')]")
 	WebElement filterUnpublished;

 	@FindBy(xpath="//div[contains(text(),'Need Work')]")
 	WebElement filterNeedWork;

 	@FindBy(xpath="//div[contains(text(),'Scheduled')]")
 	WebElement filterScheduled;

 	@FindBy(xpath="//div[contains(text(),'Submitted to Review')]")
 	WebElement filterSubmittedToReview;

 	@FindBy(xpath="//div[contains(text(),'Archived')]")
 	WebElement filterArchived;

 	@FindBy(xpath="//button[contains(.,'Apply Filter')]")
 	WebElement applyFilter;

 	@FindBy(xpath="//input[@value='Start Date']")
 	WebElement filterStartDate;

 	@FindBy(xpath="//input[@value='End Date']")
 	WebElement filterEndDate;

 	@FindBy(xpath="//input[@id='primaryGenre']")
 	WebElement filterPrimaryGenre;

 	@FindBy(xpath="//input[@id='secondaryGenre']")
 	WebElement filterSecondaryGenre;

 	@FindBy(xpath="//input[@id='thematicGenre']")
 	WebElement filterThematicGenre;

 	@FindBy(xpath="//input[@id='settingGenre']")
 	WebElement filterSettingGenre;

 	//@FindBy(xpath="//input[@id='actor']")
 	@FindBy(xpath="//input[@id='auto-actor-0-4']")
 	WebElement filterActor;

 	@FindBy(xpath="//input[@id='subType']")
 	WebElement filterSubType;

 	@FindBy(xpath="//input[@id='contentCategory']")
 	WebElement filterContentCategory;

 	@FindBy(xpath="//input[@id='rcsCategory']")
 	WebElement filterRCSCategory;

 	@FindBy(xpath="//html/body/div[2]/div[3]/div/div[1]")
 	WebElement filterClosetab;

 	@FindBy(xpath="//input[@id='theme']")
 	WebElement filterTheme;

 	@FindBy(xpath="//input[@id='targetAudience']")
 	WebElement filterTargetAudienceFilter;

 	@FindBy(xpath="//input[@id='licenseGroupCountries']")
 	WebElement filterLicenseGroupCountries;

 	@FindBy(xpath="//input[@id='ageRating']")
 	WebElement filterAgeRating;

 	@FindBy(xpath="//input[@id='businessType']")
 	WebElement filterBusinessType;

 	@FindBy(xpath="//input[@name='externalId']")
 	WebElement filterExternalId;

 	@FindBy(xpath="//input[@id='contentRating']")
 	WebElement filterContentRatingFilter;

 	@FindBy(xpath="//input[@id='contentOwner']")
 	WebElement filterContentOwner;

 	@FindBy(xpath="//input[@id='audioLanguage']")
 	WebElement filterAudioLanguage;

 	@FindBy(xpath="//input[@id='originalLanguage']")
 	WebElement filterOriginalLanguage;

 	//@FindBy(xpath="//input[@id='tags']")
 	@FindBy(xpath="//input[contains(@id,'tags')]")
 	WebElement filterTags;

 	@FindBy(xpath="//input[@id='translationLanguage']")
 	WebElement filterTranslationLanguage;

 	@FindBy(xpath="//input[@id='translationStatus']")
 	WebElement filterTranslationStatus;

 	@FindBy(xpath="//input[@id='moodEmotion']")
 	WebElement filterMoodEmotion;

 	@FindBy(xpath="//input[@id='broadcastState']")
 	WebElement filterBroadcast;
   
	
	
    //------------------------------------------Tranlations ---------------------------------------------------------------
  
    @FindBy(className =  "auto-quickLinks-Translations")
    WebElement translations;
    
    @FindBy(xpath = "//div[contains(@id,'auto-Short Description')]//child::div[contains(@class,'ql-editor')]")
    WebElement translationsShortDesc;
    
    @FindBy(xpath = "//div[contains(@id,'auto-Web Description')]//child::div[contains(@class,'ql-editor')]")
    WebElement translationsWebDesc;
   
    @FindBy(xpath = "//div[contains(@id,'auto-App Description')]//child::div[contains(@class,'ql-editor')]")
    WebElement translationsAppDesc;
    
    @FindBy(xpath = "//div[contains(@id,'auto-TV Description')]//child::div[contains(@class,'ql-editor')]")
    WebElement translationsTVDesc;
    
    @FindBy(name = "upcomingPageText")
    WebElement translationsUpcomingPage;
    
    @FindBy(xpath = "//input[@name='alternativeTitle']")
    WebElement translationsAlternativeTitle;
    
    @FindBy(name = "socialShareTitle")
    WebElement translationsTitleForSocialShare;    
    
    @FindBy(name = "socialShareDescription")
    WebElement translationsDescriptionForSocialShare;
    
	@FindBy(xpath = "//input[contains(@id,'auto-Actor')]")
	WebElement translationsActor;
	
	@FindBy(xpath = "//input[contains(@id,'auto-Character')]")
	WebElement translationsCharacter;
    
    //------------------------------------------ Quick Filling --------------------------------------------------------
    @FindBy(xpath = "//span[contains(text(),'Quick Filing')]")
    WebElement quickFilling;
    
    @FindBy(xpath = "//li[contains(text(),'Create Quick Filing')]")
    WebElement createQuickFiling;
    
    @FindBy(xpath = "//li[contains(text(),'Single Landing Page')]")
    WebElement createSingleLanding;
    
    
    //------------------------------------------Listing Page ---------------------------------------
  
    @FindBy(xpath = "//button[contains(@class,'auto-dropdown-btn-TVShow')]")
    WebElement tvShowSearchType;
    
    @FindBy(xpath = "//button[contains(@class,'auto-dropdown-btn-Seasons')]")
    WebElement seasonsSearchType;
    
    @FindBy(xpath = "//button[contains(@class,'auto-dropdown-btn-Episodes')]")
    WebElement episodesSearchType;
  
    @FindBy(xpath = "//ul[@id='menu-list-grow']/li[1]")
    WebElement searchTVShow;
    
    @FindBy(xpath = "//ul[@id='menu-list-grow']/li[2]")
    WebElement searchSeasons;
    
    @FindBy(xpath = "//ul[@id='menu-list-grow']/li[3]")
    WebElement searchEpisodes;
  
    @FindBy(xpath = "(//div[@class = 'license-badge'])[1]")
    WebElement listingExpiringLicense;
 
    @FindBy(xpath = "(//div[contains(@class,'auto-mov-link-btn')])[1]")
    WebElement listingLinkTVShows;
    
    @FindBy(xpath = "(//a)[3]")
    WebElement listingLicenseCountries;
    
    @FindBy(xpath = "//span[contains(text(),'Close')]")
    WebElement listingCloseLicenseCountries;  
    
    @FindBy(xpath = "(//span[@class='pub-history'])[1]")
    WebElement listingPublishedHistory;    
    
    @FindBy(xpath = "//div[@class='side-close-btn']")
    WebElement listingClosePublishedHistory;
    
    @FindBy(xpath = " (//div[@data-test='tvshow-child-arrow'])[1]")
    WebElement listingSeasonsArrow;
    
    @FindBy(xpath = "//li[@class='auto-leftTab-All']")
    WebElement listingAllStatus;
    	
    @FindBy(xpath = "//li[@class='auto-leftTab-Draft']")
    WebElement listingDraftStatus;
    
    @FindBy(xpath = "//li[@class='auto-leftTab-Changed']")
    WebElement listingChangedStatus;
    
    @FindBy(xpath = "//li[@class='auto-leftTab-Published']")
    WebElement listingPublishedStatus;
    
    @FindBy(xpath = "//li[@class='auto-leftTab-Unpublished']")
    WebElement listingUnpublishedStatus;
    
    @FindBy(xpath = "//li[@class='auto-leftTab-NeedWork']")
    WebElement listingNeedWorkStatus;
    
    @FindBy(xpath = "//li[@class='auto-leftTab-Scheduled']")
    WebElement listingScheduledStatus;
    
    @FindBy(xpath = "//li[@class='auto-leftTab-SubmittedToReview']")
    WebElement listingSubmittedToReviewStatus;
    
    @FindBy(xpath = "//li[@class='auto-leftTab-Archived']")
    WebElement listingArchivedStatus;
    
    @FindBy(xpath = "//li[@class='auto-leftTab-PublishingQueue']")
    WebElement listingPublishingQueueStatus;
	
    //----------------------------------------- Common Elements -----------------------------------------------------------
    @FindBy(className = "next-step-btn")
    WebElement nextButton;
    
    @FindBy(className = "prev-step-btn")
    WebElement previousButton;    
	

    @FindBy(css = "button.MuiButtonBase-root.MuiIconButton-root.MuiAutocomplete-popupIndicator.MuiAutocomplete-popupIndicatorOpen > span.MuiIconButton-label > svg.MuiSvgIcon-root")
    WebElement closeDropdown;

    @FindBy(xpath = "//div[contains(@class,'auto-TVShow')]")
    WebElement tvShowButton;

    @FindBy(xpath = "//div[contains(text(),'Movie')]")
    WebElement movieButton;

    @FindBy(id = "view-icon_svg__Rectangle_273")
    List < WebElement > editButton;

    @FindBy(xpath = "//span[contains(text(),'Edit TV Show')]")
    WebElement editTvShow;

    @FindBy(name = "searchString")
    WebElement searchString;

    @FindBy(xpath = "//span[contains(text(),'Yes') or contains(text(),'Ok')]")
    List < WebElement > yes;

    @FindBy(xpath = "//span[contains(text(),'No')]")
    WebElement no;
    
    @FindBy(xpath = "//span[contains(text(),'Ok')]//ancestor::button")
    WebElement ok;
    
    @FindBy(xpath = "//span[contains(text(),'Ok') or contains(text(),'Yes')]")
    WebElement scheduleConfirm;  
  

    @FindBy(xpath = "//div[contains(@class,'mark-fill-active')]")
    WebElement doneButtonActive; // rgba(255, 255, 255, 1)

    @FindBy(xpath = "//li[@role='option' or @role='menuitem']")
    List < WebElement > dropDownList;

    @FindBy(xpath = "//div[contains(@class,'mark-active')]")
    WebElement doneButtonClicked; // rgba(52, 194, 143, 1)

    @FindBy(xpath = "//button[contains(@class,'auto-tab-Dashboard')]")
    WebElement dashboard;

    @FindBy(css = "button.MuiButtonBase-root.MuiIconButton-root.MuiAutocomplete-popupIndicator.MuiAutocomplete-popupIndicatorOpen > span.MuiIconButton-label > svg.MuiSvgIcon-root")
    WebElement filterCloseDropdown;

    @FindBy(xpath = ("//input[@type='checkbox']"))
    List < WebElement > checkBox;
    
    @FindBy(xpath ="//span[contains(text(),'Save')]")
    WebElement save;

    @FindBy(css = "span.MuiIconButton-label > svg.MuiSvgIcon-root.MuiSvgIcon-fontSizeSmall")
    WebElement clearInput;
    
    @FindBy(xpath = "//a[contains(text(),'TV Shows')]")
    WebElement tvShowsLink;
    
    @FindBy(id = "assignedContent")
    WebElement assignContent;    
    
    @FindBy(xpath = "//a[contains(@class,'auto-breadCrum-UpdateTVShow')]")
    WebElement updateTVShowBreadcrumb;
    
    @FindBy(xpath = "//a[contains(@class,'auto-breadCrum-TVShow')]")
    WebElement tvShowBreadcrumb;																			
	 	
    @FindBy(xpath = "//div[@class = 'loc-icon']")
    WebElement unlock;
    
    
    //---------------------------- Listing Page Search --------------------------------------------------------------------------------
    
    
    @FindBy(xpath="//button[contains(@class,'TVShow')]")
    WebElement tvShowSearch;
    
    @FindBy(xpath="//button[contains(@class,'Seasons')]")
    WebElement seasonSearch;
    
    @FindBy(xpath="//button[contains(@class,'Episodes')]")
    WebElement episodesSearch;
    
    @FindBy(className = "head-external-id")
    WebElement tvShowExternalID;  
    
    //-----------------------------Initializing the page objects-----------------------------

    public TvShowsPage() {
    	PageFactory.initElements(driver, this); 
	} 
         
  

    //----------------------------------------------- Main Content ---------------------------------------------------------

    public void TvProperties() throws InterruptedException, Exception {
    	 WebUtility.Click(dashboard);
         JavascriptExecutor js = (JavascriptExecutor) driver;
         WebUtility.Click(tvShowButton);
/*
        //------EDIT Flow for Testing ----------------------------------------------
  
		searchString.click();
		searchString.sendKeys("The Expanse");
		Thread.sleep(2000);
		WebUtility.element_to_be_clickable(editButton.get(0));
		editButton.get(1).click();
		Thread.sleep(2000);
		editTvShow.click();
*/

       WebUtility.Click(createTvShow);

        // Content Properties section -----------------------------------------
        WebUtility.Click(tvShowProperties);
  
        externalID = js.executeScript("return arguments[0].childNodes[1].innerText", tvShowExternalID).toString();	
        WebUtility.Click(note);
        note.sendKeys(Keys.CONTROL + "a");
        note.sendKeys(Keys.DELETE);
        note.sendKeys("Automation Test");

        title.sendKeys(Keys.CONTROL + "a");
        title.sendKeys(Keys.DELETE);
        title.sendKeys(tvShowTitle);

        shortDesc.sendKeys("The Expanse is an American science fiction television series developed by Mark Fergus and Hawk Ostby, based on the series of novels of the same name by James S. A. Corey.");
         
        webDesc.sendKeys("Automation Test");

        appDesc.sendKeys("Automation Test");

        tvDesc.sendKeys("Automation Test");

        WebUtility.Click(subType);
        WebUtility.Click(dropDownList.get(0));

        WebUtility.Click(category);
        WebUtility.Click(dropDownList.get(0));

        WebUtility.Click(primaryGenre);
        WebUtility.Click(dropDownList.get(0));

        WebUtility.Click(secondaryGenre);
        WebUtility.Click(dropDownList.get(0));

        WebUtility.Click(thematicGenre);
        WebUtility.Click(dropDownList.get(0));

        WebUtility.Click(settingGenre);
        WebUtility.Click(dropDownList.get(0));        

        WebUtility.Click(rcsCategory);
        WebUtility.Click(dropDownList.get(0));

        WebUtility.Click(creativeTitle);
        creativeTitle.sendKeys(Keys.CONTROL + "a");
        creativeTitle.sendKeys(Keys.BACK_SPACE);
        creativeTitle.sendKeys("Automation Creative Title");

        alternateTitle.sendKeys(Keys.CONTROL + "a");
        alternateTitle.sendKeys(Keys.BACK_SPACE);
        alternateTitle.sendKeys("Automation Alternative Title");

        pageTitle.sendKeys(Keys.CONTROL + "a");
        pageTitle.sendKeys(Keys.BACK_SPACE);
        pageTitle.sendKeys("Automation Page Title");

        pageDescription.sendKeys(Keys.CONTROL + "a");
        pageDescription.sendKeys(Keys.BACK_SPACE);
        pageDescription.sendKeys("Automation Page Description");

        titleForSocialShare.sendKeys(Keys.CONTROL + "a");
        titleForSocialShare.sendKeys(Keys.BACK_SPACE);
        titleForSocialShare.sendKeys("Automation Title for Social Share");

        descriptionForSocialShare.sendKeys(Keys.CONTROL + "a");
        descriptionForSocialShare.sendKeys(Keys.BACK_SPACE);
        descriptionForSocialShare.sendKeys("Automation Description for Social Share");
    
        WebUtility.Click(zee5ReleaseDate);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ESCAPE);
        
        WebUtility.Click(telecastDate);
        datePicker.sendKeys(Keys.ARROW_LEFT);
        datePicker.sendKeys(Keys.ESCAPE);    
        
		WebUtility.Click(audioLanguage);
		WebUtility.Click(dropDownList.get(0));
		
		multiAudio.click();

		WebUtility.Click(contentLanguage);
		WebUtility.Click(dropDownList.get(0));

		WebUtility.Click(primaryLanguage);
		WebUtility.Click(dropDownList.get(0));

		WebUtility.Click(dubbedLanguageTitle);
		WebUtility.Click(dropDownList.get(0));

		WebUtility.Click(originalLanguage);
		WebUtility.Click(dropDownList.get(0));
		
		WebUtility.Click(subtitleLanguages);
        WebUtility.Click(dropDownList.get(0));

        WebUtility.Click(contentVersion);
        WebUtility.Click(dropDownList.get(0));

        WebUtility.Click(theme);
        WebUtility.Click(dropDownList.get(0));

        contractId.sendKeys("Automation Contract ID");

        WebUtility.Click(specialCategory);
        WebUtility.Click(dropDownList.get(0));

        WebUtility.Click(specialCategoryCountry);
        WebUtility.Click(dropDownList.get(0));

        
        WebUtility.Click(specialCategoryFrom);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ESCAPE);

        WebUtility.Click(specialCategoryTo);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ESCAPE);

        WebUtility.Click(nextButton);

		WebUtility.Click(broadcastState);
		WebUtility.Click(dropDownList.get(0));
		
        WebUtility.Click(rating);
        WebUtility.Click(dropDownList.get(0));

        WebUtility.Click(contentOwner);
        WebUtility.Click(dropDownList.get(0));

        WebUtility.Click(originCountry);
        WebUtility.Click(dropDownList.get(0));

		contentProduction.click();
		WebUtility.element_to_be_clickable(dropDownList.get(0));
		dropDownList.get(0).click();
	
        WebUtility.Click(upcomingPage);
        upcomingPage.sendKeys(Keys.CONTROL + "a");
        upcomingPage.sendKeys(Keys.BACK_SPACE);
        upcomingPage.sendKeys("Automation Upcoming Page Text");

        WebUtility.Click(contentDescriptor);
        WebUtility.Click(dropDownList.get(0));
		
        WebUtility.Click(certification);
        WebUtility.Click(dropDownList.get(0));

        WebUtility.Click(emotions);
        WebUtility.Click(dropDownList.get(0));

        WebUtility.Click(contentGrade);
        WebUtility.Click(dropDownList.get(0));

        WebUtility.Click(era);
        WebUtility.Click(dropDownList.get(0));

        WebUtility.Click(targetAge);
        WebUtility.Click(dropDownList.get(0));

        WebUtility.Click(targetAudiences);
        WebUtility.Click(dropDownList.get(0));      
  
		tags.sendKeys("Automation Tags");
		WebUtility.Wait();
		WebUtility.Click(dropDownList.get(0));

        WebUtility.Click(digitalKeywords);
        digitalKeywords.sendKeys(Keys.CONTROL + "a");
        digitalKeywords.sendKeys(Keys.BACK_SPACE);
        digitalKeywords.sendKeys("Automation Digital Keywords");

        WebUtility.Click(adaptation);
        adaptation.sendKeys(Keys.CONTROL + "a");
        adaptation.sendKeys(Keys.BACK_SPACE);
        adaptation.sendKeys("Automation Adaption");
        adaptation.sendKeys(Keys.ENTER);

        WebUtility.Click(events);
        events.sendKeys(Keys.CONTROL + "a");
        events.sendKeys(Keys.BACK_SPACE);
        events.sendKeys("Automation Event");

        WebUtility.Click(productionCompany);
        WebUtility.Click(dropDownList.get(0));

        WebUtility.Click(popularityScore);
		popularityScore.sendKeys(Keys.CONTROL + "a");
		popularityScore.sendKeys(Keys.BACK_SPACE);
		popularityScore.sendKeys("50");

		WebUtility.Click(trivia);
	    trivia.clear();
	    trivia.sendKeys("Automation Trivia");

		WebUtility.Click(epgProgramId);
		epgProgramId.sendKeys(Keys.CONTROL + "a");
		epgProgramId.sendKeys(Keys.BACK_SPACE);
		epgProgramId.sendKeys("Automation Data");

		WebUtility.Click(channel);
		WebUtility.Click(dropDownList.get(0));

		showAirTime.click();
		List<WebElement> airTime = driver.findElements(By.xpath("//li[@role='button']"));
		for (WebElement e : airTime) {
			String videohrs = e.getText();
			if (videohrs.contains("02")) {
				e.click();
				break;
			}
		}

		WebUtility.Click(showAirTimeDays);
		WebUtility.Click(dropDownList.get(0));
		
		WebUtility.Click(officialSite);
		officialSite.sendKeys("http://www.zee5.com");

		WebUtility.Click(awardRecipient);
		awardRecipient.sendKeys("Test");
		WebUtility.Click(dropDownList.get(0));

		WebUtility.Click(awardsCategory);
		WebUtility.Click(dropDownList.get(0));

		WebUtility.Click(awardHonor);
		awardHonor.sendKeys(Keys.CONTROL + "a");
		awardHonor.sendKeys(Keys.BACK_SPACE);
		awardHonor.sendKeys("Automation Award");
		
		//---TODO GLobal Fields have not been implemented yet ---------------------------
/*
		Thread.sleep(2000);
		nextButton.click();
		Thread.sleep(2000);
		
		//driver.findElement(By.xpath("//span[contains(text(),'Global')]")).click();
		WebUtility.element_to_be_clickable(globalCountry);
		globalCountry.click();
		WebUtility.element_to_be_clickable(dropDownList.get(0));
		dropDownList.get(0).click();
		
		globalTitle.click();
		globalTitle.clear();
		globalTitle.sendKeys(Keys.CONTROL + "a");
		globalTitle.sendKeys(Keys.BACK_SPACE);
		Thread.sleep(1000);
		globalTitle.sendKeys("Automation Test Data");
		
		globalCreativeTitle.click();
		globalCreativeTitle.sendKeys(Keys.CONTROL + "a");
		globalCreativeTitle.sendKeys(Keys.BACK_SPACE);
		globalCreativeTitle.sendKeys("Automation Test Data");
		
		globalAlternativeTitle.click();
		globalAlternativeTitle.sendKeys(Keys.CONTROL + "a");
		globalAlternativeTitle.sendKeys(Keys.BACK_SPACE);
		globalAlternativeTitle.sendKeys("Automation Test Data");
		
		globalPageTitle.click();
		globalPageTitle.sendKeys(Keys.CONTROL + "a");
		globalPageTitle.sendKeys(Keys.BACK_SPACE);
		globalPageTitle.sendKeys("Automation Test Data");
		
		globalPageDescription.click();
		globalPageDescription.sendKeys(Keys.CONTROL + "a");
		globalPageDescription.sendKeys(Keys.BACK_SPACE);
		Thread.sleep(2000);
		globalPageDescription.sendKeys("Automation Test Data");
		
		globalSocialShareTitle.click();
		globalSocialShareTitle.clear();
		globalSocialShareTitle.sendKeys(Keys.CONTROL + "a");
		globalSocialShareTitle.sendKeys(Keys.BACK_SPACE);
		Thread.sleep(2000);
		globalSocialShareTitle.sendKeys("Automation Test Data");
				
  
		zee5ReleaseDate.click();
		zee5ReleaseDate.sendKeys(Keys.SPACE);
		zee5ReleaseDate.sendKeys(Keys.ARROW_RIGHT);
		zee5ReleaseDate.sendKeys(Keys.ARROW_RIGHT);
		zee5ReleaseDate.sendKeys(Keys.ENTER);
		
		globalTelecastDate.click();
		globalTelecastDate.sendKeys(Keys.SPACE);
		globalTelecastDate.sendKeys(Keys.ARROW_RIGHT);
		globalTelecastDate.sendKeys(Keys.ARROW_RIGHT);
		globalTelecastDate.sendKeys(Keys.ENTER);
		
		globalSubtitleLanguages.click();
		WebUtility.element_to_be_clickable(dropDownList.get(0));
		dropDownList.get(0).click();
		
		globalDescriptionForSocialShare.click();
		globalDescriptionForSocialShare.sendKeys(Keys.CONTROL + "a");
		globalDescriptionForSocialShare.sendKeys(Keys.BACK_SPACE);
		Thread.sleep(2000);
		globalDescriptionForSocialShare.sendKeys("Automation Test Data");
		
		globalShortDesc.click();
		globalShortDesc.sendKeys(Keys.CONTROL + "a");
		globalShortDesc.sendKeys(Keys.BACK_SPACE);
		globalShortDesc.sendKeys("Automation Test Data");
		
		globalWebDesc.click();
		globalWebDesc.sendKeys(Keys.CONTROL + "a");
		globalWebDesc.sendKeys(Keys.BACK_SPACE);
		globalWebDesc.sendKeys("Automation Test Data");
		
		globalAppDesc.click();
		globalAppDesc.sendKeys(Keys.CONTROL + "a");
		globalAppDesc.sendKeys(Keys.BACK_SPACE);
		globalAppDesc.sendKeys("Automation Test Data");
		
		globalTVDesc.click();
		globalTVDesc.sendKeys(Keys.CONTROL + "a");
		globalTVDesc.sendKeys(Keys.BACK_SPACE);
		globalTVDesc.sendKeys("Automation Test Data");
		
		broadcastState.click();
		WebUtility.element_to_be_clickable(dropDownList.get(0));
		dropDownList.get(0).click();
		
		globalUpcomingPage.click();
		globalUpcomingPage.clear();
		globalUpcomingPage.sendKeys("Automation Test Data");
		
		globalTitle.click();
		*/
		js.executeScript("window.scrollTo(0,0);");
		Thread.sleep(2000);
		String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
		sa.assertTrue(doneButtonActiveColor.contains("255, 255, 255") || doneButtonActiveColor.contains("255,255,255"), "TV Shows Properties Mark As Done Assertion Fails");
		WebUtility.Click(doneButtonActive);
		WebUtility.Wait();

    }

    public void TvPropertiesAssertions() throws InterruptedException {
        JavascriptExecutor js = (JavascriptExecutor) driver;
    /*    
        //------EDIT Flow for Testing ----------------------------------------------
	    tvShowButton.click();
        Thread.sleep(2000);
		searchString.click();
		searchString.sendKeys("The Expanse Suite");
		Thread.sleep(2000);
		WebUtility.element_to_be_clickable(editButton.get(0));
		editButton.get(0).click();
		Thread.sleep(2000);
		editTvShow.click();
		Thread.sleep(2000);
*/	 	 
    	WebUtility.Click(titleSummary);
        String expectedTitle = tvShowTitle;
        Assert.assertEquals(title.getAttribute("value"), expectedTitle, "Title Assertion Failed");
       // js.executeScript("arguments[0].style.background = 'Green'", title);																		   

        String expectedShortDescription = "The Expanse is an American science fiction television series developed by Mark Fergus and Hawk Ostby, based on the series of novels of the same name by James S. A. Corey.";
        List < WebElement > shortDescValue = driver.findElements(By.className("ql-editor"));
        Assert.assertEquals(shortDescValue.get(0).getAttribute("innerText"), expectedShortDescription, "Short Desc Assertion Fails");
       // js.executeScript("arguments[0].style.background = 'Green'", shortDesc);
		
        js.executeScript("window.scrollTo(0,document.body.scrollHeight);");

        Object subTypeDropDown = js.executeScript("var dropDown=document.getElementById(\"subtype\");if(dropDown.value==\"\")var dropDownValue=false;else var dropDownValue=true; return dropDownValue");
        Assert.assertEquals(subTypeDropDown, true, "Sub Type Assertion Fails");
		//js.executeScript("arguments[0].style.background = 'Green'", subType);																	 

        Object contentCategoryDropDown = js.executeScript("var dropDown=document.getElementById(\"category\");if(dropDown.value==\"\")var dropDownValue=false;else var dropDownValue=true; return dropDownValue");
        Assert.assertEquals(contentCategoryDropDown, true, "Content Category Assertion Fails");
		//js.executeScript("arguments[0].style.background = 'Green'", category);																	  

        Assert.assertNotNull(primaryGenreAssertion.getText(),"Primary Genre Dropdown is Empty");
     
        Assert.assertNotNull(secondaryGenreAssertion.getText(),"Secondary Genre Dropdown is Empty");        

        Object zee5ReleaseDateValueExists = js.executeScript("var dropDown=document.getElementsByName('dateZee5Published');if(dropDown[0].defaultValue=='')var dropDownValue=false;else var dropDownValue=true; return dropDownValue");
        Assert.assertEquals(zee5ReleaseDateValueExists, true, "Zee5 Release Date Assertion Failed");

        Assert.assertNotNull(audioLanguagesAssertion.getText(),"Audio Language Dropdown is Empty");

        Object primaryLanguageDropDown = js.executeScript("var dropDown=document.getElementById('primaryLanguage');if(dropDown.value=='')var dropDownValue=false;else var dropDownValue=true; return dropDownValue");
        Assert.assertEquals(primaryLanguageDropDown, true, "Primary Language Assertion Fails");

        Assert.assertNotNull(dubbedLanguageAssertion.getText(),"Dubbed Language Dropdown is Empty");
        
        Object originalLanguageDropDown = js.executeScript("var dropDown=document.getElementById('originalLanguage');if(dropDown.value=='')var dropDownValue=false;else var dropDownValue=true; return dropDownValue");
        Assert.assertEquals(originalLanguageDropDown, true, "Original Language Assertion Fails");

        Object contentVersionDropDown = js.executeScript("var dropDown=document.getElementById('contentVersion');if(dropDown.value=='')var dropDownValue=false;else var dropDownValue=true; return dropDownValue");
        Assert.assertEquals(contentVersionDropDown, true, "Content Version Assertion Fails");

        WebUtility.Click(nextButton);

        js.executeScript("window.scrollTo(0,0);");
        //contentOwner.click();
      
        Object contentOwnerDropDown = js.executeScript("var dropDown=document.getElementById('contentOwner');if(dropDown.value=='')var dropDownValue=false;else var dropDownValue=true; return dropDownValue");
        Assert.assertEquals(contentOwnerDropDown, true, "Content Owner Assertion Fails");

       
        Object certificationDropDown = js.executeScript("var dropDown=document.getElementById('certification');if(dropDown.value=='')var dropDownValue=false;else var dropDownValue=true; return dropDownValue");
        Assert.assertEquals(certificationDropDown, true, "Certification Assertion Fails");
        // Assert.assertNotNull(certificationAssertion.getText(),"Certification Dropdown is Empty");

        String doneButtonClickedColor = doneButtonClicked.getCssValue("background-color");
        Assert.assertTrue(doneButtonClickedColor.contains("52, 194, 143"), "TV Shows Properties Mark As Done Assertion Fails");

    }

    public void TvCastCrew() throws InterruptedException {
        JavascriptExecutor js = (JavascriptExecutor) driver;     
        
        WebUtility.Click(castCrew);
        try {
            WebUtility.Click(actor);
            actor.sendKeys("Salman Khan");
            WebUtility.Click(dropDownList.get(0));

            WebUtility.Click(character);
            Thread.sleep(2000);
            character.sendKeys("Test Character");

            WebUtility.Wait();
            WebUtility.Click(addActor);
            WebUtility.Click(actor2);
            actor2.sendKeys("Salman Khan II");
            WebUtility.element_to_be_visible(dropDownList.get(0));
            actor2.sendKeys(Keys.ARROW_DOWN);
            actor2.sendKeys(Keys.ENTER);
            WebUtility.Click(character2);
            character2.sendKeys("Test Character");

            WebUtility.Wait();
            WebUtility.Click(addActor);
            WebUtility.Click(actor3);
            actor3.sendKeys("Salman Khan III");
            WebUtility.element_to_be_visible(dropDownList.get(0));
            actor3.sendKeys(Keys.ARROW_DOWN);
            actor3.sendKeys(Keys.ENTER);
            WebUtility.Click(character3);
            character3.sendKeys("Test Character");

            WebUtility.Wait();
            WebUtility.Click(removeActor.get(1));
            WebUtility.Wait();

            actorChange.sendKeys("Test Actor Change");
            WebUtility.Wait();
            WebUtility.Click(dropDownList.get(0));
            
            performer.sendKeys("Test Performer");
            WebUtility.Wait();
            WebUtility.Click(dropDownList.get(0));

            host.sendKeys("Test Host");
            WebUtility.Wait();
            WebUtility.Click(dropDownList.get(0));

            singer.sendKeys("Automation Test Data Singer");
            WebUtility.Wait();
            WebUtility.Click(dropDownList.get(0));

            lyricst.sendKeys("Automation Test Data Lyricst");
            WebUtility.Wait();
            WebUtility.Click(dropDownList.get(0));

            director.sendKeys("Automation Test Data Director");
            WebUtility.Wait();
            WebUtility.Click(dropDownList.get(0));

            cinematography.sendKeys("Automation Test Data Cinematography");
            WebUtility.Wait();
            WebUtility.Click(dropDownList.get(0));

            producer.sendKeys("Automation Test Data Producer");
            WebUtility.Wait();
            WebUtility.Click(dropDownList.get(0));

            executiveProducer.sendKeys("Automation Test Data Ex Producer");
            WebUtility.Wait();
            WebUtility.Click(dropDownList.get(0));

            musicDirector.sendKeys("Automation Test Data Music Director");
            WebUtility.Wait();
            WebUtility.Click(dropDownList.get(0));

            choreographer.sendKeys("Automation Test Data Choreographer");
            WebUtility.Wait();
            WebUtility.Click(dropDownList.get(0));

            titleThemeMusic.sendKeys("Automation Test Data Theme Music");
            WebUtility.Wait();
            WebUtility.Click(dropDownList.get(0));

            backgroundScore.sendKeys("Automation Test Data Background Score");
            WebUtility.Wait();
            WebUtility.Click(dropDownList.get(0));

            storyWriter.sendKeys("Automation Test Data Story Writer");
            WebUtility.Wait();
            WebUtility.Click(dropDownList.get(0));

            screenPlay.sendKeys("Automation Test Data Screen Play");
            WebUtility.Wait();
            WebUtility.Click(dropDownList.get(0));

            dialogueWriter.sendKeys("Automation Test Data Dialogue Writer");
            WebUtility.Wait();
            WebUtility.Click(dropDownList.get(0));

            filmEditing.sendKeys("Automation Test Data Film Editing");
            WebUtility.Wait();
            WebUtility.Click(dropDownList.get(0));

            casting.sendKeys("Automation Test Data Casting");
            WebUtility.Wait();
            WebUtility.Click(dropDownList.get(0));

            productionDesign.sendKeys("Automation Test Data Production Design");
            WebUtility.Wait();
            WebUtility.Click(dropDownList.get(0));

            artDirection.sendKeys("Automation Test Data Art Direction");
            WebUtility.Wait();
            WebUtility.Click(dropDownList.get(0));

            setDecoration.sendKeys("Automation Test Data Set Decoration");
            WebUtility.Wait();
            WebUtility.Click(dropDownList.get(0));

            costumeDesign.sendKeys("Automation Test Data Costume Design");
            WebUtility.Wait();
            WebUtility.Click(dropDownList.get(0));

            productionCo.sendKeys("Automation Test Data Production Co");
            WebUtility.Wait();
            WebUtility.Click(dropDownList.get(0));

            presenter.sendKeys("Automation Test Data Presenter");
            WebUtility.Wait();
            WebUtility.Click(dropDownList.get(0));

            guest.sendKeys("Automation Test Data Guest");
            WebUtility.Wait();
            WebUtility.Click(dropDownList.get(0));

            participant.sendKeys("Automation Test Data Participant");
            WebUtility.Wait();
            WebUtility.Click(dropDownList.get(0));

            judges.sendKeys("Automation Test Data Judges");
            WebUtility.Wait();
            WebUtility.Click(dropDownList.get(0));

            narrator.sendKeys("Automation Test Data Narrator");
            WebUtility.Wait();
            WebUtility.Click(dropDownList.get(0));

            sponsor.sendKeys("Automation Test Data Sponsor");
            WebUtility.Wait();
            WebUtility.Click(dropDownList.get(0));

            graphics.sendKeys("Automation Test Data Graphics");
            WebUtility.Wait();
            WebUtility.Click(dropDownList.get(0));

            vocalist.sendKeys("Automation Test Data Vocalist");
            WebUtility.Wait();
            WebUtility.Click(dropDownList.get(0));

            String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
            sa.assertTrue(doneButtonActiveColor.contains("255, 255, 255") || doneButtonActiveColor.contains("255,255,255"), "Video Cast & Crew Mark As Done Assertion Fails");
            WebUtility.Click(doneButtonActive);


            try {
                TestUtil.captureScreenshot("TV Show Cast and Crew Section","TV Show");
            } catch (IOException e) {
                System.out.println("Screenshot Capture Failed ->" + e.getMessage());
            }

            js.executeScript("window.scrollTo(0,document.body.scrollHeight);");

            try {
                TestUtil.captureScreenshot("TV Show Cast and Crew Section 2","TV Show");
            } catch (IOException e) {
                System.out.println("Screenshot Capture Failed ->" + e.getMessage());
            }
        } catch (Exception e) {
            WebUtility.Click(doneButtonActive);
        }
        
    }


    public void TvCastCrewAssertions() throws InterruptedException {
        WebUtility.Click(castCrew);

        String expectedActor = "Salman Khan";
        Assert.assertEquals(actor.getAttribute("value"), expectedActor, "Cast & Crew Actor Assertion Failed");

        String expectedCharacter = "Test Character";
        Assert.assertEquals(character.getAttribute("value"), expectedCharacter, "Cast & Crew Character Assertion Failed");

        String doneButtonClickedColor = doneButtonClicked.getCssValue("background-color");
        Assert.assertTrue(doneButtonClickedColor.contains("52, 194, 143"), "TV Shows Cast & Crew Mark As Done Assertion Fails");
    }

    
    public void TvLicense() throws InterruptedException {

/*
        //------EDIT Flow for Testing ----------------------------------------------
		tvShowButton.click();
		Thread.sleep(2000);
		searchString.click();
		searchString.sendKeys("The Expanse");
		WebUtility.Wait();
		WebUtility.Wait();
		editButton.get(0).click();
		WebUtility.Wait();
		editTvShow.click();
		WebUtility.Wait();

*/
        WebUtility.Click(license);

        try
        {
	        WebUtility.Click(createLicense);	
 
	        WebUtility.Wait();
	        useTemplateRadioButton.click();	
 
	        WebUtility.Click(selectTemplate);
	        WebUtility.Click(dropDownList.get(0));
	        js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
	        try {
				int licenseTemplateSize = licenseTemplateDeleteSets.size();
				for(int i = 1;i< licenseTemplateSize ;i++)
				{
					System.out.println("License Sets Size--->"+licenseTemplateDeleteSets.size());
					licenseTemplateDeleteSets.get(1).click();
					Thread.sleep(1000);
					System.out.println("License Sets Size After Delete--->"+licenseTemplateDeleteSets.size());
				}
			}
			catch (Exception e){
				System.out.println("No Sets in License Template");
			}
	        WebUtility.Click(editLicense);
	        WebUtility.Click(editLicenseCountry);
	        WebUtility.Click(clearInput);	        
	        for (int x = 0; x < dropDownList.size(); x++) {
	        	if(x<4) {
	            WebUtility.Click(dropDownList.get(x));
	        	}
	        	else {
	        		break;
	        	}
	        }
	        WebUtility.Wait();
	        WebUtility.Click(editLicenseCountry);
	        WebUtility.Wait();
	        WebUtility.javascript(saveLicense);
	        WebUtility.Wait();

			WebUtility.Click(licenseSetName);
	        licenseSetName.sendKeys("Automation Test Data");
		 
		  	WebUtility.Click(licenseFromDate);
	        datePicker.sendKeys(Keys.ARROW_RIGHT);
	        datePicker.sendKeys(Keys.ESCAPE);
	
	 		WebUtility.Click(licenseToDate);
	        datePicker.sendKeys(Keys.ARROW_RIGHT);
	        datePicker.sendKeys(Keys.ARROW_RIGHT);
	        datePicker.sendKeys(Keys.ESCAPE);
	
	     
	        WebUtility.Click(saveLicense);
	        WebUtility.Wait();
        }
        catch(Exception e)
        {
        	System.out.println("Creating a license through template Failed!!");
        	e.printStackTrace();
        }
        try
        {
	        WebUtility.Click(createLicense);
	
			WebUtility.Click(licenseSetName);
	        licenseSetName.sendKeys("Automation Test Data II");							  
		 
	        WebUtility.Click(licenseFromDate);
	        datePicker.sendKeys(Keys.ARROW_RIGHT);
	        datePicker.sendKeys(Keys.ARROW_RIGHT);
	        datePicker.sendKeys(Keys.ARROW_RIGHT);
	        datePicker.sendKeys(Keys.ESCAPE);
	
	        WebUtility.Click(licenseToDate);
	        datePicker.sendKeys(Keys.ARROW_RIGHT);
	        datePicker.sendKeys(Keys.ARROW_RIGHT);
	        datePicker.sendKeys(Keys.ARROW_RIGHT);
	        datePicker.sendKeys(Keys.ARROW_RIGHT);
	        datePicker.sendKeys(Keys.ESCAPE);
	
	        WebUtility.Click(licenseCountry);
	        for (int x = 0; x < dropDownList.size(); x++) {
	        	if(x<4) {
	            WebUtility.Click(dropDownList.get(x));
	        	}
	        	else {
	        		break;
	        	}
	        }	
	        WebUtility.Click(licenseBusinessType);
	        WebUtility.Click(dropDownList.get(0));
	
	        WebUtility.Click(licensePlatform);
	        WebUtility.Click(dropDownList.get(0));
	
	        WebUtility.Click(licenseTVOD);
	        WebUtility.Click(dropDownList.get(0));
		       
						   
	        WebUtility.javascript(saveLicense);
	        WebUtility.Wait();
        }
        catch(Exception e)
        {
        	System.out.println("Creating a Manual License Failed");
        	e.printStackTrace();
        }

        String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
        sa.assertTrue(doneButtonActiveColor.contains("255, 255, 255") || doneButtonActiveColor.contains("255,255,255"), "TV Show License Mark As Done Assertion Fails");
        WebUtility.Click(doneButtonActive);
        WebUtility.Wait();

        try {
            TestUtil.captureScreenshot("Main Content License Section","TV Show");
        } catch (IOException e) {
            System.out.println("Screenshot Capture Failed ->" + e.getMessage());
        }


        WebUtility.Click(expiredLicense);
        Thread.sleep(2000);
        WebUtility.Click(expiredLicenseBack);


        WebUtility.Click(licenseFilters);
        WebUtility.Click(licenseFromDate);
        datePicker.sendKeys(Keys.ARROW_LEFT);
        datePicker.sendKeys(Keys.ARROW_LEFT);
        datePicker.sendKeys(Keys.ESCAPE);


        WebUtility.Click(licenseToDate);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ESCAPE);

        WebUtility.Click(applyFilter);

        Thread.sleep(2000);
        WebUtility.Click(licenseFilters);
        WebUtility.Click(clearFilter);
        
        WebUtility.Click(licenseFilters);
        WebUtility.Click(licenseFilterBusinessType);
        WebUtility.Click(dropDownList.get(0));
        WebUtility.Wait();
        
        WebUtility.Click(applyFilter);

        Thread.sleep(2000);
        WebUtility.Click(licenseFilters);
        WebUtility.Click(clearFilter);

        WebUtility.Click(licenseFilters);
        WebUtility.Click(licenseFilterBillingType);
        WebUtility.Click(dropDownList.get(0));
        WebUtility.Wait();
        WebUtility.Click(applyFilter);

        Thread.sleep(2000);
        WebUtility.Click(licenseFilters);
        WebUtility.Click(clearFilter);

        WebUtility.Click(licenseFilters);
        WebUtility.Click(licenseFilterPlatform);
        WebUtility.Click(dropDownList.get(0));
        WebUtility.Wait();
        filterCloseDropdown.click();
        WebUtility.Click(applyFilter);

        Thread.sleep(2000);
        WebUtility.Click(licenseFilters);
        WebUtility.Click(clearFilter);

        WebUtility.Click(licenseFilters);
        WebUtility.Wait();
        licenseFilterActiveStatus.click();
        WebUtility.Wait();
        WebUtility.Click(applyFilter);

        Thread.sleep(2000);
        WebUtility.Click(licenseFilters);
        WebUtility.Click(clearFilter);

        WebUtility.Click(licenseFilters);
        WebUtility.Wait();
        licenseFilterInActiveStatus.click();
        WebUtility.Wait();
        WebUtility.Click(applyFilter);

        Thread.sleep(2000);
        WebUtility.Click(licenseFilters);
        WebUtility.Click(clearFilter);

    }

    public void TvImages() throws InterruptedException {
/*
	
         //------EDIT Flow for Testing ----------------------------------------------
		tvShowButton.click();
		Thread.sleep(2000);
		searchString.click();
		searchString.sendKeys("Test123");
		Thread.sleep(2000);
		WebUtility.element_to_be_clickable(editButton.get(0));
		editButton.get(0).click();
		Thread.sleep(2000);
		editTvShow.click();
		Thread.sleep(2000);
*/
 		JavascriptExecutor js = (JavascriptExecutor) driver;  
 		WebUtility.Click(images);
		
 		WebUtility.Wait();
        cover.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\CoverPNG.png");

        WebUtility.Wait();
        appcover.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\AppCoverPNG.png");

        WebUtility.Wait();
        list.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\List.jpg");

        WebUtility.Wait();
        square.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\Square.jpg");
        
        js.executeScript("window.scrollBy(0,600)");

        Thread.sleep(2000);
        tvCover.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\TVCover.jpg");

        WebUtility.Wait();
        portrait.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\portraitPNG.png");

        WebUtility.Wait();
        listClean.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\ListClean.jpg");

		js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
		
		WebUtility.Wait();
        portraitClean.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\PortraitCleanPNG.png");

        WebUtility.Wait();
        telcoSquare.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\TelcoSquare.jpg");

        WebUtility.Wait();
        passport.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\Passport.jpg");
        WebUtility.Wait();

		try {
			// ------------------ Image Set 2 --------------------------------
			WebUtility.element_to_be_fluentwait(imagesCreateNewSet);
			WebUtility.Click(imagesCreateNewSet);

			WebUtility.Click(newSetName);
			newSetName.sendKeys("Automation Test Data");

			newSetCountry.click();
			WebUtility.Click(dropDownList.get(0));
			closeDropdown.click();

			newSetPlatform.click();
			WebUtility.Click(dropDownList.get(0));
			closeDropdown.click();

			newSetGender.click();
			WebUtility.Click(dropDownList.get(0));
			closeDropdown.click();

			newSetGenre.click();
			WebUtility.Click(dropDownList.get(0));
			closeDropdown.click();

			newSetAgeGroup.click();
			WebUtility.Click(dropDownList.get(0));
			closeDropdown.click();

			newSetLanguage.click();
			WebUtility.Click(dropDownList.get(0));
			closeDropdown.click();

			newSetOthers.click();
			newSetOthers.sendKeys("Automation Test Data");

			WebUtility.Click(save);

			WebUtility.Click(imageSets.get(1));

			js.executeScript("window.scrollTo(0,0)");

	 		WebUtility.Wait();
	        cover.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\CoverPNG.png");

	        WebUtility.Wait();
	        appcover.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\AppCoverPNG.png");

	        WebUtility.Wait();
	        list.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\List.jpg");

	        WebUtility.Wait();
	        square.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\Square.jpg");
	        
	        js.executeScript("window.scrollBy(0,600)");

	        Thread.sleep(2000);
	        tvCover.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\TVCover.jpg");

	        WebUtility.Wait();
	        portrait.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\portraitPNG.png");

	        WebUtility.Wait();
	        listClean.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\ListClean.jpg");

			js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
			
			WebUtility.Wait();
	        portraitClean.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\PortraitCleanPNG.png");

	        WebUtility.Wait();
	        telcoSquare.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\TelcoSquare.jpg");

	        WebUtility.Wait();
	        passport.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\Passport.jpg");
	        WebUtility.Wait();
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			// -------------------------- Image Set 3
			// -----------------------------------------------------
			WebUtility.element_to_be_fluentwait(imagesCreateNewSet);
			WebUtility.Click(imagesCreateNewSet);

			WebUtility.Click(newSetName);
			newSetName.sendKeys("Automation Test Data II");

			newSetCountry.click();
			WebUtility.Click(dropDownList.get(1));
			closeDropdown.click();

			newSetPlatform.click();
			WebUtility.Click(dropDownList.get(1));
			closeDropdown.click();

			newSetGender.click();
			WebUtility.Click(dropDownList.get(1));
			closeDropdown.click();

			newSetGenre.click();
			WebUtility.Click(dropDownList.get(1));
			closeDropdown.click();

			newSetAgeGroup.click();
			WebUtility.Click(dropDownList.get(1));
			closeDropdown.click();

			newSetLanguage.click();
			WebUtility.Click(dropDownList.get(1));
			closeDropdown.click();

			newSetOthers.click();
			newSetOthers.sendKeys("Automation Test Data II");

			WebUtility.Click(save);

			WebUtility.Click(imageSets.get(2));

			js.executeScript("window.scrollTo(0,0)");

	 		WebUtility.Wait();
	        cover.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\CoverPNG.png");

	        WebUtility.Wait();
	        appcover.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\AppCoverPNG.png");

	        WebUtility.Wait();
	        list.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\List.jpg");

	        WebUtility.Wait();
	        square.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\Square.jpg");
	        
	        js.executeScript("window.scrollBy(0,600)");

	        Thread.sleep(2000);
	        tvCover.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\TVCover.jpg");

	        WebUtility.Wait();
	        portrait.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\portraitPNG.png");

	        WebUtility.Wait();
	        listClean.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\ListClean.jpg");

			js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
			
			WebUtility.Wait();
	        portraitClean.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\PortraitCleanPNG.png");

	        WebUtility.Wait();
	        telcoSquare.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\TelcoSquare.jpg");

	        WebUtility.Wait();
	        passport.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\Passport.jpg");
	        WebUtility.Wait();
		} catch (Exception e) {
			e.printStackTrace();
		}

		// ---------------------- Inactivate Image Set ------------------------
		try {
			js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
			WebUtility.Wait();
			WebUtility.Click(imageSetActiveButton);
			WebUtility.Wait();
			WebUtility.Click(dropDownList.get(0));
			WebUtility.javascript(yes.get(0));
			WebUtility.Wait();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		//------------------------ Delete Image Set -----------------------------
		try {
			removeSet.get(1).click();
			WebUtility.Wait();
			yes.get(0).click();
			WebUtility.Wait();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
        WebUtility.Wait();
        String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
        sa.assertTrue(doneButtonActiveColor.contains("255, 255, 255") || doneButtonActiveColor.contains("255,255,255"), "TV Show Images Mark As Done Assertion Fails");
        WebUtility.Click(doneButtonActive);
 		
        js.executeScript("window.scrollTo(0,0);");
        WebUtility.Wait();
        WebUtility.Click(imageSets.get(0));
 		try {
            TestUtil.captureScreenshot("TV Show - Images Section Default Set","TV Show");
        } catch (IOException e) {
            System.out.println("Screenshot Capture Failed ->" + e.getMessage());
        }
 	
        js.executeScript("window.scrollTo(0,document.body.scrollHeight);");

        try {
            TestUtil.captureScreenshot("TV Show - Images Section Default Set 2","TV Show");
        } catch (IOException e) {
            System.out.println("Screenshot Capture Failed ->" + e.getMessage());
        }
        
        WebUtility.Click(imageSets.get(1));
        js.executeScript("window.scrollTo(0,0);");
        try {
            TestUtil.captureScreenshot("TV Show - Images Section Created Set 1","TV Show");
        } catch (IOException e) {
            System.out.println("Screenshot Capture Failed ->" + e.getMessage());
        }
 	
        js.executeScript("window.scrollTo(0,document.body.scrollHeight);");

        try {
            TestUtil.captureScreenshot("TV Show - Images Section Created Set 2","TV Show");
        } catch (IOException e) {
            System.out.println("Screenshot Capture Failed ->" + e.getMessage());
        }

    }


    public void TvImagesAssertions() throws InterruptedException {

    /*	
        //------EDIT Flow for Testing ----------------------------------------------
		tvShowButton.click();
		Thread.sleep(2000);
		searchString.click();
		searchString.sendKeys("The Expanse");
		Thread.sleep(2000);
		WebUtility.element_to_be_clickable(editButton.get(0));
		editButton.get(0).click();
		Thread.sleep(2000);
		editTvShow.click();
		Thread.sleep(2000);
 	*/	
        images.click();
        Thread.sleep(2000);
        String coverName = "CoverPNG", appCoverName = "AppCoverPNG", listName = "List", squareName = "Square", tvCoverName = "TVCover", portraitName = "PortraitPNG", listCleanName = "ListClean", portraitCleanName = "PortraitCleanPNG", telcoSquareName = "TelcoSquare", passportName = "Passport";
        boolean coverFound = false, appCoverFound = false, listFound = false, squareFound = false, tvCoverFound = false, portraitFound = false, listCleanFound = false, portraitCleanFound = false, telcoSquareFound = false, passportFound = false;
        List < WebElement > imageCheck = driver.findElements(By.tagName("strong"));

        for (int i = 0; i < imageCheck.size(); i++) {
            if (imageCheck.get(i).getText().equals(coverName.toLowerCase()))
                coverFound = true;

            if (imageCheck.get(i).getText().equals(appCoverName.toLowerCase()))
                appCoverFound = true;

            if (imageCheck.get(i).getText().equals(listName.toLowerCase()))
                listFound = true;

            if (imageCheck.get(i).getText().equals(squareName.toLowerCase()))
                squareFound = true;

            if (imageCheck.get(i).getText().equals(tvCoverName.toLowerCase()))
                tvCoverFound = true;

            if (imageCheck.get(i).getText().equals(portraitName.toLowerCase()))
                portraitFound = true;

            if (imageCheck.get(i).getText().equals(listCleanName.toLowerCase()))
                listCleanFound = true;

            if (imageCheck.get(i).getText().equals(portraitCleanName.toLowerCase()))
                portraitCleanFound = true;

            if (imageCheck.get(i).getText().equals(telcoSquareName.toLowerCase()))
                telcoSquareFound = true;

            if (imageCheck.get(i).getText().equals(passportName.toLowerCase()))
                passportFound = true;

        }

        Assert.assertEquals(coverFound, true, "TV Shows Main Content - Image Cover Assertion Failed");
        Assert.assertEquals(appCoverFound, true, "TV Shows Main Content - Image App Cover Assertion Failed");
        Assert.assertEquals(listFound, true, "TV Shows Main Content - Image List Assertion Failed");
        Assert.assertEquals(squareFound, true, "TV Shows Main Content - Image Square Assertion Failed");
        Assert.assertEquals(tvCoverFound, true, "TV Shows Main Content - Image TV Cover Assertion Failed");
        Assert.assertEquals(portraitFound, true, "TV Shows Main Content - Image Portrait Assertion Failed");
        Assert.assertEquals(listCleanFound, true, "TV Shows Main Content - Image List CLean Assertion Failed");
        Assert.assertEquals(portraitCleanFound, true, "TV Shows Main Content - Image Portrait Clean Assertion Failed");
        Assert.assertEquals(telcoSquareFound, true, "TV Shows Main Content - Image Telco Square Assertion Failed");
        Assert.assertEquals(passportFound, true, "TV Shows Main Content - Image Passport Assertion Failed");

        String doneButtonClickedColor = doneButtonClicked.getCssValue("background-color");
        Assert.assertTrue(doneButtonClickedColor.contains("52, 194, 143"), "TV Shows Images Mark As Done Assertion Fails");
    }

    public void TvSeoDetails() throws InterruptedException {
    	JavascriptExecutor js = (JavascriptExecutor) driver; 		
    	Actions actionProvider = new Actions(driver);

     /*     
	   
        //------EDIT Flow for Testing ----------------------------------------------
		tvShowButton.click();
		Thread.sleep(2000);
		searchString.click();
		searchString.sendKeys(tvShowTitle);
		Thread.sleep(2000);
		WebUtility.element_to_be_clickable(editButton.get(0));
		editButton.get(0).click();
		Thread.sleep(2000);
		editTvShow.click();
		Thread.sleep(2000);
 */		

        WebUtility.Click(seoDetails);
        js.executeScript("window.scrollTo(0,0);");
        WebUtility.Wait();
        WebUtility.Click(seoTitleTag);
        seoTitleTag.sendKeys(Keys.CONTROL + "a");
        seoTitleTag.sendKeys(Keys.BACK_SPACE);
        WebUtility.Wait();
        seoTitleTag.sendKeys("Automation Test Data");


        seoMetaDescription.click();
        seoMetaDescription.sendKeys(Keys.CONTROL + "a");
        seoMetaDescription.sendKeys(Keys.BACK_SPACE);
        WebUtility.Wait();
        seoMetaDescription.sendKeys("Automation Test Data");

        seoMetaSynopsis.click();
        seoMetaSynopsis.sendKeys(Keys.CONTROL + "a");
        seoMetaSynopsis.sendKeys(Keys.BACK_SPACE);
        WebUtility.Wait();
        seoMetaSynopsis.sendKeys("Automation Test Data");

        seoRedirectionType.click();
        WebUtility.Wait();
        dropDownList.get(0).click();

        seoRedirectionLink.click();
        seoRedirectionLink.sendKeys(Keys.CONTROL + "a");
        seoRedirectionLink.sendKeys(Keys.BACK_SPACE);
        WebUtility.Wait();
        seoRedirectionLink.sendKeys("https://www.kelltontech.com");

        seoH1Heading.click();
        seoH1Heading.sendKeys(Keys.CONTROL + "a");
        seoH1Heading.sendKeys(Keys.BACK_SPACE);
        WebUtility.Wait();
        seoH1Heading.sendKeys("Automation Test Data");

        js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
        seoH2Heading.click();
        seoH2Heading.sendKeys(Keys.CONTROL + "a");
        seoH2Heading.sendKeys(Keys.BACK_SPACE);
        WebUtility.Wait();
        seoH2Heading.sendKeys("Automation Test Data");

        seoH3Heading.click();
        seoH3Heading.sendKeys(Keys.CONTROL + "a");
        seoH3Heading.sendKeys(Keys.BACK_SPACE);
        WebUtility.Wait();
        seoH3Heading.sendKeys("Automation Test Data");

        seoH4Heading.click();
        seoH4Heading.sendKeys(Keys.CONTROL + "a");
        seoH4Heading.sendKeys(Keys.BACK_SPACE);
        WebUtility.Wait();
        seoH4Heading.sendKeys("Automation Test Data");

        seoH5Heading.click();
        seoH5Heading.sendKeys(Keys.CONTROL + "a");
        seoH5Heading.sendKeys(Keys.BACK_SPACE);
        WebUtility.Wait();
        seoH5Heading.sendKeys("Automation Test Data");

        seoH6Heading.click();
        seoH6Heading.sendKeys(Keys.CONTROL + "a");
        seoH6Heading.sendKeys(Keys.BACK_SPACE);
        WebUtility.Wait();
        seoH6Heading.sendKeys("Automation Test Data");

        seoRobotsMetaIndex.get(0).click();
        seoRobotsMetaIndex.get(0).sendKeys(Keys.CONTROL + "a");
        seoRobotsMetaIndex.get(0).sendKeys(Keys.BACK_SPACE);
        WebUtility.Wait();
        seoRobotsMetaIndex.get(0).sendKeys("Automation Test Data");

        seoRobotsMetaIndex.get(1).click();
        seoRobotsMetaIndex.get(1).sendKeys(Keys.CONTROL + "a");
        seoRobotsMetaIndex.get(1).sendKeys(Keys.BACK_SPACE);
        WebUtility.Wait();
        seoRobotsMetaIndex.get(1).sendKeys("Automation Test Data");

        seoRobotsMetaImageIndex.click();
        seoRobotsMetaImageIndex.sendKeys(Keys.CONTROL + "a");
        seoRobotsMetaImageIndex.sendKeys(Keys.BACK_SPACE);
        WebUtility.Wait();
        seoRobotsMetaImageIndex.sendKeys("Automation Test Data");

        seoRobotsMetaImageNoIndex.click();
        seoRobotsMetaImageNoIndex.sendKeys(Keys.CONTROL + "a");
        seoRobotsMetaImageNoIndex.sendKeys(Keys.BACK_SPACE);
        WebUtility.Wait();
        seoRobotsMetaImageNoIndex.sendKeys("Automation Test Data");


        seoBreadcrumbTitle.click();
        seoBreadcrumbTitle.sendKeys(Keys.CONTROL + "a");
        seoBreadcrumbTitle.sendKeys(Keys.BACK_SPACE);
        WebUtility.Wait();
        seoBreadcrumbTitle.sendKeys("Automation Test Data");

        seoInternalLinkBuilding.click();
        seoInternalLinkBuilding.sendKeys(Keys.CONTROL + "a");
        seoInternalLinkBuilding.sendKeys(Keys.BACK_SPACE);
        WebUtility.Wait();
        seoInternalLinkBuilding.sendKeys("Automation Test Data");

		for (int i = 0; i < seoCheckboxes.size(); i++) {
			if (seoCheckboxes.get(i).isSelected() == false)
				seoCheckboxes.get(i).click();
		}
        
       
  		 /*       nextButton.click();
       
        js.executeScript("window.scrollTo(0,0);");
		WebUtility.element_to_be_clickable(globalCountry);
		globalSeoCountry.click();
		WebUtility.element_to_be_clickable(dropDownList.get(0));
		dropDownList.get(0).click();
		
  
        WebUtility.element_to_be_clickable(seoTitleTag);
        seoTitleTag.click();
        seoTitleTag.sendKeys(Keys.CONTROL + "a");
        seoTitleTag.sendKeys(Keys.BACK_SPACE);
        Thread.sleep(2000);
        seoTitleTag.sendKeys("Global Field Automation Test Data");

        Thread.sleep(2000);
        seoMetaDescription.click();
        seoMetaDescription.sendKeys(Keys.CONTROL + "a");
        seoMetaDescription.sendKeys(Keys.BACK_SPACE);
        Thread.sleep(2000);
        seoMetaDescription.sendKeys("Global Field Automation Test Data");

        Thread.sleep(2000);
        seoMetaSynopsis.click();
        seoMetaSynopsis.sendKeys(Keys.CONTROL + "a");
        seoMetaSynopsis.sendKeys(Keys.BACK_SPACE);
        Thread.sleep(2000);
        seoMetaSynopsis.sendKeys("Global Field Automation Test Data");

        seoRedirectionType.click();
        Thread.sleep(2000);
        dropDownList.get(0).click();

        Thread.sleep(2000);
        seoRedirectionLink.click();
        seoRedirectionLink.sendKeys(Keys.CONTROL + "a");
        seoRedirectionLink.sendKeys(Keys.BACK_SPACE);
        Thread.sleep(2000);
        seoRedirectionLink.sendKeys("https://www.kelltontech.com");

        Thread.sleep(2000);
        seoH1Heading.click();
        seoH1Heading.sendKeys(Keys.CONTROL + "a");
        seoH1Heading.sendKeys(Keys.BACK_SPACE);
        Thread.sleep(2000);
        seoH1Heading.sendKeys("Global Field Automation Test Data");

        js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
        Thread.sleep(2000);
        seoH2Heading.click();
        seoH2Heading.sendKeys(Keys.CONTROL + "a");
        seoH2Heading.sendKeys(Keys.BACK_SPACE);
        Thread.sleep(2000);
        seoH2Heading.sendKeys("Global Field Automation Test Data");

        Thread.sleep(2000);
        seoH3Heading.click();
        seoH3Heading.sendKeys(Keys.CONTROL + "a");
        seoH3Heading.sendKeys(Keys.BACK_SPACE);
        Thread.sleep(2000);
        seoH3Heading.sendKeys("Global Field Automation Test Data");

        Thread.sleep(2000);
        seoH4Heading.click();
        seoH4Heading.sendKeys(Keys.CONTROL + "a");
        seoH4Heading.sendKeys(Keys.BACK_SPACE);
        Thread.sleep(2000);
        seoH4Heading.sendKeys("Global Field Automation Test Data");

        Thread.sleep(2000);
        seoH5Heading.click();
        seoH5Heading.sendKeys(Keys.CONTROL + "a");
        seoH5Heading.sendKeys(Keys.BACK_SPACE);
        Thread.sleep(2000);
        seoH5Heading.sendKeys("Global Field Automation Test Data");

        Thread.sleep(2000);
        seoH6Heading.click();
        seoH6Heading.sendKeys(Keys.CONTROL + "a");
        seoH6Heading.sendKeys(Keys.BACK_SPACE);
        Thread.sleep(2000);
        seoH6Heading.sendKeys("Global Field Automation Test Data");

        Thread.sleep(2000);
        seoRobotsMetaIndex.get(0).click();
        seoRobotsMetaIndex.get(0).sendKeys(Keys.CONTROL + "a");
        seoRobotsMetaIndex.get(0).sendKeys(Keys.BACK_SPACE);
        Thread.sleep(2000);
        seoRobotsMetaIndex.get(0).sendKeys("Global Field Automation Test Data");

        Thread.sleep(2000);
        seoRobotsMetaIndex.get(1).click();
        seoRobotsMetaIndex.get(1).sendKeys(Keys.CONTROL + "a");
        seoRobotsMetaIndex.get(1).sendKeys(Keys.BACK_SPACE);
        Thread.sleep(2000);
        seoRobotsMetaIndex.get(1).sendKeys("Global Field Automation Test Data");

        Thread.sleep(2000);
        seoRobotsMetaImageIndex.click();
        seoRobotsMetaImageIndex.sendKeys(Keys.CONTROL + "a");
        seoRobotsMetaImageIndex.sendKeys(Keys.BACK_SPACE);
        Thread.sleep(2000);
        seoRobotsMetaImageIndex.sendKeys("Global Field Automation Test Data");

        Thread.sleep(2000);
        seoRobotsMetaImageNoIndex.click();
        seoRobotsMetaImageNoIndex.sendKeys(Keys.CONTROL + "a");
        seoRobotsMetaImageNoIndex.sendKeys(Keys.BACK_SPACE);
        Thread.sleep(2000);
        seoRobotsMetaImageNoIndex.sendKeys("Global Field Automation Test Data");

        Thread.sleep(2000);
        seoBreadcrumbTitle.click();
        seoBreadcrumbTitle.sendKeys(Keys.CONTROL + "a");
        seoBreadcrumbTitle.sendKeys(Keys.BACK_SPACE);
        Thread.sleep(2000);
        seoBreadcrumbTitle.sendKeys("Global Field Automation Test Data");

        Thread.sleep(2000);
        seoInternalLinkBuilding.click();
        seoInternalLinkBuilding.sendKeys(Keys.CONTROL + "a");
        seoInternalLinkBuilding.sendKeys(Keys.BACK_SPACE);
        Thread.sleep(2000);
        seoInternalLinkBuilding.sendKeys("Global Field Automation Test Data");

		Thread.sleep(2000);
		for (int i = 0; i < seoCheckboxes.size(); i++) {
			if (seoCheckboxes.get(i).isSelected() == false)
				seoCheckboxes.get(i).click();
		}
     */   
		String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
        sa.assertTrue(doneButtonActiveColor.contains("255, 255, 255") || doneButtonActiveColor.contains("255,255,255"), "TV Shows SEO Details Mark As Done Assertion Fails");
        WebUtility.Click(doneButtonActive);

		try {
			TestUtil.captureScreenshot("TV Show - SEO Details Section","TV Show");
		} catch (IOException e) {
			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}

        js.executeScript("window.scrollTo(0,document.body.scrollHeight);");

        try {
            TestUtil.captureScreenshot("TV Show - Seo Details Section 2","TV Show");
        } catch (IOException e) {
            System.out.println("Screenshot Capture Failed ->" + e.getMessage());
        }

    }

    public void TvSeoDetailsAssertions() throws InterruptedException {
    /*	
      //------EDIT Flow for Testing ----------------------------------------------
        
        videoButton.click();
 		searchString.click();
 		searchString.sendKeys("Star Trek (2020)");
 		Thread.sleep(2000);
 		WebUtility.element_to_be_clickable(editButton.get(0));
 		editButton.get(0).click();
 		Thread.sleep(2000);
 		editVideo.click();
 	*/	
        JavascriptExecutor js = (JavascriptExecutor) driver;
        WebUtility.Wait();
        WebUtility.Click(seoDetails);
        js.executeScript("window.scrollTo(0,0);");
        String expectedTitleTag = "Automation Test Data";
        Assert.assertEquals(seoTitleTag.getAttribute("value"), expectedTitleTag, "SEO Title Tag Assertion Failed");
        
        String expectedRedirectionLink = "https://www.kelltontech.com";
        Assert.assertEquals(seoRedirectionLink.getAttribute("value"), expectedRedirectionLink, "SEO Redirection Link Assertion Failed");
        
        String doneButtonClickedColor = doneButtonClicked.getCssValue("background-color");
        Assert.assertTrue(doneButtonClickedColor.contains("52, 194, 143"), "TV Shows SEO Details Mark As Done Assertion Fails");
    }

    public void TvMapContent() throws InterruptedException {

 /*       //------EDIT Flow for Testing ----------------------------------------------
		tvShowButton.click();
		Thread.sleep(2000);
		searchString.click();
		searchString.sendKeys("The Expanse");
		Thread.sleep(2000);
		WebUtility.element_to_be_clickable(editButton.get(0));
		editButton.get(0).click();
		Thread.sleep(2000);
		editTvShow.click();
		Thread.sleep(2000);
	*/	
    	WebUtility.Click(mapContent);
    	WebUtility.Wait();
    	WebUtility.Click(addContent);

        WebUtility.element_to_be_visible(mapContentAssign.get(0));
        checkBox.get(0).click();
        WebUtility.Click(assignContent);

        WebUtility.element_to_be_visible(doneButtonActive);
        String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
        sa.assertTrue(doneButtonActiveColor.contains("255, 255, 255") || doneButtonActiveColor.contains("255,255,255"), "TV Shows Map Content Mark As Done Assertion Fails");
        WebUtility.Click(doneButtonActive);
        
        try {
            TestUtil.captureScreenshot("TV Show - Map Content Section","TV Show");
        } catch (IOException e) {
            System.out.println("Screenshot Capture Failed ->" + e.getMessage());
        }
        
    }

    public void TvMapContentAssertions() throws InterruptedException {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        mapContent.click();
        
        Object mappedContentExists = js.executeScript("var x = document.getElementsByClassName('list-profile-box flex justify-content-between'),contentMapped = false;if(x.length > 0 ) contentMapped = true; return contentMapped");
        Assert.assertEquals(mappedContentExists, true, "Mapped Content Assertion Failed");
        
        String doneButtonClickedColor = doneButtonClicked.getCssValue("background-color");
        Assert.assertTrue(doneButtonClickedColor.contains("52, 194, 143"), "TV Shows Map Content Mark As Done Assertion Fails");

    }

    public void TvPublishFlow() throws InterruptedException {
    	JavascriptExecutor js = (JavascriptExecutor) driver; 		
        //---------------------------------Publish --------------------------------------------------------------------------------        
    	/*
        //------EDIT Flow for Testing ----------------------------------------------
		dashboard.click();
		Thread.sleep(2000);
		tvShowButton.click();
		Thread.sleep(2000);
		searchString.click();
		searchString.sendKeys("No TitleZee5 The Expanse Suite");
		Thread.sleep(2000);
		WebUtility.element_to_be_clickable(editButton.get(0));
		editButton.get(0).click();
		Thread.sleep(2000);
		editTvShow.click();
		Thread.sleep(2000);

	
    	//------EDIT Flow for Testing Single Landing ----------------------------------------------
    	dashboard.click();
		Thread.sleep(2000);
		tvShowButton.click();
		Thread.sleep(2000);
		searchString.click();
		searchString.sendKeys("Zee6 The Expanse Suite");
		Thread.sleep(2000);
		WebUtility.element_to_be_clickable(editButton.get(0));
		editButton.get(0).click();
		Thread.sleep(2000);
		editTvShow.click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//input[@name= 'selectJourney' and @value = 3]")).click();
		yes.get(0).click();
	*/	
		//--Verifying TvShow Draft Status
		try
		{
			tvShowProperties.click();
			WebUtility.element_to_be_visible(contentStatus);
			TestUtil.captureScreenshot("TvShow Content Draft Status","TV Show");
			String expectedStatus = "Draft";
			String actualStatus = contentStatus.getText();
			sa.assertEquals(actualStatus,expectedStatus,"TvShow Content Draft Status is Failed");
			Reporter.log("TvShow Content Draft Status is Passed",true);	
		}
		catch (Exception e)
		{
			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}
	
    	 Actions actionProvider = new Actions(driver);
    	 WebUtility.Wait();
         WebUtility.Click(checklist);

         WebUtility.Wait();
         js.executeScript("window.scrollBy(0,600)");
         WebUtility.Click(scheduledCountry.get(0));
         WebUtility.Click(dropDownList.get(0));

         WebUtility.Click(publishContent);
         WebUtility.Click(yes.get(0));
         WebUtility.Click(ok); 	

         js.executeScript("window.scrollTo(0,0)");
         
       //--Verifying TvShow Published Status
        try {
			tvShowProperties.click();
			WebUtility.element_to_be_visible(contentStatus);
            TestUtil.captureScreenshot("TvShow Content Published Status","TV Show");
			String expectedStatus = "Published";
			String actualStatus = contentStatus.getText();
			sa.assertEquals(actualStatus,expectedStatus,"TvShow Content Published Status is Failed");
			Reporter.log("TvShow Content Published Status is Passed",true);
        } catch (IOException e) {
            System.out.println("Screenshot Capture Failed ->" + e.getMessage());
        }
        WebUtility.Wait();

        //----------------Republish -----------------------------------------------------------------------------
       
        try {
	        license.click();
	        WebUtility.Wait();
	       	
	        WebUtility.Click(licenseStatusButton);
	        WebUtility.Click(inactivateLicenseDropdown);
	        WebUtility.Wait();
	        actionProvider.moveToElement(inactivateLicenseReason).click().build().perform();
	        WebUtility.Click(dropDownList.get(0));
	        WebUtility.Click(yes.get(0));
	        WebUtility.Click(doneButtonActive);
       }
       catch(Exception e)
       {
    	   try
    	   {
    	   WebUtility.Click(no);
    	   }
    	   catch(Exception e1) {
    		 e1.printStackTrace();
    	   }
    	   
    	   WebUtility.Click(tvShowProperties);
    	   
           WebUtility.Click(note);
           note.sendKeys(Keys.CONTROL + "a");
           note.sendKeys(Keys.DELETE);
           note.sendKeys("Automation Test 2");  	    
		   
           WebUtility.Click(title);           
		   
           WebUtility.Click(doneButtonActive);
           e.printStackTrace();
           
       }
        
        WebUtility.Click(checklist);
        js.executeScript("window.scrollTo(0,document.body.scrollHeight)");
        WebUtility.Click(republishContent);
        WebUtility.Click(yes.get(0));
        //WebUtility.Click(yes.get(1));
        WebUtility.Click(ok);
        js.executeScript("window.scrollTo(0,0)");
		WebUtility.Wait();
		
		//--Verifying TvShow Republished status
		try
		{
				tvShowProperties.click();
				WebUtility.element_to_be_visible(contentStatus);
				TestUtil.captureScreenshot("TvShow Content Republished Status","TV Show");
				String expectedStatus = "Published";
				String actualStatus = contentStatus.getText();
				sa.assertEquals(actualStatus,expectedStatus,"TvShow Content Republished Status is Failed");
				Reporter.log("TvShow Content Republished Status is Passed",true);	
		}
		catch (Exception e)
		{
				System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}
        //------------------------------Unpublish --------------------------------------------------------------------------------------------        
        js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
        WebUtility.Click(checklist);
        WebUtility.Click(unpublishContent);

        WebUtility.Click(unpublishReason);
        WebUtility.Click(dropDownList.get(0));
        WebUtility.Click(yes.get(0));
        WebUtility.Click(ok);
        js.executeScript("window.scrollTo(0,0)");
        
      //--Verifying TvShow Unpublished Status
        try {
			tvShowProperties.click();
			WebUtility.element_to_be_visible(contentStatus);
            TestUtil.captureScreenshot("TvShow Content Unpublished Status","TV Show");
			String expectedStatus = "Unpublished";
			String actualStatus = contentStatus.getText();
			sa.assertEquals(actualStatus,expectedStatus,"TvShow Content Unpublished Status is Failed");
			Reporter.log("TvShow Content Unpublished Status is Passed",true);
        } catch (IOException e) {
            System.out.println("Screenshot Capture Failed ->" + e.getMessage());
        }
        WebUtility.Wait();
        publishedHistory.click();
        WebUtility.Wait();
        try {
            TestUtil.captureScreenshot("TV Main Content - Published History","TV Show");
        } catch (IOException e) {
            System.out.println("Screenshot Capture Failed ->" + e.getMessage());
        }

        WebUtility.Click(updateTVShowBreadcrumb);
        WebUtility.Wait();

        // Archive unpublished content -------------------------------------     
        WebUtility.Click(dashboard);
        WebUtility.Click(tvShowButton);
        WebUtility.Wait();
        WebUtility.Click(searchString);
        searchString.sendKeys(tvShowTitle);
        WebUtility.Wait();
		Archive.get(0).click();				
		ArchiveYes.click();
		WebUtility.Wait();
        WebUtility.Click(editButton.get(0));
		WebUtility.Wait();

		//--Verifying TvShow content Archive status
		try
		{
			tvShowProperties.click();
			WebUtility.element_to_be_visible(contentStatus);
			TestUtil.captureScreenshot("TvShow content Archived Status","Movie");
			String expectedStatus = "Archived";
			String actualStatus = contentStatus.getText();
			sa.assertEquals(actualStatus,expectedStatus,"TvShow content Archived Status is Failed");
			Reporter.log("TvShow content Archived Status is Passed",true);	
		}
		catch (Exception e)
		{
			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}
		WebUtility.Wait();
		
		// Restore Content to draft mode ---------------------------------------
        WebUtility.Click(dashboard);
        WebUtility.Click(tvShowButton);
        WebUtility.Wait();
        WebUtility.Click(searchString);
        searchString.sendKeys(tvShowTitle);
        WebUtility.Wait();
		WebUtility.javascript(Restore);			
		WebUtility.Wait();
		WebUtility.javascript(RestoreYes);
		WebUtility.Wait();
        WebUtility.Click(editButton.get(0));
		WebUtility.Wait();
		
		//--Verifying TvShow content Restore Status
		try
		{
			tvShowProperties.click();
			WebUtility.element_to_be_visible(contentStatus);
			TestUtil.captureScreenshot("TvShow content Restore Status","Movie");
			String expectedStatus = "Draft";
			String actualStatus = contentStatus.getText();
			sa.assertEquals(actualStatus,expectedStatus,"TvShow content Restore Status is Failed");
			Reporter.log("TvShow content Restore Status is Passed",true);	
		}
		catch (Exception e)
		{
			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}

    }

    public void TvScheduleContent() throws InterruptedException {
    	JavascriptExecutor js = (JavascriptExecutor) driver; 		
    	Actions actionProvider = new Actions(driver);
/*
		// -------- Edit Flow For Testing Purposes ----------------------------

    	WebUtility.Wait();
		//WebUtility.Click(dashboard);
		WebUtility.Click(tvShowButton);

		searchString.click();
		searchString.sendKeys(tvShowTitle);
		WebUtility.Wait();
		WebUtility.Wait();
		WebUtility.element_to_be_clickable(editButton.get(0));
		editButton.get(0).click();      
*/
    	WebUtility.Click(checklist);
    	js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
        WebUtility.Click(scheduleContent);
        

      //  System.out.println("--------> Current Time = "+currentTime.getAttribute("innerText").substring(20, 28));
      //  String currentTimeString  = currentTime.getAttribute("innerText").substring(20, 27);
    //	js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
 
        WebUtility.Click(scheduledPublicationTime);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        WebUtility.Click(datePickerMinutes);
        actionProvider.moveToElement(datePickerMinutesClock).click().build().perform();      
        datePicker.sendKeys(Keys.ESCAPE);						  

        WebUtility.Click(scheduledContentCountry);
        WebUtility.Click(dropDownList.get(0));

        WebUtility.Click(scheduleContentButton);
        WebUtility.Click(yes.get(0));
        WebUtility.Wait();
        WebUtility.Click(ok);

    	js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
        WebUtility.Wait();
        js.executeScript("window.scrollTo(0,0);");
        WebUtility.Wait();
        

        //System.out.println("--------> Scheduled Time = "+scheduledTime.getAttribute("innerText").substring(12, 20));
       // String scheduledTimeString = scheduledTime.getAttribute("innerText").substring(12, 20);
    	//js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
 
    	//sa.assertTrue(scheduledTimeString.contains(currentTimeString),"TV Main Content - Current Time and Scheduled Time are not Equal");
	
      //--Verifying TvShow Scheduled Status
        try {
			tvShowProperties.click();
			WebUtility.Wait();
			WebUtility.element_to_be_visible(contentStatus);
            TestUtil.captureScreenshot("TvShow Content Scheduled Status","TV Show");
			String expectedStatus = "Scheduled";
			String actualStatus = contentStatus.getText();
			sa.assertEquals(actualStatus,expectedStatus,"TvShow Content Scheduled Status is Failed");
			Reporter.log("TvShow Content Scheduled Status is Passed",true);
        } catch (Exception e) {
            System.out.println("Screenshot Capture Failed ->" + e.getMessage());
        }
    
        WebUtility.Wait();
        WebUtility.Click(checklist);
        WebUtility.Click(deleteSchedule);
        WebUtility.Click(yes.get(0));
        WebUtility.Click(ok);
        
        WebUtility.Wait();
        js.executeScript("window.scrollTo(0,document.body.scrollHeight)");
        WebUtility.Click(scheduledCountry.get(0));
        WebUtility.Click(dropDownList.get(0));

        WebUtility.Click(publishContent);
        WebUtility.Click(yes.get(0));
        WebUtility.Click(ok); 	

        js.executeScript("window.scrollTo(0,0)");
    }

    public void TvRelatedContent() throws InterruptedException {
    	
		WebUtility.Click(relatedContent);
		WebUtility.Click(relatedContentAssignTvShows);

		WebUtility.element_to_be_visible(relatedContentAddTvShows);
		WebUtility.Click(relatedContentAddTvShows);

		WebUtility.Wait();
		checkBox.get(0).click();
		checkBox.get(1).click();
		WebUtility.Click(assignRelatedContent);
		WebUtility.Click(deleteRelatedContent);
		WebUtility.Wait();
		WebUtility.Click(yes.get(0));
		WebUtility.Wait();

		WebUtility.Click(relatedContentAssignVideos);
		WebUtility.element_to_be_visible(relatedContentAddVideos);
		WebUtility.Click(relatedContentAddVideos);

		WebUtility.Wait();
		checkBox.get(0).click();
		checkBox.get(1).click();
		WebUtility.Click(assignRelatedContent);
		WebUtility.Click(deleteRelatedContent);
		WebUtility.Wait();
		WebUtility.Click(yes.get(0));
		WebUtility.Wait();

		WebUtility.Click(relatedContentAssignMovies);
		WebUtility.element_to_be_visible(relatedContentAddMovies);
		WebUtility.Click(relatedContentAddMovies);

		WebUtility.Wait();
		checkBox.get(0).click();
		checkBox.get(1).click();
		WebUtility.Click(assignRelatedContent);
		WebUtility.Click(deleteRelatedContent);
		WebUtility.Wait();
		WebUtility.Click(yes.get(0));
		WebUtility.Wait();

		WebUtility.Click(relatedContentAssignSeasons);
		WebUtility.element_to_be_visible(relatedContentAddSeasons);
		WebUtility.Click(relatedContentAddSeasons);

		WebUtility.Wait();
		checkBox.get(0).click();
		checkBox.get(1).click();
		WebUtility.Click(assignRelatedContent);
		WebUtility.Click(deleteRelatedContent);
		WebUtility.Wait();
		WebUtility.Click(yes.get(0));
		WebUtility.Wait();

		WebUtility.Click(relatedContentAssignEpisodes);
		WebUtility.element_to_be_visible(relatedContentAddEpisodes);
		WebUtility.Click(relatedContentAddEpisodes);

		WebUtility.Wait();
		checkBox.get(0).click();
		checkBox.get(1).click();
		WebUtility.Click(assignRelatedContent);
		WebUtility.Click(deleteRelatedContent);
		WebUtility.Wait();
		WebUtility.Click(yes.get(0));
		WebUtility.Wait();

		String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
		sa.assertTrue(doneButtonActiveColor.contains("255, 255, 255") || doneButtonActiveColor.contains("255,255,255"), "TV Related Content Mark As Done Assertion Fails");
		WebUtility.Click(doneButtonActive);

		WebUtility.Wait();
		updateTVShowBreadcrumb.click();
		WebUtility.Wait();
        
        
    }
    
    public void TvTranslations() throws InterruptedException{
    	try {
    		WebUtility.Wait();
    		updateTVShowBreadcrumb.click();
    		WebUtility.Wait();
    	}

    	catch(Exception e){    		
    	}
  
 		JavascriptExecutor js = (JavascriptExecutor) driver;
  		WebUtility.Click(translations);
 		
		WebUtility.Wait();
		WebUtility.Click(title);
		title.sendKeys(Keys.CONTROL + "a");
		title.sendKeys(Keys.BACK_SPACE);
		title.sendKeys("फैलाव");
		
		translationsShortDesc.click();
		translationsShortDesc.sendKeys(Keys.CONTROL + "a");
		translationsShortDesc.sendKeys(Keys.BACK_SPACE);
		translationsShortDesc.sendKeys("जेम्स एस ए कोरी द्वारा इसी नाम के उपन्यासों की श्रृंखला के आधार पर मार्क फर्गस और हॉक ओस्बी द्वारा विकसित एक अमेरिकी विज्ञान कथा टेलीविजन श्रृंखला है।");
		
		translationsWebDesc.click();
		translationsWebDesc.sendKeys(Keys.CONTROL + "a");
		translationsWebDesc.sendKeys(Keys.BACK_SPACE);
		translationsWebDesc.sendKeys("स्वचालन परीक्षण");
		
		translationsAppDesc.click();
		translationsAppDesc.sendKeys(Keys.CONTROL + "a");
		translationsAppDesc.sendKeys(Keys.BACK_SPACE);
		translationsAppDesc.sendKeys("स्वचालन परीक्षण");
		
		translationsTVDesc.click();
		translationsTVDesc.sendKeys(Keys.CONTROL + "a");
		translationsTVDesc.sendKeys(Keys.BACK_SPACE);
		translationsTVDesc.sendKeys("स्वचालन परीक्षण");
		creativeTitle.click();
		creativeTitle.sendKeys(Keys.CONTROL + "a");
		creativeTitle.sendKeys(Keys.BACK_SPACE);
		creativeTitle.sendKeys("स्वचालन रचनात्मक शीर्षक");

		translationsAlternativeTitle.click();
		translationsAlternativeTitle.sendKeys(Keys.CONTROL + "a");
		translationsAlternativeTitle.sendKeys(Keys.BACK_SPACE);
		translationsAlternativeTitle.sendKeys("स्वचालन वैकल्पिक शीर्षक");

		pageTitle.click();
		pageTitle.sendKeys(Keys.CONTROL + "a");
		pageTitle.sendKeys(Keys.BACK_SPACE);
		pageTitle.sendKeys("ऑटोमेशन पेज का शीर्षक");

		pageDescription.click();
		pageDescription.sendKeys(Keys.CONTROL + "a");
		pageDescription.sendKeys(Keys.BACK_SPACE);
		pageDescription.sendKeys("स्वचालन पृष्ठ विवरण");

		translationsTitleForSocialShare.click();
		translationsTitleForSocialShare.sendKeys(Keys.CONTROL + "a");
		translationsTitleForSocialShare.sendKeys(Keys.BACK_SPACE);
		translationsTitleForSocialShare.sendKeys("सामाजिक हिस्सेदारी के लिए स्वचालन शीर्षक");

		translationsDescriptionForSocialShare.click();
		translationsDescriptionForSocialShare.sendKeys(Keys.CONTROL + "a");
		translationsDescriptionForSocialShare.sendKeys(Keys.BACK_SPACE);
		translationsDescriptionForSocialShare.sendKeys("सामाजिक शेयर के लिए स्वचालन विवरण");
		
		translationsUpcomingPage.click();
		translationsUpcomingPage.sendKeys(Keys.CONTROL + "a");
		translationsUpcomingPage.sendKeys(Keys.BACK_SPACE);
		translationsUpcomingPage.sendKeys("ऑटोमेशन आगामी पृष्ठ पाठ");
        
        trivia.click();
		trivia.sendKeys(Keys.CONTROL + "a");
	    trivia.sendKeys(Keys.BACK_SPACE);
	    trivia.sendKeys("ऑटोमेशन ट्रिविया");

	    js.executeScript("window.scrollTo(0,0);");
	    Thread.sleep(2000);
	    castCrew.click();
	    
	    WebUtility.element_to_be_clickable(translationsActor);
	    translationsActor.click();
	    translationsActor.sendKeys(Keys.CONTROL + "a");
	    translationsActor.sendKeys(Keys.BACK_SPACE);
	    translationsActor.sendKeys("ऑटोमेशन आगामी पृष्ठ पाठ");
		
		translationsCharacter.click();
		translationsCharacter.sendKeys(Keys.CONTROL + "a");
		translationsCharacter.sendKeys(Keys.BACK_SPACE);
		translationsCharacter.sendKeys("ऑटोमेशन आगामी पृष्ठ पाठ");
		
		WebUtility.Wait();
		contentProperties.click();
		WebUtility.Wait();
		String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
		sa.assertTrue(doneButtonActiveColor.contains("255, 255, 255") || doneButtonActiveColor.contains("255,255,255"), "TV Translations Mark As Done Assertion Fails");
		WebUtility.Click(doneButtonActive);
        WebUtility.Wait();
		
    	
        updateTVShowBreadcrumb.click();
    }
    //------------------------------------------ End of Main Content --------------------------------------------------------------
    
    //-------------------------------------------TV Show Quick Filling ------------------------------------------------------------
    
    public void TvPropertiesQuickFiling() throws InterruptedException, Exception {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        WebUtility.Click(dashboard);
        WebUtility.Click(tvShowButton);
/*      
        //------EDIT Flow for Testing ----------------------------------------------

		searchString.click();
		searchString.sendKeys("The Expanse Quick Filing");
		Thread.sleep(2000);
		WebUtility.element_to_be_clickable(editButton.get(0));
		editButton.get(0).click();
		Thread.sleep(2000);
		editTvShow.click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//input[@name= 'selectJourney' and @value = 2]")).click();
		yes.get(0).click();
*/
  
		quickFilling.click();
		WebUtility.Click(createQuickFiling);
		WebUtility.Wait();

        // Content Properties section -----------------------------------------
        
        tvShowProperties.click();
        WebUtility.element_to_be_clickable(title);
        title.click();
        title.sendKeys(Keys.CONTROL + "a");
        title.sendKeys(Keys.BACK_SPACE);
        title.sendKeys(tvShowQuickFilingTitle);

        shortDesc.click();
        shortDesc.sendKeys(Keys.CONTROL + "a");
        shortDesc.sendKeys(Keys.BACK_SPACE);
        shortDesc.sendKeys("The Expanse is an American science fiction television series developed by Mark Fergus and Hawk Ostby, based on the series of novels of the same name by James S. A. Corey.");
         
		subType.click();
		WebUtility.Click(dropDownList.get(0));

		category.click();
		WebUtility.Click(dropDownList.get(0));

		primaryGenre.click();
		WebUtility.Click(dropDownList.get(0));

		WebUtility.Click(zee5ReleaseDate);
		datePicker.sendKeys(Keys.ARROW_RIGHT);
		datePicker.sendKeys(Keys.ARROW_RIGHT);
		datePicker.sendKeys(Keys.ESCAPE);

		WebUtility.Click(audioLanguage);
		WebUtility.Click(dropDownList.get(0));

		primaryLanguage.click();
		WebUtility.Click(dropDownList.get(0));

		dubbedLanguageTitle.click();
		WebUtility.Click(dropDownList.get(0));

		originalLanguage.click();
		WebUtility.Click(dropDownList.get(0));

		contentVersion.click();
		WebUtility.Click(dropDownList.get(0));

		nextButton.click();

		WebUtility.Click(contentOwner);
		WebUtility.Click(dropDownList.get(0));

		certification.click();
		WebUtility.Click(dropDownList.get(0));

		nextButton.click();
		
		
		//driver.findElement(By.xpath("//span[contains(text(),'Global')]")).click();
/*		
		globalTitle.click();
		globalTitle.sendKeys(Keys.CONTROL + "a");
		globalTitle.sendKeys(Keys.BACK_SPACE);
		Thread.sleep(1000);
		globalTitle.sendKeys("Automation Test Data");
		
		
		globalCountryQuickFilling.click();
		WebUtility.element_to_be_clickable(dropDownList.get(0));
		dropDownList.get(0).click();
		
		zee5ReleaseDate.click();
		zee5ReleaseDate.sendKeys(Keys.SPACE);
		zee5ReleaseDate.sendKeys(Keys.ARROW_RIGHT);
		zee5ReleaseDate.sendKeys(Keys.ARROW_RIGHT);
		zee5ReleaseDate.sendKeys(Keys.ENTER);

		globalShortDesc.click();
		globalShortDesc.sendKeys(Keys.CONTROL + "a");
		globalShortDesc.sendKeys(Keys.BACK_SPACE);
		globalShortDesc.sendKeys("Automation Test Data");

		globalTitle.click();
		previousButton.click();
*/		
		js.executeScript("window.scrollTo(0,0);");
		WebUtility.Wait();
		String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
		sa.assertTrue(doneButtonActiveColor.contains("255, 255, 255") || doneButtonActiveColor.contains("255,255,255"), "TV Quick Filing - Properties Mark As Done Assertion Fails");
		WebUtility.element_to_be_clickable(doneButtonActive);
		doneButtonActive.click();

    }
    
    
    public void TvLicenseQuickFiling() throws InterruptedException {
/*
		// ------EDIT Flow for Testing ----------------------------------------------
    	tvShowButton.click();
    	Thread.sleep(2000);
    	searchString.click();
		searchString.sendKeys("The Expanse Quick Filing");
		Thread.sleep(2000);
		WebUtility.element_to_be_clickable(editButton.get(0));
		editButton.get(0).click();
		Thread.sleep(2000);
		editTvShow.click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//input[@name= 'selectJourney' and @value = 2]")).click();
		yes.get(0).click();
		Thread.sleep(2000);
	*/
		WebUtility.Click(license);
		WebUtility.Click(createLicense);

		WebUtility.Click(licenseSetName);
        licenseSetName.sendKeys("Automation Test Data");				
		
        WebUtility.Click(licenseFromDate);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ESCAPE);

        WebUtility.Click(licenseToDate);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ESCAPE);		
		
        WebUtility.Click(licenseCountry);
        for (int x = 0; x < dropDownList.size(); x++) {
        	if(x<4) {
            WebUtility.Click(dropDownList.get(x));
        	}
        	else {
        		break;
        	}
        }
		
		
		WebUtility.Click(licenseBusinessType);
		WebUtility.Click(dropDownList.get(0));

		WebUtility.Wait();
		driver.findElement(By.xpath("//span[contains(text(),'SAVE')]")).click();
		
		WebUtility.Click(license);
		WebUtility.Click(createLicense);
		
		WebUtility.Click(licenseSetName);
        licenseSetName.sendKeys("Automation Test Data II");		   

        WebUtility.Click(licenseFromDate);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ESCAPE);

        WebUtility.Click(licenseToDate);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ESCAPE);
			
		WebUtility.Click(licenseCountry);
        for (int x = 0; x < dropDownList.size(); x++) {
        	if(x<4) {
            WebUtility.Click(dropDownList.get(x));
        	}
        	else {
        		break;
        	}
        }
		
		
		WebUtility.Click(licenseBusinessType);
		WebUtility.Click(dropDownList.get(0));

		WebUtility.Wait();
		driver.findElement(By.xpath("//span[contains(text(),'SAVE')]")).click();

		
		String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
		sa.assertTrue(doneButtonActiveColor.contains("255, 255, 255") || doneButtonActiveColor.contains("255,255,255"), "TV Quick Filing - License Mark As Done Assertion Fails");
		WebUtility.Click(doneButtonActive);	
		WebUtility.Wait();
		try {
			TestUtil.captureScreenshot("TV Quick Filing - License Section","TV Show");
		} catch (IOException e) {
			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}


        WebUtility.Click(expiredLicense);
        Thread.sleep(2000);
        WebUtility.Click(expiredLicenseBack);

        WebUtility.Click(licenseFilters);
        WebUtility.Click(licenseFromDate);
        datePicker.sendKeys(Keys.ARROW_LEFT);
        datePicker.sendKeys(Keys.ARROW_LEFT);
        datePicker.sendKeys(Keys.ESCAPE);

        WebUtility.Click(licenseToDate);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ESCAPE);
        Thread.sleep(1000);

        applyFilter.click();

        Thread.sleep(2000);
        licenseFilters.click();
        clearFilter.click();

        WebUtility.Click(licenseFilters);
        licenseFilterBusinessType.click();
        WebUtility.Click(dropDownList.get(0));
        Thread.sleep(1000);
        applyFilter.click();

        Thread.sleep(2000);
        licenseFilters.click();
        clearFilter.click();
/*
        WebUtility.Click(licenseFilters);
        Thread.sleep(2000);
        licenseFilterBillingType.click();
        WebUtility.Click(dropDownList.get(0));
        Thread.sleep(2000);
        applyFilter.click();

        Thread.sleep(3000);
        licenseFilters.click();
        clearFilter.click();

        WebUtility.Click(licenseFilters);
        Thread.sleep(2000);
        licenseFilterPlatform.click();
        WebUtility.Click(dropDownList.get(0));
        Thread.sleep(2000);
        filterCloseDropdown.click();
        applyFilter.click();

        Thread.sleep(3000);
        licenseFilters.click();
        clearFilter.click();
*/
        WebUtility.Click(licenseFilters);
        Thread.sleep(2000);
        licenseFilterActiveStatus.click();
        applyFilter.click();

        Thread.sleep(2000);
        licenseFilters.click();
        clearFilter.click();

        WebUtility.Click(licenseFilters);
        Thread.sleep(2000);
        licenseFilterInActiveStatus.click();
        applyFilter.click();

        Thread.sleep(2000);
        licenseFilters.click();
        clearFilter.click();
	}
    
    public void TvImagesQuickFiling() throws InterruptedException {
    	JavascriptExecutor js = (JavascriptExecutor) driver;  
 		WebUtility.Click(images);
	

        WebUtility.Wait();
        list.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\List.jpg");
        
        WebUtility.Wait();
        WebUtility.Click(imagesCreateNewSet);
        
        WebUtility.Click(newSetName);
  		newSetName.sendKeys("Automation Test Data Quick Filing");		
 		
 		newSetCountry.click();
 		WebUtility.Click(dropDownList.get(0));
 		closeDropdown.click();
 		
 		newSetPlatform.click();
 		WebUtility.Click(dropDownList.get(0));
 		closeDropdown.click();
 		
 		newSetGender.click();
 		WebUtility.Click(dropDownList.get(0));
 		closeDropdown.click();
 		
 		newSetGenre.click();
 		WebUtility.Click(dropDownList.get(0));
 		closeDropdown.click();
 		
 		newSetAgeGroup.click();
 		WebUtility.Click(dropDownList.get(0));
 		closeDropdown.click();
 		
 		newSetLanguage.click();
 		WebUtility.Click(dropDownList.get(0));
 		closeDropdown.click();
 		
 		newSetOthers.click();
 		newSetOthers.sendKeys("Automation Test Data");
 		
 		WebUtility.Click(save);
  		
 		WebUtility.Click(imageSets.get(1));	
 		
 		js.executeScript("window.scrollTo(0,0)"); 		
 		
        WebUtility.Wait();
        list.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\List.jpg");

        
        WebUtility.Wait();
        WebUtility.Click(imagesCreateNewSet);
        
        WebUtility.Click(newSetName);
  		newSetName.sendKeys("Automation Test Data Quick Filing II");		
 		
 		newSetCountry.click();
 		WebUtility.Click(dropDownList.get(1));
 		closeDropdown.click();
 		
 		newSetPlatform.click();
 		WebUtility.Click(dropDownList.get(1));
 		closeDropdown.click();
 		
 		newSetGender.click();
 		WebUtility.Click(dropDownList.get(1));
 		closeDropdown.click();
 		
 		newSetGenre.click();
 		WebUtility.Click(dropDownList.get(1));
 		closeDropdown.click();
 		
 		newSetAgeGroup.click();
 		WebUtility.Click(dropDownList.get(1));
 		closeDropdown.click();
 		
 		newSetLanguage.click();
 		WebUtility.Click(dropDownList.get(1));
 		closeDropdown.click();
 		
 		newSetOthers.click();
 		newSetOthers.sendKeys("Automation Test Data II");
 		
 		WebUtility.Click(save);
  		
 		WebUtility.Click(imageSets.get(2));	
 		
 		js.executeScript("window.scrollTo(0,0)"); 		
 		
        WebUtility.Wait();
        list.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\List.jpg");
        
        // ---------------------- Inactivate Image Set ------------------------
		try {
			js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
			WebUtility.Wait();
			WebUtility.Click(imageSetActiveButton);
			WebUtility.Wait();
			WebUtility.Click(dropDownList.get(0));
			WebUtility.javascript(yes.get(0));
			WebUtility.Wait();
		}
        catch(Exception e) {
        	e.printStackTrace();
        }
		
		//------------------------ Delete Image Set -----------------------------
		try {
			removeSet.get(1).click();
			WebUtility.Wait();
			yes.get(0).click();
			WebUtility.Wait();
		} catch (Exception e) {
			e.printStackTrace();
		}
        
        String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
		sa.assertTrue(doneButtonActiveColor.contains("255, 255, 255") || doneButtonActiveColor.contains("255,255,255"), "TV Quick Filing - Images Mark As Done Assertion Fails");
		WebUtility.Click(doneButtonActive);	

        js.executeScript("window.scrollTo(0,0);");
        WebUtility.Wait();
        WebUtility.Click(imageSets.get(0));
 		try {
            TestUtil.captureScreenshot("TV Show Quick Filing - Images Section Default Set","TV Show");
        } catch (IOException e) {
            System.out.println("Screenshot Capture Failed ->" + e.getMessage());
        } 	
        
        WebUtility.Click(imageSets.get(1));
        js.executeScript("window.scrollTo(0,0);");
        try {
            TestUtil.captureScreenshot("TV Show Quick Filing - Images Section Created Set","TV Show");
        } catch (IOException e) {
            System.out.println("Screenshot Capture Failed ->" + e.getMessage());
        }

	}
    
    
    
    public void TvSeoDetailsQuickFiling() throws InterruptedException {
    	JavascriptExecutor js = (JavascriptExecutor) driver;
    	WebUtility.Click(seoDetails);

        WebUtility.Wait();
        WebUtility.Click(seoTitleTag);
        seoTitleTag.sendKeys(Keys.CONTROL + "a");
        seoTitleTag.sendKeys(Keys.BACK_SPACE);
        WebUtility.Wait();
        seoTitleTag.sendKeys("Automation Test Data");

        seoMetaDescription.click();
        seoMetaDescription.sendKeys(Keys.CONTROL + "a");
        seoMetaDescription.sendKeys(Keys.BACK_SPACE);
        WebUtility.Wait();
        seoMetaDescription.sendKeys("Automation Test Data");

        seoMetaSynopsis.click();
        seoMetaSynopsis.sendKeys(Keys.CONTROL + "a");
        seoMetaSynopsis.sendKeys(Keys.BACK_SPACE);
        WebUtility.Wait();
        seoMetaSynopsis.sendKeys("Automation Test Data");
		
        seoH1Heading.click();
        seoH1Heading.sendKeys(Keys.CONTROL + "a");
        seoH1Heading.sendKeys(Keys.BACK_SPACE);
        WebUtility.Wait();
        seoH1Heading.sendKeys("Automation Test Data");

        seoH2Heading.click();
        seoH2Heading.sendKeys(Keys.CONTROL + "a");
        seoH2Heading.sendKeys(Keys.BACK_SPACE);
        WebUtility.Wait();
        seoH2Heading.sendKeys("Automation Test Data");

        for (int i = 0; i < seoCheckboxes.size(); i++) {
			if (seoCheckboxes.get(i).isSelected() == false)
				seoCheckboxes.get(i).click();
		}

		//WebUtility.element_to_be_clickable(nextButton);
		//WebUtility.Click(nextButton);

		js.executeScript("window.scrollTo(0,0);");
		/*
		WebUtility.element_to_be_clickable(seoTitleTag);
		seoTitleTag.click();
		seoTitleTag.sendKeys(Keys.CONTROL + "a");
		seoTitleTag.sendKeys(Keys.BACK_SPACE);
		Thread.sleep(2000);
		seoTitleTag.sendKeys("Global Field Automation Test Data");

		Thread.sleep(2000);
		seoMetaDescription.click();
		seoMetaDescription.sendKeys(Keys.CONTROL + "a");
		seoMetaDescription.sendKeys(Keys.BACK_SPACE);
		Thread.sleep(2000);
		seoMetaDescription.sendKeys("Global Field Automation Test Data");

		Thread.sleep(2000);
		seoMetaSynopsis.click();
		seoMetaSynopsis.sendKeys(Keys.CONTROL + "a");
		seoMetaSynopsis.sendKeys(Keys.BACK_SPACE);
		Thread.sleep(2000);
		seoMetaSynopsis.sendKeys("Global Field Automation Test Data");

		Thread.sleep(2000);
		seoH1Heading.click();
		seoH1Heading.sendKeys(Keys.CONTROL + "a");
		seoH1Heading.sendKeys(Keys.BACK_SPACE);
		Thread.sleep(2000);
		seoH1Heading.sendKeys("Global Field Automation Test Data");

		js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
		Thread.sleep(2000);
		seoH2Heading.click();
		seoH2Heading.sendKeys(Keys.CONTROL + "a");
		seoH2Heading.sendKeys(Keys.BACK_SPACE);
		Thread.sleep(2000);
		seoH2Heading.sendKeys("Global Field Automation Test Data");

		Thread.sleep(2000);
		for (int i = 0; i < seoCheckboxes.size(); i++) {
			if (seoCheckboxes.get(i).isSelected() == false)
				seoCheckboxes.get(i).click();
		}
*/
	//	previousButton.click();
		WebUtility.Click(seoTitleTag);
		WebUtility.Wait();
		String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
		sa.assertTrue(doneButtonActiveColor.contains("255, 255, 255") || doneButtonActiveColor.contains("255,255,255"), "TV Quick Filing - SEO Details Mark As Done Assertion Fails");
		WebUtility.Click(doneButtonActive);

		try {
			TestUtil.captureScreenshot("TV Quick Filing - SEO Details Section","TV Show");
		} catch (IOException e) {
			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}



	}

    
    public void TvMapContentQuickFiling() throws InterruptedException {
		
		WebUtility.Click(mapContent);
		WebUtility.Wait();
		WebUtility.Click(addContent);
	
	    WebUtility.element_to_be_visible(mapContentAssign.get(0));
	    checkBox.get(0).click();
	    WebUtility.Click(assignContent);
		
		WebUtility.element_to_be_clickable(doneButtonActive);
		String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
		sa.assertTrue(doneButtonActiveColor.contains("255, 255, 255") || doneButtonActiveColor.contains("255,255,255"), "TV Quick Filing - Map Content Mark As Done Assertion Fails");
		WebUtility.Click(doneButtonActive);

		try {
			TestUtil.captureScreenshot("TV Quick Filing - Map Content Section","TV Show");
		} catch (IOException e) {
			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}
		WebUtility.Wait();
	}
    
    
    public void TvQuickFilingPublishFlow() throws InterruptedException {
        //---------------------------------Publish --------------------------------------------------------------------------------        
    	/*
 	
    	//------EDIT Flow for Testing ----------------------------------------------
    	dashboard.click();
		Thread.sleep(2000);
		tvShowButton.click();
		Thread.sleep(2000);
		searchString.click();
		searchString.sendKeys("Zee6 The Expanse Suite");
		Thread.sleep(2000);
		WebUtility.element_to_be_clickable(editButton.get(0));
		editButton.get(0).click();
		Thread.sleep(2000);
		editTvShow.click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//input[@name= 'selectJourney' and @value = 2]")).click();
		yes.get(0).click();
	*/	
		//--Verifying TvShow QuickFiling Draft Status
		try
		{
			tvShowProperties.click();
			WebUtility.element_to_be_visible(contentStatus);
			TestUtil.captureScreenshot("TvShow QuickFiling Draft Status","TV Show");
			String expectedStatus = "Draft";
			String actualStatus = contentStatus.getText();
			sa.assertEquals(actualStatus,expectedStatus,"TvShow QuickFiling Draft Status is Failed");
			Reporter.log("TvShow QuickFiling Draft Status is Passed",true);	
		}
		catch (Exception e)
		{
			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}
		
    	 JavascriptExecutor js = (JavascriptExecutor) driver;
    	 Actions actionProvider = new Actions(driver);
    	 WebUtility.Wait();
         WebUtility.Click(checklist);

         WebUtility.Wait();
         js.executeScript("window.scrollBy(0,600)");
         WebUtility.Click(scheduledCountry.get(0));
         WebUtility.Click(dropDownList.get(0));

         WebUtility.Click(publishContent);
         WebUtility.Click(yes.get(0));
         WebUtility.Click(ok); 	

         js.executeScript("window.scrollTo(0,0)");
         
         //--Verifying TvShow QuickFiling Published Status
         try {
 			tvShowProperties.click();
 			WebUtility.element_to_be_visible(contentStatus);
             TestUtil.captureScreenshot("TvShow QuickFiling Published Status","TV Show");
 			String expectedStatus = "Published";
 			String actualStatus = contentStatus.getText();
 			sa.assertEquals(actualStatus,expectedStatus,"TvShow QuickFiling Published Status is Failed");
 			Reporter.log("TvShow QuickFiling Published Status is Passed",true);
         } catch (IOException e) {
             System.out.println("Screenshot Capture Failed ->" + e.getMessage());
         }
         WebUtility.Wait();

        //----------------Republish -----------------------------------------------------------------------------
            
        try {
	        license.click();
	        WebUtility.Wait();
	       	
	        WebUtility.Click(licenseStatusButton);
	        WebUtility.Click(inactivateLicenseDropdown);
	        WebUtility.Wait();
	        actionProvider.moveToElement(inactivateLicenseReason).click().build().perform();
	        WebUtility.Click(dropDownList.get(0));
	        WebUtility.Click(yes.get(0));
	        WebUtility.Click(doneButtonActive);
       }
       catch(Exception e)
       {
    	   try
    	   {
    	   WebUtility.Click(no);
    	   }
    	   catch(Exception e1) {
    		 e1.printStackTrace();
    	   }
    	   
    	   WebUtility.Click(tvShowProperties);
    	   
           WebUtility.Click(note);
           note.sendKeys(Keys.CONTROL + "a");
           note.sendKeys(Keys.DELETE);
           note.sendKeys("Automation Test 2");  	            
		   
           WebUtility.Click(title);
		   
           WebUtility.Click(doneButtonActive);
           e.printStackTrace();

       }
        
        WebUtility.Click(checklist);
        js.executeScript("window.scrollBy(0,600)");
        WebUtility.Click(republishContent);
        WebUtility.Click(yes.get(0));
       // WebUtility.Click(yes.get(1));
        WebUtility.Click(ok);
        js.executeScript("window.scrollTo(0,0)");
        WebUtility.Wait();
        
		//--Verifying TvShow QuickFiling Republished status
		try
		{
				tvShowProperties.click();
				WebUtility.element_to_be_visible(contentStatus);
				TestUtil.captureScreenshot("TvShow QuickFiling Republished Status","TV Show");
				String expectedStatus = "Published";
				String actualStatus = contentStatus.getText();
				sa.assertEquals(actualStatus,expectedStatus,"TvShow QuickFiling Republished Status is Failed");
				Reporter.log("TvShow QuickFiling Republished Status is Passed",true);	
		}
		catch (Exception e)
		{
				System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}


        //------------------------------Unpublish --------------------------------------------------------------------------------------------        
        js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
        WebUtility.Click(checklist);
        WebUtility.Click(unpublishContent);

        WebUtility.Click(unpublishReason);
        WebUtility.Click(dropDownList.get(0));
        WebUtility.Click(yes.get(0));
        WebUtility.Click(ok);
        js.executeScript("window.scrollTo(0,0)");
        
        //--Verifying TvShow QuickFiling Unpublished Status
        try {
			tvShowProperties.click();
			WebUtility.element_to_be_visible(contentStatus);
            TestUtil.captureScreenshot("TvShow QuickFiling Unpublished Status","TV Show");
			String expectedStatus = "Unpublished";
			String actualStatus = contentStatus.getText();
			sa.assertEquals(actualStatus,expectedStatus,"TvShow QuickFiling Unpublished Status is Failed");
			Reporter.log("TvShow QuickFiling Unpublished Status is Passed",true);
        } catch (IOException e) {
            System.out.println("Screenshot Capture Failed ->" + e.getMessage());
        }
        WebUtility.Wait();
        publishedHistory.click();
        WebUtility.Wait();
        try {
            TestUtil.captureScreenshot("TV Quick Filing - Published History","TV Show");
        } catch (IOException e) {
            System.out.println("Screenshot Capture Failed ->" + e.getMessage());
        }

        WebUtility.Click(updateTVShowBreadcrumb);
        WebUtility.Wait();
        
        // Archive unpublished content -------------------------------------     
        WebUtility.Click(dashboard);
        WebUtility.Click(tvShowButton);
        WebUtility.Wait();
        WebUtility.Click(searchString);
        searchString.sendKeys(tvShowQuickFilingTitle);
        WebUtility.Wait();
		Archive.get(0).click();				
		ArchiveYes.click();
		WebUtility.Wait();
        WebUtility.Click(editButton.get(0));
		WebUtility.Wait();

		//--Verifying TvShow Quick Filing Archive status
		try
		{
			tvShowProperties.click();
			WebUtility.element_to_be_visible(contentStatus);
			TestUtil.captureScreenshot("TvShow QuickFiling Archived Status","Movie");
			String expectedStatus = "Archived";
			String actualStatus = contentStatus.getText();
			sa.assertEquals(actualStatus,expectedStatus,"TvShow QuickFiling Archived Status is Failed");
			Reporter.log("TvShow QuickFiling Archived Status is Passed",true);	
		}
		catch (Exception e)
		{
			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}
		WebUtility.Wait();
		
		// Restore Content to draft mode ---------------------------------------
        WebUtility.Click(dashboard);
        WebUtility.Click(tvShowButton);
        WebUtility.Wait();
        WebUtility.Click(searchString);
        searchString.sendKeys(tvShowQuickFilingTitle);
        WebUtility.Wait();
		WebUtility.javascript(Restore);			
		WebUtility.Wait();
		WebUtility.javascript(RestoreYes);
		WebUtility.Wait();
        WebUtility.Click(editButton.get(0));
		WebUtility.Wait();
		
		//--Verifying TvShow Quick Filing Restore Status
		try
		{
			tvShowProperties.click();
			WebUtility.element_to_be_visible(contentStatus);
			TestUtil.captureScreenshot("TvShow QuickFiling Restore Status","Movie");
			String expectedStatus = "Draft";
			String actualStatus = contentStatus.getText();
			sa.assertEquals(actualStatus,expectedStatus,"TvShow QuickFiling Restore Status is Failed");
			Reporter.log("TvShow QuickFiling Restore Status is Passed",true);	
		}
		catch (Exception e)
		{
			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}

        
        //---------TvShow Scheduling--------------------------------
    	WebUtility.Click(checklist);
    	WebUtility.Wait();
        js.executeScript("window.scrollTo(0,document.body.scrollHeight);");

        WebUtility.Click(scheduleContent);

       // System.out.println("--------> Current Time = "+currentTime.getAttribute("innerText").substring(20, 28));
       // String currentTimeString  = currentTime.getAttribute("innerText").substring(20, 27);
    	//js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
    	
        WebUtility.Click(scheduledPublicationTime);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        WebUtility.Click(datePickerMinutes);
        actionProvider.moveToElement(datePickerMinutesClock).click().build().perform();      
        datePicker.sendKeys(Keys.ESCAPE);	
						  

        WebUtility.Click(scheduledContentCountry);
        WebUtility.Click(dropDownList.get(0));

        WebUtility.Click(scheduleContentButton);
        WebUtility.Click(yes.get(0));
        WebUtility.Wait();
        WebUtility.Click(ok);

    	js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
        WebUtility.Wait();
        js.executeScript("window.scrollTo(0,0);");
        WebUtility.Wait();

        //System.out.println("--------> Scheduled Time = "+scheduledTime.getAttribute("innerText").substring(12, 20));
        //String scheduledTimeString = scheduledTime.getAttribute("innerText").substring(12, 20);
    	//js.executeScript("window.scrollTo(0,document.body.scrollHeight);");


    	//sa.assertTrue(scheduledTimeString.contains(currentTimeString),"TV Shows Quick Filing - Current Time and Scheduled Time are not Equal");
      //--Verifying TvShow QuickFiling Scheduled Status
        try {
			tvShowProperties.click();
			WebUtility.element_to_be_visible(contentStatus);
            TestUtil.captureScreenshot("TvShow QuickFiling Scheduled Status","TV Show");
			String expectedStatus = "Scheduled";
			String actualStatus = contentStatus.getText();
			sa.assertEquals(actualStatus,expectedStatus,"TvShow QuickFiling Scheduled Status is Failed");
			Reporter.log("TvShow QuickFiling Scheduled Status is Passed",true);
        } catch (IOException e) {
            System.out.println("Screenshot Capture Failed ->" + e.getMessage());
        }
        
        WebUtility.Wait();
        WebUtility.Click(checklist);
        WebUtility.Click(deleteSchedule);
        WebUtility.Click(yes.get(0));
        WebUtility.Click(ok);
        
        WebUtility.Wait();
        js.executeScript("window.scrollTo(0,document.body.scrollHeight)");
        WebUtility.Click(scheduledCountry.get(0));
        WebUtility.Click(dropDownList.get(0));

        WebUtility.Click(publishContent);
        WebUtility.Click(yes.get(0));
        WebUtility.Click(ok); 	

        js.executeScript("window.scrollTo(0,0)");
        WebUtility.Wait();
    }    
    //-------------------------------------------------- End of Quick Filling ----------------------------------------------------------------------------
    
    
   
    
    //-----------------------------------------------------TV Show Single Landing ----------------------------------------------------------------------
    
    public void TvPropertiesSingleLanding() throws InterruptedException, Exception {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        WebUtility.Click(dashboard);
        WebUtility.Click(tvShowButton);
/*      
        //------EDIT Flow for Testing ----------------------------------------------

		searchString.click();
		searchString.sendKeys("The Expanse Single Landing");
		Thread.sleep(2000);
		WebUtility.element_to_be_clickable(editButton.get(0));
		editButton.get(0).click();
		Thread.sleep(2000);
		editTvShow.click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//input[@name= 'selectJourney' and @value = 3]")).click();
		yes.get(0).click();
*/

	    quickFilling.click();
		WebUtility.Click(createSingleLanding);
		WebUtility.Wait();
		

        // Content Properties section -----------------------------------------
        
		WebUtility.element_to_be_clickable(title);
		title.click();
        title.sendKeys(Keys.CONTROL + "a");
        title.sendKeys(Keys.BACK_SPACE);
        title.sendKeys(tvShowSingleLandingTitle);

        shortDesc.click();
        shortDesc.sendKeys(Keys.CONTROL + "a");
        shortDesc.sendKeys(Keys.BACK_SPACE);
        shortDesc.sendKeys("The Expanse is an American science fiction television series developed by Mark Fergus and Hawk Ostby, based on the series of novels of the same name by James S. A. Corey.");
         
       
        subType.click();
        WebUtility.element_to_be_clickable(dropDownList.get(0));
        dropDownList.get(0).click();

        category.click();
        WebUtility.element_to_be_clickable(dropDownList.get(0));
        dropDownList.get(0).click();

        primaryGenre.click();
        WebUtility.element_to_be_clickable(dropDownList.get(0));
        dropDownList.get(0).click();
        
        WebUtility.Click(zee5ReleaseDate);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ESCAPE);
		
        WebUtility.Click(telecastDate);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        Thread.sleep(2000);
        datePicker.sendKeys(Keys.ESCAPE);  
		
		WebUtility.Click(audioLanguage);
		WebUtility.element_to_be_clickable(dropDownList.get(0));
		dropDownList.get(0).click();

		WebUtility.Click(nextButton);
		WebUtility.Wait();
		
		WebUtility.Click(broadcastState);
        WebUtility.element_to_be_clickable(dropDownList.get(0));
        dropDownList.get(0).click();

		WebUtility.element_to_be_clickable(upcomingPage);
		upcomingPage.sendKeys(Keys.CONTROL + "a");
		upcomingPage.sendKeys(Keys.BACK_SPACE);
		upcomingPage.sendKeys("Automation Upcoming Page Text");

		WebUtility.Click(previousButton);

	/*	
		//driver.findElement(By.xpath("//span[contains(text(),'Global')]")).click();
		
		globalTitle.click();
		globalTitle.sendKeys(Keys.CONTROL + "a");
		globalTitle.sendKeys(Keys.BACK_SPACE);
		Thread.sleep(1000);
		globalTitle.sendKeys("Automation Test Data");
		
		
		globalCountryQuickFilling.click();
		WebUtility.element_to_be_clickable(dropDownList.get(0));
		dropDownList.get(0).click();
		
		zee5ReleaseDate.click();
		zee5ReleaseDate.sendKeys(Keys.SPACE);
		zee5ReleaseDate.sendKeys(Keys.ARROW_RIGHT);
		zee5ReleaseDate.sendKeys(Keys.ARROW_RIGHT);
		zee5ReleaseDate.sendKeys(Keys.ENTER);

		globalShortDesc.click();
		globalShortDesc.sendKeys(Keys.CONTROL + "a");
		globalShortDesc.sendKeys(Keys.BACK_SPACE);
		globalShortDesc.sendKeys("Automation Test Data");

		WebUtility.element_to_be_clickable(broadcastState);
		broadcastState.click();
		WebUtility.element_to_be_clickable(dropDownList.get(0));
		dropDownList.get(0).click();
		
		upcomingPage.sendKeys(Keys.CONTROL + "a");
		upcomingPage.sendKeys(Keys.BACK_SPACE);
		upcomingPage.sendKeys("Automation Upcoming Page Text");
		
		globalTitle.click();
		previousButton.click();
*/		
		js.executeScript("window.scrollTo(0,0);");
		WebUtility.Wait();
		String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
		sa.assertTrue(doneButtonActiveColor.contains("255, 255, 255") || doneButtonActiveColor.contains("255,255,255"), "TV Single Landing - Properties Mark As Done Assertion Fails");
		WebUtility.element_to_be_clickable(doneButtonActive);
		doneButtonActive.click();
		WebUtility.Wait();
    }
    
    public void TvCastCrewSingleLanding() throws InterruptedException {
    	WebUtility.Click(castCrew);
   
    	WebUtility.Click(actor);
    	actor.sendKeys(Keys.CONTROL + "a");
		actor.sendKeys(Keys.BACK_SPACE);
		actor.sendKeys("Salman Khan Single Landing");
		WebUtility.element_to_be_clickable(dropDownList.get(0));
		actor.sendKeys(Keys.ARROW_DOWN);
		actor.sendKeys(Keys.ENTER);		
		
		WebUtility.Click(character);
		character.sendKeys(Keys.CONTROL + "a");
		character.sendKeys(Keys.BACK_SPACE);
		character.sendKeys("Test Character Single Landing");
				
		WebUtility.Click(director);
		director.sendKeys(Keys.CONTROL + "a");
		director.sendKeys(Keys.BACK_SPACE);
		director.sendKeys("Automation Test Data Single Landing");
        WebUtility.Wait();
        WebUtility.Click(dropDownList.get(0));
		
		
		WebUtility.Wait();
		String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
		sa.assertTrue(doneButtonActiveColor.contains("255, 255, 255") || doneButtonActiveColor.contains("255,255,255"), "TV Single Landing - Cast & Crew Mark As Done Assertion Fails");
		WebUtility.Click(doneButtonActive);
    }
    
    public void TvLicenseSingleLanding() throws InterruptedException {
    /*	
    	// ------EDIT Flow for Testing ----------------------------------------------
 		searchString.click();
		searchString.sendKeys("The Expanse Single Landing");
		Thread.sleep(2000);
		WebUtility.element_to_be_clickable(editButton.get(0));
		editButton.get(0).click();
		Thread.sleep(2000);
		editTvShow.click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//input[@name= 'selectJourney' and @value = 3]")).click();
		yes.get(0).click();
    	Thread.sleep(2000);
    	*/		
    	
		WebUtility.Click(license);
		WebUtility.Click(createLicense);

		WebUtility.Click(licenseSetName);
        licenseSetName.sendKeys("Automation Test Data");				
		
        WebUtility.Click(licenseFromDate);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ESCAPE);

        WebUtility.Click(licenseToDate);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ESCAPE);
		
		
        WebUtility.Click(licenseCountry);
        for (int x = 0; x < dropDownList.size(); x++) {
        	if(x<4) {
            WebUtility.Click(dropDownList.get(x));
        	}
        	else {
        		break;
        	}
        }
		
		
		WebUtility.Click(licenseBusinessType);
		WebUtility.Click(dropDownList.get(0));

		WebUtility.Wait();
		driver.findElement(By.xpath("//span[contains(text(),'SAVE')]")).click();
		
		WebUtility.Click(license);
		WebUtility.Click(createLicense);
		
		WebUtility.Click(licenseSetName);
        licenseSetName.sendKeys("Automation Test Data II");				   

        WebUtility.Click(licenseFromDate);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ESCAPE);

        WebUtility.Click(licenseToDate);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ESCAPE);
			
        WebUtility.Click(licenseCountry);
        for (int x = 0; x < dropDownList.size(); x++) {
        	if(x<4) {
            WebUtility.Click(dropDownList.get(x));
        	}
        	else {
        		break;
        	}
        }
		
		WebUtility.Click(licenseBusinessType);
		WebUtility.Click(dropDownList.get(0));

		WebUtility.Wait();
		driver.findElement(By.xpath("//span[contains(text(),'SAVE')]")).click();

		WebUtility.element_to_be_clickable(doneButtonActive);
		String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
		sa.assertTrue(doneButtonActiveColor.contains("255, 255, 255") || doneButtonActiveColor.contains("255,255,255"), "TV Single Landing - License Mark As Done Assertion Fails");
		WebUtility.Click(doneButtonActive);
		WebUtility.Wait();
		
		try {
			TestUtil.captureScreenshot(" Single Landing - License Section","TV Show");
		} catch (IOException e) {
			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}
		

        WebUtility.Click(expiredLicense);
        Thread.sleep(2000);
        WebUtility.Click(expiredLicenseBack);
        Thread.sleep(2000);

        licenseFilters.click();
        WebUtility.Click(licenseFromDate);
        datePicker.sendKeys(Keys.ARROW_LEFT);
        datePicker.sendKeys(Keys.ARROW_LEFT);
        datePicker.sendKeys(Keys.ESCAPE);


        WebUtility.Click(licenseToDate);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ESCAPE);
        Thread.sleep(2000);

        applyFilter.click();

        Thread.sleep(3000);
        licenseFilters.click();
        clearFilter.click();

        WebUtility.Click(licenseFilters);
        Thread.sleep(2000);
        licenseFilterBusinessType.click();
        WebUtility.Click(dropDownList.get(0));
        Thread.sleep(2000);
        applyFilter.click();

        Thread.sleep(3000);
        licenseFilters.click();
        clearFilter.click();

        WebUtility.Click(licenseFilters);
        Thread.sleep(2000);
        licenseFilterActiveStatus.click();
        Thread.sleep(2000);
        applyFilter.click();

        Thread.sleep(3000);
        licenseFilters.click();
        clearFilter.click();

        WebUtility.Click(licenseFilters);
        Thread.sleep(2000);
        licenseFilterInActiveStatus.click();
        Thread.sleep(2000);
        applyFilter.click();

        Thread.sleep(3000);
        licenseFilters.click();
        clearFilter.click();
	}
    	    
   
    public void TvImagesSingleLanding() throws InterruptedException {
    	
    	JavascriptExecutor js = (JavascriptExecutor) driver;  
 		WebUtility.Click(images);
		
 		WebUtility.Wait();
        cover.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\CoverPNG.png");

        WebUtility.Wait();
        appcover.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\AppCoverPNG.png");

        WebUtility.Wait();
        list.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\List.jpg");

        WebUtility.Wait();
        square.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\Square.jpg");
        
        js.executeScript("window.scrollBy(0,600)");

        Thread.sleep(2000);
        tvCover.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\TVCover.jpg");

        WebUtility.Wait();
        portrait.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\portraitPNG.png");

        WebUtility.Wait();
        listClean.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\ListClean.jpg");

		js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
		
		WebUtility.Wait();
        portraitClean.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\PortraitCleanPNG.png");

        WebUtility.Wait();
        telcoSquare.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\TelcoSquare.jpg");

        WebUtility.Wait();
        passport.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\Passport.jpg");


        WebUtility.element_to_be_fluentwait(imagesCreateNewSet);
        WebUtility.Click(imagesCreateNewSet);

        
        WebUtility.Click(newSetName);
  		newSetName.sendKeys("Automation Test Data");		
 		
 		newSetCountry.click();
 		WebUtility.Click(dropDownList.get(0));
 		closeDropdown.click();
 		
 		newSetPlatform.click();
 		WebUtility.Click(dropDownList.get(0));
 		closeDropdown.click();
 		
 		newSetGender.click();
 		WebUtility.Click(dropDownList.get(0));
 		closeDropdown.click();
 		
 		newSetGenre.click();
 		WebUtility.Click(dropDownList.get(0));
 		closeDropdown.click();
 		
 		newSetAgeGroup.click();
 		WebUtility.Click(dropDownList.get(0));
 		closeDropdown.click();
 		
 		newSetLanguage.click();
 		WebUtility.Click(dropDownList.get(0));
 		closeDropdown.click();
 		
 		newSetOthers.click();
 		newSetOthers.sendKeys("Automation Test Data");
 		
 		WebUtility.Click(save);
  		
 		WebUtility.Click(imageSets.get(1));	
 		
 		js.executeScript("window.scrollTo(0,0)");
    
 		WebUtility.Wait();
        cover.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\CoverPNG.png");

        WebUtility.Wait();
        appcover.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\AppCoverPNG.png");

        WebUtility.Wait();
        list.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\List.jpg");

        WebUtility.Wait();
        square.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\Square.jpg");
        
        js.executeScript("window.scrollBy(0,600)");

        Thread.sleep(2000);
        tvCover.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\TVCover.jpg");

        WebUtility.Wait();
        portrait.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\portraitPNG.png");

        WebUtility.Wait();
        listClean.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\ListClean.jpg");

		js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
		
		WebUtility.Wait();
        portraitClean.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\PortraitCleanPNG.png");

        WebUtility.Wait();
        telcoSquare.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\TelcoSquare.jpg");

        WebUtility.Wait();
        passport.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\Passport.jpg");

        
        WebUtility.element_to_be_fluentwait(imagesCreateNewSet);
        WebUtility.Click(imagesCreateNewSet);

        
        WebUtility.Click(newSetName);
  		newSetName.sendKeys("Automation Test Data II");		
 		
 		newSetCountry.click();
 		WebUtility.Click(dropDownList.get(1));
 		closeDropdown.click();
 		
 		newSetPlatform.click();
 		WebUtility.Click(dropDownList.get(1));
 		closeDropdown.click();
 		
 		newSetGender.click();
 		WebUtility.Click(dropDownList.get(1));
 		closeDropdown.click();
 		
 		newSetGenre.click();
 		WebUtility.Click(dropDownList.get(1));
 		closeDropdown.click();
 		
 		newSetAgeGroup.click();
 		WebUtility.Click(dropDownList.get(1));
 		closeDropdown.click();
 		
 		newSetLanguage.click();
 		WebUtility.Click(dropDownList.get(1));
 		closeDropdown.click();
 		
 		newSetOthers.click();
 		newSetOthers.sendKeys("Automation Test Data II");
 		
 		WebUtility.Click(save);
  		
 		WebUtility.Click(imageSets.get(2));	
 		
 		js.executeScript("window.scrollTo(0,0)");
    
 		WebUtility.Wait();
        cover.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\CoverPNG.png");

        WebUtility.Wait();
        appcover.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\AppCoverPNG.png");

        WebUtility.Wait();
        list.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\List.jpg");

        WebUtility.Wait();
        square.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\Square.jpg");
        
        js.executeScript("window.scrollBy(0,600)");

        Thread.sleep(2000);
        tvCover.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\TVCover.jpg");

        WebUtility.Wait();
        portrait.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\portraitPNG.png");

        WebUtility.Wait();
        listClean.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\ListClean.jpg");

		js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
		
		WebUtility.Wait();
        portraitClean.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\PortraitCleanPNG.png");

        WebUtility.Wait();
        telcoSquare.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\TelcoSquare.jpg");

        WebUtility.Wait();
        passport.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\Passport.jpg");

        // ---------------------- Inactivate Image Set ------------------------
		try {
			js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
			WebUtility.Wait();
			WebUtility.Click(imageSetActiveButton);
			WebUtility.Wait();
			WebUtility.Click(dropDownList.get(0));
			WebUtility.javascript(yes.get(0));
			WebUtility.Wait();
		}
        catch(Exception e) {
        	e.printStackTrace();
        }
		
		//------------------------ Delete Image Set -----------------------------
		try {
			removeSet.get(1).click();
			WebUtility.Wait();
			yes.get(0).click();
			WebUtility.Wait();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		WebUtility.Wait();
        String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
        sa.assertTrue(doneButtonActiveColor.contains("255, 255, 255") || doneButtonActiveColor.contains("255,255,255"), "TV Single Landing - Images Mark As Done Assertion Fails");
        WebUtility.Click(doneButtonActive);


        js.executeScript("window.scrollTo(0,0);");
        WebUtility.Wait();
        WebUtility.Click(imageSets.get(0));
 		try {
            TestUtil.captureScreenshot("TV Show Single Landing - Images Section Default Set","TV Show");
        } catch (IOException e) {
            System.out.println("Screenshot Capture Failed ->" + e.getMessage());
        }
 	
        js.executeScript("window.scrollTo(0,document.body.scrollHeight);");

        try {
            TestUtil.captureScreenshot("TV Show Single Landing - Images Section Default Set 2","TV Show");
        } catch (IOException e) {
            System.out.println("Screenshot Capture Failed ->" + e.getMessage());
        }
        
        WebUtility.Click(imageSets.get(1));
        js.executeScript("window.scrollTo(0,0);");
        try {
            TestUtil.captureScreenshot("TV Show Single Landing - Images Section Created Set","TV Show");
        } catch (IOException e) {
            System.out.println("Screenshot Capture Failed ->" + e.getMessage());
        }
 	
        js.executeScript("window.scrollTo(0,document.body.scrollHeight);");

        try {
            TestUtil.captureScreenshot("TV Show Single Landing - Images Section Created Set 2","TV Show");
        } catch (IOException e) {
            System.out.println("Screenshot Capture Failed ->" + e.getMessage());
        }

	}
    
    public void TvSeoDetailsSingleLanding() throws InterruptedException {

		WebUtility.Click(seoDetails);

        WebUtility.Wait();
        WebUtility.Click(seoTitleTag);
        seoTitleTag.sendKeys(Keys.CONTROL + "a");
        seoTitleTag.sendKeys(Keys.BACK_SPACE);
        WebUtility.Wait();
        seoTitleTag.sendKeys("Automation Test Data");

        seoMetaDescription.click();
        seoMetaDescription.sendKeys(Keys.CONTROL + "a");
        seoMetaDescription.sendKeys(Keys.BACK_SPACE);
        WebUtility.Wait();
        seoMetaDescription.sendKeys("Automation Test Data");

        seoMetaSynopsis.click();
        seoMetaSynopsis.sendKeys(Keys.CONTROL + "a");
        seoMetaSynopsis.sendKeys(Keys.BACK_SPACE);
        WebUtility.Wait();
        seoMetaSynopsis.sendKeys("Automation Test Data");
		
        seoH1Heading.click();
        seoH1Heading.sendKeys(Keys.CONTROL + "a");
        seoH1Heading.sendKeys(Keys.BACK_SPACE);
        WebUtility.Wait();
        seoH1Heading.sendKeys("Automation Test Data");

        seoH2Heading.click();
        seoH2Heading.sendKeys(Keys.CONTROL + "a");
        seoH2Heading.sendKeys(Keys.BACK_SPACE);
        WebUtility.Wait();
        seoH2Heading.sendKeys("Automation Test Data");

	
		for (int i = 0; i < seoCheckboxes.size(); i++) {
			if (seoCheckboxes.get(i).isSelected() == false)
				seoCheckboxes.get(i).click();
		}

		/*
		WebUtility.element_to_be_clickable(seoTitleTag);
		seoTitleTag.click();
		seoTitleTag.sendKeys(Keys.CONTROL + "a");
		seoTitleTag.sendKeys(Keys.BACK_SPACE);
		Thread.sleep(2000);
		seoTitleTag.sendKeys("Global Field Automation Test Data");

		Thread.sleep(2000);
		seoMetaDescription.click();
		seoMetaDescription.sendKeys(Keys.CONTROL + "a");
		seoMetaDescription.sendKeys(Keys.BACK_SPACE);
		Thread.sleep(2000);
		seoMetaDescription.sendKeys("Global Field Automation Test Data");

		Thread.sleep(2000);
		seoMetaSynopsis.click();
		seoMetaSynopsis.sendKeys(Keys.CONTROL + "a");
		seoMetaSynopsis.sendKeys(Keys.BACK_SPACE);
		Thread.sleep(2000);
		seoMetaSynopsis.sendKeys("Global Field Automation Test Data");

		Thread.sleep(2000);
		seoH1Heading.click();
		seoH1Heading.sendKeys(Keys.CONTROL + "a");
		seoH1Heading.sendKeys(Keys.BACK_SPACE);
		Thread.sleep(2000);
		seoH1Heading.sendKeys("Global Field Automation Test Data");

		js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
		Thread.sleep(2000);
		seoH2Heading.click();
		seoH2Heading.sendKeys(Keys.CONTROL + "a");
		seoH2Heading.sendKeys(Keys.BACK_SPACE);
		Thread.sleep(2000);
		seoH2Heading.sendKeys("Global Field Automation Test Data");

		Thread.sleep(2000);
		for (int i = 0; i < seoCheckboxes.size(); i++) {
			if (seoCheckboxes.get(i).isSelected() == false)
				seoCheckboxes.get(i).click();
		}

		previousButton.click();
*/		WebUtility.Wait();
		String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
		sa.assertTrue(doneButtonActiveColor.contains("255, 255, 255") || doneButtonActiveColor.contains("255,255,255"), "TV Single Landing - Seo Details Mark As Done Assertion Fails");
		WebUtility.Click(doneButtonActive);

		try {
			TestUtil.captureScreenshot("Single Landing - SEO Details Section","TV Show");
		} catch (IOException e) {
			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}
	}
   
    public void TvMapContentSingleLanding() throws InterruptedException {
		
		WebUtility.Click(mapContent);
		WebUtility.Wait();
		WebUtility.Click(addContent);
	
	    WebUtility.element_to_be_visible(mapContentAssign.get(0));
	    checkBox.get(0).click();
	    WebUtility.Click(assignContent);
		
		WebUtility.element_to_be_clickable(doneButtonActive);
		String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
		sa.assertTrue(doneButtonActiveColor.contains("255, 255, 255") || doneButtonActiveColor.contains("255,255,255"), "TV Single Landing - Map Content Mark As Done Assertion Fails");
		WebUtility.Click(doneButtonActive);

		try {
			TestUtil.captureScreenshot("TV Shows Single Landing - Map Content Section","TV Show");
		} catch (IOException e) {
			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}

	}

    public void TvSingleLandingPublishFlow() throws InterruptedException {
        //---------------------------------Publish --------------------------------------------------------------------------------        
    	
 	/*
    	//------EDIT Flow for Testing ----------------------------------------------
    	dashboard.click();
		Thread.sleep(2000);
		tvShowButton.click();
		Thread.sleep(2000);
		searchString.click();
		searchString.sendKeys(tvShowTitle);
		Thread.sleep(2000);
		WebUtility.element_to_be_clickable(editButton.get(0));
		editButton.get(0).click();
		Thread.sleep(2000);
		editTvShow.click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//input[@name= 'selectJourney' and @value = 3]")).click();
		yes.get(0).click();
	*/
		//--Verifying TvShow SingleLanding Draft Status
		try
		{
			tvShowProperties.click();
			WebUtility.element_to_be_visible(contentStatus);
			TestUtil.captureScreenshot("TvShow SingleLanding Draft Status","TV Show");
			String expectedStatus = "Draft";
			String actualStatus = contentStatus.getText();
			sa.assertEquals(actualStatus,expectedStatus,"TvShow SingleLanding Draft Status is Failed");
			Reporter.log("TvShow SingleLanding Draft Status is Passed",true);	
		}
		catch (Exception e)
		{
			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}
		
    	 JavascriptExecutor js = (JavascriptExecutor) driver;
    	 Actions actionProvider = new Actions(driver);
    	 WebUtility.Wait();
         WebUtility.Click(checklist);

         WebUtility.Wait();
         js.executeScript("window.scrollBy(0,600)");
         WebUtility.Click(scheduledCountry.get(0));
         WebUtility.Click(dropDownList.get(0));

         WebUtility.Click(publishContent);
         WebUtility.Click(yes.get(0));
         WebUtility.Click(ok); 	

         js.executeScript("window.scrollTo(0,0)");
         
         //--Verifying TvShow Single Landing Published Status
         try {
 			tvShowProperties.click();
 			WebUtility.element_to_be_visible(contentStatus);
             TestUtil.captureScreenshot("TvShow Singlelanding Published Status","TV Show");
 			String expectedStatus = "Published";
 			String actualStatus = contentStatus.getText();
 			sa.assertEquals(actualStatus,expectedStatus,"TvShow Singlelanding Published Status is Failed");
 			Reporter.log("TvShow Singlelanding Published Status is Passed",true);
         } catch (IOException e) {
             System.out.println("Screenshot Capture Failed ->" + e.getMessage());
         }
         WebUtility.Wait();

        //----------------Republish -----------------------------------------------------------------------------
            
        try {
	        license.click();
	        WebUtility.Wait();
	       	
	        WebUtility.Click(licenseStatusButton);
	        WebUtility.Click(inactivateLicenseDropdown);
	        WebUtility.Wait();
	        actionProvider.moveToElement(inactivateLicenseReason).click().build().perform();
	        WebUtility.Click(dropDownList.get(0));
	        WebUtility.Click(yes.get(0));
	        WebUtility.Click(doneButtonActive);
       }
       catch(Exception e)
       {
    	   try
    	   {
    	   WebUtility.Click(no);
    	   }
    	   catch(Exception e1) {
    		 e1.printStackTrace();
    	   }
    	   
    	   WebUtility.Click(tvShowProperties);
    	   
           WebUtility.Click(note);
           note.sendKeys(Keys.CONTROL + "a");
           note.sendKeys(Keys.DELETE);
           note.sendKeys("Automation Test 2");  	   
           
           WebUtility.Click(title);
           
           WebUtility.Click(doneButtonActive);
           e.printStackTrace();
           
    }

        WebUtility.Click(checklist);
        js.executeScript("window.scrollBy(0,600)");
        WebUtility.Click(republishContent);

        WebUtility.Click(yes.get(0));
       // WebUtility.Click(yes.get(1));
        WebUtility.Click(ok);
        js.executeScript("window.scrollTo(0,0)");
        
		//--Verifying TvShow Single Landing Republished status
		try
		{
				tvShowProperties.click();
				WebUtility.element_to_be_visible(contentStatus);
				TestUtil.captureScreenshot("TvShow Singlelanding Republished Status","TV Show");
				String expectedStatus = "Published";
				String actualStatus = contentStatus.getText();
				sa.assertEquals(actualStatus,expectedStatus,"TvShow Singlelanding Republished Status is Failed");
				Reporter.log("TvShow Singlelanding Republished Status is Passed",true);	
		}
		catch (Exception e)
		{
				System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}
        WebUtility.Wait();


        //------------------------------Unpublish --------------------------------------------------------------------------------------------        
        js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
        WebUtility.Click(checklist);
        WebUtility.Click(unpublishContent);

        WebUtility.Click(unpublishReason);
        WebUtility.Click(dropDownList.get(0));
        WebUtility.Click(yes.get(0));
        WebUtility.Click(ok);
        js.executeScript("window.scrollTo(0,0)");
        
        //--Verifying TvShow Single Landing Unpublished Status
        try {
			tvShowProperties.click();
			WebUtility.element_to_be_visible(contentStatus);
            TestUtil.captureScreenshot("TvShow Singlelanding Unpublished Status","TV Show");
			String expectedStatus = "Unpublished";
			String actualStatus = contentStatus.getText();
			sa.assertEquals(actualStatus,expectedStatus,"TvShow Singlelanding Unpublished Status is Failed");
			Reporter.log("TvShow Singlelanding Unpublished Status is Passed",true);
        } catch (IOException e) {
            System.out.println("Screenshot Capture Failed ->" + e.getMessage());
        }
        WebUtility.Wait();
        
        publishedHistory.click();
        WebUtility.Wait();
        try {
            TestUtil.captureScreenshot("TV Quick Filing - Published History","TV Show");
        } catch (IOException e) {
            System.out.println("Screenshot Capture Failed ->" + e.getMessage());
        }

        WebUtility.Click(updateTVShowBreadcrumb);
        WebUtility.Wait();
        
        // Archive unpublished content -------------------------------------     
        WebUtility.Click(dashboard);
        WebUtility.Click(tvShowButton);
        WebUtility.Wait();
        WebUtility.Click(searchString);
        searchString.sendKeys(tvShowSingleLandingTitle);
        WebUtility.Wait();
		Archive.get(0).click();				
		ArchiveYes.click();
		WebUtility.Wait();
        WebUtility.Click(editButton.get(0));
		WebUtility.Wait();

		//--Verifying TvShow SingleLanding Archive status
		try
		{
			tvShowProperties.click();
			WebUtility.element_to_be_visible(contentStatus);
			TestUtil.captureScreenshot("TvShow SingleLanding Archived Status","Movie");
			String expectedStatus = "Archived";
			String actualStatus = contentStatus.getText();
			sa.assertEquals(actualStatus,expectedStatus,"TvShow SingleLanding Archived Status is Failed");
			Reporter.log("TvShow SingleLanding Archived Status is Passed",true);	
		}
		catch (Exception e)
		{
			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}
		WebUtility.Wait();
		
		// Restore Content to draft mode ---------------------------------------
        WebUtility.Click(dashboard);
        WebUtility.Click(tvShowButton);
        WebUtility.Wait();
        WebUtility.Click(searchString);
        searchString.sendKeys(tvShowSingleLandingTitle);
        WebUtility.Wait();
		WebUtility.javascript(Restore);			
		WebUtility.Wait();
		WebUtility.javascript(RestoreYes);
		WebUtility.Wait();
        WebUtility.Click(editButton.get(0));
		WebUtility.Wait();
		
		//--Verifying TvShow SingleLanding Restore Status
		try
		{
			tvShowProperties.click();
			WebUtility.element_to_be_visible(contentStatus);
			TestUtil.captureScreenshot("TvShow SingleLanding Restore Status","Movie");
			String expectedStatus = "Draft";
			String actualStatus = contentStatus.getText();
			sa.assertEquals(actualStatus,expectedStatus,"TvShow SingleLanding Restore Status is Failed");
			Reporter.log("TvShow SingleLanding Restore Status is Passed",true);	
		}
		catch (Exception e)
		{
			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}
        
        //-TvShow Scheduling
     	WebUtility.Click(checklist);
         js.executeScript("window.scrollTo(0,document.body.scrollHeight);");

         WebUtility.Click(scheduleContent);
         
        // System.out.println("--------> Current Time = "+currentTime.getAttribute("innerText").substring(20, 28));
       //  String currentTimeString  = currentTime.getAttribute("innerText").substring(20, 27);
     	// js.executeScript("window.scrollTo(0,document.body.scrollHeight);");

         scheduledPublicationTime.click();
         datePicker.sendKeys(Keys.ARROW_RIGHT);
         WebUtility.Click(datePickerMinutes);
         actionProvider.moveToElement(datePickerMinutesClock).click().build().perform();      
         datePicker.sendKeys(Keys.ESCAPE);	
 						  

         WebUtility.Click(scheduledContentCountry);
         WebUtility.Click(dropDownList.get(0));

         WebUtility.Click(scheduleContentButton);
         WebUtility.Click(yes.get(0));
         WebUtility.Wait();
         WebUtility.Click(ok);

     	js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
        WebUtility.Wait();
        js.executeScript("window.scrollTo(0,0);");
        WebUtility.Wait();

       // System.out.println("--------> Scheduled Time = "+scheduledTime.getAttribute("innerText").substring(12, 20));
       // String scheduledTimeString = scheduledTime.getAttribute("innerText").substring(12, 20);
    	//js.executeScript("window.scrollTo(0,document.body.scrollHeight);");

    	//sa.assertTrue(scheduledTimeString.contains(currentTimeString),"TV Single Landing - Current Time and Scheduled Time are not Equal");
       //--Verifying TvShow SingleLanding Scheduled Status
         try {
 			tvShowProperties.click();
 			WebUtility.Wait();
 			WebUtility.element_to_be_visible(contentStatus);
            TestUtil.captureScreenshot("TvShow SingleLanding Scheduled Status","TV Show");
 			String expectedStatus = "Scheduled";
 			String actualStatus = contentStatus.getText();
 			sa.assertEquals(actualStatus,expectedStatus,"TvShow SingleLanding Scheduled Status is Failed");
 			Reporter.log("TvShow SingleLanding Scheduled Status is Passed",true);
         } catch (IOException e) {
             System.out.println("Screenshot Capture Failed ->" + e.getMessage());
         }
         
         WebUtility.Wait();
         WebUtility.Click(checklist);
         WebUtility.Click(deleteSchedule);
         WebUtility.Click(yes.get(0));
         WebUtility.Click(ok);
         
         WebUtility.Wait();
         js.executeScript("window.scrollTo(0,document.body.scrollHeight)");
         WebUtility.Click(scheduledCountry.get(0));
         WebUtility.Click(dropDownList.get(0));

         WebUtility.Click(publishContent);
         WebUtility.Click(yes.get(0));
         WebUtility.Click(ok); 	

         js.executeScript("window.scrollTo(0,0)");
         WebUtility.Wait();
    }																		  
    //-------------------------------------END of Single Landing -------------------------------------------------------------
    
    //-------------------------------------Sort and Filter --------------------------------------------------------------------
    public void TvShowSort() throws InterruptedException {
        dashboard.click();
        WebUtility.Wait();
        tvShowButton.click();
        WebUtility.Wait();
		
        //Sorting with TvShows Content
        searchContentType.click();
        WebUtility.Wait();  
        searchTVShow.click();
        WebUtility.Wait();   
        searchString.click();
        

        
        Sort.click();
		sortAscToDesc.click();
		WebUtility.Wait();  
		applySort.click();
		WebUtility.Wait();  
		
		
		Sort.click();
		sortDescToAsc.click();
		WebUtility.Wait();  
		applySort.click();
		WebUtility.Wait();  

		Sort.click();
		sortOldToNew.click();
		WebUtility.Wait();  
		applySort.click();
		WebUtility.Wait(); 
		
		Sort.click();
		sortNewToOld.click();
		WebUtility.Wait();  
		applySort.click();
		WebUtility.Wait();  

				
		//Sorting with Seasons Content
        searchContentType.click();
        WebUtility.Wait();
        searchSeasons.click();
        WebUtility.Wait();  
        
        Sort.click();
        sortAscToDesc.click();
        WebUtility.Wait(); 
		applySort.click();
		WebUtility.Wait();  
			
		
		Sort.click();
		sortDescToAsc.click();
		WebUtility.Wait(); 
		applySort.click();
		WebUtility.Wait();  
		
		Sort.click();
		sortOldToNew.click();
		WebUtility.Wait(); 
		applySort.click();
		WebUtility.Wait();  
		
		Sort.click();
		sortNewToOld.click();
		WebUtility.Wait(); 
		applySort.click();
		WebUtility.Wait();  		
			
        
		//Sorting with Episodes Content  
        searchContentType.click();
        WebUtility.Wait(); 
        searchEpisodes.click();
        WebUtility.Wait(); 
        
        Sort.click();
        sortAscToDesc.click();
        WebUtility.Wait(); 
		applySort.click();
		WebUtility.Wait();  
		
		
		Sort.click();
		sortDescToAsc.click();
		WebUtility.Wait(); 
		applySort.click();
		WebUtility.Wait();  
		
		
		Sort.click();
		sortNewToOld.click();
		WebUtility.Wait(); 
		applySort.click();
		WebUtility.Wait();  
		
		
		Sort.click();
		sortOldToNew.click();
		WebUtility.Wait(); 
		applySort.click();
		WebUtility.Wait();  
		
		
	}
	
	public void TvShowFilter() throws InterruptedException {

	    WebUtility.Click(dashboard);
        WebUtility.Wait();
        WebUtility.Click(tvShowButton);
        WebUtility.Wait();
        
		//Filter with TvShows Content
        WebUtility.Click(searchContentType);
        WebUtility.Click(searchTVShow);
        WebUtility.Wait();         
		
		Filter.click();
		filterDraft.click();
		WebUtility.Wait();
		applyFilter.click();
		WebUtility.Wait();

		Filter.click();
		filterAllStatus.click();
		WebUtility.Wait();
		applyFilter.click();
		WebUtility.Wait();
		
		Filter.click();
		filterChanged.click();
		WebUtility.Wait();
		applyFilter.click();
		WebUtility.Wait();

		Filter.click();
		filterPublishedStatus.click();
		WebUtility.Wait();
		applyFilter.click();
		WebUtility.Wait();

		Filter.click();
		filterUnpublished.click();
		WebUtility.Wait();
		applyFilter.click();
		WebUtility.Wait();

		Filter.click();
		filterNeedWork.click();
		WebUtility.Wait();
		applyFilter.click();
		WebUtility.Wait();

		Filter.click();
		filterScheduled.click();
		WebUtility.Wait();
		applyFilter.click();
		WebUtility.Wait();

		Filter.click();
		filterSubmittedToReview.click();
		WebUtility.Wait();
		applyFilter.click();
		WebUtility.Wait();

		Filter.click();
		filterArchived.click();
		WebUtility.Wait();
		applyFilter.click();
		WebUtility.Wait();

        Filter.click();
		clearFilter.click();
		WebUtility.Wait(); 	
	
		Filter.click();
		WebUtility.Click(filterStartDate);
		datePicker.sendKeys(Keys.ARROW_LEFT);
		datePicker.sendKeys(Keys.ARROW_LEFT);
		datePicker.sendKeys(Keys.ARROW_LEFT);
		datePicker.sendKeys(Keys.ARROW_LEFT);
		datePicker.sendKeys(Keys.ESCAPE);	
	    
		WebUtility.Click(filterEndDate);
	    datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ESCAPE);
        WebUtility.Click(applyFilter);
		WebUtility.Wait();

		Filter.click();
		clearFilter.click();
		WebUtility.Wait();
		
		Filter.click();
		filterPrimaryGenre.click();
		WebUtility.Wait();
		dropDownList.get(0).click();
		filterClosetab.click();

		filterSecondaryGenre.click();
		WebUtility.Wait();
		dropDownList.get(0).click();
		filterClosetab.click();

		filterThematicGenre.click();
		WebUtility.Wait();
		dropDownList.get(0).click();
		filterClosetab.click();
		
		filterSettingGenre.click();
		WebUtility.Wait();
		dropDownList.get(0).click();
		filterClosetab.click();

		filterActor.sendKeys("Prabhas");
		WebUtility.Wait();
		dropDownList.get(0).click();
		filterClosetab.click();

		filterSubType.click();
		WebUtility.Wait();
		dropDownList.get(0).click();
		filterClosetab.click();
				
		filterContentCategory.click();
		WebUtility.Wait();
		dropDownList.get(0).click();
		filterClosetab.click();
		
		filterRCSCategory.click();
		WebUtility.Wait();
		dropDownList.get(0).click();
		filterClosetab.click();
		
		filterTheme.click();
		WebUtility.Wait();
		dropDownList.get(0).click();
		filterClosetab.click();
		
		filterTargetAudienceFilter.click();
		WebUtility.Wait();
		dropDownList.get(0).click();
		filterClosetab.click();
		
		filterLicenseGroupCountries.click();
		WebUtility.Wait();
		dropDownList.get(0).click();
		filterClosetab.click();
	
		filterAgeRating.click();
		WebUtility.Wait();
		dropDownList.get(0).click();
		filterClosetab.click();
		
		filterBusinessType.click();
		WebUtility.Wait();
		dropDownList.get(0).click();
		filterClosetab.click();

		//filterExternalId.sendKeys("Sample");
				
		filterContentRatingFilter.click();
		WebUtility.Wait();
		dropDownList.get(0).click();
		filterClosetab.click();
		
		filterContentOwner.click();
		WebUtility.Wait();
		dropDownList.get(0).click();	
		filterClosetab.click();
		
		filterAudioLanguage.click();
		WebUtility.Wait();
		dropDownList.get(0).click();
		filterClosetab.click();
		
		filterOriginalLanguage.click();
		WebUtility.Wait();
		dropDownList.get(0).click();
		filterClosetab.click();

		filterTags.sendKeys("actor");
		WebUtility.Wait();
		filterTags.sendKeys(Keys.ARROW_DOWN);
		filterTags.sendKeys(Keys.ENTER);
		WebUtility.Wait();
		filterClosetab.click();

		filterTranslationLanguage.click();
		WebUtility.Wait();
		dropDownList.get(0).click();
		filterClosetab.click();
		
		filterTranslationStatus.click();
		WebUtility.Wait();
		dropDownList.get(0).click();
		filterClosetab.click();
		
		filterMoodEmotion.click();
		WebUtility.Wait();
		dropDownList.get(0).click();
		filterClosetab.click();
		
		filterBroadcast.click();
		WebUtility.Wait();
		dropDownList.get(0).click();
		filterClosetab.click();
		
		applyFilter.click();
		WebUtility.Wait();
		WebUtility.Wait();
		
		Filter.click();
		clearFilter.click();
		WebUtility.Wait();
		
		// -------------------------Filter with Seasons Content---------------------------------------
        searchContentType.click();
        Thread.sleep(2000);
        searchSeasons.click();
        WebUtility.Wait();   
        	
        Filter.click();
		filterDraft.click();
		WebUtility.Wait();
		applyFilter.click();
		WebUtility.Wait();

		Filter.click();
		filterAllStatus.click();
		WebUtility.Wait();
		applyFilter.click();
		WebUtility.Wait();
		
		Filter.click();
		filterChanged.click();
		WebUtility.Wait();
		applyFilter.click();
		WebUtility.Wait();

		Filter.click();
		filterPublishedStatus.click();
		WebUtility.Wait();
		applyFilter.click();
		WebUtility.Wait();

		Filter.click();
		filterUnpublished.click();
		WebUtility.Wait();
		applyFilter.click();
		WebUtility.Wait();

		Filter.click();
		filterNeedWork.click();
		WebUtility.Wait();
		applyFilter.click();
		WebUtility.Wait();

		Filter.click();
		filterScheduled.click();
		WebUtility.Wait();
		applyFilter.click();
		WebUtility.Wait();

		Filter.click();
		filterSubmittedToReview.click();
		WebUtility.Wait();
		applyFilter.click();
		WebUtility.Wait();

		Filter.click();
		filterArchived.click();
		WebUtility.Wait();
		applyFilter.click();
		WebUtility.Wait();
		
		Filter.click();
		clearFilter.click();
		WebUtility.Wait();
		
		Filter.click();
		WebUtility.Click(filterStartDate);
		datePicker.sendKeys(Keys.ARROW_LEFT);
		datePicker.sendKeys(Keys.ARROW_LEFT);
		datePicker.sendKeys(Keys.ARROW_LEFT);
		datePicker.sendKeys(Keys.ARROW_LEFT);
		datePicker.sendKeys(Keys.ESCAPE);	
	    
		WebUtility.Click(filterEndDate);
	    datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ESCAPE);	
        WebUtility.Click(applyFilter);
		WebUtility.Wait();
		
		Filter.click();
		clearFilter.click();
		WebUtility.Wait();
		
		Filter.click();
		filterPrimaryGenre.click();
		WebUtility.Wait();
		dropDownList.get(0).click();
		filterClosetab.click();

		filterSecondaryGenre.click();
		WebUtility.Wait();
		dropDownList.get(0).click();
		filterClosetab.click();

		filterThematicGenre.click();
		WebUtility.Wait();
		dropDownList.get(0).click();
		filterClosetab.click();
		
		filterSettingGenre.click();
		WebUtility.Wait();
		dropDownList.get(0).click();
		filterClosetab.click();

		filterActor.sendKeys("Prabhas");
		WebUtility.Wait();
		dropDownList.get(0).click();
		filterClosetab.click();

		filterContentCategory.click();
		WebUtility.Wait();
		dropDownList.get(0).click();
		filterClosetab.click();
		
		filterRCSCategory.click();
		WebUtility.Wait();
		dropDownList.get(0).click();
		filterClosetab.click();
		
		filterTheme.click();
		WebUtility.Wait();
		dropDownList.get(0).click();
		filterClosetab.click();
		
		filterTargetAudienceFilter.click();
		WebUtility.Wait();
		dropDownList.get(0).click();
		filterClosetab.click();
		
		filterLicenseGroupCountries.click();
		WebUtility.Wait();
		dropDownList.get(0).click();
		filterClosetab.click();	
		
		filterAgeRating.click();
		WebUtility.Wait();
		dropDownList.get(0).click();
		filterClosetab.click();
		
		filterBusinessType.click();
		WebUtility.Wait();
		dropDownList.get(0).click();
		filterClosetab.click();

		//filterExternalId.sendKeys("Sample");
				
		filterContentRatingFilter.click();
		WebUtility.Wait();
		dropDownList.get(0).click();
		filterClosetab.click();
		
		filterContentOwner.click();
		WebUtility.Wait();
		dropDownList.get(0).click();	
		filterClosetab.click();
		
		filterAudioLanguage.click();
		WebUtility.Wait();
		dropDownList.get(0).click();
		filterClosetab.click();
		
		filterOriginalLanguage.click();
		WebUtility.Wait();
		dropDownList.get(0).click();
		filterClosetab.click();
	
		filterTags.sendKeys("actor");
		WebUtility.Wait();
		filterTags.sendKeys(Keys.ARROW_DOWN);
		filterTags.sendKeys(Keys.ENTER);
		WebUtility.Wait();
		filterClosetab.click();

		filterTranslationLanguage.click();
		WebUtility.Wait();
		dropDownList.get(0).click();
		filterClosetab.click();
		
		filterTranslationStatus.click();
		WebUtility.Wait();
		dropDownList.get(0).click();
		filterClosetab.click();
		
		filterMoodEmotion.click();
		WebUtility.Wait();
		dropDownList.get(0).click();
		filterClosetab.click();
		
		filterBroadcast.click();
		WebUtility.Wait();
		dropDownList.get(0).click();
		filterClosetab.click();
		
		applyFilter.click();
		WebUtility.Wait();
		WebUtility.Wait();
		
		Filter.click();
		clearFilter.click();
		WebUtility.Wait();
		
		//---------------------------------------Filter with Episodes Content--------------------------------------------------------
        searchContentType.click();
        Thread.sleep(2000);
        searchEpisodes.click();
        WebUtility.Wait();   
		
        Filter.click();
		filterDraft.click();
		WebUtility.Wait();
		applyFilter.click();
		WebUtility.Wait();

		Filter.click();
		filterAllStatus.click();
		WebUtility.Wait();
		applyFilter.click();
		WebUtility.Wait();
		
		Filter.click();
		filterChanged.click();
		WebUtility.Wait();
		applyFilter.click();
		WebUtility.Wait();

		Filter.click();
		filterPublishedStatus.click();
		WebUtility.Wait();
		applyFilter.click();
		WebUtility.Wait();

		Filter.click();
		filterUnpublished.click();
		WebUtility.Wait();
		applyFilter.click();
		WebUtility.Wait();

		Filter.click();
		filterNeedWork.click();
		WebUtility.Wait();
		applyFilter.click();
		WebUtility.Wait();

		Filter.click();
		filterScheduled.click();
		WebUtility.Wait();
		applyFilter.click();
		WebUtility.Wait();

		Filter.click();
		filterSubmittedToReview.click();
		WebUtility.Wait();
		applyFilter.click();
		WebUtility.Wait();

		Filter.click();
		filterArchived.click();
		WebUtility.Wait();
		applyFilter.click();
		WebUtility.Wait();
		
		Filter.click();
		clearFilter.click();
		WebUtility.Wait();
		
		Filter.click();
		WebUtility.Click(filterStartDate);
		datePicker.sendKeys(Keys.ARROW_LEFT);
		datePicker.sendKeys(Keys.ARROW_LEFT);
		datePicker.sendKeys(Keys.ARROW_LEFT);
		datePicker.sendKeys(Keys.ARROW_LEFT);
		datePicker.sendKeys(Keys.ESCAPE);	
	    
		WebUtility.Click(filterEndDate);
	    datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ESCAPE);	
        WebUtility.Click(applyFilter);
		WebUtility.Wait();

		Filter.click();
		clearFilter.click();
		WebUtility.Wait();
		
		Filter.click();
		filterPrimaryGenre.click();
		WebUtility.Wait();
		dropDownList.get(0).click();
		filterClosetab.click();

		filterSecondaryGenre.click();
		WebUtility.Wait();
		dropDownList.get(0).click();
		filterClosetab.click();

		filterThematicGenre.click();
		WebUtility.Wait();
		dropDownList.get(0).click();
		filterClosetab.click();
		
		filterSettingGenre.click();
		WebUtility.Wait();
		dropDownList.get(0).click();
		filterClosetab.click();

		filterActor.sendKeys("Prabhas");
		WebUtility.Wait();
		dropDownList.get(0).click();
		filterClosetab.click();
		
		filterSubType.click();
		WebUtility.Wait();
		dropDownList.get(0).click();
		filterClosetab.click();
				
		filterContentCategory.click();
		WebUtility.Wait();
		dropDownList.get(0).click();
		filterClosetab.click();
		
		filterRCSCategory.click();
		WebUtility.Wait();
		dropDownList.get(0).click();
		filterClosetab.click();
		
		filterTheme.click();
		WebUtility.Wait();
		dropDownList.get(0).click();
		filterClosetab.click();
		
		filterTargetAudienceFilter.click();
		WebUtility.Wait();
		dropDownList.get(0).click();
		filterClosetab.click();
		
		filterLicenseGroupCountries.click();
		WebUtility.Wait();
		dropDownList.get(0).click();
		filterClosetab.click();
	
		filterAgeRating.click();
		WebUtility.Wait();
		dropDownList.get(0).click();
		filterClosetab.click();
		
		filterBusinessType.click();
		WebUtility.Wait();
		dropDownList.get(0).click();
		filterClosetab.click();

		//filterExternalId.sendKeys("Sample");
				
		filterContentRatingFilter.click();
		WebUtility.Wait();
		dropDownList.get(0).click();
		filterClosetab.click();
		
		filterContentOwner.click();
		WebUtility.Wait();
		dropDownList.get(0).click();	
		filterClosetab.click();
		
		filterAudioLanguage.click();
		WebUtility.Wait();
		dropDownList.get(0).click();
		filterClosetab.click();
		
		filterOriginalLanguage.click();
		WebUtility.Wait();
		dropDownList.get(0).click();
		filterClosetab.click();
	
		filterTags.sendKeys("actor");
		WebUtility.Wait();
		filterTags.sendKeys(Keys.ARROW_DOWN);
		filterTags.sendKeys(Keys.ENTER);
		WebUtility.Wait();
		filterClosetab.click();

		filterTranslationLanguage.click();
		WebUtility.Wait();
		dropDownList.get(0).click();
		filterClosetab.click();
		
		filterTranslationStatus.click();
		WebUtility.Wait();
		dropDownList.get(0).click();
		filterClosetab.click();
		
		filterMoodEmotion.click();
		WebUtility.Wait();
		dropDownList.get(0).click();
		filterClosetab.click();
		
		applyFilter.click();
		WebUtility.Wait();
		
		Filter.click();
		clearFilter.click();
		WebUtility.Wait();
	
	}
	
	public void TvListingPage() throws InterruptedException,Exception{
		JavascriptExecutor js = (JavascriptExecutor) driver; 		
		Actions actionProvider = new Actions(driver);
        dashboard.click();
        WebUtility.Wait();
        tvShowButton.click();
        WebUtility.Wait();	
	
		WebUtility.Click(searchString);
		searchString.sendKeys(tvShowTitle);
		WebUtility.Wait();
		WebUtility.Wait();
		
		WebUtility.Click(listingExpiringLicense);
		WebUtility.Wait();
		Thread.sleep(3000);
		WebUtility.Click(tvShowBreadcrumb);
		WebUtility.Wait();
		
		WebUtility.Click(searchString);
		searchString.sendKeys(tvShowTitle);
		WebUtility.Wait();
		WebUtility.Wait();
		
		WebUtility.Click(listingLinkTVShows);
		WebUtility.Wait();
		Thread.sleep(3000);
		WebUtility.Click(tvShowBreadcrumb);
		WebUtility.Wait();
		
		WebUtility.Click(searchString);
		searchString.sendKeys(tvShowTitle);
		WebUtility.Wait();
		WebUtility.Wait();
		
		WebUtility.Click(listingPublishedHistory);
		WebUtility.Wait();
		Thread.sleep(3000);
		WebUtility.Click(listingClosePublishedHistory);
		WebUtility.Wait();
		
		WebUtility.Click(listingSeasonsArrow);
		js.executeScript("arguments[0].focus()", listingSeasonsArrow);
		WebUtility.Wait();
		Thread.sleep(3000);
		WebUtility.Click(listingSeasonsArrow);
		WebUtility.Wait();	
		
		try {
		WebUtility.Click(listingLicenseCountries);
		WebUtility.Wait();
		WebUtility.Click(listingCloseLicenseCountries);
		WebUtility.Wait();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
		listingDraftStatus.click();
		WebUtility.Wait();
		Thread.sleep(3000);
		
		listingAllStatus.click();
		WebUtility.Wait();
		Thread.sleep(3000);
		
		listingChangedStatus.click();
		WebUtility.Wait();
		Thread.sleep(3000);
		
		listingPublishedStatus.click();
		WebUtility.Wait();
		Thread.sleep(3000);
		
		listingUnpublishedStatus.click();
		WebUtility.Wait();
		Thread.sleep(3000);
		
		listingNeedWorkStatus.click();
		WebUtility.Wait();
		Thread.sleep(3000);
		
		listingScheduledStatus.click();
		WebUtility.Wait();
		Thread.sleep(3000);
		
		listingSubmittedToReviewStatus.click();
		WebUtility.Wait();
		Thread.sleep(3000);
		
		listingArchivedStatus.click();
		WebUtility.Wait();
		Thread.sleep(3000);
		
		listingPublishingQueueStatus.click();
		WebUtility.Wait();
		Thread.sleep(3000);
		
	}
	
	public void TvListingSearch() throws InterruptedException,Exception {	
/*
		// -------- Edit Flow For Testing Purposes ----------------------------

		WebUtility.Click(dashboard);
		WebUtility.Click(tvShowButton);

		searchString.click();
		searchString.sendKeys(tvShowTitle);
		WebUtility.Wait();
		WebUtility.Wait();
		WebUtility.element_to_be_clickable(editButton.get(0));
		editButton.get(0).click();


		externalID = js.executeScript("return arguments[0].childNodes[1].innerText", tvShowExternalID).toString();
	*/
		WebUtility.Click(dashboard);
		WebUtility.Click(tvShowButton);
		
		WebUtility.Click(tvShowSearch);
		WebUtility.Click(dropDownList.get(0));
		WebUtility.Wait();
		
		WebUtility.Click(searchString);
		searchString.sendKeys(tvShowTitle.toLowerCase());
		WebUtility.Wait();
		WebUtility.Wait();
				
		WebUtility.Click(searchString);
		searchString.sendKeys(Keys.CONTROL+"a");
		searchString.sendKeys(Keys.BACK_SPACE);
		searchString.sendKeys(externalID);
		WebUtility.Wait();
		WebUtility.Wait();
		
		try {
			TestUtil.captureScreenshot("TV Show Search", "TV Show");
		} catch (IOException e) {
			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}

		
	}
	
	public void TVAssertionResults() throws Exception{
		sa.assertAll();
	}
}
