package com.zee5.qa.pages;


import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.InvalidArgumentException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.asserts.SoftAssert;

import com.qa.utilities.WebUtility;
import com.zee5.qa.util.TestUtil;

public class MoviesPage extends com.zee5.qa.base.TestBase {
	
    SoftAssert sa = new SoftAssert();
	JavascriptExecutor js = (JavascriptExecutor) driver;
	public String originalUrl;
	public String ActualActorName;
	public String Castname;
	String externalID;
	//List<WebElement> dropDownList;
	LoginPage loginPage1= new LoginPage();
	Actions actionProvider = new Actions(driver);
	
//------------------------------Movie Titles ----------------------------------------
	String movieTitle ="Tanu Weds Manu";
	String movieQuickFilingTitle = "Golmaal";
	String movieSingleLandingTitle = "Star Wars Single Landing";
	
	
// get User name----------------------------------------- 
	
    @FindBy(xpath = "//li[@role='option' or @role='menuitem']")
    List < WebElement > dropDownList;
	
	@FindBy(xpath="//span[@class='u-name']/span")
	WebElement Username;
		
//Navigate to master-------------------(Adding genre)
		
	@FindBy(xpath="//span[contains(text(),'Master Management')]")
	WebElement mastermgmt;

	@FindBy(xpath="//div[contains(text(),'Manage Genres')]")
	WebElement managegenre;

	@FindBy(xpath="//div[contains(text(),'Add Genre')]")
	WebElement addgenre;

	@FindBy(xpath="//input[@name='title']")
	WebElement genre;

	@FindBy(xpath="//span[contains(text(),'Add')]")
	WebElement add;

	@FindBy(xpath="//button[contains(text(),'OK')]")
	WebElement ok;
		
	@FindBy(xpath="//input[@name='searchVal']")
	WebElement search;
	
//Adding language-----------------------------------------------
		
	@FindBy(xpath="//div[contains(text(),'Manage Language')]")
	WebElement managelng;

	@FindBy(xpath="//div[contains(@class,'btn-add-master')]")
	WebElement addlng;

	@FindBy(xpath="//input[@name='code']")
	WebElement lngcode;

	@FindBy(xpath="//input[@name='title']")
	WebElement lngname;

	@FindBy(xpath="//span[contains(text(),'Add')]")
	WebElement addl;

	@FindBy(xpath="//button[contains(text(),'OK')]")
	WebElement okl;

	@FindBy(xpath="//input[@name='searchVal']")
	WebElement searchl;

//Navigate to CastProfile Management page ------------------

	@FindBy(xpath="//div[contains(text(),'Cast Profile Management')]")
	WebElement castprofilemgmt;

	@FindBy(xpath="//div[contains(text(),'Create Profile')]")
	WebElement createprofilebtn;
	
	@FindBy(xpath="//input[@type='file']")
	WebElement uploadbtn;
	
	@FindBy(xpath="//input[@name='castName']")
	WebElement profilename;	
	
	@FindBy(xpath="//input[@name='castKnownAs']")
	WebElement castknown;

	@FindBy(xpath="//input[@id='castTag']")
	WebElement tagdrop;

	@FindBy(xpath="//input[@id='castType']")
	WebElement casttype;

	@FindBy(xpath="//label[contains(text(),'Cast Type')]")
	WebElement casttype1;
		
	@FindBy(xpath="//input[@name='castBirthday']")
	WebElement birthcalen;
		
	@FindBy(xpath="//input[@name='castBirthPlace']")
	WebElement birthplace;
		
	@FindBy(xpath="//input[@name='castCareer']")
	WebElement career;
	
	@FindBy(xpath="//input[@name='castAwards']")
	WebElement awards;
	
	@FindBy(xpath="//div[contains(@id,'castTrivia')]/div[2]/div[1]")
	WebElement trivia;
	
	@FindBy(xpath="//div[contains(@id,'castBackground')]/div[2]/div[1]")
	WebElement background;
	
	@FindBy(xpath="//div[contains(@id,'castProfileBio')]/div[2]/div[1]")
	WebElement profilebio;
		
	@FindBy(xpath="//input[contains(@id,'castProfileId')]")
	WebElement relatedto;
	
	@FindBy(xpath="//input[@id='relation']")
	WebElement relation;	
	
	@FindBy(xpath = "//div[contains(@class,'mark-fill-active')]")
	WebElement markasdone;
	
// Image section------------------------------------------------
	
	@FindBy(xpath="//button[contains(@class,'Images')]")
	WebElement Imagesection;
	
	@FindBy(xpath="//div[@class='img-upload-btn']/input")
	WebElement uploadImagebtn;
	
	@FindBy(xpath = "//div[contains(@class,'mark-fill-active')]")
	WebElement markasdoneimg;
	
//Checklist page----------------------------------------
	
	@FindBy(xpath="//button[contains(@class,'Checklist')]")
	WebElement chklistradioBtn;
	
	@FindBy(xpath="//span[contains(text(),'Publish')]")
	WebElement publishBtn;
	
	@FindBy(xpath="//span[contains(text(),'Yes')]")
	WebElement publishYesbtn;
	
	@FindBy(xpath="//span[contains(text(),'Ok')]")
	WebElement okbtn;
	
	@FindBy(xpath="//body//div[@id='app']//div//div[1]//div[1]//div[2]//div[1]//strong[1]")
	WebElement CastCreated;
	
	@FindBy(xpath="//*[@id=\"searchVal\"]")
	WebElement Castsearchbar;
	
//Cast Profile Translations-------------------------------------
	
    @FindBy(xpath = "//input[@id='auto-Profile Name']")
    WebElement profileName;

    @FindBy(xpath = "//input[@id='auto-Also known as']")
    WebElement knownAs;

    @FindBy(xpath = "//input[@id='auto-Birthplace']")
    WebElement birthPlace;

    @FindBy(xpath = "//div[contains(@class,'ql-editor')]")
    WebElement profileBio;

    @FindBy(xpath = "//input[@id='auto-Background']")
    WebElement profileBackground;

    @FindBy(xpath = "//input[@id='auto-Career']")
    WebElement profileCareer;

    @FindBy(xpath = "//input[@id='auto-Awards & Notification']")
    WebElement profileAwards;

    @FindBy(xpath = "//input[@id='auto-Trivia']")
    WebElement profileTrivia;
    
    @FindBy(xpath = "//a[contains(text(),'Cast Profile Management')]")
    WebElement castProfileBreadCrumb;
    

// Navigate to Movies page --------------------------------

	//@FindBy(xpath="//a[contains(text(),'Dashboard')]")
    @FindBy(xpath = "//button[contains(@class,'auto-tab-Dashboard')]")
	WebElement dashboard;
	
	@FindBy(xpath="//div[contains(text(),'Movie')]")
	WebElement movies;
	
	@FindBy(xpath="//div[contains(text(),'Create Movie')]")
	WebElement createmoviebtn;

// Title Summary ----------------------------------------------

    //@FindBy(xpath = "//button[contains(@class,'auto-leftTab-ContentProperties')]")
    //WebElement contentProperties;
    
	@FindBy(xpath="//button[contains(.,'Content Properties')]")
	WebElement contentProperties;	
	
	@FindBy(xpath="//input[@name='note']")
	WebElement note;

	@FindBy(xpath="//input[@name='title']")
	WebElement title;

	@FindBy(xpath="//div[contains(@id,'auto-shortDescription')]//child::div[contains(@class,'ql-editor')]")
	WebElement shortdesc;
	
	@FindBy(xpath="//div[contains(@class,'auto-WebDescription')]//child::div[contains(@class,'ql-editor')]")
	WebElement WebDesc;

	@FindBy(xpath="//div[contains(@class,'auto-AppDescription')]//child::div[contains(@class,'ql-editor')]")
	WebElement AppDesc;

	@FindBy(xpath="//div[contains(@class,'auto-TVDescription')]//child::div[contains(@class,'ql-editor')]")
	WebElement TvDesc;	

	@FindBy(xpath="//input[@id='subtype']")
	WebElement subtype;

	@FindBy(xpath="//input[@id='category']")
	WebElement ContentCategory;

	@FindBy(xpath="//input[@id='primaryGenre']")
	WebElement PrimaryGenre;
	
	@FindBy(xpath="//input[@id='secondaryGenre']")
	WebElement SecondaryGen;

	@FindBy(xpath="//input[@id='thematicGenre']")
	WebElement ThematicGen;

	@FindBy(xpath="//input[@id='settingGenre']")
	WebElement SettingGen;

	@FindBy(xpath="//input[@id='rcsCategory']")
	WebElement RcsCategory;

	@FindBy(xpath="//input[@name='creativeTitle']")
	WebElement CreativeTitle;

	@FindBy(xpath="//input[@name='alternatetitle']")
	WebElement AlternateTitle;

	@FindBy(xpath="//input[@name='pageTitle']")
	WebElement PageTitle;

	@FindBy(xpath="//textarea[@name='pageDescription']")
	WebElement PageDesc;

	@FindBy(xpath="//input[@name='titleForSocialShare']")
	WebElement SocialTitle;

	@FindBy(xpath="//textarea[@name='descriptionForSocialShare']")
	WebElement SocialDesc;

	@FindBy(xpath="//input[@name='dateZee5Published']")
//	@FindBy(xpath="//div[@class='MuiFormControl-root MuiTextField-root zee-input-field auto-ZEE5ReleaseDate-0-19']//button[@type='button']")
	WebElement ZeeReleaseDate;
	
	@FindBy(xpath="//input[@name='telecastDate']")
//	@FindBy(xpath="//div[@class='MuiFormControl-root MuiTextField-root zee-input-field auto-OriginalTelecastDate-0-20']//button[@type='button']")
	WebElement telecastDate;
	
	
    @FindBy(xpath = "//div[@class='MuiPaper-root MuiPopover-paper MuiPaper-elevation8 MuiPaper-rounded']")
    WebElement datePicker; 

	@FindBy(xpath="//input[@name='duration']")
	WebElement VideoDuration;

	@FindBy(xpath="//input[@name='isMultiAudio']")
	WebElement MultiAudio;

	@FindBy(xpath="//input[@id='audioLanguages']")
	WebElement AudioLanguage;
	
	@FindBy(xpath="//input[@id='contentLanguage']")
	WebElement ContentLanguage;

	@FindBy(xpath="//input[@id='primaryLanguage']")
	WebElement PrimaryLanguage;

	@FindBy(xpath="//input[@id='dubbedLanguageTitle']")
	WebElement DubbedLanguage;

	@FindBy(xpath="//input[@id='originalLanguage']")
	WebElement OriginalLanguage;
	
	@FindBy(xpath="//input[@id='subtitleLanguages']")
	WebElement SubtitleLanguage;
	
	@FindBy(xpath="//input[@id='contentVersion']")
	WebElement ContentVersion;
	
	@FindBy(xpath="//input[@id='theme']")
	WebElement MovieTheme;

	@FindBy(xpath="//input[@name='contractId']")
	WebElement ContractId;

	@FindBy(xpath="//input[@id='specialCategory']")
	WebElement SpecialCategory;

	@FindBy(xpath="//input[@id='specialCategoryCountry']")
	WebElement SpecialCountry;

//	@FindBy(xpath="//input[@name='specialCategoryFrom']")
	@FindBy(xpath="//div[@class='MuiFormControl-root MuiTextField-root zee-input-field auto-SpecialCategory-FromTimeline-0-2']//button[@type='button']")
	WebElement FromTimeline;

//	@FindBy(xpath="//*[@id='app']/div/div[3]/div[3]/div/div[3]")
//	WebElement ClickOut;

//	@FindBy(xpath="//input[@name='specialCategoryTo']")
	@FindBy(xpath="//div[@class='MuiFormControl-root MuiTextField-root zee-input-field auto-SpecialCategory-ToTimeline-0-3']//button[@type='button']")
	WebElement ToTimeline;
	
	@FindBy(name="specialCategoryFrom")
	WebElement fromDateCalendar; 
	
	@FindBy(name="specialCategoryTo")
	WebElement toDateCalendar;

	@FindBy(xpath="//div[@class= 'next-step-btn']")
	WebElement Next;
	
	@FindBy(xpath="//div[contains(@class,'mark-done')]")
	WebElement ContentPropertiesMarkAsDone;

    @FindBy(xpath = "//input[@name='specialCategoryFrom']") 
    WebElement specialCategoryFrom;

    @FindBy(xpath = "//input[@name='specialCategoryTo']")
     WebElement specialCategoryTo;
    
//  Classification-----------------------------------------------
	
	@FindBy(xpath="//input[@id='rating']")
	WebElement ContentRating;
	
	@FindBy(xpath="//input[@id='contentOwner']")
	WebElement ContentOwner;

	@FindBy(xpath="//input[@id='certification']")
	WebElement Certification;
	
	@FindBy(xpath="//input[@id='originCountry']")
	WebElement OriginalCountry;

	@FindBy(xpath="//input[@name='upcomingPage']")
	WebElement UpcomingText;

	@FindBy(xpath="//input[@id='ageRating']")
	WebElement ContentDescription;
	
	@FindBy(xpath="//input[@id='emotions']")
	WebElement Moods;

	@FindBy(xpath="//input[@id='contentGrade']")
	WebElement ContentGrade;

	@FindBy(xpath="//input[@id='era']")
	WebElement Era;

	@FindBy(xpath="//input[@id='targetAge']")
	WebElement TargetAge;

	@FindBy(xpath="//input[@id='targetAudiences']")
	WebElement TargetAudience;

	@FindBy(xpath="//input[@id='auto-tags-0-11']")
	WebElement Tags;

	@FindBy(xpath="//button[contains(text(),'OK')]")
	WebElement Error;

	@FindBy(xpath="//input[@name='digitalKeywords']")
	WebElement Digital;

	@FindBy(xpath="//input[@name='adaptation']")
	WebElement Adaption;

	@FindBy(xpath="//input[@name='events']")	
	//@FindBy(xpath="//input[@id='auto-undefined-0-14']")	
	WebElement Events;

	@FindBy(xpath="//input[@id='productionCompany']")
	WebElement ProductionCompany;

	@FindBy(xpath="//input[@name='popularityScore']")
	WebElement PopularityScore;

	@FindBy(xpath="//input[@name='trivia']")
	WebElement Trivia;

	@FindBy(xpath="//input[@name='officialSite']")
	//@FindBy(xpath="//input[@id='auto-undefined-0-18']")
	WebElement OfficialSite;

	
	@FindBy(xpath="//input[contains(@id,'awardRecipient')]")
	WebElement AwardRecipient;

	@FindBy(xpath="//input[@id='awardsCategory']")
	WebElement AwardCategory;

	@FindBy(xpath="//input[@name='awardsandrecognition']")
	WebElement AwardRecognition;
	
// Play Attributes ---------------------------------------------------

	@FindBy(xpath="//input[@name='skipAvailable']")
	WebElement Skip;

	@FindBy(xpath="//input[@name='introStartTime']")
	WebElement StartTime;

	@FindBy(xpath="//input[@name='introEndTime']")
	WebElement EndTime;

	@FindBy(xpath="//input[@name='recapStartTime']")
	WebElement RecapStart;

	@FindBy(xpath="//input[@name='recapEndTime']")
	WebElement RecapEnd;

	@FindBy(xpath="//input[@name='endCreditsStartTime']")
	WebElement EndCredit;

	@FindBy(xpath="//input[@name='adMarker']")
	WebElement AdMarker;

	@FindBy(xpath="//input[@name='skipSongStartTime']")
	WebElement SkipSong;

	@FindBy(xpath="//input[@name='skipSongEndTime']")
	WebElement SkipEnd;

	@FindBy(xpath="//div[@class='next-step-btn']")
	WebElement NextControl;

	@FindBy(xpath="//div[@class= 'next-step-btn']")
	WebElement NextBtn;

	@FindBy(xpath = "//div[contains(@class,'mark-fill-active')]")
	WebElement MarkDone;

// Cast & Crew -------------------------------------------------------

	@FindBy(xpath="//button[contains(.,'Cast & Crew')]")
	WebElement CastCrew;
	
	@FindBy(xpath="//input[@id='actor']")
	WebElement Actor1;
	
	@FindBy(xpath="//input[@name='character']")
	WebElement character;

	@FindBy(xpath="//span[contains(text(),'+')]")
	WebElement addActor;
	
	@FindBy(xpath="//input[@id='auto-actor-0-1']")
	WebElement Actor2;
	
	@FindBy(xpath="//input[@id='auto-character-0-1']")
	WebElement Char2;
	
	@FindBy(xpath="//span[contains(text(),'-')]")
	WebElement removeActor;
	
	/*
	@FindBy(xpath="//input[@id='auto-42c1e2b7-0578-4e5f-b805-c2601fddee28-0-0']")
	WebElement Performer;

	@FindBy(xpath="//input[@id='auto-72dab561-6c03-4634-ab6f-f5268f716211-0-1']")
	WebElement Host;

	@FindBy(xpath="//input[@id='auto-d7d7bdab-3a6e-47d6-8be1-50d99535a98e-0-2']")
	WebElement Singer;

	@FindBy(xpath="//input[@id='auto-88a65fb3-ee63-42ad-97ba-53e340120800-0-3']")
	WebElement Lyricist;

	@FindBy(xpath="//input[@id='auto-37b661a0-c72a-4114-ae68-01e42dcbf149-0-4']")
	WebElement Director;

	@FindBy(xpath="//input[@id='auto-e5293be6-f107-4e5f-83ce-4de5176a0638-0-5']")
	WebElement Dop;

	@FindBy(xpath="//input[@id='auto-d1958a0d-9593-4b42-9aa4-45f21e4e008c-0-6']")
	WebElement Producer;

	@FindBy(xpath="//input[@id='auto-67d360dc-91a4-4076-97a8-6bcad057fbef-0-7']")
	WebElement ExecutivePrd;

	@FindBy(xpath="//input[@id='auto-ff299957-95e9-4893-8ddc-7773a19ca8f3-0-8']")
	WebElement MusicDirector;

	@FindBy(xpath="//input[@id='auto-d6d9417c-487b-4721-b835-1c6bc8a26419-0-9']")
	WebElement Choreographer;

	@FindBy(xpath="//input[@id='auto-86bf9076-5e85-439b-8c56-72466547f4f6-0-10']")
	WebElement TitleTheme;

	@FindBy(xpath="//input[@id='auto-01f0b5bc-b4e0-47a9-a771-6d1441327051-0-11']")
	WebElement BackgroundScore;

	@FindBy(xpath="//input[@id='auto-36055682-92b5-4a06-8d1f-7bee5091a2e9-0-12']")
	WebElement StoryWriter;

	@FindBy(xpath="//input[@id='auto-4b36aec2-b1d9-41a1-932d-afb4167e31ed-0-13']")
	WebElement ScreenPlay;

	@FindBy(xpath="//input[@id='auto-f61f9a2c-06fe-46ba-873e-be8ec6250eee-0-14']")
	WebElement Dialogue;

	@FindBy(xpath="//input[@id='auto-7a54e245-1ed3-4cf3-8239-836e3e31c496-0-15']")
	WebElement FilmEditing;

	@FindBy(xpath="//input[@id='auto-72a18e11-afc7-4fde-a610-690b447e64b7-0-16']")
	WebElement Casting;

	@FindBy(xpath="//input[@id='auto-cbe6285a-c4c9-4326-843e-19f4edcc6c80-0-17']")
	WebElement ProdDesign;

	@FindBy(xpath="//input[@id='auto-ce6c59af-85bb-41b2-b39c-4fdb95571b14-0-18']")
	WebElement Art;

	@FindBy(xpath="//input[@id='auto-4cf637af-cd57-4f2d-9743-796fda296f64-0-19']")
	WebElement SetDecoration;

	@FindBy(xpath="//input[@id='auto-99f58b87-fd54-48b4-8514-33c443cd0290-0-20']")
	WebElement Costume;

	@FindBy(xpath="//input[@id='auto-8184d837-d622-4ac9-82bb-1b1a8fb7143f-0-21']")
	WebElement ProductionCo;

	@FindBy(xpath="//input[@id='auto-afecd044-82ec-463f-a681-da564d2d9ab0-0-22']")
	WebElement Presenter;

	@FindBy(xpath="//input[@id='auto-79c5dd74-6912-48f8-b0da-60c24b5fa917-0-23']")
	WebElement Guest;

	@FindBy(xpath="//input[@id='auto-d9e1fb0d-b368-46df-a53e-49c1473ce8ff-0-24']")
	WebElement Participant;

	@FindBy(xpath="//input[@id='auto-a7420fdc-43b8-49dc-98ea-2db61fdfa8db-0-25']")
	WebElement Judges;

	@FindBy(xpath="//input[@id='auto-ba9e3b38-5269-43dc-ab50-701380281461-0-26']")
	WebElement Narrator;

	@FindBy(xpath="//input[@id='auto-cdad2189-ffbe-41c2-914a-cb92ffb457a5-0-27']")
	WebElement Sponsor;

	@FindBy(xpath="//input[@id='auto-305cb138-0ef4-4068-9a13-ab96eb7be36c-0-28']")
	WebElement Graphics;

	@FindBy(xpath="//input[@id='auto-7a5893e2-b4ea-4c55-99af-f50cd7a949e2-0-29']")
	WebElement Vocalist;
*/
	@FindBy(xpath="//input[@id='42c1e2b7-0578-4e5f-b805-c2601fddee28']")
	WebElement Performer;

	@FindBy(xpath="//input[@id='72dab561-6c03-4634-ab6f-f5268f716211']")
	WebElement Host;

	@FindBy(xpath="//input[@id='d7d7bdab-3a6e-47d6-8be1-50d99535a98e']")
	WebElement Singer;

	@FindBy(xpath="//input[@id='88a65fb3-ee63-42ad-97ba-53e340120800']")
	WebElement Lyricist;

	@FindBy(xpath="//input[@id='37b661a0-c72a-4114-ae68-01e42dcbf149']")
	WebElement Director;

	@FindBy(xpath="//input[@id='e5293be6-f107-4e5f-83ce-4de5176a0638']")
	WebElement Dop;

	@FindBy(xpath="//input[@id='d1958a0d-9593-4b42-9aa4-45f21e4e008c']")
	WebElement Producer;

	@FindBy(xpath="//input[@id='67d360dc-91a4-4076-97a8-6bcad057fbef']")
	WebElement ExecutivePrd;

	@FindBy(xpath="//input[@id='ff299957-95e9-4893-8ddc-7773a19ca8f3']")
	WebElement MusicDirector;

	@FindBy(xpath="//input[@id='d6d9417c-487b-4721-b835-1c6bc8a26419']")
	WebElement Choreographer;

	@FindBy(xpath="//input[@id='86bf9076-5e85-439b-8c56-72466547f4f6']")
	WebElement TitleTheme;

	@FindBy(xpath="//input[@id='01f0b5bc-b4e0-47a9-a771-6d1441327051']")
	WebElement BackgroundScore;

	@FindBy(xpath="//input[@id='36055682-92b5-4a06-8d1f-7bee5091a2e9']")
	WebElement StoryWriter;

	@FindBy(xpath="//input[@id='4b36aec2-b1d9-41a1-932d-afb4167e31ed']")
	WebElement ScreenPlay;

	@FindBy(xpath="//input[@id='f61f9a2c-06fe-46ba-873e-be8ec6250eee']")
	WebElement Dialogue;

	@FindBy(xpath="//input[@id='7a54e245-1ed3-4cf3-8239-836e3e31c496']")
	WebElement FilmEditing;

	@FindBy(xpath="//input[@id='72a18e11-afc7-4fde-a610-690b447e64b7']")
	WebElement Casting;

	@FindBy(xpath="//input[@id='cbe6285a-c4c9-4326-843e-19f4edcc6c80']")
	WebElement ProdDesign;

	@FindBy(xpath="//input[@id='ce6c59af-85bb-41b2-b39c-4fdb95571b14']")
	WebElement Art;

	@FindBy(xpath="//input[@id='4cf637af-cd57-4f2d-9743-796fda296f64']")
	WebElement SetDecoration;

	@FindBy(xpath="//input[@id='99f58b87-fd54-48b4-8514-33c443cd0290']")
	WebElement Costume;

	@FindBy(xpath="//input[@id='8184d837-d622-4ac9-82bb-1b1a8fb7143f']")
	WebElement ProductionCo;

	@FindBy(xpath="//input[@id='afecd044-82ec-463f-a681-da564d2d9ab0']")
	WebElement Presenter;

	@FindBy(xpath="//input[@id='79c5dd74-6912-48f8-b0da-60c24b5fa917']")
	WebElement Guest;

	@FindBy(xpath="//input[@id='d9e1fb0d-b368-46df-a53e-49c1473ce8ff']")
	WebElement Participant;

	@FindBy(xpath="//input[@id='a7420fdc-43b8-49dc-98ea-2db61fdfa8db']")
	WebElement Judges;

	@FindBy(xpath="//input[@id='ba9e3b38-5269-43dc-ab50-701380281461']")
	WebElement Narrator;

	@FindBy(xpath="//input[@name='cdad2189-ffbe-41c2-914a-cb92ffb457a5']")
	WebElement Sponsor;

	@FindBy(xpath="//input[@id='305cb138-0ef4-4068-9a13-ab96eb7be36c']")
	WebElement Graphics;

	@FindBy(xpath="//input[@id='7a5893e2-b4ea-4c55-99af-f50cd7a949e2']")
	WebElement Vocalist;
	
	@FindBy(xpath = "//div[contains(@class,'mark-fill-active')]")
	WebElement CastMarkAsDone;

// Videos  -----------------------------------------------------------

	@FindBy(xpath="//span[contains(text(),'Videos')]")
	WebElement Videos;

	@FindBy(xpath="//input[@name='audioLanguage']")
	WebElement AudioTrack;

	@FindBy(xpath="//input[@name='subtitleLanguages']")
	WebElement VideoSubType;

	@FindBy(xpath="//input[@name='dashRootFolderName']")
	WebElement DashRoot;

	@FindBy(xpath="//input[@name='dashManifestName']")
	WebElement DashManifest;

	@FindBy(xpath="//input[@name='hlsRootFolderName']")
	WebElement HlsRoot;

	@FindBy(xpath="//input[@name='hlsManifestName']")
	WebElement HlsManifest;

	@FindBy(xpath="//input[@name='drmKeyId']")
	WebElement DrmID;

	@FindBy(xpath="//input[@name='size']")
	WebElement Size;

	@FindBy(xpath="//label[contains(@class,'Protected')]")
	WebElement ProtectedCheckbox;

	@FindBy(xpath="//input[@id='dashSuffixesId']")
	WebElement Dash;

	@FindBy(xpath="//input[@id='hlsSuffixesId']")
	WebElement Hls;

	@FindBy(xpath="//input[@name='mediathekFileUid']")
	WebElement Media;

	@FindBy(xpath = "//div[contains(@class,'mark-fill-active')]")
	WebElement VideosMarkDone;
	
    @FindBy(xpath = " //a[contains(text(),'Import Details')]")
    WebElement importDetails;  

// License Module -------------------------------------------------------

	@FindBy(xpath="//span[contains(text(),'License Module')]")
	WebElement License;

	@FindBy(xpath="//div[contains(text(),'Create License')]")
	WebElement CreateLicense;

// Manual --------------------------------------------------
	
	@FindBy(xpath="//input[@name='setName']")
	WebElement licenseSetName;
	
	@FindBy(xpath="//input[@name='fromDate']")
//	@FindBy(xpath="//div[@class='MuiFormControl-root MuiTextField-root zee-input-field auto-LicenseFromDate-0-1']//button[@type='button']")
	WebElement licenseFromDate;
	
	@FindBy(xpath="//input[@name='toDate']")
//	@FindBy(xpath="//div[@class='MuiFormControl-root MuiTextField-root zee-input-field auto-LicenseToDate-0-2']//button[@type='button']")
	WebElement licenseToDate;
	
	@FindBy(xpath="//div[@class='MuiFormControl-root MuiTextField-root zee-input-field auto-LicenseFromDate-0-2']//button[@type='button']")
	WebElement licenseTemplateFromDate;
	
	@FindBy(xpath="//div[@class='MuiFormControl-root MuiTextField-root zee-input-field auto-LicenseToDate-0-3']//button[@type='button']")
	WebElement licenseTemplateToDate;
	
	@FindBy(xpath="//input[contains(@id,'auto-country')]")        
	WebElement Country;

	@FindBy(xpath="//input[contains(@id,'auto-businessType')]")
	WebElement BusinessType;
	
	@FindBy(xpath = "//input[contains(@id,'auto-platform')]")   
    WebElement licensePlatform;
	
	@FindBy(xpath = "//input[contains(@id,'auto-tvodTier')]")
	WebElement licenseTVOD;
	
	@FindBy(xpath="//input[contains(@id,'auto-platform')]")
	WebElement platform;
	
	@FindBy(xpath="//input[contains(@id,'auto-tvodTier')]")
	WebElement Tvod;
	
	@FindBy(xpath="//span[contains(text(),'SAVE')]")
	WebElement SaveLicense;

	@FindBy(xpath="//div[contains(@ class,'mark-done')]")
	WebElement LicenseMarkDone;
	
    @FindBy(xpath = "//div[contains(@class,'mark-fill-active')]")
    WebElement doneButtonActive; // rgba(255, 255, 255, 1)

// Use Template -----------------------------------------------------

	@FindBy(xpath="//input[@name='manual' and @value='2']")
	WebElement UseTemplate;

	@FindBy(xpath="//input[@type='text']")
	WebElement TemplateDropdown;
	
	@FindBy(xpath="//div[@class=\"default-edit-btn\"]")
	WebElement editLicense;
	
    @FindBy(css = "span.MuiIconButton-label > svg.MuiSvgIcon-root.MuiSvgIcon-fontSizeSmall")
    WebElement clearInput;
	
	@FindBy(xpath="//input[contains(@id,'country')]//parent::div/div/button[2]/span")
	WebElement UseTemplateCountryEdit;
	
	@FindBy(xpath="//input[contains(@id,'country')]//parent::div/div/button[1]")
	WebElement CLearCountry;
	
	@FindBy(xpath="//span[contains(text(),'SAVE')]")
	WebElement SaveUseTemplate;
	
	@FindBy(xpath = "//button[contains(@class,'short-btn')]")
	WebElement expiredLicense;

	@FindBy(className = "item")
	WebElement expiredLicenseBack;

	@FindBy(css = "button.MuiButtonBase-root.MuiIconButton-root.MuiAutocomplete-popupIndicator.MuiAutocomplete-popupIndicatorOpen > span.MuiIconButton-label > svg.MuiSvgIcon-root")
	WebElement filterCloseDropdown;
	
	@FindBy(xpath = "//button[contains(@class,'filter-btn')]")
	WebElement licenseFilters;
	
	@FindBy(xpath = "//div[@class='MuiFormControl-root MuiTextField-root zee-input-field auto-LicenseFromDate-0-0']//button[@type='button']")
	WebElement licenseFilterFromDate;
	
	@FindBy(xpath = "//div[@class='MuiFormControl-root MuiTextField-root zee-input-field auto-LicenseToDate-0-1']//button[@type='button']")
	WebElement licenseFilterToDate;

	@FindBy(id = "auto-businessType-0-2")
	WebElement licenseFilterBusinessType;

	@FindBy(id = "auto-billingType-0-3")
	WebElement licenseFilterBillingType;

	@FindBy(id = "auto-platform-0-4")
	WebElement licenseFilterPlatform;

	@FindBy(xpath = "//input[@name='status' and @value = '1']")
	WebElement licenseFilterActiveStatus;
	    
	@FindBy(xpath = "//input[@name='status' and @value = '0']")
	WebElement licenseFilterInActiveStatus;
	
	@FindBy(xpath = "//div[contains(@class,'delete')]")
	List<WebElement> licenseTemplateDeleteSets;

    @FindBy(xpath = "//input[contains(@id,'contentAgeRatingId')]")
    WebElement licenseContentAgeRatingId;
// Images ------------------------------------------------------

	@FindBy(xpath="//span[contains(text(),'Images')]")
	WebElement Images;
	
	@FindBy(xpath="//input[@name='cover']")
	WebElement Cover;
	
	@FindBy(xpath="//input[@name='appcover']")
	WebElement AppCover;
	
	@FindBy(xpath="//input[@name='list']")
	WebElement List;
	
	@FindBy(xpath="//input[@name='square']")
	WebElement Square;
	
	@FindBy(xpath="//input[@name='tvcover']")
	WebElement TVCover;

	@FindBy(xpath="//input[@name='portrait']")
	WebElement Portrait;
	
	@FindBy(xpath="//input[@name='listclean']")
	WebElement Listclean;
	
	@FindBy(xpath="//input[@name='portraitclean']")
	WebElement Portraitclean;
	
	@FindBy(xpath="//input[@name='telcosquare']")
	WebElement Telcosquare;
	
	@FindBy(xpath="//input[@name='passport']")
	WebElement Passport;

	@FindBy(xpath="//div[contains(@class,'mark-done')]")
	WebElement ImageMarkAsDone;

	@FindBy(xpath = "//button[contains(@class,\"auto-create-newSet\")]")
	WebElement imagesCreateNewSet;

	@FindBy(name = "setName")
	WebElement newSetName;

	@FindBy(id = "auto-GroupCountry-0-1")
	WebElement newSetCountry;

	@FindBy(id = "auto-Platform-0-2")
	WebElement newSetPlatform;

	@FindBy(id = "auto-gender-0-0")
	WebElement newSetGender;

	@FindBy(id = "auto-genre-0-1")
	WebElement newSetGenre;

	@FindBy(id = "auto-ageGroup-0-2")
	WebElement newSetAgeGroup;

	@FindBy(id = "auto-language-0-3")
	WebElement newSetLanguage;

	@FindBy(name = "others")
	WebElement newSetOthers;

	// @FindBy(xpath = "//div[contains(@class,'auto-question-block')]")
	@FindBy(xpath = "//div[contains(@class,'imageset-acc')]")
	List<WebElement> imageSets;

	//@FindBy(xpath = "(//button[contains(@class,'Active')])[1]")
	@FindBy(xpath = "//div[@class='status-dropdown val']//div//button[@type='button']")  
	WebElement imageSetActiveButton;

	@FindBy(xpath = "//span[@class='remove']")
	List<WebElement> removeSet;

	@FindBy(xpath = "/html/body/div[4]/div[3]/div/div[3]/button[1]")	
	WebElement deactivateSetYes;
	
// SEO ------------------------------------------------------

	@FindBy(xpath="//span[contains(text(),'SEO Details')]")
	WebElement Seo;

	@FindBy(xpath="//input[@name='titleTag']")
	WebElement SeoTitle;
	
	@FindBy(xpath="//textarea[@name='metaDescription']")
	WebElement MetaDesc;

	@FindBy(xpath="//textarea[@name='metaSynopsis']")
	WebElement MetaSynp;

	@FindBy(xpath="//input[@id='redirectionType']")
	WebElement Redirection;

	@FindBy(xpath="//textarea[@name='redirectionLink']")
	WebElement RedirectionLink;

	@FindBy(xpath="//input[@name='noIndexNoFollow']")
	WebElement NoIndex;

	@FindBy(xpath="//input[@name='h1Heading']")
	WebElement H1;

	@FindBy(xpath="//input[@name='h2Heading']")
	WebElement H2;

	@FindBy(xpath="//input[@name='h3Heading']")
	WebElement H3;

	@FindBy(xpath="//input[@name='h4Heading']")
	WebElement H4;

	@FindBy(xpath="//input[@name='h5Heading']")
	WebElement H5;

	@FindBy(xpath="//input[@name='h6Heading']")
	WebElement H6;

	@FindBy(xpath="//input[@name='robotsMetaNoIndex']")
	WebElement RobotMeta;

	@FindBy(xpath="//input[contains(@id,'robotsMetaNoIndex-0-14')]")
	WebElement RobotNoIndex;

	@FindBy(xpath="//input[@name='robotsMetaImageIndex']")
	WebElement MetaImage;

	@FindBy(xpath="//input[@name='robotsMetaImageNoIndex']")
	WebElement MetaImageNoIndex;

	@FindBy(xpath="//input[@name='breadcrumbTitle']")
	WebElement BreadCrumb;

	@FindBy(xpath="//input[@name='internalLinkBuilding']")
	WebElement InternalLink;

	@FindBy(xpath="//input[@name='videoObjectSchema']")
	WebElement Videoseo;

	@FindBy(xpath="//input[@name='websiteSchema']")
	WebElement Website;

	@FindBy(xpath="//input[@name='breadcrumListSchema']")
	WebElement BreadcrumList;

	@FindBy(xpath="//input[@name='imageObjectSchema']")
	WebElement Image;

	@FindBy(xpath="//input[@name='organizationSchema']")
	WebElement Organization;

	@FindBy(xpath="//input[@name='imageGallerySchema']")
	WebElement ImageGallery;

	@FindBy(xpath="//input[@name='linkToStories']")
	WebElement Link;

	@FindBy(xpath = "//div[contains(@class,'mark-fill-active')]")
	WebElement MarkAsDone;

// Map Content -----------------------------------------------

	@FindBy(xpath="//span[contains(text(),'Map Content')]")
	WebElement MapContent;

	@FindBy(xpath="//div[contains(@class,'add-content')]")
	WebElement AddContent;

	@FindBy(xpath="//input[@type='checkbox']")
	WebElement Checkbox;

	@FindBy(xpath="//div[contains(text(),'Assign Content')]")
	WebElement AssignContent;

	@FindBy(xpath = "//div[contains(@class,'mark-fill-active')]")
	WebElement MapMarkAsDone;

//Checklist   ------------------------------------------------------

	@FindBy(xpath="//span[contains(text(),'Checklist')]")
	WebElement Checklist;

	@FindBy(xpath="//span[contains(text(),'Group / Country')]")
	WebElement Dropdown;

	@FindBy(xpath="//span[contains(text(),'Publish Content')]")
	WebElement Publish;
	
	@FindBy(xpath="//span[contains(text(),'Submit To Review')]")
	WebElement SubmitToReview;
	
	@FindBy(xpath="//span[contains(text(),'Yes')]")
	WebElement SubmitYesBtn;
	
	@FindBy(xpath="//span[contains(text(),'Ok')]")
	WebElement SubmitOk;
	
	@FindBy(xpath="//label[contains(.,'Reason')]//following::input")
	WebElement NeedWorkReason;
	
	@FindBy(xpath="//span[contains(.,'Need Work')]//ancestor::button")
	WebElement NeedWorkbtn;
	
	@FindBy(xpath="//span[contains(text(),'Yes')]")
	WebElement NeedWorkYes;
	
	@FindBy(xpath="//span[contains(text(),'Ok')]")
	WebElement NeedWorkOk;
	
// WorkFlow ---------------------------------------------------------

	// Published to Changed Status ----------------------------------------

	//@FindBy(xpath="//input[@name='searchString']")
	//WebElement Search;

	//@FindBy(xpath="//*[@id=\"view-icon_svg__Rectangle_273\"]")
	//WebElement View;

	@FindBy(id = "view-icon_svg__Rectangle_273")
	 List < WebElement > Edit;
	
	@FindBy(xpath="//span[contains(text(),'Unpublish Content')]")
	WebElement UnpublishContent;

	@FindBy(xpath="//label[contains(text(),'Reason')]//parent::div/div/input")
	WebElement Reason;

	@FindBy(xpath="//span[contains(text(),'Yes')]")
	WebElement YesBtn;

	@FindBy(xpath="//span[contains(text(),'Ok')]")
	WebElement Yes;

	@FindBy(xpath="//a[contains(text(),'Movies')]")
	WebElement MoviesListing;
	
//Move from Archive to Restore ---------------------------------------------- 

	@FindBy(id = "archive-icon_svg__Rectangle_273")
    List < WebElement > Archive;

	@FindBy(xpath="//span[contains(text(),'Yes')]")
	WebElement ArchiveYes;
	
	@FindBy(xpath="//span[@class='edit tooltip-sec hand-cursor']")
	WebElement Restore;

	@FindBy(xpath="//span[contains(text(),'Yes')]")
	WebElement RestoreYes;

	//@FindBy(xpath="//label[contains( text(),'Country / Group')]/parent::div/div/div/button[2]")
	@FindBy(xpath = "//div[contains(@class,'auto-countryGroup')]//descendant::input")
	WebElement PublishCountryDropdown;

	@FindBy(xpath="//span[contains(text(),'Publish Content')]")
	WebElement PublishedBtn;

	@FindBy(xpath="//span[contains(text(),'Yes')]")
	WebElement PublishedYes;

	@FindBy(xpath="//span[contains(text(),'Ok')]")
	WebElement Published;
	
	@FindBy(xpath="//span[contains(text(),'Republish Content')]")
	WebElement Republished;

	@FindBy(xpath="//span[contains(text(),'Yes')]")
	WebElement RepublishedYes;

	@FindBy(xpath="//div[3]/div[3]/div[1]/div[3]/button[1]")
	WebElement RepublishedConfirm;

	@FindBy(xpath="//button[contains(.,'Ok')]")
	WebElement PublishedAgain;
	
	@FindBy(xpath="//li[contains(text(),'Published History')]")
	WebElement PublishedHistory;

	@FindBy(xpath="//div[@class='mov-icon mov-view tooltip-sec']")
	List<WebElement> Clone;
	
	@FindBy(xpath="//span[contains(text(),'Yes')]")
	WebElement CloneYes;
	
	@FindBy(xpath="//span[contains(text(),'+')]")
	WebElement Scheduled_plus;
	
	@FindBy(xpath="//input[@name='scheduledPublicationTime']")
	//@FindBy(xpath="//button[@class='MuiButtonBase-root MuiIconButton-root']")
	WebElement ScheduledDate;
	
	@FindBy(xpath = "(//div[contains(@class,'auto-countryGroup')]//input)[2]")
	WebElement ScheduledCountry;
	
	@FindBy(xpath="//span[contains(text(),'Schedule Content')]")
	WebElement ScheduledBtn;
	
	@FindBy(xpath="//span[contains(text(),'Yes')]")
	WebElement ScheduledYes;
	
	@FindBy(xpath="//span[contains(text(),'Ok')]")
	WebElement OkBtn;
	
	@FindBy(xpath = "(//div[contains(@class,'val')])[3]")
	WebElement scheduledTime;
	
	@FindBy(xpath = "(//div[contains(@class,'icon-w-text icon-w-14 m-b-10')])[1]")
	WebElement currentTime;	
	
	@FindBy(xpath = "(//button[contains(@class,'MuiPickersToolbarButton-toolbarBtn')])[4]")
	WebElement datePickerMinutes;		
	
	@FindBy(xpath = "//span[contains(@class,'MuiTypography-root MuiPickersClockNumber-clockNumber') and contains(text(),'05')]")
	WebElement datePickerMinutesClock;	
	
// Sort, Filter ----------------------------------------------------------
	
	@FindBy(xpath="//span[contains(text(),'Sort')]")
	WebElement Sort;

	@FindBy(xpath="//span[contains(text(),'Ascending to Descending')]")
	WebElement AscToDesc;

	@FindBy(xpath="//span[contains(text(),'Apply Sort')]")
	WebElement ApplySort;

	@FindBy(xpath="//span[contains(text(),'Clear')]")
	WebElement Clear;

	@FindBy(xpath="//span[contains(text(),'Descending to Ascending')]")
	WebElement DescToAsc;

	@FindBy(xpath="//span[contains(text(),'Oldest to Newest')]")
	WebElement OldToNew;
	
	@FindBy(xpath="//span[contains(text(),'Newest to Oldest')]")
	WebElement NewToOld;

	@FindBy(xpath="//span[contains(text(),'Filters')]")
	WebElement Filter;

	@FindBy(xpath="//div[contains(text(),'Draft')]")
	WebElement Draft;
	
	@FindBy(xpath="//div[contains(text(),'All')]")
	WebElement AllStatus;

	@FindBy(xpath="//div[contains(text(),'Changed')]")
	WebElement Changed;

	@FindBy(xpath="//div[contains(text(),'Published')]")
	WebElement PublishedStatus;

	@FindBy(xpath="//div[contains(text(),'Unpublished')]")
	WebElement Unpublished;

	@FindBy(xpath="//div[contains(text(),'Need Work')]")
	WebElement NeedWork;

	@FindBy(xpath="//div[contains(text(),'Scheduled')]")
	WebElement Scheduled;

	@FindBy(xpath="//div[contains(text(),'Submitted to Review')]")
	WebElement SubmittedToReview;

	@FindBy(xpath="//div[contains(text(),'Archived')]")
	WebElement Archived;

	@FindBy(xpath="//span[contains(text(),'Apply Filter')]")
	WebElement ApplyFilter;

	@FindBy(xpath="//span[contains(text(),'Clear')]")
	WebElement ClearFilter;

	@FindBy(xpath="//input[@value='Start Date']")   
 	WebElement StartDate;

	@FindBy(xpath="//input[@value='End Date']")
 	WebElement EndDate;

	@FindBy(xpath="//input[@id='primaryGenre']")
	WebElement FilterPrimaryGenre;

	@FindBy(xpath="//input[@id='secondaryGenre']")
	WebElement SecondaryGenre;

	@FindBy(xpath="//input[@id='thematicGenre']")
	WebElement ThematicGenre;

	@FindBy(xpath="//input[@id='settingGenre']")
	WebElement SettingGenre;

	@FindBy(xpath="//input[@id='auto-actor-0-4']")
	WebElement Actor;

	@FindBy(xpath="//input[@id='subType']")
	WebElement SubType;

	@FindBy(xpath="//input[@id='contentCategory']")
	WebElement FilterContentCategory;

	@FindBy(xpath="//input[@id='rcsCategory']")
	WebElement RCSCategory;

	@FindBy(xpath="//html/body/div[2]/div[3]/div/div[1]")
	WebElement Closetab;

	@FindBy(xpath="//input[@id='theme']")
	WebElement Theme;

	@FindBy(xpath="//input[@id='targetAudience']")
	WebElement TargetAudienceFilter;

	@FindBy(xpath="//input[@id='licenseGroupCountries']")
	WebElement LicenseGroupCountries;

	@FindBy(xpath="//input[@id='ageRating']")
	WebElement AgeRating;

	@FindBy(xpath="//input[@id='businessType']")
	WebElement FilterBusinessType;

	@FindBy(xpath="//input[@name='externalId']")
	WebElement ExternalId;

	@FindBy(xpath="//input[@id='contentRating']")
	WebElement ContentRatingFilter;

	@FindBy(xpath="//input[@id='contentOwner']")
	WebElement FilterContentOwner;

	@FindBy(xpath="//input[@id='audioLanguage']")
	WebElement FilterAudioLanguage;

	@FindBy(xpath="//input[@id='originalLanguage']")
	WebElement FilterOriginalLanguage;

	@FindBy(xpath="//input[contains(@id,'tags')]")
	WebElement TagsFilter;

	@FindBy(xpath="//input[@id='translationLanguage']")
	WebElement TranslationLanguage;

	@FindBy(xpath="//input[@id='translationStatus']")
	WebElement TranslationStatus;

	@FindBy(xpath="//input[@id='moodEmotion']")
	WebElement MoodEmotion;

// Quick Filling & Single Landing Page ------------------------------------------------

	@FindBy(xpath="//span[contains(text(),'Quick Filing')]")
	WebElement DropdwnOptions;

	@FindBy(xpath="//*[@id=\"menu-list-grow\"]/li[1]")
	WebElement QuickFilling;

	@FindBy(xpath="//input[@id='subtype']")
	WebElement QuickSubtype;

	@FindBy(xpath="//input[@id='category']")
	WebElement QuickContentCategory;

	@FindBy(xpath="//input[@id='primaryGenre']")
	WebElement QuickPrimaryGenre;

//	@FindBy(xpath="//input[@type='date']")
	@FindBy(xpath="//button[@class='MuiButtonBase-root MuiIconButton-root']")
	WebElement QuickZeeReleaseDate;

	@FindBy(xpath="//input[@name='duration']")
	WebElement QuickVideoDuration;

	@FindBy(xpath="//input[@id='audioLanguages']")
	WebElement QuickAudioLanguage;

	@FindBy(xpath="//input[@id='primaryLanguage']")
	WebElement QuickPrimaryLanguage;

	@FindBy(xpath="//input[@id='dubbedLanguageTitle']")
	WebElement QuickDubbedLanguage;

	@FindBy(xpath="//input[@id='originalLanguage']")
	WebElement QuickOriginalLanguage;

	@FindBy(xpath="//input[@id='contentVersion']")
	WebElement QuickContentVersion;

	@FindBy(xpath="//div[@class='next-step-btn']")
	WebElement QuickNext;

	@FindBy(xpath="//input[@id='contentOwner']")
	WebElement QuickContentOwner;

	@FindBy(xpath="//input[@id='certification']")
	WebElement QuickCertification;
	@FindBy(xpath="//div[contains(text(),'Mark as Done')]")
	WebElement QuickMarkAsDone;
	
//License section------------------------------------------------------

	@FindBy(xpath="//span[contains(text(),'License')]")
	WebElement license;
	
	@FindBy(xpath="//div[contains(text(),'Create License')]")	
	WebElement createLicense;
	
//	@FindBy(xpath="//*[@id=\"app\"]/div/div[3]/div[3]/div/div[2]/div/div/div[2]/div[1]/div[2]/div/button[2]")	
//	WebElement licenseFilter;
	
	@FindBy(xpath="//input[@name='fromDate']")
	WebElement licenseFromDateFilter;
	
	@FindBy(xpath="//input[@name='toDate']")
	WebElement licenseToDateFilter;
	
// Video section ------------------------------------------------------

	@FindBy(xpath="//span[contains(text(),'Videos')]")
	WebElement QuickVideoSection;
	
	@FindBy(xpath="//input[@name='dashRootFolderName']")
	WebElement QuickDashRoot;
	
	@FindBy(xpath="//input[@name='dashManifestName']")
	WebElement QuickDashManifest;
	
	@FindBy(xpath="//input[@name='hlsRootFolderName']")
	WebElement QuickHLSRoot;
	
	@FindBy(xpath="//input[@name='hlsManifestName']")
	WebElement QuickHLSManifest;
	
	@FindBy(xpath="//input[@name='mediathekFileUid']")
	WebElement QuickMediathek;

// Image --------------------------------------------------------------

	@FindBy(xpath="//span[contains(text(),'Images')]")
	WebElement QuickImageSection;

	@FindBy(xpath="//input[@name='list']")
	WebElement QuickUploadimage;

// SEO section ---------------------------------------------------------

	@FindBy(xpath="//span[contains(text(),'SEO Details')]")
	WebElement QuickSeoSection;

	@FindBy(xpath="//input[@name='titleTag']")
	WebElement QuickSeoTitle;

	@FindBy(xpath="//textarea[@name='metaDescription']")
	WebElement QuickSeoMataDesc;

	@FindBy(xpath="//textarea[@name='metaSynopsis']")
	WebElement QuickSeoMeta;

	@FindBy(xpath="//input[@name='h1Heading']")
	WebElement QuickHeading;

	@FindBy(xpath="//input[@name='h2Heading']")
	WebElement QuickHeading2;

	@FindBy(xpath="//input[@name='videoObjectSchema']")
	WebElement QuickVideo; 

	@FindBy(xpath="//input[@name='websiteSchema']")
	WebElement QuickWeb;

	@FindBy(xpath="//input[@name='breadcrumListSchema']")
	WebElement QuickBread;

	@FindBy(xpath="//input[@name='imageObjectSchema']")
	WebElement QuickImage;

	@FindBy(xpath="//input[@name='organizationSchema']")
	WebElement QuickOrganization;

// MapContent ----------------------------------------------------------------

	@FindBy(xpath="//button[contains(@class,'auto-leftTab-MapContent')]")
	WebElement QuickMapContent;

	@FindBy(xpath="//div[contains(text(),'Add Content')]")
	WebElement QuickAddContent;

	@FindBy(xpath="//span[contains(text(),'Link Content')]")
	WebElement LinkContent;

	@FindBy(xpath="//div[contains(text(),'Add Content')]")
	WebElement LinkAddContent;

	@FindBy(xpath="//input[@type='checkbox']")
	WebElement LinkCheckbox;

	@FindBy(xpath="//div[contains(text(),'Assign Content')]")
	WebElement LinkAssignContent;

// CheckList ------------------------------------------------------------------

	@FindBy(xpath="//button[contains(@class,'auto-leftTab-Checklist')]")
	WebElement QuickChecklistSection;

	@FindBy(xpath="//*[@id='app']/div/div[3]/div[1]/nav/ol/li[3]/a")
	WebElement QuickMoviesListing;

	@FindBy(xpath="//span[contains(text(),'View Details')]")
	WebElement ViewDetails;

	@FindBy(xpath="//*[@id=\"app\"]/div/div[3]/div/div[3]/div/div[2]/div[1]/div[3]/div[2]/div[2]/div")
	WebElement LinkMovies;

//	@FindBy(xpath="//a[contains(text(),'Movies')]")
	@FindBy(xpath="//li[3]")
	WebElement movieBreadcrumb;

// Single Landing Page ---------------------------------------------------------

	@FindBy(xpath="//div[@class='filling-dropdown']//li[2]")
	WebElement SingleLanding;
	
	@FindBy(xpath="//input[@name='title']")
	WebElement SingleLandingTitle;
	
	@FindBy(xpath="//div[@class='ql-editor ql-blank']")
	WebElement SingleShortdesc;
	
	@FindBy(xpath="//input[@id='subtype']")
	WebElement SingleSubType;
	
	@FindBy(xpath="//input[@id='primaryGenre']")
	WebElement SinglePrimaryGenre;
	
	@FindBy(xpath="//input[@id='audioLanguages']")
	WebElement SingleAudioLanguage;
	
	@FindBy(xpath="//div[@class='next-step-btn']")
	WebElement SingleNextBtn;
	
	@FindBy(xpath="//div[contains(text(),'Mark as Done')]")
	WebElement SingleMarkAsDone;

//Images -------------------------------------------------------------
		
	@FindBy(xpath="//span[contains(text(),'Images')]")
	WebElement SingleImagesSection;

	@FindBy(xpath="//input[@name='list']")
	WebElement SingleUploadimage;

//SEO -----------------------------------------------------------------

	@FindBy(xpath="//span[contains(text(),'SEO Details')]")
	WebElement SingleSeoSection;
		
	@FindBy(xpath="//input[@name='titleTag']")
	WebElement SingleSeoTitle;

	@FindBy(xpath="//textarea[@name='metaDescription']")
	WebElement SingleSeoMataDesc;

	@FindBy(xpath="//textarea[@name='metaSynopsis']")
	WebElement SingleSeoMeta;
		
	@FindBy(xpath="//input[@name='h1Heading']")
	WebElement SingleHeading;

	@FindBy(xpath="//input[@name='h2Heading']")
	WebElement SingleHeading2;

	@FindBy(xpath="//input[@name='videoObjectSchema']")
	WebElement SingleVideo; 

	@FindBy(xpath="//input[@name='websiteSchema']")
	WebElement SingleWeb;

	@FindBy(xpath="//input[@name='breadcrumListSchema']")
	WebElement SingleBread;

	@FindBy(xpath="//input[@name='imageObjectSchema']")
	WebElement SingleImage;

	@FindBy(xpath="//input[@name='organizationSchema']")
	WebElement SingleOrganization;

//License ------------------------------------------------------------------

	//	@FindBy(xpath="//*[@id='app']/div/div[3]/div[3]/div/div[1]/div/div/div[2]/div/button[2]/span[1]")
   //	WebElement SingleLicenseSection;
	
	@FindBy(xpath="//span[contains(text(),'License')]")
	WebElement license1;
	
	@FindBy(xpath="//div[contains(text(),'Create License')]")	
	WebElement createLicense1;
	
//	@FindBy(xpath="//*[@id=\"app\"]/div/div[3]/div[3]/div/div[2]/div/div/div[2]/div[1]/div[2]/div/button[2]")	
//	WebElement licenseFilter1;
	
	@FindBy(xpath="//input[@name='fromDate']")
	WebElement licenseFromDateFilter1;
	
	@FindBy(xpath="//input[@name='toDate']")
	WebElement licenseToDateFilter1;
	
//	@FindBy(xpath="/html/body/div[2]/div[3]/div/div[3]/button[1]")
//	WebElement licenseFilterButton1;
	
	@FindBy(xpath="//input[@name='fromDate']")
	WebElement licenseFromDate1;
	
	@FindBy(xpath="//input[@name='toDate']")
	WebElement licenseToDate1;
	
//	@FindBy(xpath="//body/div[@id='app']/div[1]/div[3]/div[3]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/form[1]/div[1]/div[3]/div[1]/div[1]/div[1]")
//	WebElement createLicenseCountry1;
	
//	@FindBy(xpath="//body/div[@id='app']/div[1]/div[3]/div[3]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/form[1]/div[1]/div[4]/div[1]/div[1]/div[1]")
//	WebElement createLicenseBusinessType1;
	
	@FindBy(xpath="//span[contains(text(),'SAVE')]")
	WebElement createLicenseSave1;
	
// MapContent -----------------------------------------------------------------------------

	@FindBy(xpath="//button[contains(.,'Map Content')]")
	WebElement SingleMapContent;

//Checklist --------------------------------------------------------------------------------

	@FindBy(xpath="//button[contains(@class,'auto-leftTab-Checklist')]")
	WebElement SingleChecklistSection;

	@FindBy(xpath="//*[@id='app']/div/div[3]/div[1]/nav/ol/li[3]/a")
	WebElement SingleMovieListing;

///Quick link ----------------------------------------------------------------------------------

	//Movies --> QuickLinks --> Related Content --> AssignContent

//--QuickLinks--------------------------------------------------------------
		
	@FindBy(xpath="//input[@name='searchString']")
	WebElement Search;

	@FindBy(xpath="//*[@id='view-icon_svg__Rectangle_273']")
	WebElement View;
	
    @FindBy(xpath = "//div[contains(@class,'span view tooltip-sec')]")
    List < WebElement > editView;
		
	@FindBy(xpath="//li[@class='auto-quickLinks-RelatedContent']")
	WebElement relatedContent;
		
	@FindBy(xpath="//button[contains(.,'Assign Movies')]")
	WebElement assignMovies;
		
	@FindBy(xpath="//div[contains(@class,'auto-add-content')]")
	WebElement addContent;

	@FindBy(xpath = "//input[@type='checkbox']")
	List < WebElement > addContentCheckbox;

	//@FindBy(xpath="//div[@id='assignedContent']")
    @FindBy(xpath = "//div[contains(text(),'Assign Video')]")
    WebElement assignRelatedContentVideos;
    
    @FindBy(xpath = "//div[contains(text(),'Assign Movie')]")
    WebElement assignRelatedContentMovies;

	@FindBy(xpath = "//div[1]//div[1]//div[1]//div[3]//div[1]//span[1]")
	List < WebElement > removeContent;

	@FindBy(xpath="//button[contains(@class,'MuiButtonBase-root MuiButton-root MuiButton-text auto-button-Yes')]")
	WebElement RemoveYesbtn;

	@FindBy(xpath="//button[contains(.,'Assign Videos')]")
	WebElement assignVideos;

	@FindBy(xpath="//button[contains(.,'Assign TV Shows')]")
	WebElement assignTvShows;

	@FindBy(xpath = "//div[2]//div[1]//div[1]//div[2]//div[2]//div[1]//span[1]")
	List < WebElement > removeTvShowContent;

	@FindBy(xpath="//button[contains(.,'Assign Seasons')]")
	WebElement assignSeasons;

	@FindBy(xpath = "//div//div[1]//div[1]//div[1]//div[2]//div[2]//div[1]//span[1]")
	List < WebElement > removeSeasonContent;
		
	@FindBy(xpath="//button[contains(.,'Assign Episodes')]")
	WebElement assignEpisodes;

	@FindBy(xpath = "//div//div[1]//div[1]//div[1]//div[2]//div[2]//div[1]//span[1]")
	List < WebElement > removeEpisodeContent;

	@FindBy(xpath="//div[contains(text(),'Mark as Done')]")
	WebElement MarkasDone;

	@FindBy(xpath="//strong[contains(text(),'Related Content')]")
	WebElement RelatedBackbtn;	

    @FindBy(name = "searchVal")
    WebElement relatedContentSearch;

    @FindBy(xpath = "//span[contains(text(),'Assign Movies')]")
    WebElement relatedContentAssignMovies;

    @FindBy(xpath = "//div[contains(text(),'Add Movies')]")
    WebElement relatedContentAddMovies;

    @FindBy(xpath = "//span[contains(text(),'Assign TV Shows')]")
    WebElement relatedContentAssignTvShows;

    @FindBy(xpath = "//div[contains(text(),'Add TV Shows')]")
    WebElement relatedContentAddTvShows;

    @FindBy(xpath = "//span[contains(text(),'Assign Videos')]")
    WebElement relatedContentAssignVideos;

    @FindBy(xpath = "//div[contains(text(),'Add Videos')]")
    WebElement relatedContentAddVideos;

    @FindBy(xpath = "//button[contains(.,'Assign Seasons')]")
    WebElement relatedContentAssignSeasons;

    @FindBy(xpath = "//div[contains(text(),'Add Seasons')]")
    WebElement relatedContentAddSeasons;

    @FindBy(xpath = "//button[contains(.,'Assign Episodes')]")
    WebElement relatedContentAssignEpisodes;

    @FindBy(xpath = "//div[contains(text(),'Add Episodes')]")
    WebElement relatedContentAddEpisodes;

    @FindBy(xpath = "//li[contains(text(),'Published History')]")
    WebElement publishedHistory;

    @FindBy(id = "assignedContent")
    WebElement assignRelatedContent;
    
    @FindBy(xpath = "(//span[contains(@class,'auto-delete')])[1]")
    WebElement deleteRelatedContent;
    
    @FindBy(xpath = "//div[contains(@class,'auto-back-btn')]//span")
    WebElement relatedContentBack;
		
//Logout and Login------------------------------------------------------------
		
	@FindBy(xpath="//div[contains(@class,'logout-link')]/button")
	WebElement logoutlink;
	
	@FindBy(xpath="//*[@id=\"menu-list-grow\"]/li")
	WebElement logoutbutton;
	
//login through Adfs----------------------------------------------------------
	
	@FindBy(xpath="//span[text()='Login with ADFS']")
	public WebElement ADFSLogin;

	@FindBy(xpath="//input[@name='UserName']")
	public WebElement UserName;

	@FindBy(xpath="//input[@name='Password']")
	public WebElement Password;

	@FindBy(xpath="//span[@id='submitButton']")
	public WebElement submit;

//Translation---------------------------------------------------------------
	
	@FindBy(xpath = "//li[@class='auto-quickLinks-Translations']")
	WebElement TranslationBtn;
	
	@FindBy(xpath = "//div[@class='flex align-items-center justify-content-between']")
	WebElement LanguageHeader;

	@FindBy(xpath="//input[@name='title']")
	WebElement TitleTranslate;
	
	@FindBy(xpath = "//input[@name='title']")
	WebElement TitleTextbox;
	
	@FindBy(xpath="//div[contains(@id,'auto-Short Description')]//child::div[contains(@class,'ql-editor')]")
	WebElement ShortDescriptionTextbox;
	
	@FindBy(xpath = "//div[contains(@id,'auto-Web Description')]//child::div[contains(@class,'ql-editor')]")
	WebElement WebDescriptionTextbox;

	@FindBy(xpath ="//div[contains(@id,'auto-App Description')]//child::div[contains(@class,'ql-editor')]")
	WebElement AppDescriptionTextbox;

	@FindBy(xpath ="//div[contains(@id,'auto-TV Description')]//child::div[contains(@class,'ql-editor')]")
	WebElement TvDescriptionTextbox;

	@FindBy(xpath ="//input[@name='creativeTitle']")
	WebElement CreativeTitleTextbox;

	@FindBy(xpath = "//input[@name='alternativeTitle']")
	WebElement AlternativeTitleTextbox;

	@FindBy(xpath = "//input[@name='pageTitle']")
	WebElement PageTitleTextbox;

	@FindBy(xpath = "//textarea[@name='pageDescription']")
	WebElement PageDescriptionTextbox;

	@FindBy(xpath = "//input[@name='socialShareTitle']")
	WebElement SocialShareTitleTextbox;

	@FindBy(xpath = "//textarea[@name='socialShareDescription']")
	WebElement SocialShareDescriptionTextbox;

	@FindBy(xpath = "//input[@name='upcomingPageText']")
	WebElement UpcomingPageTextbox;

	@FindBy(xpath = "//input[@name='trivia']")
	WebElement TriviaTextbox;
	
	@FindBy(xpath = "//div[contains(@class,'mark-fill-active')]")
	WebElement MarkDoneBtn;
	
	@FindBy(xpath = "//span[contains(text(),'Hindi')]")
	WebElement HindiBtn;
	
	@FindBy(xpath = "//span[contains(text(),'Marathi')]")
	WebElement MarathiBtn;

 /*	@FindBy(xpath = "//span[contains(text(),'Telugu')]")
	WebElement TelguBtn;

	@FindBy(xpath = "//span[contains(text(),'Kannada')]")
	WebElement KannadaBtn;

	@FindBy(xpath = "//span[contains(text(),'Bengali')]")
	WebElement BengaliBtn;

	@FindBy(xpath = "//span[contains(text(),'Bhojpuri')]")
	WebElement BhojpuriBtn;

	@FindBy(xpath = "//span[contains(text(),'Gujarati')]")
	WebElement GujaratiBtn;

	@FindBy(xpath = "//span[contains(text(),'Arabic')]")
	WebElement ArabicBtn;

	@FindBy(xpath = "//body/div[@id='app']/div[1]/div[3]/div[3]/div[1]/div[1]/div[1]/div[2]/div[4]/*[1]")
	WebElement ScrollMovieListBtn;

	@FindBy(xpath = "//span[contains(text(),'French')]")
	WebElement FrenchBtn;

	@FindBy(xpath = "//span[contains(text(),'German')]")
	WebElement GermanBtn; */
		
//--- Assertions -----------------------------------------------------------------------
		
	@FindBy(xpath="//span[contains(text(),'Title Summary')]")
	WebElement TitleSummarytab;
	
	@FindBy(xpath = "//div[contains(@class,'auto-PrimaryGenre')]//descendant::span[@class='MuiChip-label']")
	WebElement primaryGenreAssertion;

	@FindBy(xpath = "//div[contains(@class,'auto-SecondaryGenre')]//descendant::span[@class='MuiChip-label']")
	WebElement secondaryGenreAssertion;

	@FindBy(id = "view-icon_svg__Rectangle_273")
	List<WebElement> editButton;

	@FindBy(xpath = "//span[contains(text(),'Edit Movie')]")
	WebElement editMovie;
	
	@FindBy(xpath = "//button[contains(.,'Edit Profile')]")
	WebElement editProfile;
	
    @FindBy(xpath = "//span[contains(text(),'Continue Editing with Quick Filing')]")
    WebElement editMovieQuickFiling;
    
    @FindBy(xpath = "//span[contains(text(),'Continue Editing with Single Landing Page')]")
    WebElement editMovieSingleLanding;
		 
	@FindBy(xpath="//div[contains(@class,'auto-DubbedLanguageTitle')]//descendant::span[@class='MuiChip-label']")
	WebElement dubbedLanguageAssertion;
	   
	@FindBy(xpath="//div[contains(@class,'auto-AudioLanguage')]//descendant::span[@class='MuiChip-label']")
	WebElement audioLanguagesAssertion;
		 
	@FindBy(className = "next-step-btn")
	WebElement nextButton;

	@FindBy(xpath = "//div[contains(@class,'mark-active')]")
	WebElement doneButtonClicked;
		 
	@FindBy(xpath = "//button[contains(@class,'auto-leftTab-Cast&Crew')]")
	WebElement castCrew;
	
	@FindBy(xpath = "//button[contains(.,'Cast & Crew')]")
	WebElement castAndCrew;
		
	@FindBy(xpath = "//input[@id='auto-Actor']")
	WebElement actorName;
		
	@FindBy(xpath = "//input[@id='auto-Character']")
	WebElement characterName;
		
	@FindBy(xpath = "//div[@id='markAsDoneBtn']")
	WebElement markAsDoneBtn;
		
	@FindBy(xpath = "//div[1]/div[3]/div[2]/div[1]/span[1]/*[1]")
	WebElement backToTranslations;
	
	@FindBy(xpath = "//div[contains(@class,'s-badge')]")
	WebElement contentStatus; 

	 //------------------------------------------Listing Page ---------------------------------------
    @FindBy(xpath = "(//div[contains(@class,'back-user-btn')]//span)[1]")
    WebElement listingBackArrow;

    @FindBy(xpath = "(//div[@class = 'license-badge'])[1]")
    WebElement listingExpiringLicense;
 
    @FindBy(xpath = "(//div[contains(@class,'mov-link-btn')])[1]")
    WebElement listingLinkedMovies;
    
    @FindBy(xpath = "(//a)[3]")
    WebElement listingLicenseCountries;
    
    @FindBy(xpath = "//span[contains(text(),'Close')]")
    WebElement listingCloseLicenseCountries;  
    
    @FindBy(xpath = "(//span[@class='pub-history'])[1]")
    WebElement listingPublishedHistory;    
    
    @FindBy(xpath = "//div[@class='side-close-btn']")
    WebElement listingClosePublishedHistory;    

    @FindBy(xpath = "//li[@class='auto-leftTab-All']")
    WebElement listingAllStatus;
    	
    @FindBy(xpath = "//li[@class='auto-leftTab-Draft']")
    WebElement listingDraftStatus;
    
    @FindBy(xpath = "//li[@class='auto-leftTab-Changed']")
    WebElement listingChangedStatus;
    
    @FindBy(xpath = "//li[@class='auto-leftTab-Published']")
    WebElement listingPublishedStatus;
    
    @FindBy(xpath = "//li[@class='auto-leftTab-Unpublished']")
    WebElement listingUnpublishedStatus;
    
    @FindBy(xpath = "//li[@class='auto-leftTab-NeedWork']")
    WebElement listingNeedWorkStatus;
    
    @FindBy(xpath = "//li[@class='auto-leftTab-Scheduled']")
    WebElement listingScheduledStatus;
    
    @FindBy(xpath = "//li[@class='auto-leftTab-SubmittedToReview']")
    WebElement listingSubmittedToReviewStatus;
    
    @FindBy(xpath = "//li[@class='auto-leftTab-Archived']")
    WebElement listingArchivedStatus;
    
    @FindBy(xpath = "//li[@class='auto-leftTab-PublishingQueue']")
    WebElement listingPublishingQueueStatus;
    
    @FindBy(xpath = "(//span[contains(text(),'View Details')])[1]")
    WebElement listingViewDetails;
    
    @FindBy(xpath = "(//span[contains(text(),'Hide Details')])[1]")
    WebElement listingHideDetails;
    
//--------------Common Elements----------------------------
	
	@FindBy(xpath = "//div[@class = 'loc-icon']")
	WebElement unlock;
	    
    @FindBy(xpath = "//span[contains(text(),'Yes') or contains(text(),'Ok')]")
    List < WebElement > yes;	    

    @FindBy(className = "head-external-id")
    WebElement movieExternalID; 
    
    @FindBy(css = "button.MuiButtonBase-root.MuiIconButton-root.MuiAutocomplete-popupIndicator.MuiAutocomplete-popupIndicatorOpen > span.MuiIconButton-label > svg.MuiSvgIcon-root")
    WebElement closeDropdown;
    
    @FindBy(xpath = "//span[contains(text(),'Save')]")
    WebElement save;
    
    @FindBy(xpath = ("//input[@type='checkbox']"))
    List < WebElement > checkBox;
    
  //Initializing the page objects-----------------------------

  	public MoviesPage()
  	{
  		PageFactory.initElements(driver, this);
  	}
  	
  	public void LoginVerify() throws InterruptedException{
  		  WebUtility.Wait();
  		//WebUtility.element_to_be_visible(Username);
  		//String username=Username.getText();
  		  originalUrl = driver.getCurrentUrl();
  	}
  	
  	public void MasterAndCastProfile()  throws InterruptedException {
  		//Navigate to master-------------------
  		WebUtility.Click(mastermgmt);
  		WebUtility.Wait();
  		WebUtility.Click(managegenre);
  		WebUtility.Wait();
  		WebUtility.Click(addgenre);
  		genre.sendKeys("Horror");
  		WebUtility.Click(add);
  		WebUtility.Wait();
  		WebUtility.Click(ok);
  		search.sendKeys("Horror");
  		Thread.sleep(1000);
  		//--TODO Update when Roles is fixed ----
  		/*
  		//go to language and add language--------
  		WebUtility.javascript(mastermgmt);
  		WebUtility.Wait();
  		managelng.click();
  		WebUtility.Wait();
  		addlng.click();
  		lngcode.sendKeys("NEWZ");		
  		lngname.sendKeys("NewAutomateLanguage");
  		addl.click();
  		WebUtility.Wait();
  		okl.click();
  		searchl.sendKeys("NewAutomateLanguage");
  		Thread.sleep(3000);
  			
  		//Navigate to cast profile--------------
  		logoutlink.click();
  		logoutbutton.click();
  		WebUtility.clear_cache();
  		driver.get(prop.getProperty("url")); 
  		loginPage1.gmailLogin(prop.getProperty("Gusername3"),prop.getProperty("Gpassword"));	
  		*/
  		WebUtility.Click(dashboard);
  		WebUtility.Wait();
  		WebUtility.javascript(castprofilemgmt);
  		WebUtility.javascript(createprofilebtn);
  		WebUtility.Wait();
  		try {
  		uploadbtn.sendKeys(System.getProperty("user.dir") + "\\images\\movies\\kangna.jpg");
  		}
  		catch(Exception e)
  		{
  			uploadbtn.sendKeys("/dev/test/images/movies/kangna.jpg");
  		}
  		WebUtility.Wait();
  		profilename.sendKeys("Kangana Ranuat");
  		castknown.sendKeys("Queen");
  		WebUtility.Click(tagdrop);
  		WebUtility.Wait();
  		WebUtility.Click(dropDownList.get(0));
  		casttype.click();
  		WebUtility.Wait();
  		WebUtility.Click(dropDownList.get(0));
  	
  		WebUtility.Click(birthcalen);
        datePicker.sendKeys(Keys.ARROW_LEFT);
        datePicker.sendKeys(Keys.ARROW_LEFT);
        datePicker.sendKeys(Keys.ESCAPE);
        WebUtility.Wait();
          
        WebUtility.Click(birthplace);
  		birthplace.sendKeys("delhi");		
  		career.sendKeys("Heroine");		
  		awards.sendKeys("Oscar");
  	
  		WebUtility.Click(trivia);
  		trivia.sendKeys("Oscar");
  		WebUtility.Click(background);
  		background.sendKeys("filmstar");
  		Thread.sleep(1000);
  		WebUtility.Click(profilebio);
  		profilebio.sendKeys("Kangana Ranaut was born on March 1987 (Age: 34 years, as in 2020) at Bhambla, Himachal Pradesh. Her father name is Amardeep Ranaut (businessman and contractor) and her mother name is Asha Ranaut (school teacher). She has an elder sister named Rangoli and a younger brother named Akshit Ranaut.");
  		relatedto.sendKeys("Kangana");
  		WebUtility.Wait();	
  		relatedto.sendKeys(Keys.ARROW_DOWN);
  		relatedto.sendKeys(Keys.ENTER);
  		relation.click();
  		WebUtility.Wait();
  		dropDownList.get(0).click();
  		WebUtility.Click(profilebio);
  		js.executeScript("window.scrollTo(0,0);");
  		WebUtility.Wait();
  		WebUtility.Click(markasdone);
  		WebUtility.Wait();
  		
  		//Go to image section of cast---------
  		WebUtility.Click(Imagesection);
  		WebUtility.Wait();
  		
		try {
			uploadImagebtn.sendKeys(System.getProperty("user.dir") + "\\images\\movies\\kangna.jpg");
		} catch (Exception e) {
			uploadImagebtn.sendKeys("/dev/test/images/movies/kangna.jpg");
		}

  		WebUtility.Wait();
  		WebUtility.Click(markasdoneimg);
  		WebUtility.Wait();
  		chklistradioBtn.click();
  		WebUtility.Wait();
  		JavascriptExecutor js = (JavascriptExecutor) driver;
  		js.executeScript("window.scrollBy(0,1000)");
  		WebUtility.element_to_be_clickable(publishBtn);
  		WebUtility.javascript(publishBtn);
  		WebUtility.Wait();
  		WebUtility.Click(publishYesbtn);
  		WebUtility.Wait();
  		WebUtility.Click(okbtn);
  		WebUtility.Wait();
  		
  		//Create another cast-------------------------------------
  		WebUtility.javascript(createprofilebtn);
  		WebUtility.Wait();  		
		try {
			uploadbtn.sendKeys(System.getProperty("user.dir") + "\\images\\movies\\anand-l-rai-1200.jpg");
		} catch (Exception e) {
			uploadbtn.sendKeys("/dev/test/images/movies/anand-l-rai-1200.jpg");
		}				
  		WebUtility.Wait();
  		profilename.sendKeys("Aanand L Rai");
  		castknown.sendKeys("Director");
  		
  		WebUtility.Click(tagdrop);
  		WebUtility.Wait();
  		WebUtility.Click(dropDownList.get(0));
  		
  		WebUtility.Click(casttype);
  		WebUtility.Wait();
  		WebUtility.Click(dropDownList.get(0));	
  		
		WebUtility.Click(birthcalen);
		datePicker.sendKeys(Keys.ARROW_LEFT);
		datePicker.sendKeys(Keys.ARROW_LEFT);
		datePicker.sendKeys(Keys.ESCAPE);
		// birthcalen.sendKeys("06281971");
		WebUtility.Click(birthplace);
		birthplace.sendKeys("delhi");
		career.sendKeys("Heroine");
		awards.sendKeys("Oscar");
		// trivia.sendKeys("Oscar");
		background.sendKeys("filmstar");
		WebUtility.Click(profilebio);
		profilebio.sendKeys(
				"Aanand L. Rai is a Hindi film director and producer known for romantic-comedy movies Tanu Weds Manu, Raanjhanaa, Tanu Weds Manu: Returns and Zero");
		relatedto.sendKeys("Kangana");
		WebUtility.Wait();
		relatedto.sendKeys(Keys.ARROW_DOWN);
		relatedto.sendKeys(Keys.ENTER);
		relation.click();
		WebUtility.Wait();
		WebUtility.Click(dropDownList.get(0));
		WebUtility.Click(profilebio);

		js.executeScript("window.scrollTo(0,0);");
		WebUtility.Wait();
		WebUtility.Click(markasdone);
		WebUtility.Wait();
  		
  		//Go to image section------------------------
		WebUtility.Click(Imagesection);
  		WebUtility.Wait(); 		
		try {
			uploadImagebtn.sendKeys(System.getProperty("user.dir") + "\\images\\movies\\anand-l-rai-1200.jpg");
		} catch (Exception e) {
			uploadImagebtn.sendKeys("/dev/test/images/movies/anand-l-rai-1200.jpg");
		}		
  		WebUtility.Wait();
  		WebUtility.Click(markasdoneimg);
  		WebUtility.Wait();
  		chklistradioBtn.click();
  		WebUtility.Wait();
  		JavascriptExecutor js6 = (JavascriptExecutor) driver;
  		js6.executeScript("window.scrollBy(0,1000)");
  		WebUtility.element_to_be_clickable(publishBtn);
  		WebUtility.javascript(publishBtn);
  		WebUtility.Wait();
  		WebUtility.Click(publishYesbtn);
  		WebUtility.Wait();
  		WebUtility.Click(okbtn);
  		WebUtility.Wait();
  		
  		//Apply assertion --------------------------------
  		Castsearchbar.sendKeys("Kangana Ranuat");
  		Thread.sleep(3000);
  		WebUtility.element_to_be_visible(CastCreated);
  		Castname=CastCreated.getText();
  		
  		
		// --TODO Update when roles is fixed ---
  		/*
  		//Adding Translations to Cast Profile--------------------------
  		logoutlink.click();
  		logoutbutton.click();
  		WebUtility.clear_cache();
  		driver.get(prop.getProperty("url")); 
  		loginPage1.gmailLogin(prop.getProperty("Gusername2"),prop.getProperty("Gpassword"));
  		
  		WebUtility.Click(dashboard);
  		WebUtility.Wait();
  		WebUtility.Click(castprofilemgmt);
  		WebUtility.Wait();
  		Castsearchbar.sendKeys("Kangana Ranuat");
  		WebUtility.Wait();
          WebUtility.Click(editView.get(0));

  		TranslationBtn.click();
  		WebUtility.element_to_be_visible(LanguageHeader);
  		WebUtility.Wait();
          
  		//Hindi Translation
         	HindiBtn.click();
  		WebUtility.Wait();
        	WebUtility.Click(profileName);
        	profileName.sendKeys(Keys.CONTROL + "a");
        	profileName.sendKeys(Keys.BACK_SPACE);
        	profileName.sendKeys("कंगना रनौत");
          
        	WebUtility.Click(knownAs);
        	knownAs.sendKeys(Keys.CONTROL + "a");
        	knownAs.sendKeys(Keys.BACK_SPACE);
        	knownAs.sendKeys("रानी");
          
        	WebUtility.Click(birthPlace);
        	birthPlace.sendKeys(Keys.CONTROL + "a");
        	birthPlace.sendKeys(Keys.BACK_SPACE);
        	birthPlace.sendKeys("दिल्ली");
          
        	WebUtility.Click(profileBio);
        	profileBio.sendKeys(Keys.CONTROL + "a");
        	profileBio.sendKeys(Keys.BACK_SPACE);
        	profileBio.sendKeys("कंगना रनौत का जन्म मार्च 1987 (आयु: 34 वर्ष, 2020 तक) हिमाचल प्रदेश के भांबला में हुआ था। उनके पिता का नाम अमरदीप रानौत (व्यापारी और ठेकेदार) है और उनकी माँ का नाम आशा रनौत (स्कूल शिक्षक) है। उनकी एक बड़ी बहन रंगोली है और एक छोटा भाई जिसका नाम अक्षत रनौत है।");
                 
        	WebUtility.Click(profileBackground);
        	profileBackground.sendKeys(Keys.CONTROL + "a");
        	profileBackground.sendKeys(Keys.BACK_SPACE);
        	profileBackground.sendKeys("फिल्म अभिनेता");
          
        	WebUtility.Click(profileCareer);
        	profileCareer.sendKeys(Keys.CONTROL + "a");
        	profileCareer.sendKeys(Keys.BACK_SPACE);
        	profileCareer.sendKeys("नायिका");
          
        	WebUtility.Click(profileAwards);
        	profileAwards.sendKeys(Keys.CONTROL + "a");
        	profileAwards.sendKeys(Keys.BACK_SPACE);
        	profileAwards.sendKeys("ऑस्कर");
          
        	WebUtility.Click(profileTrivia);
        	profileTrivia.sendKeys(Keys.CONTROL + "a");
        	profileTrivia.sendKeys(Keys.BACK_SPACE);
        	profileTrivia.sendKeys("ऑस्कर");

  		js.executeScript("window.scrollTo(0,0);");
  		WebUtility.Wait();
  		WebUtility.Click(doneButtonActive);
  		WebUtility.Wait();

  		//Marathi Translation
  		MarathiBtn.click();
  		WebUtility.Wait();
        	WebUtility.Click(profileName);
        	profileName.sendKeys(Keys.CONTROL + "a");
        	profileName.sendKeys(Keys.BACK_SPACE);
        	profileName.sendKeys("कंगना रानुआत");
          
        	WebUtility.Click(knownAs);
        	knownAs.sendKeys(Keys.CONTROL + "a");
        	knownAs.sendKeys(Keys.BACK_SPACE);
        	knownAs.sendKeys("राणी");
          
        	WebUtility.Click(birthPlace);
        	birthPlace.sendKeys(Keys.CONTROL + "a");
        	birthPlace.sendKeys(Keys.BACK_SPACE);
        	birthPlace.sendKeys("दिल्ली");
          
        	WebUtility.Click(profileBio);
        	profileBio.sendKeys(Keys.CONTROL + "a");
        	profileBio.sendKeys(Keys.BACK_SPACE);
        	profileBio.sendKeys("कंगना रनौत यांचा जन्म मार्च 1987 रोजी (वय: 34 वर्षे, 2020 प्रमाणे) हिमाचल प्रदेशच्या भांबला येथे झाला. तिच्या वडिलांचे नाव अमरदीप राणौत (व्यापारी आणि ठेकेदार) आणि आईचे नाव आशा रानौत (शालेय शिक्षिका) आहे. तिच्याकडे रंगोली नावाची मोठी बहीण आणि अक्षित रनौत नावाचा एक छोटा भाऊ आहे.");
                 
        	WebUtility.Click(profileBackground);
        	profileBackground.sendKeys(Keys.CONTROL + "a");
        	profileBackground.sendKeys(Keys.BACK_SPACE);
        	profileBackground.sendKeys("चित्रपट तारा");
          
        	WebUtility.Click(profileCareer);
        	profileCareer.sendKeys(Keys.CONTROL + "a");
        	profileCareer.sendKeys(Keys.BACK_SPACE);
        	profileCareer.sendKeys("नायिका");
          
        	WebUtility.Click(profileAwards);
        	profileAwards.sendKeys(Keys.CONTROL + "a");
        	profileAwards.sendKeys(Keys.BACK_SPACE);
        	profileAwards.sendKeys("ऑस्कर");
          
        	WebUtility.Click(profileTrivia);
        	profileTrivia.sendKeys(Keys.CONTROL + "a");
        	profileTrivia.sendKeys(Keys.BACK_SPACE);
        	profileTrivia.sendKeys("ऑस्कर");

  		js.executeScript("window.scrollTo(0,0);");
  		WebUtility.Wait();
  		WebUtility.Click(doneButtonActive);
  		WebUtility.Wait();
  		HindiBtn.click();
  		WebUtility.Wait();
  		
  		castProfileBreadCrumb.click();
  		WebUtility.Wait();
  		*/

  	}
  	public void CreateMovie() throws InterruptedException {
  		//TODO ---- Update when Roles is fixed ---------
  		/*
  		//create movie through content creator
  		logoutlink.click();
  		logoutbutton.click();
  		WebUtility.clear_cache();
  		driver.get(prop.getProperty("url")); 
  		loginPage1.gmailLogin(prop.getProperty("Gusername1"),prop.getProperty("Gpassword"));
  		*/
  		WebUtility.Wait();
  		WebUtility.Click(dashboard);
  		WebUtility.Click(movies);
  		WebUtility.Wait();
  	
  		// Click on Create Movies Button 
  		WebUtility.javascript(createmoviebtn);
  		
  		// Content Properties And Classification 
  		WebUtility.Wait();
  		WebUtility.element_to_be_clickable(note);
  		externalID = js.executeScript("return arguments[0].childNodes[1].innerText", movieExternalID).toString();
  		note.sendKeys("New Note");
  		WebUtility.Wait();
  		WebUtility.Click(title);
  		title.sendKeys(Keys.CONTROL + "a");
  		title.sendKeys(Keys.DELETE);
  		title.sendKeys(movieTitle);
  		WebUtility.Wait();
  		WebUtility.element_to_be_clickable(shortdesc);
  		shortdesc.sendKeys("Tanu Weds Manu is a 2011 Indian Hindi-language romantic comedy-drama by Aanand L. Rai, and produced by Shailesh R Singh.");
  		WebUtility.element_to_be_clickable(WebDesc);
  		WebDesc.sendKeys("Tanu Weds Manu is a good movies");
  		WebUtility.element_to_be_clickable(AppDesc);
  		AppDesc.sendKeys("Tanu Weds Manu is a good movies");
  		WebUtility.element_to_be_clickable(TvDesc);
  		TvDesc.sendKeys("Tanu Weds Manu is a good movies");
  		WebUtility.element_to_be_visible(subtype);
  		subtype.click();
  		WebUtility.Click(dropDownList.get(0));
  		WebUtility.element_to_be_clickable(ContentCategory);
  		ContentCategory.click();
  		WebUtility.Click(dropDownList.get(0));
  		WebUtility.element_to_be_clickable(PrimaryGenre); 
  		PrimaryGenre.click();
  		WebUtility.Click(dropDownList.get(0));
  		WebUtility.element_to_be_clickable(SecondaryGen); 
  		SecondaryGen.click();
  		WebUtility.Click(dropDownList.get(0));
  		WebUtility.element_to_be_clickable(ThematicGen);
  		ThematicGen.click();
  		dropDownList.get(0).click();
  		WebUtility.element_to_be_clickable(SettingGen);
  		SettingGen.click();
  		WebUtility.Click(dropDownList.get(0));
  		WebUtility.element_to_be_clickable(RcsCategory);
  		RcsCategory.click();
  		WebUtility.Click(dropDownList.get(0));
  		CreativeTitle.sendKeys("New Creative Title");
  		WebUtility.element_to_be_clickable(AlternateTitle);
  		AlternateTitle.sendKeys("Alternate Title");
  		WebUtility.element_to_be_clickable(PageTitle);
  		PageTitle.sendKeys("Page title new");
  		WebUtility.element_to_be_clickable(PageDesc);
  		PageDesc.sendKeys("Page Description new title");
  		SocialTitle.sendKeys("New Social title");
  		WebUtility.element_to_be_clickable(SocialDesc);
  		SocialDesc.sendKeys("Social Description new title");
  		
//  		ZeeReleaseDate.sendKeys("30042021");   	//Date format is DD/MM/YYYY

  		WebUtility.Click(ZeeReleaseDate);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ESCAPE);
  		
//  		telecastDate.sendKeys("30042021"); 		//Date format is DD/MM/YYYY
  	
  		WebUtility.Click(telecastDate);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ESCAPE);
  		
  		//Video Duration  -----------------------------------
        WebUtility.Click(VideoDuration);
  		List<WebElement> videoduration = driver.findElements(By.xpath("//li[@role='button']"));
  		for(WebElement e9:videoduration)
  		{
  			String videohrs= e9.getText();
  			if(videohrs.contains("04"))
  			{
  				e9.click();
  				break;
  			}
  		}
  		
  		Thread.sleep(1000);
  		WebUtility.javascript(VideoDuration);

  		WebUtility.javascript(MultiAudio);
  		Thread.sleep(2000);

  		//WebUtility.javascript(AudioLanguage);
  	    WebUtility.Click(AudioLanguage);
  		WebUtility.Wait();
  		WebUtility.Click(dropDownList.get(0));
  		WebUtility.element_to_be_clickable(ContentLanguage);
  		ContentLanguage.click();
  		WebUtility.Click(dropDownList.get(0));

  		PrimaryLanguage.click();
  		WebUtility.Wait();
  		WebUtility.Click(dropDownList.get(0));

  		DubbedLanguage.click();
  		WebUtility.Wait();
  		WebUtility.Click(dropDownList.get(0));

  		OriginalLanguage.click();
  		WebUtility.Wait();
  		WebUtility.Click(dropDownList.get(0));
  		
  		SubtitleLanguage.click();
  		WebUtility.Wait();
  		WebUtility.Click(dropDownList.get(0));

  		ContentVersion.click();
  		WebUtility.Wait();
  		WebUtility.Click(dropDownList.get(0));
  		
  		MovieTheme.click();
  		WebUtility.Wait();
  		WebUtility.Click(dropDownList.get(0));
  		
  		ContractId.sendKeys("New Contract id");

  		WebUtility.element_to_be_clickable(SpecialCategory); //From Date format "DD/MM/YYY HH:MM"
  		WebUtility.Click(SpecialCategory);
  		WebUtility.Wait();
  		WebUtility.Click(dropDownList.get(0));
  		
		WebUtility.element_to_be_clickable(SpecialCountry);
		WebUtility.Click(SpecialCountry);
		WebUtility.Wait();
		WebUtility.Click(dropDownList.get(0));

		WebUtility.Click(specialCategoryFrom);
		datePicker.sendKeys(Keys.ARROW_RIGHT);
		datePicker.sendKeys(Keys.ESCAPE);

		WebUtility.Click(specialCategoryTo);
		datePicker.sendKeys(Keys.ARROW_RIGHT);
		datePicker.sendKeys(Keys.ARROW_RIGHT);
		datePicker.sendKeys(Keys.ESCAPE);

		WebUtility.Click(Next);
  		js.executeScript("window.scrollTo(0,0)");
  		//Classification----------------------------------------			
  		WebUtility.Wait();
  		WebUtility.Click(ContentRating);
  		ContentRating.sendKeys(Keys.ARROW_DOWN);
  		dropDownList.get(0).click();
  		
  		WebUtility.Click(ContentOwner);
  		ContentOwner.sendKeys(Keys.ARROW_DOWN);
  		dropDownList.get(0).click();
  		
  		WebUtility.javascript(OriginalCountry);	
  		OriginalCountry.sendKeys(Keys.ARROW_DOWN);
  		dropDownList.get(0).click();
  		
  		UpcomingText.sendKeys("Upcoming Text New");
  		
  		WebUtility.javascript(ContentDescription);
  		ContentDescription.sendKeys(Keys.ARROW_DOWN);
  		dropDownList.get(0).click();
  		
  		WebUtility.Click(Certification);
  		Certification.sendKeys(Keys.ARROW_DOWN);
  		dropDownList.get(0).click();
  		
  		WebUtility.javascript(Moods);
  		Moods.sendKeys(Keys.ARROW_DOWN);
  		dropDownList.get(0).click();
  		
  		WebUtility.javascript(ContentGrade);
  		ContentGrade.sendKeys(Keys.ARROW_DOWN);
  		dropDownList.get(0).click();
  		
  		WebUtility.javascript(Era);
  		Era.sendKeys(Keys.ARROW_DOWN);
  		dropDownList.get(0).click();
  		
  		WebUtility.javascript(TargetAge);
  		TargetAge.sendKeys(Keys.ARROW_DOWN);
  		dropDownList.get(0).click();
  		
  		WebUtility.Click(TargetAudience);
  		WebUtility.Wait();
  		TargetAudience.sendKeys(Keys.ARROW_DOWN);		
  		dropDownList.get(0).click();  		

  		Tags.sendKeys("Nice Movie");
  		WebUtility.Wait();
  		Tags.sendKeys(Keys.ARROW_DOWN);
  		Tags.sendKeys(Keys.ENTER);

  		WebUtility.element_to_be_clickable(Digital);
  		Digital.sendKeys("Digital keyword");
  		
  		WebUtility.element_to_be_clickable(Adaption);
  		Adaption.sendKeys("adaption keywords");
  		
  		WebUtility.Click(Events);
  		Events.sendKeys("Events Text");
  		
  		WebUtility.Click(ProductionCompany);
  		WebUtility.Wait();
  		ProductionCompany.sendKeys(Keys.ARROW_DOWN);
  		ProductionCompany.sendKeys(Keys.ENTER);
  		
  		PopularityScore.sendKeys("5");
  		
  		Trivia.sendKeys("New Trivia keywords");  		
  		
  		OfficialSite.sendKeys("Official");
  		
  		AwardRecipient.sendKeys("Best Award");
  		
  		WebUtility.Wait();
  		AwardRecipient.sendKeys(Keys.ARROW_DOWN);
  		AwardRecipient.sendKeys(Keys.ENTER);
  		WebUtility.Click(AwardCategory);
  		
  		WebUtility.Wait();
  		AwardCategory.sendKeys(Keys.ARROW_DOWN);
  		AwardCategory.sendKeys(Keys.ARROW_DOWN);
  		AwardCategory.sendKeys(Keys.ENTER);	
  		AwardRecognition.sendKeys("Recognition of awards");
  		WebUtility.Click(NextBtn);
  		WebUtility.Wait();
  		
  		// Player Attributes -------------------------------------------
  		Skip.click();
  		StartTime.click();Thread.sleep(2000);
  		
  		List<WebElement> starttime = driver.findElements(By.xpath("//li[@role='button']"));
  		for(WebElement e9:starttime)
  		{
  			String starthrs= e9.getText();
  			if(starthrs.contains("02"))
  			{
  				e9.click();
  				break;
  			}
  		}

  		EndTime.click();
  		List<WebElement> endtime = driver.findElements(By.xpath("//li[@role='button']"));
  		for(WebElement e9:endtime)
  		{
  			String endhrs= e9.getText();
  			if(endhrs.contains("03"))
  			{
  				e9.click();
  				break;
  			}
  		}

  		RecapStart.click();
  		List<WebElement> recaptime = driver.findElements(By.xpath("//li[@role='button']"));
  		for(WebElement e9:recaptime)
  		{
  			String recaphrs= e9.getText();
  			if(recaphrs.contains("00"))
  			{
  				e9.click();
  				break;
  			}
  		}

  		RecapEnd.click();
  		List<WebElement> recapend = driver.findElements(By.xpath("//li[@role='button']"));
  		for(WebElement e9:recapend)
  		{  			
  			String endhrs= e9.getText();
  			if(endhrs.contains("01"))
  			{
  				e9.click();
  				break;
  			}
  		}

  		EndCredit.click();
  		List<WebElement> endcredit = driver.findElements(By.xpath("//li[@role='button']"));
  		for(WebElement e9:endcredit)
  		{
  			String endhrs= e9.getText();
  			if(endhrs.contains("02"))
  			{
  				e9.click();
  				break;
  			}
  		}

  		AdMarker.sendKeys("AdMarkers keywords");

  		SkipSong.click();
  		List<WebElement> skipsong = driver.findElements(By.xpath("//li[@role='button']"));
  		for(WebElement e9:skipsong)
  		{
  			String skip= e9.getText();
  			if(skip.contains("05"))
  			{
  				e9.click();
  				break;
  			}
  		}

  		SkipEnd.click();
  		List<WebElement> skipend = driver.findElements(By.xpath("//li[@role='button']"));
  		for(WebElement e9:skipend)
  		{
  			String skipendhrs= e9.getText();
  			if(skipendhrs.contains("06"))
  			{
  				e9.click();
  				break;
  			}
  		}

  		WebUtility.Click(NextControl);
  		Thread.sleep(1000);
  		WebUtility.Click(MarkDone);

  	}
  	
  	public void MoviesContentPropertiesAssertions() throws InterruptedException {

  		JavascriptExecutor js = (JavascriptExecutor) driver;

  		TitleSummarytab.click();
  		WebUtility.Wait();
  		String expectedTitle =movieTitle;
  		Assert.assertEquals(title.getAttribute("value"), expectedTitle, "Title Assertion Failed");

  		String expectedShortDescription = "Tanu Weds Manu is a 2011 Indian Hindi-language romantic comedy-drama by Aanand L. Rai, and produced by Shailesh R Singh.";
  		List<WebElement> shortDescValue = driver.findElements(By.className("ql-editor"));
  		Assert.assertEquals(shortDescValue.get(0).getAttribute("innerText"), expectedShortDescription,"Short Desc Assertion Fails");

  		js.executeScript("window.scrollTo(0,document.body.scrollHeight);");

  		Object subTypeDropDown = js.executeScript("var dropDown=document.getElementById(\"subtype\");if(dropDown.value==\"\")var dropDownValue=false;else var dropDownValue=true; return dropDownValue");Assert.assertEquals(subTypeDropDown, true, "Sub Type Assertion Fails");

  		Object contentCategoryDropDown = js.executeScript("var dropDown=document.getElementById(\"category\");if(dropDown.value==\"\")var dropDownValue=false;else var dropDownValue=true; return dropDownValue");
  		Assert.assertEquals(contentCategoryDropDown, true, "Content Category Assertion Fails");

  		Assert.assertNotNull(primaryGenreAssertion.getText(), "Primary Genre Dropdown is Empty");

  		Assert.assertNotNull(secondaryGenreAssertion.getText(), "Secondary Genre Dropdown is Empty");

  		Object zee5ReleaseDateValueExists = js.executeScript("var dropDown=document.getElementsByName(\"dateZee5Published\");if(dropDown[0].defaultValue==\"\")var dropDownValue=false;else var dropDownValue=true; return dropDownValue");
  		Assert.assertEquals(zee5ReleaseDateValueExists, true, "Zee5 Release Date Assertion Failed");

  		Object videoDurationValueExists = js.executeScript("var dropDown=document.getElementsByName(\"duration\");if(dropDown[0].defaultValue==\"\")var dropDownValue=false;else var dropDownValue=true; return dropDownValue");
  		Assert.assertEquals(videoDurationValueExists, true, "Video Duration Assertion Failed");

  		Assert.assertNotNull(audioLanguagesAssertion.getText(), "Audio Language Dropdown is Empty");

  	 	Object primaryLanguageDropDown = js.executeScript("var dropDown=document.getElementById(\"primaryLanguage\");if(dropDown.value==\"\")var dropDownValue=false;else var dropDownValue=true; return dropDownValue");
  		Assert.assertEquals(primaryLanguageDropDown, true, "Primary Language Assertion Fails");

  		Assert.assertNotNull(dubbedLanguageAssertion.getText(), "Dubbed Language Dropdown is Empty");

  		Object originalLanguageDropDown = js.executeScript("var dropDown=document.getElementById(\"originalLanguage\");if(dropDown.value==\"\")var dropDownValue=false;else var dropDownValue=true; return dropDownValue");
  		Assert.assertEquals(originalLanguageDropDown, true, "Original Language Assertion Fails");

  		Object contentVersionDropDown = js.executeScript("var dropDown=document.getElementById(\"contentVersion\");if(dropDown.value==\"\")var dropDownValue=false;else var dropDownValue=true; return dropDownValue");
  		Assert.assertEquals(contentVersionDropDown, true, "Content Version Assertion Fails");

  		nextButton.click();

  		js.executeScript("window.scrollTo(0,0);");
   		Object contentOwnerDropDown = js.executeScript("var dropDown=document.getElementById(\"contentOwner\");if(dropDown.value==\"\")var dropDownValue=false;else var dropDownValue=true; return dropDownValue");
  		Assert.assertEquals(contentOwnerDropDown, true, "Content Owner Assertion Fails");

  		Object certificationDropDown = js.executeScript("var dropDown=document.getElementById(\"certification\");if(dropDown.value==\"\")var dropDownValue=false;else var dropDownValue=true; return dropDownValue");
  		Assert.assertEquals(certificationDropDown, true, "Certification Assertion Fails");

  		String doneButtonClickedColor = doneButtonClicked.getCssValue("background-color");
  		sa.assertTrue(doneButtonClickedColor.contains("52, 194, 143"), "Content Properties Mark As Done Assertion Fails");
  	}

  	public void CastCrew() throws InterruptedException {
  	
  		WebUtility.javascript(CastCrew);
  		WebUtility.Wait();
  		
  		Actor1.sendKeys("Kangna Ranuat");
  		WebUtility.Wait();
  		Actor1.sendKeys(Keys.ARROW_DOWN);
  		Actor1.sendKeys(Keys.ENTER);
  		
  		character.sendKeys("Tanuja tanu trivedi");
  		WebUtility.Wait();
  		
  		Performer.sendKeys("Comedian");
  		Performer.sendKeys(Keys.ARROW_DOWN);
  		Performer.sendKeys(Keys.ENTER);
  		
  		Host.sendKeys("Prabhas");
  		Host.sendKeys(Keys.ARROW_DOWN);
  		Host.sendKeys(Keys.ENTER);
  		
  		Singer.sendKeys("Sonu Nigam");
  		Singer.sendKeys(Keys.ARROW_DOWN);
  		Singer.sendKeys(Keys.ENTER);
  		
  		Lyricist.sendKeys("Javed Akhtar");
  		Lyricist.sendKeys(Keys.ARROW_DOWN);
  		Lyricist.sendKeys(Keys.ENTER);
  		
  		Director.sendKeys("Aanand L Rai");
  		Director.sendKeys(Keys.ARROW_DOWN);
  		Director.sendKeys(Keys.ENTER);
  		
  		Dop.sendKeys("New Dop");
  		Dop.sendKeys(Keys.ARROW_DOWN);
  		Dop.sendKeys(Keys.ENTER);
  		
  		Producer.sendKeys("Karan Johar");
  		WebUtility.Wait();
  		Producer.sendKeys(Keys.ARROW_DOWN);
  		Producer.sendKeys(Keys.ENTER);
  		
  		ExecutivePrd.sendKeys("Karan");
  		ExecutivePrd.sendKeys(Keys.ARROW_DOWN);
  		ExecutivePrd.sendKeys(Keys.ENTER);
  		
  		MusicDirector.sendKeys("Anu Malik");
  		MusicDirector.sendKeys(Keys.ARROW_DOWN);
  		MusicDirector.sendKeys(Keys.ENTER);
  		
  		Choreographer.sendKeys("Saroj khan");
  		Choreographer.sendKeys(Keys.ARROW_DOWN);
  		Choreographer.sendKeys(Keys.ENTER);
  		
  		TitleTheme.sendKeys("New Title");
  		TitleTheme.sendKeys(Keys.ARROW_DOWN);
  		TitleTheme.sendKeys(Keys.ENTER);
  		
  		BackgroundScore.sendKeys("Background");
  		WebUtility.Wait();
  		BackgroundScore.sendKeys(Keys.ARROW_DOWN);
  		BackgroundScore.sendKeys(Keys.ENTER);
  		
  		StoryWriter.sendKeys("Javed");
  		StoryWriter.sendKeys(Keys.ARROW_DOWN);
  		StoryWriter.sendKeys(Keys.ENTER);
  		
  		ScreenPlay.sendKeys("ScreenPlay Test");
  		ScreenPlay.sendKeys(Keys.ARROW_DOWN);
  		ScreenPlay.sendKeys(Keys.ENTER);
  		
  		Dialogue.sendKeys("Javed");
  		Dialogue.sendKeys(Keys.ARROW_DOWN);
  		Dialogue.sendKeys(Keys.ENTER);
  		
  		FilmEditing.sendKeys("FilmEditing");
  		FilmEditing.sendKeys(Keys.ARROW_DOWN);
  		FilmEditing.sendKeys(Keys.ENTER);
  		
  		Casting.sendKeys("Casting");
  		Casting.sendKeys(Keys.ARROW_DOWN);
  		Casting.sendKeys(Keys.ENTER);
  		
  		ProdDesign.sendKeys("Production Design");
  		ProdDesign.sendKeys(Keys.ARROW_DOWN);
  		ProdDesign.sendKeys(Keys.ENTER);
  		
  		Art.sendKeys("Art new");
  		Art.sendKeys(Keys.ARROW_DOWN);
  		Art.sendKeys(Keys.ENTER);
  		
  		SetDecoration.sendKeys("SetDecoration");
  		SetDecoration.sendKeys(Keys.ARROW_DOWN);
  		SetDecoration.sendKeys(Keys.ENTER);
  		
  		Costume.sendKeys("Costume");
  		Costume.sendKeys(Keys.ARROW_DOWN);
  		Costume.sendKeys(Keys.ENTER);
  		
  		ProductionCo.sendKeys("Production Company");
  		ProductionCo.sendKeys(Keys.ARROW_DOWN);
  		ProductionCo.sendKeys(Keys.ENTER);
  		
  		Presenter.sendKeys("New presenter");
  		Presenter.sendKeys(Keys.ARROW_DOWN);
  		Presenter.sendKeys(Keys.ENTER);
  		
  		Guest.sendKeys("Guest new");
  		Guest.sendKeys(Keys.ARROW_DOWN);
  		Guest.sendKeys(Keys.ENTER);
  		
  		Participant.sendKeys("Participant");
  		Participant.sendKeys(Keys.ARROW_DOWN);
  		Participant.sendKeys(Keys.ENTER);
  		
  		Judges.sendKeys("Judge");
  		Judges.sendKeys(Keys.ARROW_DOWN);
  		Judges.sendKeys(Keys.ENTER);
  		
  		Narrator.sendKeys("Narrator");
  		Narrator.sendKeys(Keys.ARROW_DOWN);
  		Narrator.sendKeys(Keys.ENTER);
  		
  		Sponsor.sendKeys("Sponsor");
  		Sponsor.sendKeys(Keys.ARROW_DOWN);
  		Sponsor.sendKeys(Keys.ENTER);
  		
  		Vocalist.sendKeys("Vocalist");
  		Vocalist.sendKeys(Keys.ARROW_DOWN);
  		Vocalist.sendKeys(Keys.ENTER);

  		WebUtility.Click(CastMarkAsDone);
  		WebUtility.Wait();
  	
  	}
  	
  	public void MoviesCastCrewAssertions() throws InterruptedException {

          String expectedActor = "Kangna Ranuat";
          Assert.assertEquals(Actor1.getAttribute("value"), expectedActor, "Movie Main Content Cast & Crew Actor Assertion Failed");

          String expectedCharacter = "Tanuja tanu trivedi";
          Assert.assertEquals(character.getAttribute("value"), expectedCharacter, "Cast & Crew Character Assertion Failed");

          String doneButtonClickedColor = doneButtonClicked.getCssValue("background-color");
          sa.assertTrue(doneButtonClickedColor.contains("52, 194, 143"), "Movie Main Content Cast & Crew Mark As Done Assertion Fails");
      }

  	public void Video() throws InterruptedException {
  	
  		WebUtility.javascript(Videos);
  		WebUtility.Wait();
  		
  		Media.sendKeys(Keys.CONTROL + "a");
  		Media.sendKeys(Keys.BACK_SPACE);
  		Media.sendKeys("Kumkum_Bhagya_2_Episode_04_May_2021_bn");
        
        importDetails.click();
      //  WebUtility.Click(yes.get(0));
      //  WebUtility.Click(ok);
        WebUtility.Wait();
        
        WebUtility.Click(AudioTrack);
        AudioTrack.sendKeys(Keys.CONTROL + "a");
        AudioTrack.sendKeys(Keys.BACK_SPACE);
        AudioTrack.sendKeys("Episode Test Data");

        WebUtility.Click(VideoSubType);
        VideoSubType.sendKeys(Keys.CONTROL + "a");
        VideoSubType.sendKeys(Keys.BACK_SPACE);
        VideoSubType.sendKeys("Episode Test Data");
        
        WebUtility.Click(Size);
        Size.sendKeys(Keys.CONTROL + "a");
        Size.sendKeys(Keys.BACK_SPACE);
        Size.sendKeys("Episode Test Data");
        WebUtility.Wait();
        
        WebUtility.Click(Hls);
        WebUtility.Click(dropDownList.get(0));        

  		WebUtility.Wait();
  		WebUtility.Click(VideosMarkDone);
  		WebUtility.Wait();		
  		
        WebUtility.Click(contentProperties);
        WebUtility.Wait();
        
        title.sendKeys(Keys.CONTROL + "a");
        title.sendKeys(Keys.DELETE);
        title.sendKeys(movieTitle);
        
        WebUtility.Click(note);
        WebUtility.Click(doneButtonActive);
        WebUtility.Wait();
        
    	WebUtility.Click(Videos);
  		WebUtility.Wait();
  	}

  	public void MoviesVideoSectionAssertions() throws InterruptedException {	
      	/*
          String expectedDashRootFolder = "new test";
          Assert.assertEquals(DashRoot.getAttribute("value"), expectedDashRootFolder, "Dash Root Folder Assertion Failed");

          String expectedDashManifest = "Dash";
          Assert.assertEquals(DashManifest.getAttribute("value"), expectedDashManifest, "Dash Manifest Assertion Failed");

          String expectedHlsRootFolder = "HLS root";
          Assert.assertEquals(HlsRoot.getAttribute("value"), expectedHlsRootFolder, "HLS Root Folder Assertion Failed");

          String expectedHlsManifest = "Hls manifest";
          Assert.assertEquals(HlsManifest.getAttribute("value"), expectedHlsManifest, "HLS Manifest Assertion Failed");
*/
          String doneButtonClickedColor = doneButtonClicked.getCssValue("background-color");
          sa.assertTrue(doneButtonClickedColor.contains("52, 194, 143"), "Movie Main Content Video Section Mark As Done Assertion Fails");
          
      }


  	public void License() throws InterruptedException {
  
  		try
  		{
		WebUtility.javascript(License);
		WebUtility.Wait();
		WebUtility.javascript(CreateLicense);
		WebUtility.Wait();
		licenseSetName.sendKeys("Automation Test Data");

		WebUtility.Click(licenseFromDate);
		WebUtility.Wait();
		datePicker.sendKeys(Keys.ARROW_RIGHT);
		datePicker.sendKeys(Keys.ESCAPE);

		WebUtility.Click(licenseToDate);
		WebUtility.Wait();
		datePicker.sendKeys(Keys.ARROW_RIGHT);
		datePicker.sendKeys(Keys.ARROW_RIGHT);
		datePicker.sendKeys(Keys.ESCAPE);

		WebUtility.Click(Country);
        for (int x = 0; x < dropDownList.size(); x++) {
        	if(x<4) {
            WebUtility.Click(dropDownList.get(x));
        	}
        	else {
        		break;
        	}
        }

        WebUtility.Click(BusinessType);
		WebUtility.Wait();
		dropDownList.get(0).click();

		WebUtility.Click(licensePlatform);
		WebUtility.Click(dropDownList.get(0));
		
		WebUtility.Click(licenseTVOD);
		WebUtility.Click(dropDownList.get(0));
		
        WebUtility.Click(licenseContentAgeRatingId);	
        WebUtility.Wait();
        licenseContentAgeRatingId.sendKeys(Keys.ARROW_DOWN);
        licenseContentAgeRatingId.sendKeys(Keys.ENTER);
        
        WebUtility.Click(SaveLicense);
		WebUtility.Wait();
	}
      catch(Exception e)
      {
      	System.out.println("Creating a Manual License Failed");
      	e.printStackTrace();
      }
  		
  		// -------------------------USE TEMPLATE ---------------------------------------
  	 try
		{
			WebUtility.javascript(CreateLicense);
			WebUtility.Wait();
			WebUtility.javascript(UseTemplate);
			WebUtility.Wait();
			WebUtility.Click(TemplateDropdown);
			WebUtility.Wait();
			WebUtility.Click(dropDownList.get(0));
			
			try {
				int licenseTemplateSize = licenseTemplateDeleteSets.size();
				for(int i = 1;i< licenseTemplateSize ;i++)
				{
					System.out.println("License Sets Size--->"+licenseTemplateDeleteSets.size());
					licenseTemplateDeleteSets.get(i).click();
					Thread.sleep(1000);
					System.out.println("License Sets Size After Delete--->"+licenseTemplateDeleteSets.size());
				}
			}
			catch (Exception e){
				System.out.println("No Sets in License Template");
			}
			js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
			
			WebUtility.Click(editLicense);
			WebUtility.Wait();
			UseTemplateCountryEdit.click();
			WebUtility.Click(clearInput);
	        for (int x = 0; x < dropDownList.size(); x++) {
	        	if(x<4) {
	            WebUtility.Click(dropDownList.get(x));
	        	}
	        	else {
	        		break;
	        	}
	        }
			WebUtility.Wait();
			WebUtility.Click(UseTemplateCountryEdit);
			WebUtility.Wait();
			WebUtility.javascript(SaveLicense);
			WebUtility.Wait();
			licenseSetName.sendKeys("Automation Sample Data");
			WebUtility.Wait();
			
	        WebUtility.Click(licenseContentAgeRatingId);	
	        WebUtility.Wait();
	        licenseContentAgeRatingId.sendKeys(Keys.ARROW_DOWN);
	        licenseContentAgeRatingId.sendKeys(Keys.ENTER);
	        
			WebUtility.Click(licenseFromDate);
			WebUtility.Wait();
			datePicker.sendKeys(Keys.ARROW_RIGHT);
			datePicker.sendKeys(Keys.ARROW_RIGHT);
			datePicker.sendKeys(Keys.ARROW_RIGHT);
			datePicker.sendKeys(Keys.ESCAPE);

			WebUtility.Click(licenseToDate);
			WebUtility.Wait();
			datePicker.sendKeys(Keys.ARROW_RIGHT);
			datePicker.sendKeys(Keys.ARROW_RIGHT);
			datePicker.sendKeys(Keys.ARROW_RIGHT);
			datePicker.sendKeys(Keys.ARROW_RIGHT);
			datePicker.sendKeys(Keys.ESCAPE);

			WebUtility.Click(SaveLicense);
			WebUtility.Wait();
		}
       catch(Exception e)
       {
       	System.out.println("Creating a license through template Failed!!");
       	e.printStackTrace();
       }
  	
          String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
          System.out.println("Mark as Done Button Color ------>"+doneButtonActiveColor );
          System.out.println("Mark as Done Button Color ------>"+doneButtonActiveColor.contains("255, 255, 255") );
          sa.assertTrue(doneButtonActiveColor.contains("255,255,255") || doneButtonActiveColor.contains("255, 255, 255"), "Movie Main Content License Mark As Done Assertion Fails");
          WebUtility.Click(doneButtonActive);
          WebUtility.Wait();

          try {
              TestUtil.captureScreenshot("Movie Main Content License Section","Movie");
          } catch (IOException e) {
              System.out.println("Screenshot Capture Failed ->" + e.getMessage());
          }
       
          WebUtility.Click(expiredLicense);
          WebUtility.Wait();
          WebUtility.Click(expiredLicenseBack);
          WebUtility.Wait();
          
          WebUtility.Click(licenseFilters);
          WebUtility.Click(licenseFromDate);
          datePicker.sendKeys(Keys.ARROW_LEFT);
          datePicker.sendKeys(Keys.ARROW_LEFT);
          datePicker.sendKeys(Keys.ESCAPE);

          WebUtility.Click(licenseFilterToDate);
          datePicker.sendKeys(Keys.ARROW_RIGHT);
          datePicker.sendKeys(Keys.ARROW_RIGHT);
          datePicker.sendKeys(Keys.ESCAPE);
          WebUtility.Click(ApplyFilter);

          Thread.sleep(2000);
          WebUtility.Click(licenseFilters);
          WebUtility.Click(ClearFilter);

          WebUtility.Click(licenseFilters);
          Thread.sleep(1000);
          licenseFilterBusinessType.click();
          WebUtility.Click(dropDownList.get(0));
          WebUtility.Click(ApplyFilter);

          Thread.sleep(2000);
          WebUtility.Click(licenseFilters);
          WebUtility.Click(ClearFilter);

          WebUtility.Click(licenseFilters);
          Thread.sleep(1000);
          licenseFilterBillingType.click();
          WebUtility.Click(dropDownList.get(0));
          WebUtility.Click(ApplyFilter);

          Thread.sleep(2000);
          WebUtility.Click(licenseFilters);
          WebUtility.Click(ClearFilter);

          WebUtility.Click(licenseFilters);
          Thread.sleep(1000);
          licenseFilterPlatform.click();
          WebUtility.Click(dropDownList.get(0));
          Thread.sleep(1000);
          filterCloseDropdown.click();
          WebUtility.Click(ApplyFilter);

          Thread.sleep(2000);
          WebUtility.Click(licenseFilters);
          WebUtility.Click(ClearFilter);
          
          WebUtility.Click(licenseFilters);
          Thread.sleep(1000);
          licenseContentAgeRatingId.click();
          WebUtility.Click(dropDownList.get(0));
          WebUtility.Click(ApplyFilter);

          Thread.sleep(2000);
          WebUtility.Click(licenseFilters);
          WebUtility.Click(ClearFilter);
          
          WebUtility.Click(licenseFilters);
          Thread.sleep(1000);
          licenseFilterActiveStatus.click();
          WebUtility.Click(ApplyFilter);

          Thread.sleep(2000);
          WebUtility.Click(licenseFilters);
          WebUtility.Click(ClearFilter);

          WebUtility.Click(licenseFilters);
          Thread.sleep(1000);
          licenseFilterInActiveStatus.click();
          WebUtility.Click(ApplyFilter);

          Thread.sleep(2000);
          WebUtility.Click(licenseFilters);
          WebUtility.Click(ClearFilter);

  	}
  	
	public void Images() throws InterruptedException {

		WebUtility.Wait();
		WebUtility.Click(Images);
		WebUtility.Wait();
		try {
			Cover.sendKeys(System.getProperty("user.dir") + "\\images\\movies\\Tanuwedsmanu1.jpg");
		} catch (Exception e) {
			Cover.sendKeys("/dev/test/images/movies/Tanuwedsmanu1.jpg");
		}	
		
		WebUtility.Wait();
		try {
			AppCover.sendKeys(System.getProperty("user.dir") + "\\images\\movies\\Tanuwedsmanu2.jpg");
		} catch (Exception e) {
			AppCover.sendKeys("/dev/test/images/movies/Tanuwedsmanu2.jpg");
		}	
		WebUtility.Wait();

		try {	        
			List.sendKeys(System.getProperty("user.dir") + "\\images\\movies\\Tanuwedsmanu3.jpg");
	        WebUtility.Wait();
		}
		catch(Exception e)
		{
			
			try {
				List.sendKeys("/dev/test/images/movies/Tanuwedsmanu3.jpg");
			} catch (Exception e2) {
				//List Image Successfully imported from UID
			}
			
		}
		WebUtility.Wait();
		
		try {
			Square.sendKeys(System.getProperty("user.dir") + "\\images\\movies\\Tanuwedsmanu4.jpg");
		} catch (Exception e) {
			Square.sendKeys("/dev/test/images/movies/Tanuwedsmanu4.jpg");
		}
		WebUtility.Wait();
		
		js.executeScript("window.scrollBy(0,600)");		
		try {
			TVCover.sendKeys(System.getProperty("user.dir") + "\\images\\movies\\Tanuwedsmanu5.jpg");
		} catch (Exception e) {
			TVCover.sendKeys("/dev/test/images/movies/Tanuwedsmanu5.jpg");
		}
		WebUtility.Wait();

		try {
			Portrait.sendKeys(System.getProperty("user.dir") + "\\images\\movies\\Tanuwedsmanu6.jpg");
		} catch (Exception e) {
			Portrait.sendKeys("/dev/test/images/movies/Tanuwedsmanu6.jpg");
		}
		WebUtility.Wait();

		try {
			Listclean.sendKeys(System.getProperty("user.dir") + "\\images\\movies\\Tanuwedsmanu6.jpg");
		} catch (Exception e) {
			Listclean.sendKeys("/dev/test/images/movies/Tanuwedsmanu6.jpg");
		}
		WebUtility.Wait();

		js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
		try {
			Portraitclean.sendKeys(System.getProperty("user.dir") + "\\images\\movies\\Tanuwedsmanu6.jpg");
		} catch (Exception e) {
			Portraitclean.sendKeys("/dev/test/images/movies/Tanuwedsmanu6.jpg");
		}
		WebUtility.Wait();

		try {
			Telcosquare.sendKeys(System.getProperty("user.dir") + "\\images\\movies\\Tanuwedsmanu6.jpg");
		} catch (Exception e) {
			Telcosquare.sendKeys("/dev/test/images/movies/Tanuwedsmanu6.jpg");
		}
		WebUtility.Wait();
		try {
			Passport.sendKeys(System.getProperty("user.dir") + "\\images\\movies\\Tanuwedsmanu6.jpg");
		} catch (Exception e) {
			Passport.sendKeys("/dev/test/images/movies/Tanuwedsmanu6.jpg");
		}
		WebUtility.Wait();

		// ---------------------------Images Set 2
		// ----------------------------------------------
		// ---------------------------Images Set 2
		// ----------------------------------------------
		try {
			WebUtility.element_to_be_fluentwait(imagesCreateNewSet);
			WebUtility.Click(imagesCreateNewSet);

			WebUtility.Click(newSetName);
			newSetName.sendKeys("Automation Test Data");

			newSetCountry.click();
			WebUtility.Click(dropDownList.get(0));
			closeDropdown.click();

			newSetPlatform.click();
			WebUtility.Click(dropDownList.get(0));
			closeDropdown.click();

			newSetGender.click();
			WebUtility.Click(dropDownList.get(0));
			closeDropdown.click();

			newSetGenre.click();
			WebUtility.Click(dropDownList.get(0));
			closeDropdown.click();

			newSetAgeGroup.click();
			WebUtility.Click(dropDownList.get(0));
			closeDropdown.click();

			newSetLanguage.click();
			WebUtility.Click(dropDownList.get(0));
			closeDropdown.click();

			newSetOthers.click();
			newSetOthers.sendKeys("Automation Test Data");

			WebUtility.Click(save);
			WebUtility.Wait();

			WebUtility.Click(imageSets.get(1));
			WebUtility.Wait();

			js.executeScript("window.scrollTo(0,0)");
			WebUtility.Wait();

			try {
				Cover.sendKeys(System.getProperty("user.dir") + "\\images\\movies\\Tanuwedsmanu1.jpg");
			} catch (Exception e) {
				Cover.sendKeys("/dev/test/images/movies/Tanuwedsmanu1.jpg");
			}	
			
			WebUtility.Wait();
			try {
				AppCover.sendKeys(System.getProperty("user.dir") + "\\images\\movies\\Tanuwedsmanu2.jpg");
			} catch (Exception e) {
				AppCover.sendKeys("/dev/test/images/movies/Tanuwedsmanu2.jpg");
			}	
			WebUtility.Wait();

			try {	        
				List.sendKeys(System.getProperty("user.dir") + "\\images\\movies\\Tanuwedsmanu3.jpg");
		        WebUtility.Wait();
			}
			catch(Exception e)
			{
				
				try {
					List.sendKeys("/dev/test/images/movies/Tanuwedsmanu3.jpg");
				} catch (Exception e2) {
					//List Image Successfully imported from UID
				}
				
			}
			WebUtility.Wait();
			
			try {
				Square.sendKeys(System.getProperty("user.dir") + "\\images\\movies\\Tanuwedsmanu4.jpg");
			} catch (Exception e) {
				Square.sendKeys("/dev/test/images/movies/Tanuwedsmanu4.jpg");
			}
			WebUtility.Wait();
			
			js.executeScript("window.scrollBy(0,600)");		
			try {
				TVCover.sendKeys(System.getProperty("user.dir") + "\\images\\movies\\Tanuwedsmanu5.jpg");
			} catch (Exception e) {
				TVCover.sendKeys("/dev/test/images/movies/Tanuwedsmanu5.jpg");
			}
			WebUtility.Wait();

			try {
				Portrait.sendKeys(System.getProperty("user.dir") + "\\images\\movies\\Tanuwedsmanu6.jpg");
			} catch (Exception e) {
				Portrait.sendKeys("/dev/test/images/movies/Tanuwedsmanu6.jpg");
			}
			WebUtility.Wait();

			try {
				Listclean.sendKeys(System.getProperty("user.dir") + "\\images\\movies\\Tanuwedsmanu6.jpg");
			} catch (Exception e) {
				Listclean.sendKeys("/dev/test/images/movies/Tanuwedsmanu6.jpg");
			}
			WebUtility.Wait();

			js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
			try {
				Portraitclean.sendKeys(System.getProperty("user.dir") + "\\images\\movies\\Tanuwedsmanu6.jpg");
			} catch (Exception e) {
				Portraitclean.sendKeys("/dev/test/images/movies/Tanuwedsmanu6.jpg");
			}
			WebUtility.Wait();

			try {
				Telcosquare.sendKeys(System.getProperty("user.dir") + "\\images\\movies\\Tanuwedsmanu6.jpg");
			} catch (Exception e) {
				Telcosquare.sendKeys("/dev/test/images/movies/Tanuwedsmanu6.jpg");
			}
			WebUtility.Wait();
			try {
				Passport.sendKeys(System.getProperty("user.dir") + "\\images\\movies\\Tanuwedsmanu6.jpg");
			} catch (Exception e) {
				Passport.sendKeys("/dev/test/images/movies/Tanuwedsmanu6.jpg");
			}
			WebUtility.Wait();
		} catch (Exception e) {
			e.printStackTrace();
		}

		// -------------------------- Image Set 3
		// -----------------------------------------------
		try {

			WebUtility.element_to_be_fluentwait(imagesCreateNewSet);
			WebUtility.Click(imagesCreateNewSet);

			WebUtility.Click(newSetName);
			newSetName.sendKeys("Automation Test Data II");

			newSetCountry.click();
			WebUtility.Click(dropDownList.get(1));
			closeDropdown.click();

			newSetPlatform.click();
			WebUtility.Click(dropDownList.get(1));
			closeDropdown.click();

			newSetGender.click();
			WebUtility.Click(dropDownList.get(1));
			closeDropdown.click();

			newSetGenre.click();
			WebUtility.Click(dropDownList.get(1));
			closeDropdown.click();

			newSetAgeGroup.click();
			WebUtility.Click(dropDownList.get(1));
			closeDropdown.click();

			newSetLanguage.click();
			WebUtility.Click(dropDownList.get(1));
			closeDropdown.click();

			newSetOthers.click();
			newSetOthers.sendKeys("Automation Test Data II");

			WebUtility.Click(save);
			WebUtility.Wait();

			WebUtility.Click(imageSets.get(2));
			js.executeScript("window.scrollTo(0,0)");
			WebUtility.Wait();

			try {
				Cover.sendKeys(System.getProperty("user.dir") + "\\images\\movies\\Tanuwedsmanu1.jpg");
			} catch (Exception e) {
				Cover.sendKeys("/dev/test/images/movies/Tanuwedsmanu1.jpg");
			}	
			
			WebUtility.Wait();
			try {
				AppCover.sendKeys(System.getProperty("user.dir") + "\\images\\movies\\Tanuwedsmanu2.jpg");
			} catch (Exception e) {
				AppCover.sendKeys("/dev/test/images/movies/Tanuwedsmanu2.jpg");
			}	
			WebUtility.Wait();

			try {	        
				List.sendKeys(System.getProperty("user.dir") + "\\images\\movies\\Tanuwedsmanu3.jpg");
		        WebUtility.Wait();
			}
			catch(Exception e)
			{
				
				try {
					List.sendKeys("/dev/test/images/movies/Tanuwedsmanu3.jpg");
				} catch (Exception e2) {
					//List Image Successfully imported from UID
				}
				
			}
			WebUtility.Wait();
			
			try {
				Square.sendKeys(System.getProperty("user.dir") + "\\images\\movies\\Tanuwedsmanu4.jpg");
			} catch (Exception e) {
				Square.sendKeys("/dev/test/images/movies/Tanuwedsmanu4.jpg");
			}
			WebUtility.Wait();
			
			js.executeScript("window.scrollBy(0,600)");		
			try {
				TVCover.sendKeys(System.getProperty("user.dir") + "\\images\\movies\\Tanuwedsmanu5.jpg");
			} catch (Exception e) {
				TVCover.sendKeys("/dev/test/images/movies/Tanuwedsmanu5.jpg");
			}
			WebUtility.Wait();

			try {
				Portrait.sendKeys(System.getProperty("user.dir") + "\\images\\movies\\Tanuwedsmanu6.jpg");
			} catch (Exception e) {
				Portrait.sendKeys("/dev/test/images/movies/Tanuwedsmanu6.jpg");
			}
			WebUtility.Wait();

			try {
				Listclean.sendKeys(System.getProperty("user.dir") + "\\images\\movies\\Tanuwedsmanu6.jpg");
			} catch (Exception e) {
				Listclean.sendKeys("/dev/test/images/movies/Tanuwedsmanu6.jpg");
			}
			WebUtility.Wait();

			js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
			try {
				Portraitclean.sendKeys(System.getProperty("user.dir") + "\\images\\movies\\Tanuwedsmanu6.jpg");
			} catch (Exception e) {
				Portraitclean.sendKeys("/dev/test/images/movies/Tanuwedsmanu6.jpg");
			}
			WebUtility.Wait();

			try {
				Telcosquare.sendKeys(System.getProperty("user.dir") + "\\images\\movies\\Tanuwedsmanu6.jpg");
			} catch (Exception e) {
				Telcosquare.sendKeys("/dev/test/images/movies/Tanuwedsmanu6.jpg");
			}
			WebUtility.Wait();
			try {
				Passport.sendKeys(System.getProperty("user.dir") + "\\images\\movies\\Tanuwedsmanu6.jpg");
			} catch (Exception e) {
				Passport.sendKeys("/dev/test/images/movies/Tanuwedsmanu6.jpg");
			}
			WebUtility.Wait();
  		}
  		catch(Exception e)
  		{
  			e.printStackTrace();
  		}
  		
  		// ---------------------- Inactivate Image Set ------------------------
  		try {
  			js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
  			WebUtility.Wait();
  			WebUtility.Click(imageSetActiveButton);
  			WebUtility.Wait();
  			WebUtility.Click(dropDownList.get(0));
  			WebUtility.javascript(yes.get(0));
  			WebUtility.Wait();
  		}
          catch(Exception e) {
          	e.printStackTrace();
          }

  		//------------------------ Delete Image Set -----------------------------
  		try {
  			removeSet.get(1).click();
  			WebUtility.Wait();
  			yes.get(0).click();
  			WebUtility.Wait();
  		} 
  		catch (Exception e) {
  			e.printStackTrace();
  		}

		js.executeScript("window.scrollTo(0,0);");
		WebUtility.Wait();

		WebUtility.Click(doneButtonActive);

  	}

  	public void MoviesImagesAssertions() throws InterruptedException {

        WebUtility.Wait();
        WebUtility.Click(imageSets.get(0));
  		String coverName = "Tanuwedsmanu1", appCoverName = "Tanuwedsmanu2", listName = "Tanuwedsmanu3",squareName = "Tanuwedsmanu4", tvCoverName = "Tanuwedsmanu5", portraitName = "Tanuwedsmanu6";
  		boolean coverFound = false, appCoverFound = false, listFound = false, squareFound = false, tvCoverFound = false,portraitFound = false;
  		List<WebElement> imageCheck = driver.findElements(By.tagName("strong"));

  		for (int i = 0; i < imageCheck.size(); i++) {
  			if (imageCheck.get(i).getText().equals(coverName.toLowerCase()))
  				coverFound = true;

  			if (imageCheck.get(i).getText().equals(appCoverName.toLowerCase()))
  				appCoverFound = true;

  			if (imageCheck.get(i).getText().equals(listName.toLowerCase()))
  				listFound = true;

  			if (imageCheck.get(i).getText().equals(squareName.toLowerCase()))
  				squareFound = true;

  			if (imageCheck.get(i).getText().equals(tvCoverName.toLowerCase()))
  				tvCoverFound = true;

  			if (imageCheck.get(i).getText().equals(portraitName.toLowerCase()))
  				portraitFound = true;

  		}

  		Assert.assertEquals(coverFound, true, "Image Cover Assertion Failed");
  		Assert.assertEquals(appCoverFound, true, "Image App Cover Assertion Failed");
  		Assert.assertEquals(listFound, true, "Image List Assertion Failed");
  		Assert.assertEquals(squareFound, true, "Image Square Assertion Failed");
  		Assert.assertEquals(tvCoverFound, true, "Image TV Cover Assertion Failed");
  		Assert.assertEquals(portraitFound, true, "Image Portrait Assertion Failed");

  		String doneButtonClickedColor = doneButtonClicked.getCssValue("background-color");
  		sa.assertTrue(doneButtonClickedColor.contains("52, 194, 143"), "Movie Main Content Images Mark As Done Assertion Fails");
  	}

  	public void SEO() throws InterruptedException {
  		
  		WebUtility.javascript(Seo);
  		WebUtility.Wait();
  		WebUtility.Click(SeoTitle);
  		SeoTitle.sendKeys(Keys.CONTROL + "a");
  		SeoTitle.sendKeys(Keys.BACK_SPACE);
  		Thread.sleep(1000);
  		SeoTitle.sendKeys("Seo Test Title");
  		Thread.sleep(1000);
  		MetaDesc.click();
  		MetaDesc.sendKeys(Keys.CONTROL + "a");
  		MetaDesc.sendKeys(Keys.BACK_SPACE);
  		Thread.sleep(1000);
  		MetaDesc.sendKeys("Meta description");
  		Thread.sleep(1000);
  		MetaSynp.click();
  		MetaSynp.sendKeys(Keys.CONTROL + "a");
  		MetaSynp.sendKeys(Keys.BACK_SPACE);
  		Thread.sleep(1000);
  		MetaSynp.sendKeys("Meta data");
  		Thread.sleep(1000);

  		Redirection.sendKeys("301 Permanent Redirection");
  		List<WebElement> hls = driver.findElements(By.xpath("//li[@role='option']"));
  		System.out.println((hls.size()));
  			for(WebElement e9:hls)
  			{
  				String hlsnew= e9.getText();
  			if(hlsnew.contains("301 Permanent Redirection"))
  			{
  				e9.click();
  				break;	
  			}
  			}
  		Thread.sleep(1000);
  		Redirection.click();
  		RedirectionLink.sendKeys(Keys.CONTROL + "a");
  		RedirectionLink.sendKeys(Keys.BACK_SPACE);
  		Thread.sleep(1000);
  		RedirectionLink.sendKeys("http://www.redirection.com");
  		Thread.sleep(1000);

  		WebUtility.javascript(NoIndex);
  		Thread.sleep(1000);
  		H1.click();
  		H1.sendKeys(Keys.CONTROL + "a");
  		H1.sendKeys(Keys.BACK_SPACE);
  		Thread.sleep(1000);
  		H1.sendKeys("Main Test heading");
  		Thread.sleep(1000);

  		H2.click();
  		H2.sendKeys(Keys.CONTROL + "a");
  		H2.sendKeys(Keys.BACK_SPACE);
  		Thread.sleep(1000);
  		H2.sendKeys("Main Test heading");
  		Thread.sleep(1000);
  		Thread.sleep(1000);

  		H3.click();
  		H3.sendKeys(Keys.CONTROL + "a");
  		H3.sendKeys(Keys.BACK_SPACE);
  		Thread.sleep(1000);
  		H3.sendKeys("Heading3");
  		Thread.sleep(2000);

  		H4.click();
  		H4.sendKeys(Keys.CONTROL + "a");
  		H4.sendKeys(Keys.BACK_SPACE);
  		Thread.sleep(1000);
  		H4.sendKeys("MainTestheading");
  		Thread.sleep(1000);

  		H5.click();
  		H5.sendKeys(Keys.CONTROL + "a");
  		H5.sendKeys(Keys.BACK_SPACE);
  		Thread.sleep(1000);
  		H5.sendKeys("MainTest heading");
  		Thread.sleep(1000);

  		H6.click();
  		H6.sendKeys(Keys.CONTROL + "a");
  		H6.sendKeys(Keys.BACK_SPACE);
  		Thread.sleep(1000);
  		H6.sendKeys("MainTest heading");
  		Thread.sleep(1000);

  		RobotMeta.click();
  		RobotMeta.sendKeys(Keys.CONTROL + "a");
  		RobotMeta.sendKeys(Keys.BACK_SPACE);
  		Thread.sleep(1000);
  		RobotMeta.sendKeys("Main Robot meta data");
  		Thread.sleep(1000);

  		RobotNoIndex.click();
  		RobotNoIndex.sendKeys(Keys.CONTROL + "a");
  		RobotNoIndex.sendKeys(Keys.BACK_SPACE);
  		Thread.sleep(1000);
  		RobotNoIndex.sendKeys("Main Robot meta no");
  		Thread.sleep(1000);

  		MetaImage.click();
  		MetaImage.sendKeys(Keys.CONTROL + "a");
  		MetaImage.sendKeys(Keys.BACK_SPACE);
  		Thread.sleep(1000);
  		MetaImage.sendKeys("Main Robot meta no index data");
  		Thread.sleep(1000);

  		MetaImageNoIndex.click();
  		MetaImageNoIndex.sendKeys(Keys.CONTROL + "a");
  		MetaImageNoIndex.sendKeys(Keys.BACK_SPACE);
  		Thread.sleep(1000);
  		MetaImageNoIndex.sendKeys("Main Robot meta no index data");
  		Thread.sleep(1000);

  		BreadCrumb.click();
  		BreadCrumb.sendKeys(Keys.CONTROL + "a");
  		BreadCrumb.sendKeys(Keys.BACK_SPACE);
  		Thread.sleep(1000);
  		BreadCrumb.sendKeys("Internal link for mains movie");
  		Thread.sleep(1000);

  		InternalLink.click();
  		InternalLink.sendKeys(Keys.CONTROL + "a");
  		InternalLink.sendKeys(Keys.BACK_SPACE);
  		Thread.sleep(1000);
  		InternalLink.sendKeys("Internal link for mains movie");
  		Thread.sleep(1000);

  		Videoseo.click();
  		Website.click();
  		BreadcrumList.click();
  		Image.click();
  		Organization.click();
  		ImageGallery.click();
  		Link.click();

  		WebUtility.javascript(MarkAsDone);
  		WebUtility.Wait();
  	}
  	
  	public void MoviesSeoDetailsAssertions() throws InterruptedException {
  		
  	    js.executeScript("window.scrollTo(0,0);");
  	    String expectedTitleTag = "Seo Test Title";
  	    Assert.assertEquals(SeoTitle.getAttribute("value"), expectedTitleTag, "SEO Title Tag Assertion Failed");
  	       
  	    String doneButtonClickedColor = doneButtonClicked.getCssValue("background-color");
  	    sa.assertTrue(doneButtonClickedColor.contains("52, 194, 143"), "Movie Main Content SEO Details Mark As Done Assertion Fails");
  	}

  	public void Mapcontent() throws InterruptedException {
  	/*
  		logoutlink.click();
  		logoutbutton.click();
  		WebUtility.clear_cache();
  		driver.get(prop.getProperty("url")); 
  		loginPage1.gmailLogin(prop.getProperty("Gusername2"),prop.getProperty("Gpassword"));
  		WebUtility.Wait();
  		WebUtility.Click(movies);
  		WebUtility.Wait();
  		Search.sendKeys(movieTitle);
  		Thread.sleep(1000);
  		WebUtility.Wait();
  		WebUtility.Click(View);
  		WebUtility.Wait();

  		editMovie.click();
  		WebUtility.Wait();
*/
  		MapContent.click();
  		WebUtility.Wait();

  		WebUtility.javascript(AddContent);
  		WebUtility.Wait();

  		WebUtility.javascript(Checkbox);
  		WebUtility.Wait();

  		WebUtility.javascript(AssignContent);
  		WebUtility.Wait();

  		WebUtility.javascript(LinkContent);
  		WebUtility.Wait();
  		WebUtility.javascript(LinkAddContent);
  		WebUtility.Wait();
  		WebUtility.javascript(LinkCheckbox);
  		WebUtility.Wait();
  		WebUtility.javascript(LinkAssignContent);
  		WebUtility.Wait();

  		WebUtility.javascript(MapMarkAsDone);
  		WebUtility.Wait();
  		
  	}

      public void MovieMapContentAssertions() throws InterruptedException {
      	
          Object mappedContentExists = js.executeScript("var x = document.getElementsByClassName('whitebox list-profile-box flex justify-content-between'),contentMapped = false;if(x.length > 0 ) contentMapped = true; return contentMapped");
          Assert.assertEquals(mappedContentExists, true, "Mapped Content Assertion Failed");
        
          String doneButtonClickedColor = doneButtonClicked.getCssValue("background-color");
          sa.assertTrue(doneButtonClickedColor.contains("52, 194, 143"), "Movie Main Content Map Content Mark As Done Assertion Fails");
      }
  	
  	public void Quicklink() throws InterruptedException {
  	  		
  		WebUtility.javascript(relatedContent);
  		WebUtility.Wait();
  		 	
  		assignMovies.click();
  		WebUtility.javascript(addContent);
  		WebUtility.Wait();
  		WebUtility.Wait(); 
  		addContentCheckbox.get(0).click();
  		addContentCheckbox.get(1).click();
  		assignRelatedContentMovies.click();
  		WebUtility.Wait();
  		WebUtility.Click(deleteRelatedContent);
  		WebUtility.Wait();
  		WebUtility.element_to_be_visible(RemoveYesbtn);		
  		RemoveYesbtn.click();
  		WebUtility.Wait();
 
  		assignVideos.click();
  		WebUtility.Wait();
  		WebUtility.javascript(addContent);
  		WebUtility.Wait();
  		WebUtility.Wait();
  		addContentCheckbox.get(0).click();
  		addContentCheckbox.get(1).click();
  		assignRelatedContentVideos.click();
  		WebUtility.Wait();
  		WebUtility.Click(deleteRelatedContent);
  		WebUtility.Wait();
  		WebUtility.element_to_be_visible(RemoveYesbtn);		
  		RemoveYesbtn.click();
  		WebUtility.Wait();
  		
  	// --TODO --- Update when Roles is fixed -------------
  		/*
  		assignTvShows.click();
  		WebUtility.Wait();
  		WebUtility.javascript(addContent);
  		WebUtility.Wait();
  		addContentCheckbox.get(0).click();
  		addContentCheckbox.get(1).click();
  		Assignbtn.click();
  		WebUtility.Wait();
  		WebUtility.Click(deleteRelatedContent);
  		WebUtility.Wait();
  		WebUtility.element_to_be_visible(RemoveYesbtn);		
  		RemoveYesbtn.click();
  		WebUtility.Wait();
  	
  		assignSeasons.click();
  		WebUtility.Wait();
  		WebUtility.javascript(addContent);
  		WebUtility.Wait();
  		addContentCheckbox.get(0).click();
  		addContentCheckbox.get(1).click();
  		Assignbtn.click();
  		WebUtility.Wait();
  		WebUtility.Click(deleteRelatedContent);
  		WebUtility.Wait();
  		WebUtility.element_to_be_visible(RemoveYesbtn);		
  		RemoveYesbtn.click();
  		WebUtility.Wait();
  	
  		assignEpisodes.click();
  		WebUtility.Wait();
  		WebUtility.javascript(addContent);
  		WebUtility.Wait();
  		addContentCheckbox.get(0).click();
  		addContentCheckbox.get(1).click();
  		Assignbtn.click();
  		WebUtility.Wait();
  		WebUtility.Click(deleteRelatedContent);
  		WebUtility.Wait();
  		WebUtility.element_to_be_visible(RemoveYesbtn);		
  		RemoveYesbtn.click();
  		WebUtility.Wait();
  	*/
  		WebUtility.element_to_be_visible(MarkasDone);		
  		MarkasDone.click();
  		WebUtility.Wait();
  		WebUtility.element_to_be_visible(RelatedBackbtn);		
  		RelatedBackbtn.click();
  		WebUtility.Wait();
  		TranslationBtn.click();

  		WebUtility.element_to_be_visible(LanguageHeader);
  		WebUtility.Wait();
          
  		WebUtility.element_to_be_clickable(HindiBtn);
  		WebUtility.Wait();
          HindiBtn.click();
  		Translation_Form("तनु वेड्स मनु रिटर्न 5", "तनु वेड्स मनु, 2011 की भारतीय हिंदी-भाषा की रोमांटिक कॉमेडी-ड्रामा है, जिसका निर्माण अरण्ड एल राय द्वारा किया गया है" + 
  				"");
  		
  		WebUtility.element_to_be_clickable(MarathiBtn);
  		WebUtility.Wait();
          MarathiBtn.click();
          WebUtility.Wait();
  		Translation_Form("तनु वेड्स मनु रिटर्न 5", "तनु वेड्स मनु हे २०११ मधील हिंदी हिंदी भाषेतील रोमँटिक विनोदी नाटक आहे, ज्याचा आनंद आनंद एल राय यांनी केला असून शैलेश आर सिंग यांनी निर्मित केले. यात आर." + 
  				".");
  		
  	
  		WebUtility.Wait();
  		backToTranslations.click();
  		WebUtility.Wait();


  	}
  	private void Translation_Form(String Title, String Description) throws InterruptedException {
  		js = (JavascriptExecutor) driver;

  		TitleTextbox.sendKeys(Title);
  	
  		WebUtility.javascript(ShortDescriptionTextbox);
  		ShortDescriptionTextbox.sendKeys(Description);
  	
  		js.executeScript("arguments[0].scrollIntoView();", WebDescriptionTextbox);
  		WebDescriptionTextbox.click();
  		WebDescriptionTextbox.sendKeys(Description);
  	
  		AppDescriptionTextbox.sendKeys(Description);
  	
  		TvDescriptionTextbox.click();
  		TvDescriptionTextbox.sendKeys(Description);
  			
  		js.executeScript("arguments[0].scrollIntoView();", CreativeTitleTextbox);
  		CreativeTitleTextbox.sendKeys(Title);
  	
  		AlternativeTitleTextbox.sendKeys(Title);
  		
  		PageTitleTextbox.sendKeys(Title);
  		PageDescriptionTextbox.sendKeys(Description);
  	
  		SocialShareTitleTextbox.sendKeys(Title);
  		SocialShareDescriptionTextbox.sendKeys(Description);	
  	
  		js.executeScript("arguments[0].scrollIntoView();", UpcomingPageTextbox);
  		UpcomingPageTextbox.sendKeys(Title);
  	
  		TriviaTextbox.sendKeys(Title);	
  	
  		js.executeScript("arguments[0].scrollIntoView();", LanguageHeader);
  		WebUtility.Wait();
  		TitleTextbox.click();
  		WebUtility.Wait();
  		driver.findElement(By.xpath("//span[contains(text(),'Cast & Crew')]")).click();
  		WebUtility.Wait();
  		WebUtility.Click(actorName);
  		actorName.sendKeys(Keys.CONTROL + "a");
  		actorName.sendKeys(Keys.DELETE);
  		actorName.sendKeys("Kangna Ranuat");
  		characterName.sendKeys(Keys.CONTROL + "a");
  		characterName.sendKeys(Keys.DELETE);
  		characterName.sendKeys("Heroine");
  		WebUtility.Wait();
  		WebUtility.Click(doneButtonActive);
  		WebUtility.Wait();
  		driver.findElement(By.xpath("//span[contains(text(),'Content Properties')]")).click();	  	
  		WebUtility.Wait();

  	}

  	public void Checklist() throws InterruptedException {
  	//TODO ---- Update when Roles is fixed ---------
  		/*
  		logoutlink.click();
  		logoutbutton.click();
  		WebUtility.clear_cache();
  		driver.get(prop.getProperty("url")); 
  		loginPage1.gmailLogin(prop.getProperty("Gusername1"),prop.getProperty("Gpassword"));///login with content creator
  		WebUtility.Wait();
  		WebUtility.Click(movies);
  		WebUtility.Wait();
  		Search.sendKeys(movieTitle);
  		Thread.sleep(1000);
  		WebUtility.Wait();	
  		WebUtility.Click(View);
  		WebUtility.Wait();
  		WebUtility.javascript(editMovie);
  		WebUtility.Wait();
  
  		//--Verifying Movie Draft Status
  		try
  		{
  			contentProperties.click();
  			WebUtility.element_to_be_visible(contentStatus);
  			TestUtil.captureScreenshot("Movie Checklist Draft Status","Movie");
  			String expectedStatus = "Draft";
  			String actualStatus = contentStatus.getText();
  			Assert.assertEquals(actualStatus,expectedStatus,"Movie Draft Status is Failed");
  			Reporter.log("Movie Draft Status is Passed",true);	
  		}
  		catch (Exception e)
  		{
  			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
  		}
  		
  		WebUtility.javascript(Checklist);
  		WebUtility.Wait();
  		WebUtility.javascript(SubmitToReview);
  		WebUtility.Wait();
  		WebUtility.javascript(SubmitYesBtn);
  		WebUtility.Wait();
  		WebUtility.javascript(SubmitOk);
  		WebUtility.Wait();
  		
  		//--Verifying Movie Submit to Review status
  		try
  		{
  			contentProperties.click();
  			WebUtility.element_to_be_visible(contentStatus);
  			TestUtil.captureScreenshot("Movie Checklist Submitted To Review Status","Movie");
  			String expectedStatus = "Submitted To Review";
  			String actualStatus = contentStatus.getText();
  			Assert.assertEquals(actualStatus,expectedStatus,"Movie Submitted To Review Status is Failed");
  			Reporter.log("Movie Submitted To Review Status is Passed",true);	
  		}
  		catch (Exception e)
  		{
  			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
  		}
          
  		logoutlink.click();
  		logoutbutton.click();
  		WebUtility.clear_cache();
  		driver.get(prop.getProperty("url"));
  		loginPage1.gmailLogin(prop.getProperty("Gusername3"),prop.getProperty("Gpassword"));
  		WebUtility.Wait();
  		WebUtility.Click(movies);
  		WebUtility.Wait();
  		Search.sendKeys(movieTitle);
  		Thread.sleep(1000);
  		WebUtility.Wait();
  		WebUtility.Click(View);
  		WebUtility.Wait();	
  		editMovie.click();
  		WebUtility.Wait();
  		WebUtility.javascript(Checklist);
  		WebUtility.Wait();
  		NeedWorkReason.click();
  		List<WebElement> countrylist = driver.findElements(By.xpath("//li[@role='option']"));
  		System.out.println((countrylist.size()));
  		for(WebElement e22:countrylist)
  		{
  			System.out.println(e22.getText());
  			String countryname= e22.getText();
  			if(countryname.contains("Missing metadata"))
  			{
  				e22.click();
  				break;
  			}
  		}		

  		Thread.sleep(2000);
  		WebUtility.Click(NeedWorkbtn);
  		WebUtility.Wait();
  		WebUtility.Click(NeedWorkYes);
  		WebUtility.Wait();
  		WebUtility.Click(NeedWorkOk);
  		WebUtility.Wait();
  		
  		//--Verifying Movie Need Work status
  		try
  		{
  			contentProperties.click();
  			WebUtility.element_to_be_visible(contentStatus);
  			TestUtil.captureScreenshot("Movie Checklist Need Work Status","Movie");
  			String expectedStatus = "Need Work";
  			String actualStatus = contentStatus.getText();
  			Assert.assertEquals(actualStatus,expectedStatus,"Movie Need Work Status is Failed");
  			Reporter.log("Movie Need Work Status is Passed",true);	
  		}
  		catch (Exception e)
  		{
  			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
  		}
          
  		logoutlink.click();
  		logoutbutton.click();
  		WebUtility.clear_cache();
  		driver.get(prop.getProperty("url")); 
  		loginPage1.gmailLogin(prop.getProperty("Gusername1"),prop.getProperty("Gpassword"));
  		WebUtility.Wait();
  		WebUtility.Click(movies);
  		WebUtility.Wait();
  		Search.sendKeys(movieTitle);
  		Thread.sleep(1000);
  		WebUtility.Wait();
  		WebUtility.Click(View);
  		WebUtility.Wait();
  		editMovie.click();
  		WebUtility.Wait();
  		WebUtility.element_to_be_clickable(note);
  		note.sendKeys("s");
  		WebUtility.Wait();
  		NextBtn.click();	
  		WebUtility.Wait();
  		MarkAsDone.click();
  		WebUtility.Wait();
  		WebUtility.javascript(Checklist);
  		WebUtility.Wait();
  		WebUtility.javascript(SubmitToReview);
  		WebUtility.Wait();
  		WebUtility.javascript(SubmitYesBtn);
  		WebUtility.Wait();
  		WebUtility.javascript(SubmitOk);
  		WebUtility.Wait();

  		//--Verifying Movie Submit to Review status
  		try
  		{
  			contentProperties.click();
  			WebUtility.element_to_be_visible(contentStatus);
  			TestUtil.captureScreenshot("Movie Checklist Submitted To Review Status","Movie");
  			String expectedStatus = "Submitted To Review";
  			String actualStatus = contentStatus.getText();
  			Assert.assertEquals(actualStatus,expectedStatus,"Movie Submitted To Review Status is Failed");
  			Reporter.log("Movie Submitted To Review Status is Passed",true);	
  		}
  		catch (Exception e)
  		{
  			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
  		}
          
          logoutlink.click();
  		logoutbutton.click();
  		WebUtility.clear_cache();
  		driver.get(prop.getProperty("url")); 
  		loginPage1.gmailLogin(prop.getProperty("Gusername3"),prop.getProperty("Gpassword"));Thread.sleep(4000);
  		WebUtility.Wait();
  		WebUtility.Click(movies);
  		WebUtility.Wait();
  		Search.sendKeys(movieTitle);
  		Thread.sleep(1000);
  		WebUtility.Wait();
  		WebUtility.Click(View);
  		WebUtility.Wait();
  		editMovie.click();
  		WebUtility.Wait();
  		*/
		
  		WebUtility.Click(Checklist);
  		WebUtility.Wait();		 	

  		WebUtility.Click(PublishCountryDropdown);
        WebUtility.Click(dropDownList.get(0));
        /*
  		List<WebElement> countryList = driver.findElements(By.xpath("//li[@role='option']"));
  		for(WebElement countries:countryList)
  		{
  				countries.click();
  		}	
  		*/	
  				WebUtility.Click(PublishedBtn);
  				WebUtility.Wait();
  				WebUtility.Click(PublishedYes);
  				WebUtility.Wait();
  				WebUtility.Click(Published);
  				WebUtility.Wait();	

  		//--Verifying Movie Published status
  		try
  		{
  				contentProperties.click();
  				WebUtility.element_to_be_visible(contentStatus);
  				TestUtil.captureScreenshot("Movie Checklist Published Status","Movie");
  				String expectedStatus = "Published";
  				String actualStatus = contentStatus.getText();
  				Assert.assertEquals(actualStatus,expectedStatus,"Movie Published Status is Failed");
  				Reporter.log("Movie Published Status is Passed",true);	
  		}
  		catch (Exception e)
  		{
  				System.out.println("Screenshot Capture Failed ->" + e.getMessage());
  		}
          
  	}
  		
  	public void Workflow() throws InterruptedException {		
  		
  		WebUtility.Wait();
  		WebUtility.javascript(Checklist);
  		WebUtility.Wait();
  		//Unpublished the Published Movie ------------------------------------
  		WebUtility.Click(UnpublishContent);
  		WebUtility.Wait();    

  		Reason.click();
  		List<WebElement> countrylist = driver.findElements(By.xpath("//li[@role='option']"));
  		System.out.println((countrylist.size()));
  		for(WebElement e22:countrylist)
  		{
  			String countryname= e22.getText();
  			if(countryname.contains("On Hold"))
  			{
  				e22.click();
  				break;
  			}
  		}		

  		WebUtility.Click(YesBtn);
  		WebUtility.Wait();
  		WebUtility.Click(Yes);
  		WebUtility.Wait();
  		
  		//--Verifying Movie UnPublished status
  		try
  		{
  			contentProperties.click();
  			WebUtility.element_to_be_visible(contentStatus);
  			TestUtil.captureScreenshot("Movie Worflow Unpublished Status","Movie");
  			String expectedStatus = "Unpublished";
  			String actualStatus = contentStatus.getText();
  			sa.assertEquals(actualStatus,expectedStatus,"Movie Unpublished Status is Failed");
  			Reporter.log("Movie Unpublished Status is Passed",true);	
  		}
  		catch (Exception e)
  		{
  			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
  		}
          
          // Archive unpublished content -------------------------------------     
  		MoviesListing.click();
  		WebUtility.Wait();
  		Search.sendKeys(movieTitle);
  		Thread.sleep(1000);
  		WebUtility.Wait();
  		WebUtility.Wait();
  		Archive.get(0).click();	
  		ArchiveYes.click();
  		WebUtility.Wait();
  		Thread.sleep(5000);
  		WebUtility.Click(View);
  		WebUtility.Wait();
  		editMovie.click();
  		WebUtility.Wait();
  		
  		//--Verifying Movie Archive status
  		try
  		{
  			contentProperties.click();
  			WebUtility.element_to_be_visible(contentStatus);
  			TestUtil.captureScreenshot("Movie Worflow Archived Status","Movie");
  			String expectedStatus = "Archived";
  			String actualStatus = contentStatus.getText();
  			sa.assertEquals(actualStatus,expectedStatus,"Movie Archived Status is Failed");
  			Reporter.log("Movie Archived Status is Passed",true);	
  		}
  		catch (Exception e)
  		{
  			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
  		}
          
  		// Restore Content to draft mode ---------------------------------------
  		MoviesListing.click();
  		WebUtility.Wait();
  		Search.sendKeys(movieTitle);
  		Thread.sleep(1000);
  		WebUtility.Wait();
  		WebUtility.javascript(Restore);			
  		WebUtility.javascript(RestoreYes);
  		WebUtility.Wait();
  		WebUtility.Wait();
  		Thread.sleep(5000);
  		WebUtility.Click(View);
  		WebUtility.Wait();
  		editMovie.click();
  		WebUtility.Wait();
  		
  		//--Verifying Movie Restore Status
  		try
  		{
  			contentProperties.click();
  			WebUtility.element_to_be_visible(contentStatus);
  			TestUtil.captureScreenshot("Movie Worflow Restore Status","Movie");
  			String expectedStatus = "Draft";
  			String actualStatus = contentStatus.getText();
  			sa.assertEquals(actualStatus,expectedStatus,"Movie Restore Status is Failed");
  			Reporter.log("Movie Restore Status is Passed",true);	
  		}
  		catch (Exception e)
  		{
  			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
  		}
  		
  		Checklist.click();
  		WebUtility.Wait();

  		// Published Content ----------------------------------------------      
  		WebUtility.Click(PublishCountryDropdown);
        WebUtility.Click(dropDownList.get(0));
        /*
  		List<WebElement> countryList = driver.findElements(By.xpath("//li[@role='option']"));
  		for(WebElement countries:countryList)
  		{
  				countries.click();
  		}
  		*/
  		WebUtility.Wait();
  		WebUtility.Click(PublishedBtn);
  		WebUtility.Wait();
  		WebUtility.Click(PublishedYes);
  		WebUtility.Wait();
  		WebUtility.Click(Published);
  		WebUtility.Wait();
  		
  		//--Verifying Movie Published status
  		try
  		{
  			contentProperties.click();
  			WebUtility.element_to_be_visible(contentStatus);
  			TestUtil.captureScreenshot("Movie Worflow Published Status","Movie");
  			String expectedStatus = "Published";
  			String actualStatus = contentStatus.getText();
  			sa.assertEquals(actualStatus,expectedStatus,"Movie Published Status is Failed");
  			Reporter.log("Movie Published Status is Passed",true);	
  		}
  		catch (Exception e)
  		{
  			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
  		}
		//---------TODO -------Update when roles is fixed ---------------   
  		// Make some changes to republished the content ----------------------------
  		/*
  		logoutlink.click();
  		logoutbutton.click();
  		WebUtility.clear_cache();
  		driver.get(prop.getProperty("url")); 
  		loginPage1.gmailLogin(prop.getProperty("Gusername1"),prop.getProperty("Gpassword"));
  		WebUtility.Wait();
  		WebUtility.Click(movies);
  		WebUtility.Wait();
  		Search.sendKeys(movieTitle);
  		Thread.sleep(1000);
  		WebUtility.Wait();
  		WebUtility.Click(View);
  		editMovie.click();
  		*/
  		
  		WebUtility.element_to_be_clickable(note);
  		note.sendKeys("s");
  		WebUtility.Wait();
  		NextBtn.click();
  		WebUtility.Wait();
  		MarkAsDone.click();
  		WebUtility.Wait();
  		
  		//--Verifying Movie Changed status
  		try
  		{
  			contentProperties.click();
  			WebUtility.element_to_be_visible(contentStatus);
  			TestUtil.captureScreenshot("Movie Worflow Changed Status","Movie");
  			String expectedStatus = "Changed";
  			String actualStatus = contentStatus.getText();
  			sa.assertEquals(actualStatus,expectedStatus,"Movie Changed Status is Failed");
  			Reporter.log("Movie Changed Status is Passed",true);	
  		}
  		catch (Exception e)
  		{
  			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
  		}
  	//---------TODO -------Update when roles is fixed ---------------   
  		/*
  		logoutlink.click();
  		logoutbutton.click();
  		WebUtility.clear_cache();
  		driver.get(prop.getProperty("url")); 
  		loginPage1.gmailLogin(prop.getProperty("Gusername3"),prop.getProperty("Gpassword"));
  		WebUtility.Wait();
  		WebUtility.Click(movies);
  		WebUtility.Wait();
  		Search.sendKeys(movieTitle);
  		Thread.sleep(1000);
  		WebUtility.Wait();
  		WebUtility.Click(View);
  		WebUtility.Wait();
  		editMovie.click();
  		*/
  		WebUtility.Wait();

  		WebUtility.javascript(Checklist);
  		WebUtility.Wait();

  		js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
  		// Republished the content ------------------------------
  		WebUtility.javascript(Republished);            

  		WebUtility.javascript(RepublishedYes);

  		WebUtility.Click(yes.get(1));
  		WebUtility.Wait();
  		WebUtility.Click(PublishedAgain);
  		WebUtility.Wait();

  		//PublishedAgain.click();

  		//--Verifying Movie Republished status
  		try
  		{
  			contentProperties.click();
  			WebUtility.element_to_be_visible(contentStatus);
  			TestUtil.captureScreenshot("Movie Worflow Republished Status","Movie");
  			String expectedStatus = "Published";
  			String actualStatus = contentStatus.getText();
  			sa.assertEquals(actualStatus,expectedStatus,"Movie Republished Status is Failed");
  			Reporter.log("Movie Republished Status is Passed",true);	
  		}
  		catch (Exception e)
  		{
  			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
  		}
  		
  		PublishedHistory.click();
  		js.executeScript("window.scrollBy(0,1000)");
  		Thread.sleep(5000);
  		
  	}

  	public void Clone() throws InterruptedException {
  		

  		movieBreadcrumb.click();
  		WebUtility.Wait();
  		Search.sendKeys(Keys.CONTROL+"a");
  		Search.sendKeys(Keys.BACK_SPACE);
  		Search.sendKeys(movieTitle);
  		Thread.sleep(1000);
  		WebUtility.Wait();
  		WebUtility.Wait();	  
  		Clone.get(0).click();
  		WebUtility.Wait();
  		WebUtility.Click(CloneYes);
  		WebUtility.Wait();

  		WebUtility.javascript(Checklist);
  		js.executeScript("window.scrollTo(0,document.body.scrollHeight);");

  		WebUtility.Wait();
  		WebUtility.javascript(Scheduled_plus);
  		js.executeScript("window.scrollTo(0,document.body.scrollHeight);");  	
  		
  		WebUtility.Wait();
       // System.out.println("--------> Current Time = "+currentTime.getAttribute("innerText").substring(20, 28));
       // String currentTimeString  = currentTime.getAttribute("innerText").substring(20, 27);
    	//js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
  		//WebUtility.Wait();
  		
  		WebUtility.Click(ScheduledDate);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        WebUtility.Click(datePickerMinutes);
        actionProvider.moveToElement(datePickerMinutesClock).click().build().perform();      
        datePicker.sendKeys(Keys.ESCAPE);     
        
  		WebUtility.Click(ScheduledCountry);
        WebUtility.Click(dropDownList.get(0));
  		WebUtility.Wait();

  		WebUtility.javascript(ScheduledBtn);
  		WebUtility.Wait();
  		WebUtility.javascript(ScheduledYes);
  		WebUtility.Wait();
  		WebUtility.javascript(OkBtn);
  		WebUtility.Wait();
  		WebUtility.Wait();
 		
    	js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
        WebUtility.Wait();
        js.executeScript("window.scrollTo(0,0);");
        WebUtility.Wait();
        
     
        //System.out.println("--------> Scheduled Time = "+scheduledTime.getAttribute("innerText").substring(12, 20));
        //String scheduledTimeString = scheduledTime.getAttribute("innerText").substring(12, 20);
    	//js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
    	//sa.assertTrue(scheduledTimeString.contains(currentTimeString),"Movie Main Content - Current Time and Scheduled Time are not Equal");
    	
  		PublishedHistory.click();
  		js.executeScript("window.scrollBy(0,1000)");
  		WebUtility.Wait();
  		
  	}

  	public void Sort() throws InterruptedException {

  		dashboard.click();
  		WebUtility.Wait();
  		movies.click();
  		WebUtility.Wait();
  		
  		WebUtility.Click(Sort);		
  		AscToDesc.click();
  		ApplySort.click();
  		WebUtility.Wait();

  		Sort.click();
  		DescToAsc.click();
  		ApplySort.click();
  		WebUtility.Wait();
  		
  		Sort.click();
  		OldToNew.click();
  		ApplySort.click();
  		WebUtility.Wait();
  		
  		Sort.click();
  		NewToOld.click();
  		ApplySort.click();
  		WebUtility.Wait();


  	}


  	public void Filter () throws InterruptedException {

  		// Filter the list through different filter Options ------------------------
  		dashboard.click();
  		WebUtility.Wait();
  		movies.click();
  		WebUtility.Wait();
  		
  		Filter.click();
  		Draft.click();
  		ApplyFilter.click();
  		WebUtility.Wait();

  		Filter.click();
  		AllStatus.click();
  		ApplyFilter.click();
  		WebUtility.Wait();
  		
  		Filter.click();
  		Changed.click();
  		ApplyFilter.click();
  		WebUtility.Wait();

  		Filter.click();
  		PublishedStatus.click();
  		ApplyFilter.click();
  		WebUtility.Wait();

  		Filter.click();
  		Unpublished.click();
  		ApplyFilter.click();
  		WebUtility.Wait();

  		Filter.click();
  		NeedWork.click();
  		ApplyFilter.click();
  		WebUtility.Wait();

  		Filter.click();
  		Scheduled.click();
  		ApplyFilter.click();
  		WebUtility.Wait();

  		Filter.click();
  		SubmittedToReview.click();
  		ApplyFilter.click();
  		WebUtility.Wait();

  		Filter.click();
  		Archived.click();
  		ApplyFilter.click();
  		WebUtility.Wait();

  		Filter.click();
  		ClearFilter.click();
  		WebUtility.Wait();
  		
  		Filter.click();
  		WebUtility.Click(StartDate);
  		datePicker.sendKeys(Keys.ARROW_LEFT);
  		datePicker.sendKeys(Keys.ARROW_LEFT);
  		datePicker.sendKeys(Keys.ARROW_LEFT);
  		datePicker.sendKeys(Keys.ARROW_LEFT);
  		datePicker.sendKeys(Keys.ESCAPE);	
  	    
 		WebUtility.Click(EndDate);
		datePicker.sendKeys(Keys.ARROW_RIGHT);
		datePicker.sendKeys(Keys.ARROW_RIGHT);
		datePicker.sendKeys(Keys.ARROW_RIGHT);
		datePicker.sendKeys(Keys.ARROW_RIGHT);
		datePicker.sendKeys(Keys.ESCAPE);
		WebUtility.Click(ApplyFilter);
		WebUtility.Wait();

  		Filter.click();
  		ClearFilter.click();
  		WebUtility.Wait();

  		Filter.click();
  		FilterPrimaryGenre.click();
  		dropDownList.get(0).click();

  		Closetab.click();
  		SecondaryGenre.click();
  		dropDownList.get(0).click();

  		Closetab.click();
  		ThematicGenre.click();
  		dropDownList.get(0).click();
  		
  		Closetab.click();
  		SettingGenre.click();
  		dropDownList.get(0).click();
  		
  		Closetab.click();
  		Actor.sendKeys("Prabhas");
  		dropDownList.get(0).click();

  		Closetab.click();
  		SubType.click();
  		dropDownList.get(0).click();
  		
  		Closetab.click();
  		FilterContentCategory.click();
  		dropDownList.get(0).click();
  		
  		Closetab.click();
  		RCSCategory.click();
  		dropDownList.get(0).click();
  		
  		Closetab.click();
  		Theme.click();
  		dropDownList.get(0).click();
  		
  		Closetab.click();
  		TargetAudienceFilter.click();
  		dropDownList.get(0).click();
  		
  		Closetab.click();
  		LicenseGroupCountries.click();
  		dropDownList.get(0).click();
  		
  		Closetab.click();
  		AgeRating.click();
  		dropDownList.get(0).click();
  		
  		Closetab.click();	
  		FilterBusinessType.click();
  		dropDownList.get(0).click();

  	//	Closetab.click();
  	//	ExternalId.sendKeys("Sample");
  	//	WebUtility.Wait();
  	
  		Closetab.click();
  		ContentRatingFilter.click();
  		dropDownList.get(0).click();
  		
  		Closetab.click();
  		FilterContentOwner.click();
  		dropDownList.get(0).click();
  		
  		Closetab.click();
  		FilterAudioLanguage.click();
  		dropDownList.get(0).click();
  	
  		Closetab.click();
  		FilterOriginalLanguage.click();
  		dropDownList.get(0).click();
  		
  		Closetab.click();
  		TagsFilter.sendKeys("actor");
  		TagsFilter.sendKeys(Keys.ARROW_DOWN);
  		TagsFilter.sendKeys(Keys.ENTER);	 

  		Closetab.click();
  		TranslationLanguage.click();
  		dropDownList.get(0).click();
  	
  		Closetab.click();
  		TranslationStatus.click();
  		dropDownList.get(0).click();
  		
  		Closetab.click();
  		MoodEmotion.click();
  		dropDownList.get(0).click();
  		
  		Closetab.click();
  		ApplyFilter.click();
  		WebUtility.Wait();
  		
  		Filter.click();
  		ClearFilter.click();
  		WebUtility.Wait();
  		
  	}
  	
  	public void QuickFilling() throws InterruptedException {
  	
  		logoutlink.click();
  		logoutbutton.click();
  		WebUtility.clear_cache();
  		driver.get(prop.getProperty("url")); 
  		loginPage1.gmailLogin(prop.getProperty("Gusername1"),prop.getProperty("Gpassword"));
  		WebUtility.Wait();
  		WebUtility.Click(movies);
  		WebUtility.Wait();
  		DropdwnOptions.click();
  		QuickFilling.click();
  		WebUtility.Wait();

  		WebUtility.Click(title);
  		title.sendKeys(Keys.CONTROL + "a");
  		title.sendKeys(Keys.DELETE);
  		title.sendKeys(movieQuickFilingTitle);
  		Thread.sleep(3000);

		WebUtility.element_to_be_clickable(shortdesc);
		shortdesc.sendKeys(
				"Golmaal is a series of Indian comedy films directed by Rohit Shetty and produced first by Dhillin Mehta and later by Shetty and Sangeeta Ahir.");

		QuickSubtype.click();
		dropDownList.get(0).click();

		QuickContentCategory.click();
		dropDownList.get(0).click();

		QuickPrimaryGenre.click();
		dropDownList.get(0).click();

//  		QuickZeeReleaseDate.sendKeys("30042021");
		QuickZeeReleaseDate.click();
		datePicker.sendKeys(Keys.ARROW_RIGHT);
		datePicker.sendKeys(Keys.ESCAPE);

		WebUtility.Click(QuickVideoDuration);
		List<WebElement> videoduration = driver.findElements(By.xpath("//li[@role='button']"));
		System.out.println((videoduration.size()));
		for (WebElement e9 : videoduration) {
			System.out.println(e9.getText());
			String videohrs = e9.getText();
			if (videohrs.contains("02")) {
				e9.click();
				break;
			}
		}

		WebUtility.Click(QuickAudioLanguage);
		WebUtility.Click(dropDownList.get(0));
  		
  		QuickPrimaryLanguage.click();
  		WebUtility.Click(dropDownList.get(0));

  		QuickDubbedLanguage.click();
  		WebUtility.Click(dropDownList.get(0));

  		QuickOriginalLanguage.click();
  		WebUtility.Click(dropDownList.get(0));

  		QuickContentVersion.click();
  		WebUtility.Click(dropDownList.get(0));

  		WebUtility.Click(QuickNext);
  		
  		WebUtility.Click(QuickContentOwner);
  		WebUtility.Click(dropDownList.get(0));

  		WebUtility.Click(QuickCertification);
  		WebUtility.Click(dropDownList.get(0));

  		NextBtn.click();
  		WebUtility.Click(QuickMarkAsDone);
  		WebUtility.Wait();

  	
  	}

  	public void QuickVideo() throws InterruptedException {

  		WebUtility.Click(QuickVideoSection);
  		WebUtility.Wait();
  		DashRoot.sendKeys("Dash root");
  		WebUtility.Wait();
  		DashManifest.sendKeys("Dash manifest data ");
  		WebUtility.Wait();
  		HlsRoot.sendKeys("HLS root");
  		WebUtility.Wait();
  		HlsManifest.sendKeys("Hls manifest data");
  		WebUtility.Wait();
  		QuickMediathek.sendKeys("Quick root data");
  		WebUtility.Wait();
  		QuickMarkAsDone.click();	
  		WebUtility.Wait();

  	}

  	public void QuickLicense() throws InterruptedException {
  		
		WebUtility.javascript(License);
		WebUtility.Wait();
		WebUtility.javascript(createLicense);
		WebUtility.Wait();
		licenseSetName.sendKeys("Automation Test Data");

		WebUtility.Click(licenseFromDate);
		WebUtility.Wait();
		datePicker.sendKeys(Keys.ARROW_RIGHT);
		datePicker.sendKeys(Keys.ESCAPE);

		WebUtility.Click(licenseToDate);
		WebUtility.Wait();
		datePicker.sendKeys(Keys.ARROW_RIGHT);
		datePicker.sendKeys(Keys.ARROW_RIGHT);
		datePicker.sendKeys(Keys.ESCAPE);

		WebUtility.Click(Country);
  		WebUtility.Wait();
        for (int x = 0; x < dropDownList.size(); x++) {
        	if(x<4) {
            WebUtility.Click(dropDownList.get(x));
        	}
        	else {
        		break;
        	}
        }
  		BusinessType.click();
  		WebUtility.Wait();
  		WebUtility.Click(dropDownList.get(0));

        WebUtility.Click(licenseContentAgeRatingId);	
        WebUtility.Wait();
        licenseContentAgeRatingId.sendKeys(Keys.ARROW_DOWN);
        licenseContentAgeRatingId.sendKeys(Keys.ENTER);
        
  		createLicenseSave1.click();
  		WebUtility.Wait();
  		
  		//Creating Multiple License
  		WebUtility.javascript(License);
  		WebUtility.Wait();	
  		WebUtility.javascript(createLicense);
  		WebUtility.Wait();
  		licenseSetName.sendKeys("Automation Test Data II");   
  		
		WebUtility.Click(licenseFromDate);
		WebUtility.Wait();
		datePicker.sendKeys(Keys.ARROW_RIGHT);
		datePicker.sendKeys(Keys.ARROW_RIGHT);
		datePicker.sendKeys(Keys.ARROW_RIGHT);
		datePicker.sendKeys(Keys.ESCAPE);

		WebUtility.Click(licenseToDate);
		WebUtility.Wait();
		datePicker.sendKeys(Keys.ARROW_RIGHT);
		datePicker.sendKeys(Keys.ARROW_RIGHT);
		datePicker.sendKeys(Keys.ARROW_RIGHT);
		datePicker.sendKeys(Keys.ARROW_RIGHT);
		datePicker.sendKeys(Keys.ESCAPE);

		WebUtility.Click(Country);
		WebUtility.Wait();
        for (int x = 0; x < dropDownList.size(); x++) {
        	if(x<4) {
            WebUtility.Click(dropDownList.get(x));
        	}
        	else {
        		break;
        	}
        }
  	
  		BusinessType.click();
  		WebUtility.Wait();
  		WebUtility.Click(dropDownList.get(0));

        WebUtility.Click(licenseContentAgeRatingId);	
        WebUtility.Wait();
        licenseContentAgeRatingId.sendKeys(Keys.ARROW_DOWN);
        licenseContentAgeRatingId.sendKeys(Keys.ENTER);
        
  		createLicenseSave1.click();
  		WebUtility.Wait();
  		
  		WebUtility.Wait();
  		String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
  		sa.assertTrue(doneButtonActiveColor.contains("255,255,255") || doneButtonActiveColor.contains("255, 255, 255"), "Movie License Quick Filling Mark As Done Assertion Fails");
  		WebUtility.Click(doneButtonActive);	
  		WebUtility.Wait();
  		try {
  			TestUtil.captureScreenshot("Movie Quick Filing - License Section","Movie");
  		} catch (IOException e) {
  			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
  		}
  		
  	}
  	
  	public void QuickImages() throws InterruptedException {

  		QuickImageSection.click();
  		WebUtility.Wait();

  		QuickUploadimage.sendKeys(System.getProperty("user.dir") + "\\images\\movies\\Tanuwedsmanu3.jpg");
  		WebUtility.Wait();

  		// ---------------------------Images Set 2
  		// ----------------------------------------------
  		try {
  			WebUtility.element_to_be_fluentwait(imagesCreateNewSet);
  			WebUtility.Click(imagesCreateNewSet);

  			WebUtility.Click(newSetName);
  			newSetName.sendKeys("Automation Test Data");

  			newSetCountry.click();
  			WebUtility.Click(dropDownList.get(0));
  			closeDropdown.click();

  			newSetPlatform.click();
  			WebUtility.Click(dropDownList.get(0));
  			closeDropdown.click();

  			newSetGender.click();
  			WebUtility.Click(dropDownList.get(0));
  			closeDropdown.click();

  			newSetGenre.click();
  			WebUtility.Click(dropDownList.get(0));
  			closeDropdown.click();

  			newSetAgeGroup.click();
  			WebUtility.Click(dropDownList.get(0));
  			closeDropdown.click();

  			newSetLanguage.click();
  			WebUtility.Click(dropDownList.get(0));
  			closeDropdown.click();

  			newSetOthers.click();
  			newSetOthers.sendKeys("Automation Test Data");

  			WebUtility.Click(save);
  			WebUtility.Wait();

  			WebUtility.Click(imageSets.get(1));
  			WebUtility.Wait();

  			QuickUploadimage.sendKeys(System.getProperty("user.dir") + "\\images\\movies\\Tanuwedsmanu3.jpg");
  			WebUtility.Wait();

  		} catch (Exception e) {
  			e.printStackTrace();
  		}

  		// -------------------------- Image Set 3
  		// -----------------------------------------------
  		try {

  			WebUtility.element_to_be_fluentwait(imagesCreateNewSet);
  			WebUtility.Click(imagesCreateNewSet);

  			WebUtility.Click(newSetName);
  			newSetName.sendKeys("Automation Test Data II");

  			newSetCountry.click();
  			WebUtility.Click(dropDownList.get(1));
  			closeDropdown.click();

  			newSetPlatform.click();
  			WebUtility.Click(dropDownList.get(1));
  			closeDropdown.click();

  			newSetGender.click();
  			WebUtility.Click(dropDownList.get(1));
  			closeDropdown.click();

  			newSetGenre.click();
  			WebUtility.Click(dropDownList.get(1));
  			closeDropdown.click();

  			newSetAgeGroup.click();
  			WebUtility.Click(dropDownList.get(1));
  			closeDropdown.click();

  			newSetLanguage.click();
  			WebUtility.Click(dropDownList.get(1));
  			closeDropdown.click();

  			newSetOthers.click();
  			newSetOthers.sendKeys("Automation Test Data II");

  			WebUtility.Click(save);

  			WebUtility.Click(imageSets.get(2));

  			js.executeScript("window.scrollTo(0,0)");
  			WebUtility.Wait();

  			QuickUploadimage.sendKeys(System.getProperty("user.dir") + "\\images\\movies\\Tanuwedsmanu3.jpg");
  			WebUtility.Wait();
  		} catch (Exception e) {
  			e.printStackTrace();
  		}

  		// ---------------------- Inactivate Image Set ------------------------
  		try {
  			js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
  			WebUtility.Wait();
  			WebUtility.Click(imageSetActiveButton);
  			WebUtility.Wait();
  			WebUtility.Click(dropDownList.get(0));
  			WebUtility.javascript(yes.get(0));
  			WebUtility.Wait();
  		} catch (Exception e) {
  			e.printStackTrace();
  		}

  		// ------------------------ Delete Image Set -----------------------------
  		try {
  			removeSet.get(1).click();
  			WebUtility.Wait();
  			yes.get(0).click();
  			WebUtility.Wait();
  		} catch (Exception e) {
  			e.printStackTrace();
  		}

  		js.executeScript("window.scrollTo(0,0);");
  		WebUtility.Wait();
  		QuickMarkAsDone.click();
  		WebUtility.Wait();

  	}

  	public void QuickSEO() throws InterruptedException {

  		QuickSeoSection.click();
  		WebUtility.Wait();

  		QuickSeoTitle.click();
  		QuickSeoTitle.sendKeys(Keys.CONTROL + "a");
  		QuickSeoTitle.sendKeys(Keys.BACK_SPACE);
  		QuickSeoTitle.sendKeys("Quick Filling Test Title");
  		Thread.sleep(2000);

  		QuickSeoMataDesc.click();
  		QuickSeoMataDesc.sendKeys(Keys.CONTROL + "a");
  		QuickSeoMataDesc.sendKeys(Keys.BACK_SPACE);
  		QuickSeoMataDesc.sendKeys("SEO description data");
  		Thread.sleep(2000);
  		
  		QuickSeoMeta.click();
  		QuickSeoMeta.sendKeys(Keys.CONTROL + "a");
  		QuickSeoMeta.sendKeys(Keys.BACK_SPACE);
  		Thread.sleep(2000);		

  		QuickSeoMeta.sendKeys("SEO Meta description data field");

  		QuickHeading.click();
  		QuickHeading.sendKeys(Keys.CONTROL + "a");
  		QuickHeading.sendKeys(Keys.BACK_SPACE);
  		QuickHeading.sendKeys("Heading new");
  		
  		QuickHeading2.click();
  		QuickHeading2.sendKeys(Keys.CONTROL + "a");
  		QuickHeading2.sendKeys(Keys.BACK_SPACE);
  		QuickHeading2.sendKeys("New second heading for seo data");

  		QuickVideo.click();
  		QuickWeb.click();
  		QuickBread.click();
  		QuickImage.click();
  		QuickOrganization.click();

  		MarkAsDone.click();
  		WebUtility.Wait();

  	}

  	public void QuickMapcontent() throws InterruptedException {

  		QuickMapContent.click();
  		WebUtility.Wait();

  		WebUtility.javascript(QuickAddContent);

  		Checkbox.click();

  		AssignContent.click();
  		WebUtility.Wait();

  		WebUtility.javascript(LinkContent);
  		WebUtility.javascript(LinkAddContent);
  		WebUtility.javascript(LinkCheckbox);
  		WebUtility.javascript(LinkAssignContent);
  		WebUtility.Wait();

  		WebUtility.javascript(MapMarkAsDone);
  		WebUtility.Wait();

  	}

  	public void QuickChecklist() throws InterruptedException {
//--TODO Update when roles are fixed ---
  		/*
  		//--Verifying Movie Quick Filing Draft Status
  		try
  		{
  			contentProperties.click();
  			WebUtility.element_to_be_visible(contentStatus);
  			TestUtil.captureScreenshot("Movie QuickFiling Draft Status","Movie");
  			String expectedStatus = "Draft";
  			String actualStatus = contentStatus.getText();
  			sa.assertEquals(actualStatus,expectedStatus,"Movie QuickFiling Draft Status is Failed");
  			Reporter.log("Movie QuickFiling Draft Status is Passed",true);	
  		}
  		catch (Exception e)
  		{
  			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
  		}
  		
  		WebUtility.javascript(QuickChecklistSection);
  		WebUtility.Wait();
  		WebUtility.javascript(SubmitToReview);
  		WebUtility.Wait();
  		WebUtility.javascript(SubmitYesBtn);
  		WebUtility.Wait();
  		WebUtility.javascript(SubmitOk);
  		WebUtility.Wait();
  		
  		//--Verifying Movie Quick Filing Submit to Review status
  		try
  		{
  			contentProperties.click();
  			WebUtility.element_to_be_visible(contentStatus);
  			TestUtil.captureScreenshot("Movie QuickFiling Submitted To Review Status","Movie");
  			String expectedStatus = "Submitted To Review";
  			String actualStatus = contentStatus.getText();
  			sa.assertEquals(actualStatus,expectedStatus,"Movie QuickFiling Submitted To Review Status is Failed");
  			Reporter.log("Movie QuickFiling Submitted To Review Status is Passed",true);	
  		}
  		catch (Exception e)
  		{
  			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
  		}
          
  		logoutlink.click();
  		logoutbutton.click();
  		WebUtility.clear_cache();
  		driver.get(prop.getProperty("url"));
  		loginPage1.gmailLogin(prop.getProperty("Gusername3"),prop.getProperty("Gpassword"));
  		WebUtility.Wait();
  		WebUtility.Click(movies);
  		WebUtility.Wait();
  		Search.sendKeys(movieQuickFilingTitle);
  		Thread.sleep(1000);
  		WebUtility.Wait();
  		WebUtility.Click(View);
  		WebUtility.Wait();	
  		editMovie.click();
  		WebUtility.Wait();
    		editMovieQuickFiling.click();
  		yes.get(0).click();
  		WebUtility.Wait();
  		WebUtility.javascript(Checklist);
  		WebUtility.Wait();
  		NeedWorkReason.click();
  		List<WebElement> countrylist = driver.findElements(By.xpath("//li[@role='option']"));
  		System.out.println((countrylist.size()));
  		for(WebElement e22:countrylist)
  		{
  			System.out.println(e22.getText());
  			String countryname= e22.getText();
  			if(countryname.contains("Missing metadata"))
  			{
  				e22.click();
  				break;
  			}
  		}		

  		Thread.sleep(2000);
  		WebUtility.Click(NeedWorkbtn);
  		WebUtility.Wait();
  		WebUtility.Click(NeedWorkYes);
  		WebUtility.Wait();
  		WebUtility.Click(NeedWorkOk);
  		WebUtility.Wait();
  		
  		//--Verifying Movie Quick Filing Need Work status
  		try
  		{
  			contentProperties.click();
  			WebUtility.element_to_be_visible(contentStatus);
  			TestUtil.captureScreenshot("Movie QuickFiling Need Work Status","Movie");
  			String expectedStatus = "Need Work";
  			String actualStatus = contentStatus.getText();
  			sa.assertEquals(actualStatus,expectedStatus,"Movie QuickFiling Need Work Status is Failed");
  			Reporter.log("Movie QuickFiling Need Work Status is Passed",true);	
  		}
  		catch (Exception e)
  		{
  			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
  		}
          
  		logoutlink.click();
  		logoutbutton.click();
  		WebUtility.clear_cache();
  		driver.get(prop.getProperty("url")); 
  		loginPage1.gmailLogin(prop.getProperty("Gusername1"),prop.getProperty("Gpassword"));
  		WebUtility.Wait();
  		WebUtility.Wait();		
  		WebUtility.Click(movies);
  		WebUtility.Wait();
  		Search.sendKeys(movieQuickFilingTitle);
  		Thread.sleep(1000);
  		WebUtility.Wait();
  		WebUtility.Click(View);
  		WebUtility.Wait();
  		editMovie.click();
  		WebUtility.Wait();
    		editMovieQuickFiling.click();
  		yes.get(0).click();
  		WebUtility.Wait();
  		QuickSubtype.click();
  		dropDownList.get(1).click();
  		WebUtility.Wait();
  		NextBtn.click();	
  		WebUtility.Wait();
  		MarkAsDone.click();
  		WebUtility.Wait();
  		WebUtility.javascript(Checklist);
  		WebUtility.Wait();
  		WebUtility.javascript(SubmitToReview);
  		WebUtility.Wait();
  		WebUtility.javascript(SubmitYesBtn);
  		WebUtility.Wait();
  		WebUtility.javascript(SubmitOk);
  		WebUtility.Wait();

  		//--Verifying Movie Quick Filing Submit to Review status
  		try
  		{
  			contentProperties.click();
  			WebUtility.element_to_be_visible(contentStatus);
  			TestUtil.captureScreenshot("Movie QuickFiling Submitted To Review Status","Movie");
  			String expectedStatus = "Submitted To Review";
  			String actualStatus = contentStatus.getText();
  			sa.assertEquals(actualStatus,expectedStatus,"Movie QuickFiling Submitted To Review Status is Failed");
  			Reporter.log("Movie QuickFiling Submitted To Review Status is Passed",true);	
  		}
  		catch (Exception e)
  		{
  			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
  		}
      	*/     
        logoutlink.click();
  		logoutbutton.click();
  		WebUtility.clear_cache();
  		driver.get(prop.getProperty("url")); 
  		loginPage1.gmailLogin(prop.getProperty("Gusername3"),prop.getProperty("Gpassword"));Thread.sleep(4000);
  		WebUtility.Wait();
  		WebUtility.Click(movies);
  		WebUtility.Wait();
  		Search.sendKeys(movieQuickFilingTitle);
  		Thread.sleep(1000);
  		WebUtility.Wait();
  		WebUtility.Click(View);
  		WebUtility.Wait();
  		editMovie.click();
  		WebUtility.Wait();
    	editMovieQuickFiling.click();
  		yes.get(0).click();
 

  		WebUtility.Wait();
  		WebUtility.javascript(QuickChecklistSection);
  		WebUtility.Wait();		 	

  		WebUtility.Click(PublishCountryDropdown);
        WebUtility.Click(dropDownList.get(0));
        /*
  		List<WebElement> licensedcountrylist = driver.findElements(By.xpath("//li[@role='option']"));
  		System.out.println((licensedcountrylist.size()));
  		List<WebElement> countryList = driver.findElements(By.xpath("//li[@role='option']"));
  		for(WebElement countries:countryList)
  		{
  			countries.click();
  		}
  		*/	
  		WebUtility.Click(PublishedBtn);
  		WebUtility.Wait();
  		WebUtility.Click(PublishedYes);

  		WebUtility.Click(Published);
  		WebUtility.Wait();
  		
  		//--Verifying Movie QuickFiling Published status
  		try
  		{
  				contentProperties.click();
  				WebUtility.element_to_be_visible(contentStatus);
  				TestUtil.captureScreenshot("Movie QuickFiling Published Status","Movie");
  				String expectedStatus = "Published";
  				String actualStatus = contentStatus.getText();
  				sa.assertEquals(actualStatus,expectedStatus,"Movie QuickFiling Published Status is Failed");
  				Reporter.log("Movie QuickFiling Published Status is Passed",true);	
  		}
  		catch (Exception e)
  		{
  				System.out.println("Screenshot Capture Failed ->" + e.getMessage());
  		}

  		WebUtility.Wait();
  		WebUtility.javascript(Checklist);
  		WebUtility.Wait();
  		
  		//Unpublished the Published Movie ------------------------------------
  		WebUtility.Click(UnpublishContent);
  		WebUtility.Wait();    
  		Reason.click();
  		List<WebElement> countrylist1 = driver.findElements(By.xpath("//li[@role='option']"));
  		System.out.println((countrylist1.size()));
  		for(WebElement e22:countrylist1)
  		{
  			System.out.println(e22.getText());
  			String countryname= e22.getText();
  			if(countryname.contains("On Hold"))
  			{
  				e22.click();
  				break;
  			}
  		}		

  		WebUtility.Click(YesBtn);
  		WebUtility.Click(Yes);
  		WebUtility.Wait();
  		
  		//--Verifying Movie Quick Filing UnPublished status
  		try
  		{
  			contentProperties.click();
  			WebUtility.element_to_be_visible(contentStatus);
  			TestUtil.captureScreenshot("Movie QuickFiling Unpublished Status","Movie");
  			String expectedStatus = "Unpublished";
  			String actualStatus = contentStatus.getText();
  			sa.assertEquals(actualStatus,expectedStatus,"Movie QuickFiling Unpublished Status is Failed");
  			Reporter.log("Movie QuickFiling Unpublished Status is Passed",true);	
  		}
  		catch (Exception e)
  		{
  			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
  		}
          
          // Archive unpublished content -------------------------------------
  		MoviesListing.click();
  		WebUtility.Wait();
  		Search.sendKeys(movieQuickFilingTitle);
  		Thread.sleep(1000);
  		WebUtility.Wait();
  		Archive.get(0).click();				
  		ArchiveYes.click();
  		WebUtility.Wait();
  		WebUtility.Wait();
  		WebUtility.Click(View);
  		WebUtility.Wait();
  		
  		//--Verifying Movie Quick Filing Archive status
  		try
  		{
  			contentProperties.click();
  			WebUtility.element_to_be_visible(contentStatus);
  			TestUtil.captureScreenshot("Movie QuickFiling Archived Status","Movie");
  			String expectedStatus = "Archived";
  			String actualStatus = contentStatus.getText();
  			sa.assertEquals(actualStatus,expectedStatus,"Movie QuickFiling Archived Status is Failed");
  			Reporter.log("Movie QuickFiling Archived Status is Passed",true);	
  		}
  		catch (Exception e)
  		{
  			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
  		}
  		WebUtility.Wait();
  		// Restore Content to draft mode ---------------------------------------
  		MoviesListing.click();
  		WebUtility.Wait();
  		Search.sendKeys(movieQuickFilingTitle);
  		Thread.sleep(1000);
  		WebUtility.Wait();
  		WebUtility.javascript(Restore);			
  		WebUtility.Wait();
  		WebUtility.javascript(RestoreYes);
  		WebUtility.Wait();
  		WebUtility.Click(View);
  		WebUtility.Wait();
  		WebUtility.Click(editMovie);
  		WebUtility.Wait();
    	editMovieQuickFiling.click();
  		yes.get(0).click();
  		WebUtility.Wait();
  		
  		//--Verifying Movie Quick Filing Restore Status
  		try
  		{
  			contentProperties.click();
  			WebUtility.element_to_be_visible(contentStatus);
  			TestUtil.captureScreenshot("Movie QuickFiling Restore Status","Movie");
  			String expectedStatus = "Draft";
  			String actualStatus = contentStatus.getText();
  			sa.assertEquals(actualStatus,expectedStatus,"Movie QuickFiling Restore Status is Failed");
  			Reporter.log("Movie QuickFiling Restore Status is Passed",true);	
  		}
  		catch (Exception e)
  		{
  			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
  		}
  		WebUtility.Click(Checklist);
  		WebUtility.Wait();

  		// Published Content ----------------------------------------------      
  		WebUtility.Click(PublishCountryDropdown);
        WebUtility.Click(dropDownList.get(0));
        /*
  		List<WebElement> countryList1 = driver.findElements(By.xpath("//li[@role='option']"));
  		for(WebElement countries:countryList1)
  		{
  				countries.click();
  		}
  		*/
  		WebUtility.Wait();
  		WebUtility.Click(PublishedBtn);
  		WebUtility.Wait();
  		WebUtility.Click(PublishedYes);
  		WebUtility.Wait();
  		WebUtility.Click(Published);
  		WebUtility.Wait();
  		
  		//--Verifying Movie Quick Filing Published status
  		try
  		{
  			contentProperties.click();
  			WebUtility.element_to_be_visible(contentStatus);
  			TestUtil.captureScreenshot("Movie QuickFiling Published Status","Movie");
  			String expectedStatus = "Published";
  			String actualStatus = contentStatus.getText();
  			sa.assertEquals(actualStatus,expectedStatus,"Movie QuickFiling Published Status is Failed");
  			Reporter.log("Movie QuickFiling Published Status is Passed",true);	
  		}
  		catch (Exception e)
  		{
  			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
  		}
          
  		// Make some changes to republished the content ----------------------------
  		logoutlink.click();
  		logoutbutton.click();
  		WebUtility.clear_cache();
  		driver.get(prop.getProperty("url")); 
  		loginPage1.gmailLogin(prop.getProperty("Gusername1"),prop.getProperty("Gpassword"));
  		WebUtility.Wait();
  		WebUtility.Click(movies);
  		WebUtility.Wait();
  		Search.sendKeys(movieQuickFilingTitle);
  		Thread.sleep(1000);
  		WebUtility.Wait();
  		WebUtility.Click(View);
  		editMovie.click();
  		WebUtility.Wait();
    		editMovieQuickFiling.click();
  		yes.get(0).click();
  		WebUtility.Wait();
  		QuickSubtype.click();
  		dropDownList.get(2).click();
  		WebUtility.Wait();
  		NextBtn.click();
  		WebUtility.Wait();
  		MarkAsDone.click();
  		WebUtility.Wait();
  		
  		//--Verifying Movie Quick Filing Changed status
  		try
  		{
  			contentProperties.click();
  			WebUtility.element_to_be_visible(contentStatus);
  			TestUtil.captureScreenshot("Movie QuickFiling Changed Status","Movie");
  			String expectedStatus = "Changed";
  			String actualStatus = contentStatus.getText();
  			sa.assertEquals(actualStatus,expectedStatus,"Movie QuickFiling Changed Status is Failed");
  			Reporter.log("Movie QuickFiling Changed Status is Passed",true);	
  		}
  		catch (Exception e)
  		{
  			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
  		}
          
  		logoutlink.click();
  		logoutbutton.click();
  		WebUtility.clear_cache();
  		driver.get(prop.getProperty("url")); 
  		loginPage1.gmailLogin(prop.getProperty("Gusername3"),prop.getProperty("Gpassword"));
  		WebUtility.Wait();
  		WebUtility.Click(movies);
  		WebUtility.Wait();
  		Search.sendKeys(movieQuickFilingTitle);
  		Thread.sleep(1000);
  		WebUtility.Wait();
  		WebUtility.Click(View);
  		WebUtility.Wait();
  		editMovie.click();
  		WebUtility.Wait();
    	editMovieQuickFiling.click();
  		yes.get(0).click();
  		WebUtility.Wait();
  		WebUtility.javascript(Checklist);
  		WebUtility.Wait();

  		// Republished the content ------------------------------
  		WebUtility.javascript(Republished);            

  		WebUtility.javascript(RepublishedYes);

  		WebUtility.javascript(RepublishedConfirm);
  		WebUtility.Wait();

  		WebUtility.javascript(PublishedAgain);
  		WebUtility.Wait();

  		//--Verifying Movie Quick FIling Republished status
  		try
  		{
  			contentProperties.click();
  			WebUtility.element_to_be_visible(contentStatus);
  			TestUtil.captureScreenshot("Movie QuickFiling Republished Status","Movie");
  			String expectedStatus = "Published";
  			String actualStatus = contentStatus.getText();
  			sa.assertEquals(actualStatus,expectedStatus,"Movie QuickFiling Republished Status is Failed");
  			Reporter.log("Movie QuickFiling Republished Status is Passed",true);	
  		}
  		catch (Exception e)
  		{
  			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
  		}
  		
  		//--Clone QuickFiling Movie
  		
  		MoviesListing.click();
  		WebUtility.Wait();
  		Search.sendKeys(movieQuickFilingTitle);
  		Thread.sleep(1000);
  		WebUtility.Wait();
  		WebUtility.Wait();
  		Clone.get(0).click();
  		WebUtility.Wait();
  		WebUtility.javascript(CloneYes);
  		WebUtility.Wait();		
  		WebUtility.javascript(Checklist);
  		WebUtility.Wait();
  		
  		WebUtility.javascript(Scheduled_plus);
  		WebUtility.Wait();
  		
        //System.out.println("--------> Current Time = "+currentTime.getAttribute("innerText").substring(20, 28));
        //String currentTimeString  = currentTime.getAttribute("innerText").substring(20, 27);
    	//js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
    	
  		WebUtility.Click(ScheduledDate);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        WebUtility.Click(datePickerMinutes);
        actionProvider.moveToElement(datePickerMinutesClock).click().build().perform();      
        datePicker.sendKeys(Keys.ESCAPE);    
         
  		WebUtility.Click(ScheduledCountry);
  		WebUtility.Wait();
  		WebUtility.Click(dropDownList.get(0));
  		
  		WebUtility.javascript(ScheduledBtn);
  		WebUtility.Wait();
  		WebUtility.javascript(ScheduledYes);
  		WebUtility.Wait();
  		WebUtility.javascript(OkBtn);
  		WebUtility.Wait();
  		
    	js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
        WebUtility.Wait();
        js.executeScript("window.scrollTo(0,0);");
        WebUtility.Wait();
        
     
        //System.out.println("--------> Scheduled Time = "+scheduledTime.getAttribute("innerText").substring(12, 20));
        //String scheduledTimeString = scheduledTime.getAttribute("innerText").substring(12, 20);
    	//js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
    	//sa.assertTrue(scheduledTimeString.contains(currentTimeString),"Movie Quick Filing  - Current Time and Scheduled Time are not Equal");
    	
  		//--Verifying Movie Quick Filing Scheduled status
  		try
  		{
  			contentProperties.click();
  			WebUtility.Wait();
  			WebUtility.element_to_be_visible(contentStatus);
  			TestUtil.captureScreenshot("Movie QuickFiling Scheduled Status","Movie");
  			String expectedStatus = "Scheduled";
  			String actualStatus = contentStatus.getText();
  			sa.assertEquals(actualStatus,expectedStatus,"Movie QuickFiling Scheduled Status is Failed");
  			Reporter.log("Movie QuickFiling Scheduled Status is Passed",true);	
  		}
  		catch (Exception e)
  		{
  			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
  		}
  		
  		PublishedHistory.click();
  		js.executeScript("window.scrollBy(0,1000)");
  		Thread.sleep(5000);
  			
  		movieBreadcrumb.click();
  		WebUtility.Wait();

  		Search.sendKeys(movieQuickFilingTitle);
  		Thread.sleep(1000);
  		WebUtility.Wait();

  		ViewDetails.click();
  		WebUtility.Wait();

  		LinkMovies.click();
  		WebUtility.Wait();

//  		JavascriptExecutor j = (JavascriptExecutor)driver;
//  		j.executeScript("arguments[0].click();", movieBreadcrumb);
  		movieBreadcrumb.click();
  		WebUtility.Wait();

  	}
  	
  	public void MovieQuickFilingReleatedContent() throws InterruptedException{
  			
  		  WebUtility.Click(relatedContent);
  		  /*
          WebUtility.Click(relatedContentAssignTvShows);

          WebUtility.element_to_be_visible(relatedContentAddTvShows);
          WebUtility.Click(relatedContentAddTvShows);

          WebUtility.Wait();
          checkBox.get(0).click();
          checkBox.get(1).click();
          WebUtility.Click(assignRelatedContent);
          WebUtility.Click(deleteRelatedContent); 
          WebUtility.Wait();
          WebUtility.Click(yes.get(0));
          WebUtility.Wait();
    */
          WebUtility.Click(relatedContentAssignVideos);
          WebUtility.element_to_be_visible(relatedContentAddVideos);
          WebUtility.Click(relatedContentAddVideos);

          WebUtility.Wait();
          checkBox.get(0).click();
          checkBox.get(1).click();
          WebUtility.Click(assignRelatedContentVideos);
          WebUtility.Wait();
          WebUtility.Click(deleteRelatedContent);
          WebUtility.Wait();
          WebUtility.Click(yes.get(0));
          WebUtility.Wait();
      
          WebUtility.Click(relatedContentAssignMovies);
          WebUtility.element_to_be_visible(relatedContentAddMovies);
          WebUtility.Click(relatedContentAddMovies);

          WebUtility.Wait();
          checkBox.get(0).click();
          checkBox.get(1).click();
          WebUtility.Click(assignRelatedContentMovies);
          WebUtility.Wait();
          WebUtility.Click(deleteRelatedContent);
          WebUtility.Wait();
          WebUtility.Click(yes.get(0));
          WebUtility.Wait();
/*
          WebUtility.Click(relatedContentAssignSeasons);
          WebUtility.element_to_be_visible(relatedContentAddSeasons);
          WebUtility.Click(relatedContentAddSeasons);

          WebUtility.Wait();
          checkBox.get(0).click();
          checkBox.get(1).click();
          WebUtility.Click(assignRelatedContent);
          WebUtility.Click(deleteRelatedContent);
          WebUtility.Wait();
          WebUtility.Click(yes.get(0));
          WebUtility.Wait();

          WebUtility.Click(relatedContentAssignEpisodes);
          WebUtility.element_to_be_visible(relatedContentAddEpisodes);
          WebUtility.Click(relatedContentAddEpisodes);

          WebUtility.Wait();
          checkBox.get(0).click();
          checkBox.get(1).click();
          WebUtility.Click(assignRelatedContent);
          WebUtility.Click(deleteRelatedContent);
          WebUtility.Wait();
          WebUtility.Click(yes.get(0));
          WebUtility.Wait();
       */
          String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
          sa.assertTrue(doneButtonActiveColor.contains("255,255,255") || doneButtonActiveColor.contains("255, 255, 255"), "Movie Quick Filing Related Content Content Mark As Done Assertion Fails");
          WebUtility.Click(doneButtonActive);
          WebUtility.Click(relatedContentBack);
          WebUtility.Wait();
  	}
  	//---------------------------------------------Movies Single Landing -----------------------------------------------------
  	public void SingleLanding() throws InterruptedException {
  	/*
  		logoutlink.click();
  		logoutbutton.click();
  		WebUtility.clear_cache();
  		driver.get(prop.getProperty("url")); 
  		loginPage1.gmailLogin(prop.getProperty("Gusername1"),prop.getProperty("Gpassword"));
  		*/
  		WebUtility.Click(dashboard);
  		WebUtility.Wait();
  		WebUtility.Click(movies);
  		WebUtility.Wait();
  		
  		WebUtility.javascript(DropdwnOptions);
  		WebUtility.javascript(SingleLanding);
  		WebUtility.Wait();

  		WebUtility.Click(SingleLandingTitle);
  		SingleLandingTitle.sendKeys(Keys.CONTROL + "a");
  		SingleLandingTitle.sendKeys(Keys.DELETE);  		
  		SingleLandingTitle.sendKeys(movieSingleLandingTitle);
  		Thread.sleep(3000);
  		
  		SingleShortdesc.sendKeys("When Thor's evil brother, Loki (Tom Hiddleston), gains access to the unlimited power of the energy cube called the Tesseract");
  		WebUtility.Wait();
  		SingleSubType.click();
  		dropDownList.get(0).click();

  		SinglePrimaryGenre.click();
  		dropDownList.get(0).click();

  		SingleAudioLanguage.click();
  		dropDownList.get(0).click();

  		SingleNextBtn.click();
  		WebUtility.Wait(); 

  		WebUtility.Click(SingleMarkAsDone);
  		WebUtility.Wait(); 	 

  	}

  	public void SingleLicense() throws InterruptedException {

		WebUtility.javascript(License);
		WebUtility.Wait();
		WebUtility.javascript(createLicense);
		WebUtility.Wait();
		licenseSetName.sendKeys("Automation Test Data");

		WebUtility.Click(licenseFromDate);
		WebUtility.Wait();
		datePicker.sendKeys(Keys.ARROW_RIGHT);
		datePicker.sendKeys(Keys.ESCAPE);

		WebUtility.Click(licenseToDate);
		WebUtility.Wait();
		datePicker.sendKeys(Keys.ARROW_RIGHT);
		datePicker.sendKeys(Keys.ARROW_RIGHT);
		datePicker.sendKeys(Keys.ESCAPE);

		WebUtility.Click(Country);
  		WebUtility.Wait();
        for (int x = 0; x < dropDownList.size(); x++) {
        	if(x<4) {
            WebUtility.Click(dropDownList.get(x));
        	}
        	else {
        		break;
        	}
        }
  	
  		WebUtility.Click(BusinessType);
  		WebUtility.Wait();
  		WebUtility.Click(dropDownList.get(0));
  		
        WebUtility.Click(licenseContentAgeRatingId);	
        WebUtility.Wait();
        licenseContentAgeRatingId.sendKeys(Keys.ARROW_DOWN);
        licenseContentAgeRatingId.sendKeys(Keys.ENTER);

  		createLicenseSave1.click();
  		WebUtility.Wait();
  		
  		//Creating Multiple License
  		WebUtility.javascript(License);
  		WebUtility.Wait();	
  		WebUtility.javascript(createLicense);
		WebUtility.Wait();
		licenseSetName.sendKeys("Automation Test Data II");

		WebUtility.Click(licenseFromDate);
		WebUtility.Wait();
		datePicker.sendKeys(Keys.ARROW_RIGHT);
		datePicker.sendKeys(Keys.ARROW_RIGHT);
		datePicker.sendKeys(Keys.ARROW_RIGHT);
		datePicker.sendKeys(Keys.ESCAPE);

		WebUtility.Click(licenseToDate);
		WebUtility.Wait();
		datePicker.sendKeys(Keys.ARROW_RIGHT);
		datePicker.sendKeys(Keys.ARROW_RIGHT);
		datePicker.sendKeys(Keys.ARROW_RIGHT);
		datePicker.sendKeys(Keys.ARROW_RIGHT);
		datePicker.sendKeys(Keys.ESCAPE);

		WebUtility.Click(Country);
		WebUtility.Wait();
        for (int x = 0; x < dropDownList.size(); x++) {
        	if(x<4) {
            WebUtility.Click(dropDownList.get(x));
        	}
        	else {
        		break;
        	}
        }
  	
  		WebUtility.Click(BusinessType);
  		WebUtility.Wait();
  		WebUtility.Click(dropDownList.get(0));

        WebUtility.Click(licenseContentAgeRatingId);	
        WebUtility.Wait();
        licenseContentAgeRatingId.sendKeys(Keys.ARROW_DOWN);
        licenseContentAgeRatingId.sendKeys(Keys.ENTER);
        
  		createLicenseSave1.click();
  		WebUtility.Wait();
  		
  		WebUtility.Wait();
  		String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
  		sa.assertTrue(doneButtonActiveColor.contains("255,255,255") || doneButtonActiveColor.contains("255, 255, 255"), "Movie License SingleLanding Mark As Done Assertion Fails");
  		WebUtility.Click(doneButtonActive);	
  		WebUtility.Wait();
  		try {
  			TestUtil.captureScreenshot("Movie doneButtonActive - License Section","Movie");
  		} catch (IOException e) {
  			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
  		}
  		
  	}

  	public void SingleImages() throws InterruptedException {
  		
  		WebUtility.Click(Images);
  		WebUtility.Wait();
  		
  		SingleUploadimage.sendKeys(System.getProperty("user.dir") + "\\images\\movies\\Tanuwedsmanu3.jpg");
  		WebUtility.Wait();

  		// ---------------------------Images Set 2
  		// ----------------------------------------------
  		try {
  			WebUtility.element_to_be_fluentwait(imagesCreateNewSet);
  			WebUtility.Click(imagesCreateNewSet);

  			WebUtility.Click(newSetName);
  			newSetName.sendKeys("Automation Test Data");

  			newSetCountry.click();
  			WebUtility.Click(dropDownList.get(0));
  			closeDropdown.click();

  			newSetPlatform.click();
  			WebUtility.Click(dropDownList.get(0));
  			closeDropdown.click();

  			newSetGender.click();
  			WebUtility.Click(dropDownList.get(0));
  			closeDropdown.click();

  			newSetGenre.click();
  			WebUtility.Click(dropDownList.get(0));
  			closeDropdown.click();

  			newSetAgeGroup.click();
  			WebUtility.Click(dropDownList.get(0));
  			closeDropdown.click();

  			newSetLanguage.click();
  			WebUtility.Click(dropDownList.get(0));
  			closeDropdown.click();

  			newSetOthers.click();
  			newSetOthers.sendKeys("Automation Test Data");

  			WebUtility.Click(save);
  			WebUtility.Wait();

  			WebUtility.Click(imageSets.get(1));
  			WebUtility.Wait();

  			SingleUploadimage.sendKeys(System.getProperty("user.dir") + "\\images\\movies\\Tanuwedsmanu3.jpg");
  			WebUtility.Wait();

  		} catch (Exception e) {
  			e.printStackTrace();
  		}

  		// -------------------------- Image Set 3
  		// -----------------------------------------------
  		try {

  			WebUtility.element_to_be_fluentwait(imagesCreateNewSet);
  			WebUtility.Click(imagesCreateNewSet);

  			WebUtility.Click(newSetName);
  			newSetName.sendKeys("Automation Test Data II");

  			newSetCountry.click();
  			WebUtility.Click(dropDownList.get(1));
  			closeDropdown.click();

  			newSetPlatform.click();
  			WebUtility.Click(dropDownList.get(1));
  			closeDropdown.click();

  			newSetGender.click();
  			WebUtility.Click(dropDownList.get(1));
  			closeDropdown.click();

  			newSetGenre.click();
  			WebUtility.Click(dropDownList.get(1));
  			closeDropdown.click();

  			newSetAgeGroup.click();
  			WebUtility.Click(dropDownList.get(1));
  			closeDropdown.click();

  			newSetLanguage.click();
  			WebUtility.Click(dropDownList.get(1));
  			closeDropdown.click();

  			newSetOthers.click();
  			newSetOthers.sendKeys("Automation Test Data II");

  			WebUtility.Click(save);

  			WebUtility.Click(imageSets.get(2));

  			js.executeScript("window.scrollTo(0,0)");
  			WebUtility.Wait();

  			SingleUploadimage.sendKeys(System.getProperty("user.dir") + "\\images\\movies\\Tanuwedsmanu3.jpg");
  			WebUtility.Wait();
  		} catch (Exception e) {
  			e.printStackTrace();
  		}

  		// ---------------------- Inactivate Image Set ------------------------
  		try {
  			js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
  			WebUtility.Wait();
  			WebUtility.Click(imageSetActiveButton);
  			WebUtility.Wait();
  			WebUtility.Click(dropDownList.get(0));
  			WebUtility.javascript(yes.get(0));
  			WebUtility.Wait();
  		} catch (Exception e) {
  			e.printStackTrace();
  		}

  		// ------------------------ Delete Image Set -----------------------------
  		try {
  			removeSet.get(1).click();
  			WebUtility.Wait();
  			yes.get(0).click();
  			WebUtility.Wait();
  		} catch (Exception e) {
  			e.printStackTrace();
  		}
  		
  		SingleMarkAsDone.click();
  		WebUtility.Wait();
  		
  	} 
  	
  	public void SingleSeo() throws InterruptedException {

  		SingleSeoSection.click();
  		WebUtility.Wait();
  		SingleSeoTitle.click();
  		SingleSeoTitle.sendKeys(Keys.CONTROL + "a");
  		SingleSeoTitle.sendKeys(Keys.DELETE);
  		SingleSeoTitle.sendKeys("SEO title for data");
  		Thread.sleep(2000);
  		SingleSeoMataDesc.click();
  		SingleSeoMataDesc.sendKeys(Keys.CONTROL + "a");
  		SingleSeoMataDesc.sendKeys(Keys.DELETE);
  		SingleSeoMataDesc.sendKeys("SEO description data");
  		Thread.sleep(2000);
  		SingleSeoMeta.click();
  		SingleSeoMeta.sendKeys(Keys.CONTROL + "a");
  		Thread.sleep(2000);
  		SingleSeoMeta.sendKeys(Keys.DELETE);
  		SingleSeoMeta.sendKeys("SEO Meta description data field");
  		Thread.sleep(2000);
  		SingleHeading.click();
  		SingleHeading.sendKeys(Keys.CONTROL + "a");
  		SingleHeading.sendKeys(Keys.DELETE);
  		SingleHeading.sendKeys("Heading new");
  		Thread.sleep(2000);
  		SingleHeading2.click();
  		SingleHeading2.sendKeys(Keys.CONTROL + "a");
  		SingleHeading2.sendKeys(Keys.DELETE);
  		SingleHeading2.sendKeys("New second heading for seo data");
  		Thread.sleep(2000);
  			
  		SingleVideo.click();
  		SingleWeb.click();
  		SingleBread.click();
  		SingleImage.click();
  		SingleOrganization.click();
  		SingleMarkAsDone.click();
  		WebUtility.Wait();

  	}

  	public void SingleMapcontent() throws InterruptedException {

  		WebUtility.javascript(SingleMapContent);
  		WebUtility.Wait();

  		WebUtility.javascript(QuickAddContent);
  		WebUtility.Wait();

  		WebUtility.javascript(Checkbox);

  		WebUtility.javascript(AssignContent);
  		WebUtility.Wait();

  		WebUtility.javascript(LinkContent);
  		WebUtility.Wait();

  		WebUtility.javascript(LinkAddContent);
  		WebUtility.Wait();
  		WebUtility.javascript(LinkCheckbox);
  		WebUtility.javascript(LinkAssignContent);
  		WebUtility.Wait();

  		WebUtility.javascript(MapMarkAsDone);
  		WebUtility.Wait();

  	}

  	public void SingleChecklist() throws InterruptedException {
  	/*
  		//--Verifying Movie SingleLanding Draft Status
  		try
  		{
  			contentProperties.click();
  			WebUtility.element_to_be_visible(contentStatus);
  			TestUtil.captureScreenshot("Movie SingleLanding Draft Status","Movie");
  			String expectedStatus = "Draft";
  			String actualStatus = contentStatus.getText();
  			sa.assertEquals(actualStatus,expectedStatus,"Movie SingleLanding Draft Status is Failed");
  			Reporter.log("Movie SingleLanding Draft Status is Passed",true);	
  		}
  		catch (Exception e)
  		{
  			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
  		}
  		
  		WebUtility.javascript(SingleChecklistSection);
  		WebUtility.Wait();
  		WebUtility.javascript(SubmitToReview);
  		WebUtility.Wait();
  		WebUtility.javascript(SubmitYesBtn);
  		WebUtility.Wait();
  		WebUtility.javascript(SubmitOk);
  		WebUtility.Wait();
  		
  		//--Verifying Movie SingleLanding Submit to Review status
  		try
  		{
  			contentProperties.click();
  			WebUtility.element_to_be_visible(contentStatus);
  			TestUtil.captureScreenshot("Movie SingleLanding Submitted To Review Status","Movie");
  			String expectedStatus = "Submitted To Review";
  			String actualStatus = contentStatus.getText();
  			sa.assertEquals(actualStatus,expectedStatus,"Movie SingleLanding Submitted To Review Status is Failed");
  			Reporter.log("Movie SingleLanding Submitted To Review Status is Passed",true);	
  		}
  		catch (Exception e)
  		{
  			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
  		}
          
  		logoutlink.click();
  		logoutbutton.click();
  		WebUtility.clear_cache();
  		driver.get(prop.getProperty("url"));
  		loginPage1.gmailLogin(prop.getProperty("Gusername3"),prop.getProperty("Gpassword"));
  		WebUtility.Wait();
  		WebUtility.Click(movies);
  		WebUtility.Wait();
  		Search.sendKeys(movieSingleLandingTitle);
  		Thread.sleep(1000);
  		WebUtility.Wait();
  		WebUtility.Click(View);
  		WebUtility.Wait();	
  		editMovie.click();
  		WebUtility.Wait();
  		editMovieSingleLanding.click();
  		WebUtility.Click(yes.get(0));
  		WebUtility.Wait();
  		WebUtility.javascript(SingleChecklistSection);
  		WebUtility.Wait();
  		NeedWorkReason.click();
  		List<WebElement> countrylist = driver.findElements(By.xpath("//li[@role='option']"));
  		System.out.println((countrylist.size()));
  		for(WebElement e22:countrylist)
  		{
  			System.out.println(e22.getText());
  			String countryname= e22.getText();
  			if(countryname.contains("Missing metadata"))
  			{
  				e22.click();
  				break;
  			}
  		}		

  		Thread.sleep(2000);
  		WebUtility.Click(NeedWorkbtn);
  		WebUtility.Wait();
  		WebUtility.Click(NeedWorkYes);
  		WebUtility.Wait();
  		WebUtility.Click(NeedWorkOk);
  		WebUtility.Wait();
  		
  		//--Verifying Movie SingleLanding Need Work status
  		try
  		{
  			contentProperties.click();
  			WebUtility.element_to_be_visible(contentStatus);
  			TestUtil.captureScreenshot("Movie SingleLanding Need Work Status","Movie");
  			String expectedStatus = "Need Work";
  			String actualStatus = contentStatus.getText();
  			sa.assertEquals(actualStatus,expectedStatus,"Movie SingleLanding Need Work Status is Failed");
  			Reporter.log("Movie SingleLanding Need Work Status is Passed",true);	
  		}
  		catch (Exception e)
  		{
  			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
  		}
          
  		logoutlink.click();
  		logoutbutton.click();
  		WebUtility.clear_cache();
  		driver.get(prop.getProperty("url")); 
  		loginPage1.gmailLogin(prop.getProperty("Gusername1"),prop.getProperty("Gpassword"));
  		WebUtility.Wait();
  		WebUtility.Click(movies);
  		WebUtility.Wait();
  		Search.sendKeys(movieSingleLandingTitle);
  		Thread.sleep(1000);
  		WebUtility.Wait();
  		WebUtility.Click(View);
  		WebUtility.Wait();
  		editMovie.click();
  		WebUtility.Wait();
  		editMovieSingleLanding.click();
  		yes.get(0).click();
  		WebUtility.Wait();
  		QuickSubtype.click();
  		dropDownList.get(1).click();
  		WebUtility.Wait();
  		NextBtn.click();	
  		WebUtility.Wait();
  		MarkAsDone.click();
  		WebUtility.Wait();
  		WebUtility.javascript(SingleChecklistSection);
  		WebUtility.Wait();
  		WebUtility.javascript(SubmitToReview);
  		WebUtility.Wait();
  		WebUtility.javascript(SubmitYesBtn);
  		WebUtility.Wait();
  		WebUtility.javascript(SubmitOk);
  		WebUtility.Wait();

  		//--Verifying Movie SingleLanding Submit to Review status
  		try
  		{
  			contentProperties.click();
  			WebUtility.element_to_be_visible(contentStatus);
  			TestUtil.captureScreenshot("Movie SingleLanding Submitted To Review Status","Movie");
  			String expectedStatus = "Submitted To Review";
  			String actualStatus = contentStatus.getText();
  			sa.assertEquals(actualStatus,expectedStatus,"Movie SingleLanding Submitted To Review Status is Failed");
  			Reporter.log("Movie SingleLanding Submitted To Review Status is Passed",true);	
  		}
  		catch (Exception e)
  		{
  			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
  		}
          */
        logoutlink.click();
  		logoutbutton.click();
  		WebUtility.clear_cache();
  		driver.get(prop.getProperty("url")); 
  		loginPage1.gmailLogin(prop.getProperty("Gusername3"),prop.getProperty("Gpassword"));Thread.sleep(4000);
  		WebUtility.Wait();
  		WebUtility.Click(movies);
  		WebUtility.Wait();
  		Search.sendKeys(movieSingleLandingTitle);
  		Thread.sleep(1000);
  		WebUtility.Wait();
 		WebUtility.Click(View);
  		WebUtility.Wait();
  		editMovie.click();
  		WebUtility.Wait();
  		editMovieSingleLanding.click();
  		yes.get(0).click();
  		WebUtility.Wait();
  		WebUtility.Click(SingleChecklistSection);
  		WebUtility.Wait();
  		WebUtility.Click(PublishCountryDropdown);
        WebUtility.Click(dropDownList.get(0));
        /*
  		List<WebElement> countryList = driver.findElements(By.xpath("//li[@role='option']"));
  		for(WebElement countries:countryList)
  		{
  			countries.click();
  		}
*/
  		WebUtility.Click(PublishedBtn);
  		WebUtility.Wait();
  		WebUtility.Click(PublishedYes);
  		WebUtility.Click(Published);
  		WebUtility.Wait();
  		
  		//--Verifying Movie SingleLanding Published status
  		try
  		{
  				contentProperties.click();
  				WebUtility.element_to_be_visible(contentStatus);
  				TestUtil.captureScreenshot("Movie Single Landing Published Status","Movie");
  				String expectedStatus = "Published";
  				String actualStatus = contentStatus.getText();
  				sa.assertEquals(actualStatus,expectedStatus,"Movie SingleLanding Published Status is Failed");
  				Reporter.log("Movie SingleLanding Published Status is Passed",true);	
  		}
  		catch (Exception e)
  		{
  				System.out.println("Screenshot Capture Failed ->" + e.getMessage());
  		}
  		
  		WebUtility.Wait();
  		WebUtility.javascript(SingleChecklistSection);
  		WebUtility.Wait();
  		
  		//Unpublished the Published Movie ------------------------------------
  		WebUtility.Click(UnpublishContent);
  		WebUtility.Wait();    
  		WebUtility.Click(Reason);
  		List<WebElement> countrylist1 = driver.findElements(By.xpath("//li[@role='option']"));
  		System.out.println((countrylist1.size()));
  		for(WebElement e22:countrylist1)
  		{
  			System.out.println(e22.getText());
  			String countryname= e22.getText();
  			if(countryname.contains("On Hold"))
  			{
  				e22.click();
  				break;
  			}
  		}		

 		WebUtility.Click(YesBtn);
 		WebUtility.Click(Yes);
  		WebUtility.Wait();
  		
  		//--Verifying Movie SingleLanding UnPublished status
  		try
  		{
  			contentProperties.click();
  			WebUtility.element_to_be_visible(contentStatus);
  			TestUtil.captureScreenshot("Movie SingleLanding Unpublished Status","Movie");
  			String expectedStatus = "Unpublished";
  			String actualStatus = contentStatus.getText();
  			sa.assertEquals(actualStatus,expectedStatus,"Movie SingleLanding Unpublished Status is Failed");
  			Reporter.log("Movie SingleLanding Unpublished Status is Passed",true);	
  		}
  		catch (Exception e)
  		{
  			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
  		}
          
          // Archive unpublished content -------------------------------------     
  		MoviesListing.click();
  		WebUtility.Wait();
  		Search.sendKeys(movieSingleLandingTitle);
  		Thread.sleep(1000);
  		WebUtility.Wait();
  		Archive.get(0).click();				
  		ArchiveYes.click();
  		WebUtility.Wait();
  		WebUtility.Wait();
  		WebUtility.Click(View);
  		WebUtility.Wait();
  		
  		//--Verifying Movie SingleLanding Archive status
  		try
  		{
  			contentProperties.click();
  			WebUtility.element_to_be_visible(contentStatus);
  			TestUtil.captureScreenshot("Movie SingleLanding Archived Status","Movie");
  			String expectedStatus = "Archived";
  			String actualStatus = contentStatus.getText();
  			sa.assertEquals(actualStatus,expectedStatus,"Movie SingleLanding Archived Status is Failed");
  			Reporter.log("Movie SingleLanding Archived Status is Passed",true);	
  		}
  		catch (Exception e)
  		{
  			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
  		}
  		WebUtility.Wait();
  		// Restore Content to draft mode ---------------------------------------
  		MoviesListing.click();
  		WebUtility.Wait();
  		Search.sendKeys(movieSingleLandingTitle);
  		Thread.sleep(1000);
  		WebUtility.Wait();
  		WebUtility.javascript(Restore);			
  		WebUtility.Wait();
  		WebUtility.javascript(RestoreYes);
  		WebUtility.Wait();
  		WebUtility.Click(View);
  		WebUtility.Wait();
  		editMovie.click();
  		WebUtility.Wait();
  		editMovieSingleLanding.click();
  		yes.get(0).click();
  		WebUtility.Wait();
  		
  		//--Verifying Movie SingleLanding Restore Status
  		try
  		{
  			contentProperties.click();
  			WebUtility.element_to_be_visible(contentStatus);
  			TestUtil.captureScreenshot("Movie SingleLanding Restore Status","Movie");
  			String expectedStatus = "Draft";
  			String actualStatus = contentStatus.getText();
  			sa.assertEquals(actualStatus,expectedStatus,"Movie SingleLanding Restore Status is Failed");
  			Reporter.log("Movie SingleLanding Restore Status is Passed",true);	
  		}
  		catch (Exception e)
  		{
  			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
  		}
  		
  		WebUtility.Wait();
  		WebUtility.javascript(SingleChecklistSection);
  		WebUtility.Wait();

  		// Published Content ----------------------------------------------      
  		WebUtility.Click(PublishCountryDropdown);
        WebUtility.Click(dropDownList.get(0));
        /*
  		List<WebElement> countryList1 = driver.findElements(By.xpath("//li[@role='option']"));
  		for(WebElement countries:countryList1)
  		{
  				countries.click();
  		}
  		*/
  		WebUtility.Wait();
  		WebUtility.Click(PublishedBtn);
  		WebUtility.Wait();
  		WebUtility.Click(PublishedYes);
  		WebUtility.Wait();
  		WebUtility.Click(Published);
  		WebUtility.Wait();
  		
  		//--Verifying Movie SingleLanding Published status
  		try
  		{
  			contentProperties.click();
  			WebUtility.element_to_be_visible(contentStatus);
  			TestUtil.captureScreenshot("Movie SingleLanding Published Status","Movie");
  			String expectedStatus = "Published";
  			String actualStatus = contentStatus.getText();
  			sa.assertEquals(actualStatus,expectedStatus,"Movie SingleLanding Published Status is Failed");
  			Reporter.log("Movie SingleLanding Published Status is Passed",true);	
  		}
  		catch (Exception e)
  		{
  			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
  		}
          
  		// Make some changes to republished the content ----------------------------
  		logoutlink.click();
  		logoutbutton.click();
  		WebUtility.clear_cache();
  		driver.get(prop.getProperty("url")); 
  		loginPage1.gmailLogin(prop.getProperty("Gusername1"),prop.getProperty("Gpassword"));
  		WebUtility.Wait();
  		WebUtility.Click(movies);
  		WebUtility.Wait();
  		Search.sendKeys(movieSingleLandingTitle);
  		Thread.sleep(1000);
  		WebUtility.Wait();
  		WebUtility.Click(View);
  		WebUtility.Click(editMovie);
  		WebUtility.Wait();
  		editMovieSingleLanding.click();
  		yes.get(0).click();
  		
  		WebUtility.Wait();
  		QuickSubtype.click();
  		dropDownList.get(2).click();
  		WebUtility.Wait();
  		NextBtn.click();
  		WebUtility.Wait();
  		MarkAsDone.click();
  		WebUtility.Wait();
  		
  		//--Verifying Movie SingleLanding Changed status
  		try
  		{
  			contentProperties.click();
  			WebUtility.element_to_be_visible(contentStatus);
  			TestUtil.captureScreenshot("Movie SingleLanding Changed Status","Movie");
  			String expectedStatus = "Changed";
  			String actualStatus = contentStatus.getText();
  			sa.assertEquals(actualStatus,expectedStatus,"Movie SingleLanding Changed Status is Failed");
  			Reporter.log("Movie SingleLanding Changed Status is Passed",true);	
  		}
  		catch (Exception e)
  		{
  			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
  		}
          
  		logoutlink.click();
  		logoutbutton.click();
  		WebUtility.clear_cache();
  		driver.get(prop.getProperty("url")); 
  		loginPage1.gmailLogin(prop.getProperty("Gusername3"),prop.getProperty("Gpassword"));
  		WebUtility.Wait();
  		WebUtility.Click(movies);
  		WebUtility.Wait();
  		Search.sendKeys(movieSingleLandingTitle);
  		Thread.sleep(1000);
  		WebUtility.Wait();
  		WebUtility.Click(View);
  		WebUtility.Wait();
  		editMovie.click();
  		WebUtility.Wait();
  		editMovieSingleLanding.click();
  		yes.get(0).click();
  		WebUtility.Wait();
  		WebUtility.javascript(SingleChecklistSection);
  		WebUtility.Wait();

  		// Republished the content ------------------------------
  		WebUtility.javascript(Republished);            

  		WebUtility.javascript(RepublishedYes);

  		WebUtility.javascript(RepublishedConfirm);
  		WebUtility.Wait();

  		WebUtility.javascript(PublishedAgain);
  		WebUtility.Wait();

  		//--Verifying Movie SingleLanding Republished status
  		try
  		{
  			contentProperties.click();
  			WebUtility.element_to_be_visible(contentStatus);
  			TestUtil.captureScreenshot("Movie SingleLanding Republished Status","Movie");
  			String expectedStatus = "Published";
  			String actualStatus = contentStatus.getText();
  			sa.assertEquals(actualStatus,expectedStatus,"Movie SingleLanding Republished Status is Failed");
  			Reporter.log("Movie SingleLanding Republished Status is Passed",true);	
  		}
  		catch (Exception e)
  		{
  			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
  		}
  		
  		//--Clone SingleLanding Movie
  		MoviesListing.click();
  		WebUtility.Wait();
  		Search.sendKeys(movieSingleLandingTitle);
  		Thread.sleep(1000);
  		WebUtility.Wait();
  		Clone.get(0).click();
  		WebUtility.Wait();
  		WebUtility.javascript(CloneYes);
  		WebUtility.Wait();		
  		WebUtility.Click(SingleChecklistSection);
  		WebUtility.Wait();
  		WebUtility.Click(Scheduled_plus);
  		WebUtility.Wait();
  		
        //System.out.println("--------> Current Time = "+currentTime.getAttribute("innerText").substring(20, 28));
        //TimeString  = currentTime.getAttribute("innerText").substring(20, 27);
    	//js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
    	
  		WebUtility.Click(ScheduledDate);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        WebUtility.Click(datePickerMinutes);
        actionProvider.moveToElement(datePickerMinutesClock).click().build().perform();      
        datePicker.sendKeys(Keys.ESCAPE); 
         
  		WebUtility.Click(ScheduledCountry);
  		WebUtility.Wait();
  		WebUtility.Click(dropDownList.get(0));
  		
  		WebUtility.javascript(ScheduledBtn);
  		WebUtility.Wait();
  		WebUtility.javascript(ScheduledYes);
  		WebUtility.Wait();
  		WebUtility.javascript(OkBtn);
  		WebUtility.Wait();
  		
    	js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
        WebUtility.Wait();
        js.executeScript("window.scrollTo(0,0);");
        WebUtility.Wait();
        
     
        //System.out.println("--------> Scheduled Time = "+scheduledTime.getAttribute("innerText").substring(12, 20));
        //String scheduledTimeString = scheduledTime.getAttribute("innerText").substring(12, 20);
    	//js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
    	//sa.assertTrue(scheduledTimeString.contains(currentTimeString),"Movie Single Landing - Current Time and Scheduled Time are not Equal");
  		//--Verifying Movie SingleLanding Scheduled status
  		try
  		{
  			contentProperties.click();
  			WebUtility.Wait();
  			WebUtility.element_to_be_visible(contentStatus);
  			TestUtil.captureScreenshot("Movie SingleLanding Scheduled Status","Movie");
  			String expectedStatus = "Scheduled";
  			String actualStatus = contentStatus.getText();
  			sa.assertEquals(actualStatus,expectedStatus,"Movie SingleLanding Scheduled Status is Failed");
  			Reporter.log("Movie SingleLanding Scheduled Status is Passed",true);	
  		}
  		catch (Exception e)
  		{
  			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
  		}
  		
  		PublishedHistory.click();
  		js.executeScript("window.scrollBy(0,1000)");
  		Thread.sleep(5000);

  		movieBreadcrumb.click();
  		WebUtility.Wait();
  		Search.sendKeys(movieSingleLandingTitle);
  		Thread.sleep(1000);
  		WebUtility.Wait();
  		WebUtility.Click(ViewDetails);
  		WebUtility.Wait();
  		LinkMovies.click();
  		WebUtility.Wait();
  		movieBreadcrumb.click();
//  		JavascriptExecutor j = (JavascriptExecutor)driver;
//  		j.executeScript("arguments[0].click();", movieBreadcrumb);
  		WebUtility.Wait();

  	}
  	
  	public void MovieSingleLandingReleatedContent() throws InterruptedException{

		WebUtility.Click(relatedContent);
		/*
		WebUtility.Click(relatedContentAssignTvShows);

		WebUtility.element_to_be_visible(relatedContentAddTvShows);
		WebUtility.Click(relatedContentAddTvShows);

		WebUtility.Wait();
		checkBox.get(0).click();
		checkBox.get(1).click();
		WebUtility.Click(assignRelatedContent);
		WebUtility.Click(deleteRelatedContent);
		WebUtility.Wait();
		WebUtility.Click(yes.get(0));
		WebUtility.Wait();
*/
		WebUtility.Click(relatedContentAssignVideos);
		WebUtility.element_to_be_visible(relatedContentAddVideos);
		WebUtility.Click(relatedContentAddVideos);

		WebUtility.Wait();
		checkBox.get(0).click();
		checkBox.get(1).click();
		WebUtility.Click(assignRelatedContentVideos);
		WebUtility.Wait();
		WebUtility.Click(deleteRelatedContent);
		WebUtility.Wait();
		WebUtility.Click(yes.get(0));
		WebUtility.Wait();

		WebUtility.Click(relatedContentAssignMovies);
		WebUtility.element_to_be_visible(relatedContentAddMovies);
		WebUtility.Click(relatedContentAddMovies);

		WebUtility.Wait();
		checkBox.get(0).click();
		checkBox.get(1).click();
		WebUtility.Click(assignRelatedContentMovies);
		WebUtility.Wait();
		WebUtility.Click(deleteRelatedContent);
		WebUtility.Wait();
		WebUtility.Click(yes.get(0));
		WebUtility.Wait();
/*
		WebUtility.Click(relatedContentAssignSeasons);
		WebUtility.element_to_be_visible(relatedContentAddSeasons);
		WebUtility.Click(relatedContentAddSeasons);

		WebUtility.Wait();
		checkBox.get(0).click();
		checkBox.get(1).click();
		WebUtility.Click(assignRelatedContent);
		WebUtility.Click(deleteRelatedContent);
		WebUtility.Wait();
		WebUtility.Click(yes.get(0));
		WebUtility.Wait();

		WebUtility.Click(relatedContentAssignEpisodes);
		WebUtility.element_to_be_visible(relatedContentAddEpisodes);
		WebUtility.Click(relatedContentAddEpisodes);

		WebUtility.Wait();
		checkBox.get(0).click();
		checkBox.get(1).click();
		WebUtility.Click(assignRelatedContent);
		WebUtility.Click(deleteRelatedContent);
		WebUtility.Wait();
		WebUtility.Click(yes.get(0));
		WebUtility.Wait();
*/
		String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
		sa.assertTrue(doneButtonActiveColor.contains("255,255,255") || doneButtonActiveColor.contains("255, 255, 255") ,"Movie Single Landing Related Content Content Mark As Done Assertion Fails");
		WebUtility.Click(doneButtonActive);
		WebUtility.Click(relatedContentBack);
		WebUtility.Wait();
	}
  	
  	//------------------------------------------------------------Movie Listing Page Tests ---------------------------------------------------------------
      public void MovieListingSearch() throws InterruptedException,Exception {	
  /*
  		// -------- Edit Flow For Testing Purposes ----------------------------

  		WebUtility.Click(dashboard);
  		WebUtility.Click(movies);

  		Search.click();
  		Search.sendKeys(movieTitle);
  		Thread.sleep(1000);
  		WebUtility.Wait();
  		WebUtility.element_to_be_clickable(editButton.get(0));
  		editButton.get(0).click();
  		WebUtility.Wait();
  		externalID = js.executeScript("return arguments[0].childNodes[1].innerText", movieExternalID).toString();
  	*/
  		WebUtility.Click(dashboard);
  		WebUtility.Click(movies);
  		
  		WebUtility.Click(Search);
  		Search.sendKeys(Keys.CONTROL+"a");
  		Search.sendKeys(Keys.BACK_SPACE);
  		Search.sendKeys(movieTitle.toLowerCase());
  		Thread.sleep(1000);
  		WebUtility.Wait();
  		WebUtility.Wait();
  		
  		WebUtility.Click(Search);
  		Search.sendKeys(Keys.CONTROL+"a");
  		Search.sendKeys(Keys.BACK_SPACE);
  		Search.sendKeys(externalID);
  		Thread.sleep(1000);
  		WebUtility.Wait();
  		WebUtility.Wait();
  		
  		try {
  			TestUtil.captureScreenshot("Movie Search", "Movie");
  		} catch (IOException e) {
  			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
  		}

  		
  	}
      
      //--------------------- Movies Listing Page Test -----------------------------------------------------------
     	public void MovieListingPage() throws InterruptedException{
  		dashboard.click();
  		WebUtility.Wait();
  		WebUtility.Click(movies);
  		WebUtility.Wait();

  		WebUtility.Click(Search);
  		Search.sendKeys(Keys.CONTROL+"a");
  		Search.sendKeys(Keys.BACK_SPACE);
  		Search.sendKeys(movieTitle);
  		Thread.sleep(1000);
  		WebUtility.Wait();
  		WebUtility.Wait();

  		WebUtility.Click(listingExpiringLicense);
  		WebUtility.Wait();
  		Thread.sleep(3000);
  		WebUtility.Click(listingBackArrow);
  		WebUtility.Wait();

  		WebUtility.Click(Search);
  		Search.sendKeys(movieTitle);
  		Thread.sleep(1000);
  		WebUtility.Wait();
  		WebUtility.Wait();

  		WebUtility.Click(listingLinkedMovies);
  		WebUtility.Wait();
  		Thread.sleep(3000);
  		WebUtility.Click(listingBackArrow);
  		WebUtility.Wait();

  		WebUtility.Click(Search);
  		Search.sendKeys(movieTitle);
  		Thread.sleep(1000);
  		WebUtility.Wait();
  		WebUtility.Wait();

  		WebUtility.Click(listingPublishedHistory);
  		WebUtility.Wait();
  		WebUtility.Click(listingClosePublishedHistory);
  		WebUtility.Wait();

  		WebUtility.Click(listingViewDetails);
  		WebUtility.Wait();
  		WebUtility.Click(listingHideDetails);
  		WebUtility.Wait();
  		
  		try {
  		WebUtility.Click(listingLicenseCountries);
  		WebUtility.Wait();
  		WebUtility.Click(listingCloseLicenseCountries);
  		WebUtility.Wait();
  		}
  		catch(Exception e) {
  			e.printStackTrace();
  		}

  		listingDraftStatus.click();
  		WebUtility.Wait();
  		Thread.sleep(3000);

  		listingAllStatus.click();
  		WebUtility.Wait();
  		Thread.sleep(3000);

  		listingChangedStatus.click();
  		WebUtility.Wait();
  		Thread.sleep(3000);

  		listingPublishedStatus.click();
  		WebUtility.Wait();
  		Thread.sleep(3000);

  		listingUnpublishedStatus.click();
  		WebUtility.Wait();
  		Thread.sleep(3000);

  		listingNeedWorkStatus.click();
  		WebUtility.Wait();
  		Thread.sleep(3000);

  		listingScheduledStatus.click();
  		WebUtility.Wait();
  		Thread.sleep(3000);

  		listingSubmittedToReviewStatus.click();
  		WebUtility.Wait();
  		Thread.sleep(3000);

  		listingArchivedStatus.click();
  		WebUtility.Wait();
  		Thread.sleep(3000);

  		listingPublishingQueueStatus.click();
  		WebUtility.Wait();
  		Thread.sleep(3000);

     	}
     	
    	public void AssertionResults() throws Exception{
    		sa.assertAll();
    	}
  } 
