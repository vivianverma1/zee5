package com.zee5.qa.pages;

import java.io.IOException;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.asserts.SoftAssert;

import com.qa.utilities.WebUtility;
import com.zee5.qa.util.TestUtil;
import com.zee5.qa.base.TestBase;

public class EpisodesPage extends TestBase {
	
	//------------------------------TV Show Titles ---------------------------------------------------------
	
	String tvShowTitle ="The Expanse";
	String tvShowQuickFilingTitle = "The Expanse Quick Filing";
	String tvShowSingleLandingTitle = "The Expanse Single Landing";
	JavascriptExecutor js = (JavascriptExecutor) driver; 
	Actions actionProvider = new Actions(driver);
	String externalID;
	SoftAssert sa = new SoftAssert();
	
	//---------------------------- Episode Properties ----------------------------------------------------------------
    @FindBy(xpath = "//div[contains(@class,'auto-tvShow')]")
    WebElement createTvShow;

    @FindBy(xpath = "//button[contains(@class,'auto-leftTab-TVShowProperties')]")
    WebElement tvShowProperties;

   // @FindBy(xpath = "//span[contains(text(),'Title Summary')]")
    @FindBy(xpath = "//button[contains(@class,'auto-leftTab-TitleSummary')]")  
    WebElement titleSummary;

    @FindBy(xpath = "//input[@name='note']")
    WebElement note;

    @FindBy(xpath = "//input[@name=\"title\"]")
    WebElement title;

    @FindBy(xpath = "//div[contains(@id,'auto-shortDescription')]//child::div[contains(@class,'ql-editor')]")
    WebElement shortDesc;
    
    @FindBy(xpath = "//div[contains(@class,'auto-WebDescription')]//child::div[contains(@class,'ql-editor')]")
    WebElement webDesc;
    
    @FindBy(xpath = "//div[contains(@class,'auto-AppDescription')]//child::div[contains(@class,'ql-editor')]")
    WebElement appDesc;
  
    @FindBy(xpath = "//div[contains(@class,'auto-TVDescription')]//child::div[contains(@class,'ql-editor')]")
    WebElement tvDesc;  

    @FindBy(xpath = "//input[@id='subtype']")
    WebElement subType;
    
    @FindBy(xpath = "//input[@id='category']")
    WebElement category;

    @FindBy(xpath = "//input[@id='primaryGenre']")
    WebElement primaryGenre;
    
    @FindBy(xpath="//div[contains(@class,'auto-primaryGenre')]//descendant::span[@class='MuiChip-label']")
    WebElement primaryGenreAssertion;

    @FindBy(xpath = "//input[@id='secondaryGenre']")
    WebElement secondaryGenre;

    @FindBy(xpath="//div[contains(@class,'auto-SecondaryGenre')]//descendant::span[@class='MuiChip-label']")
    WebElement secondaryGenreAssertion;
    
    @FindBy(xpath = "//input[@id='thematicGenre']")
    WebElement thematicGenre;

    @FindBy(xpath = "//input[@id='settingGenre']")
    WebElement settingGenre;

    @FindBy(xpath = "//input[@id='rcsCategory']")
    WebElement rcsCategory;

    @FindBy(xpath = "//input[@name='creativeTitle']")
    WebElement creativeTitle;

    @FindBy(xpath = "//input[@name='alternatetitle']")
    WebElement alternateTitle;
    
    @FindBy(xpath = "//input[@name='pageTitle']")
    WebElement pageTitle;

    @FindBy(xpath = "//textarea[@name='pageDescription']")
    WebElement pageDescription;

    @FindBy(name = "titleForSocialShare")
    WebElement titleForSocialShare;

    @FindBy(name = "descriptionForSocialShare")
    WebElement descriptionForSocialShare;
     
    @FindBy(name = "dateZee5Published")
    WebElement zee5ReleaseDate;

    @FindBy(name = "telecastDate")
    WebElement telecastDate;

    @FindBy(name = "videoDuration")
    WebElement videoDuration;
    
    @FindBy(xpath = "//input[@name='isMultiAudio']")
    WebElement multiAudio;

    @FindBy(xpath = "//input[@id='audioLanguages']")
    WebElement audioLanguage;
    
    @FindBy(xpath="//div[contains(@class,'auto-AudioLanguage')]//descendant::span[@class='MuiChip-label']")
    WebElement audioLanguagesAssertion;
    
    @FindBy(xpath = "//input[@id='contentLanguage']")
    WebElement contentLanguage;
    
    @FindBy(xpath = "//input[@id='primaryLanguage']")
    WebElement primaryLanguage;

    @FindBy(xpath = "//input[@id='dubbedLanguageTitle']")
    WebElement dubbedLanguageTitle;
    
    @FindBy(xpath="//div[contains(@class,'auto-DubbedLanguageTitle')]//descendant::span[@class='MuiChip-label']")
    WebElement dubbedLanguageAssertion;

    @FindBy(xpath = "//input[@id='originalLanguage']")
    WebElement originalLanguage;
    
    @FindBy(xpath = "//input[@id='subtitleLanguages']")
    WebElement subtitleLanguages;

    @FindBy(xpath = "//input[@id='contentVersion']")
    WebElement contentVersion;

    @FindBy(xpath = "//input[@id='theme']")
    WebElement theme;

    @FindBy(xpath = "//input[@name='contractId']")
    WebElement contractId;

    @FindBy(xpath = "//input[@id='specialCategory']")
    WebElement specialCategory;

    @FindBy(xpath = "//input[@id='specialCategoryCountry']")
    WebElement specialCategoryCountry;

    @FindBy(xpath = "//input[@name='specialCategoryFrom']")
    WebElement specialCategoryFrom;

    @FindBy(xpath = "//input[@name='specialCategoryTo']")
    WebElement specialCategoryTo;
    
	@FindBy(xpath = "//div[contains(@class,'s-badge create-movie-stage ')]")
	WebElement contentStatus; 

    //-------------------------------------------------Episode Properties ->Classification -----------------------------------------------------------------------------------------------
    @FindBy(id = "broadcastState")
    WebElement broadcastState;    
    
    @FindBy(id = "rating")
    WebElement rating;

    @FindBy(id = "contentOwner")
    WebElement contentOwner;

    @FindBy(id = "originCountry")
    WebElement originCountry;
   
    @FindBy(id = "contentProduction")
    WebElement contentProduction;    
    
    @FindBy(name = "upcomingPage")
    WebElement upcomingPage;
  
    @FindBy(id = "ageRating")
    WebElement contentDescriptor;
    
    @FindBy(id = "certification")
    WebElement certification;
    
    @FindBy(id = "emotions")
    WebElement emotions;
    
    @FindBy(id = "contentGrade")
    WebElement contentGrade;
    
    @FindBy(id = "era")
    WebElement era;

    @FindBy(id = "targetAge")
    WebElement targetAge;
    
    @FindBy(id = "targetAudiences")
    WebElement targetAudiences;
    
    @FindBy(xpath = "//input[contains(@id,'tags')]")
    WebElement tags;
    
    @FindBy(name = "digitalKeywords")
    WebElement digitalKeywords;
    
    @FindBy(name = "adaptation")
    WebElement adaptation;
    
    @FindBy(name = "events")
    WebElement events;
    
    @FindBy(id = "productionCompany")
    WebElement productionCompany;
    
    @FindBy(name = "popularityScore")
    WebElement popularityScore;
    
    @FindBy(name = "trivia")
    WebElement trivia;
    
    @FindBy(name = "epgProgramId")
    WebElement epgProgramId;
    
    @FindBy(id = "channel")
    WebElement channel; 
    
    @FindBy(name = "showAirTime")
    WebElement showAirTime;
    
    @FindBy(id = "showAirTimeDays")
    WebElement showAirTimeDays; 

    @FindBy(id = "showOnAir")
    WebElement showOnAir; 
    
    @FindBy(id = "showOnApp")
    WebElement showOnApp; 

    @FindBy(name = "damBmsId")
    WebElement damBmsId; 
    
    @FindBy(xpath = "//input[contains(@id,'awardRecipient')]")
    WebElement awardRecipient;

    @FindBy(id = "awardsCategory")
    WebElement awardsCategory;

    @FindBy(name = "awardsandrecognition")
    WebElement awardHonor;
    
    
  //-------------------------------------------------------Episode Properties -> Player Attributes---
    @FindBy(xpath = "//input[@name='skipAvailable']")
    WebElement skip;

    @FindBy(xpath = "//input[@name='introStartTime']")
    WebElement introStartTime;

    @FindBy(xpath = "//input[@name='introEndTime']")
    WebElement introEndTime;

    @FindBy(xpath = "//input[@name='recapStartTime']")
    WebElement recapStartTime;

    @FindBy(xpath = "//input[@name='recapEndTime']")
    WebElement recapEndTime;

    @FindBy(xpath = "//input[@name='endCreditStartTime']")
    WebElement endCreditStartTime;

    @FindBy(xpath = "//input[@name='adMarker']")
    WebElement adMarker;

    @FindBy(xpath = "//input[@name='skipSongStartTime']")
    WebElement skipSongStartTime;

    @FindBy(xpath = "//input[@name='skipSongEndTime']")
    WebElement skipSongEndTime;
    //---------------------------------------------------------Episode Properties -> Global Fields ---------------------------------------------------------------

    @FindBy(id="auto-country-0-0")
    WebElement globalCountry;
    
    @FindBy(id="country")
    WebElement globalCountryQuickFilling;
    
    @FindBy(name="title")
    WebElement globalTitle;
    
    @FindBy(id="auto-title-0-0")
    WebElement globalTitleQuickFilling;
        
    @FindBy(id="auto-creativeTitle-0-2")
    WebElement globalCreativeTitle;
    
    @FindBy(id="auto-alternativeTitle-0-3")
    WebElement globalAlternativeTitle;    
    
    @FindBy(id="auto-pageTitle-0-4")
    WebElement globalPageTitle;
    
    @FindBy(id="auto-pageDescription-0-5")
    WebElement globalPageDescription;
    
    @FindBy(id="auto-socialShareTitle-0-6")
    WebElement globalSocialShareTitle;
        
    @FindBy(name="telecastDate")
    WebElement globalTelecastDate;
    
    @FindBy(id="subtitleLanguages")
    WebElement globalSubtitleLanguages;
    
    
    @FindBy(id="auto-socialShareDescription-0-10")
    WebElement globalDescriptionForSocialShare;
    
    @FindBy(xpath = "//div[contains(@id,'auto-shortDescription')]//child::div[contains(@class,'ql-editor')]")
    WebElement globalShortDesc;
    
    @FindBy(xpath = "//div[contains(@class,'auto-WebDescription')]//child::div[contains(@class,'ql-editor')]")    
    WebElement globalWebDesc;
    
    @FindBy(xpath = "//div[contains(@class,'auto-AppDescription')]//child::div[contains(@class,'ql-editor')]")
    WebElement globalAppDesc;
  
    @FindBy(xpath = "//div[contains(@class,'auto-TVDescription')]//child::div[contains(@class,'ql-editor')]")
    WebElement globalTVDesc;
   
    @FindBy(name="upcomingPageText")
    WebElement globalUpcomingPage;
    
  
  
    //-------------------------------------------- Cast and Crew -----------------------------------------------------------
    @FindBy(xpath = "//button[contains(@class,'auto-leftTab-Cast&Crew')]")
    WebElement castCrew;

    @FindBy(xpath = "//input[contains(@id,'auto-castName')]")
    WebElement actor;

    @FindBy(xpath = "//input[contains(@id,'auto-character')]")
    WebElement character;
    
    @FindBy(xpath = "//div[contains(@class,'auto-ActorChange')]//descendant::input")
    WebElement actorChange;      

    @FindBy(xpath = "//div[contains(@class,'auto-Performer')]//descendant::input")
    WebElement performer;

    @FindBy(xpath = "//div[contains(@class,'auto-Host/Anchorman')]//descendant::input")
    WebElement host;

    @FindBy(xpath = "//div[contains(@class,'auto-Singer')]//descendant::input")
    WebElement singer;

    @FindBy(xpath = "//div[contains(@class,'auto-Lyricist')]//descendant::input")
    WebElement lyricst;

    @FindBy(xpath = "//div[contains(@class,'auto-Director')]//descendant::input")
    WebElement director;

    @FindBy(xpath = "//div[contains(@class,'auto-Cinematography/DOP')]//descendant::input")
    WebElement cinematography;

    @FindBy(xpath = "//div[contains(@class,'auto-Producer')]//descendant::input")
    WebElement producer;

    @FindBy(xpath = "//div[contains(@class,'auto-ExecutiveProducer')]//descendant::input")
    WebElement executiveProducer;

    @FindBy(xpath = "//div[contains(@class,'auto-MusicDirector')]//descendant::input")
    WebElement musicDirector;

    @FindBy(xpath = "//div[contains(@class,'auto-Choreographer')]//descendant::input")
    WebElement choreographer;

    @FindBy(xpath = "//div[contains(@class,'auto-TitleThemeMusic')]//descendant::input")
    WebElement titleThemeMusic;

    @FindBy(xpath = "//div[contains(@class,'auto-BackgroundScore')]//descendant::input")
    WebElement backgroundScore;

    @FindBy(xpath = "//div[contains(@class,'auto-StoryWriter')]//descendant::input")
    WebElement storyWriter;

    @FindBy(xpath = "//div[contains(@class,'auto-ScreenPlay')]//descendant::input")
    WebElement screenPlay;

    @FindBy(xpath = "//div[contains(@class,'auto-DialogueWriter')]//descendant::input")
    WebElement dialogueWriter;

    @FindBy(xpath = "//div[contains(@class,'auto-FilmEditing')]//descendant::input")
    WebElement filmEditing;

    @FindBy(xpath = "//div[contains(@class,'auto-Casting')]//descendant::input")
    WebElement casting;

    @FindBy(xpath = "//div[contains(@class,'auto-ProductionDesign')]//descendant::input")
    WebElement productionDesign;

    @FindBy(xpath = "//div[contains(@class,'auto-ArtDirection')]//descendant::input")
    WebElement artDirection;

    @FindBy(xpath = "//div[contains(@class,'auto-SetDecoration')]//descendant::input")
    WebElement setDecoration;

    @FindBy(xpath = "//div[contains(@class,'auto-CostumeDesign')]//descendant::input")
    WebElement costumeDesign;

    @FindBy(xpath = "//div[contains(@class,'auto-ProductionCo')]//descendant::input")
    WebElement productionCo;

    @FindBy(xpath = "//div[contains(@class,'auto-Presenter')]//descendant::input")
    WebElement presenter;

    @FindBy(xpath = "//div[contains(@class,'auto-Guest')]//descendant::input")
    WebElement guest;

    @FindBy(xpath = "//div[contains(@class,'auto-Participant')]//descendant::input")
    WebElement participant;

    @FindBy(xpath = "//div[contains(@class,'auto-Judges')]//descendant::input")
    WebElement judges;

    @FindBy(xpath = "//div[contains(@class,'auto-Narrator')]//descendant::input")
    WebElement narrator;

    @FindBy(xpath = "//div[contains(@class,'auto-Sponsor')]//descendant::input")
    WebElement sponsor;

    @FindBy(xpath = "//div[contains(@class,'auto-Graphics')]//descendant::input")
    WebElement graphics;

    @FindBy(xpath = "//div[contains(@class,'auto-Vocalist')]//descendant::input")
    WebElement vocalist;
    
    @FindBy(id = "sponsors")
    WebElement globalSponsors;
    
    @FindBy(id = "country")
    WebElement globalGroupCountry;


    //-----------------------------------------Videos --------------------------------------------
    @FindBy(xpath = "//button[contains(.,'Video')]")
    WebElement videos;

    @FindBy(name = "audioLanguage")
    WebElement audioTrack;

    @FindBy(xpath = "//input[@name='dashRootFolderName']")
    WebElement dashRootVideo;

    @FindBy(xpath = "//input[@name='dashManifestName']")
    WebElement dashManifestVideo;

    @FindBy(xpath = "//input[@name='hlsRootFolderName']")
    WebElement hlsRootVideo;

    @FindBy(xpath = "//input[@name='hlsManifestName']")
    WebElement hlsManifestVideo;

    @FindBy(name = "subtitleLanguages")
    WebElement subTitle;

    @FindBy(name = "drmKeyId")
    WebElement drmKeyId;

    @FindBy(name = "size")
    WebElement size;

    @FindBy(name = "protected")
    WebElement protectedCheckbox;

    @FindBy(id = "dashSuffixesId")
    WebElement dashSuffix;

    @FindBy(id = "hlsSuffixesId")
    WebElement hlsSuffix;

    @FindBy(xpath = "//input[@name='mediathekFileUid']")
    WebElement mediaThekUID;
    
    @FindBy(linkText = "Import Details")
    WebElement importDetails;

    //--------------------------------------License ------------------------------------------------------	
    @FindBy(xpath = "//button[contains(@class,'auto-leftTab-LicenseModule')]")
    WebElement license;

    @FindBy(xpath = "//div[contains(text(),'Create License')]")
    WebElement createLicense;

    @FindBy(xpath = "//input[@name='fromDate']")
    WebElement licenseFromDate;

	@FindBy(xpath = "//input[@name='toDate']")
	WebElement licenseToDate;

	@FindBy(xpath = "//input[contains(@id,'country')]")
    WebElement licenseCountry;

	@FindBy(id = "businessType")   
    WebElement licenseBusinessType;	
	
	@FindBy(xpath = "//input[contains(@id,'businessType')]")   
    WebElement singleLandingLicenseBusinessType;
	
	@FindBy(xpath = "//input[contains(@id,'platform')]")   
    WebElement licensePlatform;
	
	@FindBy(xpath = "//input[contains(@id,'tvodTier')]")
	WebElement licenseTVOD;

	@FindBy(xpath = "//span[contains(text(),\"SAVE\")]")
	WebElement saveLicense;

    @FindBy(xpath = "//div[@role='tooltip']")
    WebElement inactivateLicenseDropdown;

    @FindBy(xpath = "//div[@class='ml-10 status-dropdown val']")
    WebElement licenseStatusButton;

    @FindBy(xpath = "//input[contains(@id,'reason')]")
    WebElement inactivateLicenseReason;

    @FindBy(xpath = "//input[@name='manual' and @value='2']")
    WebElement useTemplateRadioButton;

    @FindBy(xpath = "//input[@type='text']")
    WebElement selectTemplate;

    @FindBy(xpath = "//div[@class='default-edit-btn']")
    WebElement editLicense;

    @FindBy(id = "country")
    WebElement editLicenseCountry;

    @FindBy(xpath = "//button[contains(@class,'auto-button-short-btn')]")
    WebElement expiredLicense;

    @FindBy(className = "item")
    WebElement expiredLicenseBack;

    @FindBy(xpath = "//button[contains(@class,'auto-button-filter-btn')]")
    WebElement licenseFilters;

    @FindBy(xpath = "//input[contains(@id,'businessType')]")
    WebElement licenseFilterBusinessType;

    @FindBy(id = "billingType")
    WebElement licenseFilterBillingType;

    @FindBy(id = "platform")
    WebElement licenseFilterPlatform;

    @FindBy(xpath = "//input[@name='status' and @value = '1']")
    WebElement licenseFilterActiveStatus;
    
    @FindBy(xpath = "//input[@name='status' and @value = '0']")
    WebElement licenseFilterInActiveStatus;
    
    @FindBy(xpath = "//input[@name='setName']")
    WebElement licenseSetName;

	@FindBy(xpath = "//div[contains(@class,'delete')]")
	List<WebElement> licenseTemplateDeleteSets;
    //--------------------------------------------Images -----------------------------------------------------------------
    @FindBy(xpath = "//button[contains(@class,'auto-leftTab-Images')]")
    WebElement images;

    @FindBy(name = "cover")
    WebElement cover;

    @FindBy(name = "appcover")
    WebElement appcover;

    @FindBy(name = "list")
    WebElement list;

    @FindBy(name = "square")
    WebElement square;

    @FindBy(name = "tvcover")
    WebElement tvCover;

    @FindBy(name = "portrait")
    WebElement portrait;

    @FindBy(name = "listclean")
    WebElement listClean;

    @FindBy(name = "portraitclean")
    WebElement portraitClean;

    @FindBy(name = "telcosquare")
    WebElement telcoSquare;

    @FindBy(name = "passport")
    WebElement passport;

    @FindBy(xpath = "//button[contains(@class,'upload-btn')]")
    WebElement imagesCreateNewSet;
    
    @FindBy(name = "setName")
    WebElement newSetName;

    @FindBy(id="auto-GroupCountry-0-1")
    WebElement newSetCountry;
    
    @FindBy(id = "auto-Platform-0-2")
    WebElement newSetPlatform;
    
    @FindBy(id = "auto-gender-0-0")
    WebElement newSetGender;
    
    @FindBy(id="auto-genre-0-1")
    WebElement newSetGenre;
    
    @FindBy(id = "auto-ageGroup-0-2")
    WebElement newSetAgeGroup;
    
    @FindBy(id = "auto-language-0-3")
    WebElement newSetLanguage;
    
    @FindBy(name="others")
    WebElement newSetOthers;
    
    @FindBy(xpath = "//div[contains(@class,'question-list flex')]")
    List<WebElement> imageSets;
    
    @FindBy(xpath = "(//button[contains(@class,'Active')])[1]")
    WebElement imageSetActiveButton;
    
    @FindBy(xpath = "//span[@class='remove']")
    List<WebElement> removeSet;
    
    //----------------------------------------- SEO Details ---------------------------------------------------------------

    @FindBy(xpath = "//button[contains(@class,'auto-leftTab-SEODetails')]")
    WebElement seoDetails;

    @FindBy(name = "titleTag")
    WebElement seoTitleTag;

    @FindBy(name = "metaDescription")
    WebElement seoMetaDescription;

    @FindBy(name = "metaSynopsis")
    WebElement seoMetaSynopsis;

    @FindBy(id = "redirectionType")
    WebElement seoRedirectionType;

    @FindBy(name = "redirectionLink")
    WebElement seoRedirectionLink;

    @FindBy(name = "noIndexNoFollow")
    WebElement seoNoIndexNoFollow;

    @FindBy(name = "h1Heading")
    WebElement seoH1Heading;

    @FindBy(name = "h2Heading")
    WebElement seoH2Heading;

    @FindBy(name = "h3Heading")
    WebElement seoH3Heading;

    @FindBy(name = "h4Heading")
    WebElement seoH4Heading;

    @FindBy(name = "h5Heading")
    WebElement seoH5Heading;

    @FindBy(name = "h6Heading")
    WebElement seoH6Heading;

    @FindBy(name = "robotsMetaNoIndex")
    List < WebElement > seoRobotsMetaIndex;

    @FindBy(name = "robotsMetaImageIndex")
    WebElement seoRobotsMetaImageIndex;

    @FindBy(name = "robotsMetaImageNoIndex")
    WebElement seoRobotsMetaImageNoIndex;

    @FindBy(name = "breadcrumbTitle")
    WebElement seoBreadcrumbTitle;

    @FindBy(name = "internalLinkBuilding")
    WebElement seoInternalLinkBuilding;

    @FindBy(xpath = "//input[@type=\"checkbox\"]")
    List < WebElement > seoCheckboxes;

    @FindBy(xpath = "//input[@name='videoObjectSchema']")
    WebElement seoVideoObjectSchema;
    
    @FindBy(xpath = "//input[@name='websiteSchema']")
    WebElement seoWebsiteSchema;
    
    @FindBy(xpath = "//input[@name='breadcrumListSchema']")
    WebElement seoBreadcrumbListSchema;
    
    @FindBy(xpath = "//input[@name='imageObjectSchema']")
    WebElement seoImageObjectSchema;
    
    @FindBy(xpath = "//input[@name='organizationSchema']")
    WebElement seoOrganizationSchema;
    
    // ---------------------------- Seo Details Global Fields -----------------------------------------------------------------------------------
    
    @FindBy(xpath="//div[contains(@class,'auto-Country/Group')]//input")
    WebElement globalSeoCountry;
    
    //-------------------------- MAP Content -----------------------------------------------------------------------------------------------------

    @FindBy(xpath = "//button[contains(@class,'auto-leftTab-MapContent')]")
    WebElement mapContent;

    @FindBy(xpath = "//div[contains(text(),\"Add Content\")]")
    WebElement addContent;

    @FindBy(xpath = "//input[@type='checkbox']")
    WebElement checkbox;

    //@FindBy(className = "btn-create-user  ")
 

    @FindBy(xpath = "//div[@class='mark-done mark-fill-active']")
    WebElement mapMarkAsDone;

    @FindBy(name = "searchVal")
    List < WebElement > searchMapContent;

    //-----------------------------------------Quick Links ----------------------------------------------------

    @FindBy(xpath = "//li[contains(text(),'Related Content')]")
    WebElement relatedContent;

    @FindBy(name = "searchVal")
    WebElement relatedContentSearch;

    @FindBy(xpath = "//span[contains(text(),'Assign Movies')]")
    WebElement relatedContentAssignMovies;
    
    @FindBy(xpath = "//div[contains(text(),'Add Movies')]")
    WebElement relatedContentAddMovies;
    
    @FindBy(xpath = "//span[contains(text(),'Assign TV Shows')]")
    WebElement relatedContentAssignTvShows;
    
    @FindBy(xpath = "//div[contains(text(),'Add TV Shows')]")
    WebElement relatedContentAddTvShows;

    @FindBy(xpath = "//span[contains(text(),'Assign Videos')]")
    WebElement relatedContentAssignVideos;

    @FindBy(xpath = "//div[contains(text(),'Add Videos')]")
    WebElement relatedContentAddVideos;     
    
    @FindBy(xpath = "//button[contains(.,'Assign Seasons')]")
    WebElement relatedContentAssignSeasons;

    @FindBy(xpath = "//div[contains(text(),'Add Seasons')]")
    WebElement relatedContentAddSeasons;

    @FindBy(xpath = "//button[contains(.,'Assign Episodes')]")
    WebElement relatedContentAssignEpisodes;

    @FindBy(xpath = "//div[contains(text(),'Add Episodes')]")
    WebElement relatedContentAddEpisodes;
      
    @FindBy(id = "assignedContent")
    WebElement assignRelatedContent;
      
    @FindBy(xpath = "(//span[contains(@class,'auto-delete')])[1]")
    WebElement deleteRelatedContent;
    
    @FindBy(xpath = "//li[contains(text(),'Published History')]")
    WebElement publishedHistory;

    //----------------------------------- CheckList---------------------------------------------------

    @FindBy(xpath = "//button[contains(@class,'auto-leftTab-Checklist')]")
    WebElement checklist;

    @FindBy(xpath = "//div[contains(@class,'auto-countryGroup')]//descendant::input")
    WebElement checklistCountry;

    @FindBy(xpath = "//span[contains(text(),'Publish Content')]")
    WebElement publishContent;

    @FindBy(xpath = "//span[contains(text(),'Republish Content')]")
    WebElement republishContent;

    @FindBy(xpath = "//span[contains(text(),'Unpublish Content')]//ancestor::button")
    WebElement unpublishContent;

    @FindBy(xpath = "//span[@class='edit tooltip-sec hand-cursor']")
    WebElement restoreButton;

    @FindBy(id = "archive-icon_svg__Rectangle_273")
    List < WebElement > archiveButton;

    @FindBy(xpath = "//div[contains(@class,'Schedule-box')]//div[contains(@class,'add-btn create-btn')]")
    WebElement scheduleContent;

    @FindBy(name = "scheduledPublicationTime")
    WebElement scheduledPublicationTime;

    @FindBy(xpath = "//div[contains(@class,'auto-countryGroup')]//descendant::input")
    List < WebElement > scheduledCountry;

    @FindBy(xpath = "//div[contains(@class,'Schedule-box m-b-20 ')]//div[contains(@class,'auto-countryGroup')]//input")
    WebElement scheduledContentCountry;
    
    @FindBy(xpath = "//span[contains(text(),'Schedule Content')]")
    WebElement scheduleContentButton;

   // @FindBy(how = How.CSS, using = "div.unpublish-ui-row[xpath='1']>div>div>div>input")
    @FindBy(xpath = "(//label[contains(text(),'Country')]//following::input)[3]")
    WebElement unpublishReason;
    
	@FindBy(xpath = "(//div[contains(@class,'val')])[3]")
	WebElement scheduledTime;
	
	@FindBy(xpath = "(//div[contains(@class,'icon-w-text icon-w-14 m-b-10')])[1]")
	WebElement currentTime;	
	
	@FindBy(xpath = "(//button[contains(@class,'MuiPickersToolbarButton-toolbarBtn')])[4]")
	WebElement datePickerMinutes;		
	
	@FindBy(xpath = "//span[contains(@class,'MuiTypography-root MuiPickersClockNumber-clockNumber') and contains(text(),'05')]")
	WebElement datePickerMinutesClock;	
    // ------------------------------Sort And Filter -------------------------------------------------------

    @FindBy(xpath = "//span[contains(text(),'Sort')]")
    WebElement sortVideo;

    @FindBy(xpath = "//input[@value='asc']")
    WebElement ascOrderVideo;

    @FindBy(xpath = "//input[@value='desc']")
    WebElement descOrderVideo;

    @FindBy(xpath = "//span[contains(text(),'Apply Sort')]")
    WebElement applySortVideo;

    @FindBy(xpath = "//span[contains(text(),'Clear')]")
    WebElement clearFilter;

    @FindBy(xpath = "//span[contains(text(),'Filters')]")
    WebElement filters;

    @FindBy(xpath = "//div[contains(text(),'Draft')]")
    WebElement filtersDraftStatus;

    @FindBy(xpath = "//div[contains(text(),'Changed')]")
    WebElement filtersChangedStatus;

    @FindBy(xpath = "//div[contains(text(),'Published')]")
    WebElement filtersPublishedStatus;

    @FindBy(xpath = "//div[contains(text(),'Unpublished')]")
    WebElement filtersUnpublishedStatus;

    @FindBy(xpath = "//div[contains(text(),'Need Work')]")
    WebElement filtersNeedWorkStatus;

    @FindBy(xpath = "//div[contains(text(),'Scheduled')]")
    WebElement filtersScheduledStatus;

    @FindBy(xpath = "//div[contains(text(),'Submitted to Review')]")
    WebElement filtersSubmittedToReviewStatus;

    @FindBy(xpath = "//div[contains(text(),'Archived')]")
    WebElement filtersArchivedStatus;

    @FindBy(xpath = "//input[@type='date']")
    List < WebElement > filtersStartDate;

    @FindBy(xpath = "//input[@type='date']")
    List < WebElement > filtersEndDate;

    @FindBy(xpath = "//input[@id='primaryGenre']")
    WebElement filtersPrimaryGenre;

    @FindBy(xpath = "//input[@id='secondaryGenre']")
    WebElement filtersSecondaryGenre;

    @FindBy(xpath = "//input[@id='thematicGenre']")
    WebElement filtersThematicGenre;

    @FindBy(xpath = "//input[@id='settingGenre']")
    WebElement filtersSettingGenre;

    @FindBy(xpath = "//input[@id='VideoActivity']")
    WebElement filtersActivity;

    @FindBy(xpath = "//input[@id='VideoStyle']")
    WebElement filtersStyle;

    @FindBy(xpath = "//input[@id='actor']")
    WebElement filtersActor;

    @FindBy(xpath = "//input[@id='subType']")
    WebElement filtersSubType;

    @FindBy(xpath = "//input[@id='contentCategory']")
    WebElement filtersContentCategory;

    @FindBy(xpath = "//input[@id='rcsCategory']")
    WebElement filtersRcsCategory;

    @FindBy(xpath = "//input[@id='theme']")
    WebElement filtersTheme;

    @FindBy(xpath = "//input[@id='targetAudience']")
    WebElement filtersTargetAudience;

    @FindBy(xpath = "//input[@id='licenseGroupCountries']")
    WebElement filtersLicenseGroup;

    @FindBy(xpath = "//input[@id='ageRating']")
    WebElement filtersAgeRating;

    @FindBy(xpath = "//input[@id='businessType']")
    WebElement filtersBusinessType;

    @FindBy(xpath = "//input[@name='externalId']")
    WebElement filtersExternalID;

    @FindBy(xpath = "//input[@id='contentRating']")
    WebElement filtersContentRating;

    @FindBy(xpath = "//input[@id='contentOwner']")
    WebElement filtersContentOwner;

    @FindBy(xpath = "//input[@id='audioLanguage']")
    WebElement filtersAudioLanguage;

    @FindBy(xpath = "//input[@id='originalLanguage']")
    WebElement filtersOriginalLanguage;

    @FindBy(xpath = "//input[@id='tags']")
    WebElement filtersTags;

    @FindBy(xpath = "//input[@id='translationLanguage']")
    WebElement filtersTranslationLanguage;

    @FindBy(xpath = "//input[@id='translationStatus']")
    WebElement filtersTranslationLanguageStatus;

    @FindBy(xpath = "//input[@id='moodEmotion']")
    WebElement filtersMood;

    @FindBy(xpath = "//span[contains(text(),'Apply Filter')]")
    WebElement applyFilter;

    
    //------------------------------------------Tranlations ---------------------------------------------------------------
  
    @FindBy(className =  "auto-quickLinks-Translations")
    WebElement translations;
    
    @FindBy(name = "upcomingPageText")
    WebElement translationsUpcomingPage;
    
    @FindBy(xpath = "//input[@name='alternativeTitle']")
    WebElement translationsAlternativeTitle;
    
    @FindBy(name = "socialShareTitle")
    WebElement translationsTitleForSocialShare;    
    
    @FindBy(name = "socialShareDescription")
    WebElement translationsDescriptionForSocialShare;

    @FindBy(xpath = "//div[contains(@id,'auto-Short Description')]//child::div[contains(@class,'ql-editor')]")
    WebElement translationsShortDesc;

    @FindBy(xpath = "//div[contains(@id,'auto-Web Description')]//child::div[contains(@class,'ql-editor')]")
    WebElement translationsWebDesc;

    @FindBy(xpath = "//div[contains(@id,'auto-App Description')]//child::div[contains(@class,'ql-editor')]")
    WebElement translationsAppDesc;

    @FindBy(xpath = "//div[contains(@id,'auto-TV Description')]//child::div[contains(@class,'ql-editor')]")
    WebElement translationsTVDesc;

    @FindBy(xpath = "//input[contains(@id,'auto-Actor')]")
    WebElement translationsActor;

    @FindBy(xpath = "//input[contains(@id,'auto-Character')]")
    WebElement translationsCharacter;
    
    @FindBy(xpath = "//span[contains(@class,'editSaveButton')]")
    List < WebElement > editTranslation;
    
    @FindBy(xpath = "//span[contains(text(),'Save')]")
    WebElement saveTranslation;
    
    @FindBy(xpath = "(//span[contains(@class,'editSaveButton')])[1]")
     WebElement  editTranslationActor;
    
    @FindBy(xpath = "(//span[contains(@class,'editSaveButton')])[2]")
    WebElement  editTranslationCharacter;
    
    
    //------------------------------------------ Quick Filling --------------------------------------------------------
    @FindBy(xpath = "//span[contains(text(),'Quick Filing')]")
    WebElement quickFilling;
    
    @FindBy(xpath = "//li[contains(text(),'Create Quick Filing')]")
    WebElement createQuickFiling;
    
    @FindBy(xpath = "//li[contains(text(),'Single Landing Page')]")
    WebElement createSingleLanding;
    
    
    // --------------------------------------------- Episode Properties---------------------------------------------------------------
    @FindBy(className =  "auto-quickLinks-Seasons")
    WebElement seasons;
    
    @FindBy(className =  "auto-quickLinks-Episodes")
    WebElement episodes;    
    
    @FindBy(xpath =  "//button[contains(.,'Create Episode')]")
    WebElement createEpisode;     
    
    @FindBy(xpath =  "(//input[@name='selectCreateType'])[1]")
    WebElement createManualEpisode;  
   
    @FindBy(xpath =  "(//input[@name='selectJourney'])[3]")
    WebElement createMainEpisode;  
    
    @FindBy(xpath =  "(//input[@name='selectJourney'])[2]")
    WebElement createQuickFilingEpisode; 
    
    @FindBy(xpath =  "(//input[@name='selectJourney'])[1]")
    WebElement createSingleLandingEpisode;    
    		
    @FindBy(name = "index")
    WebElement index;
    
    @FindBy(xpath = "//div[contains(text(),'Note')]//following::span[contains(text(),'Edit') or contains(text(),'Save')][1]")
    WebElement editNote;
  
    @FindBy(xpath = "//div[contains(text(),'Title')]//following::span[contains(text(),'Edit') or contains(text(),'Save')][1]")
    WebElement editTitle;
    
    @FindBy(xpath = "//div[contains(text(),'Short Description')]//following::span[contains(text(),'Edit') or contains(text(),'Save')][1]")
    WebElement editShortDesc;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-3')]")
    WebElement editWebDesc;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-4')]")
    WebElement editAppDesc;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-5')]")
    WebElement editTvDesc;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-6')]")
    WebElement editIndex;
    
    @FindBy(xpath = "//div[contains(text(),'Sub Type')]//following::span[contains(text(),'Edit') or contains(text(),'Save')][1]")
    WebElement editSubType;

    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-9')]")
    WebElement editCategory;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-10')]")
    WebElement editPrimaryGenre;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-11')]")
    WebElement editSecondaryGenre;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-12')]")
    WebElement editThematicGenre;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-13')]")
    WebElement editSettingGenre;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-14')]")
    WebElement editRcsCategory;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-15')]")
    WebElement editCreativeTitle;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-16')]")
    WebElement editAlternativeTitle;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-17')]")
    WebElement editPageTitle;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-18')]")
    WebElement editPageDescription;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-19')]")
    WebElement editTitleForSocialShare;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-20')]")
    WebElement editDescriptionForSocialShare;
    
    @FindBy(xpath = "//div[contains(text(),'ZEE5 Release Date')]//following::span[contains(text(),'Edit') or contains(text(),'Save')][1]")
    WebElement editZeeReleaseDate;
    
    @FindBy(xpath = "//div[contains(text(),'Original Telecast Date')]//following::span[contains(text(),'Edit') or contains(text(),'Save')][1]")
    WebElement editOriginalTelecastDate;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-23')]")
    WebElement editVideoDuration;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-24')]")
    WebElement editMultiAudio;
    
    @FindBy(xpath = "//div[contains(text(),'Audio Language')]//following::span[contains(text(),'Edit') or contains(text(),'Save')][1]")
    WebElement editAudioLanguage;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-26')]")
    WebElement editContentLanguage;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-27')]")
    WebElement editPrimaryLanguage;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-28')]")
    WebElement editDubbedLanguageTitle;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-29')]")
    WebElement editOriginalLanguage;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-30')]")
    WebElement editSubtitleLanguage;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-31')]")
    WebElement editContentVersion;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-32')]")
    WebElement editTheme;    
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-33')]")
    WebElement editContractID;
        
    @FindBy(xpath = "(//span[@class='edit-save-b auto-edit-save-b-0'])[2]")
    WebElement editSpecialCategory;
  
    @FindBy(xpath = "(//span[@class='edit-save-b auto-edit-save-b-1'])[2]")
    WebElement editSpecialCategoryCountry;
    
    @FindBy(xpath = "(//span[@class='edit-save-b auto-edit-save-b-2'])[2]")
    WebElement editSpecialFromTimeline;
    
    @FindBy(xpath = "(//span[@class='edit-save-b auto-edit-save-b-3'])[2]")
    WebElement editSpecialToTimeline;
    
    @FindBy(xpath = "//button[contains(.,'Episode Properties')]")
    WebElement episodeProperties;
  
    
    
    //--------------------------------Episode Properties - > Classification -------------------------------------------------
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-0')]")
    WebElement editContentRating;
  
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-1')]")
    WebElement editContentOwner;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-2')]")
    WebElement editOriginCountry;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-3')]")
    WebElement editContentProduction;
    
    @FindBy(xpath = "//div[contains(text(),'Upcoming Page Text')]//following::span[contains(text(),'Edit') or contains(text(),'Save')][1]")
    WebElement editUpcomingPageText;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-5')]")
    WebElement editContentDescriptors;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-6')]")
    WebElement editCertification;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-7')]")
    WebElement editMoodsEmotions;

    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-8')]")
    WebElement editContentGrade;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-9')]")
    WebElement editEra;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-10')]")
    WebElement editTargetAgeGroup;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-11')]")
    WebElement editTargetAudience;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-12')]")
    WebElement editTags;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-13')]")
    WebElement editDigitalKeywords;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-14')]")
    WebElement editAdaptation;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-15')]")
    WebElement editEvent;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-16')]")
    WebElement editProductionCompanyClassification;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-17')]")
    WebElement editPopularityScore;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-18')]")
    WebElement editTrivia;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-19')]")
    WebElement editEpgProgramID;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-20')]")
    WebElement editShowAirTime;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-21')]")
    WebElement editShowAirTimeDays;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-20')]")
    WebElement editShowOnAir;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-21')]")
    WebElement editShowOnApp;
    
    @FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-22')]")
    WebElement editDamBmsID;    
    
    @FindBy(xpath = "(//span[@class='edit-save-b auto-edit-save-b-0'])[2]")
    WebElement editAwardRecipient;
    
    @FindBy(xpath = "(//span[@class='edit-save-b auto-edit-save-b-1'])[2]")
    WebElement editAwardCategory;
    
    @FindBy(xpath = "(//span[@class='edit-save-b auto-edit-save-b-2'])[2]")
    WebElement editAwardHonor;
    
  //-------------------------------------------- Episode Properties Player Attributes ------------------
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-0']")
    WebElement editIntroStartTime;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-1']")
    WebElement editIntroEndTime;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-2']")
    WebElement editRecapStartTime;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-3']")
    WebElement editRecapEndTime;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-4']")
    WebElement editEndCreditStartTime;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-5']")
    WebElement editAdMarker;
    
    @FindBy(xpath = "(//span[@class='edit-save-b auto-edit-save-b-0'])[2]")
    WebElement editSkipSongStartTime;
    
    @FindBy(xpath = "(//span[@class='edit-save-b auto-edit-save-b-1'])[2]")
    WebElement editSkipSongEndTime;
    
    //------------------------------------------Episode Cast/Crew ---------------------------------------------------------
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-0']")
    WebElement editActor;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-1']")
    WebElement editCharacter;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-0']")
    List<WebElement> editActorChange;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-1']")
    List<WebElement> editPerformer;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-2']")
    WebElement editHost;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-3']")
    WebElement editSinger;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-4']")
    WebElement editLyricst;
    
    @FindBy(xpath = "//div[contains(text(),'Director')]//following::span[contains(text(),'Edit') or contains(text(),'Save')][1]")
    WebElement editDirector;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-6']")
    WebElement editCinematography;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-7']")
    WebElement editProducer;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-8']")
    WebElement editExecutiveProducer;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-9']")
    WebElement editMusicDirector;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-10']")
    WebElement editChoreographer;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-11']")
    WebElement editTitleThemeMusic;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-12']")
    WebElement editBackgroundScore;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-13']")
    WebElement editStoryWriter;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-14']")
    WebElement editScreenPlay;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-15']")
    WebElement editDialogueWriter;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-16']")
    WebElement editFilmEditing;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-17']")
    WebElement editCasting;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-18']")
    WebElement editProductionDesign;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-19']")
    WebElement editArtDirection;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-20']")
    WebElement editSetDecoration;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-21']")
    WebElement editCostumeDesign;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-22']")
    WebElement editProductionCo;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-23']")
    WebElement editPresenter;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-24']")
    WebElement editGuest;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-25']")
    WebElement editParticipant;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-26']")
    WebElement editJudges;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-27']")
    WebElement editNarrator;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-28']")
    WebElement editSponsors;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-29']")
    WebElement editGraphics;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-30']")
    WebElement editVocalist;

    @FindBy(xpath = "//div[contains(@class,'remove-btn')]")
    WebElement removeActor;
    
    //------------------------------------------Episode SEO Details -------------------------------------------------------

    
    @FindBy(xpath = "//div[contains(text(),'SEO Title Tag')]//following::span[contains(text(),'Edit') or contains(text(),'Save')][1]")
    WebElement editSeoTitleTag;  
    
    @FindBy(xpath = "//div[contains(text(),'SEO Meta Description')]//following::span[contains(text(),'Edit') or contains(text(),'Save')][1]")
    WebElement editSeoMetaDescription;
    
    @FindBy(xpath = "//div[contains(text(),'SEO Meta Synopsis')]//following::span[contains(text(),'Edit') or contains(text(),'Save')][1]")
    WebElement editSeoMetaSynopsis;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-4']")
    WebElement editSeoRedirectionType;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-5']")
    WebElement editSeoRedirectionLink;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-6']")
    WebElement editSeoNoIndex;
    
    @FindBy(xpath = "//div[contains(text(),'H1 Heading')]//following::span[contains(text(),'Edit') or contains(text(),'Save')][1]")
    WebElement editSeoH1Heading;
    
    @FindBy(xpath = "//div[contains(text(),'H2 Heading')]//following::span[contains(text(),'Edit') or contains(text(),'Save')][1]")
    WebElement editSeoH2Heading;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-9']")
    WebElement editSeoH3Heading;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-10']")
    WebElement editSeoH4Heading;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-11']")
    WebElement editSeoH5Heading;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-12']")
    WebElement editSeoH6Heading;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-13']")
    WebElement editSeoRobotMetaIndex;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-14']")
    WebElement editSeoRobotMetaNoIndex;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-15']")
    WebElement editSeoRobotMetaImageIndex;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-16']")
    WebElement editSeoRobotMetaImageNoIndex;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-17']")
    WebElement editSeoCanonicalURL;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-18']")
    WebElement editSeoBreadcrumbTitle;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-19']")
    WebElement editSeoInternalLinkBuilding;
    
    @FindBy(xpath = "//div[contains(text(),'Video Object Schema')]//following::span[contains(text(),'Edit') or contains(text(),'Save')][1]")
    WebElement editSeoVideoObjectSchema;
    
    @FindBy(xpath = "//div[contains(text(),'Website Schema')]//following::span[contains(text(),'Edit') or contains(text(),'Save')][1]")
    WebElement editSeoWebsiteSchema;
    
    @FindBy(xpath = "//div[contains(text(),'Breadcrum List Schema')]//following::span[contains(text(),'Edit') or contains(text(),'Save')][1]")
    WebElement editSeoBreadcrumListSchema;
    
    @FindBy(xpath = "//div[contains(text(),'Image Object Schema')]//following::span[contains(text(),'Edit') or contains(text(),'Save')][1]")
    WebElement editSeoImageObjectSchema;
    
    @FindBy(xpath = "//div[contains(text(),'Organization Schema')]//following::span[contains(text(),'Edit') or contains(text(),'Save')][1]")
    WebElement editSeoOrganizationSchema;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-25']")
    WebElement editSeoImageGallerySchema;
    
    @FindBy(xpath = "//span[@class='edit-save-b auto-edit-save-b-26']")
    WebElement editSeoLinkToStories;
    
   
    //----------------------------------------------------Episode Quick Filling ------------------------------------------

	@FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-0')]")
	WebElement editQuickFillingTitle;

	@FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-1')]")
	WebElement editQuickFillingShortDesc;

	@FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-2')]")
	WebElement editQuickFillingIndex;

	@FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-3')]")
	WebElement editQuickFillingSubType;

	@FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-4')]")
	WebElement editQuickFillinZee5ReleaseDate;

	@FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-5')]")
	WebElement editQuickFillingVideoDuration;

	@FindBy(xpath = "//span[contains(@class,'auto-edit-save-b-6')]")
	WebElement editQuickFillingAudioLanguage;

    //-------------------------------------------------- Ordering -------------------------------------------------------
    @FindBy(xpath = "//div[@class='drop-icon flex align-items-center justify-content-center auto-drop-icon']")  
    List<WebElement> seasonListingOrder;
 
    @FindBy(xpath = "//span[contains(text(),'Set Ordering')]")  
    WebElement setOrdering;
  
    @FindBy(xpath = "//button[contains(text(),'Create New Set')]")  
    WebElement createNewSet;
    
    @FindBy(xpath = "//input[@name='setName']")  
    WebElement setName;
  
    @FindBy(xpath = "//input[@id='country']")  
    WebElement setCountry;
    
    //------------------------------Place Holder--------------------------------------
    
    @FindBy(xpath =  "//span[contains(text(),'Placeholder')]")
    WebElement createPlaceholderEpisode; 
        
    @FindBy(xpath =  "//input[@id='assetType']")
    WebElement assetType; 
    
    @FindBy(xpath =  "//input[contains(@id,'episodeNoStartFrom')]")
    WebElement assetStartFrom;
    
    @FindBy(xpath =  "//input[contains(@id,'dayPart')]")
    WebElement dayPart;
    
    @FindBy(xpath =  "//input[@name='numberOfAssets']")
    WebElement assetNumber;
    
    @FindBy(xpath =  "//button[@class='MuiButtonBase-root MuiIconButton-root']")
    WebElement releaseDate;
    
    @FindBy(xpath = "//div[@class='MuiPaper-root MuiPopover-paper MuiPaper-elevation8 MuiPaper-rounded']")
    WebElement datePicker;
    
	@FindBy(xpath="//span[contains(text(),'Yes')]")
	WebElement Yesbtn;

	@FindBy(xpath="//span[contains(text(),'Ok')]")
	WebElement Okbtn;
    
	
	//-------------------------------------------Templates ---------------------------------------------------------------
	@FindBy(xpath="//li[contains(text(),'Templates')]")
	WebElement templates;
  
	@FindBy(xpath="//button[contains(@class,'Episode')]")
	WebElement templatesEpisode;
	
	@FindBy(xpath="//button[contains(@class,'webisode')]")
	WebElement templatesWebisode;
	
	@FindBy(xpath="//button[contains(@class,'mobisode')]")
	WebElement templatesMobisode;
	
	@FindBy(xpath="//button[contains(@class,'Preview')]")
	WebElement templatesPreview;
	
	@FindBy(xpath="//button[contains(@class,'BestScene')]")
	WebElement templatesBestScene;
	
	@FindBy(xpath="//button[contains(@class,'WeekInShorts')]")
	WebElement templatesWeekInShorts;
	
	@FindBy(xpath="//input[contains(@id,'Collection')]")
	WebElement templatesCollections;
		
	
    //------------------------------------------Listing Page ---------------------------------------
	  
    @FindBy(xpath = "//button[contains(@class,'auto-dropdown-btn-TVShow')]")
    WebElement tvShowSearchType;
    
    @FindBy(xpath = "//button[contains(@class,'auto-dropdown-btn-Seasons')]")
    WebElement seasonsSearchType;
    
    @FindBy(xpath = "//button[contains(@class,'auto-dropdown-btn-Episodes')]")
    WebElement episodesSearchType;
  
    @FindBy(xpath = "//ul[@id='menu-list-grow']/li[1]")
    WebElement searchTVShow;
    
    @FindBy(xpath = "//ul[@id='menu-list-grow']/li[2]")
    WebElement searchSeasons;
    
    @FindBy(xpath = "//ul[@id='menu-list-grow']/li[3]")
    WebElement searchEpisodes;
  
    @FindBy(xpath = "(//div[@class = 'license-badge'])[1]")
    WebElement listingExpiringLicense;
 
    @FindBy(xpath = "(//div[contains(@class,'auto-mov-link-btn')])[1]")
    WebElement listingLinkedTVShows;
    
    @FindBy(xpath = "(//a)[3]")
    WebElement listingLicenseCountries;
    
    @FindBy(xpath = "//span[contains(text(),'Close')]")
    WebElement listingCloseLicenseCountries;  
    
    @FindBy(xpath = "(//span[@class='pub-history'])[1]")
    WebElement listingPublishedHistory;    
    
    @FindBy(xpath = "//div[@class='side-close-btn']")
    WebElement listingClosePublishedHistory;
    
    @FindBy(xpath = "(//span[@class = 'val'])[1]")
    WebElement listingTvShowLink;
    
    @FindBy(xpath = "(//span[@class = 'val'])[2]")
    WebElement listingSeasonLink;
    
    @FindBy(xpath = " (//div[@data-test='tvshow-child-arrow'])[1]")
    WebElement listingSeasonsArrow;
    
    @FindBy(xpath = "//li[@class='auto-leftTab-All']")
    WebElement listingAllStatus;
    	
    @FindBy(xpath = "//li[@class='auto-leftTab-Draft']")
    WebElement listingDraftStatus;
    
    @FindBy(xpath = "//li[@class='auto-leftTab-Changed']")
    WebElement listingChangedStatus;
    
    @FindBy(xpath = "//li[@class='auto-leftTab-Published']")
    WebElement listingPublishedStatus;
    
    @FindBy(xpath = "//li[@class='auto-leftTab-Unpublished']")
    WebElement listingUnpublishedStatus;
    
    @FindBy(xpath = "//li[@class='auto-leftTab-NeedWork']")
    WebElement listingNeedWorkStatus;
    
    @FindBy(xpath = "//li[@class='auto-leftTab-Scheduled']")
    WebElement listingScheduledStatus;
    
    @FindBy(xpath = "//li[@class='auto-leftTab-SubmittedToReview']")
    WebElement listingSubmittedToReviewStatus;
    
    @FindBy(xpath = "//li[@class='auto-leftTab-Archived']")
    WebElement listingArchivedStatus;
    
    @FindBy(xpath = "//li[@class='auto-leftTab-PublishingQueue']")
    WebElement listingPublishingQueueStatus;
    
    //----------------------------------------- Common Elements -----------------------------------------------------------
    @FindBy(className = "next-step-btn")
    WebElement nextButton;
    
    @FindBy(className = "prev-step-btn")
    WebElement previousButton;
    

    @FindBy(css = "button.MuiButtonBase-root.MuiIconButton-root.MuiAutocomplete-popupIndicator.MuiAutocomplete-popupIndicatorOpen > span.MuiIconButton-label > svg.MuiSvgIcon-root")
    WebElement closeDropdown;

    @FindBy(xpath = "//div[contains(@class,'auto-TVShow')]")
    WebElement tvShowButton;

    @FindBy(xpath = "//div[contains(text(),'Movie')]")
    WebElement movieButton;

    @FindBy(xpath ="//div[contains(@class,'mov-icon mov-view tooltip-sec auto-view')]")
    List < WebElement > editButton;

    @FindBy(xpath = "//span[contains(text(),'Edit TV Show')]")
    WebElement editTvShow;

    @FindBy(name = "searchString")
    WebElement searchString;

    @FindBy(xpath = "//span[contains(text(),'Yes') or contains(text(),'Ok')]")
    List < WebElement > yes;

    @FindBy(xpath = "//span[contains(text(),'Ok') or contains(text(),'ok')]")
    WebElement ok;
    
    @FindBy(xpath = "//span[contains(text(),'No')]")
    WebElement no;

    @FindBy(xpath = "//div[contains(@class,'mark-fill-active')]")
    WebElement doneButtonActive; // rgba(255, 255, 255, 1)

    @FindBy(xpath = "//li[@role='option' or @role='menuitem']")
    List < WebElement > dropDownList;

    @FindBy(xpath = "//div[contains(@class,'mark-active')]")
    WebElement doneButtonClicked; // rgba(52, 194, 143, 1)

    @FindBy(xpath = "//button[contains(@class,'auto-tab-Dashboard')]")
    WebElement dashboard;

    @FindBy(css = "button.MuiButtonBase-root.MuiIconButton-root.MuiAutocomplete-popupIndicator.MuiAutocomplete-popupIndicatorOpen > span.MuiIconButton-label > svg.MuiSvgIcon-root")
    WebElement filterCloseDropdown;

    @FindBy(xpath = ("//input[@type='checkbox']"))
    List < WebElement > checkBox;
    
    @FindBy(xpath ="//span[contains(text(),'Save') or contains(text(),'SAVE') or contains(text(),'UPDATE')]")
    WebElement save;

    @FindBy(css = "span.MuiIconButton-label > svg.MuiSvgIcon-root.MuiSvgIcon-fontSizeSmall")
    WebElement clearInput;
    
    @FindBy(xpath = "//a[contains(text(),'TV Shows')]")
    WebElement tvShowsLink;
    
    @FindBy(id = "assignedContent")
    WebElement assignContent;
    
    @FindBy(xpath = "//a[contains(@class,'auto-breadCrum-Seasons')]")
    WebElement seasonsBreadcrumb;
    
    @FindBy(xpath = "//a[contains(@class,'auto-breadCrum-Episodes')]")
    WebElement episodesBreadcrumb;
  
    @FindBy(xpath = "//a[contains(text(),'Update Episode')]")
    WebElement updateEpisodeBreadcrumb;
    
    @FindBy(xpath = "//a[contains(@class,'auto-breadCrum-TVShow')]")
    WebElement tvShowBreadcrumb;
    
    @FindBy(xpath = "//span[contains(text(),'Edit Episode')]")
    WebElement editEpisode;
    
  
    //---------------------------- Listing Page Search --------------------------------------------------------------------------------
    
    
    @FindBy(xpath="//button[contains(@class,'TVShow')]")
    WebElement tvShowSearch;
    
    @FindBy(xpath="//button[contains(@class,'Seasons')]")
    WebElement seasonSearch;
    
    @FindBy(xpath="//button[contains(@class,'Episodes')]")
    WebElement episodesSearch;
    
    @FindBy(className = "head-external-id")
    WebElement episodeExternalID;  
    
    //-----------------------------Initializing the page objects-----------------------------

	public EpisodesPage() {
		PageFactory.initElements(driver, this);
	}

	public void EpisodeProperties() throws InterruptedException, Exception {
		WebUtility.javascript(dashboard);
		WebUtility.Click(tvShowButton);
		WebUtility.Click(searchString);
		searchString.sendKeys(tvShowTitle);
		searchString.sendKeys(Keys.ENTER);
		WebUtility.Wait();
		WebUtility.Wait();
		WebUtility.Click(editButton.get(0));
		
		WebUtility.Wait();
		WebUtility.Click(seasons);

		WebUtility.Click(editButton.get(0));
		
		WebUtility.Click(episodes);
		WebUtility.Wait();
		WebUtility.Click(createEpisode);
		createManualEpisode.click();
	
		WebUtility.Click(yes.get(0));
		
		createMainEpisode.click();
		WebUtility.Click(yes.get(0));
		
		WebUtility.Wait();
		externalID = js.executeScript("return arguments[0].childNodes[1].innerText", episodeExternalID).toString();		
	
		WebUtility.Click(editNote);
		WebUtility.element_to_be_clickable(note);
		note.sendKeys(Keys.CONTROL + "a");
		note.sendKeys(Keys.DELETE);
		note.sendKeys("Automation Test - Episode");
		WebUtility.Click(editNote);
		Thread.sleep(1000);
  		
		WebUtility.Click(editTitle);
        WebUtility.Click(title);
        title.sendKeys(Keys.CONTROL + "a");
        title.sendKeys(Keys.BACK_SPACE);
        title.sendKeys(tvShowTitle+"- Episode");
        WebUtility.Click(editTitle);
		Thread.sleep(1000);  

        WebUtility.Click(editShortDesc);
        WebUtility.Click(shortDesc);
        shortDesc.sendKeys(Keys.CONTROL + "a");
        shortDesc.sendKeys(Keys.BACK_SPACE);
        shortDesc.sendKeys("The Expanse is an American science fiction television series developed by Mark Fergus and Hawk Ostby, based on the series of novels of the same name by James S. A. Corey.- Episode");
        WebUtility.Click(editShortDesc);
        WebUtility.Wait();
                 
        WebUtility.Click(editWebDesc);
        WebUtility.Click(webDesc);
        webDesc.sendKeys(Keys.CONTROL + "a");
        webDesc.sendKeys(Keys.BACK_SPACE);
        webDesc.sendKeys("Automation Test - Episode");
        WebUtility.Click(editWebDesc);
		Thread.sleep(1000);
      
        WebUtility.Click(editAppDesc);
        WebUtility.Click(appDesc);
        appDesc.sendKeys(Keys.CONTROL + "a");
        appDesc.sendKeys(Keys.BACK_SPACE);
        appDesc.sendKeys("Automation Test - Episode");
        WebUtility.Click(editAppDesc);
		Thread.sleep(1000);
 
        WebUtility.Click(editTvDesc);
        WebUtility.javascript(tvDesc);
        js.executeScript("arguments[0].focus()", tvDesc);
        tvDesc.sendKeys(Keys.CONTROL + "a");
        tvDesc.sendKeys(Keys.BACK_SPACE);
        tvDesc.sendKeys("Automation Test - Episode");
        WebUtility.Click(editTvDesc);
		Thread.sleep(1000); 

        WebUtility.Click(editSubType);
        WebUtility.Click(subType);
        subType.sendKeys(Keys.BACK_SPACE);
    	WebUtility.Wait();
        WebUtility.Click(dropDownList.get(0));
        editSubType.click(); 
		Thread.sleep(1000);

        WebUtility.javascript(editCategory);
        WebUtility.Click(category);
        category.sendKeys(Keys.BACK_SPACE);
    	WebUtility.Wait();
        WebUtility.Click(dropDownList.get(0));
        editCategory.click(); 
		Thread.sleep(1000);       
        
        WebUtility.Click(editPrimaryGenre);        
        primaryGenre.click();
        primaryGenre.sendKeys(Keys.BACK_SPACE);
    	WebUtility.Wait();
		WebUtility.Click(dropDownList.get(0));
        editPrimaryGenre.click();
		Thread.sleep(1000);
 
        WebUtility.Click(editSecondaryGenre);
        secondaryGenre.click();
        secondaryGenre.sendKeys(Keys.BACK_SPACE);
    	WebUtility.Wait();
        WebUtility.Click(dropDownList.get(0));
		editSecondaryGenre.click();
		Thread.sleep(1000);
            
        WebUtility.Click(editThematicGenre);
		thematicGenre.click();
		thematicGenre.sendKeys(Keys.BACK_SPACE);
		WebUtility.Wait();
		WebUtility.Click(dropDownList.get(0));
		editThematicGenre.click();
		Thread.sleep(1000);
   
		WebUtility.Click(editSettingGenre);
        settingGenre.click();
        settingGenre.sendKeys(Keys.BACK_SPACE);
    	WebUtility.Wait();
        WebUtility.Click(dropDownList.get(0));
        editSettingGenre.click();
		Thread.sleep(1000);
        
        WebUtility.Click(editRcsCategory);
        rcsCategory.click();
        rcsCategory.sendKeys(Keys.BACK_SPACE);
    	WebUtility.Wait();
        WebUtility.Click(dropDownList.get(0));
        WebUtility.Click(editRcsCategory);
		Thread.sleep(1000);    
        
        WebUtility.Click(editCreativeTitle);
        creativeTitle.click();
        creativeTitle.sendKeys(Keys.CONTROL + "a");
        creativeTitle.sendKeys(Keys.BACK_SPACE);
        creativeTitle.sendKeys("Automation Creative Title - Episode");
        WebUtility.Click(editCreativeTitle);
		Thread.sleep(1000);    
        
        WebUtility.Click(editAlternativeTitle);
        alternateTitle.click();
        alternateTitle.sendKeys(Keys.CONTROL + "a");
        alternateTitle.sendKeys(Keys.BACK_SPACE);
        alternateTitle.sendKeys("Automation Alternative Title - Episode");
        WebUtility.Click(editAlternativeTitle);
		Thread.sleep(1000);
        
        WebUtility.Click(editPageTitle);
        pageTitle.click();
        pageTitle.sendKeys(Keys.CONTROL + "a");
        pageTitle.sendKeys(Keys.BACK_SPACE);
        pageTitle.sendKeys("Automation Page Title - Episode");
        WebUtility.Click(editPageTitle);
		Thread.sleep(1000);
        
        WebUtility.Click(editPageDescription);
        pageDescription.click();
        pageDescription.sendKeys(Keys.CONTROL + "a");
        pageDescription.sendKeys(Keys.BACK_SPACE);
        pageDescription.sendKeys("Automation Page Description - Episode");
        WebUtility.Click(editPageDescription);
		Thread.sleep(1000);
  
        WebUtility.Click(editTitleForSocialShare);
        titleForSocialShare.click();
        titleForSocialShare.sendKeys(Keys.CONTROL + "a");
        titleForSocialShare.sendKeys(Keys.BACK_SPACE);
        titleForSocialShare.sendKeys("Automation Title for Social Share - Episode");
        WebUtility.Click(editTitleForSocialShare);
		Thread.sleep(1000);
        
        WebUtility.Click(editDescriptionForSocialShare);
        descriptionForSocialShare.click();
        descriptionForSocialShare.sendKeys(Keys.CONTROL + "a");
        descriptionForSocialShare.sendKeys(Keys.BACK_SPACE);
        descriptionForSocialShare.sendKeys("Automation Description for Social Share - Episode");
        WebUtility.Click(editDescriptionForSocialShare);
		Thread.sleep(1000);
        
        WebUtility.Click(editOriginalTelecastDate);
        WebUtility.Click(telecastDate);
        datePicker.sendKeys(Keys.ARROW_LEFT);
        datePicker.sendKeys(Keys.ESCAPE);
        WebUtility.Click(editOriginalTelecastDate);
		Thread.sleep(1000);
   
        WebUtility.Click(editZeeReleaseDate);
        WebUtility.Click(zee5ReleaseDate);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ESCAPE);
        WebUtility.Click(editZeeReleaseDate);
		Thread.sleep(1000);
      
        WebUtility.Click(editVideoDuration);
        videoDuration.click();
        List < WebElement > videoduration = driver.findElements(By.xpath("//li[@role='button']"));
        for (WebElement e: videoduration) {
            String videohrs = e.getText();
            if (videohrs.contains("06")) {
                e.click();
                break;
            }
        }
		WebUtility.Click(editVideoDuration);
		Thread.sleep(1000);

		WebUtility.Click(editMultiAudio);
		if (!multiAudio.isSelected()) {
			multiAudio.click();
			WebUtility.Click(editMultiAudio);
		} else
			WebUtility.Click(editMultiAudio);

		Thread.sleep(1000);
		
		WebUtility.Click(editAudioLanguage);
		js.executeScript("arguments[0].focus()", audioLanguage);
		audioLanguage.click();
		audioLanguage.sendKeys(Keys.BACK_SPACE);
		WebUtility.Wait();
		WebUtility.Click(dropDownList.get(0));
		WebUtility.Click(editAudioLanguage);
		Thread.sleep(1000);
		
		WebUtility.Click(editContentLanguage);
		contentLanguage.click();
		contentLanguage.sendKeys(Keys.BACK_SPACE);
		WebUtility.Wait();
		WebUtility.Click(dropDownList.get(0));
		WebUtility.Click(editContentLanguage);
		Thread.sleep(1000);
		
		WebUtility.Click(editPrimaryLanguage);
		primaryLanguage.click();
		primaryLanguage.sendKeys(Keys.BACK_SPACE);
		WebUtility.Wait();
		WebUtility.Click(dropDownList.get(0));
		WebUtility.Click(editPrimaryLanguage);
		Thread.sleep(1000);
		
		WebUtility.Click(editDubbedLanguageTitle);
		dubbedLanguageTitle.click();
		dubbedLanguageTitle.sendKeys(Keys.BACK_SPACE);
		WebUtility.Wait();
		WebUtility.Click(dropDownList.get(0));
		WebUtility.Click(editDubbedLanguageTitle);
		Thread.sleep(1000);
		
		WebUtility.Click(editOriginalLanguage);
		originalLanguage.click();
		originalLanguage.sendKeys(Keys.BACK_SPACE);
		WebUtility.Wait();
		WebUtility.Click(dropDownList.get(0));
		WebUtility.Click(editOriginalLanguage);
		Thread.sleep(1000);
		
		WebUtility.Click(editSubtitleLanguage);
		subtitleLanguages.click();
		subtitleLanguages.sendKeys(Keys.BACK_SPACE);
		WebUtility.Wait();
		WebUtility.Click(dropDownList.get(0));
		WebUtility.Click(editSubtitleLanguage);
		Thread.sleep(1000);
		
		WebUtility.Click(editContentVersion);
		contentVersion.click();
		contentVersion.sendKeys(Keys.BACK_SPACE);
		WebUtility.Wait();
		WebUtility.Click(dropDownList.get(0));
		WebUtility.Click(editContentVersion);
		Thread.sleep(1000);
		
		WebUtility.Click(editTheme);
		theme.click();
		theme.sendKeys(Keys.BACK_SPACE);
		WebUtility.Wait();
		WebUtility.Click(dropDownList.get(0));
		WebUtility.Click(editTheme);
		Thread.sleep(1000);
		
        WebUtility.Click(editContractID);
        contractId.click();
        contractId.sendKeys(Keys.CONTROL + "a");
        contractId.sendKeys(Keys.BACK_SPACE);
        contractId.sendKeys("Automation Page Title Episode");
        WebUtility.Click(editContractID);
		Thread.sleep(1000);

        WebUtility.Click(editSpecialCategory);
        specialCategory.click();
        specialCategory.sendKeys(Keys.BACK_SPACE);
    	WebUtility.Wait();
		WebUtility.Click(dropDownList.get(0));
		WebUtility.Click(editSpecialCategory);
		Thread.sleep(1000);
        
        WebUtility.Click(editSpecialCategoryCountry);
        specialCategoryCountry.click();
        specialCategoryCountry.sendKeys(Keys.BACK_SPACE);
    	WebUtility.Wait();
		WebUtility.Click(dropDownList.get(0));
		WebUtility.Click(editSpecialCategoryCountry);
		Thread.sleep(1000);

		WebUtility.Click(editSpecialToTimeline);
		specialCategoryTo.click();
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ESCAPE);
       // driver.findElement(By.xpath("//body")).click();	
		WebUtility.element_to_be_clickable(editSpecialToTimeline);
		WebUtility.Click(editSpecialToTimeline);
		Thread.sleep(1000);
		
		WebUtility.Click(editSpecialFromTimeline);
		specialCategoryFrom.click();
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ESCAPE);
		WebUtility.element_to_be_clickable(editSpecialFromTimeline);
		WebUtility.Click(editSpecialFromTimeline);
		Thread.sleep(1000);
  
		WebUtility.Click(nextButton);
		
		WebUtility.Click(editContentRating);
		rating.click();
		rating.sendKeys(Keys.BACK_SPACE);
		WebUtility.Wait();
		WebUtility.Click(dropDownList.get(0));
		WebUtility.Click(editContentRating);
		Thread.sleep(1000);
	
		WebUtility.Click(editContentOwner);
		contentOwner.click();
		contentOwner.sendKeys(Keys.BACK_SPACE);
		WebUtility.Wait();
		WebUtility.Click(dropDownList.get(0));
		WebUtility.Click(editContentOwner);
		Thread.sleep(1000);
		
		WebUtility.Click(editOriginCountry);
		originCountry.click();
		originCountry.sendKeys(Keys.BACK_SPACE);
		WebUtility.Wait();
		WebUtility.Click(dropDownList.get(0));
		WebUtility.Click(editOriginCountry);
		Thread.sleep(1000);
		
		WebUtility.Click(editContentProduction);
		contentProduction.click();
		contentProduction.sendKeys(Keys.BACK_SPACE);
		WebUtility.Wait();
		WebUtility.Click(dropDownList.get(0));
		WebUtility.Click(editContentProduction);
		Thread.sleep(1000);
		
        WebUtility.Click(editUpcomingPageText);
        upcomingPage.click();
        upcomingPage.sendKeys(Keys.CONTROL + "a");
        upcomingPage.sendKeys(Keys.BACK_SPACE);
        upcomingPage.sendKeys("Automation Upcoming Page Text Episode");
        WebUtility.Click(editUpcomingPageText);
		Thread.sleep(1000);
     
		WebUtility.Click(editContentDescriptors);
		contentDescriptor.click();
		contentDescriptor.sendKeys(Keys.BACK_SPACE);
		WebUtility.Wait();
		WebUtility.Click(dropDownList.get(0));
		WebUtility.Click(editContentDescriptors);
		Thread.sleep(1000);
		
		WebUtility.Click(editCertification);
		certification.click();
		certification.sendKeys(Keys.BACK_SPACE);
		WebUtility.Wait();
		WebUtility.Click(dropDownList.get(0));
		WebUtility.Click(editCertification);
		Thread.sleep(1000);
		
		WebUtility.Click(editMoodsEmotions);
		emotions.click();
		emotions.sendKeys(Keys.BACK_SPACE);
		WebUtility.Wait();
		WebUtility.Click(dropDownList.get(0));
		WebUtility.Click(editMoodsEmotions);
		Thread.sleep(1000);
		
		WebUtility.Click(editContentGrade);
		contentGrade.click();
		contentGrade.sendKeys(Keys.BACK_SPACE);
		WebUtility.Wait();
		WebUtility.Click(dropDownList.get(0));
		WebUtility.Click(editContentGrade);
		Thread.sleep(1000);
		
		WebUtility.Click(editEra);
		era.click();
		era.sendKeys(Keys.BACK_SPACE);
		WebUtility.Wait();
		WebUtility.Click(dropDownList.get(0));
		WebUtility.Click(editEra);
		Thread.sleep(1000);
		
		WebUtility.Click(editTargetAgeGroup);
		targetAge.click();
		targetAge.sendKeys(Keys.BACK_SPACE);
		WebUtility.Wait();
		WebUtility.Click(dropDownList.get(0));
		WebUtility.Click(editTargetAgeGroup);
		Thread.sleep(1000);
		
		WebUtility.Click(editTargetAudience);
		targetAudiences.click();
		targetAudiences.sendKeys(Keys.BACK_SPACE);
		WebUtility.Wait();
		WebUtility.Click(dropDownList.get(0));
		WebUtility.Click(editTargetAudience);
		Thread.sleep(1000);

		WebUtility.Click(editTags);
		tags.click();
		tags.sendKeys(Keys.CONTROL + "a");
		tags.sendKeys(Keys.BACK_SPACE);
		tags.sendKeys("Automation Tags Episode");
		WebUtility.Wait();
		WebUtility.Click(dropDownList.get(0));
		WebUtility.Wait();
		WebUtility.Click(editTags);
		Thread.sleep(1000);

		WebUtility.Click(editDigitalKeywords);
		digitalKeywords.click();
		digitalKeywords.sendKeys(Keys.CONTROL + "a");
		digitalKeywords.sendKeys(Keys.BACK_SPACE);
		digitalKeywords.sendKeys("Automation Digital Keywords Episode");
		WebUtility.Click(editDigitalKeywords);
		Thread.sleep(1000);

		WebUtility.Click(editAdaptation);
		adaptation.click();
		adaptation.sendKeys(Keys.CONTROL + "a");
		adaptation.sendKeys(Keys.BACK_SPACE);
		adaptation.sendKeys("Automation Adaptation Episode");
		WebUtility.Click(editAdaptation);
		Thread.sleep(1000);	

		WebUtility.Click(editEvent);
		events.click();
		events.sendKeys(Keys.CONTROL + "a");
		events.sendKeys(Keys.BACK_SPACE);
		events.sendKeys("Automation Event Episode");
		WebUtility.Click(editEvent);
		Thread.sleep(1000);
		
		WebUtility.Click(editProductionCompanyClassification);
		productionCo.click();
		productionCo.sendKeys(Keys.BACK_SPACE);
		WebUtility.Wait();
		WebUtility.Click(dropDownList.get(0));
		WebUtility.Click(editProductionCompanyClassification);
		Thread.sleep(1000);
		
		WebUtility.Click(editPopularityScore);
		popularityScore.click();
		popularityScore.sendKeys(Keys.CONTROL + "a");
		popularityScore.sendKeys(Keys.BACK_SPACE);
		popularityScore.sendKeys("99");
		WebUtility.Click(editPopularityScore);
		Thread.sleep(1000);
		
		WebUtility.Click(editTrivia);
		trivia.click();
		trivia.sendKeys(Keys.CONTROL + "a");
		trivia.sendKeys(Keys.BACK_SPACE);
		trivia.sendKeys("Automation Trivia Episode");
		WebUtility.Click(editTrivia);
		Thread.sleep(1000);
			
		WebUtility.Click(editEpgProgramID);
		epgProgramId.click();
		epgProgramId.sendKeys(Keys.CONTROL + "a");
		epgProgramId.sendKeys(Keys.BACK_SPACE);
		epgProgramId.sendKeys("Automation EPG Episode");
		WebUtility.Click(editEpgProgramID);
		Thread.sleep(1000);

		//-----TODO Show Air Time and Show Air Time Days removed?
		/*	
        WebUtility.Click(editShowAirTime);
        WebUtility.javascript(showAirTime);
        List < WebElement > airTimeList = driver.findElements(By.xpath("//li[@role='button']"));
        for (WebElement e: airTimeList) {
            String airTime = e.getText();
            if (airTime.contains("06")) {
                e.click();
                break;
            }
        }
		WebUtility.Click(editShowAirTime);
		WebUtility.Wait();
		
		WebUtility.Click(editShowAirTimeDays);
		showAirTimeDays.click();
		showAirTimeDays.sendKeys(Keys.BACK_SPACE);
		WebUtility.Click(dropDownList.get(0));
		WebUtility.Click(editShowAirTimeDays);
		WebUtility.Wait();

	*/		
		WebUtility.Click(editShowOnAir);
		showOnAir.click();
		showOnAir.sendKeys(Keys.BACK_SPACE);
		WebUtility.Click(dropDownList.get(0));
		WebUtility.Click(editShowOnAir);
		Thread.sleep(1000);
		
		WebUtility.Click(editShowOnApp);
		showOnApp.click();
		showOnApp.sendKeys(Keys.BACK_SPACE);
		WebUtility.Click(dropDownList.get(0));
		WebUtility.Click(editShowOnApp);
		Thread.sleep(1000);
		
		WebUtility.Click(editDamBmsID);
		damBmsId.click();
		damBmsId.sendKeys(Keys.CONTROL + "a");
		damBmsId.sendKeys(Keys.BACK_SPACE);
		damBmsId.sendKeys("Automation Dam BMS ID Episode");
		WebUtility.Click(editDamBmsID);
		Thread.sleep(1000);

		WebUtility.Click(editAwardRecipient);
		awardRecipient.click();
		awardRecipient.sendKeys(Keys.CONTROL + "a");
		awardRecipient.sendKeys(Keys.BACK_SPACE);
		awardRecipient.sendKeys("Automation Recipient Episode");
		WebUtility.Wait();
		WebUtility.Click(dropDownList.get(0));
		WebUtility.Click(editAwardRecipient);
		Thread.sleep(1000);
		
		WebUtility.Click(editAwardCategory);
		awardsCategory.click();
		WebUtility.Wait();
		awardsCategory.sendKeys(Keys.BACK_SPACE);
		WebUtility.Click(dropDownList.get(0));
		WebUtility.Click(editAwardCategory);
		Thread.sleep(1000);
		
		WebUtility.Click(editAwardHonor);
		awardHonor.click();
		awardHonor.sendKeys(Keys.CONTROL + "a");
		awardHonor.sendKeys(Keys.BACK_SPACE);
		awardHonor.sendKeys("Automation Honor Episode");
		WebUtility.Click(editAwardHonor);
		Thread.sleep(1000);
	

		WebUtility.Click(nextButton);
		
        WebUtility.Click(editIntroStartTime);
        WebUtility.javascript(introStartTime);
        List < WebElement > introStartTimeList = driver.findElements(By.xpath("//li[@role='button']"));
        for (WebElement e: introStartTimeList) {
            String startTime = e.getText();
            if (startTime.contains("01")) {
                e.click();
                break;
            }
        }
    	WebUtility.Click(editIntroStartTime);
		WebUtility.Wait();
		
	    WebUtility.Click(editIntroEndTime);
        WebUtility.javascript(introEndTime);
        List < WebElement > introEndTimeList = driver.findElements(By.xpath("//li[@role='button']"));
        for (WebElement e: introEndTimeList) {
            String endTime = e.getText();
            if (endTime.contains("02")) {
                e.click();
                break;
            }
        }
    	WebUtility.Click(editIntroEndTime);
		Thread.sleep(1000);
		
	    WebUtility.Click(editRecapStartTime);
        WebUtility.javascript(recapStartTime);
        List < WebElement > recapStartTimeList = driver.findElements(By.xpath("//li[@role='button']"));
        for (WebElement e: recapStartTimeList) {
            String recapStartTime = e.getText();
            if (recapStartTime.contains("03")) {
                e.click();
                break;
            }
        }
    	WebUtility.Click(editRecapStartTime);
		Thread.sleep(1000);
		
	    WebUtility.Click(editRecapEndTime);
        WebUtility.javascript(recapEndTime);
        List < WebElement > recapEndTimeList = driver.findElements(By.xpath("//li[@role='button']"));
        for (WebElement e: recapEndTimeList) {
            String recapEndTime = e.getText();
            if (recapEndTime.contains("04")) {
                e.click();
                break;
            }
        }
    	WebUtility.Click(editRecapEndTime);
		Thread.sleep(1000);
		
	    WebUtility.Click(editEndCreditStartTime);
        WebUtility.javascript(endCreditStartTime);
        List < WebElement > endCreditTimeList = driver.findElements(By.xpath("//li[@role='button']"));
        for (WebElement e: endCreditTimeList) {
            String endCreditTime = e.getText();
            if (endCreditTime.contains("05")) {
                e.click();
                break;
            }
        }
    	WebUtility.Click(editEndCreditStartTime);
		Thread.sleep(1000);

		WebUtility.Click(editAdMarker);
		adMarker.click();
		adMarker.sendKeys(Keys.CONTROL + "a");
		adMarker.sendKeys(Keys.BACK_SPACE);
		adMarker.sendKeys("99");
		WebUtility.Click(editAdMarker);
		Thread.sleep(1000);

	    WebUtility.Click(editSkipSongStartTime);
        WebUtility.javascript(skipSongStartTime);
        List < WebElement > skipStartTimeList = driver.findElements(By.xpath("//li[@role='button']"));
        for (WebElement e: skipStartTimeList) {
            String skipStartTime = e.getText();
            if (skipStartTime.contains("01")) {
                e.click();
                break;
            }
        }
    	WebUtility.Click(editSkipSongStartTime);
		Thread.sleep(1000);
		
	    WebUtility.Click(editSkipSongEndTime);
        WebUtility.javascript(skipSongEndTime);
        List < WebElement > skipEndTimeList = driver.findElements(By.xpath("//li[@role='button']"));
        for (WebElement e: skipEndTimeList) {
            String skipEndTime = e.getText();
            if (skipEndTime.contains("02")) {
                e.click();
                break;
            }
        }
    	WebUtility.Click(editSkipSongEndTime);
		WebUtility.Wait();
		WebUtility.Click(nextButton);
		Thread.sleep(2000);
		String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
		sa.assertTrue(doneButtonActiveColor.contains("255, 255, 255") || doneButtonActiveColor.contains("255,255,255"), "Episodes Main Content Properties Mark As Done Assertion Fails");
		WebUtility.Click(doneButtonActive);
		WebUtility.Wait();
		 try {
	            TestUtil.captureScreenshot("Episode Main Content Properties Page","Episode");
	        } catch (IOException e) {
	            System.out.println("Screenshot Capture Failed ->" + e.getMessage());
	        }
	       
	}
	
	public void EpisodeCastCrew() throws InterruptedException, Exception{
			
		WebUtility.Click(castCrew);
		WebUtility.Wait();
		try {
			WebUtility.Click(editActor);
			actor.sendKeys(Keys.CONTROL + "a");
			actor.sendKeys(Keys.CONTROL + "a");
			actor.sendKeys(Keys.BACK_SPACE);
			actor.sendKeys("Salman Khan Episode");
			WebUtility.element_to_be_clickable(dropDownList.get(0));
			WebUtility.Wait();
			WebUtility.Click(dropDownList.get(0));
			WebUtility.Click(editActor);
			WebUtility.Wait();

			WebUtility.Click(editCharacter);
			character.sendKeys(Keys.CONTROL + "a");
			character.sendKeys(Keys.BACK_SPACE);
			character.sendKeys("Test Character Episode");
			WebUtility.Wait();
			WebUtility.Click(editCharacter);
			WebUtility.Wait();

			editActorChange.get(1).click();
			actorChange.sendKeys(Keys.CONTROL + "a");
			actorChange.sendKeys(Keys.BACK_SPACE);
			actorChange.sendKeys("Test Actor Episode");
			WebUtility.element_to_be_clickable(dropDownList.get(0));
			dropDownList.get(0).click();
			WebUtility.element_to_be_clickable(editActorChange.get(1));
			editActorChange.get(1).click();
			Thread.sleep(1000);

			editPerformer.get(1).click();
			performer.sendKeys(Keys.CONTROL + "a");
			performer.sendKeys(Keys.BACK_SPACE);
			performer.sendKeys("Test Performer Episode");
			WebUtility.element_to_be_clickable(dropDownList.get(0));
			dropDownList.get(0).click();
			WebUtility.element_to_be_clickable(editPerformer.get(1));
			editPerformer.get(1).click();
			Thread.sleep(1000);

			WebUtility.Click(editHost);
			host.sendKeys(Keys.CONTROL + "a");
			host.sendKeys(Keys.BACK_SPACE);
			host.sendKeys("Test Host Episode");
			WebUtility.element_to_be_clickable(dropDownList.get(0));
			dropDownList.get(0).click();
			WebUtility.Click(editHost);
			Thread.sleep(1000);

			WebUtility.Click(editSinger);
			singer.sendKeys(Keys.CONTROL + "a");
			singer.sendKeys(Keys.BACK_SPACE);
			singer.sendKeys("Automation Test Data Episode");
			WebUtility.element_to_be_clickable(dropDownList.get(0));
			WebUtility.Click(dropDownList.get(0));
			WebUtility.Click(editSinger);
			Thread.sleep(1000);

			WebUtility.Click(editLyricst);
			lyricst.sendKeys(Keys.CONTROL + "a");
			lyricst.sendKeys(Keys.BACK_SPACE);
			lyricst.sendKeys("Automation Test Data Episode");
			WebUtility.element_to_be_clickable(dropDownList.get(0));
			WebUtility.Click(dropDownList.get(0));
			WebUtility.Click(editLyricst);
			Thread.sleep(1000);

			WebUtility.Click(editDirector);
			director.sendKeys(Keys.CONTROL + "a");
			director.sendKeys(Keys.BACK_SPACE);
			director.sendKeys("Automation Test Data Episode");
			WebUtility.element_to_be_clickable(dropDownList.get(0));
			WebUtility.Click(dropDownList.get(0));
			WebUtility.Click(editDirector);
			Thread.sleep(1000);

			WebUtility.Click(editCinematography);
			cinematography.sendKeys(Keys.CONTROL + "a");
			cinematography.sendKeys(Keys.BACK_SPACE);
			cinematography.sendKeys("Automation Test Data Episode");
			WebUtility.element_to_be_clickable(dropDownList.get(0));
			WebUtility.Click(dropDownList.get(0));
			WebUtility.Click(editCinematography);
			Thread.sleep(1000);

			WebUtility.Click(editProducer);
			producer.sendKeys(Keys.CONTROL + "a");
			producer.sendKeys(Keys.BACK_SPACE);
			producer.sendKeys("Automation Test Data Episode");
			WebUtility.element_to_be_clickable(dropDownList.get(0));
			WebUtility.Click(dropDownList.get(0));
			WebUtility.Click(editProducer);
			Thread.sleep(1000);

			WebUtility.Click(editExecutiveProducer);
			executiveProducer.sendKeys(Keys.CONTROL + "a");
			executiveProducer.sendKeys(Keys.BACK_SPACE);
			executiveProducer.sendKeys("Automation Test Data Episode");
			WebUtility.element_to_be_clickable(dropDownList.get(0));
			WebUtility.Click(dropDownList.get(0));
			WebUtility.Click(editExecutiveProducer);
			Thread.sleep(1000);

			WebUtility.Click(editMusicDirector);
			musicDirector.sendKeys(Keys.CONTROL + "a");
			musicDirector.sendKeys(Keys.BACK_SPACE);
			musicDirector.sendKeys("Automation Test Data Episode");
			WebUtility.element_to_be_clickable(dropDownList.get(0));
			WebUtility.Click(dropDownList.get(0));
			WebUtility.Click(editMusicDirector);
			Thread.sleep(1000);

			WebUtility.Click(editChoreographer);
			choreographer.sendKeys(Keys.CONTROL + "a");
			choreographer.sendKeys(Keys.BACK_SPACE);
			choreographer.sendKeys("Automation Test Data Episode");
			WebUtility.element_to_be_clickable(dropDownList.get(0));
			WebUtility.Click(dropDownList.get(0));
			WebUtility.Click(editChoreographer);
			Thread.sleep(1000);

			WebUtility.Click(editTitleThemeMusic);
			titleThemeMusic.sendKeys(Keys.CONTROL + "a");
			titleThemeMusic.sendKeys(Keys.BACK_SPACE);
			titleThemeMusic.sendKeys("Automation Test Data Episode");
			WebUtility.element_to_be_clickable(dropDownList.get(0));
			WebUtility.Click(dropDownList.get(0));
			WebUtility.Click(editTitleThemeMusic);
			Thread.sleep(1000);

			WebUtility.Click(editBackgroundScore);
			backgroundScore.sendKeys(Keys.CONTROL + "a");
			backgroundScore.sendKeys(Keys.BACK_SPACE);
			backgroundScore.sendKeys("Automation Test Data Episode");
			WebUtility.element_to_be_clickable(dropDownList.get(0));
			WebUtility.Click(dropDownList.get(0));
			WebUtility.Click(editBackgroundScore);
			Thread.sleep(1000);

			WebUtility.Click(editStoryWriter);
			storyWriter.sendKeys(Keys.CONTROL + "a");
			storyWriter.sendKeys(Keys.BACK_SPACE);
			storyWriter.sendKeys("Automation Test Data Episode");
			WebUtility.element_to_be_clickable(dropDownList.get(0));
			WebUtility.Click(dropDownList.get(0));
			WebUtility.Click(editStoryWriter);
			Thread.sleep(1000);

			WebUtility.Click(editScreenPlay);
			screenPlay.sendKeys(Keys.CONTROL + "a");
			screenPlay.sendKeys(Keys.BACK_SPACE);
			screenPlay.sendKeys("Automation Test Data Episode");
			WebUtility.element_to_be_clickable(dropDownList.get(0));
			WebUtility.Click(dropDownList.get(0));
			WebUtility.Click(editScreenPlay);
			Thread.sleep(1000);

			WebUtility.Click(editDialogueWriter);
			dialogueWriter.sendKeys(Keys.CONTROL + "a");
			dialogueWriter.sendKeys(Keys.BACK_SPACE);
			dialogueWriter.sendKeys("Automation Test Data Episode");
			WebUtility.element_to_be_clickable(dropDownList.get(0));
			WebUtility.Click(dropDownList.get(0));
			WebUtility.Click(editDialogueWriter);
			Thread.sleep(1000);

			WebUtility.Click(editFilmEditing);
			filmEditing.sendKeys(Keys.CONTROL + "a");
			filmEditing.sendKeys(Keys.BACK_SPACE);
			filmEditing.sendKeys("Automation Test Data Episode");
			WebUtility.element_to_be_clickable(dropDownList.get(0));
			WebUtility.Click(dropDownList.get(0));
			WebUtility.Click(editFilmEditing);
			Thread.sleep(1000);

			WebUtility.Click(editCasting);
			casting.sendKeys(Keys.CONTROL + "a");
			casting.sendKeys(Keys.BACK_SPACE);
			casting.sendKeys("Automation Test Data Episode");
			WebUtility.element_to_be_clickable(dropDownList.get(0));
			WebUtility.Click(dropDownList.get(0));
			WebUtility.Click(editCasting);
			Thread.sleep(1000);

			WebUtility.Click(editProductionDesign);
			productionDesign.sendKeys(Keys.CONTROL + "a");
			productionDesign.sendKeys(Keys.BACK_SPACE);
			productionDesign.sendKeys("Automation Test Data Episode");
			WebUtility.element_to_be_clickable(dropDownList.get(0));
			WebUtility.Click(dropDownList.get(0));
			WebUtility.Click(editProductionDesign);
			Thread.sleep(1000);

			WebUtility.Click(editArtDirection);
			artDirection.sendKeys(Keys.CONTROL + "a");
			artDirection.sendKeys(Keys.BACK_SPACE);
			artDirection.sendKeys("Automation Test Data Episode");
			WebUtility.element_to_be_clickable(dropDownList.get(0));
			WebUtility.Click(dropDownList.get(0));
			WebUtility.Click(editArtDirection);
			Thread.sleep(1000);

			WebUtility.Click(editSetDecoration);
			setDecoration.sendKeys(Keys.CONTROL + "a");
			setDecoration.sendKeys(Keys.BACK_SPACE);
			setDecoration.sendKeys("Automation Test Data Episode");
			WebUtility.element_to_be_clickable(dropDownList.get(0));
			WebUtility.Click(dropDownList.get(0));
			WebUtility.Click(editSetDecoration);
			Thread.sleep(1000);

			WebUtility.Click(editCostumeDesign);
			costumeDesign.sendKeys(Keys.CONTROL + "a");
			costumeDesign.sendKeys(Keys.BACK_SPACE);
			costumeDesign.sendKeys("Automation Test Data Episode");
			WebUtility.element_to_be_clickable(dropDownList.get(0));
			WebUtility.Click(dropDownList.get(0));
			WebUtility.Click(editCostumeDesign);
			Thread.sleep(1000);

			WebUtility.Click(editProductionCo);
			productionCo.sendKeys(Keys.CONTROL + "a");
			productionCo.sendKeys(Keys.BACK_SPACE);
			productionCo.sendKeys("Automation Test Data Episode");
			WebUtility.element_to_be_clickable(dropDownList.get(0));
			WebUtility.Click(dropDownList.get(0));
			WebUtility.Click(editProductionCo);
			Thread.sleep(1000);

			WebUtility.Click(editPresenter);
			presenter.sendKeys(Keys.CONTROL + "a");
			presenter.sendKeys(Keys.BACK_SPACE);
			presenter.sendKeys("Automation Test Data Episode");
			WebUtility.element_to_be_clickable(dropDownList.get(0));
			WebUtility.Click(dropDownList.get(0));
			WebUtility.Click(editPresenter);
			Thread.sleep(1000);

			WebUtility.Click(editGuest);
			guest.sendKeys(Keys.CONTROL + "a");
			guest.sendKeys(Keys.BACK_SPACE);
			guest.sendKeys("Automation Test Data Episode");
			WebUtility.element_to_be_clickable(dropDownList.get(0));
			WebUtility.Click(dropDownList.get(0));
			WebUtility.Click(editGuest);
			Thread.sleep(1000);

			WebUtility.Click(editParticipant);
			participant.sendKeys(Keys.CONTROL + "a");
			participant.sendKeys(Keys.BACK_SPACE);
			participant.sendKeys("Automation Test Data Episode");
			WebUtility.element_to_be_clickable(dropDownList.get(0));
			WebUtility.Click(dropDownList.get(0));
			WebUtility.Click(editParticipant);
			Thread.sleep(1000);

			WebUtility.Click(editJudges);
			judges.sendKeys(Keys.CONTROL + "a");
			judges.sendKeys(Keys.BACK_SPACE);
			judges.sendKeys("Automation Test Data Episode");
			WebUtility.element_to_be_clickable(dropDownList.get(0));
			WebUtility.Click(dropDownList.get(0));
			WebUtility.Click(editJudges);
			Thread.sleep(1000);

			WebUtility.Click(editNarrator);
			narrator.sendKeys(Keys.CONTROL + "a");
			narrator.sendKeys(Keys.BACK_SPACE);
			narrator.sendKeys("Automation Test Data Episode");
			WebUtility.element_to_be_clickable(dropDownList.get(0));
			WebUtility.Click(dropDownList.get(0));
			WebUtility.Click(editNarrator);
			WebUtility.Wait();

			WebUtility.Click(editSponsors);
			sponsor.sendKeys(Keys.CONTROL + "a");
			sponsor.sendKeys(Keys.BACK_SPACE);
			sponsor.sendKeys("Automation Test Data Episode");
			WebUtility.element_to_be_clickable(dropDownList.get(0));
			WebUtility.Click(dropDownList.get(0));
			WebUtility.Click(editSponsors);
			WebUtility.Wait();

			WebUtility.Click(editGraphics);
			graphics.sendKeys(Keys.CONTROL + "a");
			graphics.sendKeys(Keys.BACK_SPACE);
			graphics.sendKeys("Automation Test Data Episode");
			WebUtility.element_to_be_clickable(dropDownList.get(0));
			WebUtility.Click(dropDownList.get(0));
			WebUtility.Click(editGraphics);
			Thread.sleep(1000);

			WebUtility.Click(editVocalist);
			vocalist.sendKeys(Keys.CONTROL + "a");
			vocalist.sendKeys(Keys.BACK_SPACE);
			vocalist.sendKeys("Automation Test Data Episode");
			WebUtility.element_to_be_clickable(dropDownList.get(0));
			WebUtility.Click(dropDownList.get(0));
			WebUtility.Click(editVocalist);
			Thread.sleep(1000);

			String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
			sa.assertTrue(doneButtonActiveColor.contains("255, 255, 255") || doneButtonActiveColor.contains("255,255,255"), "Episodes Main Content Cast & Crew Mark As Done Assertion Fails");
			WebUtility.Click(doneButtonActive);
			WebUtility.Wait();
			try {
				TestUtil.captureScreenshot("Cast and Crew Section","Episode");
			} catch (IOException e) {
				System.out.println("Screenshot Capture Failed ->" + e.getMessage());
			}

			js.executeScript("window.scrollTo(0,document.body.scrollHeight);");

			try {
				TestUtil.captureScreenshot("Cast and Crew Section 2","Episode");
			} catch (IOException e) {
				System.out.println("Screenshot Capture Failed ->" + e.getMessage());
			}
		} catch (Exception e) {
			WebUtility.Click(doneButtonActive);
			e.printStackTrace();
		}
	    }

	//---------------------------Episode Videos Section --------------------------------------
	public void EpisodeVideoSection() throws InterruptedException {
		
		WebUtility.Click(videos);
        WebUtility.Wait();
/*
        WebUtility.Click(dashRootVideo);
        dashRootVideo.sendKeys(Keys.CONTROL + "a");
        dashRootVideo.sendKeys(Keys.BACK_SPACE);
        dashRootVideo.sendKeys("Episode Video Dash root");

        WebUtility.Click(dashManifestVideo);
        dashManifestVideo.sendKeys(Keys.CONTROL + "a");
        dashManifestVideo.sendKeys(Keys.BACK_SPACE);
        dashManifestVideo.sendKeys("Episode Video Dash Manifest");

        WebUtility.Click(hlsRootVideo);
        hlsRootVideo.sendKeys(Keys.CONTROL + "a");
        hlsRootVideo.sendKeys(Keys.BACK_SPACE);
        hlsRootVideo.sendKeys("Episode Test Data");

        WebUtility.Click(hlsManifestVideo);
        hlsManifestVideo.sendKeys(Keys.CONTROL + "a");
        hlsManifestVideo.sendKeys(Keys.BACK_SPACE);
        hlsManifestVideo.sendKeys("Episode Test Data");

        WebUtility.Click(drmKeyId);
        drmKeyId.sendKeys(Keys.CONTROL + "a");
        drmKeyId.sendKeys(Keys.BACK_SPACE);
        drmKeyId.sendKeys("Episode Test Data");
*/


        //WebUtility.Click(dashSuffix);
        //WebUtility.Click(dropDownList.get(0));


        mediaThekUID.sendKeys(Keys.CONTROL + "a");
        mediaThekUID.sendKeys(Keys.BACK_SPACE);
        mediaThekUID.sendKeys("Kumkum_Bhagya_2_Episode_04_May_2021_bn");
        
        importDetails.click();
        WebUtility.Click(yes.get(0));
        WebUtility.Click(ok);
        WebUtility.Wait();

        WebUtility.Click(audioTrack);
        audioTrack.sendKeys(Keys.CONTROL + "a");
        audioTrack.sendKeys(Keys.BACK_SPACE);
        audioTrack.sendKeys("Episode Test Data");

        WebUtility.Click(subTitle);
        subTitle.sendKeys(Keys.CONTROL + "a");
        subTitle.sendKeys(Keys.BACK_SPACE);
        subTitle.sendKeys("Episode Test Data");
        
        WebUtility.Click(size);
        size.sendKeys(Keys.CONTROL + "a");
        size.sendKeys(Keys.BACK_SPACE);
        size.sendKeys("Episode Test Data");
        WebUtility.Wait();
        
        WebUtility.Click(hlsSuffix);
        WebUtility.Click(dropDownList.get(0));
        //protectedCheckbox.click();
        
        String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
        sa.assertTrue(doneButtonActiveColor.contains("255, 255, 255") || doneButtonActiveColor.contains("255,255,255"), "Episodes Main Content Video Section Mark As Done Assertion Fails");
        WebUtility.Click(doneButtonActive);
        WebUtility.Wait();
        try {
            TestUtil.captureScreenshot("Episode Main Content - Video Section","Episode");
        } catch (IOException e) {
            System.out.println("Screenshot Capture Failed ->" + e.getMessage());
        }

        WebUtility.Click(episodeProperties);
        WebUtility.Wait();
		WebUtility.Click(editTitle);
        WebUtility.Click(title);
        title.sendKeys(Keys.CONTROL + "a");
        title.sendKeys(Keys.BACK_SPACE);
        title.sendKeys(tvShowTitle+"- Episode");
        WebUtility.Click(editTitle);
		Thread.sleep(1000);  
		
        WebUtility.Click(doneButtonActive);
        WebUtility.Wait();
    }
	
	public void EpisodeLicense() throws InterruptedException, Exception{

		WebUtility.Click(license);
		WebUtility.Wait();
       
        try
        {
	        WebUtility.Click(createLicense);
	
	        WebUtility.Wait();
	        useTemplateRadioButton.click();
	
	        WebUtility.Click(selectTemplate);
	        WebUtility.Click(dropDownList.get(0));
	        js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
	        try {
				int licenseTemplateSize = licenseTemplateDeleteSets.size();
				for(int i = 1;i< licenseTemplateSize ;i++)
				{
					System.out.println("License Sets Size--->"+licenseTemplateDeleteSets.size());
					licenseTemplateDeleteSets.get(1).click();
					Thread.sleep(1000);
					System.out.println("License Sets Size After Delete--->"+licenseTemplateDeleteSets.size());
				}
			}
			catch (Exception e){
				System.out.println("No Sets in License Template");
			}
	        WebUtility.Click(editLicense);
	        WebUtility.Click(editLicenseCountry);
	        WebUtility.Click(clearInput);
	        for (int x = 0; x < dropDownList.size(); x++) {
	        	if(x<4) {
	            WebUtility.Click(dropDownList.get(x));
	        	}
	        	else {
	        		break;
	        	}
	        }
	        WebUtility.Wait();
	        WebUtility.Click(editLicenseCountry);
	        WebUtility.Wait();
	        WebUtility.javascript(saveLicense);
	
	        WebUtility.Click(licenseSetName);
	        licenseSetName.sendKeys("Automation Test Data");
	        
	        WebUtility.Click(licenseFromDate);
	        datePicker.sendKeys(Keys.ARROW_RIGHT);
	        datePicker.sendKeys(Keys.ESCAPE);
	
	        WebUtility.Click(licenseToDate);
	        datePicker.sendKeys(Keys.ARROW_RIGHT);
	        datePicker.sendKeys(Keys.ARROW_RIGHT);
	        datePicker.sendKeys(Keys.ESCAPE);
	
	        WebUtility.Wait();
	        WebUtility.Click(saveLicense);
        }
        catch(Exception e)
        {
        	System.out.println("Creating a license through template Failed!!");
        	e.printStackTrace();
        }
      
        try
        {
	        WebUtility.Click(createLicense);
	        
	        WebUtility.Click(licenseSetName);
	        licenseSetName.sendKeys("Automation Test Data II");
	        
	        WebUtility.Click(licenseFromDate);
	        datePicker.sendKeys(Keys.ARROW_RIGHT);
	        datePicker.sendKeys(Keys.ARROW_RIGHT);
	        datePicker.sendKeys(Keys.ARROW_RIGHT);
	        datePicker.sendKeys(Keys.ESCAPE);
	
	        WebUtility.Click(licenseToDate);
	        datePicker.sendKeys(Keys.ARROW_RIGHT);
	        datePicker.sendKeys(Keys.ARROW_RIGHT);
	        datePicker.sendKeys(Keys.ARROW_RIGHT);
	        datePicker.sendKeys(Keys.ARROW_RIGHT);
	        datePicker.sendKeys(Keys.ESCAPE);
	
	        WebUtility.Click(licenseCountry);
	        for (int x = 0; x < dropDownList.size(); x++) {
	        	if(x<4) {
	            WebUtility.Click(dropDownList.get(x));
	        	}
	        	else {
	        		break;
	        	}
	        }
	
	        licenseBusinessType.click();
	        WebUtility.Click(dropDownList.get(0));
	
	        licensePlatform.click();
	        WebUtility.Click(dropDownList.get(0));
	
	        licenseTVOD.click();
	        WebUtility.Click(dropDownList.get(0));
	
	        WebUtility.Wait();
	        WebUtility.javascript(saveLicense);
        }
        catch(Exception e)
        {
        	System.out.println("Creating a Manual License Failed");
        	e.printStackTrace();
        }

		WebUtility.element_to_be_clickable(doneButtonActive);
		String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
		sa.assertTrue(doneButtonActiveColor.contains("255, 255, 255") || doneButtonActiveColor.contains("255,255,255"), "Episodes Main Content License Mark As Done Assertion Fails");
		WebUtility.Click(doneButtonActive);

        try {
            TestUtil.captureScreenshot("Episodes Main Content - License Section","Season");
        } catch (IOException e) {
            System.out.println("Screenshot Capture Failed ->" + e.getMessage());
        }			

        WebUtility.Click(expiredLicense);
        Thread.sleep(2000);
        WebUtility.Click(expiredLicenseBack);
        Thread.sleep(2000);

        WebUtility.Click(licenseFilters);
        WebUtility.Click(licenseFromDate);
        datePicker.sendKeys(Keys.ARROW_LEFT);
        datePicker.sendKeys(Keys.ARROW_LEFT);
        datePicker.sendKeys(Keys.ESCAPE);


        WebUtility.Click(licenseToDate);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ESCAPE);

        WebUtility.Click(applyFilter);

        Thread.sleep(2000);
        WebUtility.Click(licenseFilters);
        WebUtility.Click(clearFilter);
        
        WebUtility.Click(licenseFilters);
        WebUtility.Click(licenseFilterBusinessType);
        WebUtility.Click(dropDownList.get(0));
        WebUtility.Wait();
        
        WebUtility.Click(applyFilter);

        Thread.sleep(2000);
        WebUtility.Click(licenseFilters);
        WebUtility.Click(clearFilter);

        WebUtility.Click(licenseFilters);
        WebUtility.Click(licenseFilterBillingType);
        WebUtility.Click(dropDownList.get(0));
        WebUtility.Wait();
        WebUtility.Click(applyFilter);

        Thread.sleep(2000);
        WebUtility.Click(licenseFilters);
        WebUtility.Click(clearFilter);

        WebUtility.Click(licenseFilters);
        WebUtility.Click(licenseFilterPlatform);
        WebUtility.Click(dropDownList.get(0));
        WebUtility.Wait();
        filterCloseDropdown.click();
        WebUtility.Click(applyFilter);

        Thread.sleep(2000);
        WebUtility.Click(licenseFilters);
        WebUtility.Click(clearFilter);

        WebUtility.Click(licenseFilters);
        WebUtility.Wait();
        licenseFilterActiveStatus.click();
        WebUtility.Wait();
        WebUtility.Click(applyFilter);

        Thread.sleep(2000);
        WebUtility.Click(licenseFilters);
        WebUtility.Click(clearFilter);

        WebUtility.Click(licenseFilters);
        WebUtility.Wait();
        licenseFilterInActiveStatus.click();
        WebUtility.Wait();
        WebUtility.Click(applyFilter);

        Thread.sleep(2000);
        WebUtility.Click(licenseFilters);
        WebUtility.Click(clearFilter);

	}
	
	public void EpisodeImages() throws InterruptedException, Exception{
		
		JavascriptExecutor js = (JavascriptExecutor) driver;
		WebUtility.Click(images);

		WebUtility.Wait();
		cover.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\Cover.jpg");

		WebUtility.Wait();
		appcover.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\AppCover.jpg");

		try {
		WebUtility.Wait();
		list.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\List.jpg");
		}
		catch(Exception e)
		{
			//List Image Successfully imported from UID
		}
		WebUtility.Wait();
		square.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\SquarePNG.png");

		js.executeScript("window.scrollBy(0,600)");

		WebUtility.Wait();
		tvCover.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\TVCoverPNG.png");

		WebUtility.Wait();
		portrait.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\portrait.jpg");

		WebUtility.Wait();
		listClean.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\ListCleanPNG.png");

		js.executeScript("window.scrollTo(0,document.body.scrollHeight);");

		WebUtility.Wait();
		portraitClean.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\PortraitCleanPNG.png");

		WebUtility.Wait();
		telcoSquare.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\TelcoSquare.jpg");

		WebUtility.Wait();
		passport.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\PassportPNG.png");
		WebUtility.Wait();
		
		try {
		WebUtility.Click(imagesCreateNewSet);

		WebUtility.Click(newSetName);
		newSetName.sendKeys("Automation Test Data");

		newSetCountry.click();
		WebUtility.Click(dropDownList.get(0));
		closeDropdown.click();

		newSetPlatform.click();
		WebUtility.Click(dropDownList.get(0));
		closeDropdown.click();

		newSetGender.click();
		WebUtility.Click(dropDownList.get(0));
		closeDropdown.click();

		newSetGenre.click();
		WebUtility.Click(dropDownList.get(0));
		closeDropdown.click();

		newSetAgeGroup.click();
		WebUtility.Click(dropDownList.get(0));
		closeDropdown.click();

		newSetLanguage.click();
		WebUtility.Click(dropDownList.get(0));
		closeDropdown.click();

		newSetOthers.click();
		newSetOthers.sendKeys("Automation Test Data");

		WebUtility.Click(save);

		WebUtility.Click(imageSets.get(1));

		js.executeScript("window.scrollTo(0,0)");
		WebUtility.Wait();
		cover.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\Cover.jpg");

		WebUtility.Wait();
		appcover.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\AppCover.jpg");

		WebUtility.Wait();
		list.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\List.jpg");

		WebUtility.Wait();
		square.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\SquarePNG.png");

		js.executeScript("window.scrollBy(0,600)");

		WebUtility.Wait();
		tvCover.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\TVCoverPNG.png");

		WebUtility.Wait();
		portrait.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\portrait.jpg");

		WebUtility.Wait();
		listClean.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\ListCleanPNG.png");

		js.executeScript("window.scrollTo(0,document.body.scrollHeight);");

		WebUtility.Wait();
		portraitClean.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\PortraitCleanPNG.png");

		WebUtility.Wait();
		telcoSquare.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\TelcoSquare.jpg");

		WebUtility.Wait();
		passport.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\PassportPNG.png");
		WebUtility.Wait();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		try {
			// -------------------------- Image Set 3
			// -----------------------------------------------------
			WebUtility.element_to_be_fluentwait(imagesCreateNewSet);
			WebUtility.Click(imagesCreateNewSet);

			WebUtility.Click(newSetName);
			newSetName.sendKeys("Automation Test Data II");

			newSetCountry.click();
			WebUtility.Click(dropDownList.get(1));
			closeDropdown.click();

			newSetPlatform.click();
			WebUtility.Click(dropDownList.get(1));
			closeDropdown.click();

			newSetGender.click();
			WebUtility.Click(dropDownList.get(1));
			closeDropdown.click();

			newSetGenre.click();
			WebUtility.Click(dropDownList.get(1));
			closeDropdown.click();

			newSetAgeGroup.click();
			WebUtility.Click(dropDownList.get(1));
			closeDropdown.click();

			newSetLanguage.click();
			WebUtility.Click(dropDownList.get(1));
			closeDropdown.click();

			newSetOthers.click();
			newSetOthers.sendKeys("Automation Test Data II");

			WebUtility.Click(save);

			WebUtility.Click(imageSets.get(2));

			js.executeScript("window.scrollTo(0,0)");

			WebUtility.Wait();
			cover.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\Cover.jpg");

			WebUtility.Wait();
			appcover.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\AppCover.jpg");

			WebUtility.Wait();
			list.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\List.jpg");

			WebUtility.Wait();
			square.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\SquarePNG.png");

			js.executeScript("window.scrollBy(0,600)");

			WebUtility.Wait();
			tvCover.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\TVCoverPNG.png");

			WebUtility.Wait();
			portrait.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\portrait.jpg");

			WebUtility.Wait();
			listClean.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\ListCleanPNG.png");

			js.executeScript("window.scrollTo(0,document.body.scrollHeight);");

			WebUtility.Wait();
			portraitClean.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\PortraitCleanPNG.png");

			WebUtility.Wait();
			telcoSquare.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\TelcoSquare.jpg");

			WebUtility.Wait();
			passport.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\PassportPNG.png");
			WebUtility.Wait();
		} catch (Exception e) {
			e.printStackTrace();
		}

		// ---------------------- Inactivate Image Set ------------------------
		try {
			js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
			WebUtility.Wait();
			WebUtility.Click(imageSetActiveButton);
			WebUtility.Wait();
			WebUtility.Click(dropDownList.get(0));
			WebUtility.javascript(yes.get(0));
			WebUtility.Wait();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		//------------------------ Delete Image Set -----------------------------
		try {
			removeSet.get(1).click();
			WebUtility.Wait();
			yes.get(0).click();
			WebUtility.Wait();
		} catch (Exception e) {
			e.printStackTrace();
		}
		String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
		sa.assertTrue(doneButtonActiveColor.contains("255, 255, 255") || doneButtonActiveColor.contains("255,255,255"), "Episodes Main Content Images Mark As Done Assertion Fails");
		WebUtility.Click(doneButtonActive);

		js.executeScript("window.scrollTo(0,0);");
		WebUtility.Wait();
		WebUtility.Click(imageSets.get(0));
		try {
			TestUtil.captureScreenshot("Episode Main Content Images Section Default Set","Episode");
		} catch (IOException e) {
			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}

		js.executeScript("window.scrollTo(0,document.body.scrollHeight);");

		try {
			TestUtil.captureScreenshot("Episode Main Content Images Section Default Set 2 ","Episode");
		} catch (IOException e) {
			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}

		WebUtility.Click(imageSets.get(1));
		js.executeScript("window.scrollTo(0,0);");
		try {
			TestUtil.captureScreenshot("Episode Main Content Images Section Created Set","Episode");
		} catch (IOException e) {
			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}
		js.executeScript("window.scrollTo(0,document.body.scrollHeight);");

		try {
			TestUtil.captureScreenshot("Episode Main Content Images Section Created Set 2","Episode");
		} catch (IOException e) {
			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}

	}
	
	public void EpisodeSEODetails() throws InterruptedException, Exception{
		JavascriptExecutor js = (JavascriptExecutor) driver;
	/*
		WebUtility.javascript(dashboard);
		WebUtility.Click(tvShowButton);
		WebUtility.Click(searchString);
		searchString.sendKeys("1234567");
		searchString.sendKeys(Keys.ENTER);
		Thread.sleep(2000);
		WebUtility.Wait();
		WebUtility.Click(editButton.get(0));
		
		WebUtility.Wait();
		WebUtility.Click(seasons);

		WebUtility.Click(editButton.get(0));
		
		WebUtility.Click(episodes);
		WebUtility.Wait();
		WebUtility.Click(createEpisode);
		createManualEpisode.click();
	
		WebUtility.Click(yes.get(0));
		
		createMainEpisode.click();
		WebUtility.Click(yes.get(0));
		*/
		WebUtility.Click(seoDetails);
    	WebUtility.Wait();

		editSeoTitleTag.click();
		seoTitleTag.click();
		seoTitleTag.sendKeys(Keys.CONTROL + "a");
		seoTitleTag.sendKeys(Keys.BACK_SPACE);
		seoTitleTag.sendKeys("Automation Test Data Episode");
		editSeoTitleTag.click();
		Thread.sleep(1000);
	
		editSeoMetaDescription.click();
		seoMetaDescription.click();
		seoMetaDescription.sendKeys(Keys.CONTROL + "a");
		seoMetaDescription.sendKeys(Keys.BACK_SPACE);
		seoMetaDescription.sendKeys("Automation Test Data Episode");
		editSeoMetaDescription.click();
		Thread.sleep(1000);

		editSeoMetaSynopsis.click();
		seoMetaSynopsis.click();
		seoMetaSynopsis.sendKeys(Keys.CONTROL + "a");
		seoMetaSynopsis.sendKeys(Keys.BACK_SPACE);
		seoMetaSynopsis.sendKeys("Automation Test Data Episode");
		editSeoMetaSynopsis.click();
		Thread.sleep(1000);

		editSeoRedirectionType.click();
		seoRedirectionType.click();
		dropDownList.get(0).click();
		WebUtility.Wait();
		editSeoRedirectionType.click();
		Thread.sleep(1000);

		editSeoRedirectionLink.click();
		seoRedirectionLink.click();
		seoRedirectionLink.sendKeys(Keys.CONTROL + "a");
		seoRedirectionLink.sendKeys(Keys.BACK_SPACE);
		seoRedirectionLink.sendKeys("https://www.kelltontech.com/Episode");
		editSeoRedirectionLink.click();
		Thread.sleep(1000);

		editSeoNoIndex.click();
		editSeoNoIndex.click();
		Thread.sleep(1000);
		
		editSeoH1Heading.click();
		seoH1Heading.click();
		seoH1Heading.sendKeys(Keys.CONTROL + "a");
		seoH1Heading.sendKeys(Keys.BACK_SPACE);
		seoH1Heading.sendKeys("Automation Test Data Episode");
		editSeoH1Heading.click();
		Thread.sleep(1000);

		editSeoH2Heading.click();
		seoH2Heading.click();
		seoH2Heading.sendKeys(Keys.CONTROL + "a");
		seoH2Heading.sendKeys(Keys.BACK_SPACE);
		seoH2Heading.sendKeys("Automation Test Data Episode");
		editSeoH2Heading.click();
		Thread.sleep(1000);

		editSeoH3Heading.click();
		seoH3Heading.click();
		seoH3Heading.sendKeys(Keys.CONTROL + "a");
		seoH3Heading.sendKeys(Keys.BACK_SPACE);
		seoH3Heading.sendKeys("Automation Test Data Episode");
		editSeoH3Heading.click();
		Thread.sleep(1000);

		editSeoH4Heading.click();
		seoH4Heading.click();
		seoH4Heading.sendKeys(Keys.CONTROL + "a");
		seoH4Heading.sendKeys(Keys.BACK_SPACE);
		seoH4Heading.sendKeys("Automation Test Data Episode");
		editSeoH4Heading.click();
		Thread.sleep(1000);

		editSeoH5Heading.click();
		seoH5Heading.click();
		seoH5Heading.sendKeys(Keys.CONTROL + "a");
		seoH5Heading.sendKeys(Keys.BACK_SPACE);
		seoH5Heading.sendKeys("Automation Test Data Episode");
		editSeoH5Heading.click();
		Thread.sleep(1000);

		editSeoH6Heading.click();
		seoH6Heading.click();
		seoH6Heading.sendKeys(Keys.CONTROL + "a");
		seoH6Heading.sendKeys(Keys.BACK_SPACE);
		seoH6Heading.sendKeys("Automation Test Data Episode");
		editSeoH6Heading.click();
		Thread.sleep(1000);

		editSeoRobotMetaIndex.click();
		seoRobotsMetaIndex.get(0).click();
		seoRobotsMetaIndex.get(0).sendKeys(Keys.CONTROL + "a");
		seoRobotsMetaIndex.get(0).sendKeys(Keys.BACK_SPACE);
		seoRobotsMetaIndex.get(0).sendKeys("Automation Test Data Episode");
		editSeoRobotMetaIndex.click();
		Thread.sleep(1000);

		editSeoRobotMetaNoIndex.click();
		seoRobotsMetaIndex.get(0).click();
		seoRobotsMetaIndex.get(0).sendKeys(Keys.CONTROL + "a");
		seoRobotsMetaIndex.get(0).sendKeys(Keys.BACK_SPACE);
		seoRobotsMetaIndex.get(0).sendKeys("Automation Test Data Episode");
		editSeoRobotMetaNoIndex.click();
		Thread.sleep(1000);

		js.executeScript("window.scrollTo(0,document.body.scrollHeight);");

		editSeoRobotMetaImageIndex.click();
		seoRobotsMetaImageIndex.click();
		seoRobotsMetaImageIndex.sendKeys(Keys.CONTROL + "a");
		seoRobotsMetaImageIndex.sendKeys(Keys.BACK_SPACE);
		seoRobotsMetaImageIndex.sendKeys("Automation Test Data Episode");
		editSeoRobotMetaImageIndex.click();
		Thread.sleep(1000);

		editSeoRobotMetaImageNoIndex.click();
		seoRobotsMetaImageNoIndex.click();
		seoRobotsMetaImageNoIndex.sendKeys(Keys.CONTROL + "a");
		seoRobotsMetaImageNoIndex.sendKeys(Keys.BACK_SPACE);
		seoRobotsMetaImageNoIndex.sendKeys("Automation Test Data Episode");
		editSeoRobotMetaImageNoIndex.click();
		Thread.sleep(1000);

		editSeoBreadcrumbTitle.click();
		seoBreadcrumbTitle.click();
		seoBreadcrumbTitle.sendKeys(Keys.CONTROL + "a");
		seoBreadcrumbTitle.sendKeys(Keys.BACK_SPACE);
		seoBreadcrumbTitle.sendKeys("Automation Test Data Episode");
		editSeoBreadcrumbTitle.click();
		Thread.sleep(1000);

		editSeoInternalLinkBuilding.click();
		seoInternalLinkBuilding.click();
		seoInternalLinkBuilding.sendKeys(Keys.CONTROL + "a");
		seoInternalLinkBuilding.sendKeys(Keys.BACK_SPACE);
		seoInternalLinkBuilding.sendKeys("Automation Test Data Episode");
		editSeoInternalLinkBuilding.click();
		Thread.sleep(1000);

		editSeoVideoObjectSchema.click();
		editSeoVideoObjectSchema.click();
		Thread.sleep(1000);

		editSeoWebsiteSchema.click();
		editSeoWebsiteSchema.click();
		Thread.sleep(1000);

		editSeoBreadcrumListSchema.click();
		editSeoBreadcrumListSchema.click();
		Thread.sleep(1000);

		editSeoImageObjectSchema.click();
		editSeoImageObjectSchema.click();
		Thread.sleep(1000);

		editSeoOrganizationSchema.click();
		editSeoOrganizationSchema.click();
		Thread.sleep(1000);

		editSeoImageGallerySchema.click();
		editSeoImageGallerySchema.click();
		Thread.sleep(1000);

		editSeoLinkToStories.click();
		editSeoLinkToStories.click();
		Thread.sleep(1000);

		String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
		sa.assertTrue(doneButtonActiveColor.contains("255, 255, 255") || doneButtonActiveColor.contains("255,255,255"), "Episodes Main Content SEO Details Mark As Done Assertion Fails");
		WebUtility.Click(doneButtonActive);

	}
	
	public void EpisodePublishFlow() throws InterruptedException, Exception{
		
		//--Verifying Episode Draft Status
		try
		{
			episodeProperties.click();
			WebUtility.element_to_be_visible(contentStatus);
			TestUtil.captureScreenshot("Episode Main Content Draft Status","Episode");
			String expectedStatus = "Draft";
			String actualStatus = contentStatus.getText();
			sa.assertEquals(actualStatus,expectedStatus,"Episode Draft Status is Failed");
			Reporter.log("Episode Draft Status is Passed",true);	
		}
		catch (Exception e)
		{
			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}
		WebUtility.Click(checklist);
		WebUtility.Wait();
		js.executeScript("window.scrollBy(0,600)");
		scheduledCountry.get(0).click();
		WebUtility.Click(dropDownList.get(0));
		WebUtility.Click(publishContent);
		WebUtility.Click(yes.get(0));
		WebUtility.Click(ok);     
        
        js.executeScript("window.scrollTo(0,0)");        
        WebUtility.Wait();
        
        //--Verifying Episode Published Status
  		try
  		{
  			episodeProperties.click();
  			WebUtility.Wait();
  			WebUtility.element_to_be_visible(contentStatus);
  			TestUtil.captureScreenshot("Episode Main Content Published Status","Episode");
  			String expectedStatus = "Published";
  			String actualStatus = contentStatus.getText();
  			sa.assertEquals(actualStatus,expectedStatus,"Episode Published Status is Failed");
  			Reporter.log("Episode Published Status is Passed",true);	
  		}
  		catch (Exception e){
  			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
  		}
        
		
        //----------------Republish -----------------------------------------------------------------------------
	       
        try {
        	WebUtility.Click(license);
	        WebUtility.Wait();
	       	
	        WebUtility.Click(licenseStatusButton);
	        WebUtility.Click(inactivateLicenseDropdown);
	        WebUtility.Wait();
	        actionProvider.moveToElement(inactivateLicenseReason).click().build().perform();
	        WebUtility.Click(dropDownList.get(0));
	        WebUtility.Click(yes.get(0));
	        WebUtility.Click(doneButtonActive);
       }
       catch(Exception e)
       {
    	   try
    	   {
    	   WebUtility.Click(no);
    	   }
    	   catch(Exception e1) {
    		 e1.printStackTrace();
    	   }
    	   WebUtility.Click(seoDetails);
			editSeoTitleTag.click();
			Thread.sleep(1000);
			seoTitleTag.click();
			seoTitleTag.sendKeys(Keys.CONTROL + "a");
			seoTitleTag.sendKeys(Keys.BACK_SPACE);
			Thread.sleep(2000);
			seoTitleTag.sendKeys("Automation Test Data Episode Quick Filing Edit");
			Thread.sleep(1000);
			editSeoTitleTag.click();
			Thread.sleep(1000);
			WebUtility.Click(doneButtonActive);
			e.printStackTrace();                
       }
        
        WebUtility.Click(checklist);
        js.executeScript("window.scrollTo(0,document.body.scrollHeight)");
        WebUtility.Click(republishContent);
        WebUtility.Click(yes.get(0));
        //WebUtility.Click(yes.get(1));
        WebUtility.Click(ok);
        js.executeScript("window.scrollTo(0,0)");
		WebUtility.Wait();
		
		//--Verifying TvShow Republished status
		try
		{
				episodeProperties.click();
				WebUtility.element_to_be_visible(contentStatus);
				TestUtil.captureScreenshot("Episode Main Content Republished Status","Episode");
				String expectedStatus = "Published";
				String actualStatus = contentStatus.getText();
				sa.assertEquals(actualStatus,expectedStatus,"Episode Main Content Republished Status is Failed");
		}
		catch (Exception e)
		{
				e.printStackTrace();
		}
		
		//----------------------------Unpublish ----------------------------------------------
		WebUtility.Click(checklist);
		WebUtility.Wait();
        js.executeScript("window.scrollTo(0,document.body.scrollHeight);");       
        WebUtility.Click(unpublishContent);
        WebUtility.Click(unpublishReason);
        WebUtility.Click(dropDownList.get(0));
    	WebUtility.Click(yes.get(0));
		WebUtility.Click(ok);
        
        js.executeScript("window.scrollTo(0,0)");
        
        //------------------Verifying Episode Unpublished Status----------------------------------
        try {
        	episodeProperties.click();
			WebUtility.element_to_be_visible(contentStatus);
			TestUtil.captureScreenshot("Episode Main Content Unpublished Status","Episode");
			String expectedStatus = "Unpublished";
			String actualStatus = contentStatus.getText();
			sa.assertEquals(actualStatus,expectedStatus,"Episode Unpublished Status is Failed");
			Reporter.log("Episode Unpublished Status is Passed",true);	
        } catch (IOException e) {
            System.out.println("Screenshot Capture Failed ->" + e.getMessage());
        }
   
        // ------------- Schedule Content -------------------------------------
    	WebUtility.Click(checklist);
    	js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
        WebUtility.Click(scheduleContent);

        //System.out.println("--------> Current Time = "+currentTime.getAttribute("innerText").substring(20, 28));
        //String currentTimeString  = currentTime.getAttribute("innerText").substring(20, 27);
    	//js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
    	
        WebUtility.Click(scheduledPublicationTime);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        WebUtility.Click(datePickerMinutes);
        actionProvider.moveToElement(datePickerMinutesClock).click().build().perform();      
        datePicker.sendKeys(Keys.ESCAPE);					  

        WebUtility.Click(scheduledContentCountry);
        WebUtility.Click(dropDownList.get(0));    

        WebUtility.Click(scheduleContentButton);
        WebUtility.Click(yes.get(0));
        WebUtility.Wait();
        WebUtility.Click(ok);

        js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
        WebUtility.Wait();
        js.executeScript("window.scrollTo(0,0);");
        WebUtility.Wait();
        
     
       // System.out.println("--------> Scheduled Time = "+scheduledTime.getAttribute("innerText").substring(12, 20));
        //String scheduledTimeString = scheduledTime.getAttribute("innerText").substring(12, 20);
    	//js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
    	//sa.assertTrue(scheduledTimeString.contains(currentTimeString),"Episode Main Content - Current Time and Scheduled Time are not Equal");

      //-------------------Verifying Episode Scheduled Status---------------------------------
        try {
        	episodeProperties.click();
			WebUtility.Wait();
			WebUtility.element_to_be_visible(contentStatus);
            TestUtil.captureScreenshot("Episode Main Content Scheduled Status","Episode");
			String expectedStatus = "Scheduled";
			String actualStatus = contentStatus.getText();
			sa.assertEquals(actualStatus,expectedStatus,"Episode Main Content Scheduled Status is Failed");
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        publishedHistory.click();
        WebUtility.Wait();
        try {
            TestUtil.captureScreenshot("Episode Main Content - Published History","Episode");
        } catch (IOException e) {
            System.out.println("Screenshot Capture Failed ->" + e.getMessage());
        }
        
        episodesBreadcrumb.click();
     
	}

	//-----------------------------------------Episode Ordering ----------------------------------------------------------------------------
	public void EpisodesOrdering() throws InterruptedException,Exception{
	/*	
		dashboard.click();
		tvShowButton.click();
		Thread.sleep(2000);
	
		searchString.click();
		searchString.sendKeys("Zee5 The Expanse Suite");
		Thread.sleep(2000);
		WebUtility.element_to_be_clickable(editButton.get(0));
		editButton.get(0).click();
		Thread.sleep(2000);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		seasons.click();
		Thread.sleep(2000);
	/*
		if(seasonListingOrder.size() > 2)
		{
		
		WebElement clickSource = seasonListingOrder.get(0);
		WebElement clickDestination = seasonListingOrder.get(2);
		Actions action = new Actions(driver);		
		action.moveToElement(clickSource).clickAndHold().moveByOffset(100, 400).release().build().perform();
		
		
		//action.clickAndHold().perform();
		//action.clickAndHold(clickSource).perform();
		//action.dragAndDropBy(clickSource, 50, 200).perform();
		//action.moveByOffset(100,300).perform();
		//action.release().perform();
		//action.dragAndDrop(clickSource, clickDestination).perform();
		}
 */
		WebUtility.Click(setOrdering);
		WebUtility.element_to_be_clickable(createNewSet);		
		createNewSet.click();
		WebUtility.element_to_be_clickable(setName);
		setName.click();
		setName.sendKeys("Ordering Test");
		Thread.sleep(1000);
		setCountry.click();
		Thread.sleep(2000);
		for (int x = 0; x < dropDownList.size(); x++)
			dropDownList.get(x).click();
		Thread.sleep(1000);
		closeDropdown.click();
		yes.get(0).click();
	}
	
	   public void EpisodesRelatedContent() throws InterruptedException {
//-----------Edit Flow for Testing ----------------------------------------
/*
	    	WebUtility.javascript(dashboard);
			WebUtility.Click(tvShowButton);
			WebUtility.Click(searchString);
			searchString.sendKeys(tvShowTitle);
			searchString.sendKeys(Keys.ENTER);
			WebUtility.Wait();
			WebUtility.Wait();
			WebUtility.Click(editButton.get(0));
			
			WebUtility.Wait();
			WebUtility.Click(seasons);

			WebUtility.Click(editButton.get(0));
			
			WebUtility.Click(episodes);
			WebUtility.Click(editButton.get(2));
*/
		  
		     WebUtility.Click(relatedContent);	
		     WebUtility.Wait();
		     WebUtility.Click(relatedContentAssignTvShows);
	         WebUtility.Click(relatedContentAddTvShows);
	         WebUtility.Wait();
	         checkBox.get(0).click();
	         checkBox.get(1).click();
	         WebUtility.Click(assignRelatedContent);
	         WebUtility.Click(deleteRelatedContent); 
	         WebUtility.Wait();
	         WebUtility.Click(yes.get(0));
	         WebUtility.Wait();
	   

	         WebUtility.Click(relatedContentAssignVideos);
	         WebUtility.element_to_be_visible(relatedContentAddVideos);
	         WebUtility.Click(relatedContentAddVideos);

	         WebUtility.Wait();
	         checkBox.get(0).click();
	         checkBox.get(1).click();
	         WebUtility.Click(assignRelatedContent);
	         WebUtility.Click(deleteRelatedContent);
	         WebUtility.Wait();
	         WebUtility.Click(yes.get(0));
	         WebUtility.Wait();
	         
	         WebUtility.Click(relatedContentAssignMovies);
	         WebUtility.element_to_be_visible(relatedContentAddMovies);
	         WebUtility.Click(relatedContentAddMovies);

	         WebUtility.Wait();
	         checkBox.get(0).click();
	         checkBox.get(1).click();
	         WebUtility.Click(assignRelatedContent);
	         WebUtility.Click(deleteRelatedContent);
	         WebUtility.Wait();
	         WebUtility.Click(yes.get(0));
	         WebUtility.Wait();

	         WebUtility.Click(relatedContentAssignSeasons);
	         WebUtility.element_to_be_visible(relatedContentAddSeasons);
	         WebUtility.Click(relatedContentAddSeasons);

	         WebUtility.Wait();
	         checkBox.get(0).click();
	         checkBox.get(1).click();
	         WebUtility.Click(assignRelatedContent);
	         WebUtility.Click(deleteRelatedContent);
	         WebUtility.Wait();
	         WebUtility.Click(yes.get(0));
	         WebUtility.Wait();

	         WebUtility.Click(relatedContentAssignEpisodes);
	         WebUtility.element_to_be_visible(relatedContentAddEpisodes);
	         WebUtility.Click(relatedContentAddEpisodes);

	         WebUtility.Wait();
	         checkBox.get(0).click();
	         checkBox.get(1).click();
	         WebUtility.Click(assignRelatedContent);
	         WebUtility.Click(deleteRelatedContent);
	         WebUtility.Wait();
	         WebUtility.Click(yes.get(0));
	         WebUtility.Wait();

	        String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
			sa.assertTrue(doneButtonActiveColor.contains("255, 255, 255") || doneButtonActiveColor.contains("255,255,255"), "Episodes Main Content Related Content Mark As Done Assertion Fails");
	        WebUtility.Click(doneButtonActive);


	        WebUtility.Click(updateEpisodeBreadcrumb);

	    }

	    public void EpisodesTranslations() throws InterruptedException {	   
	 	
	    	JavascriptExecutor js = (JavascriptExecutor) driver;
	    	WebUtility.Click(translations);
	        js.executeScript("window.scrollTo(0,0);");
	    	WebUtility.Wait();
	    	
	    	WebUtility.Click(editTranslation.get(0));
	    	WebUtility.Click(title);
	        title.sendKeys(Keys.CONTROL + "a");
	        title.sendKeys(Keys.BACK_SPACE);
	        title.sendKeys("फैलाव");
	        saveTranslation.click();
	        Thread.sleep(1000);

	        WebUtility.Click(editTranslation.get(1));
	        WebUtility.Click(translationsShortDesc);
	        js.executeScript("arguments[0].focus()", translationsShortDesc);
	        translationsShortDesc.sendKeys(Keys.CONTROL + "a");
	        translationsShortDesc.sendKeys(Keys.BACK_SPACE);
	        translationsShortDesc.sendKeys("जेम्स एस ए कोरी द्वारा इसी नाम के उपन्यासों की श्रृंखला के आधार पर मार्क फर्गस और हॉक ओस्बी द्वारा विकसित एक अमेरिकी विज्ञान कथा टेलीविजन श्रृंखला है।");
	        saveTranslation.click();
	        Thread.sleep(1000);
	        
	        WebUtility.Click(editTranslation.get(2));
	        WebUtility.Click(translationsWebDesc);
	        translationsWebDesc.sendKeys(Keys.CONTROL + "a");
	        translationsWebDesc.sendKeys(Keys.BACK_SPACE);
	        translationsWebDesc.sendKeys("स्वचालन परीक्षण");
	        saveTranslation.click();
	        Thread.sleep(1000);

	        WebUtility.Click(editTranslation.get(3));
	        WebUtility.Click(translationsAppDesc);
	        translationsAppDesc.sendKeys(Keys.CONTROL + "a");
	        translationsAppDesc.sendKeys(Keys.BACK_SPACE);
	        translationsAppDesc.sendKeys("स्वचालन परीक्षण");
	        saveTranslation.click();
	        Thread.sleep(1000);

	        WebUtility.Click(editTranslation.get(4));
	        WebUtility.Click(translationsTVDesc);
	        translationsTVDesc.sendKeys(Keys.CONTROL + "a");
	        translationsTVDesc.sendKeys(Keys.BACK_SPACE);
	        translationsTVDesc.sendKeys("स्वचालन परीक्षण");
	        saveTranslation.click();
	        Thread.sleep(1000);

	        WebUtility.Click(editTranslation.get(5));
	        WebUtility.Click(creativeTitle);
	        creativeTitle.sendKeys(Keys.CONTROL + "a");
	        creativeTitle.sendKeys(Keys.BACK_SPACE);
	        creativeTitle.sendKeys("स्वचालन रचनात्मक शीर्षक");
	        saveTranslation.click();
	        Thread.sleep(1000);

	        WebUtility.Click(editTranslation.get(6));
	        WebUtility.Click(translationsAlternativeTitle);
	        translationsAlternativeTitle.sendKeys(Keys.CONTROL + "a");
	        translationsAlternativeTitle.sendKeys(Keys.BACK_SPACE);
	        translationsAlternativeTitle.sendKeys("स्वचालन वैकल्पिक शीर्षक");
	        saveTranslation.click();
	        Thread.sleep(1000);

	        WebUtility.Click(editTranslation.get(7));
	        WebUtility.Click(pageTitle);
	        pageTitle.sendKeys(Keys.CONTROL + "a");
	        pageTitle.sendKeys(Keys.BACK_SPACE);
	        pageTitle.sendKeys("ऑटोमेशन पेज का शीर्षक");
	        saveTranslation.click();
	        Thread.sleep(1000);

	        WebUtility.Click(editTranslation.get(8));
	        WebUtility.Click(pageDescription);
	        pageDescription.sendKeys(Keys.CONTROL + "a");
	        pageDescription.sendKeys(Keys.BACK_SPACE);
	        pageDescription.sendKeys("स्वचालन पृष्ठ विवरण");
	        saveTranslation.click();
	        Thread.sleep(1000);
	        
	        WebUtility.Click(editTranslation.get(9));
	        WebUtility.Click(translationsTitleForSocialShare);
	        translationsTitleForSocialShare.sendKeys(Keys.CONTROL + "a");
	        translationsTitleForSocialShare.sendKeys(Keys.BACK_SPACE);
	        translationsTitleForSocialShare.sendKeys("सामाजिक हिस्सेदारी के लिए स्वचालन शीर्षक");
	        saveTranslation.click();
	        Thread.sleep(1000);

	        WebUtility.Click(editTranslation.get(10));
	        WebUtility.Click(translationsDescriptionForSocialShare);
	        translationsDescriptionForSocialShare.sendKeys(Keys.CONTROL + "a");
	        translationsDescriptionForSocialShare.sendKeys(Keys.BACK_SPACE);
	        translationsDescriptionForSocialShare.sendKeys("सामाजिक शेयर के लिए स्वचालन विवरण");
	        saveTranslation.click();
	        Thread.sleep(1000);

	        WebUtility.Click(editTranslation.get(11));
	        WebUtility.Click(translationsUpcomingPage);
	        translationsUpcomingPage.sendKeys(Keys.CONTROL + "a");
	        translationsUpcomingPage.sendKeys(Keys.BACK_SPACE);
	        translationsUpcomingPage.sendKeys("ऑटोमेशन आगामी पृष्ठ पाठ");
	        saveTranslation.click();
	        Thread.sleep(1000);

	        WebUtility.Click(editTranslation.get(12));
	        WebUtility.Click(trivia);
	        trivia.sendKeys(Keys.CONTROL + "a");
	        trivia.sendKeys(Keys.BACK_SPACE);
	        trivia.sendKeys("ऑटोमेशन ट्रिविया");
	        saveTranslation.click();
	        Thread.sleep(1000);
	        
	        js.executeScript("window.scrollTo(0,0);");
	        WebUtility.Click(doneButtonActive);
	        WebUtility.Wait();

	        castCrew.click();        
	        
	        WebUtility.Click(editTranslationActor);
	        WebUtility.Click(translationsActor);
	        translationsActor.sendKeys(Keys.CONTROL + "a");
	        translationsActor.sendKeys(Keys.BACK_SPACE);
	        translationsActor.sendKeys("ऑटोमेशन आगामी पृष्ठ पाठ");
	        saveTranslation.click();
	        Thread.sleep(1000);

	        WebUtility.Click(editTranslationCharacter);
	        WebUtility.Click(translationsCharacter);
	        translationsCharacter.sendKeys(Keys.CONTROL + "a");
	        translationsCharacter.sendKeys(Keys.BACK_SPACE);
	        translationsCharacter.sendKeys("ऑटोमेशन आगामी पृष्ठ पाठ");
	        saveTranslation.click();
	        Thread.sleep(1000);

	        String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
			sa.assertTrue(doneButtonActiveColor.contains("255, 255, 255") || doneButtonActiveColor.contains("255,255,255"), "Episodes Main Content Translations Mark As Done Assertion Fails");
	        WebUtility.Click(doneButtonActive);
	        WebUtility.Wait();

	        WebUtility.Click(updateEpisodeBreadcrumb);

	    }		


//------------------------------------Episodes Single Landing -----------------------------------------------------------------
	    
	public void EpisodeSingleLandingProperties() throws InterruptedException{

			WebUtility.javascript(dashboard);
			WebUtility.Click(tvShowButton);
			WebUtility.Click(searchString);
			searchString.sendKeys(tvShowSingleLandingTitle);
			searchString.sendKeys(Keys.ENTER);
			WebUtility.Wait();
			WebUtility.Wait();
			WebUtility.Click(editButton.get(0));
			
			WebUtility.Wait();
			WebUtility.Click(seasons);
	
			WebUtility.Click(editButton.get(0));
			
			WebUtility.Wait();
			WebUtility.Click(episodes);
			WebUtility.Wait();
			WebUtility.Click(createEpisode);
			createManualEpisode.click();
		
			WebUtility.Click(yes.get(0));
			
			createSingleLandingEpisode.click();
			WebUtility.Click(yes.get(0));
			
			WebUtility.Wait();
			
			WebUtility.Click(editTitle);
			WebUtility.Click(title);
			title.sendKeys(Keys.CONTROL + "a");
			title.sendKeys(Keys.BACK_SPACE);
			title.sendKeys(tvShowSingleLandingTitle+" - Episode Single Landing");
			WebUtility.Click(editTitle);
			WebUtility.Wait();
	
			WebUtility.Click(editShortDesc);
			WebUtility.Click(shortDesc);
			shortDesc.sendKeys(Keys.CONTROL + "a");
			shortDesc.sendKeys(Keys.BACK_SPACE);
			shortDesc.sendKeys("The Expanse is an American science fiction television series developed by Mark Fergus and Hawk Ostby - Episode Single Landing");
			WebUtility.Click(editShortDesc);
			WebUtility.Wait();
	
			WebUtility.Click(editSubType);
			WebUtility.Click(subType);
			clearInput.click();
			WebUtility.Click(dropDownList.get(0));
			WebUtility.Click(editSubType);
			WebUtility.Wait();
	
			WebUtility.Click(editZeeReleaseDate);
			WebUtility.Click(zee5ReleaseDate);
	        datePicker.sendKeys(Keys.ARROW_RIGHT);
	        datePicker.sendKeys(Keys.ARROW_RIGHT);
	        datePicker.sendKeys(Keys.ESCAPE);
			WebUtility.Click(editZeeReleaseDate);
			WebUtility.Wait();
	
			WebUtility.Click(editOriginalTelecastDate);
			WebUtility.Click(telecastDate);
	        datePicker.sendKeys(Keys.ARROW_RIGHT);
	        datePicker.sendKeys(Keys.ARROW_RIGHT);
	        datePicker.sendKeys(Keys.ESCAPE);
			WebUtility.Click(editOriginalTelecastDate);
			WebUtility.Wait();
	
			WebUtility.Wait();
			WebUtility.Click(editAudioLanguage);
			audioLanguage.click();
			audioLanguage.sendKeys(Keys.BACK_SPACE);
			WebUtility.Click(dropDownList.get(0));
			WebUtility.Click(editAudioLanguage);
			WebUtility.Wait();
			
			WebUtility.Click(nextButton);
			
	        WebUtility.Click(editUpcomingPageText);
	        upcomingPage.click();
	        upcomingPage.sendKeys(Keys.CONTROL + "a");
	        upcomingPage.sendKeys(Keys.BACK_SPACE);
	        upcomingPage.sendKeys("Automation Upcoming Page Text Episode Single Landing");
	        WebUtility.Click(editUpcomingPageText);
	        WebUtility.Wait();
	        
			WebUtility.Click(nextButton);
			
			String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
			sa.assertTrue(doneButtonActiveColor.contains("255, 255, 255") || doneButtonActiveColor.contains("255,255,255"), "Episodes Single Landing Properties Mark As Done Assertion Fails");
			WebUtility.Click(doneButtonActive);
	}
	
	public void EpisodeSingleLandingCastCrew() throws InterruptedException{
			WebUtility.Click(castCrew);
			try {
				WebUtility.Click(editActor);
				actor.clear();
				actor.sendKeys(Keys.CONTROL + "a");
				actor.sendKeys(Keys.CONTROL + "a");
				actor.sendKeys(Keys.BACK_SPACE);
				actor.sendKeys("Salman Khan Episode Single Landing");
				WebUtility.element_to_be_clickable(dropDownList.get(0));
				dropDownList.get(0).click();
				WebUtility.Wait();
				WebUtility.Click(editActor);
				WebUtility.Wait();
	
				WebUtility.Click(editCharacter);
				character.sendKeys(Keys.CONTROL + "a");
				character.sendKeys(Keys.BACK_SPACE);
				character.sendKeys("Test Character Episode Single Landing");
				WebUtility.Click(editCharacter);
				WebUtility.Wait();
	
				WebUtility.Click(editDirector);
				director.sendKeys(Keys.CONTROL + "a");
				director.sendKeys(Keys.BACK_SPACE);
				director.sendKeys("Automation Test Data Episode");
				WebUtility.element_to_be_clickable(dropDownList.get(0));
				dropDownList.get(0).click();
				WebUtility.Wait();
				WebUtility.Click(editDirector);
				WebUtility.Wait();
	
				String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
				sa.assertTrue(doneButtonActiveColor.contains("255, 255, 255") || doneButtonActiveColor.contains("255,255,255"), "Episodes Single Landing Cast & Crew Mark As Done Assertion Fails");
				WebUtility.Click(doneButtonActive);
			} catch (Exception e) {
				WebUtility.Click(doneButtonActive);
			}
	}
	
	public void EpisodeSingleLandingLicense() throws InterruptedException, Exception{

			WebUtility.Click(license);
			WebUtility.Click(createLicense);

			WebUtility.Click(licenseSetName);
        	licenseSetName.sendKeys("Automation Test Data");
        
        	WebUtility.Click(licenseFromDate);
        	datePicker.sendKeys(Keys.ARROW_RIGHT);
        	datePicker.sendKeys(Keys.ESCAPE);

        	WebUtility.Click(licenseToDate);
	        datePicker.sendKeys(Keys.ARROW_RIGHT);
        	datePicker.sendKeys(Keys.ARROW_RIGHT);
        	datePicker.sendKeys(Keys.ESCAPE);
	
        	WebUtility.Click(licenseCountry);	
	        for (int x = 0; x < dropDownList.size(); x++) {
	        	if(x<4) {
	            WebUtility.Click(dropDownList.get(x));
	        	}
	        	else {
	        		break;
	        	}
	        }
		
		
			WebUtility.Click(singleLandingLicenseBusinessType);
			WebUtility.Click(dropDownList.get(0));

			WebUtility.Wait();
			driver.findElement(By.xpath("//span[contains(text(),'SAVE')]")).click();
		
			WebUtility.Click(license);
			WebUtility.Click(createLicense);
		
			WebUtility.Click(licenseSetName);
			licenseSetName.sendKeys("Automation Test Data II");

			WebUtility.Click(licenseFromDate);
			datePicker.sendKeys(Keys.ARROW_RIGHT);
			datePicker.sendKeys(Keys.ARROW_RIGHT);
			datePicker.sendKeys(Keys.ARROW_RIGHT);
			datePicker.sendKeys(Keys.ESCAPE);

			WebUtility.Click(licenseToDate);
        	datePicker.sendKeys(Keys.ARROW_RIGHT);
        	datePicker.sendKeys(Keys.ARROW_RIGHT);
        	datePicker.sendKeys(Keys.ARROW_RIGHT);
        	datePicker.sendKeys(Keys.ARROW_RIGHT);
        	datePicker.sendKeys(Keys.ESCAPE);
			
        	WebUtility.Click(licenseCountry);
	        for (int x = 0; x < dropDownList.size(); x++) {
	        	if(x<4) {
	            WebUtility.Click(dropDownList.get(x));
	        	}
	        	else {
	        		break;
	        	}
	        }
		
		
			WebUtility.Click(singleLandingLicenseBusinessType);
			WebUtility.Click(dropDownList.get(0));
			WebUtility.Wait();
			driver.findElement(By.xpath("//span[contains(text(),'SAVE')]")).click();
			WebUtility.Wait();
  
			WebUtility.element_to_be_clickable(doneButtonActive);
			String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
			sa.assertTrue(doneButtonActiveColor.contains("255, 255, 255") || doneButtonActiveColor.contains("255,255,255"), "Episodes Single Landing License Mark As Done Assertion Fails");
			WebUtility.Click(doneButtonActive);
			
	        WebUtility.Click(expiredLicense);
	        Thread.sleep(2000);
	        WebUtility.Click(expiredLicenseBack);
	        Thread.sleep(2000);

	        WebUtility.Click(licenseFilters);
	        WebUtility.Click(licenseFromDate);
	        datePicker.sendKeys(Keys.ARROW_LEFT);
	        datePicker.sendKeys(Keys.ARROW_LEFT);
	        datePicker.sendKeys(Keys.ESCAPE);

	        WebUtility.Click(licenseToDate);
	        datePicker.sendKeys(Keys.ARROW_RIGHT);
	        datePicker.sendKeys(Keys.ARROW_RIGHT);
	        datePicker.sendKeys(Keys.ESCAPE);

	        WebUtility.Click(applyFilter);

	        Thread.sleep(2000);
	        WebUtility.Click(licenseFilters);
	        WebUtility.Click(clearFilter);
	        
	        WebUtility.Click(licenseFilters);
	        WebUtility.Click(licenseFilterBusinessType);
	        WebUtility.Click(dropDownList.get(0));
	        WebUtility.Click(applyFilter);

	        Thread.sleep(2000);
	        WebUtility.Click(licenseFilters);
	        WebUtility.Click(clearFilter);
		    
	        WebUtility.Click(licenseFilters);
	        WebUtility.Wait();
	        licenseFilterActiveStatus.click();
	        WebUtility.Wait();
	        WebUtility.Click(applyFilter);

	        Thread.sleep(2000);
	        WebUtility.Click(licenseFilters);
	        WebUtility.Click(clearFilter);

	        WebUtility.Click(licenseFilters);
	        WebUtility.Wait();
	        licenseFilterInActiveStatus.click();
	        WebUtility.Wait();
	        WebUtility.Click(applyFilter);

	        Thread.sleep(2000);
	        WebUtility.Click(licenseFilters);
	        WebUtility.Click(clearFilter);
	
	}
	
    public void EpisodeSingleLandingImages() throws InterruptedException, Exception{
	
 		JavascriptExecutor js = (JavascriptExecutor) driver;  
 		WebUtility.Click(images);
		
 		WebUtility.Wait();
        cover.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\CoverPNG.png");

        WebUtility.Wait();
        appcover.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\AppCoverPNG.png");

        WebUtility.Wait();
        list.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\List.jpg");

        WebUtility.Wait();
        square.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\Square.jpg");
        
        js.executeScript("window.scrollBy(0,600)");

        Thread.sleep(2000);
        tvCover.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\TVCover.jpg");

        WebUtility.Wait();
        portrait.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\portraitPNG.png");

        WebUtility.Wait();
        listClean.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\ListClean.jpg");

		js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
		
		WebUtility.Wait();
        portraitClean.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\PortraitCleanPNG.png");

        WebUtility.Wait();
        telcoSquare.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\TelcoSquare.jpg");

        WebUtility.Wait();
        passport.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\Passport.jpg");
        WebUtility.Wait();

		try {
			// ------------------ Image Set 2 --------------------------------
			WebUtility.element_to_be_fluentwait(imagesCreateNewSet);
			WebUtility.Click(imagesCreateNewSet);

			WebUtility.Click(newSetName);
			newSetName.sendKeys("Automation Test Data");

			newSetCountry.click();
			WebUtility.Click(dropDownList.get(0));
			closeDropdown.click();

			newSetPlatform.click();
			WebUtility.Click(dropDownList.get(0));
			closeDropdown.click();

			newSetGender.click();
			WebUtility.Click(dropDownList.get(0));
			closeDropdown.click();

			newSetGenre.click();
			WebUtility.Click(dropDownList.get(0));
			closeDropdown.click();

			newSetAgeGroup.click();
			WebUtility.Click(dropDownList.get(0));
			closeDropdown.click();

			newSetLanguage.click();
			WebUtility.Click(dropDownList.get(0));
			closeDropdown.click();

			newSetOthers.click();
			newSetOthers.sendKeys("Automation Test Data");

			WebUtility.Click(save);

			WebUtility.Click(imageSets.get(1));

			js.executeScript("window.scrollTo(0,0)");

			WebUtility.Wait();
	        cover.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\CoverPNG.png");

	        WebUtility.Wait();
	        appcover.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\AppCoverPNG.png");

	        WebUtility.Wait();
	        list.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\List.jpg");

	        WebUtility.Wait();
	        square.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\Square.jpg");
	        
	        js.executeScript("window.scrollBy(0,600)");

	        Thread.sleep(2000);
	        tvCover.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\TVCover.jpg");

	        WebUtility.Wait();
	        portrait.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\portraitPNG.png");

	        WebUtility.Wait();
	        listClean.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\ListClean.jpg");

			js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
			
			WebUtility.Wait();
	        portraitClean.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\PortraitCleanPNG.png");

	        WebUtility.Wait();
	        telcoSquare.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\TelcoSquare.jpg");

	        WebUtility.Wait();
	        passport.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\Passport.jpg");
	        WebUtility.Wait();
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			// -------------------------- Image Set 3
			// -----------------------------------------------------
			WebUtility.element_to_be_fluentwait(imagesCreateNewSet);
			WebUtility.Click(imagesCreateNewSet);

			WebUtility.Click(newSetName);
			newSetName.sendKeys("Automation Test Data II");

			newSetCountry.click();
			WebUtility.Click(dropDownList.get(1));
			closeDropdown.click();

			newSetPlatform.click();
			WebUtility.Click(dropDownList.get(1));
			closeDropdown.click();

			newSetGender.click();
			WebUtility.Click(dropDownList.get(1));
			closeDropdown.click();

			newSetGenre.click();
			WebUtility.Click(dropDownList.get(1));
			closeDropdown.click();

			newSetAgeGroup.click();
			WebUtility.Click(dropDownList.get(1));
			closeDropdown.click();

			newSetLanguage.click();
			WebUtility.Click(dropDownList.get(1));
			closeDropdown.click();

			newSetOthers.click();
			newSetOthers.sendKeys("Automation Test Data II");

			WebUtility.Click(save);

			WebUtility.Click(imageSets.get(2));

			js.executeScript("window.scrollTo(0,0)");

			WebUtility.Wait();
	        cover.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\CoverPNG.png");

	        WebUtility.Wait();
	        appcover.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\AppCoverPNG.png");

	        WebUtility.Wait();
	        list.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\List.jpg");

	        WebUtility.Wait();
	        square.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\Square.jpg");
	        
	        js.executeScript("window.scrollBy(0,600)");

	        Thread.sleep(2000);
	        tvCover.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\TVCover.jpg");

	        WebUtility.Wait();
	        portrait.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\portraitPNG.png");

	        WebUtility.Wait();
	        listClean.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\ListClean.jpg");

			js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
			
			WebUtility.Wait();
	        portraitClean.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\PortraitCleanPNG.png");

	        WebUtility.Wait();
	        telcoSquare.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\TelcoSquare.jpg");

	        WebUtility.Wait();
	        passport.sendKeys(System.getProperty("user.dir") + "\\images\\tv\\Passport.jpg");
	        WebUtility.Wait();
		} catch (Exception e) {
			e.printStackTrace();
		}

		// ---------------------- Inactivate Image Set ------------------------
			try {
				js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
				WebUtility.Wait();
				WebUtility.Click(imageSetActiveButton);
				WebUtility.Wait();
				WebUtility.Click(dropDownList.get(0));
				WebUtility.javascript(yes.get(0));
				WebUtility.Wait();
			} catch (Exception e) {
				e.printStackTrace();
			}
		
		//------------------------ Delete Image Set -----------------------------
			try {
				removeSet.get(1).click();
				WebUtility.Wait();
				yes.get(0).click();
				WebUtility.Wait();
			} catch (Exception e) {
				e.printStackTrace();
			}
		
 
			String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
			sa.assertTrue(doneButtonActiveColor.contains("255, 255, 255") || doneButtonActiveColor.contains("255,255,255"), "Episodes Single Landing Images Mark As Done Assertion Fails");
			WebUtility.Click(doneButtonActive);
	
			js.executeScript("window.scrollTo(0,0);");
			WebUtility.Wait();
			WebUtility.Click(imageSets.get(0));
			try {
				TestUtil.captureScreenshot("Episode Single Landing Images Section Default Set","Episode");
			} catch (IOException e) {
				System.out.println("Screenshot Capture Failed ->" + e.getMessage());
			}
	
			WebUtility.Click(imageSets.get(1));
			js.executeScript("window.scrollTo(0,0);");
			try {
				TestUtil.captureScreenshot("Episode Single Landing Images Section Created Set","Episode");
			} catch (IOException e) {
				System.out.println("Screenshot Capture Failed ->" + e.getMessage());
			}
		}
    
    public void EpisodeSingleLandingSeoDetails() throws InterruptedException, Exception{
			WebUtility.Click(seoDetails);
			WebUtility.Wait();
	
			editSeoTitleTag.click();
			seoTitleTag.click();
			seoTitleTag.sendKeys(Keys.CONTROL + "a");
			seoTitleTag.sendKeys(Keys.BACK_SPACE);
			seoTitleTag.sendKeys("Automation Test Data Episode Single Landing");
			editSeoTitleTag.click();
			WebUtility.Wait();
	
			editSeoMetaDescription.click();
			seoMetaDescription.click();
			seoMetaDescription.sendKeys(Keys.CONTROL + "a");
			seoMetaDescription.sendKeys(Keys.BACK_SPACE);
			seoMetaDescription.sendKeys("Automation Test Data Episode Single Landing");
			editSeoMetaDescription.click();
			WebUtility.Wait();
	
			editSeoMetaSynopsis.click();
			seoMetaSynopsis.click();
			seoMetaSynopsis.sendKeys(Keys.CONTROL + "a");
			seoMetaSynopsis.sendKeys(Keys.BACK_SPACE);
			seoMetaSynopsis.sendKeys("Automation Test Data Episode Single Landing");
			editSeoMetaSynopsis.click();
			WebUtility.Wait();
	
			editSeoH1Heading.click();
			seoH1Heading.click();
			seoH1Heading.sendKeys(Keys.CONTROL + "a");
			seoH1Heading.sendKeys(Keys.BACK_SPACE);
			seoH1Heading.sendKeys("Automation Test Data Episode Single Landing");
			editSeoH1Heading.click();
			WebUtility.Wait();
	
			editSeoH2Heading.click();
			seoH2Heading.click();
			seoH2Heading.sendKeys(Keys.CONTROL + "a");
			seoH2Heading.sendKeys(Keys.BACK_SPACE);
			seoH2Heading.sendKeys("Automation Test Data Episode Single Landing");
			editSeoH2Heading.click();
			WebUtility.Wait();
	
			editSeoVideoObjectSchema.click();
			WebUtility.Wait();
			editSeoVideoObjectSchema.click();
			WebUtility.Wait();
	
			editSeoWebsiteSchema.click();
			WebUtility.Wait();
			editSeoWebsiteSchema.click();
			WebUtility.Wait();
	
			editSeoBreadcrumListSchema.click();
			WebUtility.Wait();
			editSeoBreadcrumListSchema.click();
			WebUtility.Wait();
	
			editSeoImageObjectSchema.click();
			WebUtility.Wait();
			editSeoImageObjectSchema.click();
			WebUtility.Wait();
	
			editSeoOrganizationSchema.click();
			WebUtility.Wait();
			editSeoOrganizationSchema.click();
			WebUtility.Wait();
	
			String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
			sa.assertTrue(doneButtonActiveColor.contains("255, 255, 255") || doneButtonActiveColor.contains("255,255,255"), "Episodes Single Landing SEO Details Mark As Done Assertion Fails");
			WebUtility.Click(doneButtonActive);
    }
	public void EpisodeSingleLandingPublishFlow() throws InterruptedException, Exception{

		//--Verifying Episode SingleLanding Draft Status
		try
		{
			episodeProperties.click();
			WebUtility.element_to_be_visible(contentStatus);
			TestUtil.captureScreenshot("Episode SingleLanding Draft Status","Episode");
			String expectedStatus = "Draft";
			String actualStatus = contentStatus.getText();
			Assert.assertEquals(actualStatus,expectedStatus,"Episode SingleLanding Draft Status is Failed");
			Reporter.log("Episode SingleLanding Draft Status is Passed",true);	
		}
		catch (Exception e)
		{
			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}
		
		WebUtility.Click(checklist);
		WebUtility.Wait();
		js.executeScript("window.scrollBy(0,600)");
		scheduledCountry.get(0).click();
		WebUtility.Click(dropDownList.get(0));
		WebUtility.Click(publishContent);
		WebUtility.Click(yes.get(0));
		WebUtility.Click(ok);      
        
        js.executeScript("window.scrollTo(0,0)"); 
        
        //--Verifying Episode SingleLanding Published Status
  		try
  		{
  			episodeProperties.click();
  			WebUtility.element_to_be_visible(contentStatus);
  			TestUtil.captureScreenshot("Episode SingleLanding Published Status","Episode");
  			String expectedStatus = "Published";
  			String actualStatus = contentStatus.getText();
  			sa.assertEquals(actualStatus,expectedStatus,"Episode  SingleLanding Published Status is Failed");
  			Reporter.log("Episode SingleLanding Published Status is Passed",true);	
  		}
  		catch (Exception e){
  			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
  		}
  		
        //----------------Republish -----------------------------------------------------------------------------
	       
        try {
	        license.click();
	        WebUtility.Wait();
	       	
	        WebUtility.Click(licenseStatusButton);
	        WebUtility.Click(inactivateLicenseDropdown);
	        WebUtility.Wait();
	        actionProvider.moveToElement(inactivateLicenseReason).click().build().perform();
	        WebUtility.Click(dropDownList.get(0));
	        WebUtility.Click(yes.get(0));
	        WebUtility.Click(doneButtonActive);
       }
       catch(Exception e)
       {
    	   try
    	   {
    	   WebUtility.Click(no);
    	   }
    	   catch(Exception e1) {
    		 e1.printStackTrace();
    	   }
			WebUtility.Click(seoDetails);
			editSeoTitleTag.click();
			Thread.sleep(1000);
			seoTitleTag.click();
			seoTitleTag.sendKeys(Keys.CONTROL + "a");
			seoTitleTag.sendKeys(Keys.BACK_SPACE);
			Thread.sleep(2000);
			seoTitleTag.sendKeys("Automation Test Data Episode Quick Filing Edit");
			Thread.sleep(1000);
			editSeoTitleTag.click();
			Thread.sleep(1000);
			WebUtility.Click(doneButtonActive);
			e.printStackTrace();           
       }
        
        WebUtility.Click(checklist);
        js.executeScript("window.scrollTo(0,document.body.scrollHeight)");
        WebUtility.Click(republishContent);
        WebUtility.Click(yes.get(0));
        //WebUtility.Click(yes.get(1));
        WebUtility.Click(ok);
        js.executeScript("window.scrollTo(0,0)");
		WebUtility.Wait();
		
		//--Verifying TvShow Republished status
		try
		{
				episodeProperties.click();
				WebUtility.element_to_be_visible(contentStatus);
				TestUtil.captureScreenshot("Episode Main Content Republished Status","Episode");
				String expectedStatus = "Published";
				String actualStatus = contentStatus.getText();
				sa.assertEquals(actualStatus,expectedStatus,"Episode Quick Filing Republished Status is Failed");
		}
		catch (Exception e)
		{
				e.printStackTrace();
		}
		
		//----------------------------Unpublish ----------------------------------------------

        WebUtility.Wait();
        WebUtility.Click(checklist);
        js.executeScript("window.scrollTo(0,document.body.scrollHeight);");       
        WebUtility.Click(unpublishContent);
        WebUtility.Click(unpublishReason);
        WebUtility.Click(dropDownList.get(0));
    	WebUtility.Click(yes.get(0));
		WebUtility.Click(ok);
        
        js.executeScript("window.scrollTo(0,0)");
        
        //--Verifying Episode SingleLanding Unpublished Status
        try {
        	episodeProperties.click();
			WebUtility.element_to_be_visible(contentStatus);
			TestUtil.captureScreenshot("Episode SingleLanding Unpublished Status","Episode");
			String expectedStatus = "Unpublished";
			String actualStatus = contentStatus.getText();
			sa.assertEquals(actualStatus,expectedStatus,"Episode SingleLanding Unpublished Status is Failed");
			Reporter.log("Episode SingleLanding Unpublished Status is Passed",true);	
        } catch (IOException e) {
            System.out.println("Screenshot Capture Failed ->" + e.getMessage());
        }
        
		// ------------- Schedule Content -------------------------------------
		WebUtility.Click(checklist);
		js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
		WebUtility.Click(scheduleContent);

        //System.out.println("--------> Current Time = "+currentTime.getAttribute("innerText").substring(20, 28));
        //String currentTimeString  = currentTime.getAttribute("innerText").substring(20, 27);
    	//js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
    	
		WebUtility.Click(scheduledPublicationTime);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        WebUtility.Click(datePickerMinutes);
        actionProvider.moveToElement(datePickerMinutesClock).click().build().perform();      
        datePicker.sendKeys(Keys.ESCAPE);

		WebUtility.Click(scheduledContentCountry);
		WebUtility.Click(dropDownList.get(0));

		WebUtility.Click(scheduleContentButton);
		WebUtility.Click(yes.get(0));
		WebUtility.Wait();
		WebUtility.Click(ok);

    	js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
        WebUtility.Wait();
        js.executeScript("window.scrollTo(0,0);");
        WebUtility.Wait();
        
     
        //System.out.println("--------> Scheduled Time = "+scheduledTime.getAttribute("innerText").substring(12, 20));
        //String scheduledTimeString = scheduledTime.getAttribute("innerText").substring(12, 20);
    	//js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
    	//sa.assertTrue(scheduledTimeString.contains(currentTimeString),"Episode Single Landing - Current Time and Scheduled Time are not Equal");

		// -------------------Verifying Episode Scheduled Status---------------------------------
		try {
			episodeProperties.click();
			WebUtility.Wait();
			WebUtility.element_to_be_visible(contentStatus);
			TestUtil.captureScreenshot("Episode Single Landing Scheduled Status", "Episode");
			String expectedStatus = "Scheduled";
			String actualStatus = contentStatus.getText();
			sa.assertEquals(actualStatus, expectedStatus, "Episode Single Landing Scheduled Status is Failed");
		} catch (Exception e) {
			e.printStackTrace();
		}

		publishedHistory.click();
		WebUtility.Wait();
		episodesBreadcrumb.click();
	}


    
		
    public void Templates() throws Exception{
    	
		WebUtility.javascript(dashboard);
		WebUtility.Click(tvShowButton);
		WebUtility.Click(searchString);
		searchString.sendKeys(tvShowTitle);
		WebUtility.Wait();
		WebUtility.Wait();
		WebUtility.Click(editButton.get(0));
		
		WebUtility.Wait();
		WebUtility.Click(seasons);

		WebUtility.Click(editButton.get(0));
		WebUtility.Wait();
		WebUtility.Click(templates);
		WebUtility.Wait();
		
		try {
			WebUtility.Click(templatesEpisode);
			WebUtility.Wait();
			WebUtility.Click(templatesCollections);
			WebUtility.Wait();
			templatesCollections.sendKeys(Keys.CONTROL + "a");
			templatesCollections.sendKeys(Keys.BACK_SPACE);
			WebUtility.Wait();
			templatesCollections.sendKeys("t");
			WebUtility.Wait();
			WebUtility.Click(dropDownList.get(0));
			webDesc.click();
			webDesc.sendKeys(Keys.CONTROL + "a");
			webDesc.sendKeys(Keys.BACK_SPACE);
			webDesc.sendKeys("Automation Test Data");
			WebUtility.Click(save);
			WebUtility.Click(ok);
			WebUtility.Wait();
		}
		
		catch(Exception e)
		{
			templatesCollections.sendKeys("i");
			WebUtility.Wait();
			WebUtility.Click(dropDownList.get(0));
			webDesc.click();
			webDesc.sendKeys(Keys.CONTROL+ "a");
			webDesc.sendKeys(Keys.BACK_SPACE);
			webDesc.sendKeys("Automation Test Data");
			WebUtility.Click(save);
			WebUtility.Click(ok);
			WebUtility.Wait();
			e.printStackTrace();
		}
	
		try {
			WebUtility.Click(templatesWebisode);
			WebUtility.Wait();
			WebUtility.Click(templatesCollections);
			WebUtility.Wait();
			templatesCollections.sendKeys(Keys.CONTROL+ "a");
			templatesCollections.sendKeys(Keys.BACK_SPACE);
			WebUtility.Wait();
			templatesCollections.sendKeys("t");
			WebUtility.Wait();
			WebUtility.Click(dropDownList.get(0));
			webDesc.click();
			webDesc.sendKeys(Keys.CONTROL+ "a");
			webDesc.sendKeys(Keys.BACK_SPACE);
			webDesc.sendKeys("Automation Test Data");
			WebUtility.Click(save);	
			WebUtility.Click(ok);
			WebUtility.Wait();
		}

		catch(Exception e)
		{
			templatesCollections.sendKeys("i");
			WebUtility.Wait();
			WebUtility.Click(dropDownList.get(0));
			webDesc.click();
			webDesc.sendKeys(Keys.CONTROL+ "a");
			webDesc.sendKeys(Keys.BACK_SPACE);
			webDesc.sendKeys("Automation Test Data");
			WebUtility.Click(save);
			WebUtility.Click(ok);
			WebUtility.Wait();
			e.printStackTrace();
		}
		
		
		try {
			WebUtility.Click(templatesMobisode);
			WebUtility.Wait();
			WebUtility.Click(templatesCollections);
			WebUtility.Wait();
			templatesCollections.sendKeys(Keys.CONTROL+ "a");
			templatesCollections.sendKeys(Keys.BACK_SPACE);
			WebUtility.Wait();
			templatesCollections.sendKeys("t");
			WebUtility.Wait();
			WebUtility.Click(dropDownList.get(0));
			webDesc.click();
			webDesc.sendKeys(Keys.CONTROL+ "a");
			webDesc.sendKeys(Keys.BACK_SPACE);
			webDesc.sendKeys("Automation Test Data");
			WebUtility.Click(save);	
			WebUtility.Click(ok);
			WebUtility.Wait();
		}
		
		catch(Exception e)
		{
			templatesCollections.sendKeys("i");
			WebUtility.Wait();
			WebUtility.Click(dropDownList.get(0));
			webDesc.click();
			webDesc.sendKeys(Keys.CONTROL+ "a");
			webDesc.sendKeys(Keys.BACK_SPACE);
			webDesc.sendKeys("Automation Test Data");
			WebUtility.Click(save);
			WebUtility.Click(ok);
			WebUtility.Wait();
			e.printStackTrace();
		}
		
		try {
			WebUtility.Click(templatesPreview);
			WebUtility.Wait();
			WebUtility.Click(templatesCollections);
			WebUtility.Wait();
			templatesCollections.sendKeys(Keys.CONTROL+ "a");
			templatesCollections.sendKeys(Keys.BACK_SPACE);
			WebUtility.Wait();
			templatesCollections.sendKeys("t");
			WebUtility.Wait();
			WebUtility.Click(dropDownList.get(0));
			webDesc.click();
			webDesc.sendKeys(Keys.CONTROL+ "a");
			webDesc.sendKeys(Keys.BACK_SPACE);
			webDesc.sendKeys("Automation Test Data");
			WebUtility.Click(save);	
			WebUtility.Click(ok);
			WebUtility.Wait();
		}

		catch(Exception e)
		{
			templatesCollections.sendKeys("i");
			WebUtility.Wait();
			WebUtility.Click(dropDownList.get(0));
			webDesc.click();
			webDesc.sendKeys(Keys.CONTROL+ "a");
			webDesc.sendKeys(Keys.BACK_SPACE);
			webDesc.sendKeys("Automation Test Data");
			WebUtility.Click(save);
			WebUtility.Click(ok);
			WebUtility.Wait();
			e.printStackTrace();
		}
		
		try {
			WebUtility.Click(templatesBestScene);
			WebUtility.Wait();
			WebUtility.Click(templatesCollections);
			WebUtility.Wait();
			templatesCollections.sendKeys(Keys.CONTROL+ "a");
			templatesCollections.sendKeys(Keys.BACK_SPACE);
			WebUtility.Wait();
			templatesCollections.sendKeys("t");
			WebUtility.Wait();
			WebUtility.Click(dropDownList.get(0));
			webDesc.click();
			webDesc.sendKeys(Keys.CONTROL+ "a");
			webDesc.sendKeys(Keys.BACK_SPACE);
			webDesc.sendKeys("Automation Test Data");
			WebUtility.Click(save);	
			WebUtility.Click(ok);
			WebUtility.Wait();
		}

		catch (Exception e) {
			templatesCollections.sendKeys("i");
			WebUtility.Wait();
			WebUtility.Click(dropDownList.get(0));
			webDesc.click();
			webDesc.sendKeys(Keys.CONTROL + "a");
			webDesc.sendKeys(Keys.BACK_SPACE);
			webDesc.sendKeys("Automation Test Data");
			WebUtility.Click(save);
			WebUtility.Click(ok);
			WebUtility.Wait();
			e.printStackTrace();
		}

		try {
			WebUtility.Click(templatesWeekInShorts);
			WebUtility.Wait();
			WebUtility.Click(templatesCollections);
			WebUtility.Wait();
			templatesCollections.sendKeys(Keys.CONTROL + "a");
			templatesCollections.sendKeys(Keys.BACK_SPACE);
			WebUtility.Wait();
			templatesCollections.sendKeys("t");
			WebUtility.Wait();
			WebUtility.Click(dropDownList.get(0));
			webDesc.click();
			webDesc.sendKeys(Keys.CONTROL + "a");
			webDesc.sendKeys(Keys.BACK_SPACE);
			webDesc.sendKeys("Automation Test Data");
			WebUtility.Click(save);
			WebUtility.Click(ok);
			WebUtility.Wait();
		}

		catch (Exception e) {
			templatesCollections.sendKeys("i");
			WebUtility.Wait();
			WebUtility.Click(dropDownList.get(0));
			webDesc.click();
			webDesc.sendKeys(Keys.CONTROL + "a");
			webDesc.sendKeys(Keys.BACK_SPACE);
			webDesc.sendKeys("Automation Test Data");
			WebUtility.Click(save);
			WebUtility.Click(ok);
			WebUtility.Wait();
			e.printStackTrace();
		}

	}

	public void EpisodePlaceholder() throws Exception {

		WebUtility.javascript(dashboard);
		WebUtility.Click(tvShowButton);
		WebUtility.Click(searchString);
		searchString.sendKeys(tvShowTitle);
		WebUtility.Wait();
		WebUtility.Wait();
		WebUtility.Click(editButton.get(0));

		WebUtility.Wait();
		WebUtility.Click(seasons);

		WebUtility.Click(editButton.get(0));

		WebUtility.Wait();
		WebUtility.Click(episodes);
		WebUtility.Wait();
		WebUtility.Click(createEpisode);
		createPlaceholderEpisode.click();

		WebUtility.Click(yes.get(0));

		assetType.click();
		WebUtility.Wait();	
		List<WebElement> assetTypeList = driver.findElements(By.xpath("//li[@role='option']"));
		for (WebElement assetType : assetTypeList) {
			if (assetType.getText().toLowerCase().contains("episode")) {
				assetType.click();
				break;
			}
		}

		dayPart.click();
		WebUtility.Wait();
		List<WebElement> dayPartList = driver.findElements(By.xpath("//li[@role='option']"));
		for (WebElement dayPart : dayPartList) {
			dayPart.click();
		}

		assetNumber.sendKeys("1");

		releaseDate.click();
		datePicker.sendKeys(Keys.ARROW_RIGHT);
		datePicker.sendKeys(Keys.ESCAPE);

		WebUtility.Wait();
		WebUtility.Click(save);
		WebUtility.Wait();
		WebUtility.Click(Yesbtn);
		WebUtility.Wait();
		WebUtility.Click(Okbtn);
		WebUtility.Wait();

		
		WebUtility.Click(createEpisode);
		createPlaceholderEpisode.click();

		WebUtility.Click(yes.get(0));

		assetType.click();
		WebUtility.Wait();	
		List<WebElement> assetTypeList2 = driver.findElements(By.xpath("//li[@role='option']"));
		for (WebElement assetType : assetTypeList2) {
			if (assetType.getText().toLowerCase().contains("webisode")) {
				assetType.click();
				break;
			}
		}

		dayPart.click();
		WebUtility.Wait();	
		List<WebElement> dayPartList2 = driver.findElements(By.xpath("//li[@role='option']"));
		for (WebElement dayPart : dayPartList2) {
			dayPart.click();
		}

		assetNumber.sendKeys("1");

		releaseDate.click();
		datePicker.sendKeys(Keys.ARROW_RIGHT);
		datePicker.sendKeys(Keys.ESCAPE);

		WebUtility.Wait();
		WebUtility.Click(save);
		WebUtility.Wait();
		WebUtility.Click(Yesbtn);
		WebUtility.Wait();
		WebUtility.Click(Okbtn);
		WebUtility.Wait();

		
		WebUtility.Click(createEpisode);
		createPlaceholderEpisode.click();

		WebUtility.Click(yes.get(0));

		assetType.click();
		WebUtility.Wait();	
		List<WebElement> assetTypeList3 = driver.findElements(By.xpath("//li[@role='option']"));
		for (WebElement assetType : assetTypeList3) {
			if (assetType.getText().toLowerCase().contains("mobisode")) {
				assetType.click();
				break;
			}
		}

		dayPart.click();
		WebUtility.Wait();	
		List<WebElement> dayPartList3 = driver.findElements(By.xpath("//li[@role='option']"));
		for (WebElement dayPart : dayPartList3) {
			dayPart.click();
		}

		assetNumber.sendKeys("1");

		releaseDate.click();
		datePicker.sendKeys(Keys.ARROW_RIGHT);
		datePicker.sendKeys(Keys.ESCAPE);

		WebUtility.Wait();
		WebUtility.Click(save);
		WebUtility.Wait();
		WebUtility.Click(Yesbtn);
		WebUtility.Wait();
		WebUtility.Click(Okbtn);
		WebUtility.Wait();
		
		
		WebUtility.Click(createEpisode);
		createPlaceholderEpisode.click();

		WebUtility.Click(yes.get(0));

		assetType.click();
		WebUtility.Wait();	
		List<WebElement> assetTypeList4 = driver.findElements(By.xpath("//li[@role='option']"));
		for (WebElement assetType : assetTypeList4) {
			if (assetType.getText().toLowerCase().contains("preview")) {
				assetType.click();
				break;
			}
		}

		dayPart.click();
		WebUtility.Wait();	
		List<WebElement> dayPartList4 = driver.findElements(By.xpath("//li[@role='option']"));
		for (WebElement dayPart : dayPartList4) {
			dayPart.click();
		}

		assetNumber.sendKeys("1");

		releaseDate.click();
		datePicker.sendKeys(Keys.ARROW_RIGHT);
		datePicker.sendKeys(Keys.ESCAPE);

		WebUtility.Wait();
		WebUtility.Click(save);
		WebUtility.Wait();
		WebUtility.Click(Yesbtn);
		WebUtility.Wait();
		WebUtility.Click(Okbtn);
		WebUtility.Wait();
		
		
		WebUtility.Click(createEpisode);
		createPlaceholderEpisode.click();

		WebUtility.Click(yes.get(0));

		assetType.click();
		WebUtility.Wait();	
		List<WebElement> assetTypeList5 = driver.findElements(By.xpath("//li[@role='option']"));
		for (WebElement assetType : assetTypeList5) {
			if (assetType.getText().toLowerCase().contains("best scene")) {
				assetType.click();
				break;
			}
		}

		dayPart.click();
		WebUtility.Wait();	
		List<WebElement> dayPartList5 = driver.findElements(By.xpath("//li[@role='option']"));
		for (WebElement dayPart : dayPartList5) {
			dayPart.click();
		}

		assetNumber.sendKeys("1");

		releaseDate.click();
		datePicker.sendKeys(Keys.ARROW_RIGHT);
		datePicker.sendKeys(Keys.ESCAPE);

		WebUtility.Wait();
		WebUtility.Click(save);
		WebUtility.Wait();
		WebUtility.Click(Yesbtn);
		WebUtility.Wait();
		WebUtility.Click(Okbtn);
		WebUtility.Wait();
		
		
		WebUtility.Click(createEpisode);
		createPlaceholderEpisode.click();

		WebUtility.Click(yes.get(0));

		assetType.click();
		WebUtility.Wait();	
		List<WebElement> assetTypeList6 = driver.findElements(By.xpath("//li[@role='option']"));
		for (WebElement assetType : assetTypeList6) {
			if (assetType.getText().toLowerCase().contains("week in shorts")) {
				assetType.click();
				break;
			}
		}

		dayPart.click();
		WebUtility.Wait();	
		List<WebElement> dayPartList6 = driver.findElements(By.xpath("//li[@role='option']"));
		for (WebElement dayPart : dayPartList6) {
			dayPart.click();
		}

		assetNumber.sendKeys("1");

		releaseDate.click();
		datePicker.sendKeys(Keys.ARROW_RIGHT);
		datePicker.sendKeys(Keys.ESCAPE);

		WebUtility.Wait();
		WebUtility.Click(save);
		WebUtility.Wait();
		WebUtility.Click(Yesbtn);
		WebUtility.Wait();
		WebUtility.Click(Okbtn);
		WebUtility.Wait();
		
		js.executeScript("window.scrollBy(0,300)");

		try {
			TestUtil.captureScreenshot("Episode Placeholder", "Episode");
		} catch (IOException e) {
			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}
	}
	
	
	
	public void EpisodeListingPage() throws InterruptedException,Exception{
        dashboard.click();
        WebUtility.Wait();
        tvShowButton.click();
        WebUtility.Wait();	
	
        WebUtility.Click(tvShowSearch);
		WebUtility.Click(dropDownList.get(2));
		WebUtility.Wait();
		WebUtility.Click(searchString);
		searchString.sendKeys(tvShowTitle+"- Episode");
		WebUtility.Wait();
		WebUtility.Wait();

		WebUtility.Click(listingExpiringLicense);
		WebUtility.Wait();
		Thread.sleep(3000);
		WebUtility.Click(tvShowBreadcrumb);
		WebUtility.Wait();
		
        WebUtility.Click(tvShowSearch);
		WebUtility.Click(dropDownList.get(2));
		WebUtility.Wait();
		WebUtility.Click(searchString);
		searchString.sendKeys(tvShowTitle+"- Episode");
		WebUtility.Wait();
		WebUtility.Wait();
				
		WebUtility.Click(listingPublishedHistory);
		WebUtility.Wait();
		Thread.sleep(3000);
		WebUtility.Click(listingClosePublishedHistory);
		WebUtility.Wait();
		
		try {
		WebUtility.Click(listingLicenseCountries);
		WebUtility.Wait();
		WebUtility.Click(listingCloseLicenseCountries);
		WebUtility.Wait();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
		WebUtility.Click(listingTvShowLink);
		WebUtility.Wait();
		Thread.sleep(3000);
		WebUtility.Click(tvShowBreadcrumb);
		WebUtility.Wait();
		
        WebUtility.Click(tvShowSearch);
		WebUtility.Click(dropDownList.get(2));
		WebUtility.Wait();
		WebUtility.Click(searchString);
		searchString.sendKeys(tvShowTitle+"- Episode");
		WebUtility.Wait();
		WebUtility.Wait();
		
		WebUtility.Click(listingSeasonLink);
		WebUtility.Wait();
		Thread.sleep(3000);
		WebUtility.Click(tvShowBreadcrumb);
		WebUtility.Wait();
		
		WebUtility.Click(tvShowSearch);
		WebUtility.Click(dropDownList.get(2));
		WebUtility.Wait();		
		
		listingDraftStatus.click();
		WebUtility.Wait();
		Thread.sleep(3000);
		
		listingAllStatus.click();
		WebUtility.Wait();
		Thread.sleep(3000);
		
		listingChangedStatus.click();
		WebUtility.Wait();
		Thread.sleep(3000);
		
		listingPublishedStatus.click();
		WebUtility.Wait();
		Thread.sleep(3000);
		
		listingUnpublishedStatus.click();
		WebUtility.Wait();
		Thread.sleep(3000);
		
		listingNeedWorkStatus.click();
		WebUtility.Wait();
		Thread.sleep(3000);
		
		listingScheduledStatus.click();
		WebUtility.Wait();
		Thread.sleep(3000);
		
		listingSubmittedToReviewStatus.click();
		WebUtility.Wait();
		Thread.sleep(3000);
		
		listingArchivedStatus.click();
		WebUtility.Wait();
		Thread.sleep(3000);
		
		listingPublishingQueueStatus.click();
		WebUtility.Wait();
		Thread.sleep(3000);
		
	}
	
	
	
	public void EpisodesListingSearch() throws InterruptedException,Exception {	
/*
		// -------- Edit Flow For Testing Purposes ----------------------------

		WebUtility.Click(dashboard);
		WebUtility.Click(tvShowButton);

		searchString.click();
		searchString.sendKeys(tvShowTitle);
		WebUtility.Wait();
		WebUtility.Wait();
		WebUtility.element_to_be_clickable(editButton.get(0));
		editButton.get(0).click();
		WebUtility.Wait();
		WebUtility.Click(seasons);
		WebUtility.Click(editButton.get(0));
		WebUtility.Wait();

		WebUtility.Click(episodes);
		WebUtility.Wait();
		WebUtility.Click(editButton.get(0));
		WebUtility.Wait();
		
		
		externalID = js.executeScript("return arguments[0].childNodes[1].innerText", episodeExternalID).toString();
*/	
		WebUtility.Click(dashboard);
		WebUtility.Click(tvShowButton);
		
		WebUtility.Click(tvShowSearch);
		WebUtility.Click(dropDownList.get(2));
		WebUtility.Wait();
		
		WebUtility.Click(searchString);
		searchString.sendKeys((tvShowTitle+"- Episode").toLowerCase());
		WebUtility.Wait();
		WebUtility.Wait();
		
		WebUtility.Click(searchString);
		searchString.sendKeys(Keys.CONTROL+"a");
		searchString.sendKeys(Keys.BACK_SPACE);
		searchString.sendKeys(externalID);
		WebUtility.Wait();
		WebUtility.Wait();
		
		try {
			TestUtil.captureScreenshot("Episode Search", "Episode");
		} catch (IOException e) {
			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}

		
	}
	
	public void EpisodeAssertionResults() throws Exception{
		sa.assertAll();
	}
}
