package com.zee5.qa.pages;

import java.io.File;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.zee5.qa.base.TestBase;

public class PipelinePOC extends TestBase {

	    //Initializing the page objects-----------------------------

	    public PipelinePOC() {
	        PageFactory.initElements(driver, this);  
	    }

	    public void POCTest() {
	    	Assert.assertTrue(true);
	    	System.out.println("Test Passed");
	    }
	    
	    public void POCTest2()
	    {
	        File directoryPath = new File("C:/users");
	        //List of all files and directories
	        String contents[] = directoryPath.list();
	        System.out.println("List of files and directories in the specified directory:");
	        for(int i=0; i<contents.length; i++) {
	        System.out.println(contents[i]);
	    }
	    }
}

