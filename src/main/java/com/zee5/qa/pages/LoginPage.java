package com.zee5.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import com.qa.utilities.WebUtility;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.zee5.qa.base.TestBase;

public class LoginPage extends TestBase {

	// Page Factory- OR:-
	// Adfs login flow
	@FindBy(xpath = "//span[text()='Login with ADFS']")
	public WebElement ADFSLogin;

	@FindBy(xpath = "//input[@name='UserName']")
	public WebElement UserName;

	@FindBy(xpath = "//input[@name='Password']")
	public WebElement Password;

	@FindBy(xpath = "//span[@id='submitButton']")
	public WebElement submit;

	@FindBy(xpath = "//span[@id='errorText']")
	public WebElement wrongtext;

	@FindBy(xpath = "//div[@class='logout-link']/button")
	WebElement logoutlink;
	@FindBy(xpath = "//*[@id=\"menu-list-grow\"]/li")
	WebElement logoutbutton;
	// Gmail Login Flow

	@FindBy(xpath = "//span[contains(text(),'Login with Google')]")
	WebElement Gmaillogin;

	@FindBy(xpath = "//input[@type='email']")
	WebElement gusername;

	//@FindBy(xpath = "//div[@id='identifierNext']/div/button/div[2]")
	@FindBy(xpath = "//div[@id='identifierNext']/div/button")
	WebElement next;

	// @FindBy(xpath="//span[@class='RveJvd snByac']")
	WebElement Gotit;

	@FindBy(xpath = "//input[@type='password']")
	WebElement gpassword;

	@FindBy(xpath = "//div[@class='VfPpkd-dgl2Hf-ppHlrf-sM5MNb']/button[1]")
	WebElement nextlogin;

	// Initializing the page objects;
	public LoginPage() {
		PageFactory.initElements(driver, this);
	}

	public void login(String un, String pwd) throws InterruptedException {
		ADFSLogin.click();
		UserName.sendKeys(un);
		Password.sendKeys(pwd);

		submit.click();
		Thread.sleep(5000);

	}

	public void gmailLogin(String usn, String pwsd) throws InterruptedException {
		Gmaillogin.click();
		gusername.sendKeys(usn);
		next.click();
		WebUtility.element_to_be_clickable(gpassword);
		gpassword.sendKeys(pwsd);
		WebDriverWait wait = new WebDriverWait(driver, 10);

		wait.until(ExpectedConditions.elementToBeClickable(nextlogin));
		nextlogin.click();
	}

}
