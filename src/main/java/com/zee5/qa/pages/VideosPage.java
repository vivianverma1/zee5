package com.zee5.qa.pages;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Set;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import java.util.HashMap;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.asserts.SoftAssert;

import com.qa.utilities.WebUtility;
import com.zee5.qa.util.TestUtil;



public class VideosPage extends com.zee5.qa.base.TestBase {

    LoginPage loginPage1 = new LoginPage();


    //-------------------------Video Name --------------------------------
    
    JavascriptExecutor js = (JavascriptExecutor) driver;
    String videoTitle = "Star Trek";
    String videoQuickFilingTitle = "Star Trek Quick Filling";
    String externalID;
	Actions actionProvider = new Actions(driver);
    SoftAssert sa = new SoftAssert();

    
    //---------------------------- Video Properties ----------------------------------------------------------------
    @FindBy(xpath = "//div[contains(@class,'auto-createVideo')]")
    WebElement createVideo;

    @FindBy(xpath = "//button[contains(@class,'auto-leftTab-VideoProperties')]")
    WebElement videoProperties;

    @FindBy(xpath = "//span[contains(text(),'Title Summary')]")
    WebElement titleSummary;

    @FindBy(xpath = "//input[@name='note']")
    WebElement note;

    @FindBy(xpath = "//input[@name=\"title\"]")
    WebElement title;

    @FindBy(xpath = "//div[contains(@id,'auto-shortDescription')]//child::div[contains(@class,'ql-editor')]")
    WebElement shortDesc;

    @FindBy(xpath = "//div[contains(@class,'auto-WebDescription')]//child::div[contains(@class,'ql-editor')]")
    WebElement webDesc;

    @FindBy(xpath = "//div[contains(@class,'auto-AppDescription')]//child::div[contains(@class,'ql-editor')]")
    WebElement appDesc;

    @FindBy(xpath = "//div[contains(@class,'auto-TVDescription')]//child::div[contains(@class,'ql-editor')]")
    WebElement tvDesc;

    @FindBy(xpath = "//input[@id='subtype']")
    WebElement subType;

    @FindBy(id = "subtype-option-0")
    WebElement subTypeOption;

    @FindBy(xpath = "//input[@id='category']")
    WebElement category;

    @FindBy(xpath = "//input[@id='primaryGenre']")
    WebElement primaryGenre;

    @FindBy(xpath = "//div[contains(@class,'auto-PrimaryGenre')]//descendant::span[@class='MuiChip-label']")
    WebElement primaryGenreAssertion;

    @FindBy(xpath = "//input[@id='secondaryGenre']")
    WebElement secondaryGenre;

    @FindBy(xpath = "//div[contains(@class,'auto-SecondaryGenre')]//descendant::span[@class='MuiChip-label']")
    WebElement secondaryGenreAssertion;

    @FindBy(xpath = "//input[@id='thematicGenre']")
    WebElement thematicGenre;

    @FindBy(xpath = "//input[@id='settingGenre']")
    WebElement settingGenre;

    @FindBy(xpath = "//input[@name='isrc']")
    WebElement isrc;

    @FindBy(xpath = "//input[@name='album']")
    WebElement album;

    @FindBy(xpath = "//input[@id='style']")
    WebElement style;

    @FindBy(xpath = "//input[@id='activity']")
    WebElement activity;

    @FindBy(xpath = "//input[@id='dayPart']")
    WebElement dayPart;
    @FindBy(xpath = "//input[@id='rcsCategory']")
    WebElement rcsCategory;

    @FindBy(xpath = "//input[@name='creativeTitle']")
    WebElement creativeTitle;

    @FindBy(xpath = "//input[@name='alternatetitle']")
    WebElement alternateTitle;

    @FindBy(xpath = "//input[@name='pageTitle']")
    WebElement pageTitle;

    @FindBy(xpath = "//textarea[@name='pageDescription']")
    WebElement pageDescription;

    @FindBy(name = "titleForSocialShare")
    WebElement titleForSocialShare;

    @FindBy(name = "descriptionForSocialShare")
    WebElement descriptionForSocialShare;

//    @FindBy(name = "dateZee5Published")
	@FindBy(name="dateZee5Published")
    WebElement zee5ReleaseDate;

	@FindBy(xpath="//button[@class='MuiButtonBase-root MuiIconButton-root']")
    WebElement zeeReleaseDate;

	//@FindBy(xpath="//div[@class='MuiFormControl-root MuiTextField-root zee-input-field auto-OriginalTelecastDate-0-25']//button[@type='button']")
	@FindBy(name="telecastDate")
	WebElement telecastDate;
	
    @FindBy(xpath = "//div[@class='MuiPaper-root MuiPopover-paper MuiPaper-elevation8 MuiPaper-rounded']")
    WebElement datePicker; 
	
    @FindBy(name = "duration")
    WebElement videoDuration;

    @FindBy(xpath = "//input[@name='isMultiAudio']")
    WebElement multiAudio;

    @FindBy(xpath = "//input[@id='audioLanguages']")
    WebElement audioLanguage;

    @FindBy(xpath = "//div[contains(@class,'auto-AudioLanguage')]//descendant::span[@class='MuiChip-label']")
    WebElement audioLanguagesAssertion;

    @FindBy(xpath = "//input[@id='dubbedLanguageTitle']")
    WebElement dubbedLanguageTitle;

    @FindBy(xpath = "//div[contains(@class,'auto-DubbedLanguageTitle')]//descendant::span[@class='MuiChip-label']")
    WebElement dubbedLanguageAssertion;

    @FindBy(xpath = "//input[@id='primaryLanguage']")
    WebElement primaryLanguage;

    @FindBy(xpath = "//input[@id='originalLanguage']")
    WebElement originalLanguage;

    @FindBy(xpath = "//input[@id='subtitleLanguages']")
    WebElement subtitleLanguage;

    @FindBy(xpath = "//input[@id='contentVersion']")
    WebElement contentVersion;

    @FindBy(xpath = "//input[@id='contentLanguage']")
    WebElement contentLanguage;

    @FindBy(xpath = "//input[@id='subtitleLanguages']")
    WebElement subtitleLanguages;

    @FindBy(xpath = "//input[@id='theme']")
    WebElement theme;

    @FindBy(xpath = "//input[@name='contractId']")
    WebElement contractId;

    @FindBy(xpath = "//input[@id='specialCategory']")
    WebElement specialCategory;

    @FindBy(xpath = "//input[@id='specialCategoryCountry']")
    WebElement specialCategoryCountry;

    @FindBy(xpath = "//input[@name='specialCategoryFrom']")
  //  @FindBy(xpath = "//div[@class='MuiFormControl-root MuiTextField-root zee-input-field auto-SpecialCategory-FromTimeline-0-2']//button[@type='button']")
    WebElement specialCategoryFrom;

    @FindBy(xpath = "//input[@name='specialCategoryTo']")
 //   @FindBy(xpath = "//div[@class='MuiFormControl-root MuiTextField-root zee-input-field auto-SpecialCategory-ToTimeline-0-3']//button[@type='button']")
    WebElement specialCategoryTo;
    
	@FindBy(xpath = "//div[contains(@class,'s-badge')]")
	WebElement contentStatus; 

    //-------------------------------------------------Classification -----------------------------------------------------------------------------------------------
    @FindBy(id = "rating")
    WebElement rating;

    @FindBy(id = "contentOwner")
    WebElement contentOwner;

    @FindBy(id = "certification")
    WebElement certification;

    @FindBy(id = "originCountry")
    WebElement originCountry;

    @FindBy(name = "upcomingPage")
    WebElement upcomingPage;

    @FindBy(id = "ageRating")
    WebElement contentDescriptors;

    @FindBy(id = "emotions")
    WebElement emotions;

    @FindBy(id = "contentGrade")
    WebElement contentGrade;

    @FindBy(id = "era")
    WebElement era;

    @FindBy(id = "targetAge")
    WebElement targetAge;

    @FindBy(id = "targetAudiences")
    WebElement targetAudiences;

    @FindBy(xpath = "//input[contains(@id,'auto-tags')]")
    WebElement tags;

    @FindBy(name = "digitalKeywords")
    WebElement digitalKeywords;

    @FindBy(name = "adaptation")
    WebElement adaptation;

    @FindBy(name = "events")
    WebElement events;

    @FindBy(id = "productionCompany")
    WebElement productionCompany;

    @FindBy(name = "popularityScore")
    WebElement popularityScore;

    @FindBy(name = "trivia")
    WebElement trivia;
    
    @FindBy(name = "city")
    WebElement city;
    
    @FindBy(name = "country")
    WebElement country;
    
    @FindBy(name = "highIntensityTags")
    WebElement highIntensityTags;
    
    @FindBy(name = "state")
    WebElement state;
    
    @FindBy(name = "newsTags")
    WebElement newsTags;
    
    @FindBy(name = "officialSite")
    WebElement officialSite;

    @FindBy(xpath = "//input[contains(@id,'auto-awardRecipient')]")
    WebElement awardRecipient;

    @FindBy(id = "awardsCategory")
    WebElement awardsCategory;

    @FindBy(name = "awardsandrecognition")
    WebElement awardHonor;

    //-------------------------------------Player Attributes --------------------------------
    // Play Attributes ---------------------------------------------------

    @FindBy(xpath = "//input[@name='skipAvailable']")
    WebElement skip;

    @FindBy(xpath = "//input[@name='introStartTime']")
    WebElement startTime;

    @FindBy(xpath = "//input[@name='introEndTime']")
    WebElement endTime;

    @FindBy(xpath = "//input[@name='recapStartTime']")
    WebElement recapStart;

    @FindBy(xpath = "//input[@name='recapEndTime']")
    WebElement recapEnd;

    @FindBy(xpath = "//input[@name='endCreditsStartTime']")
    WebElement endCredit;

    @FindBy(xpath = "//input[@name='adMarker']")
    WebElement adMarker;

    @FindBy(xpath = "//input[@name='skipSongStartTime']")
    WebElement skipSong;

    @FindBy(xpath = "//input[@name='skipSongEndTime']")
    WebElement skipEnd;

    @FindBy(xpath = "//div[@class='next-step-btn']")
    WebElement nextControl;

    //-------------------------------------------- Cast and Crew -----------------------------------------------------------
    @FindBy(xpath = "//button[contains(@class,'auto-leftTab-Cast&Crew')]")
    WebElement castCrew;

    @FindBy(xpath = "(//input[contains(@id,'auto-actor')])[1]")
    WebElement actor;

    @FindBy(xpath = "(//input[contains(@id,'auto-actor')])[2]")
    WebElement actor2;

    @FindBy(xpath = "(//input[contains(@id,'auto-actor')])[3]")
    WebElement actor3;

    @FindBy(xpath = "//input[contains(@id,'auto-character')]")
    WebElement character;

    @FindBy(xpath = "(//input[contains(@id,'auto-character')])[2]")
    WebElement character2;

    @FindBy(xpath = "(//input[contains(@id,'auto-character')])[3]")
    WebElement character3;


    @FindBy(xpath = "//div[contains(@class,'auto-Performer')]//descendant::input")
    WebElement performer;

    @FindBy(xpath = "//div[contains(@class,'auto-Host/Anchorman')]//descendant::input")
    WebElement host;

    @FindBy(xpath = "//div[contains(@class,'auto-Singer')]//descendant::input")
    WebElement singer;

    @FindBy(xpath = "//div[contains(@class,'auto-Lyricist')]//descendant::input")
    WebElement lyricst;

    @FindBy(xpath = "//div[contains(@class,'auto-Director')]//descendant::input")
    WebElement director;

    @FindBy(xpath = "//div[contains(@class,'auto-Cinematography/DOP')]//descendant::input")
    WebElement cinematography;

    @FindBy(xpath = "//div[contains(@class,'auto-Producer')]//descendant::input")
    WebElement producer;

    @FindBy(xpath = "//div[contains(@class,'auto-ExecutiveProducer')]//descendant::input")
    WebElement executiveProducer;

    @FindBy(xpath = "//div[contains(@class,'auto-MusicDirector')]//descendant::input")
    WebElement musicDirector;

    @FindBy(xpath = "//div[contains(@class,'auto-Choreographer')]//descendant::input")
    WebElement choreographer;

    @FindBy(xpath = "//div[contains(@class,'auto-TitleThemeMusic')]//descendant::input")
    WebElement titleThemeMusic;

    @FindBy(xpath = "//div[contains(@class,'auto-BackgroundScore')]//descendant::input")
    WebElement backgroundScore;

    @FindBy(xpath = "//div[contains(@class,'auto-StoryWriter')]//descendant::input")
    WebElement storyWriter;

    @FindBy(xpath = "//div[contains(@class,'auto-ScreenPlay')]//descendant::input")
    WebElement screenPlay;

    @FindBy(xpath = "//div[contains(@class,'auto-DialogueWriter')]//descendant::input")
    WebElement dialogueWriter;

    @FindBy(xpath = "//div[contains(@class,'auto-FilmEditing')]//descendant::input")
    WebElement filmEditing;

    @FindBy(xpath = "//div[contains(@class,'auto-Casting')]//descendant::input")
    WebElement casting;

    @FindBy(xpath = "//div[contains(@class,'auto-ProductionDesign')]//descendant::input")
    WebElement productionDesign;

    @FindBy(xpath = "//div[contains(@class,'auto-ArtDirection')]//descendant::input")
    WebElement artDirection;

    @FindBy(xpath = "//div[contains(@class,'auto-SetDecoration')]//descendant::input")
    WebElement setDecoration;

    @FindBy(xpath = "//div[contains(@class,'auto-CostumeDesign')]//descendant::input")
    WebElement costumeDesign;

    @FindBy(xpath = "//div[contains(@class,'auto-ProductionCo')]//descendant::input")
    WebElement productionCo;

    @FindBy(xpath = "//div[contains(@class,'auto-Presenter')]//descendant::input")
    WebElement presenter;

    @FindBy(xpath = "//div[contains(@class,'auto-Guest')]//descendant::input")
    WebElement guest;

    @FindBy(xpath = "//div[contains(@class,'auto-Participant')]//descendant::input")
    WebElement participant;

    @FindBy(xpath = "//div[contains(@class,'auto-Judges')]//descendant::input")
    WebElement judges;

    @FindBy(xpath = "//div[contains(@class,'auto-Narrator')]//descendant::input")
    WebElement narrator;

    @FindBy(xpath = "//div[contains(@class,'auto-Sponsor')]//descendant::input")
    WebElement sponsor;

    @FindBy(xpath = "//div[contains(@class,'auto-Graphics')]//descendant::input")
    WebElement graphics;

    @FindBy(xpath = "//div[contains(@class,'auto-Vocalist')]//descendant::input")
    WebElement vocalist;

    @FindBy(xpath = "//div[contains(@class,'add-btn')]//button")
    WebElement addActor;

    @FindBy(xpath = "//div[contains(@class,'remove-btn')]//button")
    List < WebElement > removeActor;


    //-------------------------------------------Videos -------------------------------------------------------------------

    @FindBy(xpath = "//button[contains(@class,'auto-leftTab-Videos')]")
    WebElement videos;

    @FindBy(name = "audioLanguage")
    WebElement audioTrack;

    @FindBy(xpath = "//input[@name='dashRootFolderName']")
    WebElement dashRootVideo;

    @FindBy(xpath = "//input[@name='dashManifestName']")
    WebElement dashManifestVideo;

    @FindBy(xpath = "//input[@name='hlsRootFolderName']")
    WebElement hlsRootVideo;

    @FindBy(xpath = "//input[@name='hlsManifestName']")
    WebElement hlsManifestVideo;

    @FindBy(name = "subtitleLanguages")
    WebElement subTitle;

    @FindBy(name = "drmKeyId")
    WebElement drmKeyId;

    @FindBy(name = "size")
    WebElement size;

    @FindBy(name = "protected")
    WebElement protectedCheckbox;

    @FindBy(id = "dashSuffixesId")
    WebElement dashSuffix;

    @FindBy(id = "hlsSuffixesId")
    WebElement hlsSuffix;

    @FindBy(xpath = "//input[@name='mediathekFileUid']")
    WebElement mediaThekUID;

    @FindBy(xpath = "//input[@name='spriteUrl']")
    WebElement spriteUrlText;

    @FindBy(xpath = "//input[@name='subtitleManifest']")
    WebElement subTitleManifest;

    @FindBy(xpath = " //a[contains(text(),'Import Details')]")
    WebElement importDetails;    
 
    //--------------------------------------License ------------------------------------------------------	
    @FindBy(xpath = "//button[contains(@class,'auto-leftTab-LicenseModule')]")
    WebElement license;

    @FindBy(xpath = "//div[contains(text(),'Create License')]")
    WebElement createLicense;

    @FindBy(xpath = "//input[@name='setName']")
    WebElement licenseSetName;
 
    @FindBy(xpath = "//input[contains(@name,'fromDate')]")
    WebElement licenseFromDate;
    
    @FindBy(xpath = "//input[contains(@name,'toDate')]")
    WebElement licenseToDate;

    @FindBy(xpath = "//input[contains(@id,'auto-country')]")
    WebElement licenseCountry;

    @FindBy(xpath = "//input[contains(@id,'auto-businessType')]")
    WebElement licenseBusinessType;

    @FindBy(xpath = "//input[contains(@id,'auto-billingType')]")
    WebElement licenseBillingType;

    @FindBy(xpath = "//input[contains(@id,'auto-platform')]")
    WebElement licensePlatform;

    @FindBy(xpath = "//input[contains(@id,'auto-tvodTier')]")
    WebElement licenseTVOD;

    @FindBy(xpath = "//span[contains(text(),'SAVE')]//ancestor::button")
    WebElement saveLicense;

    @FindBy(xpath = "//div[@role='tooltip']")
    WebElement inactivateLicenseDropdown;

    @FindBy(xpath = "//div[@class='ml-10 status-dropdown val']")
    WebElement licenseStatusButton;

    @FindBy(xpath = "//input[@id='auto-reason-0-0']")
    WebElement inactivateLicenseReason;

    @FindBy(xpath = "//input[@name='manual' and @value='2']")
    WebElement useTemplateRadioButton;

    @FindBy(xpath = "//input[@type='text']")
    WebElement selectTemplate;

    @FindBy(xpath = "//div[contains(@class,'default-edit-btn')]")
    WebElement editLicense;

    @FindBy(id = "auto-country-0-0")
    WebElement editLicenseCountry;

    @FindBy(xpath = "//button[contains(@class,'short-btn')]")
    WebElement expiredLicense;

    @FindBy(className = "item")
    WebElement expiredLicenseBack;

    @FindBy(xpath = "//button[contains(@class,'filter-btn')]")
    WebElement licenseFilters;
    
    @FindBy(name = "fromDate")
    WebElement licenseFilterFromDate;

    @FindBy(name = "toDate")
    WebElement licenseFilterToDate;

    @FindBy(id = "auto-businessType-0-2")
    WebElement licenseFilterBusinessType;

    @FindBy(id = "auto-billingType-0-3")
    WebElement licenseFilterBillingType;

    @FindBy(id = "auto-platform-0-4")
    WebElement licenseFilterPlatform;

    @FindBy(xpath = "//input[@name='status' and @value = '1']")
    WebElement licenseFilterActiveStatus;

    @FindBy(xpath = "//input[@name='status' and @value = '0']")
    WebElement licenseFilterInActiveStatus;
    
	@FindBy(xpath = "//div[contains(@class,'delete')]")
	List<WebElement> licenseTemplateDeleteSets;

    @FindBy(xpath = "//input[contains(@id,'contentAgeRatingId')]")
    WebElement licenseContentAgeRatingId;
	
    //--------------------------------------------Images -----------------------------------------------------------------
    @FindBy(xpath = "//button[contains(@class,'auto-leftTab-Images')]")
    WebElement images;

    @FindBy(xpath = "//input[@name='cover']")
    WebElement cover;

    @FindBy(xpath = "//input[@name='appcover']")
    WebElement appcover;

    @FindBy(xpath = "//input[@name='list']")
    WebElement list;

    @FindBy(xpath = "//input[@name='square']")
    WebElement square;

    @FindBy(xpath = "//input[@name='tvcover']")
    WebElement tvCover;

    @FindBy(xpath = "//input[@name='portrait']")
    WebElement portrait;

    @FindBy(xpath = "//input[@name='listclean']")
    WebElement listClean;

    @FindBy(xpath = "//input[@name='portraitclean']")
    WebElement portraitClean;

    @FindBy(xpath = "//input[@name='telcosquare']")
    WebElement telcoSquare;

    @FindBy(xpath = "//input[@name='passport']")
    WebElement passport;

    @FindBy(xpath = "//button[contains(@class,\"auto-create-newSet\")]")
    WebElement imagesCreateNewSet;

    @FindBy(name = "setName")
    WebElement newSetName;

    @FindBy(id = "auto-GroupCountry-0-1")
    WebElement newSetCountry;

    @FindBy(id = "auto-Platform-0-2")
    WebElement newSetPlatform;

    @FindBy(id = "auto-gender-0-0")
    WebElement newSetGender;

    @FindBy(id = "auto-genre-0-1")
    WebElement newSetGenre;

    @FindBy(id = "auto-ageGroup-0-2")
    WebElement newSetAgeGroup;

    @FindBy(id = "auto-language-0-3")
    WebElement newSetLanguage;

    @FindBy(name = "others")
    WebElement newSetOthers;

    @FindBy(xpath = "//div[contains(@class,'imageset-acc')]")
    List<WebElement> imageSets;

    @FindBy(xpath = "//div[@class='status-dropdown val']//div//button[@type='button']")
    WebElement imageSetActiveButton;
    
    @FindBy(xpath = "//span[@class='remove']")
    List<WebElement> removeSet;
    
    @FindBy(xpath = "//body[1]/div[4]/div[3]/div[1]/div[3]/button[1]")  
    WebElement deactivateSetYes;
    //----------------------------------------- SEO Details ---------------------------------------------------------------

    @FindBy(xpath = "//button[contains(@class,'auto-leftTab-SEODetails')]")
    WebElement seoDetails;

    @FindBy(name = "titleTag")
    WebElement seoTitleTag;

    @FindBy(name = "metaDescription")
    WebElement seoMetaDescription;

    @FindBy(name = "metaSynopsis")
    WebElement seoMetaSynopsis;

    @FindBy(id = "redirectionType")
    WebElement seoRedirectionType;

    @FindBy(name = "redirectionLink")
    WebElement seoRedirectionLink;

    @FindBy(name = "noIndexNoFollow")
    WebElement seoNoIndexNoFollow;

    @FindBy(name = "h1Heading")
    WebElement seoH1Heading;

    @FindBy(name = "h2Heading")
    WebElement seoH2Heading;

    @FindBy(name = "h3Heading")
    WebElement seoH3Heading;

    @FindBy(name = "h4Heading")
    WebElement seoH4Heading;

    @FindBy(name = "h5Heading")
    WebElement seoH5Heading;

    @FindBy(name = "h6Heading")
    WebElement seoH6Heading;

    @FindBy(name = "robotsMetaNoIndex")
    List < WebElement > seoRobotsMetaIndex;

    @FindBy(name = "robotsMetaImageIndex")
    WebElement seoRobotsMetaImageIndex;

    @FindBy(name = "robotsMetaImageNoIndex")
    WebElement seoRobotsMetaImageNoIndex;

    @FindBy(name = "breadcrumbTitle")
    WebElement seoBreadcrumbTitle;

    @FindBy(name = "internalLinkBuilding")
    WebElement seoInternalLinkBuilding;

    @FindBy(xpath = "//input[@type=\"checkbox\"]")
    List < WebElement > seoCheckboxes;

    //-------------------------- MAP Content -----------------------------------------------------------

    @FindBy(xpath = "//button[contains(@class,'auto-leftTab-MapContent')]")
    WebElement mapContent;

    @FindBy(xpath = "//div[contains(text(),\"Add Content\")]")
    WebElement addContent;

    @FindBy(xpath = "//input[@type='checkbox']")
    WebElement checkbox;

    @FindBy(xpath = "(//div[contains(text(),'Assign Content')])[1]")   
    WebElement assignContent; 

    @FindBy(xpath = "//div[contains(text(),'Assign Content')]")
    List < WebElement > mapContentAssign;
    
    @FindBy(xpath = "//div[@class='mark-done mark-fill-active']")
    WebElement mapMarkAsDone;

    @FindBy(name = "searchVal")
    List < WebElement > searchMapContent;

    //-----------------------------------------Quick Links ----------------------------------------------------

    @FindBy(xpath = "//li[contains(text(),'Related Content')]")
    WebElement relatedContent;

    @FindBy(name = "searchVal")
    WebElement relatedContentSearch;

    @FindBy(xpath = "//span[contains(text(),'Assign Movies')]")
    WebElement relatedContentAssignMovies;

    @FindBy(xpath = "//div[contains(text(),'Add Movies')]")
    WebElement relatedContentAddMovies;

    @FindBy(xpath = "//span[contains(text(),'Assign TV Shows')]")
    WebElement relatedContentAssignTvShows;

    @FindBy(xpath = "//div[contains(text(),'Add TV Shows')]")
    WebElement relatedContentAddTvShows;

    @FindBy(xpath = "//span[contains(text(),'Assign Videos')]")
    WebElement relatedContentAssignVideos;

    @FindBy(xpath = "//div[contains(text(),'Add Videos')]")
    WebElement relatedContentAddVideos;

    @FindBy(xpath = "//button[contains(.,'Assign Seasons')]")
    WebElement relatedContentAssignSeasons;

    @FindBy(xpath = "//div[contains(text(),'Add Seasons')]")
    WebElement relatedContentAddSeasons;

    @FindBy(xpath = "//button[contains(.,'Assign Episodes')]")
    WebElement relatedContentAssignEpisodes;

    @FindBy(xpath = "//div[contains(text(),'Add Episodes')]")
    WebElement relatedContentAddEpisodes;

    @FindBy(xpath = "//li[contains(text(),'Published History')]")
    WebElement publishedHistory;

    @FindBy(xpath = "//div[contains(text(),'Assign Videos')]")
    WebElement assignRelatedContentVideos;
    
    @FindBy(xpath = "//div[contains(text(),'Assign Movies')]")
    WebElement assignRelatedContentMovies;
    
    @FindBy(xpath = "(//span[contains(@class,'auto-delete')])[1]")
    WebElement deleteRelatedContent;

    //----------------------------------- CheckList---------------------------------------------------

    @FindBy(xpath = "//button[contains(@class,'auto-leftTab-Checklist')]")
    WebElement checklist;

    @FindBy(xpath = "//span[contains(text(),'Submit To Review')]")
    WebElement submitToReview;

    @FindBy(xpath = "//span[contains(text(),'Yes')]")
    WebElement submitYesBtn;

    @FindBy(xpath = "//span[contains(text(),'Ok')]")
    WebElement submitOk;

    @FindBy(xpath = "//label[contains(.,'Reason')]//following::input")
    WebElement needWorkReason;

    @FindBy(xpath = "//span[contains(.,'Need Work')]//ancestor::button")
    WebElement needWorkBtn;

    @FindBy(xpath = "/html/body/div[2]/div[3]/div/div[3]/button[1]")
    WebElement needWorkYes;

    @FindBy(xpath = "/html/body/div[2]/div[3]/div/div[3]/button/span[1]")
    WebElement needWorkOk;

    @FindBy(xpath = "//div[contains(@class,'auto-countryGroup')]//descendant::input")
    WebElement checklistCountry;

    @FindBy(xpath = "//span[contains(text(),'Publish Content')]")
    WebElement publishContent;

    @FindBy(xpath = "//div[contains(@class,'auto-countryGroup')]//descendant::input")
    WebElement publishCountry;

    @FindBy(xpath = "//span[contains(text(),'Republish Content')]")
    WebElement republishContent;

    @FindBy(xpath = "//span[contains(text(),'Unpublish Content')]//ancestor::button")
    WebElement unpublishContent;

    @FindBy(xpath = "//span[@class='edit tooltip-sec hand-cursor']")
    WebElement restoreButton;

    @FindBy(id = "archive-icon_svg__Rectangle_273")
    List < WebElement > archiveButton;

    @FindBy(xpath = "//div[contains(@class,'Schedule-box')]//div[contains(@class,'add-btn create-btn')]")
    WebElement scheduleContent;

    @FindBy(name = "scheduledPublicationTime")
    WebElement scheduledPublicationTime;    
       
    @FindBy(xpath = "(//div[contains(@class,'auto-countryGroup')]//input)[2]")    
    WebElement scheduledContentCountry;

    @FindBy(xpath = "//span[contains(text(),'Schedule Content')]")
    WebElement scheduleContentButton;

    @FindBy(xpath = "(//label[contains(text(),'Country')]//following::input)[3]")
    WebElement unpublishReason;
    
	@FindBy(xpath = "(//div[contains(@class,'val')])[3]")
	WebElement scheduledTime;
	
	@FindBy(xpath = "(//div[contains(@class,'icon-w-text icon-w-14 m-b-10')])[1]")
	WebElement currentTime;	
	
	@FindBy(xpath = "(//button[contains(@class,'MuiPickersToolbarButton-toolbarBtn')])[4]")
	WebElement datePickerMinutes;		
	
	@FindBy(xpath = "//span[contains(@class,'MuiTypography-root MuiPickersClockNumber-clockNumber') and contains(text(),'05')]")
	WebElement datePickerMinutesClock;	
	

    //----------------------------------- Sort And Filter -------------------------------------------------------

    @FindBy(xpath = "//span[contains(text(),'Apply Sort')]")
    WebElement applySortVideo;

    @FindBy(xpath = "//span[contains(text(),'Clear')]")
    WebElement clearFilter;

    @FindBy(xpath = "//span[contains(text(),'Filters')]")
    WebElement filters;

    @FindBy(xpath = "//div[contains(text(),'Draft')]")
    WebElement filtersDraftStatus;

 	@FindBy(xpath="//div[contains(text(),'All')]")
 	WebElement filterAllStatus;
 	
    @FindBy(xpath = "//div[contains(text(),'Changed')]")
    WebElement filtersChangedStatus;

    @FindBy(xpath = "//div[contains(text(),'Published')]")
    WebElement filtersPublishedStatus;

    @FindBy(xpath = "//div[contains(text(),'Unpublished')]")
    WebElement filtersUnpublishedStatus;

    @FindBy(xpath = "//div[contains(text(),'Need Work')]")
    WebElement filtersNeedWorkStatus;

    @FindBy(xpath = "//div[contains(text(),'Scheduled')]")
    WebElement filtersScheduledStatus;

    @FindBy(xpath = "//div[contains(text(),'Submitted to Review')]")
    WebElement filtersSubmittedToReviewStatus;

    @FindBy(xpath = "//div[contains(text(),'Archived')]")
    WebElement filtersArchivedStatus;

    @FindBy(xpath="//input[@value='Start Date']")    
 	WebElement filterStartDate;

    @FindBy(xpath="//input[@value='End Date']")    
 	WebElement filterEndDate;

    @FindBy(xpath = "//input[@id='primaryGenre']")
    WebElement filtersPrimaryGenre;

    @FindBy(xpath = "//input[@id='secondaryGenre']")
    WebElement filtersSecondaryGenre;

    @FindBy(xpath = "//input[@id='thematicGenre']")
    WebElement filtersThematicGenre;

    @FindBy(xpath = "//input[@id='settingGenre']")
    WebElement filtersSettingGenre;

    @FindBy(xpath = "//input[@id='VideoActivity']")
    WebElement filtersActivity;

    @FindBy(xpath = "//input[@id='VideoStyle']")
    WebElement filtersStyle;

    @FindBy(xpath = "//input[contains(@id,'auto-actor')]")
    WebElement filtersActor;

    @FindBy(xpath = "//input[@id='subType']")
    WebElement filtersSubType;

    @FindBy(xpath = "//input[@id='contentCategory']")
    WebElement filtersContentCategory;

    @FindBy(xpath = "//input[@id='rcsCategory']")
    WebElement filtersRcsCategory;

    @FindBy(xpath = "//input[@id='theme']")
    WebElement filtersTheme;

    @FindBy(xpath = "//input[@id='targetAudience']")
    WebElement filtersTargetAudience;

    @FindBy(xpath = "//input[@id='licenseGroupCountries']")
    WebElement filtersLicenseGroup;

    @FindBy(xpath = "//input[@id='ageRating']")
    WebElement filtersAgeRating;

    @FindBy(xpath = "//input[@id='businessType']")
    WebElement filtersBusinessType;

    @FindBy(xpath = "//input[@name='externalId']")
    WebElement filtersExternalID;

    @FindBy(xpath = "//input[@id='contentRating']")
    WebElement filtersContentRating;

    @FindBy(xpath = "//input[@id='contentOwner']")
    WebElement filtersContentOwner;

    @FindBy(xpath = "//input[@id='audioLanguage']")
    WebElement filtersAudioLanguage;

    @FindBy(xpath = "//input[@id='originalLanguage']")
    WebElement filtersOriginalLanguage;

    @FindBy(xpath = "//input[contains(@id,'auto-tags')]")
    WebElement filtersTags;

    @FindBy(xpath = "//input[@id='translationLanguage']")
    WebElement filtersTranslationLanguage;

    @FindBy(xpath = "//input[@id='translationStatus']")
    WebElement filtersTranslationLanguageStatus;

    @FindBy(xpath = "//input[@id='moodEmotion']")
    WebElement filtersMood;

    @FindBy(xpath = "//span[contains(text(),'Apply Filter')]")
    WebElement applyFilter;


    @FindBy(xpath = "//span[contains(text(),'Sort')]")
    WebElement sort;

    @FindBy(xpath = "//span[contains(text(),'Ascending to Descending')]")
    WebElement releaseDateAscToDsc;

    @FindBy(xpath = "//span[contains(text(),'Descending to Ascending')]")
    WebElement releaseDateDscToAsc;

    @FindBy(xpath = "//span[contains(text(),'Newest to Oldest')]")
    WebElement modifiedNewToOld;

    @FindBy(xpath = "//span[contains(text(),'Oldest to Newest')]")
    WebElement modifiedOldToNew;

    @FindBy(xpath = "//span[contains(text(),'Apply Sort')]")
    WebElement applySort;

    //------------------------------------------Tranlations ---------------------------------------------------------------

    @FindBy(className = "auto-quickLinks-Translations")
    WebElement translations;

    @FindBy(name = "upcomingPageText")
    WebElement translationsUpcomingPage;

    @FindBy(name = "socialShareDescription")
    WebElement translationsDescriptionForSocialShare;

    @FindBy(name = "socialShareTitle")
    WebElement translationsTitleForSocialShare;

    @FindBy(xpath = "//input[@name='alternativeTitle']")
    WebElement translationsAlternativeTitle;

    @FindBy(xpath = "//div[contains(@id,'auto-Short Description')]//child::div[contains(@class,'ql-editor')]")
    WebElement translationsShortDesc;

    @FindBy(xpath = "//div[contains(@id,'auto-Web Description')]//child::div[contains(@class,'ql-editor')]")
    WebElement translationsWebDesc;

    @FindBy(xpath = "//div[contains(@id,'auto-App Description')]//child::div[contains(@class,'ql-editor')]")
    WebElement translationsAppDesc;

    @FindBy(xpath = "//div[contains(@id,'auto-TV Description')]//child::div[contains(@class,'ql-editor')]")
    WebElement translationsTVDesc;

    @FindBy(xpath = "//input[contains(@id,'auto-Actor')]")
    WebElement translationsActor;

    @FindBy(xpath = "//input[contains(@id,'auto-Character')]")
    WebElement translationsCharacter;

    //------------------------------------------Quick Filling ---------------------------------------------------------------
    @FindBy(xpath = "//span[contains(text(),'Quick Filing')]")
    WebElement quickFilling;

    @FindBy(xpath = "//li[contains(text(),'Create Quick Filing')]")
    WebElement createQuickFiling;


    //------------------------------------------Listing Page ---------------------------------------
    @FindBy(xpath = "(//div[contains(@class,'back-user-btn')]//span)[1]")
    WebElement listingBackArrow;

    @FindBy(xpath = "(//div[@class = 'license-badge'])[1]")
    WebElement listingExpiringLicense;
 
    @FindBy(xpath = "(//div[contains(@class,'mov-link-btn')])[1]")
    WebElement listingLinkedVideos;
    
    @FindBy(xpath = "(//a)[3]")
    WebElement listingLicenseCountries;
    
    @FindBy(xpath = "//span[contains(text(),'Close')]")
    WebElement listingCloseLicenseCountries;  
    
    @FindBy(xpath = "(//span[@class='pub-history'])[1]")
    WebElement listingPublishedHistory;    
    
    @FindBy(xpath = "//div[@class='side-close-btn']")
    WebElement listingClosePublishedHistory;    

    @FindBy(xpath = "//li[@class='auto-leftTab-All']")
    WebElement listingAllStatus;
    	
    @FindBy(xpath = "//li[@class='auto-leftTab-Draft']")
    WebElement listingDraftStatus;
    
    @FindBy(xpath = "//li[@class='auto-leftTab-Changed']")
    WebElement listingChangedStatus;
    
    @FindBy(xpath = "//li[@class='auto-leftTab-Published']")
    WebElement listingPublishedStatus;
    
    @FindBy(xpath = "//li[@class='auto-leftTab-Unpublished']")
    WebElement listingUnpublishedStatus;
    
    @FindBy(xpath = "//li[@class='auto-leftTab-NeedWork']")
    WebElement listingNeedWorkStatus;
    
    @FindBy(xpath = "//li[@class='auto-leftTab-Scheduled']")
    WebElement listingScheduledStatus;
    
    @FindBy(xpath = "//li[@class='auto-leftTab-SubmittedToReview']")
    WebElement listingSubmittedToReviewStatus;
    
    @FindBy(xpath = "//li[@class='auto-leftTab-Archived']")
    WebElement listingArchivedStatus;
    
    @FindBy(xpath = "//li[@class='auto-leftTab-PublishingQueue']")
    WebElement listingPublishingQueueStatus;
    
    //----------------------------------------- Common Elements -----------------------------------------------------------
    @FindBy(className = "next-step-btn")
    WebElement nextButton;

    @FindBy(css = "button.MuiButtonBase-root.MuiIconButton-root.MuiAutocomplete-popupIndicator.MuiAutocomplete-popupIndicatorOpen > span.MuiIconButton-label > svg.MuiSvgIcon-root")
    WebElement closeDropdown;

    @FindBy(xpath = "//div[contains(@class,'auto-Video')]")
    WebElement videoButton;

    @FindBy(xpath = "//div[contains(text(),'Movie')]")
    WebElement movieButton;

    @FindBy(xpath = "//div[contains(@class,'mov-icon mov-view tooltip-sec auto-view')]")
    List < WebElement > editButton;

    @FindBy(xpath = "//span[contains(text(),'Edit Video')]")
    WebElement editVideo;
    
    @FindBy(xpath = "//span[contains(text(),'Continue Editing with Quick Filing')]")
    WebElement editVideoQuickFiling;

    @FindBy(name = "searchString")
    WebElement searchString;

    @FindBy(xpath = "//span[contains(text(),'Yes') or contains(text(),'Ok')]")
    List < WebElement > yes;

    @FindBy(xpath = "//span[contains(text(),'No')]")
    WebElement no;
    
    @FindBy(xpath = "//span[contains(text(),'Ok')]")
    WebElement ok;

    @FindBy(xpath = "//div[contains(@class,'mark-fill-active')]")
    WebElement doneButtonActive; // rgba(255, 255, 255, 1)

    @FindBy(xpath = "//li[@role='option' or @role='menuitem']")
    List < WebElement > dropDownList;

    @FindBy(xpath = "//div[contains(@class,'mark-active')]")
    WebElement doneButtonClicked; // rgba(52, 194, 143, 1)

    @FindBy(xpath = "//button[contains(@class,'auto-tab-Dashboard')]")
    WebElement dashboard;

    @FindBy(css = "button.MuiButtonBase-root.MuiIconButton-root.MuiAutocomplete-popupIndicator.MuiAutocomplete-popupIndicatorOpen > span.MuiIconButton-label > svg.MuiSvgIcon-root")
    WebElement filterCloseDropdown;

    @FindBy(xpath = ("//input[@type='checkbox']"))
    List < WebElement > checkBox;

    @FindBy(xpath = "//span[contains(text(),'Save')]")
    WebElement save;

    @FindBy(css = "span.MuiIconButton-label > svg.MuiSvgIcon-root.MuiSvgIcon-fontSizeSmall")
    WebElement clearInput;

    @FindBy(xpath = "//a[contains(text(),'Videos')]")
    WebElement videosLink;

	// TODO - Temporary Xpath until the bug is fixed
	@FindBy(xpath = "//a[contains(text(),'Update Video') or contains(text(),'View Video')]")
	WebElement updateVideoBreadcrumb;

    @FindBy(xpath = "//div[@class = 'loc-icon']")
    WebElement unlock;

    @FindBy(xpath = "(//div[contains(@class,'auto-copy')])[1]")
    WebElement clone;
    
	@FindBy(id = "archive-icon_svg__Rectangle_273")
    List < WebElement > Archive;

	@FindBy(xpath="//span[contains(text(),'Yes')]")
	WebElement ArchiveYes;
	
	@FindBy(xpath="//span[@class='edit tooltip-sec hand-cursor']")
	WebElement Restore;

	@FindBy(xpath="//span[contains(text(),'Yes')]")
	WebElement RestoreYes;

    @FindBy(className = "head-external-id")
    WebElement videoExternalID; 
    //-------------------------Logout ------------------------------------------------
    @FindBy(xpath = "//div[contains(@class,'logout-link')]/button")
    WebElement logoutlink;
    @FindBy(xpath = "//*[@id=\"menu-list-grow\"]/li")
    WebElement logoutbutton;
    
    
    
    //--------------------Initializing the page objects-----------------------------
    public VideosPage() {
        PageFactory.initElements(driver, this);
    }

    //----------------------------------------------- Main Content ---------------------------------------------------------

    public void VideoVideoProperties() throws InterruptedException, Exception {

//TODO ----------------Update when Permission are fixed ---------------------
    	/*
        logoutlink.click();
        logoutbutton.click();
        WebUtility.clear_cache();
        driver.get(prop.getProperty("url"));
        loginPage1.gmailLogin(prop.getProperty("Gusername1"), prop.getProperty("Gpassword"));

        WebUtility.Click(dashboard);
        WebUtility.Click(videoButton);
  /* 
       //------EDIT Flow for Testing ----------------------------------------------
        WebUtility.Click(searchString);
		searchString.sendKeys(videoTitle);
		searchString.sendKeys(Keys.ENTER);
				
		WebUtility.Click(editButton.get(0));
		WebUtility.Click(editVideo);
*/
        WebUtility.Click(dashboard);
        WebUtility.Click(videoButton);
        WebUtility.Click(createVideo);
        // -------------------Content Properties section -----------------------------------------
        WebUtility.element_to_be_visible(videoProperties);
        WebUtility.Click(videoProperties);

        WebUtility.Click(note);
        externalID = js.executeScript("return arguments[0].childNodes[1].innerText", videoExternalID).toString();
        note.sendKeys(Keys.CONTROL + "a");
        note.sendKeys(Keys.DELETE);
        note.sendKeys("Automation Test");

        title.sendKeys(Keys.CONTROL + "a");
        title.sendKeys(Keys.DELETE);
        title.sendKeys(videoTitle);

        shortDesc.sendKeys("Star Trek is a 2009 American science fiction action film directed by J. J. Abrams and written by Roberto Orci and Alex Kurtzman.");

        webDesc.sendKeys("Automation Test");

        appDesc.sendKeys("Automation Test");

        tvDesc.sendKeys("Automation Test");

        WebUtility.Click(subType);
        WebUtility.Click(dropDownList.get(0));


        WebUtility.Click(category);
        WebUtility.Click(dropDownList.get(0));


        WebUtility.Click(primaryGenre);
        WebUtility.Click(dropDownList.get(0));

        WebUtility.Click(secondaryGenre);
        WebUtility.Click(dropDownList.get(0));


        WebUtility.Click(thematicGenre);
        WebUtility.Click(dropDownList.get(0));

        WebUtility.Click(settingGenre);
        WebUtility.Click(dropDownList.get(0));

        isrc.sendKeys("Automation ISRC");

        album.sendKeys("Automation Album");

        WebUtility.Click(style);
        WebUtility.Click(dropDownList.get(0));


        WebUtility.Click(activity);
        WebUtility.Click(dropDownList.get(0));


        WebUtility.Click(dayPart);
        WebUtility.Click(dropDownList.get(0));


        WebUtility.Click(rcsCategory);
        WebUtility.Click(dropDownList.get(0));


        WebUtility.Click(creativeTitle);
        creativeTitle.sendKeys(Keys.CONTROL + "a");
        creativeTitle.sendKeys(Keys.BACK_SPACE);
        creativeTitle.sendKeys("Automation Creative Title");

        alternateTitle.sendKeys(Keys.CONTROL + "a");
        alternateTitle.sendKeys(Keys.BACK_SPACE);
        alternateTitle.sendKeys("Automation Alternative Title");

        pageTitle.sendKeys(Keys.CONTROL + "a");
        pageTitle.sendKeys(Keys.BACK_SPACE);
        pageTitle.sendKeys("Automation Page Title");

        pageDescription.sendKeys(Keys.CONTROL + "a");
        pageDescription.sendKeys(Keys.BACK_SPACE);
        pageDescription.sendKeys("Automation Page Description");

        titleForSocialShare.sendKeys(Keys.CONTROL + "a");
        titleForSocialShare.sendKeys(Keys.BACK_SPACE);
        titleForSocialShare.sendKeys("Automation Title for Social Share");

        descriptionForSocialShare.sendKeys(Keys.CONTROL + "a");
        descriptionForSocialShare.sendKeys(Keys.BACK_SPACE);
        descriptionForSocialShare.sendKeys("Automation Description for Social Share");

        WebUtility.Click(zee5ReleaseDate);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ESCAPE);
		
        WebUtility.Click(telecastDate);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        Thread.sleep(2000);
        datePicker.sendKeys(Keys.ESCAPE); 

        WebUtility.Click(videoDuration);
        List < WebElement > videoduration = driver.findElements(By.xpath("//li[@role='button']"));
        for (WebElement e: videoduration) {
            String videohrs = e.getText();
            if (videohrs.contains("06")) {
                e.click();
                break;
            }
        }

        multiAudio.click();

        WebUtility.Click(audioLanguage);
        WebUtility.Click(dropDownList.get(0));

        WebUtility.Click(contentLanguage);
        WebUtility.Click(dropDownList.get(0));

        WebUtility.Click(primaryLanguage);
        WebUtility.Click(dropDownList.get(0));

        WebUtility.Click(dubbedLanguageTitle);
        WebUtility.Click(dropDownList.get(0));

        WebUtility.Click(originalLanguage);
        WebUtility.Click(dropDownList.get(0));

        WebUtility.Click(subtitleLanguage);
        WebUtility.Click(dropDownList.get(0));

        WebUtility.Click(contentVersion);
        WebUtility.Click(dropDownList.get(0));

        WebUtility.Click(theme);
        WebUtility.Click(dropDownList.get(0));

        contractId.sendKeys("Automation Contract ID");

        WebUtility.Click(specialCategory);
        WebUtility.Click(dropDownList.get(0));

        WebUtility.Click(specialCategoryCountry);
        WebUtility.Click(dropDownList.get(0));

        WebUtility.Click(specialCategoryFrom);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ESCAPE);

        WebUtility.Click(specialCategoryTo);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ESCAPE);

        WebUtility.Click(nextButton);

        WebUtility.Click(rating);
        WebUtility.Click(dropDownList.get(0));


        WebUtility.Click(contentOwner);
        WebUtility.Click(dropDownList.get(0));


        WebUtility.Click(originCountry);
        WebUtility.Click(dropDownList.get(0));

        WebUtility.Click(upcomingPage);
        upcomingPage.sendKeys(Keys.CONTROL + "a");
        upcomingPage.sendKeys(Keys.BACK_SPACE);
        upcomingPage.sendKeys("Automation Upcoming Page Text");

        WebUtility.Click(contentDescriptors);
        WebUtility.Click(dropDownList.get(0));


        WebUtility.Click(certification);
        WebUtility.Click(dropDownList.get(0));

        WebUtility.Click(emotions);
        WebUtility.Click(dropDownList.get(0));

        WebUtility.Click(contentGrade);
        WebUtility.Click(dropDownList.get(0));

        WebUtility.Click(era);
        WebUtility.Click(dropDownList.get(0));

        WebUtility.Click(targetAge);
        WebUtility.Click(dropDownList.get(0));

        WebUtility.Click(targetAudiences);
        WebUtility.Click(dropDownList.get(0));      
        targetAudiences.click();
   
		tags.sendKeys("Automation Tags");
		WebUtility.Click(dropDownList.get(0));
        
        WebUtility.Click(digitalKeywords);
        digitalKeywords.sendKeys(Keys.CONTROL + "a");
        digitalKeywords.sendKeys(Keys.BACK_SPACE);
        digitalKeywords.sendKeys("Automation Digital Keywords");

        WebUtility.Click(adaptation);
        adaptation.sendKeys(Keys.CONTROL + "a");
        adaptation.sendKeys(Keys.BACK_SPACE);
        adaptation.sendKeys("Automation Adaption");
        adaptation.sendKeys(Keys.ENTER);

        WebUtility.Click(events);
        events.sendKeys(Keys.CONTROL + "a");
        events.sendKeys(Keys.BACK_SPACE);
        events.sendKeys("Automation Event");

        WebUtility.Click(productionCompany);
        WebUtility.Click(dropDownList.get(0));

        WebUtility.Click(popularityScore);
        popularityScore.clear();
        popularityScore.sendKeys("50");

        WebUtility.Click(trivia);
        trivia.clear();
        trivia.sendKeys("Automation Trivia");
   
        city.sendKeys(Keys.CONTROL + "a");
        city.sendKeys(Keys.BACK_SPACE);
        city.sendKeys("Automation City");
        
        country.sendKeys(Keys.CONTROL + "a");
        country.sendKeys(Keys.BACK_SPACE);
        country.sendKeys("Automation Country");           
        
        highIntensityTags.sendKeys(Keys.CONTROL + "a");
        highIntensityTags.sendKeys(Keys.BACK_SPACE);
        highIntensityTags.sendKeys("Automation High Intensity Tags");
        
        state.sendKeys(Keys.CONTROL + "a");
        state.sendKeys(Keys.BACK_SPACE);
        state.sendKeys("Automation State");
        
        newsTags.sendKeys(Keys.CONTROL + "a");
        newsTags.sendKeys(Keys.BACK_SPACE);
        newsTags.sendKeys("Automation News Tags");

        WebUtility.Click(officialSite);
        officialSite.clear();
        officialSite.sendKeys("https://kelltontech.com");

        WebUtility.Click(awardRecipient);
        awardRecipient.sendKeys("Test");
        WebUtility.Click(dropDownList.get(0));

        WebUtility.Click(awardsCategory);
        WebUtility.Click(dropDownList.get(0));

        WebUtility.Click(awardHonor);
        awardHonor.sendKeys(Keys.CONTROL + "a");
        awardHonor.sendKeys(Keys.BACK_SPACE);
        awardHonor.sendKeys("Automation Award");

        nextButton.click();

        if (!skip.isSelected())
            skip.click();

        WebUtility.Click(startTime);


        List < WebElement > starttime = driver.findElements(By.xpath("//li[@role='button']"));
        for (WebElement e9: starttime) {
            String starthrs = e9.getText();
            if (starthrs.contains("02")) {
                e9.click();
                break;
            }
        }

        WebUtility.Click(endTime);
        List < WebElement > endtime = driver.findElements(By.xpath("//li[@role='button']"));

        for (WebElement e9: endtime) {

            String endhrs = e9.getText();
            if (endhrs.contains("03")) {
                e9.click();
                break;
            }
        }

        WebUtility.Click(recapStart);
        List < WebElement > recaptime = driver.findElements(By.xpath("//li[@role='button']"));
        for (WebElement e9: recaptime) {
            System.out.println(e9.getText());
            String recaphrs = e9.getText();
            if (recaphrs.contains("00")) {
                e9.click();
                break;
            }
        }

        WebUtility.Click(recapEnd);
        List < WebElement > recapend = driver.findElements(By.xpath("//li[@role='button']"));
        for (WebElement e9: recapend) {

            String endhrs = e9.getText();
            if (endhrs.contains("01")) {
                e9.click();
                break;
            }
        }

        WebUtility.Click(endCredit);
        List < WebElement > endcredit = driver.findElements(By.xpath("//li[@role='button']"));
        for (WebElement e9: endcredit) {
            //System.out.println(e9.getText());
            String endhrs = e9.getText();
            if (endhrs.contains("02")) {
                e9.click();
                break;
            }
        }

        adMarker.sendKeys("AdMarkers keywords");

        WebUtility.Click(skipSong);
        List < WebElement > skipsong = driver.findElements(By.xpath("//li[@role='button']"));
        for (WebElement e9: skipsong) {
            String skip = e9.getText();
            if (skip.contains("05")) {
                e9.click();
                break;
            }
        }

        WebUtility.Click(skipEnd);
        List < WebElement > skipend = driver.findElements(By.xpath("//li[@role='button']"));
        for (WebElement e9: skipend) {
            String skipendhrs = e9.getText();
            if (skipendhrs.contains("06")) {
                e9.click();
                break;
            }
        }

        nextButton.click();
        js.executeScript("window.scrollTo(0,0);");
        String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
       // Assert.assertEquals(doneButtonActiveColor, "rgba(255, 255, 255, 1)", "Video Properties Mark As Done Assertion Fails");
        sa.assertTrue(doneButtonActiveColor.contains("255, 255, 255") || doneButtonActiveColor.contains("255,255,255"), "Video Properties Mark As Done Assertion Fails");
        WebUtility.Click(doneButtonActive);
    }

    public void VideoPropertiesAssertions() throws InterruptedException {
        /*
    	dashboard.click();
    	Thread.sleep(2000);
       // titleSummary.click();
       
		videoButton.click();
		Thread.sleep(2000);
		searchString.click();
		searchString.sendKeys("Star Trek (2020)");
		Thread.sleep(2000);
		WebUtility.Click(editButton.get(0));
		editButton.get(0).click();
		Thread.sleep(2000);
		editVideo.click();
		Thread.sleep(2000);
	*/
        JavascriptExecutor js = (JavascriptExecutor) driver;
        Thread.sleep(2000);
        titleSummary.click();
        Thread.sleep(2000);
        String expectedTitle = videoTitle;
        Assert.assertEquals(title.getAttribute("value"), expectedTitle, "Title Assertion Failed");

        String expectedShortDescription = "Star Trek is a 2009 American science fiction action film directed by J. J. Abrams and written by Roberto Orci and Alex Kurtzman.";
        List < WebElement > shortDescValue = driver.findElements(By.className("ql-editor"));
        Assert.assertEquals(shortDescValue.get(0).getAttribute("innerText"), expectedShortDescription, "Short Desc Assertion Fails");

        js.executeScript("window.scrollTo(0,document.body.scrollHeight);");

        Object subTypeDropDown = js.executeScript("var dropDown=document.getElementById(\"subtype\");if(dropDown.value==\"\")var dropDownValue=false;else var dropDownValue=true; return dropDownValue");
        Assert.assertEquals(subTypeDropDown, true, "Sub Type Assertion Fails");

        Object contentCategoryDropDown = js.executeScript("var dropDown=document.getElementById(\"category\");if(dropDown.value==\"\")var dropDownValue=false;else var dropDownValue=true; return dropDownValue");
        Assert.assertEquals(contentCategoryDropDown, true, "Content Category Assertion Fails");

        Assert.assertNotNull(primaryGenreAssertion.getText(), "Primary Genre Dropdown is Empty");

        Assert.assertNotNull(secondaryGenreAssertion.getText(), "Secondary Genre Dropdown is Empty");

        Object zee5ReleaseDateValueExists = js.executeScript("var dropDown=document.getElementsByName(\"dateZee5Published\");if(dropDown[0].defaultValue==\"\")var dropDownValue=false;else var dropDownValue=true; return dropDownValue");
        Assert.assertEquals(zee5ReleaseDateValueExists, true, "Zee5 Release Date Assertion Failed");


        Object videoDurationValueExists = js.executeScript("var dropDown=document.getElementsByName(\"duration\");if(dropDown[0].defaultValue==\"\")var dropDownValue=false;else var dropDownValue=true; return dropDownValue");
        Assert.assertEquals(videoDurationValueExists, true, "Video Duration Assertion Failed");

        Assert.assertNotNull(audioLanguagesAssertion.getText(), "Audio Language Dropdown is Empty");

        Thread.sleep(2000);
        Object primaryLanguageDropDown = js.executeScript("var dropDown=document.getElementById(\"primaryLanguage\");if(dropDown.value==\"\")var dropDownValue=false;else var dropDownValue=true; return dropDownValue");
        Assert.assertEquals(primaryLanguageDropDown, true, "Primary Language Assertion Fails");

        Assert.assertNotNull(dubbedLanguageAssertion.getText(), "Dubbed Language Dropdown is Empty");

        Object originalLanguageDropDown = js.executeScript("var dropDown=document.getElementById(\"originalLanguage\");if(dropDown.value==\"\")var dropDownValue=false;else var dropDownValue=true; return dropDownValue");
        Assert.assertEquals(originalLanguageDropDown, true, "Original Language Assertion Fails");

        Object contentVersionDropDown = js.executeScript("var dropDown=document.getElementById(\"contentVersion\");if(dropDown.value==\"\")var dropDownValue=false;else var dropDownValue=true; return dropDownValue");
        Assert.assertEquals(contentVersionDropDown, true, "Content Version Assertion Fails");

        nextButton.click();

        js.executeScript("window.scrollTo(0,0);");
        Thread.sleep(2000);
        //contentOwner.click();
        Object contentOwnerDropDown = js.executeScript("var dropDown=document.getElementById(\"contentOwner\");if(dropDown.value==\"\")var dropDownValue=false;else var dropDownValue=true; return dropDownValue");
        Assert.assertEquals(contentOwnerDropDown, true, "Content Owner Assertion Fails");

        Object certificationDropDown = js.executeScript("var dropDown=document.getElementById(\"certification\");if(dropDown.value==\"\")var dropDownValue=false;else var dropDownValue=true; return dropDownValue");
        Assert.assertEquals(certificationDropDown, true, "Certification Assertion Fails");

        String doneButtonClickedColor = doneButtonClicked.getCssValue("background-color");
       // Assert.assertEquals(doneButtonClickedColor, "rgba(52, 194, 143, 1)", "Video Properties Mark As Done Assertion Fails");
        Assert.assertTrue(doneButtonClickedColor.contains("52, 194, 143"), "Video Properties Mark As Done Assertion Fails");

    }

    public void VideoCastCrew() throws InterruptedException {

    	WebUtility.Click(castCrew);
        try {
            WebUtility.Click(actor);
            actor.sendKeys("Salman Khan");
            WebUtility.Click(dropDownList.get(0));

            WebUtility.Click(character);
            Thread.sleep(2000);
            character.sendKeys("Test Character");

            WebUtility.Wait();
            WebUtility.Click(addActor);
            WebUtility.Click(actor2);
            actor2.sendKeys("Salman Khan 2");
            WebUtility.element_to_be_visible(dropDownList.get(0));
            actor2.sendKeys(Keys.ARROW_DOWN);
            actor2.sendKeys(Keys.ENTER);
            WebUtility.Click(character2);
            character2.sendKeys("Test Character");

            WebUtility.Wait();
            WebUtility.Click(addActor);
            WebUtility.Click(actor3);
            actor3.sendKeys("Salman Khan 3");
            WebUtility.element_to_be_visible(dropDownList.get(0));
            actor3.sendKeys(Keys.ARROW_DOWN);
            actor3.sendKeys(Keys.ENTER);
            WebUtility.Click(character3);
            character3.sendKeys("Test Character");

            WebUtility.Wait();
            WebUtility.Click(removeActor.get(1));
            WebUtility.Wait();


            performer.sendKeys("Test Performer");
            WebUtility.Wait();
            WebUtility.Click(dropDownList.get(0));


            host.sendKeys("Test Host");
            WebUtility.Wait();
            WebUtility.Click(dropDownList.get(0));

            singer.sendKeys("Automation Test Data Singer");
            WebUtility.Wait();
            WebUtility.Click(dropDownList.get(0));

            lyricst.sendKeys("Automation Test Data Lyricst");
            WebUtility.Wait();
            WebUtility.Click(dropDownList.get(0));

            director.sendKeys("Automation Test Data Director");
            WebUtility.Wait();
            WebUtility.Click(dropDownList.get(0));

            cinematography.sendKeys("Automation Test Data Cinematography");
            WebUtility.Wait();
            WebUtility.Click(dropDownList.get(0));

            producer.sendKeys("Automation Test Data Producer");
            WebUtility.Wait();
            WebUtility.Click(dropDownList.get(0));

            executiveProducer.sendKeys("Automation Test Data Ex Producer");
            WebUtility.Wait();
            WebUtility.Click(dropDownList.get(0));


            musicDirector.sendKeys("Automation Test Data Music Director");
            WebUtility.Wait();
            WebUtility.Click(dropDownList.get(0));


            choreographer.sendKeys("Automation Test Data Choreographer");
            WebUtility.Wait();
            WebUtility.Click(dropDownList.get(0));


            titleThemeMusic.sendKeys("Automation Test Data Theme Music");
            WebUtility.Wait();
            WebUtility.Click(dropDownList.get(0));

            backgroundScore.sendKeys("Automation Test Data Background Score");
            WebUtility.Wait();
            WebUtility.Click(dropDownList.get(0));


            storyWriter.sendKeys("Automation Test Data Story Writer");
            WebUtility.Wait();
            WebUtility.Click(dropDownList.get(0));


            screenPlay.sendKeys("Automation Test Data Screen Play");
            WebUtility.Wait();
            WebUtility.Click(dropDownList.get(0));


            dialogueWriter.sendKeys("Automation Test Data Dialogue Writer");
            WebUtility.Wait();
            WebUtility.Click(dropDownList.get(0));


            filmEditing.sendKeys("Automation Test Data Film Editing");
            WebUtility.Wait();
            WebUtility.Click(dropDownList.get(0));


            casting.sendKeys("Automation Test Data Casting");
            WebUtility.Wait();
            WebUtility.Click(dropDownList.get(0));

            productionDesign.sendKeys("Automation Test Data Production Design");
            WebUtility.Wait();
            WebUtility.Click(dropDownList.get(0));


            artDirection.sendKeys("Automation Test Data Art Direction");
            WebUtility.Wait();
            WebUtility.Click(dropDownList.get(0));


            setDecoration.sendKeys("Automation Test Data Set Decoration");
            WebUtility.Wait();
            WebUtility.Click(dropDownList.get(0));


            costumeDesign.sendKeys("Automation Test Data Costume Design");
            WebUtility.Wait();
            WebUtility.Click(dropDownList.get(0));


            productionCo.sendKeys("Automation Test Data Production Co");
            WebUtility.Wait();
            WebUtility.Click(dropDownList.get(0));


            presenter.sendKeys("Automation Test Data Presenter");
            WebUtility.Wait();
            WebUtility.Click(dropDownList.get(0));

            guest.sendKeys("Automation Test Data Guest");
            WebUtility.Wait();
            WebUtility.Click(dropDownList.get(0));


            participant.sendKeys("Automation Test Data Participant");
            WebUtility.Wait();
            WebUtility.Click(dropDownList.get(0));

            judges.sendKeys("Automation Test Data Judges");
            WebUtility.Wait();
            WebUtility.Click(dropDownList.get(0));


            narrator.sendKeys("Automation Test Data Narrator");
            WebUtility.Wait();
            WebUtility.Click(dropDownList.get(0));

            sponsor.sendKeys("Automation Test Data Sponsor");

            graphics.sendKeys("Automation Test Data Graphics");
            WebUtility.Wait();
            WebUtility.Click(dropDownList.get(0));

            vocalist.sendKeys("Automation Test Data Vocalist");
            WebUtility.Wait();
            WebUtility.Click(dropDownList.get(0));

            String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
            sa.assertTrue(doneButtonActiveColor.contains("255, 255, 255") || doneButtonActiveColor.contains("255,255,255"), "Video Cast & Crew Mark As Done Assertion Fails");
            WebUtility.Click(doneButtonActive);

            try {
                TestUtil.captureScreenshot("Main Content Cast and Crew Section","Video");
            } catch (IOException e) {
                System.out.println("Screenshot Capture Failed ->" + e.getMessage());
            }

            js.executeScript("window.scrollTo(0,document.body.scrollHeight);");

            try {
                TestUtil.captureScreenshot("Main Content Cast and Crew Section 2","Video");
            } catch (IOException e) {
                System.out.println("Screenshot Capture Failed ->" + e.getMessage());
            }
        } catch (Exception e) {
            WebUtility.Click(doneButtonActive);
        }
    }

    public void VideoCastCrewAssertions() throws InterruptedException {
        castCrew.click();
        Thread.sleep(2000);
        String expectedActor = "Salman Khan";
        Assert.assertEquals(actor.getAttribute("value"), expectedActor, "Cast & Crew Actor Assertion Failed");

        String expectedCharacter = "Test Character";
        Assert.assertEquals(character.getAttribute("value"), expectedCharacter, "Cast & Crew Character Assertion Failed");

        String doneButtonClickedColor = doneButtonClicked.getCssValue("background-color");
        //Assert.assertEquals(doneButtonClickedColor, "rgba(52, 194, 143, 1)", "Video Cast & Crew Mark As Done Assertion Fails");
        Assert.assertTrue(doneButtonClickedColor.contains("52, 194, 143"), "Video Cast & Crew Mark As Done Assertion Fails");
    }

    public void VideoVideoSection() throws InterruptedException {

        WebUtility.Click(videos);

        mediaThekUID.sendKeys(Keys.CONTROL + "a");
        mediaThekUID.sendKeys(Keys.BACK_SPACE);
        mediaThekUID.sendKeys("Kumkum_Bhagya_2_Episode_04_May_2021_bn");
        
        importDetails.click();
      //  WebUtility.Click(yes.get(0));
      //  WebUtility.Click(ok);
        WebUtility.Wait();

        WebUtility.Click(audioTrack);
        audioTrack.sendKeys(Keys.CONTROL + "a");
        audioTrack.sendKeys(Keys.BACK_SPACE);
        audioTrack.sendKeys("Episode Test Data");

        WebUtility.Click(subTitle);
        subTitle.sendKeys(Keys.CONTROL + "a");
        subTitle.sendKeys(Keys.BACK_SPACE);
        subTitle.sendKeys("Episode Test Data");
        
        WebUtility.Click(size);
        size.sendKeys(Keys.CONTROL + "a");
        size.sendKeys(Keys.BACK_SPACE);
        size.sendKeys("Episode Test Data");
        WebUtility.Wait();
        
        WebUtility.Click(hlsSuffix);
        WebUtility.Click(dropDownList.get(0));
        /*       
        spriteUrlText.sendKeys(Keys.CONTROL + "a");
        spriteUrlText.sendKeys(Keys.BACK_SPACE);
        spriteUrlText.sendKeys("Automation Test Data");
       
        subTitleManifest.click();
*/
        String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
        sa.assertTrue(doneButtonActiveColor.contains("255,255,255") || doneButtonActiveColor.contains("255, 255, 255"), "Main Content - Video Videos Section Mark As Done Assertion Fails");
        WebUtility.Click(doneButtonActive);
        WebUtility.Wait();

        try {
            TestUtil.captureScreenshot("Main Content - Video Section","Video");
        } catch (IOException e) {
            System.out.println("Screenshot Capture Failed ->" + e.getMessage());
        }

        WebUtility.Click(videoProperties);
        WebUtility.Wait();
        
        title.sendKeys(Keys.CONTROL + "a");
        title.sendKeys(Keys.DELETE);
        title.sendKeys(videoTitle);
        
        WebUtility.Click(note);
        WebUtility.Click(doneButtonActive);
        WebUtility.Wait();
    }

    public void VideoVideoSectionAssertions() throws InterruptedException {
        videos.click();
        Thread.sleep(2000);
/*
        String expectedDashRootFolder = "New Video Dash root";
        Assert.assertEquals(dashRootVideo.getAttribute("value"), expectedDashRootFolder, "Dash Root Folder Assertion Failed");

        String expectedDashManifest = "New Video Dash Manifest";
        Assert.assertEquals(dashManifestVideo.getAttribute("value"), expectedDashManifest, "Dash Manifest Assertion Failed");

        String expectedHlsRootFolder = "Automation Test Data";
        Assert.assertEquals(hlsRootVideo.getAttribute("value"), expectedHlsRootFolder, "HLS Root Folder Assertion Failed");

        String expectedHlsManifest = "Automation Test Data";
        Assert.assertEquals(hlsManifestVideo.getAttribute("value"), expectedHlsManifest, "HLS Manifest Assertion Failed");
        */

        String doneButtonClickedColor = doneButtonClicked.getCssValue("background-color");
        Assert.assertTrue(doneButtonClickedColor.contains("52, 194, 143"), "Main Content - Video Videos Section Mark As Done Assertion Fails");
    }

    public void VideoLicense() throws InterruptedException {
    	/*
    	//------EDIT Flow for Testing ----------------------------------------------
        WebUtility.Click(dashboard);
        WebUtility.Click(videoButton);
    	WebUtility.Click(searchString);
		searchString.sendKeys(videoTitle);
		WebUtility.Wait();
		WebUtility.Wait();
		WebUtility.Click(editButton.get(0));
		WebUtility.Click(editVideo);
	*/
        WebUtility.Click(license);
    	    try
        {
	        WebUtility.Click(createLicense);
	
	        WebUtility.Wait();
	        useTemplateRadioButton.click();
	
	        WebUtility.Click(selectTemplate);
	        WebUtility.Click(dropDownList.get(0));
	        
			try {
				int licenseTemplateSize = licenseTemplateDeleteSets.size();
				for(int i = 1;i< licenseTemplateSize ;i++)
				{
					System.out.println("License Sets Size--->"+licenseTemplateDeleteSets.size());
					licenseTemplateDeleteSets.get(1).click();
					Thread.sleep(1000);
					System.out.println("License Sets Size After Delete--->"+licenseTemplateDeleteSets.size());
				}
			}
			catch (Exception e){
				System.out.println("No Sets in License Template");
			}
	
			js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
			WebUtility.Wait();
	        WebUtility.Click(editLicense);
	        WebUtility.Click(editLicenseCountry);
	        WebUtility.Click(clearInput);
	        for (int x = 0; x < dropDownList.size(); x++) {
	        	if(x<4) {
	            WebUtility.Click(dropDownList.get(x));
	        	}
	        	else {
	        		break;
	        	}
	        }
	        WebUtility.Wait();
	        WebUtility.Click(editLicenseCountry);
	        WebUtility.Wait();
	        WebUtility.javascript(saveLicense);
	
	        WebUtility.Click(licenseSetName);
	        licenseSetName.sendKeys("Automation Test Data");
	        
	        WebUtility.Click(licenseContentAgeRatingId);	
	        WebUtility.Wait();
	        licenseContentAgeRatingId.sendKeys(Keys.ARROW_DOWN);
	        licenseContentAgeRatingId.sendKeys(Keys.ENTER);
	        
	        WebUtility.Click(licenseFromDate);
	        datePicker.sendKeys(Keys.ARROW_RIGHT);
	        datePicker.sendKeys(Keys.ESCAPE);
	
	        WebUtility.Click(licenseToDate);
	        datePicker.sendKeys(Keys.ARROW_RIGHT);
	        datePicker.sendKeys(Keys.ARROW_RIGHT);
	        datePicker.sendKeys(Keys.ESCAPE);
	
	        WebUtility.Wait();
	        WebUtility.Click(saveLicense);
        }
        catch(Exception e)
        {
        	System.out.println("Creating a license through template Failed!!");
        	e.printStackTrace();
        }
        
        try
        {
        	WebUtility.Click(license);
	        WebUtility.Click(createLicense);
	
	        WebUtility.Click(licenseSetName);
	        licenseSetName.sendKeys("Automation Sample Data");
	        
	        WebUtility.Click(licenseContentAgeRatingId);	
	        WebUtility.Wait();
	        licenseContentAgeRatingId.sendKeys(Keys.ARROW_DOWN);
	        licenseContentAgeRatingId.sendKeys(Keys.ENTER);
	        
	        WebUtility.Click(licenseFromDate);
	        datePicker.sendKeys(Keys.ARROW_RIGHT);
	        datePicker.sendKeys(Keys.ARROW_RIGHT);
	        datePicker.sendKeys(Keys.ARROW_RIGHT);
	        datePicker.sendKeys(Keys.ESCAPE);
	
	        WebUtility.Click(licenseToDate);
	        datePicker.sendKeys(Keys.ARROW_RIGHT);
	        datePicker.sendKeys(Keys.ARROW_RIGHT);
	        datePicker.sendKeys(Keys.ARROW_RIGHT);
	        datePicker.sendKeys(Keys.ARROW_RIGHT);
	        datePicker.sendKeys(Keys.ESCAPE);
	        
	        WebUtility.Click(licenseCountry);
	        for (int x = 0; x < dropDownList.size(); x++) {
	        	if(x<4) {
	            WebUtility.Click(dropDownList.get(x));
	        	}
	        	else {
	        		break;
	        	}
	        }
	
	        WebUtility.Click(licenseBusinessType);
	        WebUtility.Click(dropDownList.get(0));
	
	        WebUtility.Click(licensePlatform);
	        WebUtility.Click(dropDownList.get(0));
	
	        WebUtility.Click(licenseTVOD);
	        WebUtility.Click(dropDownList.get(0));
	
	        WebUtility.Wait();
	        WebUtility.javascript(saveLicense);
        }
        catch(Exception e)
        {
        	System.out.println("Creating a Manual License Failed");
        	e.printStackTrace();
        }

        WebUtility.Wait();
        String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
        sa.assertTrue(doneButtonActiveColor.contains("255,255,255") || doneButtonActiveColor.contains("255, 255, 255"), "Main Content - Video License Section Mark As Done Assertion Fails");
        WebUtility.Click(doneButtonActive);

        try {
            TestUtil.captureScreenshot("Main Content License Section","Video");
        } catch (IOException e) {
            System.out.println("Screenshot Capture Failed ->" + e.getMessage());
        }

        WebUtility.Click(expiredLicense);
        WebUtility.Wait();
        WebUtility.Click(expiredLicenseBack);
        WebUtility.Wait();

        licenseFilters.click();
        WebUtility.Click(licenseFromDate);
        datePicker.sendKeys(Keys.ARROW_LEFT);
        datePicker.sendKeys(Keys.ARROW_LEFT);
        datePicker.sendKeys(Keys.ESCAPE);

        WebUtility.Click(licenseFilterToDate);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ESCAPE);

        WebUtility.Click(applyFilter);

        Thread.sleep(2000);
        licenseFilters.click();
        clearFilter.click();

        WebUtility.Click(licenseFilters);
        Thread.sleep(1000);
        licenseFilterBusinessType.click();
        WebUtility.Click(dropDownList.get(0));
        WebUtility.Click(applyFilter);

        Thread.sleep(2000);
        licenseFilters.click();
        clearFilter.click();

        WebUtility.Click(licenseFilters);
        Thread.sleep(1000);
        licenseFilterBillingType.click();
        WebUtility.Click(dropDownList.get(0));
        WebUtility.Click(applyFilter);

        Thread.sleep(3000);
        licenseFilters.click();
        clearFilter.click();

        WebUtility.Click(licenseFilters);
        Thread.sleep(1000);
        licenseFilterPlatform.click();
        WebUtility.Click(dropDownList.get(0));
        Thread.sleep(1000);
        filterCloseDropdown.click();
        WebUtility.Click(applyFilter);

        Thread.sleep(2000);
        licenseFilters.click();
        clearFilter.click();

        WebUtility.Click(licenseFilters);
        Thread.sleep(1000);
        licenseContentAgeRatingId.click();
        WebUtility.Click(dropDownList.get(0));
        WebUtility.Click(applyFilter);
        
        Thread.sleep(2000);
        licenseFilters.click();
        clearFilter.click();
        
        WebUtility.Click(licenseFilters);
        Thread.sleep(1000);
        licenseFilterActiveStatus.click();
        WebUtility.Click(applyFilter);

        Thread.sleep(2000);
        licenseFilters.click();
        clearFilter.click();

        WebUtility.Click(licenseFilters);
        Thread.sleep(1000);
        licenseFilterInActiveStatus.click();
        WebUtility.Click(applyFilter);

        Thread.sleep(2000);
        licenseFilters.click();
        clearFilter.click();
        

    }

    public void VideoImages() throws InterruptedException {
/*
//------EDIT Flow for Testing ----------------------------------------------

    	WebUtility.Click(dashboard);
        WebUtility.Click(videoButton);

		WebUtility.Click(searchString);
		searchString.sendKeys(videoTitle);
		WebUtility.Wait();
		WebUtility.Wait();

		WebUtility.Click(editButton.get(1));

		WebUtility.Click(editVideo);

        */
        WebUtility.Click(images);

        WebUtility.Wait();
        cover.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\Cover.jpg");

        WebUtility.Wait();
        appcover.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\AppCover.jpg");

		try {
	        WebUtility.Wait();
	        list.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\List.jpg");
		}
		catch(Exception e)
		{
			//List Image Successfully imported from UID
		}

        WebUtility.Wait();
        square.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\SquarePNG.png");

        js.executeScript("window.scrollBy(0,600)");

        WebUtility.Wait();
        tvCover.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\TVCoverPNG.png");

        WebUtility.Wait();
        portrait.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\portrait.jpg");

        WebUtility.Wait();
        listClean.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\ListCleanPNG.png");

        js.executeScript("window.scrollTo(0,document.body.scrollHeight);");

        WebUtility.Wait();
        portraitClean.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\PortraitCleanPNG.png");

		WebUtility.Wait();
		telcoSquare.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\TelcoSquare.jpg");

		WebUtility.Wait();
		passport.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\PassportPNG.png");

		//---------------------------Images Set 2 ----------------------------------------------
		try {
			WebUtility.element_to_be_fluentwait(imagesCreateNewSet);
			WebUtility.Click(imagesCreateNewSet);

			WebUtility.Click(newSetName);
			newSetName.sendKeys("Automation Test Data");

			newSetCountry.click();
			WebUtility.Click(dropDownList.get(0));
			closeDropdown.click();

			newSetPlatform.click();
			WebUtility.Click(dropDownList.get(0));
			closeDropdown.click();

			newSetGender.click();
			WebUtility.Click(dropDownList.get(0));
			closeDropdown.click();

			newSetGenre.click();
			WebUtility.Click(dropDownList.get(0));
			closeDropdown.click();

			newSetAgeGroup.click();
			WebUtility.Click(dropDownList.get(0));
			closeDropdown.click();

			newSetLanguage.click();
			WebUtility.Click(dropDownList.get(0));
			closeDropdown.click();

			newSetOthers.click();
			newSetOthers.sendKeys("Automation Test Data");

			WebUtility.Click(save);
			WebUtility.Wait();

			WebUtility.Click(imageSets.get(1));
			WebUtility.Wait();

			js.executeScript("window.scrollTo(0,0)");
			WebUtility.Wait();
			cover.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\Cover.jpg");

			WebUtility.Wait();
			appcover.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\AppCover.jpg");

			WebUtility.Wait();
			list.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\List.jpg");

			WebUtility.Wait();
			square.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\SquarePNG.png");

			js.executeScript("window.scrollBy(0,600)");

			WebUtility.Wait();
			tvCover.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\TVCoverPNG.png");

			WebUtility.Wait();
			portrait.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\portrait.jpg");

			WebUtility.Wait();
			listClean.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\ListCleanPNG.png");

			js.executeScript("window.scrollTo(0,document.body.scrollHeight);");

			WebUtility.Wait();
			portraitClean.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\PortraitCleanPNG.png");

			WebUtility.Wait();
			telcoSquare.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\TelcoSquare.jpg");

			WebUtility.Wait();
			passport.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\PassportPNG.png");

        }
        catch (Exception e) {
        	e.printStackTrace();
        }
        
        try {
			// -------------------------- Image Set 3
			// -----------------------------------------------------
			WebUtility.element_to_be_fluentwait(imagesCreateNewSet);
			WebUtility.Click(imagesCreateNewSet);

			WebUtility.Click(newSetName);
			newSetName.sendKeys("Automation Test Data II");

			newSetCountry.click();
			WebUtility.Click(dropDownList.get(1));
			closeDropdown.click();

			newSetPlatform.click();
			WebUtility.Click(dropDownList.get(1));
			closeDropdown.click();

			newSetGender.click();
			WebUtility.Click(dropDownList.get(1));
			closeDropdown.click();

			newSetGenre.click();
			WebUtility.Click(dropDownList.get(1));
			closeDropdown.click();

			newSetAgeGroup.click();
			WebUtility.Click(dropDownList.get(1));
			closeDropdown.click();

			newSetLanguage.click();
			WebUtility.Click(dropDownList.get(1));
			closeDropdown.click();

			newSetOthers.click();
			newSetOthers.sendKeys("Automation Test Data II");

			WebUtility.Click(save);
			WebUtility.Wait();

			WebUtility.Click(imageSets.get(2));

	        js.executeScript("window.scrollTo(0,0)");
	        WebUtility.Wait();
			cover.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\Cover.jpg");

			WebUtility.Wait();
			appcover.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\AppCover.jpg");

			WebUtility.Wait();
			list.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\List.jpg");

			WebUtility.Wait();
			square.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\SquarePNG.png");

			js.executeScript("window.scrollBy(0,600)");

			WebUtility.Wait();
			tvCover.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\TVCoverPNG.png");

			WebUtility.Wait();
			portrait.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\portrait.jpg");

			WebUtility.Wait();
			listClean.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\ListCleanPNG.png");

			js.executeScript("window.scrollTo(0,document.body.scrollHeight);");

			WebUtility.Wait();
			portraitClean.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\PortraitCleanPNG.png");

			WebUtility.Wait();
			telcoSquare.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\TelcoSquare.jpg");

			WebUtility.Wait();
			passport.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\PassportPNG.png");
		} catch (Exception e) {
			e.printStackTrace();
		}
        
     // ---------------------- Inactivate Image Set ------------------------
		try {
			js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
			WebUtility.Wait();
			WebUtility.Click(imageSetActiveButton);
			WebUtility.Wait();
			WebUtility.Click(dropDownList.get(0));
			WebUtility.javascript(yes.get(0));
			WebUtility.Wait();
		}
        catch(Exception e) {
        	e.printStackTrace();
        }
		
		//------------------------ Delete Image Set -----------------------------
		try {
			removeSet.get(1).click();
			WebUtility.Wait();
			yes.get(0).click();
			WebUtility.Wait();
		} catch (Exception e) {
			e.printStackTrace();
		}
		js.executeScript("window.scrollTo(0,0);");
        WebUtility.Wait();
        String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
        sa.assertTrue(doneButtonActiveColor.contains("255,255,255") || doneButtonActiveColor.contains("255, 255, 255"), "Video Images Mark As Done Assertion Fails");
        WebUtility.Click(doneButtonActive);

        
        WebUtility.Wait();
        WebUtility.Click(imageSets.get(0));
        try {
            TestUtil.captureScreenshot("Main Content Images Section Default Set","Video");
        } catch (IOException e) {
            System.out.println("Screenshot Capture Failed ->" + e.getMessage());
        }

        js.executeScript("window.scrollTo(0,document.body.scrollHeight);");

        try {
            TestUtil.captureScreenshot("Main Content Images Section Default Set 2","Video");
        } catch (IOException e) {
            System.out.println("Screenshot Capture Failed ->" + e.getMessage());
        }

        WebUtility.Click(imageSets.get(1));
        js.executeScript("window.scrollTo(0,0);");
        try {
            TestUtil.captureScreenshot("Main Images Section Created Set","Video");
        } catch (IOException e) {
            System.out.println("Screenshot Capture Failed ->" + e.getMessage());
        }
        js.executeScript("window.scrollTo(0,document.body.scrollHeight);");

        try {
            TestUtil.captureScreenshot("Main Images Section Created Set 2","Video");
        } catch (IOException e) {
            System.out.println("Screenshot Capture Failed ->" + e.getMessage());
        }
    }

    public void VideoImagesAssertions() throws InterruptedException {

        WebUtility.Click(images);
        WebUtility.Wait();
        WebUtility.Click(imageSets.get(0));
        String coverName = "Cover", appCoverName = "AppCover", listName = "List", squareName = "SquarePNG", tvCoverName = "TVCoverPNG", portraitName = "Portrait", listCleanName = "ListCleanPNG", portraitCleanName = "PortraitCleanPNG", telcoSquareName = "TelcoSquare", passportName = "PassportPNG";
        boolean coverFound = false, appCoverFound = false, listFound = false, squareFound = false, tvCoverFound = false, portraitFound = false, listCleanFound = false, portraitCleanFound = false, telcoSquareFound = false, passportFound = false;
        List < WebElement > imageCheck = driver.findElements(By.tagName("strong"));

        for (int i = 0; i < imageCheck.size(); i++) {
            if (imageCheck.get(i).getText().equals(coverName.toLowerCase()))
                coverFound = true;

            if (imageCheck.get(i).getText().equals(appCoverName.toLowerCase()))
                appCoverFound = true;

            if (imageCheck.get(i).getText().equals(listName.toLowerCase()))
                listFound = true;

            if (imageCheck.get(i).getText().equals(squareName.toLowerCase()))
                squareFound = true;

            if (imageCheck.get(i).getText().equals(tvCoverName.toLowerCase()))
                tvCoverFound = true;

            if (imageCheck.get(i).getText().equals(portraitName.toLowerCase()))
                portraitFound = true;

            if (imageCheck.get(i).getText().equals(listCleanName.toLowerCase()))
                listCleanFound = true;

            if (imageCheck.get(i).getText().equals(portraitCleanName.toLowerCase()))
                portraitCleanFound = true;

            if (imageCheck.get(i).getText().equals(telcoSquareName.toLowerCase()))
                telcoSquareFound = true;

            if (imageCheck.get(i).getText().equals(passportName.toLowerCase()))
                passportFound = true;

        }

        Assert.assertEquals(coverFound, true, "Image Cover Assertion Failed");
        Assert.assertEquals(appCoverFound, true, "Image App Cover Assertion Failed");
        Assert.assertEquals(listFound, true, "Image List Assertion Failed");
        Assert.assertEquals(squareFound, true, "Image Square Assertion Failed");
        Assert.assertEquals(tvCoverFound, true, "Image TV Cover Assertion Failed");
        Assert.assertEquals(portraitFound, true, "Image Portrait Assertion Failed");
        Assert.assertEquals(listCleanFound, true, "Image List CLean Assertion Failed");
        Assert.assertEquals(portraitCleanFound, true, "Image Portrait Clean Assertion Failed");
        Assert.assertEquals(telcoSquareFound, true, "Image Telco Square Assertion Failed");
        Assert.assertEquals(passportFound, true, "Image Passport Assertion Failed");

        String doneButtonClickedColor = doneButtonClicked.getCssValue("background-color");
        //Assert.assertEquals(doneButtonClickedColor, "rgba(52, 194, 143, 1)", "Video Images Mark As Done Assertion Fails");
        Assert.assertTrue(doneButtonClickedColor.contains("52, 194, 143"), "Video Images Mark As Done Assertion Fails");
    }

    public void VideoSeoDetails() throws InterruptedException {

        WebUtility.Click(seoDetails);
        js.executeScript("window.scrollTo(0,0);");

        WebUtility.Wait();
        WebUtility.Click(seoTitleTag);
        seoTitleTag.sendKeys(Keys.CONTROL + "a");
        seoTitleTag.sendKeys(Keys.BACK_SPACE);
        WebUtility.Wait();
        seoTitleTag.sendKeys("Automation Test Data");

        seoMetaDescription.click();
        seoMetaDescription.sendKeys(Keys.CONTROL + "a");
        seoMetaDescription.sendKeys(Keys.BACK_SPACE);
        WebUtility.Wait();
        seoMetaDescription.sendKeys("Automation Test Data");

        seoMetaSynopsis.click();
        seoMetaSynopsis.sendKeys(Keys.CONTROL + "a");
        seoMetaSynopsis.sendKeys(Keys.BACK_SPACE);
        WebUtility.Wait();
        seoMetaSynopsis.sendKeys("Automation Test Data");

        seoRedirectionType.click();
        WebUtility.Wait();
        dropDownList.get(0).click();

        seoRedirectionLink.click();
        seoRedirectionLink.sendKeys(Keys.CONTROL + "a");
        seoRedirectionLink.sendKeys(Keys.BACK_SPACE);
        WebUtility.Wait();
        seoRedirectionLink.sendKeys("https://www.kelltontech.com");

        seoH1Heading.click();
        seoH1Heading.sendKeys(Keys.CONTROL + "a");
        seoH1Heading.sendKeys(Keys.BACK_SPACE);
        WebUtility.Wait();
        seoH1Heading.sendKeys("Automation Test Data");

        js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
        seoH2Heading.click();
        seoH2Heading.sendKeys(Keys.CONTROL + "a");
        seoH2Heading.sendKeys(Keys.BACK_SPACE);
        WebUtility.Wait();
        seoH2Heading.sendKeys("Automation Test Data");

        seoH3Heading.click();
        seoH3Heading.sendKeys(Keys.CONTROL + "a");
        seoH3Heading.sendKeys(Keys.BACK_SPACE);
        WebUtility.Wait();
        seoH3Heading.sendKeys("Automation Test Data");

        seoH4Heading.click();
        seoH4Heading.sendKeys(Keys.CONTROL + "a");
        seoH4Heading.sendKeys(Keys.BACK_SPACE);
        WebUtility.Wait();
        seoH4Heading.sendKeys("Automation Test Data");

        seoH5Heading.click();
        seoH5Heading.sendKeys(Keys.CONTROL + "a");
        seoH5Heading.sendKeys(Keys.BACK_SPACE);
        WebUtility.Wait();
        seoH5Heading.sendKeys("Automation Test Data");

        seoH6Heading.click();
        seoH6Heading.sendKeys(Keys.CONTROL + "a");
        seoH6Heading.sendKeys(Keys.BACK_SPACE);
        WebUtility.Wait();
        seoH6Heading.sendKeys("Automation Test Data");

        seoRobotsMetaIndex.get(0).click();
        seoRobotsMetaIndex.get(0).sendKeys(Keys.CONTROL + "a");
        seoRobotsMetaIndex.get(0).sendKeys(Keys.BACK_SPACE);
        WebUtility.Wait();
        seoRobotsMetaIndex.get(0).sendKeys("Automation Test Data");

        seoRobotsMetaIndex.get(1).click();
        seoRobotsMetaIndex.get(1).sendKeys(Keys.CONTROL + "a");
        seoRobotsMetaIndex.get(1).sendKeys(Keys.BACK_SPACE);
        WebUtility.Wait();
        seoRobotsMetaIndex.get(1).sendKeys("Automation Test Data");

        seoRobotsMetaImageIndex.click();
        seoRobotsMetaImageIndex.sendKeys(Keys.CONTROL + "a");
        seoRobotsMetaImageIndex.sendKeys(Keys.BACK_SPACE);
        WebUtility.Wait();
        seoRobotsMetaImageIndex.sendKeys("Automation Test Data");

        seoRobotsMetaImageNoIndex.click();
        seoRobotsMetaImageNoIndex.sendKeys(Keys.CONTROL + "a");
        seoRobotsMetaImageNoIndex.sendKeys(Keys.BACK_SPACE);
        WebUtility.Wait();
        seoRobotsMetaImageNoIndex.sendKeys("Automation Test Data");

        seoBreadcrumbTitle.click();
        seoBreadcrumbTitle.sendKeys(Keys.CONTROL + "a");
        seoBreadcrumbTitle.sendKeys(Keys.BACK_SPACE);
        WebUtility.Wait();
        seoBreadcrumbTitle.sendKeys("Automation Test Data");

        seoInternalLinkBuilding.click();
        seoInternalLinkBuilding.sendKeys(Keys.CONTROL + "a");
        seoInternalLinkBuilding.sendKeys(Keys.BACK_SPACE);
        WebUtility.Wait();
        seoInternalLinkBuilding.sendKeys("Automation Test Data");

        for (int i = 0; i < seoCheckboxes.size(); i++) {
            if (seoCheckboxes.get(i).isSelected() == false)
                seoCheckboxes.get(i).click();
        }

        String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
        sa.assertTrue(doneButtonActiveColor.contains("255,255,255") || doneButtonActiveColor.contains("255, 255, 255"), "Video SEO Details Mark As Done Assertion Fails");
        WebUtility.Click(doneButtonActive);
        try {
            TestUtil.captureScreenshot("Main Content SEO Details Section","Video");
        } catch (IOException e) {
            System.out.println("Screenshot Capture Failed ->" + e.getMessage());
        }

        js.executeScript("window.scrollTo(0,document.body.scrollHeight);");

        try {
            TestUtil.captureScreenshot("Main Content Seo Details Section 2","Video");
        } catch (IOException e) {
            System.out.println("Screenshot Capture Failed ->" + e.getMessage());
        }
    }

    public void VideoSeoDetailsAssertions() throws InterruptedException {

        JavascriptExecutor js = (JavascriptExecutor) driver;
        seoDetails.click();
        js.executeScript("window.scrollTo(0,0);");
        Thread.sleep(2000);
        String expectedTitleTag = "Automation Test Data";
        Assert.assertEquals(seoTitleTag.getAttribute("value"), expectedTitleTag, "SEO Title Tag Assertion Failed");

        String expectedRedirectionLink = "https://www.kelltontech.com";
        Assert.assertEquals(seoRedirectionLink.getAttribute("value"), expectedRedirectionLink, "SEO Redirection Link Assertion Failed");

        String doneButtonClickedColor = doneButtonClicked.getCssValue("background-color");
        //Assert.assertEquals(doneButtonClickedColor, "rgba(52, 194, 143, 1)", "Video SEO Details Mark As Done Assertion Fails");
        Assert.assertTrue(doneButtonClickedColor.contains("52, 194, 143"), "Video SEO Details Mark As Done Assertion Fails");
    }

    public void VideoMapContent() throws InterruptedException {
//TODO ------------------- Update when Permissions are fixed------------------------
    	/*
        WebUtility.Wait();
        WebUtility.Click(logoutlink);

        logoutbutton.click();
        WebUtility.clear_cache();
        driver.get(prop.getProperty("url"));
        loginPage1.gmailLogin(prop.getProperty("Gusername2"), prop.getProperty("Gpassword"));

        WebUtility.Click(dashboard);
        WebUtility.Click(videoButton);

        WebUtility.Click(searchString);
        searchString.sendKeys(videoTitle);
        WebUtility.Wait();
        WebUtility.Wait();
        WebUtility.Click(editButton.get(0));

        WebUtility.Click(editVideo);
*/
        WebUtility.Click(mapContent);
		WebUtility.Wait();
		try {
            unlock.click();
            WebUtility.Click(yes.get(0));
        } catch (Exception e) {
            System.out.println("Section Unlocked");
        }
		WebUtility.Click(addContent);
	
	    WebUtility.element_to_be_visible(mapContentAssign.get(0));
	    checkBox.get(0).click();
	    WebUtility.Click(assignContent);
	    WebUtility.Wait();

        WebUtility.element_to_be_visible(doneButtonActive);
        String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
        sa.assertTrue(doneButtonActiveColor.contains("255,255,255") || doneButtonActiveColor.contains("255, 255, 255"), "Video Map Content Mark As Done Assertion Fails");
       
        WebUtility.Click(doneButtonActive);
        WebUtility.Wait();

        try {
            TestUtil.captureScreenshot("Main ContentMap Content Section","Video");
        } catch (IOException e) {
            System.out.println("Screenshot Capture Failed ->" + e.getMessage());
        }
    }

    public void VideoMapContentAssertions() throws InterruptedException {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        WebUtility.Click(mapContent);

        Object mappedContentExists = js.executeScript("var x = document.getElementsByClassName('whitebox list-profile-box flex justify-content-between'),contentMapped = false;if(x.length > 0 ) contentMapped = true; return contentMapped");
        Assert.assertEquals(mappedContentExists, true, "Mapped Content Assertion Failed");

        String doneButtonClickedColor = doneButtonClicked.getCssValue("background-color");
        sa.assertTrue(doneButtonClickedColor.contains("52, 194, 143"), "Video Map Content Mark As Done Clicked Assertion Fails");

    }

    public void VideoPublishFlow() throws InterruptedException {
    	//TODO ------------------- Update when Permissions are fixed------------------------
    	/*
    	//---------------------------------Publish --------------------------------------------------------------------------------        
        WebUtility.Wait();
        WebUtility.Click(logoutlink);

        logoutbutton.click();
        WebUtility.clear_cache();
        driver.get(prop.getProperty("url"));
        loginPage1.gmailLogin(prop.getProperty("Gusername1"), prop.getProperty("Gpassword"));
        //dashboard.click();



        WebUtility.Click(dashboard);
        WebUtility.Click(videoButton);

        WebUtility.Click(searchString);
        searchString.sendKeys(videoTitle);
        WebUtility.Wait();  
		editButton.get(0).click();
        WebUtility.Click(editVideo);
        WebUtility.Wait();
        
		//--Verifying Video Draft Status
		try
		{
			videoProperties.click();
			WebUtility.element_to_be_visible(contentStatus);
			TestUtil.captureScreenshot("Video Content Draft Status","Video");
			String expectedStatus = "Draft";
			String actualStatus = contentStatus.getText();
			sa.assertEquals(actualStatus,expectedStatus,"Video Draft Status is Failed");
			Reporter.log("Video Draft Status is Passed",true);	
		}
		catch (Exception e)
		{
			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}
																				 						  
        WebUtility.Click(checklist);
        WebUtility.Wait();
        WebUtility.Click(submitToReview);
        WebUtility.Click(submitYesBtn);
        WebUtility.Click(submitOk);
        js.executeScript("window.scrollTo(0,0)");
        WebUtility.Wait();

		//--Verifying Video Submit to Review status
		try
		{
			videoProperties.click();
			WebUtility.element_to_be_visible(contentStatus);
			TestUtil.captureScreenshot("Video Content Submitted To Review Status","Video");
			String expectedStatus = "Submitted To Review";
			String actualStatus = contentStatus.getText();
			sa.assertEquals(actualStatus,expectedStatus,"Video Submitted To Review Status is Failed");
			Reporter.log("Video Submitted To Review Status is Passed",true);	
		}
		catch (Exception e)
		{
			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}
		
        logoutlink.click();
        logoutbutton.click();
        WebUtility.clear_cache();
        driver.get(prop.getProperty("url"));
        loginPage1.gmailLogin(prop.getProperty("Gusername2"), prop.getProperty("Gpassword"));
        WebUtility.Wait();
        WebUtility.Click(dashboard);
        WebUtility.Click(videoButton);
        WebUtility.Wait();

        WebUtility.Click(searchString);
        searchString.sendKeys(videoTitle);
        WebUtility.Wait();
        WebUtility.Click(editButton.get(0));
        WebUtility.Click(editVideo);
        WebUtility.Wait();
        WebUtility.Click(checklist);
        WebUtility.Click(needWorkReason);
        WebUtility.Click(dropDownList.get(0));
        WebUtility.Click(needWorkBtn);
        WebUtility.Click(yes.get(0));
        WebUtility.Click(ok);
        js.executeScript("window.scrollTo(0,0)");
        WebUtility.Wait();

		//--Verifying Video Need Work status
		try
		{
			videoProperties.click();
			WebUtility.element_to_be_visible(contentStatus);
			TestUtil.captureScreenshot("Video Content Need Work Status","Video");
			String expectedStatus = "Need Work";
			String actualStatus = contentStatus.getText();
			sa.assertEquals(actualStatus,expectedStatus,"Video Need Work Status is Failed");
			Reporter.log("Video Need Work Status is Passed",true);	
		}
		catch (Exception e)
		{
			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}
		
        WebUtility.Click(logoutlink);
        logoutbutton.click();
        WebUtility.clear_cache();
        driver.get(prop.getProperty("url"));
        loginPage1.gmailLogin(prop.getProperty("Gusername1"), prop.getProperty("Gpassword"));
        WebUtility.Wait();
        WebUtility.Click(dashboard);
        WebUtility.Click(videoButton);
        WebUtility.Wait();
        WebUtility.Click(searchString);
        searchString.sendKeys(videoTitle);
        WebUtility.Wait();
        WebUtility.Click(editButton.get(0));
        WebUtility.Click(editVideo);
        WebUtility.Click(note);
        note.sendKeys("s");
        WebUtility.Click(nextButton);
        WebUtility.Click(doneButtonActive);
        WebUtility.Wait();
        WebUtility.Click(checklist);
        WebUtility.Click(submitToReview);
        WebUtility.Wait();
        WebUtility.Click(submitYesBtn);
        WebUtility.Click(submitOk);
        js.executeScript("window.scrollTo(0,0)");
        WebUtility.Wait();
        
		//--Verifying Video Submit to Review status
		try
		{
			videoProperties.click();
			WebUtility.element_to_be_visible(contentStatus);
			TestUtil.captureScreenshot("Video Content Submitted To Review Status","Video");
			String expectedStatus = "Submitted To Review";
			String actualStatus = contentStatus.getText();
			sa.assertEquals(actualStatus,expectedStatus,"Video Submitted To Review Status is Failed");
			Reporter.log("Video Submitted To Review Status is Passed",true);	
		}
		catch (Exception e)
		{
			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}
		
        logoutlink.click();
        logoutbutton.click();
        WebUtility.clear_cache();
        driver.get(prop.getProperty("url"));
        loginPage1.gmailLogin(prop.getProperty("Gusername2"), prop.getProperty("Gpassword"));
        WebUtility.Wait();
        WebUtility.Click(dashboard);
        WebUtility.Click(videoButton);
        WebUtility.Wait();
        WebUtility.Click(searchString);
        searchString.sendKeys(videoTitle);
        WebUtility.Wait();
        WebUtility.Click(editButton.get(0));
        WebUtility.Click(editVideo);    	
*/
        WebUtility.Wait();
        WebUtility.Click(checklist);
        WebUtility.Wait();
        js.executeScript("window.scrollTo(0,document.body.scrollHeight)");
        WebUtility.Click(publishCountry);
        WebUtility.Click(dropDownList.get(0));
        WebUtility.Click(publishContent);
        WebUtility.Click(yes.get(0));
        WebUtility.Click(ok);
        js.executeScript("window.scrollTo(0,0)");
        
		//--Verifying Video Published status
		try
		{
				videoProperties.click();
				WebUtility.element_to_be_visible(contentStatus);
				TestUtil.captureScreenshot("Video Content Published Status","Video");
				String expectedStatus = "Published";
				String actualStatus = contentStatus.getText();
				sa.assertEquals(actualStatus,expectedStatus,"Video Published Status is Failed");
				Reporter.log("Video Published Status is Passed",true);	
		}
		catch (Exception e)
		{
				System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}
        WebUtility.Wait();
        
        //----------------Republish -----------------------------------------------------------------------------

  	   WebUtility.Click(videos);
//--------------- TODO Update when roles are fixed
  	   /*
  	   try {
	            WebUtility.Click(unlock);
	            WebUtility.Click(yes.get(0));
	        } catch (Exception e2) {
	            System.out.println("Section Unlocked");
	        }
*/
    	   WebUtility.Click(audioTrack);
           audioTrack.sendKeys(Keys.CONTROL + "a");
           audioTrack.sendKeys(Keys.BACK_SPACE);
           audioTrack.sendKeys("Automation Test Data II");
           WebUtility.Click(subTitle);
           WebUtility.Click(doneButtonActive);
           WebUtility.Wait();
           
   		//--Verifying Video Changed status
   		try
   		{
   				videoProperties.click();
   				WebUtility.element_to_be_visible(contentStatus);
   				TestUtil.captureScreenshot("Video Content Changed Status","Video");
   				String expectedStatus = "Changed";
   				String actualStatus = contentStatus.getText();
   				sa.assertEquals(actualStatus,expectedStatus,"Video Changed Status is Failed");
   				Reporter.log("Video Changed Status is Passed",true);	
   		}
   		catch (Exception e)
   		{
   				System.out.println("Screenshot Capture Failed ->" + e.getMessage());
   		}
        
        WebUtility.Click(checklist);
        js.executeScript("window.scrollTo(0,document.body.scrollHeight)");
        WebUtility.Click(republishContent); 
        WebUtility.Click(yes.get(0));
        WebUtility.Click(yes.get(1));
        WebUtility.Click(ok);
        js.executeScript("window.scrollTo(0,0)");
        WebUtility.Wait();
        
		//--Verifying Video Republished status
		try
		{
				videoProperties.click();
				WebUtility.element_to_be_visible(contentStatus);
				TestUtil.captureScreenshot("Video Content Republished Status","Video");
				String expectedStatus = "Published";
				String actualStatus = contentStatus.getText();
				sa.assertEquals(actualStatus,expectedStatus,"Video Republished Status is Failed");
				Reporter.log("Video Republished Status is Passed",true);	
		}
		catch (Exception e)
		{
				System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}
		
		WebUtility.Wait();
        
        //------------------------------Unpublish --------------------------------------------------------------------------------------------        
        js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
        WebUtility.Click(checklist);
        WebUtility.Click(unpublishContent);
        WebUtility.Click(unpublishReason);
        WebUtility.Click(dropDownList.get(0));
        WebUtility.Click(yes.get(0));
        WebUtility.Click(ok);
        js.executeScript("window.scrollTo(0,0)");
        WebUtility.Wait();
        
		//--Verifying Video UnPublished status
		try
		{
			videoProperties.click();
			WebUtility.element_to_be_visible(contentStatus);
			TestUtil.captureScreenshot("Video Content Unpublished Status","Video");
			String expectedStatus = "Unpublished";
			String actualStatus = contentStatus.getText();
			sa.assertEquals(actualStatus,expectedStatus,"Video Unpublished Status is Failed");
			Reporter.log("Video Unpublished Status is Passed",true);	
		}
		catch (Exception e)
		{
			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}
        WebUtility.Wait();
        publishedHistory.click();
        WebUtility.Wait();
        try {
            TestUtil.captureScreenshot("Video Main Content - Published History","Video");
        } catch (IOException e) {
            System.out.println("Screenshot Capture Failed ->" + e.getMessage());
        }
        WebUtility.Click(updateVideoBreadcrumb);
        WebUtility.Wait();

        // Archive unpublished content -------------------------------------     
        WebUtility.Click(dashboard);
        WebUtility.Click(videoButton);
        WebUtility.Wait();
        WebUtility.Click(searchString);
        searchString.sendKeys(videoTitle);
        WebUtility.Wait();
		Archive.get(0).click();				
		ArchiveYes.click();
		WebUtility.Wait();
        WebUtility.Click(editButton.get(0));
		WebUtility.Wait();
		
		//--Verifying Video Quick Filing Archive status
		try
		{
			videoProperties.click();
			WebUtility.element_to_be_visible(contentStatus);
			TestUtil.captureScreenshot("Video content Archived Status","Movie");
			String expectedStatus = "Archived";
			String actualStatus = contentStatus.getText();
			sa.assertEquals(actualStatus,expectedStatus,"Video Archived Status is Failed");
			Reporter.log("Video Archived Status is Passed",true);	
		}
		catch (Exception e)
		{
			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}
		WebUtility.Wait();
		
		// Restore Content to draft mode ---------------------------------------
        WebUtility.Click(dashboard);
        WebUtility.Click(videoButton);
        WebUtility.Wait();
        WebUtility.Click(searchString);
        searchString.sendKeys(videoTitle);
        WebUtility.Wait();
		WebUtility.javascript(Restore);			
		WebUtility.Wait();
		WebUtility.javascript(RestoreYes);
		WebUtility.Wait();
        WebUtility.Click(editButton.get(0));
		WebUtility.Wait();
		
		//--Verifying Video Quick Filing Restore Status
		try
		{
			videoProperties.click();
			WebUtility.element_to_be_visible(contentStatus);
			TestUtil.captureScreenshot("Video content Restore Status","Movie");
			String expectedStatus = "Draft";
			String actualStatus = contentStatus.getText();
			sa.assertEquals(actualStatus,expectedStatus,"Video Restore Status is Failed");
			Reporter.log("Video Restore Status is Passed",true);	
		}
		catch (Exception e)
		{
			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}
    }


    public void VideoScheduleContent() throws InterruptedException {

        WebUtility.Click(checklist);
        WebUtility.Wait();
        JavascriptExecutor js = (JavascriptExecutor) driver;
        WebUtility.Click(scheduleContent);
        WebUtility.Wait();
        js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
        
        //System.out.println("--------> Current Time = "+currentTime.getAttribute("innerText").substring(20, 28));
        //String currentTimeString  = currentTime.getAttribute("innerText").substring(20, 27);
    	//js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
        
        scheduledPublicationTime.click();
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        WebUtility.Click(datePickerMinutes);
        actionProvider.moveToElement(datePickerMinutesClock).click().build().perform();      
        datePicker.sendKeys(Keys.ESCAPE);

        WebUtility.Click(scheduledContentCountry);
        scheduledContentCountry.sendKeys(Keys.ARROW_DOWN);
        scheduledContentCountry.sendKeys(Keys.ENTER);

        WebUtility.Click(scheduleContentButton);
        WebUtility.Click(yes.get(0));
        WebUtility.Click(ok);

        js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
        WebUtility.Wait();
        js.executeScript("window.scrollTo(0,0);");
        WebUtility.Wait();
        
     
        //System.out.println("--------> Scheduled Time = "+scheduledTime.getAttribute("innerText").substring(12, 20));
        //String scheduledTimeString = scheduledTime.getAttribute("innerText").substring(12, 20);
    	//js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
    	//sa.assertTrue(scheduledTimeString.contains(currentTimeString),"Video Main Content - Current Time and Scheduled Time are not Equal");
        
		//--Verifying Video Scheduled status
		try
		{
			videoProperties.click();
			WebUtility.element_to_be_visible(contentStatus);
			TestUtil.captureScreenshot("Video Content Scheduled Status","Movie");
			String expectedStatus = "Scheduled";
			String actualStatus = contentStatus.getText();
			sa.assertEquals(actualStatus,expectedStatus,"Video Scheduled Status is Failed");
			Reporter.log("Video Scheduled Status is Passed",true);	
		}
		catch (Exception e)
		{
			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}
    }

    public void VideosRelatedContent() throws InterruptedException {
/*
        WebUtility.Click(dashboard);
        WebUtility.Click(videoButton);
        WebUtility.Wait();

        WebUtility.Click(searchString);
        searchString.sendKeys(videoTitle);
        WebUtility.Wait();
        WebUtility.Wait();
        WebUtility.Click(editButton.get(0));
        WebUtility.Click(editVideo);
        WebUtility.Wait();
   */     
        WebUtility.Click(relatedContent);
        /*
        WebUtility.Click(relatedContentAssignTvShows);

        WebUtility.element_to_be_visible(relatedContentAddTvShows);
        WebUtility.Click(relatedContentAddTvShows);

        WebUtility.Wait();
        checkBox.get(0).click();
        checkBox.get(1).click();
        WebUtility.Click(assignRelatedContent);
        WebUtility.Click(deleteRelatedContent); 
        WebUtility.Wait();
        WebUtility.Click(yes.get(0));
        WebUtility.Wait();
  
*/
        WebUtility.Click(relatedContentAssignVideos);
        WebUtility.element_to_be_visible(relatedContentAddVideos);
        WebUtility.Click(relatedContentAddVideos);

        WebUtility.Wait();
        checkBox.get(0).click();
        checkBox.get(1).click();
        WebUtility.Click(assignRelatedContentVideos);
        WebUtility.Click(deleteRelatedContent);
        WebUtility.Wait();
        WebUtility.Click(yes.get(0));
        WebUtility.Wait();
        
        WebUtility.Click(relatedContentAssignMovies);
        WebUtility.element_to_be_visible(relatedContentAddMovies);
        WebUtility.Click(relatedContentAddMovies);

        WebUtility.Wait();
        checkBox.get(0).click();
        checkBox.get(1).click();
        WebUtility.Click(assignRelatedContentMovies);
        WebUtility.Click(deleteRelatedContent);
        WebUtility.Wait();
        WebUtility.Click(yes.get(0));
        WebUtility.Wait();
/*
        WebUtility.Click(relatedContentAssignSeasons);
        WebUtility.element_to_be_visible(relatedContentAddSeasons);
        WebUtility.Click(relatedContentAddSeasons);

        WebUtility.Wait();
        checkBox.get(0).click();
        checkBox.get(1).click();
        WebUtility.Click(assignRelatedContent);
        WebUtility.Click(deleteRelatedContent);
        WebUtility.Wait();
        WebUtility.Click(yes.get(0));
        WebUtility.Wait();

        WebUtility.Click(relatedContentAssignEpisodes);
        WebUtility.element_to_be_visible(relatedContentAddEpisodes);
        WebUtility.Click(relatedContentAddEpisodes);

        WebUtility.Wait();
        checkBox.get(0).click();
        checkBox.get(1).click();
        WebUtility.Click(assignRelatedContent);
        WebUtility.Click(deleteRelatedContent);
        WebUtility.Wait();
        WebUtility.Click(yes.get(0));
        WebUtility.Wait();
*/
        String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
        sa.assertTrue(doneButtonActiveColor.contains("255,255,255") || doneButtonActiveColor.contains("255, 255, 255"), "Video Related Content Content Mark As Done Assertion Fails");
        WebUtility.Click(doneButtonActive);


        WebUtility.Click(updateVideoBreadcrumb);
        WebUtility.Wait();
    }

    public void VideosTranslations() throws InterruptedException {

        WebUtility.Click(translations);

        WebUtility.Click(title);
        title.sendKeys(Keys.CONTROL + "a");
        title.sendKeys(Keys.BACK_SPACE);
        title.sendKeys("स्टार ट्रेक (2020)");

        translationsShortDesc.click();
        translationsShortDesc.sendKeys(Keys.CONTROL + "a");
        translationsShortDesc.sendKeys(Keys.BACK_SPACE);
        translationsShortDesc.sendKeys("स्टार ट्रेक 2009 की अमेरिकी साइंस फिक्शन एक्शन फिल्म है, जो जे। जे। अब्राम्स द्वारा निर्देशित और रॉबर्टो ओरसी और एलेक्स कर्ट्ज़मैन द्वारा लिखित है। यह स्टार ट्रेक फ्रैंचाइज़ी में ग्यारहवीं फिल्म है");

        translationsWebDesc.click();
        translationsWebDesc.sendKeys(Keys.CONTROL + "a");
        translationsWebDesc.sendKeys(Keys.BACK_SPACE);
        translationsWebDesc.sendKeys("स्वचालन परीक्षण");

        translationsAppDesc.click();
        translationsAppDesc.sendKeys(Keys.CONTROL + "a");
        translationsAppDesc.sendKeys(Keys.BACK_SPACE);
        translationsAppDesc.sendKeys("स्वचालन परीक्षण");

        translationsTVDesc.click();
        translationsTVDesc.sendKeys(Keys.CONTROL + "a");
        translationsTVDesc.sendKeys(Keys.BACK_SPACE);
        translationsTVDesc.sendKeys("स्वचालन परीक्षण");

        creativeTitle.click();
        creativeTitle.sendKeys(Keys.CONTROL + "a");
        creativeTitle.sendKeys(Keys.BACK_SPACE);
        creativeTitle.sendKeys("स्वचालन रचनात्मक शीर्षक");

        translationsAlternativeTitle.click();
        translationsAlternativeTitle.sendKeys(Keys.CONTROL + "a");
        translationsAlternativeTitle.sendKeys(Keys.BACK_SPACE);
        translationsAlternativeTitle.sendKeys("स्वचालन वैकल्पिक शीर्षक");

        pageTitle.click();
        pageTitle.sendKeys(Keys.CONTROL + "a");
        pageTitle.sendKeys(Keys.BACK_SPACE);
        pageTitle.sendKeys("ऑटोमेशन पेज का शीर्षक");

        pageDescription.click();
        pageDescription.sendKeys(Keys.CONTROL + "a");
        pageDescription.sendKeys(Keys.BACK_SPACE);
        pageDescription.sendKeys("स्वचालन पृष्ठ विवरण");

        translationsTitleForSocialShare.click();
        translationsTitleForSocialShare.sendKeys(Keys.CONTROL + "a");
        translationsTitleForSocialShare.sendKeys(Keys.BACK_SPACE);
        translationsTitleForSocialShare.sendKeys("सामाजिक हिस्सेदारी के लिए स्वचालन शीर्षक");

        translationsDescriptionForSocialShare.click();
        translationsDescriptionForSocialShare.sendKeys(Keys.CONTROL + "a");
        translationsDescriptionForSocialShare.sendKeys(Keys.BACK_SPACE);
        translationsDescriptionForSocialShare.sendKeys("सामाजिक शेयर के लिए स्वचालन विवरण");

        translationsUpcomingPage.click();
        translationsUpcomingPage.sendKeys(Keys.CONTROL + "a");
        translationsUpcomingPage.sendKeys(Keys.BACK_SPACE);
        translationsUpcomingPage.sendKeys("ऑटोमेशन आगामी पृष्ठ पाठ");

        trivia.click();
        trivia.sendKeys(Keys.CONTROL + "a");
        trivia.sendKeys(Keys.BACK_SPACE);
        trivia.sendKeys("ऑटोमेशन ट्रिविया");
        js.executeScript("window.scrollTo(0,0);");
        Thread.sleep(2000);

        castCrew.click();

        WebUtility.Click(translationsActor);
        translationsActor.sendKeys(Keys.CONTROL + "a");
        translationsActor.sendKeys(Keys.BACK_SPACE);
        translationsActor.sendKeys("ऑटोमेशन आगामी पृष्ठ पाठ");

        translationsCharacter.click();
        translationsCharacter.sendKeys(Keys.CONTROL + "a");
        translationsCharacter.sendKeys(Keys.BACK_SPACE);
        translationsCharacter.sendKeys("ऑटोमेशन आगामी पृष्ठ पाठ");
        WebUtility.Wait();
        
        String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
        sa.assertTrue(doneButtonActiveColor.contains("255,255,255") || doneButtonActiveColor.contains("255, 255, 255"), "Video Translations Mark As Done Assertion Fails");
        WebUtility.Click(doneButtonActive);
        WebUtility.Wait();
        WebUtility.Click(updateVideoBreadcrumb);

    }

	public void VideoClone() throws InterruptedException {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		WebUtility.Click(dashboard);
		WebUtility.Click(videoButton);

		WebUtility.Wait();
		WebUtility.Click(searchString);
		searchString.sendKeys(videoTitle);
		WebUtility.Wait();
		WebUtility.Wait();		
    	WebUtility.Click(clone);
    	WebUtility.Click(yes.get(0));
    	WebUtility.Wait();
    	
    	WebUtility.Click(checklist);
        js.executeScript("window.scrollTo(0,document.body.scrollHeight)");
        WebUtility.Click(publishCountry);
        WebUtility.Click(dropDownList.get(0));

        WebUtility.Click(publishContent);
        WebUtility.Click(yes.get(0));
        WebUtility.Click(ok);
        js.executeScript("window.scrollTo(0,0)");
        try {
            TestUtil.captureScreenshot("Clone Video - Published","Video");
        } catch (IOException e) {
            System.out.println("Screenshot Capture Failed ->" + e.getMessage());
        }
        WebUtility.Wait();
        
        js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
        WebUtility.Click(checklist);
        WebUtility.Click(unpublishContent);

        WebUtility.Click(unpublishReason);
        WebUtility.Click(dropDownList.get(0));
        WebUtility.Click(yes.get(0));
        WebUtility.Click(ok);
        js.executeScript("window.scrollTo(0,0)");
        WebUtility.Wait();
        try {
            TestUtil.captureScreenshot("Clone Video - Unpublish","Video");
        } catch (IOException e) {
            System.out.println("Screenshot Capture Failed ->" + e.getMessage());
        }

        WebUtility.Wait();
        publishedHistory.click();
        WebUtility.Wait();
        try {
            TestUtil.captureScreenshot("Clone Video - Published History","Video");
        } catch (IOException e) {
            System.out.println("Screenshot Capture Failed ->" + e.getMessage());
        }
    }

    // ----------------------------- END of Main Content ------------------------------------------------------------

    //-------------------------------- Sort & Filter Sections -------------------------------------------------------	   



    public void VideoFilter() throws InterruptedException {

		WebUtility.Click(dashboard);
		WebUtility.Click(videoButton);
		WebUtility.Wait();		
		
		filters.click();
		filtersDraftStatus.click();     
		WebUtility.Wait();
        WebUtility.Click(applyFilter);
        WebUtility.Wait();

        filters.click();
		filterAllStatus.click();
		WebUtility.Wait();
        WebUtility.Click(applyFilter);
		WebUtility.Wait();
      
        filters.click();
        filtersChangedStatus.click();
        WebUtility.Wait();
        WebUtility.Click(applyFilter);
        WebUtility.Wait();             
       
        filters.click();
        filtersPublishedStatus.click();
        WebUtility.Wait();
        WebUtility.Click(applyFilter);
        WebUtility.Wait();

        filters.click();
        filtersUnpublishedStatus.click();
        WebUtility.Wait();
        WebUtility.Click(applyFilter);
        WebUtility.Wait();       

        filters.click();
        filtersNeedWorkStatus.click();
        WebUtility.Wait();
        WebUtility.Click(applyFilter);
        WebUtility.Wait();  

        filters.click();
        filtersScheduledStatus.click();
        WebUtility.Wait();
        WebUtility.Click(applyFilter);
        WebUtility.Wait();      
        
        filters.click();							   
        filtersSubmittedToReviewStatus.click();
        WebUtility.Wait();
        WebUtility.Click(applyFilter);
        WebUtility.Wait();

        filters.click();
        filtersArchivedStatus.click();
        WebUtility.Wait();
        WebUtility.Click(applyFilter);				   
		WebUtility.Wait();

		filters.click();
		clearFilter.click();
		WebUtility.Wait();
		
		filters.click();
		filterStartDate.click();
		datePicker.sendKeys(Keys.ARROW_LEFT);
		datePicker.sendKeys(Keys.ARROW_LEFT);
		datePicker.sendKeys(Keys.ARROW_LEFT);
		datePicker.sendKeys(Keys.ARROW_LEFT);
		datePicker.sendKeys(Keys.ESCAPE);	
		WebUtility.Wait();
	    
	    filterEndDate.click();
	    datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ESCAPE);
        WebUtility.Click(applyFilter);
		WebUtility.Wait();

		filters.click();
		clearFilter.click();
		WebUtility.Wait();
		
        filters.click();  
        WebUtility.Click(filtersPrimaryGenre);
        WebUtility.Click(dropDownList.get(0));
        filterCloseDropdown.click();

        WebUtility.Click(filtersSecondaryGenre);
        WebUtility.Click(dropDownList.get(0));
        filterCloseDropdown.click();

        WebUtility.Click(filtersThematicGenre);
        WebUtility.Click(dropDownList.get(0));
        filterCloseDropdown.click();

        WebUtility.Click(filtersSettingGenre);
        WebUtility.Click(dropDownList.get(0));
        filterCloseDropdown.click();

        WebUtility.Click(filtersActivity);
        WebUtility.Click(dropDownList.get(0));
        filterCloseDropdown.click();

        WebUtility.Click(filtersStyle);
        WebUtility.Click(dropDownList.get(0));
        filterCloseDropdown.click();

        WebUtility.Click(filtersActor);
        filtersActor.sendKeys("Salman Khan");
        WebUtility.Wait();
        filtersActor.sendKeys(Keys.ARROW_DOWN);
        filtersActor.sendKeys(Keys.ENTER);

        WebUtility.Click(filtersSubType);
        WebUtility.Click(dropDownList.get(0));
        filtersSubType.click();
        
        WebUtility.Click(filtersContentCategory);
        WebUtility.Click(dropDownList.get(0));
        filtersContentCategory.click();

        WebUtility.Click(filtersRcsCategory);
        WebUtility.Click(dropDownList.get(0));
        filtersRcsCategory.click();

        WebUtility.Click(filtersTheme);
        WebUtility.Click(dropDownList.get(0));
        filtersTheme.click();

        WebUtility.Click(filtersTargetAudience);
        WebUtility.Click(dropDownList.get(0));
        filtersTargetAudience.click();

        WebUtility.Click(filtersLicenseGroup);
        WebUtility.Click(dropDownList.get(0));
        filtersLicenseGroup.click();

        WebUtility.Click(filtersAgeRating);
        WebUtility.Click(dropDownList.get(0));
        filtersAgeRating.click();

        WebUtility.Click(filtersBusinessType);
        WebUtility.Click(dropDownList.get(0));
        filtersBusinessType.click();

      //  WebUtility.Click(filtersExternalID);
      //  filtersExternalID.sendKeys("0-");

        WebUtility.Click(filtersContentRating);
        WebUtility.Click(dropDownList.get(0));
        filtersContentRating.click();

        WebUtility.Click(filtersContentOwner);
        WebUtility.Click(dropDownList.get(0));
        filtersContentOwner.click();

        WebUtility.Click(filtersAudioLanguage);
        WebUtility.Click(dropDownList.get(0));
        filtersAudioLanguage.click();

        WebUtility.Click(filtersOriginalLanguage);
        WebUtility.Click(dropDownList.get(0));
        filtersOriginalLanguage.click();
   
  		WebUtility.Click(filtersTags);
        filtersTags.sendKeys("Test");
        filtersTags.sendKeys(Keys.ARROW_DOWN);
        filtersTags.sendKeys(Keys.ENTER);
        WebUtility.Click(filtersTags);

        WebUtility.Click(filtersTranslationLanguage);
        WebUtility.Click(dropDownList.get(0));
        filtersTranslationLanguage.click();

        WebUtility.Click(filtersTranslationLanguageStatus);
        WebUtility.Click(dropDownList.get(0));

        filtersMood.click();
        WebUtility.Click(dropDownList.get(0));
        filtersMood.click();

        applyFilter.click();
        WebUtility.Wait();

        filters.click();
        clearFilter.click();
        WebUtility.Wait();
    }

    public void VideoSort() throws InterruptedException {
		WebUtility.Click(dashboard);
		WebUtility.Click(videoButton);
		WebUtility.Wait();		

        sort.click();						   
        releaseDateAscToDsc.click();
        WebUtility.Wait();
        applySort.click();
        WebUtility.Wait();

        sort.click();						   
        releaseDateDscToAsc.click();
        WebUtility.Wait();
        applySort.click();
        WebUtility.Wait();

        sort.click();						   
        modifiedOldToNew.click();	
        WebUtility.Wait();
        applySort.click();
        WebUtility.Wait();
        
        sort.click();						   
        modifiedNewToOld.click();
        WebUtility.Wait();
        applySort.click();
        WebUtility.Wait();

    }

    // ----------------------------------- END of Sorting and Filters ---------------------------------------------   

    //--------------------------------------- Quick Filling --------------------------------------------------------    

    public void VideoQuickFillingVideoProperties() throws InterruptedException {

    	//TODO ------------------- Update when Permissions are fixed------------------------
    	/*
    	WebUtility.Wait();
        WebUtility.Click(logoutlink);

        logoutbutton.click();
        WebUtility.clear_cache();
        driver.get(prop.getProperty("url"));
        loginPage1.gmailLogin(prop.getProperty("Gusername1"), prop.getProperty("Gpassword"));
*/
        WebUtility.Click(dashboard);
        WebUtility.Click(videoButton);      
	
		quickFilling.click();
		WebUtility.Click(createQuickFiling);
		WebUtility.Wait();

        //------------------------------ Video Properties -----------------------------------------------------------------------------------------
        WebUtility.Click(title);
        title.sendKeys(Keys.CONTROL + "a");
        title.sendKeys(Keys.BACK_SPACE);
        title.sendKeys(videoQuickFilingTitle);

        shortDesc.click();
        shortDesc.sendKeys(Keys.CONTROL + "a");
        shortDesc.sendKeys(Keys.BACK_SPACE);
        shortDesc.sendKeys("Star Trek is a 2009 American science fiction action film directed by J. J. Abrams and written by Roberto Orci and Alex Kurtzman.");

        subType.click();
        WebUtility.Click(dropDownList.get(0));

        category.click();
        WebUtility.Click(dropDownList.get(0));
 
        primaryGenre.click();
        WebUtility.Click(dropDownList.get(0));

        WebUtility.Click(zee5ReleaseDate);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ESCAPE);

        WebUtility.Click(videoDuration);
        List < WebElement > videoduration = driver.findElements(By.xpath("//li[@role='button']"));
        for (WebElement e: videoduration) {
            String videohrs = e.getText();
            if (videohrs.contains("02")) {
                e.click();
                break;
            }
        }

        WebUtility.Click(audioLanguage);
        WebUtility.Click(dropDownList.get(0));

        primaryLanguage.click();
        WebUtility.Click(dropDownList.get(0));

        dubbedLanguageTitle.click();
        WebUtility.Click(dropDownList.get(0));

        originalLanguage.click();
        WebUtility.Click(dropDownList.get(0));

        contentVersion.click();
        WebUtility.Click(dropDownList.get(0));

        nextButton.click();
        WebUtility.Wait();

        contentOwner.click();
        WebUtility.Click(dropDownList.get(0));

        certification.click();
        WebUtility.Click(dropDownList.get(0));

        nextButton.click();
        WebUtility.Wait();

        js.executeScript("window.scrollTo(0,0);");
        String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
        sa.assertTrue(doneButtonActiveColor.contains("255,255,255") || doneButtonActiveColor.contains("255, 255, 255"), "Quick Filing Video Properties Mark As Done Assertion Fails");
        WebUtility.Click(doneButtonActive);
        WebUtility.Wait();
        try {
            TestUtil.captureScreenshot("Quick Filling - Video Properties Section","Video");
        } catch (IOException e) {
            System.out.println("Screenshot Capture Failed ->" + e.getMessage());
        }
     }

    public void VideoQuickFillingVideos() throws InterruptedException {

    	WebUtility.Click(videos);
    	WebUtility.Wait();
        dashRootVideo.sendKeys(Keys.CONTROL + "a");
        dashRootVideo.sendKeys(Keys.BACK_SPACE);
        dashRootVideo.sendKeys("Automation Test Data");

        dashManifestVideo.sendKeys(Keys.CONTROL + "a" + Keys.BACK_SPACE);
        dashManifestVideo.sendKeys("Automation Test Data");


        hlsRootVideo.sendKeys(Keys.CONTROL + "a" + Keys.BACK_SPACE);
        hlsRootVideo.sendKeys("Automation Test Data");


        hlsManifestVideo.sendKeys(Keys.CONTROL + "a" + Keys.BACK_SPACE);
        hlsManifestVideo.sendKeys("Automation Test Data");


        mediaThekUID.sendKeys(Keys.CONTROL + "a" + Keys.BACK_SPACE);
        mediaThekUID.sendKeys("Automation Test Data");
        dashRootVideo.click();

        
        String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
        sa.assertTrue(doneButtonActiveColor.contains("255,255,255") || doneButtonActiveColor.contains("255, 255, 255"), "Quick Filing Video Properties Mark As Done Assertion Fails");
        WebUtility.element_to_be_clickable(doneButtonActive);
        doneButtonActive.click();
        WebUtility.Wait();
        try {
            TestUtil.captureScreenshot("Quick Filling - Video Content Section","Video");
        } catch (IOException e) {
            System.out.println("Screenshot Capture Failed ->" + e.getMessage());
        }
    }

    public void VideoQuickFillingLicense() throws InterruptedException {

		WebUtility.Click(license);
		WebUtility.Click(createLicense);

        WebUtility.Click(licenseSetName);
        licenseSetName.sendKeys("Automation Test Data");

        WebUtility.Click(licenseFromDate);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ESCAPE);

        WebUtility.Click(licenseToDate);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ESCAPE);
		
        WebUtility.Click(licenseCountry);
        for (int x = 0; x < dropDownList.size(); x++) {
        	if(x<4) {
            WebUtility.Click(dropDownList.get(x));
        	}
        	else {
        		break;
        	}
        }
		
		
		WebUtility.Click(licenseBusinessType);
		WebUtility.Click(dropDownList.get(0));

        WebUtility.Click(licenseContentAgeRatingId);	
        WebUtility.Wait();
        licenseContentAgeRatingId.sendKeys(Keys.ARROW_DOWN);
        licenseContentAgeRatingId.sendKeys(Keys.ENTER);
        
		WebUtility.Wait();
		WebUtility.Click(saveLicense);
		
		WebUtility.Click(license);
		WebUtility.Click(createLicense);

        WebUtility.Click(licenseSetName);
        licenseSetName.sendKeys("Automation Test Data 2");
        
        WebUtility.Click(licenseFromDate);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ESCAPE);

        WebUtility.Click(licenseToDate);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ESCAPE);
		
        WebUtility.Click(licenseCountry);
        for (int x = 0; x < dropDownList.size(); x++) {
        	if(x<4) {
            WebUtility.Click(dropDownList.get(x));
        	}
        	else {
        		break;
        	}
        }
		
		
		WebUtility.Click(licenseBusinessType);
		WebUtility.Click(dropDownList.get(0));

        WebUtility.Click(licenseContentAgeRatingId);	
        WebUtility.Wait();
        licenseContentAgeRatingId.sendKeys(Keys.ARROW_DOWN);
        licenseContentAgeRatingId.sendKeys(Keys.ENTER);
        
		WebUtility.Wait();
		WebUtility.Click(saveLicense);
		WebUtility.Wait();
		String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
		sa.assertTrue(doneButtonActiveColor.contains("255,255,255") || doneButtonActiveColor.contains("255, 255, 255"), "Quick Filing - Video License  Mark As Done Assertion Fails");
		WebUtility.Click(doneButtonActive);	

        try {
            TestUtil.captureScreenshot(" Quick Filing - License Section","Video");
        } catch (IOException e) {
            System.out.println("Screenshot Capture Failed ->" + e.getMessage());
        }

        WebUtility.Click(expiredLicense);
        WebUtility.Wait();
        WebUtility.Click(expiredLicenseBack);
        WebUtility.Wait();

        licenseFilters.click();
        WebUtility.Click(licenseFromDate);
        datePicker.sendKeys(Keys.ARROW_LEFT);
        datePicker.sendKeys(Keys.ARROW_LEFT);
        datePicker.sendKeys(Keys.ESCAPE);

        WebUtility.Click(licenseFilterToDate);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        datePicker.sendKeys(Keys.ESCAPE);

        WebUtility.Click(applyFilter);

        Thread.sleep(2000);
        licenseFilters.click();
        clearFilter.click();

        WebUtility.Click(licenseFilters);
        Thread.sleep(1000);
        licenseFilterBusinessType.click();
        WebUtility.Click(dropDownList.get(0));
        WebUtility.Click(applyFilter);

        Thread.sleep(2000);
        licenseFilters.click();
        clearFilter.click();
        
        WebUtility.Click(licenseFilters);
        Thread.sleep(1000);
        licenseContentAgeRatingId.click();
        WebUtility.Click(dropDownList.get(0));
        WebUtility.Click(applyFilter);
        
        Thread.sleep(2000);
        licenseFilters.click();
        clearFilter.click();
       
        WebUtility.Click(licenseFilters);
        Thread.sleep(1000);
        licenseFilterActiveStatus.click();
        WebUtility.Click(applyFilter);

        Thread.sleep(2000);
        licenseFilters.click();
        clearFilter.click();

        WebUtility.Click(licenseFilters);
        Thread.sleep(1000);
        licenseFilterInActiveStatus.click();
        WebUtility.Click(applyFilter);

        Thread.sleep(2000);
        licenseFilters.click();
        clearFilter.click();
        

        

    }
    public void VideoQuickFillingImages() throws InterruptedException {

    	JavascriptExecutor js = (JavascriptExecutor) driver;  
 		WebUtility.Click(images);

 		WebUtility.Wait();
        list.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\List.jpg");

        //--------------------- Set 2 ------------------------------------------------
        WebUtility.element_to_be_fluentwait(imagesCreateNewSet);
        WebUtility.Click(imagesCreateNewSet);
        
        WebUtility.Click(newSetName);
  		newSetName.sendKeys("Automation Test Data Quick Filing");		
 		
 		newSetCountry.click();
 		WebUtility.Click(dropDownList.get(0));
 		closeDropdown.click();
 		
 		newSetPlatform.click();
 		WebUtility.Click(dropDownList.get(0));
 		closeDropdown.click();
 		
 		newSetGender.click();
 		WebUtility.Click(dropDownList.get(0));
 		closeDropdown.click();
 		
 		newSetGenre.click();
 		WebUtility.Click(dropDownList.get(0));
 		closeDropdown.click();
 		
 		newSetAgeGroup.click();
 		WebUtility.Click(dropDownList.get(0));
 		closeDropdown.click();
 		
 		newSetLanguage.click();
 		WebUtility.Click(dropDownList.get(0));
 		closeDropdown.click();
 		
 		newSetOthers.click();
 		newSetOthers.sendKeys("Automation Test Data");
 		
 		WebUtility.Click(save);
 		WebUtility.Wait();		  		
 		WebUtility.Click(imageSets.get(1));	 		
 		js.executeScript("window.scrollTo(0,0)"); 
        list.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\List.jpg"); 		
        WebUtility.Wait();
        
        //-----------Set 3 --------------------------------------------
        WebUtility.element_to_be_fluentwait(imagesCreateNewSet);
        WebUtility.Click(imagesCreateNewSet);
        
        WebUtility.Click(newSetName);
  		newSetName.sendKeys("Automation Test Data Quick Filing II");		
 		
 		newSetCountry.click();
 		WebUtility.Click(dropDownList.get(1));
 		closeDropdown.click();
 		
 		newSetPlatform.click();
 		WebUtility.Click(dropDownList.get(1));
 		closeDropdown.click();
 		
 		newSetGender.click();
 		WebUtility.Click(dropDownList.get(1));
 		closeDropdown.click();
 		
 		newSetGenre.click();
 		WebUtility.Click(dropDownList.get(1));
 		closeDropdown.click();
 		
 		newSetAgeGroup.click();
 		WebUtility.Click(dropDownList.get(1));
 		closeDropdown.click();
 		
 		newSetLanguage.click();
 		WebUtility.Click(dropDownList.get(1));
 		closeDropdown.click();
 		
 		newSetOthers.click();
 		newSetOthers.sendKeys("Automation Test Data");
 		
 		WebUtility.Click(save);
 		WebUtility.Wait();		  		
 		WebUtility.Click(imageSets.get(2));	 		
 		js.executeScript("window.scrollTo(0,0)"); 
        list.sendKeys(System.getProperty("user.dir") + "\\images\\videos\\List.jpg"); 		
        WebUtility.Wait();
        
        // ---------------------- Inactivate Image Set ------------------------
		try {
			js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
			WebUtility.Wait();
			WebUtility.Click(imageSetActiveButton);
			WebUtility.Wait();
			WebUtility.Click(dropDownList.get(0));
			WebUtility.javascript(yes.get(0));
			WebUtility.Wait();
		}
        catch(Exception e) {
        	e.printStackTrace();
        }
		
		//------------------------ Delete Image Set -----------------------------
		try {
			removeSet.get(1).click();
			WebUtility.Wait();
			yes.get(0).click();
			WebUtility.Wait();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
        String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
        sa.assertTrue(doneButtonActiveColor.contains("255,255,255") || doneButtonActiveColor.contains("255, 255, 255"), "Quick Filing - Videos Images Mark As Done Assertion Fails");
		WebUtility.Click(doneButtonActive);	
		WebUtility.Wait();
		
		WebUtility.Click(imageSets.get(0));
		try {
			TestUtil.captureScreenshot("Video Quick Filing - Images Section Default Set","Video");
		} catch (IOException e) {
			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}

		WebUtility.Click(imageSets.get(1));
		js.executeScript("window.scrollTo(0,0);");
		try {
			TestUtil.captureScreenshot("Video Quick Filing - Images Section Created Set","Video");
		} catch (IOException e) {
			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}
    }

    public void VideoQuickFillingSeoDetails() throws InterruptedException {
        seoDetails.click();

        WebUtility.element_to_be_clickable(seoTitleTag);
        seoTitleTag.click();
        seoTitleTag.sendKeys(Keys.CONTROL + "a");
        seoTitleTag.sendKeys(Keys.BACK_SPACE);
        WebUtility.Wait();
        seoTitleTag.sendKeys("Automation Test Data");
  
        seoMetaDescription.click();
        seoMetaDescription.sendKeys(Keys.CONTROL + "a");
        seoMetaDescription.sendKeys(Keys.BACK_SPACE);
        WebUtility.Wait();
        seoMetaDescription.sendKeys("Automation Test Data");
        
        seoMetaSynopsis.click();
        seoMetaSynopsis.sendKeys(Keys.CONTROL + "a");
        seoMetaSynopsis.sendKeys(Keys.BACK_SPACE);
        WebUtility.Wait();
        seoMetaSynopsis.sendKeys("Automation Test Data");

        seoH1Heading.click();
        seoH1Heading.sendKeys(Keys.CONTROL + "a");
        seoH1Heading.sendKeys(Keys.BACK_SPACE);
        WebUtility.Wait();
        seoH1Heading.sendKeys("Automation Test Data");

        seoH2Heading.click();
        seoH2Heading.sendKeys(Keys.CONTROL + "a");
        seoH2Heading.sendKeys(Keys.BACK_SPACE);
        WebUtility.Wait();
        seoH2Heading.sendKeys("Automation Test Data");
        
        for (int i = 0; i < seoCheckboxes.size(); i++) {
            if (seoCheckboxes.get(i).isSelected() == false)
                seoCheckboxes.get(i).click();
        }
        WebUtility.Wait();
        String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
        sa.assertTrue(doneButtonActiveColor.contains("255,255,255") || doneButtonActiveColor.contains("255, 255, 255"), "Quick Filing - Video SEO Details Mark As Done Assertion Fails");
        WebUtility.Click(doneButtonActive);

        try {
            TestUtil.captureScreenshot("Video Quick Filing - SEO Details Section","Video");
        } catch (IOException e) {
            System.out.println("Screenshot Capture Failed ->" + e.getMessage());
        }

    }

    public void VideoQuickFillingMapContent() throws InterruptedException {

		WebUtility.Click(mapContent);
		WebUtility.Wait();
		WebUtility.Click(addContent);
	
	    WebUtility.element_to_be_visible(mapContentAssign.get(0));
	    checkBox.get(0).click();
	    WebUtility.Click(assignContent);
		
		WebUtility.element_to_be_clickable(doneButtonActive);
		String doneButtonActiveColor = doneButtonActive.getCssValue("background-color");
		sa.assertTrue(doneButtonActiveColor.contains("255,255,255") || doneButtonActiveColor.contains("255, 255, 255"), "Quick Filing - Video Map Content Mark As Done Assertion Fails");
		WebUtility.Click(doneButtonActive);

        try {
            TestUtil.captureScreenshot("Video Quick Filling - Map Content Section","Video");
        } catch (IOException e) {
            System.out.println("Screenshot Capture Failed ->" + e.getMessage());
        }
    }

    public void VideoQuickFillingPublishFlow() throws InterruptedException {
    	//TODO ------------------- Update when Permissions are fixed------------------------ 	
/*
        WebUtility.Click(dashboard);
        WebUtility.Click(videoButton);

        WebUtility.Click(searchString);
        searchString.sendKeys(videoQuickFilingTitle);
        WebUtility.Wait();  
		editButton.get(0).click();
        WebUtility.Click(editVideo);
        WebUtility.Wait();
		editVideoQuickFiling.click();
		yes.get(0).click();
		WebUtility.Wait();
 
		//--Verifying Video QuickFiling Draft Status
		try
		{
			videoProperties.click();
			WebUtility.element_to_be_visible(contentStatus);
			TestUtil.captureScreenshot("Video QuickFiling Draft Status","Video");
			String expectedStatus = "Draft";
			String actualStatus = contentStatus.getText();
			sa.assertEquals(actualStatus,expectedStatus,"Video QuickFiling Draft Status is Failed");
			Reporter.log("Video QuickFiling Draft Status is Passed",true);	
		}
		catch (Exception e)
		{
			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}
																				 						  
        WebUtility.Click(checklist);
        WebUtility.Wait();
        WebUtility.Click(submitToReview);
        WebUtility.Click(submitYesBtn);
        WebUtility.Click(submitOk);
        js.executeScript("window.scrollTo(0,0)");
        WebUtility.Wait();

		//--Verifying Video QuickFiling Submit to Review status
		try
		{
			videoProperties.click();
			WebUtility.element_to_be_visible(contentStatus);
			TestUtil.captureScreenshot("Video QuickFiling Submitted To Review Status","Video");
			String expectedStatus = "Submitted To Review";
			String actualStatus = contentStatus.getText();
			sa.assertEquals(actualStatus,expectedStatus,"Video QuickFiling  To Review Status is Failed");
			Reporter.log("Video QuickFiling Submitted To Review Status is Passed",true);	
		}
		catch (Exception e)
		{
			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}
		
        logoutlink.click();
        logoutbutton.click();
        WebUtility.clear_cache();
        driver.get(prop.getProperty("url"));
        loginPage1.gmailLogin(prop.getProperty("Gusername2"), prop.getProperty("Gpassword"));
        WebUtility.Wait();
        WebUtility.Click(dashboard);
        WebUtility.Click(videoButton);
        WebUtility.Wait();

        WebUtility.Click(searchString);
        searchString.sendKeys(videoQuickFilingTitle);
        WebUtility.Wait();
        WebUtility.Click(editButton.get(0));
        WebUtility.Click(editVideo);
        WebUtility.Wait();
		editVideoQuickFiling.click();
		yes.get(0).click();
		WebUtility.Wait();
        WebUtility.Click(checklist);
        WebUtility.Click(needWorkReason);
        WebUtility.Click(dropDownList.get(0));
        WebUtility.Click(needWorkBtn);
        WebUtility.Click(yes.get(0));
        WebUtility.Click(ok);
        js.executeScript("window.scrollTo(0,0)");
        WebUtility.Wait();

		//--Verifying Video QuickFiling Need Work status
		try
		{
			videoProperties.click();
			WebUtility.element_to_be_visible(contentStatus);
			TestUtil.captureScreenshot("Video QuickFiling Need Work Status","Video");
			String expectedStatus = "Need Work";
			String actualStatus = contentStatus.getText();
			sa.assertEquals(actualStatus,expectedStatus,"Video QuickFiling Need Work Status is Failed");
			Reporter.log("Video QuickFiling Need Work Status is Passed",true);	
		}
		catch (Exception e)
		{
			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}
		
        WebUtility.Click(logoutlink);
        logoutbutton.click();
        WebUtility.clear_cache();
        driver.get(prop.getProperty("url"));
        loginPage1.gmailLogin(prop.getProperty("Gusername1"), prop.getProperty("Gpassword"));
        WebUtility.Wait();
        WebUtility.Click(dashboard);
        WebUtility.Click(videoButton);
        WebUtility.Wait();
        WebUtility.Click(searchString);
        searchString.sendKeys(videoQuickFilingTitle);
        WebUtility.Wait();
        WebUtility.Click(editButton.get(0));
        WebUtility.Click(editVideo);
        WebUtility.Wait();
		editVideoQuickFiling.click();
		yes.get(0).click();
		WebUtility.Wait();
        category.click();
        WebUtility.Click(dropDownList.get(1));
        WebUtility.Wait();
        WebUtility.Click(nextButton);
        WebUtility.Click(doneButtonActive);
        WebUtility.Wait();
        WebUtility.Click(checklist);
        WebUtility.Click(submitToReview);
        WebUtility.Wait();
        WebUtility.Click(submitYesBtn);
        WebUtility.Click(submitOk);
        js.executeScript("window.scrollTo(0,0)");
        WebUtility.Wait();
        
		//--Verifying Video QuickFiling Submit to Review status
		try
		{
			videoProperties.click();
			WebUtility.element_to_be_visible(contentStatus);
			TestUtil.captureScreenshot("Video QuickFiling Submitted To Review Status","Video");
			String expectedStatus = "Submitted To Review";
			String actualStatus = contentStatus.getText();
			sa.assertEquals(actualStatus,expectedStatus,"Video QuickFiling Submitted To Review Status is Failed");
			Reporter.log("Video QuickFiling Submitted To Review Status is Passed",true);	
		}
		catch (Exception e)
		{
			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}
		
        logoutlink.click();
        logoutbutton.click();
        WebUtility.clear_cache();
        driver.get(prop.getProperty("url"));
        loginPage1.gmailLogin(prop.getProperty("Gusername2"), prop.getProperty("Gpassword"));
        */
        WebUtility.Wait();
        WebUtility.Click(dashboard);
        WebUtility.Click(videoButton);
        WebUtility.Wait();
        WebUtility.Click(searchString);
        searchString.sendKeys(videoQuickFilingTitle);
        WebUtility.Wait();
        WebUtility.Click(editButton.get(0));
        WebUtility.Click(editVideo);
        WebUtility.Wait();
		editVideoQuickFiling.click();
		yes.get(0).click();
		    
		
		
		
		
		
		
		
		
		WebUtility.Wait();
        WebUtility.Click(checklist);
        WebUtility.Wait();
        js.executeScript("window.scrollBy(0,600)");
        WebUtility.Click(publishCountry);
        WebUtility.Click(dropDownList.get(0));
        WebUtility.Click(publishContent);
        WebUtility.Click(yes.get(0));
        WebUtility.Click(ok);
        js.executeScript("window.scrollTo(0,0)");
        
		//--Verifying Video QuickFiling Published status
		try
		{
				videoProperties.click();
				WebUtility.element_to_be_visible(contentStatus);
				TestUtil.captureScreenshot("Video QuickFiling Published Status","Video");
				String expectedStatus = "Published";
				String actualStatus = contentStatus.getText();
				sa.assertEquals(actualStatus,expectedStatus,"Video QuickFiling Published Status is Failed");
				Reporter.log("Video QuickFiling Published Status is Passed",true);	
		}
		catch (Exception e)
		{
				System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}
        WebUtility.Wait();
        
        //---Republish 
   	   WebUtility.Click(videos);
   	   try {
   	   if (unlock.isEnabled())
   	   {
   		   try {
   			   		WebUtility.Click(unlock);
   			   		WebUtility.Click(yes.get(0));
   			   		WebUtility.Wait(); 	            
    		   	   dashRootVideo.sendKeys(Keys.CONTROL + "a");
       		   	   dashRootVideo.sendKeys(Keys.BACK_SPACE);
       		   	   dashRootVideo.sendKeys("Automation Test Data II");
       		   	   videoProperties.click();
       		   	   WebUtility.Wait();
       		   	   WebUtility.Click(videos);
       		   	   WebUtility.Wait();
       		   	   WebUtility.Click(doneButtonActive);
       		   	   WebUtility.Wait();
   	        } catch (Exception e2) {
   	            System.out.println("Section Unlocked");
   	        }
   	   }

   	   }
   	   catch(Exception e)
   	   {
   	   //----------- Section Unlocked ------------------
		   	   dashRootVideo.sendKeys(Keys.CONTROL + "a");
		   	   dashRootVideo.sendKeys(Keys.BACK_SPACE);
		   	   dashRootVideo.sendKeys("Automation Test Data III");
		   	   videoProperties.click();
		   	   WebUtility.Wait();
		   	   WebUtility.Click(videos);
		   	   WebUtility.Wait();
		   	   WebUtility.Click(doneButtonActive);
		   	   WebUtility.Wait();
   	   }
   		//--Verifying Video QuickFiling Changed status
   		try
   		{
   				videoProperties.click();
   				WebUtility.element_to_be_visible(contentStatus);
   				TestUtil.captureScreenshot("Video QuickFiling Changed Status","Video");
   				String expectedStatus = "Changed";
   				String actualStatus = contentStatus.getText();
   				sa.assertEquals(actualStatus,expectedStatus,"Video QuickFiling Changed Status is Failed");
   				Reporter.log("Video QuickFiling Changed Status is Passed",true);	
   		}
   		catch (Exception e)
   		{
   				System.out.println("Screenshot Capture Failed ->" + e.getMessage());
   		}
        
        WebUtility.Click(checklist);
        js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
        WebUtility.Click(republishContent); 
        WebUtility.Click(yes.get(0));
        WebUtility.Click(yes.get(1));
        WebUtility.Click(ok);
        js.executeScript("window.scrollTo(0,0)");
        WebUtility.Wait();
        
		//--Verifying Video QuickFiling Republished status
		try
		{
				videoProperties.click();
				WebUtility.element_to_be_visible(contentStatus);
				TestUtil.captureScreenshot("Video QuickFiling Republished Status","Video");
				String expectedStatus = "Published";
				String actualStatus = contentStatus.getText();
				sa.assertEquals(actualStatus,expectedStatus,"Video QuickFiling Republished Status is Failed");
				Reporter.log("Video QuickFiling Republished Status is Passed",true);	
		}
		catch (Exception e)
		{
				System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}
		
		WebUtility.Wait();
        
        //---Unpublished  
        js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
        WebUtility.Click(checklist);
        WebUtility.Click(unpublishContent);
        WebUtility.Click(unpublishReason);
        WebUtility.Click(dropDownList.get(0));
        WebUtility.Click(yes.get(0));
        WebUtility.Click(ok);
        js.executeScript("window.scrollTo(0,0)");
        WebUtility.Wait();
        
		//--Verifying Video QuickFiling UnPublished status
		try
		{
			videoProperties.click();
			WebUtility.element_to_be_visible(contentStatus);
			TestUtil.captureScreenshot("Video QuickFiling Unpublished Status","Video");
			String expectedStatus = "Unpublished";
			String actualStatus = contentStatus.getText();
			sa.assertEquals(actualStatus,expectedStatus,"Video QuickFiling Unpublished Status is Failed");
			Reporter.log("Video QuickFiling Unpublished Status is Passed",true);	
		}
		catch (Exception e)
		{
			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}
        WebUtility.Wait();
        publishedHistory.click();
        WebUtility.Wait();
        try {
            TestUtil.captureScreenshot("Video QuickFiling Content - Published History","Video");
        } catch (IOException e) {
            System.out.println("Screenshot Capture Failed ->" + e.getMessage());
        }
        WebUtility.Click(updateVideoBreadcrumb);
        WebUtility.Wait();   
         
       // Archive unpublished content -------------------------------------     
        WebUtility.Click(dashboard);
        WebUtility.Click(videoButton);
        WebUtility.Wait();
        WebUtility.Click(searchString);
        searchString.sendKeys(videoQuickFilingTitle);
        WebUtility.Wait();
		Archive.get(0).click();				
		ArchiveYes.click();
		WebUtility.Wait();
        WebUtility.Click(editButton.get(0));
		WebUtility.Wait();
		
		//--Verifying Video Quick Filing Archive status
		try
		{
			videoProperties.click();
			WebUtility.element_to_be_visible(contentStatus);
			TestUtil.captureScreenshot("Video QuickFiling Archived Status","Movie");
			String expectedStatus = "Archived";
			String actualStatus = contentStatus.getText();
			sa.assertEquals(actualStatus,expectedStatus,"Video QuickFiling Archived Status is Failed");
			Reporter.log("Video QuickFiling Archived Status is Passed",true);	
		}
		catch (Exception e)
		{
			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}
		WebUtility.Wait();
		
		// Restore Content to draft mode ---------------------------------------
        WebUtility.Click(dashboard);
        WebUtility.Click(videoButton);
        WebUtility.Wait();
        WebUtility.Click(searchString);
        searchString.sendKeys(videoQuickFilingTitle);
        WebUtility.Wait();
		WebUtility.javascript(Restore);			
		WebUtility.Wait();
		WebUtility.javascript(RestoreYes);
		WebUtility.Wait();
        WebUtility.Click(editButton.get(0));
		WebUtility.Wait();
		
		//--Verifying Video Quick Filing Restore Status
		try
		{
			videoProperties.click();
			WebUtility.element_to_be_visible(contentStatus);
			TestUtil.captureScreenshot("Video QuickFiling Restore Status","Movie");
			String expectedStatus = "Draft";
			String actualStatus = contentStatus.getText();
			sa.assertEquals(actualStatus,expectedStatus,"Video QuickFiling Restore Status is Failed");
			Reporter.log("Video QuickFiling Restore Status is Passed",true);	
		}
		catch (Exception e)
		{
			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}
		
		//------------------------- Schedule Content -----------------------------------------------------
        WebUtility.Click(checklist);
        WebUtility.Wait();
        WebUtility.Click(scheduleContent);
        js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
        
        //System.out.println("--------> Current Time = "+currentTime.getAttribute("innerText").substring(20, 28));
        //String currentTimeString  = currentTime.getAttribute("innerText").substring(20, 27);
    	//js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
        WebUtility.Wait();
        
        WebUtility.Click(scheduledPublicationTime);
        datePicker.sendKeys(Keys.ARROW_RIGHT);
        WebUtility.Click(datePickerMinutes);
        actionProvider.moveToElement(datePickerMinutesClock).click().build().perform();      
        datePicker.sendKeys(Keys.ESCAPE);

        WebUtility.Click(scheduledContentCountry);
        scheduledContentCountry.sendKeys(Keys.ARROW_DOWN);
        scheduledContentCountry.sendKeys(Keys.ENTER);

        WebUtility.Click(scheduleContentButton);
        WebUtility.Click(yes.get(0));
        WebUtility.Click(ok);

        js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
        WebUtility.Wait();
        js.executeScript("window.scrollTo(0,0);");
        WebUtility.Wait();        
     
        //System.out.println("--------> Scheduled Time = "+scheduledTime.getAttribute("innerText").substring(12, 20));
        //String scheduledTimeString = scheduledTime.getAttribute("innerText").substring(12, 20);
    	//js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
    	//sa.assertTrue(scheduledTimeString.contains(currentTimeString),"Video Quick Filing - Current Time and Scheduled Time are not Equal");
    	
		//--Verifying Video QuickFiling Scheduled status
		try
		{
			videoProperties.click();
			WebUtility.element_to_be_visible(contentStatus);
			TestUtil.captureScreenshot("Video QuickFiling Scheduled Status","Movie");
			String expectedStatus = "Scheduled";
			String actualStatus = contentStatus.getText();
			sa.assertEquals(actualStatus,expectedStatus,"Video QuickFiling Scheduled Status is Failed");
			Reporter.log("Video QuickFiling Scheduled Status is Passed",true);	
		}
		catch (Exception e)
		{
			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}
		
		//-Clone Video Content
		WebUtility.Click(dashboard);
		WebUtility.Wait();
		WebUtility.Click(videoButton);
		WebUtility.Wait();
		WebUtility.Click(searchString);
		searchString.sendKeys(videoQuickFilingTitle);
		WebUtility.Wait();
		WebUtility.Wait();		
    	WebUtility.Click(clone);
    	WebUtility.Click(yes.get(0));
    	WebUtility.Wait();
    	
    	WebUtility.Click(checklist);
        js.executeScript("window.scrollTo(0,document.body.scrollHeight)");
        WebUtility.Click(publishCountry);
        WebUtility.Click(dropDownList.get(0));

        WebUtility.Click(publishContent);
        WebUtility.Click(yes.get(0));
        WebUtility.Click(ok);
        js.executeScript("window.scrollTo(0,0)");
        
		//--Verifying Video QuickFiling Published status
		try
		{
				videoProperties.click();
				WebUtility.element_to_be_visible(contentStatus);
				TestUtil.captureScreenshot("Video QuickFiling Published Status","Video");
				String expectedStatus = "Published";
				String actualStatus = contentStatus.getText();
				sa.assertEquals(actualStatus,expectedStatus,"Video QuickFiling Published Status is Failed");
				Reporter.log("Video QuickFiling Published Status is Passed",true);	
		}
		catch (Exception e)
		{
				System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}
		
		WebUtility.Wait();
        
        js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
        WebUtility.Click(checklist);
        WebUtility.Click(unpublishContent);

        WebUtility.Click(unpublishReason);
        WebUtility.Click(dropDownList.get(0));
        WebUtility.Click(yes.get(0));
        WebUtility.Click(ok);
        js.executeScript("window.scrollTo(0,0)");
        WebUtility.Wait();
        
		//--Verifying Video QuickFiling UnPublished status
		try
		{
			videoProperties.click();
			WebUtility.element_to_be_visible(contentStatus);
			TestUtil.captureScreenshot("Video QuickFiling Unpublished Status","Video");
			String expectedStatus = "Unpublished";
			String actualStatus = contentStatus.getText();
			sa.assertEquals(actualStatus,expectedStatus,"Video QuickFiling Unpublished Status is Failed");
			Reporter.log("Video QuickFiling Unpublished Status is Passed",true);	
		}
		catch (Exception e)
		{
			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}
        WebUtility.Wait();
        
        publishedHistory.click();
        WebUtility.Wait();
        try {
            TestUtil.captureScreenshot("Clone Video - Published History","Video");
        } catch (IOException e) {
            System.out.println("Screenshot Capture Failed ->" + e.getMessage());
        }

    }

    

    //--------------------------- End of Quick Filling -----------------------------------------------------------   

    
    public void VideoListingSearch() throws InterruptedException,Exception {	
/*
		// -------- Edit Flow For Testing Purposes ----------------------------

		WebUtility.Click(dashboard);
		WebUtility.Click(videoButton);

		searchString.click();
		searchString.sendKeys(videoTitle);
		WebUtility.Wait();
		WebUtility.Wait();
		WebUtility.element_to_be_clickable(editButton.get(0));
		editButton.get(0).click();
		WebUtility.Wait();
		externalID = js.executeScript("return arguments[0].childNodes[1].innerText", videoExternalID).toString();
*/	
		WebUtility.Click(dashboard);
		WebUtility.Click(videoButton);
		
		WebUtility.Click(searchString);
		searchString.sendKeys(videoTitle.toLowerCase());
		WebUtility.Wait();
		WebUtility.Wait();
		
		WebUtility.Click(searchString);
		searchString.sendKeys(Keys.CONTROL+"a");
		searchString.sendKeys(Keys.BACK_SPACE);
		searchString.sendKeys(externalID);
		WebUtility.Wait();
		WebUtility.Wait();
		
		try {
			TestUtil.captureScreenshot("Video Search", "Video");
		} catch (IOException e) {
			System.out.println("Screenshot Capture Failed ->" + e.getMessage());
		}

		
	}
    
    
    //--------------------- Videos Listing Page Test -----------------------------------------------------------
	public void VideoListingPage() throws InterruptedException,Exception{
        dashboard.click();
        WebUtility.Wait();
        videoButton.click();
        WebUtility.Wait();	
	
		WebUtility.Click(searchString);
		searchString.sendKeys(videoTitle);
		WebUtility.Wait();
		WebUtility.Wait();
		
		WebUtility.Click(listingExpiringLicense);
		WebUtility.Wait();
		Thread.sleep(3000);
		WebUtility.Click(listingBackArrow);
		WebUtility.Wait();
		
		WebUtility.Click(searchString);
		searchString.sendKeys(videoTitle);
		WebUtility.Wait();
		WebUtility.Wait();
		
		WebUtility.Click(listingLinkedVideos);
		WebUtility.Wait();
		Thread.sleep(3000);
		WebUtility.Click(listingBackArrow);
		WebUtility.Wait();
		
		WebUtility.Click(searchString);
		searchString.sendKeys(videoTitle);
		WebUtility.Wait();
		WebUtility.Wait();
		
		WebUtility.Click(listingPublishedHistory);
		WebUtility.Wait();
		Thread.sleep(3000);
		WebUtility.Click(listingClosePublishedHistory);
		WebUtility.Wait();
		
		try {
		WebUtility.Click(listingLicenseCountries);
		WebUtility.Wait();
		WebUtility.Click(listingCloseLicenseCountries);
		WebUtility.Wait();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
		listingDraftStatus.click();
		WebUtility.Wait();
		Thread.sleep(3000);
		
		listingAllStatus.click();
		WebUtility.Wait();
		Thread.sleep(3000);
		
		listingChangedStatus.click();
		WebUtility.Wait();
		Thread.sleep(3000);
		
		listingPublishedStatus.click();
		WebUtility.Wait();
		Thread.sleep(3000);
		
		listingUnpublishedStatus.click();
		WebUtility.Wait();
		Thread.sleep(3000);
		
		listingNeedWorkStatus.click();
		WebUtility.Wait();
		Thread.sleep(3000);
		
		listingScheduledStatus.click();
		WebUtility.Wait();
		Thread.sleep(3000);
		
		listingSubmittedToReviewStatus.click();
		WebUtility.Wait();
		Thread.sleep(3000);
		
		listingArchivedStatus.click();
		WebUtility.Wait();
		Thread.sleep(3000);
		
		listingPublishingQueueStatus.click();
		WebUtility.Wait();
		Thread.sleep(3000);
		
	}
	
	
	public void AssertionResults() throws Exception{
		sa.assertAll();
	}
	
    //------------------ Writes Caught exceptions to an Excel file, ADD this to TestBase ------------------------ 
    public void WriteDataToExcel(HashMap < Integer, Object[] > errorInfo) throws Exception {

        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet spreadsheet = workbook.createSheet("Failed Tests");
        XSSFRow row;
        Set < Integer > keyid = errorInfo.keySet();

        int rowid = 0;
        for (Integer key: keyid) {
            row = spreadsheet.createRow(rowid++);
            Object[] objectArr = errorInfo.get(key);
            int cellid = 0;
            for (Object obj: objectArr) {
                Cell cell = row.createCell(cellid++);
                cell.setCellValue((String) obj);
                int columnIndex = cell.getColumnIndex();
                spreadsheet.autoSizeColumn(columnIndex);
            }
        }
        File desktopDir = new File(System.getProperty("user.home"), "Desktop");
        FileOutputStream out = new FileOutputStream(new File(desktopDir, "Failed Tests.xlsx"));
        workbook.write(out);
        out.close();
        workbook.close();

    }

    

}
