package com.qa.utilities;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.ElementClickInterceptedException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.zee5.qa.base.TestBase;


	

	public class WebUtility extends TestBase{
		
		public static WebDriverWait wait;
		
		public static void javascript(WebElement element) {
			try {
			JavascriptExecutor js = (JavascriptExecutor)driver;
			js.executeScript("arguments[0].click()", element);	
		}
			
			catch (Exception e) {
				System.out.println(e);
				log.info(e.getMessage());

			}}
		
		/*
		 * 
		 * 
		 * Explicit Wait till the element clickable
		 */
		
		public static void element_to_be_clickable(WebElement element) {
			try {
			wait = new WebDriverWait(driver,30);
			  wait.until(ExpectedConditions.elementToBeClickable(element));
			
		}
			catch (Exception e) {
				System.out.println(e);
				log.info(e.getMessage());

			}
		}
		
		public static void Click(WebElement element) throws InterruptedException{
			int count = 0;
			while(count<10)
			{			
				try {
					wait = new WebDriverWait(driver,20);
					wait.until(ExpectedConditions.visibilityOf(element)).click();
					break;
				}
				catch (ElementClickInterceptedException|IndexOutOfBoundsException|NoSuchElementException|StaleElementReferenceException e) {
					Thread.sleep(2000);
					count++;
				}
			}
		
		}
	
		public static void Wait() throws InterruptedException{

			try {
				driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
				WebElement loader = driver
						.findElement(By.xpath("//div[contains(@role,'progress') or contains(@class,'loader')]"));
				if (loader.isDisplayed()) {
					wait = new WebDriverWait(driver, 10);
					wait.until(ExpectedConditions.stalenessOf(loader));
				}
			} catch (Exception e) {
				Thread.sleep(1000);
			}
		}
		
		
		public static void SeasonWait(WebElement element) throws InterruptedException{
			try {
				driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
				wait = new WebDriverWait(driver,3);
				wait.until(ExpectedConditions.stalenessOf(element));
		
				
			}
			catch (Exception e) {
				System.out.println("SeasonWait() - Do Nothing");
		}
	}
		
	    /*
	     * 
	     * Element to be visible
	     * 
	     * 
	     */
		
		public static void element_to_be_visible(WebElement element) {
			try {
			wait = new WebDriverWait(driver,30);
			  wait.until(ExpectedConditions.visibilityOf(element));
		}
			catch (Exception e) {
				System.out.println(e);
				log.info(e.getMessage());

			}}
		public static void clear_cache() throws InterruptedException {
			   Capabilities cap = ((RemoteWebDriver) driver).getCapabilities();
			    String browserName = cap.getBrowserName().toLowerCase();
			    System.out.println("--------------Browser Name-----------"+browserName);
			    if(browserName.contains("chrome"))
			    {
					driver.manage().deleteAllCookies();
					driver.get("chrome://settings/clearBrowserData");

					driver.switchTo().activeElement();
					driver.findElement(By.xpath("//settings-ui")).sendKeys(Keys.TAB);

					driver.findElement(By.xpath("//settings-ui")).sendKeys(Keys.TAB);

					driver.findElement(By.xpath("//settings-ui")).sendKeys(Keys.TAB);

					driver.findElement(By.xpath("//settings-ui")).sendKeys(Keys.TAB);

					driver.findElement(By.xpath("//settings-ui")).sendKeys(Keys.TAB);

					driver.findElement(By.xpath("//settings-ui")).sendKeys(Keys.TAB);

					driver.findElement(By.xpath("//settings-ui")).sendKeys(Keys.TAB);
					Thread.sleep(1000);
					driver.findElement(By.xpath("//settings-ui")).sendKeys(Keys.ENTER);
			    }
			    else  if(browserName.contains("firefox"))
			    {
			    	Actions action = new Actions(driver);
			    	driver.manage().deleteAllCookies();
			    	driver.get("about:preferences#privacy");
			    	Thread.sleep(2000);
			    	driver.switchTo().activeElement();
			    	int i=0;
			    	while(i<10)
			    	{
			    	 	action.sendKeys(Keys.TAB).perform();
			    		i++;
			    	}	
			    	action.sendKeys(Keys.ENTER).perform();
			    	action.sendKeys(Keys.TAB).perform();
			    	action.sendKeys(Keys.TAB).perform();	
			    	action.sendKeys(Keys.ENTER).perform();	
			    	driver.switchTo().alert();
			    	action.sendKeys(Keys.ENTER).perform();
			    	
			 
	
			    }
			    else {
			    	//---- Do Nothing ------
			    }
		}
		
		public static void element_to_be_fluentwait(WebElement element) {
		Wait wait = new FluentWait<WebDriver>(driver)
				.withTimeout(50, TimeUnit.SECONDS)
				.pollingEvery(2, TimeUnit.SECONDS)
				.ignoring(NoSuchElementException.class,ElementClickInterceptedException.class);					
	}}
	

