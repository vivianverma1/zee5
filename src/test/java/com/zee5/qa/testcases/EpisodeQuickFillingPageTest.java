package com.zee5.qa.testcases;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.zee5.qa.base.TestBase;
import com.zee5.qa.pages.EpisodeQuickfillingPage;
import com.zee5.qa.pages.LoginPage;
@Listeners(com.zee5.qa.util.Listener.class)
public class EpisodeQuickFillingPageTest extends TestBase {
	EpisodeQuickfillingPage episodeQuickfilingpage;
	LoginPage loginPage;

	
	public EpisodeQuickFillingPageTest(){
		super();
	}
	
	@Parameters({"browserType"})
	@BeforeTest
	public void setup(@Optional String browser) throws Exception{
/*
		try {
		if(browser.equalsIgnoreCase("chrome"))
		{
		System.out.println("Initialize Chrome");
		initializeExtentReport("Chrome");
		initialization("chrome");
		loginPage = new LoginPage();
		loginPage.gmailLogin(prop.getProperty("Gusername"),prop.getProperty("adminPass"));
		}
		else if(browser.equalsIgnoreCase("firefox"))
		{	
		System.out.println("Initialize Firefox");
		initializeExtentReport("Firefox");
		initialization("firefox");
		loginPage = new LoginPage();
		loginPage.gmailLogin(prop.getProperty("Gusername"),prop.getProperty("adminPass"));
		}
		}
		catch(Exception e)
		{
		initializeExtentReport("Firefox");
		initialization("firefox");
		loginPage = new LoginPage();
		loginPage.gmailLogin(prop.getProperty("Gusername"),prop.getProperty("adminPass"));
		}*/
		episodeQuickfilingpage = new EpisodeQuickfillingPage();
	}
	
	@Test(priority = 0)
	public void EpisodeQuickfilingProperties() throws Exception {
		logger = report.createTest("Episode Quick Filing - Properties Test");
		episodeQuickfilingpage.QuickFillingEpisodeProperties();
		
	}

	@Test(priority = 1)
	public void EpisodeQuickfilingVideoSection() throws Exception {
		logger = report.createTest("Episode Quick Filing - Video Section Test");
		episodeQuickfilingpage.QuickFillingEpisodeVideoSection();	
	}

	@Test(priority = 2)
	public void EpisodeLicense() throws Exception {
		logger = report.createTest("Episode Quick Filing - Cast Crew Test");
		episodeQuickfilingpage.quickfillingEpisodeLicense();	
	}

	@Test(priority = 3)
	public void EpisodeImages() throws Exception {
		logger = report.createTest("Episode Quick Filing - Images Test");
		episodeQuickfilingpage.quickfillingImages();	
	}
	
	@Test(priority = 4)
	public void EpisodeSeoDetails() throws Exception {
		logger = report.createTest("Episode Quick Filing - Seo Details Test");
		episodeQuickfilingpage.quickfillingSeo();	
	}
	
	@Test(priority = 5)
	public void EpisodePublish() throws Exception {
		logger = report.createTest("Episode Quick Filing - Publish Flow Test");
		episodeQuickfilingpage.EpisodePublishFlow();	
	}

	@Test(priority = 6)
	public void EpisodeAssertions() throws Exception {
		logger = report.createTest("Episode Quick Filing - Assertions Test");
		episodeQuickfilingpage.EpisodeAssertionResults();	
	}

@AfterTest
	public void tearDown() throws Exception {
	}


}
