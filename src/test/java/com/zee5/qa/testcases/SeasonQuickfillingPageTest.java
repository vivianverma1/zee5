package com.zee5.qa.testcases;

	import org.testng.annotations.AfterTest;
	import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
	import com.zee5.qa.base.TestBase;
	import com.zee5.qa.pages.LoginPage;
	import com.zee5.qa.pages.SeasonsPage;
	@Listeners(com.zee5.qa.util.Listener.class)

	public class SeasonQuickfillingPageTest extends TestBase {
		SeasonsPage seasonspage;
		LoginPage loginPage;

			
		public SeasonQuickfillingPageTest(){
			super();
		}
		
		@Parameters({"browserType"})
		@BeforeTest
		public void setup(@Optional String browser) throws Exception{
/*
			try {
			if(browser.equalsIgnoreCase("chrome"))
			{
			System.out.println("Initialize Chrome");
			initializeExtentReport("Chrome");
			initialization("chrome");
			loginPage = new LoginPage();
			loginPage.gmailLogin(prop.getProperty("Gusername"),prop.getProperty("adminPass"));
			}
			else if(browser.equalsIgnoreCase("firefox"))
			{	
			System.out.println("Initialize Firefox");
			initializeExtentReport("Firefox");
			initialization("firefox");
			loginPage = new LoginPage();
			loginPage.gmailLogin(prop.getProperty("Gusername"),prop.getProperty("adminPass"));
			}
			}
			catch(Exception e)
			{
			initializeExtentReport("Firefox");
			initialization("firefox");
			loginPage = new LoginPage();
			loginPage.gmailLogin(prop.getProperty("Gusername"),prop.getProperty("adminPass"));
			}*/
			seasonspage = new SeasonsPage();
		}

		@Test(priority = 0)
		public void SeasonQuickFilingProperties() throws Exception {
			logger = report.createTest("Season Quick Filing - Properties Test");
			seasonspage.SeasonQuickfilingProperties();
			
		}
		
		@Test(priority = 1)
		public void SeasonQuickFilingLicense() throws Exception {
			logger = report.createTest("Season Quick Filing - Season Cast Crew Test");
			seasonspage.SeasonQuickfilingLicense();	
		}

		@Test(priority = 2)
		public void SeasonQuickFilingImages() throws Exception {
			logger = report.createTest("Season Quick Filing - Images Test");
			seasonspage.SeasonQuickfilingImages();	
		}
		
		@Test(priority = 3)
		public void SeasonQuickFilingSeoDetails() throws Exception {
			logger = report.createTest("Season Quick Filing - Seo Details Test");
			seasonspage.SeasonQuickfillingSeo();	
		}
		
		@Test(priority = 4)
		public void SeasonQuickFilingRelatedContent() throws Exception {
			logger = report.createTest("Season Quick Filing - Related Content Test");
			seasonspage.SeasonsRelatedContent();;	
		}
		
		@Test(priority = 5)
		public void SeasonQuickFilingPublish() throws Exception {
			logger = report.createTest("Season Quick Filing -  Publish Test");
			seasonspage.SeasonQuickfilingPublishFlow();	
		}

		@Test(priority = 6)
		public void SeasonQuickFilingAssertion() throws Exception {
			logger = report.createTest("Season Quick Filing - Assertion Results");
			seasonspage.SeasonAssertionResults();	
		}
	@AfterTest
		public void tearDown() throws Exception {
			
		}
		

	}


