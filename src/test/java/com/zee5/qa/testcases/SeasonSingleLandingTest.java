package com.zee5.qa.testcases;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.zee5.qa.base.TestBase;
import com.zee5.qa.pages.LoginPage;
import com.zee5.qa.pages.SeasonsPage;
@Listeners(com.zee5.qa.util.Listener.class)
public class SeasonSingleLandingTest extends TestBase{
	SeasonsPage seasonspage;
	LoginPage loginPage;
	
	public SeasonSingleLandingTest(){
		super();
	}
	
	@Parameters({"browserType"})
	@BeforeTest
	public void setup(@Optional String browser) throws Exception{
/*
		try {
		if(browser.equalsIgnoreCase("chrome"))
		{
		System.out.println("Initialize Chrome");
		initializeExtentReport("Chrome");
		initialization("chrome");
		loginPage = new LoginPage();
		loginPage.gmailLogin(prop.getProperty("Gusername"),prop.getProperty("adminPass"));
		}
		else if(browser.equalsIgnoreCase("firefox"))
		{	
		System.out.println("Initialize Firefox");
		initializeExtentReport("Firefox");
		initialization("firefox");
		loginPage = new LoginPage();
		loginPage.gmailLogin(prop.getProperty("Gusername"),prop.getProperty("adminPass"));
		}
		}
		catch(Exception e)
		{
		initializeExtentReport("Firefox");
		initialization("firefox");
		loginPage = new LoginPage();
		loginPage.gmailLogin(prop.getProperty("Gusername"),prop.getProperty("adminPass"));
		}*/
		seasonspage = new SeasonsPage();
	}

	@Test(priority = 0)
	public void SeasonSingleLandingProperties() throws Exception {
		logger = report.createTest("Season Single Landing - Properties Test");
		seasonspage.SeasonSingleLandingProperties();		
	}

	@Test(priority = 1)
	public void SeasonSingleLandingCastCrew() throws Exception {
		logger = report.createTest("Season Single Landing - Cast Crew Test");
		seasonspage.SeasonSingleLandingCastCrew();		
	}
	
	@Test(priority = 2)
	public void SeasonSingleLandingLicense() throws Exception {
		logger = report.createTest("Season Single Landing - License Test");
		seasonspage.SeasonSingleLandingLicense();		
	}
	
	@Test(priority = 3)
	public void SeasonSingleLandingImages() throws Exception {
		logger = report.createTest("Season Single Landing - Images Test");
		seasonspage.SeasonSingleLandingImages();		
	}
	
	@Test(priority = 4)
	public void SeasonSingleLandingSeoDetails() throws Exception {
		logger = report.createTest("Season Single Landing - SEO Details Test");
		seasonspage.SeasonSingleLandingSeoDetails();		
	}
	
	@Test(priority = 5)
	public void SeasonSingleLandingRelatedContent() throws Exception {
		logger = report.createTest("Season Single Landing - Related Content Test");
		seasonspage.SeasonsSingleLandingRelatedContent();		
	}
	
	@Test(priority = 6)
	public void SeasonSingleLandingPublish() throws Exception {
		logger = report.createTest("Season Single Landing - Publish Test");
		seasonspage.SeasonsSingleLandingPublishFlow();		
	}

	@Test(priority = 7)
	public void SeasonSingleLandingAssertions() throws Exception {
		logger = report.createTest("Season Single Landing - Assertions");
		seasonspage.SeasonAssertionResults();		
	}
@AfterTest
	public void tearDown() throws Exception {
	}
}

