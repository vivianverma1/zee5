package com.zee5.qa.testcases;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.zee5.qa.base.TestBase;
import com.zee5.qa.pages.LoginPage;
import com.zee5.qa.pages.TvShowsPage;
@Listeners(com.zee5.qa.util.Listener.class)

public class TvShowsSingleLandingTest extends TestBase {
	TvShowsPage tvshowspage;
	LoginPage loginPage;

	public TvShowsSingleLandingTest() {
		super();
	}

	@Parameters({"browserType"})
	@BeforeTest
	public void setup(@Optional String browser) throws Exception{
/*
		try {
		if(browser.equalsIgnoreCase("chrome"))
		{
		System.out.println("Initialize Chrome");
		initializeExtentReport("Chrome");
		initialization("chrome");
		loginPage = new LoginPage();
		loginPage.gmailLogin(prop.getProperty("Gusername"),prop.getProperty("adminPass"));
		}
		else if(browser.equalsIgnoreCase("firefox"))
		{	
		System.out.println("Initialize Firefox");
		initializeExtentReport("Firefox");
		initialization("firefox");
		loginPage = new LoginPage();
		loginPage.gmailLogin(prop.getProperty("Gusername"),prop.getProperty("adminPass"));
		}
		}
		catch(Exception e)
		{
		initializeExtentReport("Firefox");
		initialization("firefox");
		loginPage = new LoginPage();
		loginPage.gmailLogin(prop.getProperty("Gusername"),prop.getProperty("adminPass"));
		}
		*/
		tvshowspage = new TvShowsPage();
	}

	@Test(priority = 0)
	public void TvPropertiesSingleLanding() throws Exception {
		logger = report.createTest("TV Shows Single Landing - Properties Test");
		tvshowspage.TvPropertiesSingleLanding();		
	}
	
	@Test(priority = 1)
	public void TvCastCrewSingleLanding() throws Exception{
		 logger=report.createTest("TV Shows Single Landing - Shows Cast and Crew Test");
		 tvshowspage.TvCastCrewSingleLanding();
	}
	

	@Test(priority = 2)
	public void TvLicenseSingleLanding() throws Exception {
		logger = report.createTest("TV Shows Single Landing - Shows License Test");
		tvshowspage.TvLicenseSingleLanding();
	}

	@Test(priority = 3)
	public void TvImagesSingleLanding() throws Exception {
		logger = report.createTest("TV Shows Single Landing - Images Test");
		tvshowspage.TvImagesSingleLanding();
	}

	@Test(priority = 4)
	public void TvSeoDetailsSingleLanding() throws Exception {
		logger = report.createTest("TV Shows Single Landing - SEO Details Test");
		tvshowspage.TvSeoDetailsSingleLanding();
	}

	@Test(priority = 5)
	public void TvMapContentSingleLanding() throws Exception {
		logger = report.createTest("TV Shows Single Landing - Map Content Test");
		tvshowspage.TvMapContentSingleLanding();
	}
	
	@Test(priority = 6)
	public void TvRelatedContentSingleLanding() throws Exception {
		logger = report.createTest("TV Shows Single Landing - Related Content Test");
		tvshowspage.TvRelatedContent();
	}

	@Test(priority = 7)
	public void TvSingleLandingPublishFlow() throws Exception {
		logger = report.createTest("TV Shows Single Landing - Publish Flow Test");
		tvshowspage.TvSingleLandingPublishFlow();
	}
	
	@Test(priority = 8)
	public void TvSingleLandingAssertions() throws Exception {
		logger = report.createTest("TV Shows Single Landing -  Assertions");
		tvshowspage.TVAssertionResults();
	}

	@AfterTest

	public void tearDown() throws Exception {
	}

}
