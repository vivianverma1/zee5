package com.zee5.qa.testcases;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import com.zee5.qa.base.TestBase;
import com.zee5.qa.pages.LoginPage;
import com.zee5.qa.pages.TvShowsPage;
import com.zee5.qa.util.SendMail;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

@Listeners(com.zee5.qa.util.Listener.class)
public class SuiteTest extends TestBase {

	SendMail sendmail;
	LoginPage loginPage;
	TvShowsPage tvshowspage;
	// HashMap<Integer, Object[]> errorInfo = new HashMap<Integer, Object[]>();

	public SuiteTest() {
		super();
	}

	@Parameters({"browserType"})
	@BeforeTest
	public void setup(@Optional String browser) throws Exception{

		try {
		if(browser.equalsIgnoreCase("chrome"))
		{
		System.out.println("Initialize Chrome");
		initializeExtentReportForSuiteTest("Chrome");
		initialization("chrome");
		loginPage = new LoginPage();
		loginPage.gmailLogin(prop.getProperty("Gusername"),prop.getProperty("adminPass"));
		}
		else if(browser.equalsIgnoreCase("firefox"))
		{	
		System.out.println("Initialize Firefox");
		initializeExtentReportForSuiteTest("Firefox");
		initialization("firefox");
		loginPage = new LoginPage();
		loginPage.gmailLogin(prop.getProperty("Gusername"),prop.getProperty("adminPass"));
		}
		}
		
		//IF not running through suite , set your desired browser here 
		catch(Exception e)
		{
		initializeExtentReportForSuiteTest("Firefox");
		initialization("firefox");
		loginPage = new LoginPage();
		loginPage.gmailLogin(prop.getProperty("Gusername"),prop.getProperty("adminPass"));
		}
		
		tvshowspage = new TvShowsPage();		
	}

	@Test(priority = 0)
	public void TvShowVideoProperties() throws Exception {
		logger = report.createTest("TV Shows Main Content - Properties Test");
		tvshowspage.TvProperties();
	}

	@Test(priority = 1)
	public void TvShowVideoPropertiesAssertions() throws Exception {
		logger = report.createTest("TV Shows Main Content - Video Properties Assertions Test");
		tvshowspage.TvPropertiesAssertions();
	}

	@Test(priority = 2)
	public void TvCastCrew() throws Exception {
		logger = report.createTest("TV Shows Main Content - Cast & Crew Test");
		tvshowspage.TvCastCrew();
	}
	
	@Test(priority = 3)
	public void TvShowCastCrewAssertions() throws Exception {
		logger = report.createTest("TV Shows Main Content - Cast & Crew Assertions Test");
		tvshowspage.TvCastCrewAssertions();
	}

	@Test(priority = 4)
	public void TvShowLicense() throws Exception {
		logger = report.createTest("TV Shows Main Content - License Test");
		tvshowspage.TvLicense();
	}

	@Test(priority = 5)
	public void TvShowImages() throws Exception {
		logger = report.createTest("TV Shows Main Content - Images Test");
		tvshowspage.TvImages();
	}

	@Test(priority = 6)
	public void TvShowImagesAssertions() throws Exception {
		logger = report.createTest("TV Shows Main Content - Images Assertions Test");
		tvshowspage.TvImagesAssertions();
		
	}

	@Test(priority = 7)
	public void TvShowSeoDetails() throws Exception {
		logger = report.createTest("TV Shows Main Content - SEO Details Test");
		tvshowspage.TvSeoDetails();
	}

	@Test(priority = 8)
	public void TvShowSeoDetailsAssertions() throws Exception {
		logger = report.createTest("TV Shows Main Content - SEO Details Assertions Test");
		tvshowspage.TvSeoDetailsAssertions();
	}

	@Test(priority = 9)
	public void TvShowMapContent() throws Exception {
		logger = report.createTest("TV Shows Main Content - Map Content Test");
		tvshowspage.TvMapContent();
	}

	@Test(priority = 10)
	public void TvMapContentAssertions() throws Exception {
		logger = report.createTest("TV Shows Main Content - Map Content Assertions Test");
		tvshowspage.TvMapContentAssertions();
	}

	@Test(priority = 11)
	public void TvShowRelatedContent() throws Exception {
		logger = report.createTest("TV Shows Main Content - Related Content Test");
		tvshowspage.TvRelatedContent();
	}

	@Test(priority = 12)
	public void TvTranslations() throws Exception {
		logger = report.createTest("TV Shows Main Content - Translations Test");
		tvshowspage.TvTranslations();
	}	

	@Test(priority = 13)
	public void TvPublishFlow() throws Exception {
		logger = report.createTest("TV Shows Main Content - Publish Flow Test");
		tvshowspage.TvPublishFlow();
	}
	

	@Test(priority = 14)
	public void TvShowScheduleContent() throws Exception {
		logger = report.createTest("TV Shows Main Content - Schedule Content Test");
		tvshowspage.TvScheduleContent();
	}

	@Test(priority = 15)
	public void TvShowSort() throws Exception {
		logger = report.createTest("TV Shows Sort Test");
		tvshowspage.TvShowSort();
	}

	@Test(priority = 16)
	public void TvShowFilters() throws Exception {
		logger = report.createTest("TV Shows Filters Test");
		tvshowspage.TvShowFilter();
	}

	@Test(priority = 17)
	public void TvShowListingPage() throws Exception {
		logger = report.createTest(" TV Shows Listing Page Test");
		tvshowspage.TvListingPage();
	}

	@Test(priority = 18)
	public void TvShowListingSearch() throws Exception {
		logger = report.createTest(" TV Shows Listing Search Test");
		tvshowspage.TvListingSearch();
	}

	@Test(priority = 19)
	public void TVAssertionResults() throws Exception {
		logger = report.createTest("TV Shows Assertion Results");
		tvshowspage.TVAssertionResults();		
	}

	
	@AfterTest
	public void flushReport() throws Exception {
		report.flush();
	}

	@AfterSuite
	public void tearDown() throws Exception {
		Thread.sleep(3000);
		sendmail=new SendMail();
		sendmail.sendMail();
		driver.quit();
	}
}
