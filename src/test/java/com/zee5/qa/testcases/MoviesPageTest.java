package com.zee5.qa.testcases;

import java.net.URL;

import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.zee5.qa.pages.LoginPage;
import com.zee5.qa.pages.MoviesPage;
import com.zee5.qa.util.SendMail;
@Listeners(com.zee5.qa.util.Listener.class)
public class MoviesPageTest extends com.zee5.qa.base.TestBase{
	
	SendMail sendmail;
	LoginPage loginPage;
	MoviesPage moviepage;

	public MoviesPageTest(){		
		super();
	}
	
	@Parameters({"browserType"})
	@BeforeTest
	public void setup(@Optional String browser) throws Exception{
/*
		try {
		if(browser.equalsIgnoreCase("chrome"))
		{
		System.out.println("Initialize Chrome");
		initializeExtentReport("Chrome");
		initialization("chrome");
		loginPage = new LoginPage();
		loginPage.gmailLogin(prop.getProperty("Gusername"),prop.getProperty("adminPass"));
		}
		else if(browser.equalsIgnoreCase("firefox"))
		{	
		System.out.println("Initialize Firefox");
		initializeExtentReport("Firefox");
		initialization("firefox");
		loginPage = new LoginPage();
		loginPage.gmailLogin(prop.getProperty("Gusername"),prop.getProperty("adminPass"));
		}
		}
		
		//IF not running through suite , set your desired browser here 
		catch(Exception e)
		{
		initializeExtentReport("Chrome");
		initialization("chrome");
		loginPage = new LoginPage();
		loginPage.gmailLogin(prop.getProperty("Gusername"),prop.getProperty("adminPass"));
		}
*/
		moviepage= new MoviesPage();
	
	}

	@Test(priority=0)
	public void loginVerify() throws InterruptedException{
		logger = report.createTest("Login Test");
		moviepage.LoginVerify();
		
		String expectedUrl="https://qa-zee5cms.kelltontech.net";
        String Actual_Url=moviepage.originalUrl;
        System.out.println("Expected URL : "+expectedUrl);
        System.out.println("Actual URL : "+Actual_Url);
        
        
      
       Assert.assertTrue(Actual_Url.contains(expectedUrl), "Url of the website do not match");
       System.out.println("Logged in successfully");
	} 
/*
	//---------cast and master -------------
	@Test(priority=1)
	public void castProfileandMaster() throws InterruptedException {
		moviepage.MasterAndCastProfile();
		System.out.println("Added data in Master successfully");
	}

	//-----------Movie Created ------------
	@Test(priority=2)
	public void moviepage() throws InterruptedException {
		logger = report.createTest("Movie Main Content - Content Properties Test");
		moviepage.CreateMovie();
		Thread.sleep(2000);
		System.out.println("Movie Content properties Test passed");
	}

	@Test(priority=3)
	public void moviePropertiesAssertion() throws InterruptedException {
		logger = report.createTest("Movies Main Content - Content Properties Assertion");
		moviepage.MoviesContentPropertiesAssertions();
		System.out.println("Movie Content properties Assertion passed");
	}

	@Test(priority=4)
	public void castcrewpage() throws InterruptedException {
		logger = report.createTest("Movies Main Content - Cast and Crew Test");
		moviepage.CastCrew();
		System.out.println("Movie Cast and Crew Test passed");
	}

	@Test(priority=5)
	public void castcrewpageAssertion() throws InterruptedException {
		logger = report.createTest("Movies Main Content - Cast and Crew Assertion");
		moviepage.MoviesCastCrewAssertions();
		System.out.println("Movie Cast and Crew Assertion passed");
		
	}

	@Test(priority=6)
	public void video() throws InterruptedException {
		logger = report.createTest("Movies Main Content - Video section Test");
		moviepage.Video();
		System.out.println("Movie Video Section Test passed");
	}

	@Test(priority=7)
	public void videoAssertion() throws InterruptedException {
		logger = report.createTest("Movies Main Content - Video Assertion Test");
		moviepage.MoviesVideoSectionAssertions();
		System.out.println("Movie Video Assertion passed");
	}

	@Test(priority=8)
	public void licensepage() throws InterruptedException {
		logger = report.createTest("Movies Main Content - License Section Test");
		moviepage.License();
		System.out.println("Movie License section Test passed");
	} 

	@Test(priority=9)
	public void imagespage() throws InterruptedException {
		logger = report.createTest("Movies Main Content - Image Section Test");
		moviepage.Images();
		System.out.println("Movie Image section Test passed");
	}
		
	@Test(priority=10)
	public void imagespageAssertion() throws InterruptedException {
		logger = report.createTest("Movies Main Content - Image Assertion Test");
		moviepage.MoviesImagesAssertions();
		System.out.println("Movie Image Assertion passed");
	}

	@Test(priority=11)
	public void seopage() throws InterruptedException {
		logger = report.createTest("Movies Main Content - Seo Section Test");
		moviepage.SEO();
		System.out.println("Movie Seo section Test passed");
	}
	
	@Test(priority=12)
	public void seopageAssertion() throws InterruptedException {
		logger = report.createTest("Movies Main Content - Seo Assertion Test");
		moviepage.MoviesSeoDetailsAssertions();
		System.out.println("Movie SEO Assertion passed");
	}

	@Test(priority=13)
	public void mapcontentpage() throws InterruptedException {
		logger = report.createTest("Movies Main Content - Map content Section Test");
		moviepage.Mapcontent();
		System.out.println("Movies Map Content section Test passed");
	}
	
	@Test(priority=14)	 
		public void mapcontentpageAssertion() throws InterruptedException {
		logger = report.createTest("Movies Main Content - Map Content Assertion Test");
		moviepage.MovieMapContentAssertions();
		System.out.println("Movie Map Assertion passed");
		
	}
	
	@Test(priority=15)
	public void quicklink() throws InterruptedException {
		logger = report.createTest("Movies Main Content - Quicklink Test");
		moviepage.Quicklink();
		System.out.println("Movie QuickLink Test passed");
	}

	@Test(priority=16)
	public void checklistpage() throws InterruptedException {
		logger = report.createTest("Movies Main Content - Checklist Test");
		moviepage.Checklist();
		System.out.println("Movie CheckList Test passed");
	}

	@Test(priority=17)
	public void workflowpage() throws InterruptedException {
		logger = report.createTest("Movies Main Content - Workflow Test");
		moviepage.Workflow();
		System.out.println("Movie Workflow Test passed");
	} 

	@Test(priority=18)
	public void Clone() throws InterruptedException {
		logger = report.createTest("Movies Main Content - Clone Test");
		moviepage.Clone();
		System.out.println("Movie Clone Test passed");
	}
	
	@Test(priority=19)
	public void MoviesListingPageTest() throws InterruptedException {
		logger = report.createTest("Movies Listing Page Test");
		moviepage.MovieListingPage();
		System.out.println("Movie Listing Page Test passed");
	}

	@Test(priority = 20)
	public void MovieListingSearch() throws Exception {
		logger = report.createTest("Movies Listing Page Search Test");
		moviepage.MovieListingSearch();
	}
/*
// Quick Filling ------------------------------------------------------	

	@Test(priority=21)
	public void quickfilling() throws InterruptedException {
		logger = report.createTest("Movies Quick Filing - Properties Test");
		moviepage.QuickFilling();
		System.out.println("Movie QuickFilling Properties Test passed");
	}

	@Test(priority=22)
	public void quickvideo() throws InterruptedException {
		logger = report.createTest("Movies Quick Filing - Video Section Test");
		moviepage.QuickVideo();
		System.out.println("Movie QuickVideo Test passed");
	}

	@Test(priority=23)
	public void quicklicense() throws InterruptedException {
		logger = report.createTest("Movies Quick Filing - License Test");
		moviepage.QuickLicense();
		System.out.println("Movie QuickLicense Test passed");
	}

	@Test(priority=24)
	public void quickimage() throws InterruptedException {
		logger = report.createTest("Movies Quick Filing - Images Test");
		moviepage.QuickImages();
		System.out.println("Movie QuickImages Test passed");
	}

	@Test(priority=25)
	public void quickseo() throws InterruptedException {
		logger = report.createTest("Movies Quick Filing - SEO Test");
		moviepage.QuickSEO();
		System.out.println("Movie QuickSEO Test passed");
	}

	@Test(priority=26)
	public void quickmapcontent() throws InterruptedException{
		logger = report.createTest("Movies Quick Filing - Map Content Test");
		moviepage.QuickMapcontent();
		System.out.println("Movie QuickMapcontent Test passed");
	}

	@Test(priority=27)
	public void QuickFilingRelatedContent() throws InterruptedException{
		logger = report.createTest("Movies Quick Filing - Related Content Test");
		moviepage.MovieQuickFilingReleatedContent();
	}
	
	@Test(priority=28)
	public void quickchecklist() throws InterruptedException {
		logger = report.createTest("Movies Quick Filing - Checklist Test");
		moviepage.QuickChecklist();
		System.out.println("Movie QuickChecklist Test passed");
	} 

// Single Landing -------------------------------------------------

	@Test(priority=29)
	public void singlefilling() throws InterruptedException {
		logger = report.createTest("Movies Single Landing - Properties Test");
		moviepage.SingleLanding();
		System.out.println("Movie SingleLanding PropertiesTest passed");
	}
	
	@Test(priority=30)
	public void SingleLicensing() throws InterruptedException {
		logger = report.createTest("Movies Single Landing - License Test");
		moviepage.SingleLicense();
		System.out.println("Movie SingleLicense Test passed");
	}

	@Test(priority=31)
	public void singleimages() throws InterruptedException {
		logger = report.createTest("Movies Single Landing  - Images Test");
		moviepage.SingleImages();
		System.out.println("Movie SingleImages Test passed");
	} 
	
	@Test(priority=32)
	public void singleseo() throws InterruptedException {
		logger = report.createTest("Movies Single Landing  - Seo Test");
		moviepage.SingleSeo();
		System.out.println("Movie SingleSeo Test passed");
	}

	@Test(priority=33)
	public void singleMap() throws InterruptedException {
		logger = report.createTest("Movies Single Landing  - Map Content Test");
		moviepage.SingleMapcontent();
		System.out.println("Movie SingleMap Test passed");
	}

	@Test(priority=34)
	public void SingleLandingRelatedContent() throws InterruptedException{
		logger = report.createTest("Movies Single Landing - Related Content Test");
		moviepage.MovieSingleLandingReleatedContent();
	}
	
	@Test(priority=35)
	public void singlechecklist() throws InterruptedException {
		logger = report.createTest("Movies Single Landing  - Checklist Test");
		moviepage.SingleChecklist();
		System.out.println("Movie Singlechecklist Test passed");
	} 	

	@Test(priority=36)
	public void sortpage() throws InterruptedException {
		logger = report.createTest("Movies Sort Test");
		moviepage.Sort();
		System.out.println("Movie Sortpage Test passed");
	}
	
	@Test(priority=37)
	public void filterpage() throws InterruptedException {
		logger = report.createTest("Movies Filter Test");
		moviepage.Filter();
		System.out.println("Movie Filterpage Test passed");
	}
	
	@Test(priority=38)
	public void MovieAssertionResults() throws InterruptedException, Exception {
		logger = report.createTest("Movies Assertion Results");
		moviepage.AssertionResults();
	}
*/
	@AfterTest
	public void tearDown() throws Exception {
		//report.flush();
	}

}
