package com.zee5.qa.testcases;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.zee5.qa.base.TestBase;
import com.zee5.qa.pages.LoginPage;
import com.zee5.qa.pages.EpisodesPage;
@Listeners(com.zee5.qa.util.Listener.class)


public class EpisodesPageTest extends TestBase {
	EpisodesPage episodesPage;
	LoginPage loginPage;

		
	public EpisodesPageTest(){
		super();
	}
	
	@Parameters({"browserType"})
	@BeforeTest
	public void setup(@Optional String browser) throws Exception{
/*
		try {
		if(browser.equalsIgnoreCase("chrome"))
		{
		System.out.println("Initialize Chrome");
		initializeExtentReport("Chrome");
		initialization("chrome");
		loginPage = new LoginPage();
		loginPage.gmailLogin(prop.getProperty("Gusername"),prop.getProperty("adminPass"));
		}
		else if(browser.equalsIgnoreCase("firefox"))
		{	
		System.out.println("Initialize Firefox");
		initializeExtentReport("Firefox");
		initialization("firefox");
		loginPage = new LoginPage();
		loginPage.gmailLogin(prop.getProperty("Gusername"),prop.getProperty("adminPass"));
		}
		}
		catch(Exception e)
		{
		initializeExtentReport("Chrome");
		initialization("chrome");
		loginPage = new LoginPage();
		loginPage.gmailLogin(prop.getProperty("Gusername"),prop.getProperty("adminPass"));
		}
*/
		episodesPage = new EpisodesPage();
	}

	@Test(priority = 0)
	public void EpisodeProperties() throws Exception {
		logger = report.createTest("Episode Main Content - Properties Test");
		episodesPage.EpisodeProperties();		
	}

	@Test(priority = 1)
	public void EpisodeCastCrew() throws Exception {
		logger = report.createTest("Episode Main Content - Cast Crew Test");
		episodesPage.EpisodeCastCrew();	
	}

	@Test(priority = 2)
	public void EpisodeVideoSection() throws Exception {
		logger = report.createTest("Episode Main Content - Video Section Test");
		episodesPage.EpisodeVideoSection();	
	}

	@Test(priority = 3)
	public void EpisodeLicense() throws Exception {
		logger = report.createTest("Episode Main Content - Cast Crew Test");
		episodesPage.EpisodeLicense();	
	}

	@Test(priority = 4)
	public void EpisodeImages() throws Exception {
		logger = report.createTest("Episode Main Content - Images Test");
		episodesPage.EpisodeImages();	
	}

	@Test(priority = 5)
	public void EpisodeSeoDetails() throws Exception {
		logger = report.createTest("Episode Main Content - Seo Details Test");
		episodesPage.EpisodeSEODetails();	
	}

	@Test(priority = 6)
	public void EpisodeRelatedContent() throws Exception {
		logger = report.createTest("Episode Main Content - Related Content Test");
		episodesPage.EpisodesRelatedContent();	
	}

	@Test(priority = 7)
	public void EpisodeTranslations() throws Exception {
		logger = report.createTest("Episode Main Content - Translations Test");
		episodesPage.EpisodesTranslations();	
	}
	
	@Test(priority = 8)
	public void EpisodePublish() throws Exception {
		logger = report.createTest("Episode Main Content - Publish Flow Test");
		episodesPage.EpisodePublishFlow();	
	}

	@Test(priority = 9)
	public void EpisodeListingPage() throws Exception {
		logger = report.createTest("Episode Listing Page Test");
		episodesPage.EpisodeListingPage();	
	}
	
	@Test(priority = 10)
	public void EpisodeSearch() throws Exception {
		logger = report.createTest("Episode Listing Page Search Test");
		episodesPage.EpisodesListingSearch();	
	}
	
	@Test(priority = 11)
	public void EpisodeAssertionResults() throws Exception {
		logger = report.createTest("Episode Assertion Results");
		episodesPage.EpisodeAssertionResults();	
	}
@AfterTest
	public void tearDown() throws Exception {
	}
	

}
