package com.zee5.qa.testcases;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.zee5.qa.base.TestBase;
import com.zee5.qa.pages.LoginPage;
import com.zee5.qa.pages.VideosPage;
import com.zee5.qa.util.SendMail;
@Listeners(com.zee5.qa.util.Listener.class)

public class VideoPageQuickFillingTest extends TestBase{
	
	SendMail sendmail;
	LoginPage loginPage;
	VideosPage videospage;

	public VideoPageQuickFillingTest() {
		super();
	}

	@Parameters({"browserType"})
	@BeforeTest
	public void setup(@Optional String browser) throws Exception{
/*
		try {
		if(browser.equalsIgnoreCase("chrome"))
		{
		System.out.println("Initialize Chrome");
		initializeExtentReport("Chrome");
		initialization("chrome");
		loginPage = new LoginPage();
		loginPage.gmailLogin(prop.getProperty("Gusername"),prop.getProperty("adminPass"));
		}
		else if(browser.equalsIgnoreCase("firefox"))
		{	
		System.out.println("Initialize Firefox");
		initializeExtentReport("Firefox");
		initialization("firefox");
		loginPage = new LoginPage();
		loginPage.gmailLogin(prop.getProperty("Gusername"),prop.getProperty("adminPass"));
		}
		}
		catch(Exception e)
		{
		initializeExtentReport("Chrome");
		initialization("chrome");
		loginPage = new LoginPage();
		loginPage.gmailLogin(prop.getProperty("Gusername"),prop.getProperty("adminPass"));
		}
	*/
		videospage = new VideosPage();
	}
	

	@Test(priority = 0)
	public void VideoPropertiesQuickFilling() throws Exception {
		logger = report.createTest("Videos Quick Filing - Properties Section Test");
		videospage.VideoQuickFillingVideoProperties();

	}

	@Test(priority = 1)
	public void VideoVideoSectionQuickFilling() throws Exception {
		logger = report.createTest("Videos Quick Filing - Video Section Test");
		videospage.VideoQuickFillingVideos();

	}

	@Test(priority = 2)
	public void VideoLicenseQuickFilling() throws Exception {
		logger = report.createTest("Videos Quick Filing - License Test");
		videospage.VideoQuickFillingLicense();

	}

	@Test(priority = 3)
	public void VideoImagesQuickFilling() throws Exception {
		logger = report.createTest("Videos Quick Filing - Images Test");
		videospage.VideoQuickFillingImages();

	}

	@Test(priority = 4)
	public void VideoSeoDetailsQuickFilling() throws Exception {
		logger = report.createTest("Videos Quick Filing - SEO Details Test");
		videospage.VideoQuickFillingSeoDetails();

	}

	@Test(priority = 5)
	public void VideoMapContentQuickFilling() throws Exception {
		logger = report.createTest("Videos Quick Filing - Map Content Test");
		videospage.VideoQuickFillingMapContent();

	}
	
	@Test(priority = 5)
	public void VideoRelatedContentQuickFilling() throws Exception {
		logger = report.createTest("Videos Quick Filing - Related Content Test");
		videospage.VideosRelatedContent();

	}
	

	@Test(priority = 6)
	public void VideoQuickFillingPublishFlow() throws Exception {
		logger = report.createTest("Videos Quick Filing - Publish Flow Test");
		videospage.VideoQuickFillingPublishFlow();

	}
	
	@Test(priority = 7)
	public void VideoQuickFilingAssertionResults() throws Exception {
		logger = report.createTest("Video Quick Filing Assertion Results");
		videospage.AssertionResults();		
	}
	
@AfterTest

	public void tearDown() throws Exception {
		//Thread.sleep(3000);
	}
	

}
