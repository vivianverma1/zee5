package com.zee5.qa.testcases;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.zee5.qa.base.TestBase;
import com.zee5.qa.pages.LoginPage;
import com.zee5.qa.pages.VideosPage;
@Listeners(com.zee5.qa.util.Listener.class)

public class VideosPageTest extends TestBase {
	VideosPage videospage;
	LoginPage loginPage;

	public VideosPageTest() {
		super();
	}
	
	@Parameters({"browserType"})
	@BeforeTest
	public void setup(@Optional String browser) throws Exception{
/*
		try {
		if(browser.equalsIgnoreCase("chrome"))
		{
		System.out.println("Initialize Chrome");
		initializeExtentReport("Chrome");
		initialization("chrome");
		loginPage = new LoginPage();
		loginPage.gmailLogin(prop.getProperty("Gusername"),prop.getProperty("adminPass"));
		}
		else if(browser.equalsIgnoreCase("firefox"))
		{	
		System.out.println("Initialize Firefox");
		initializeExtentReport("Firefox");
		initialization("firefox");
		loginPage = new LoginPage();
		loginPage.gmailLogin(prop.getProperty("Gusername"),prop.getProperty("adminPass"));
		}
		}
		
		//IF not running through suite , set your desired browser here 
		catch(Exception e)
		{
		initializeExtentReport("Firefox");
		initialization("firefox");
		loginPage = new LoginPage();
		loginPage.gmailLogin(prop.getProperty("Gusername"),prop.getProperty("adminPass"));
		}
*/
		videospage = new VideosPage();		
	}

	@Test(priority = 0)
	public void VideoProperties() throws Exception {
		logger = report.createTest("Videos Main Content - Properties Test");			
		videospage.VideoVideoProperties();
	}

	@Test(priority = 1)
	public void VideoPropertiesAssertions() throws Exception {
		logger = report.createTest("Videos Main Content - Properties Assertions Test");
		videospage.VideoPropertiesAssertions();
	}
	
	@Test(priority = 2)
	public void VideoCastCrew() throws Exception {
		logger = report.createTest("Videos Main Content - Cast & Crew Test");
		videospage.VideoCastCrew();	
	}
	
	@Test(priority = 3)
	public void VideoCastCrewAssertions() throws Exception {
		logger = report.createTest("Videos Main Content -Cast & Crew Assertions Test");
		videospage.VideoCastCrewAssertions();
	}

	@Test(priority = 4)
	public void VideoVideoSection() throws Exception {
		logger = report.createTest("Videos Main Content - Video Section Test");
		videospage.VideoVideoSection();
	}

	@Test(priority = 5)
	public void VideoVideoSectionAssertions() throws Exception {
		logger = report.createTest("Videos Main Content - Video Section Assertions Test");
		videospage.VideoVideoSectionAssertions();
	}

	@Test(priority = 6)
	public void VideoLicense() throws Exception {
		logger = report.createTest("Videos Main Content - License Test");
		videospage.VideoLicense();
	}

	@Test(priority = 7)
	public void VideoImages() throws Exception {
		logger = report.createTest("Videos Main Content - Images Test");
		videospage.VideoImages();
	}

	@Test(priority = 8)
	public void VideoImagesAssertions() throws Exception {
		logger = report.createTest("Videos Main Content - Images Assertions Test");
		videospage.VideoImagesAssertions();
	}

	@Test(priority = 9)
	public void VideoSeoDetails() throws Exception {
		logger = report.createTest("Videos Main Content - SEO Details Test");
		videospage.VideoSeoDetails();
	}

	@Test(priority = 10)
	public void VideoSeoDetailsAssertions() throws Exception {
		logger = report.createTest("Videos Main Content - SEO Details Assertions Test");
		videospage.VideoSeoDetailsAssertions();
	}

	@Test(priority = 11)
	public void VideoMapContent() throws Exception {
		logger = report.createTest("Videos Main Content - Map Content Test");
		videospage.VideoMapContent();
	}

	@Test(priority = 12)
	public void VideoMapContentAssertions() throws Exception {
		logger = report.createTest("Videos Main Content - Map Content Assertions Test");
		videospage.VideoMapContentAssertions();
	}

	@Test(priority = 13)
	public void VideoRelatedContent() throws Exception {
		logger = report.createTest("Videos Main Content - Related Content Test");
		videospage.VideosRelatedContent();		
	}

	@Test(priority = 14)
	public void VideoTranslations() throws Exception {
		logger = report.createTest("Videos Main Content - Translations Test");
		videospage.VideosTranslations();
	}

	@Test(priority = 15)
	public void VideoPublishFlow() throws Exception {
		logger = report.createTest("Videos Main Content - Publish Flow Test");
		videospage.VideoPublishFlow();
	}

	@Test(priority = 16)
	public void VideoScheduleContent() throws Exception {
		logger = report.createTest("Videos Main Content - Schedule Content Test");
		videospage.VideoScheduleContent();
	}

	@Test(priority = 17)
	public void VideoClone() throws Exception {
		logger = report.createTest("Videos Clone Test");
		videospage.VideoClone();
	}

	@Test(priority = 18)
	public void VideoFilter() throws Exception {
		logger = report.createTest("Videos Filter Test");
		videospage.VideoFilter();
	}

	@Test(priority = 19)
	public void VideoSorting() throws Exception {
		logger = report.createTest("Videos Sorting Test");
		videospage.VideoSort();
	}
	
	@Test(priority = 20)
	public void VideoListingPageTest() throws Exception {
		logger = report.createTest("Videos Listing Page Test");
		videospage.VideoListingPage();
	}

	@Test(priority = 21)
	public void VideoListingSearch() throws Exception {
		logger = report.createTest("Videos Listing Page Search Test");
		videospage.VideoListingSearch();
	}
	

	@Test(priority = 22)
	public void VideoAssertionResults() throws Exception {
		logger = report.createTest("Video Assertion Results");
		videospage.AssertionResults();		
	}
@AfterTest

	public void tearDown() throws Exception {
	//report.flush();
	}

}
