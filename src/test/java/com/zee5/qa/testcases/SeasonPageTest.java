package com.zee5.qa.testcases;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.zee5.qa.base.TestBase;
import com.zee5.qa.pages.LoginPage;
import com.zee5.qa.pages.SeasonsPage;
@Listeners(com.zee5.qa.util.Listener.class)


public class SeasonPageTest extends TestBase {
	SeasonsPage seasonspage;
	LoginPage loginPage;

		
	public SeasonPageTest(){
		super();
	}

	@Parameters({"browserType"})
	@BeforeTest
	public void setup(@Optional String browser) throws Exception{
/*
		try {
		if(browser.equalsIgnoreCase("chrome"))
		{
		System.out.println("Initialize Chrome");
		initializeExtentReport("Chrome");
		initialization("chrome");
		loginPage = new LoginPage();
		loginPage.gmailLogin(prop.getProperty("Gusername"),prop.getProperty("adminPass"));
		}
		else if(browser.equalsIgnoreCase("firefox"))
		{	
		System.out.println("Initialize Firefox");
		initializeExtentReport("Firefox");
		initialization("firefox");
		loginPage = new LoginPage();
		loginPage.gmailLogin(prop.getProperty("Gusername"),prop.getProperty("adminPass"));
		}
		}
		catch(Exception e)
		{
		initializeExtentReport("Firefox");
		initialization("chrome");
		loginPage = new LoginPage();
		loginPage.gmailLogin(prop.getProperty("Gusername"),prop.getProperty("adminPass"));
		}
*/
		seasonspage = new SeasonsPage();
	}

	@Test(priority = 0)
	public void SeasonProperties() throws Exception {
		logger = report.createTest("Season Main Content - Properties Test");		
		seasonspage.SeasonProperties();
		
	}

	@Test(priority = 1)
	public void SeasonPropertiesAssertion() throws Exception {
		logger = report.createTest("Season Main Content - Properties Assertion Test");
		seasonspage.SeasonPropertiesAssertions();	
	}
	
	@Test(priority = 2)
	public void SeasonCastCrew() throws Exception {
		logger = report.createTest("Season Main Content - Cast Crew Test");
		seasonspage.SeasonCastCrew();	
	}
			
	@Test(priority = 3)
	public void SeasonCastCrewAssertion() throws Exception {
		logger = report.createTest("Season Main Content - Cast and Crew Assertion Test");
		seasonspage.SeasonCastCrewAssertions();	
	}
	
	@Test(priority = 4)
	public void SeasonLicense() throws Exception {
		logger = report.createTest("Season Main Content - Cast Crew Test");
		seasonspage.SeasonLicense();	
	}

	@Test(priority = 5)
	public void SeasonImages() throws Exception {
		logger = report.createTest("Season Main Content - Images Test");
		seasonspage.SeasonImages();	
	}
	
	@Test(priority = 6)
	public void SeasonImagesAssertion() throws Exception {
		logger = report.createTest("Season Main Content - Images Assertion Test");
		seasonspage.SeasonImagesAssertions();	
	}
	
	@Test(priority = 7)
	public void SeasonSeoDetails() throws Exception {
		logger = report.createTest("Main Content -  Season Seo Details Test");
		seasonspage.SeasonSEODetails();	
	}
	
	@Test(priority = 8)
	public void SeasonRelatedContent() throws Exception {
		logger = report.createTest("Season Main Content - Related Content Test");
		seasonspage.SeasonsRelatedContent();	
	}
	
	@Test(priority = 9)
	public void SeasonTranslations() throws Exception {
		logger = report.createTest("Season Main Content - Translations Test");
		seasonspage.SeasonsTranslations();	
	}
	
	@Test(priority = 10)
	public void SeasonPublish() throws Exception {
		logger = report.createTest("Season Main Content - Publish Flow Test");
		seasonspage.SeasonsPublishFlow();	
	}

	@Test(enabled = false)
	public void SeasonOrdering() throws Exception {
		logger = report.createTest("Season Main Content - Ordering Test");
		seasonspage.SeasonOrdering();	
	}

	@Test(priority = 12)
	public void SeasonListingPage() throws Exception {
		logger = report.createTest("Season Listing Page Test");
		seasonspage.SeasonsListingPage();	
	}
	
	@Test(priority = 13)
	public void SeasonSearch() throws Exception {
		logger = report.createTest("Season Listing Page Search Test");
		seasonspage.SeasonsListingSearch();	
	}
	
	@Test(priority = 14)
	public void SeasonAssertionResults() throws Exception {
		logger = report.createTest("Season Assertion Result");
		seasonspage.SeasonAssertionResults();	
	}
	
@AfterTest
	public void tearDown() throws Exception {
	}
	

}
