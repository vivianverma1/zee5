package com.zee5.qa.testcases;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.zee5.qa.base.TestBase;
import com.zee5.qa.pages.LoginPage;
import com.zee5.qa.pages.EpisodesPage;
@Listeners(com.zee5.qa.util.Listener.class)


public class EpisodesSingleLandingPageTest extends TestBase {
	EpisodesPage episodesPage;
	LoginPage loginPage;

		
	public EpisodesSingleLandingPageTest(){
		super();
	}
	
	@Parameters({"browserType"})
	@BeforeTest
	public void setup(@Optional String browser) throws Exception{
/*
		try {
		if(browser.equalsIgnoreCase("chrome"))
		{
		System.out.println("Initialize Chrome");
		initializeExtentReport("Chrome");
		initialization("chrome");
		loginPage = new LoginPage();
		loginPage.gmailLogin(prop.getProperty("Gusername"),prop.getProperty("adminPass"));
		}
		else if(browser.equalsIgnoreCase("firefox"))
		{	
		System.out.println("Initialize Firefox");
		initializeExtentReport("Firefox");
		initialization("firefox");
		loginPage = new LoginPage();
		loginPage.gmailLogin(prop.getProperty("Gusername"),prop.getProperty("adminPass"));
		}
		}
		catch(Exception e)
		{
		initializeExtentReport("Firefox");
		initialization("firefox");
		loginPage = new LoginPage();
		loginPage.gmailLogin(prop.getProperty("Gusername"),prop.getProperty("adminPass"));
		}*/
		episodesPage = new EpisodesPage();
	}

	@Test(priority = 0)
	public void EpisodeProperties() throws Exception {
		logger = report.createTest("Episode Single Landing - Properties Test");
		episodesPage.EpisodeSingleLandingProperties();
		
	}

	@Test(priority = 1)
	public void EpisodeCastCrew() throws Exception {
		logger = report.createTest("Episode Single Landing - Cast Crew Test");
		episodesPage.EpisodeSingleLandingCastCrew();	
	}
	

	@Test(priority = 2)
	public void EpisodeLicense() throws Exception {
		logger = report.createTest("Episode Single Landing - Cast Crew Test");
		episodesPage.EpisodeSingleLandingLicense();	
	}

	@Test(priority = 3)
	public void EpisodeImages() throws Exception {
		logger = report.createTest("Episode Single Landing - Images Test");
		episodesPage.EpisodeSingleLandingImages();	
	}

	@Test(priority = 4)
	public void EpisodeSeoDetails() throws Exception {
		logger = report.createTest("Episode Single Landing - Seo Details Test");
		episodesPage.EpisodeSingleLandingSeoDetails();	
	}

	@Test(priority = 5)
	public void EpisodeRelatedContent() throws Exception {
		logger = report.createTest("Episode Single Landing - Related Content Test");
		episodesPage.EpisodesRelatedContent();	
	}
	
	@Test(priority = 6)
	public void EpisodePublish() throws Exception {
		logger = report.createTest("Episode Single Landing - Publish Flow Test");
		episodesPage.EpisodeSingleLandingPublishFlow();	
	}

	@Test(priority = 7)
	public void EpisodeAssertions() throws Exception {
		logger = report.createTest("Episode Single Landing - Assertions");
		episodesPage.EpisodeAssertionResults();	
	}
@AfterTest
	public void tearDown() throws Exception {
	}
	

}
