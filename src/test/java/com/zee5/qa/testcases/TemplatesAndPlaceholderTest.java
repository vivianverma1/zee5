package com.zee5.qa.testcases;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.zee5.qa.base.TestBase;
import com.zee5.qa.pages.LoginPage;
import com.zee5.qa.pages.EpisodesPage;
@Listeners(com.zee5.qa.util.Listener.class)


public class TemplatesAndPlaceholderTest extends TestBase {
	EpisodesPage episodesPage;
	LoginPage loginPage;

		
	public TemplatesAndPlaceholderTest(){
		super();
	}
	
	@Parameters({"browserType"})
	@BeforeTest
	public void setup(@Optional String browser) throws Exception{

		try {
		if(browser.equalsIgnoreCase("chrome"))
		{
		System.out.println("Initialize Chrome");
		initializeExtentReport("Chrome");
		initialization("chrome");
		loginPage = new LoginPage();
		loginPage.gmailLogin(prop.getProperty("Gusername"),prop.getProperty("adminPass"));
		}
		else if(browser.equalsIgnoreCase("firefox"))
		{	
		System.out.println("Initialize Firefox");
		initializeExtentReport("Firefox");
		initialization("firefox");
		loginPage = new LoginPage();
		loginPage.gmailLogin(prop.getProperty("Gusername"),prop.getProperty("adminPass"));
		}
		}
		catch(Exception e)
		{
		initializeExtentReport("Firefox");
		initialization("firefox");
		loginPage = new LoginPage();
		loginPage.gmailLogin(prop.getProperty("Gusername"),prop.getProperty("adminPass"));
		}
		episodesPage = new EpisodesPage();
	}	

	@Test(priority = 0)
	public void Templates() throws Exception {
		logger = report.createTest("Episode Placeholder Test");
		episodesPage.Templates();
		
	}
	
	@Test(priority = 1)
	public void EpisodePlaceholder() throws Exception {
		logger = report.createTest("Episode Placeholder Test");
		episodesPage.EpisodePlaceholder();
		
	}
		
	
@AfterTest
	public void tearDown() throws Exception {
	}
	

}
