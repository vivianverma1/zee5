package com.zee5.qa.testcases;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.zee5.qa.base.TestBase;
import com.zee5.qa.pages.LoginPage;
import com.zee5.qa.pages.TvShowsPage;
@Listeners(com.zee5.qa.util.Listener.class)
public class TvShowsQuickFilingTest extends TestBase {
	TvShowsPage tvshowspage;
	LoginPage loginPage;
	
	public TvShowsQuickFilingTest(){
		super();
	}
	
	@Parameters({"browserType"})
	@BeforeTest
	public void setup(@Optional String browser) throws Exception{
/*
		try {
		if(browser.equalsIgnoreCase("chrome"))
		{
		System.out.println("Initialize Chrome");
		initializeExtentReport("Chrome");
		initialization("chrome");
		loginPage = new LoginPage();
		loginPage.gmailLogin(prop.getProperty("Gusername"),prop.getProperty("adminPass"));
		}
		else if(browser.equalsIgnoreCase("firefox"))
		{	
		System.out.println("Initialize Firefox");
		initializeExtentReport("Firefox");
		initialization("firefox");
		loginPage = new LoginPage();
		loginPage.gmailLogin(prop.getProperty("Gusername"),prop.getProperty("adminPass"));
		}
		}
		catch(Exception e)
		{
		initializeExtentReport("Firefox");
		initialization("firefox");
		loginPage = new LoginPage();
		loginPage.gmailLogin(prop.getProperty("Gusername"),prop.getProperty("adminPass"));
		}*/
		tvshowspage= new TvShowsPage();
		
	}

	@Test(priority = 0)
	public void TvPropertiesQuickFiling() throws Exception{
		 logger=report.createTest("TV Shows Quick Filling - Properties");
		 tvshowspage.TvPropertiesQuickFiling();
	}
	

	@Test(priority = 1)
	public void TvLicenseQuickFiling() throws Exception {
		logger = report.createTest("TV Shows Quick Filling - License");
		tvshowspage.TvLicenseQuickFiling();
	}

	@Test(priority = 2)
	public void TvImagesQuickFiling() throws Exception {
		logger = report.createTest("TV Shows Quick Filling - Images");
		tvshowspage.TvImagesQuickFiling();
	}

	@Test(priority = 3)
	public void TvSeoDetailsQuickFiling() throws Exception {
		logger = report.createTest("TV Shows Quick Filling - SEO Details");
		tvshowspage.TvSeoDetailsQuickFiling();
	}

	@Test(priority = 4)
	public void TvMapContentQuickFiling() throws Exception {
		logger = report.createTest("TV Shows Quick Filling - Map Content");
		tvshowspage.TvMapContentQuickFiling();
	}

	@Test(priority = 5)
	public void TvRelatedContentQuickFiling() throws Exception {
		logger = report.createTest("TV Shows Quick Filling - Related Content");
		tvshowspage.TvRelatedContent();
	}


	@Test(priority = 6)
	public void TvQuickFilingPublishFlow() throws Exception {
		logger = report.createTest("TV Shows Quick Filling - Publish Flow Test");
		tvshowspage.TvQuickFilingPublishFlow();
	}
	
	@Test(priority = 7)
	public void TvQuickFilingAssertions() throws Exception {
		logger = report.createTest("TV Shows Quick Filling - Assertions");
		tvshowspage.TVAssertionResults();
	}
@AfterTest

	public void tearDown() throws Exception {
	}
	

}
